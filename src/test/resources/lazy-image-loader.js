(() => {
    const imageLoader = () => {
        if (document.location.href === 'about:blank') {
            return;
        }
        const images = document.querySelectorAll('img');
        if (!images) {
            return;
        }
        let lazyLoadImageCount = 0;
        let lazyLoadedImageCount = 0;
        images.forEach(next => {
            const src = next.src;
            // force to load the resource immediately
            if (next.getAttribute('loading') === 'lazy') {
                lazyLoadImageCount += 1;
                const loadListener = () => {
                    lazyLoadedImageCount += 1;
                    window.__cdp4jImageLoaderProgress = (lazyLoadedImageCount / lazyLoadImageCount) * 100;
                    next.removeEventListener('load', loadListener);
                };
                next.addEventListener('load', loadListener);
                next.setAttribute('loading', 'eager');
                return;
            }
            const isLazyLoadImage = (name, value, src) => {
                return name &&
                    value &&
                    (/\.(gif|jpe?g|tiff?|png|webp|bmp)$/i).test(value) &&
                    src !== value;
            };
            // iterate dataset
            for (let name in next.dataset) {
                const value = next.dataset[name];
                if (isLazyLoadImage(name, value, src)) {
                    lazyLoadImageCount += 1;
                    const isAbsolute = value.startsWith('http') || value.startsWith('//');
                    if (isAbsolute) {
                        next.src = value;
                    } else {
                        next.src = document.location.href + value;
                    }
                    next.style.opacity = 1;
                    const loadListener = () => {
                        lazyLoadedImageCount += 1;
                        window.__cdp4jImageLoaderProgress = (lazyLoadedImageCount / lazyLoadImageCount) * 100;
                        next.removeEventListener('load', loadListener);
                    };
                    next.addEventListener('load', loadListener);
                    return;
                }
            }
            // iterate attributes
            const attributes = next.getAttributeNames();
            for (let i = 0; i < attributes.length; i++) {
                const name = attributes[i];
                const value = next.getAttribute(name);
                if (isLazyLoadImage(name, value, src)) {
                    lazyLoadImageCount += 1;
                    const isAbsolute = value.startsWith('http') || value.startsWith('//');
                    if (isAbsolute) {
                        next.src = value;
                    } else {
                        next.src = document.location.href + value;
                    }
                    next.style.opacity = 1;
                    const loadListener = () => {
                        lazyLoadedImageCount += 1;
                        window.__cdp4jImageLoaderProgress = (lazyLoadedImageCount / lazyLoadImageCount) * 100;
                        next.removeEventListener('load', loadListener);
                    };
                    next.addEventListener('load', loadListener);
                    return;
                }
            }
        });
    };
    document.addEventListener('DOMContentLoaded', imageLoader);
})();