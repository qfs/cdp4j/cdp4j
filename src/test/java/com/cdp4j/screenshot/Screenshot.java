// SPDX-License-Identifier: MIT
package com.cdp4j.screenshot;

import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Files.write;
import static java.nio.file.Paths.get;

import com.cdp4j.Launcher;
import com.cdp4j.command.Page;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;
import java.nio.file.Path;

public class Screenshot {

    public static void main(String[] args) throws IOException, InterruptedException {
        // an optional javascript utility that helps to force load all images
        String imageLoaderJs = new String(readAllBytes(get("src/test/resources/lazy-image-loader.js")));

        Launcher launcher = new Launcher();

        Path file = createTempFile("screenshot", ".png");

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            Page page = session.getCommand().getPage();
            // addScriptToEvaluateOnNewDocument() must be called before Session.navigate()
            page.addScriptToEvaluateOnNewDocument(imageLoaderJs);

            session.navigate("https://www.dw.com/");
            session.waitDocumentReady();

            // wait until most of images loaded
            session.waitUntil(
                    s -> {
                        Double result = (Double) s.evaluate("window.__cdp4jImageLoaderProgress");
                        return result.doubleValue() >= 95.00;
                    },
                    5_000);

            byte[] data = session.captureScreenshot();
            write(file, data);
        } finally {
            launcher.kill();
        }

        if (isDesktopSupported()) {
            getDesktop().open(file.toFile());
        }
    }
}
