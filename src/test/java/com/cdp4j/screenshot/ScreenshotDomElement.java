// SPDX-License-Identifier: MIT
package com.cdp4j.screenshot;

import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.write;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.constant.ImageFormat;
import com.cdp4j.type.dom.BoxModel;
import com.cdp4j.type.page.Viewport;
import java.io.IOException;
import java.nio.file.Path;

public class ScreenshotDomElement {

    public static void main(String[] args) throws IOException, InterruptedException {
        Launcher launcher = new Launcher(Options.builder().headless(true).build());

        Path file = createTempFile("screenshot", ".png");

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://google.com");
            session.waitDocumentReady();

            // get x,y coordinate, width & height of the specific HTMLElement
            BoxModel boxModel = session.getBoxModel("footer");

            double x = boxModel.getContent().get(0);
            double y = boxModel.getContent().get(1);

            // this helps to capture specific coordinates of the page
            Viewport clip = new Viewport();
            clip.setScale(1D);
            clip.setWidth((double) boxModel.getWidth());
            clip.setHeight((double) boxModel.getHeight());
            clip.setX(x);
            clip.setY(y);

            byte[] data = session.captureScreenshot(ImageFormat.Png, 1, clip, Boolean.TRUE);
            write(file, data);
        } finally {
            launcher.kill();
        }

        if (isDesktopSupported()) {
            getDesktop().open(file.toFile());
        }
    }
}
