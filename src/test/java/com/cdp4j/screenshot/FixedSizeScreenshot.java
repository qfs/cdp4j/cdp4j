// SPDX-License-Identifier: MIT
package com.cdp4j.screenshot;

import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.write;

import com.cdp4j.Launcher;
import com.cdp4j.command.CSS;
import com.cdp4j.command.DOM;
import com.cdp4j.command.Emulation;
import com.cdp4j.command.Page;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.constant.ImageFormat;
import com.cdp4j.type.css.CSSRule;
import com.cdp4j.type.css.SourceRange;
import com.cdp4j.type.dom.RGBA;
import com.cdp4j.type.dom.Rect;
import com.cdp4j.type.page.GetLayoutMetricsResult;
import com.cdp4j.type.page.Viewport;
import java.io.IOException;
import java.nio.file.Path;

public class FixedSizeScreenshot {

    public static void main(String[] args) throws IOException, InterruptedException {
        Launcher launcher = new Launcher();

        Path file = createTempFile("screenshot", ".png");
        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            session.navigate("https://news.ycombinator.com/");
            session.waitDocumentReady();

            final double maxWidth = 800d;
            final double maxHeight = 800d;

            Page page = session.getCommand().getPage();
            GetLayoutMetricsResult metrics = page.getLayoutMetrics();
            Rect cs = metrics.getContentSize();

            double captureWidth = cs.getWidth();
            double captureHeight = cs.getHeight();

            if (cs.getWidth().doubleValue() > maxHeight) {
                captureWidth = maxWidth;
            }
            if (cs.getHeight().doubleValue() > maxHeight) {
                captureHeight = maxHeight;
            }

            Viewport clip = new Viewport();
            clip.setHeight(captureWidth);
            clip.setWidth(captureHeight);
            clip.setScale(1D);
            clip.setX(0d);
            clip.setY(0d);

            Emulation emul = session.getCommand().getEmulation();

            RGBA bgColor = new RGBA();
            bgColor.setA(0.0);
            bgColor.setR(0);
            bgColor.setG(0);
            bgColor.setB(0);
            emul.setDefaultBackgroundColorOverride(bgColor);

            emul.setDeviceMetricsOverride((int) captureWidth, (int) captureHeight, 1D, Boolean.FALSE);

            // ****************************************************************
            // remove margin and paddings of body & html (optional)
            DOM dom = session.getCommand().getDOM();
            dom.enable();
            CSS css = session.getCommand().getCSS();
            css.enable();
            String styleSheetId = css.createStyleSheet(session.getFrameId());
            SourceRange location = new SourceRange();
            location.setEndColumn(0);
            location.setEndLine(0);
            location.setStartColumn(0);
            location.setStartLine(0);
            // with or without this css rule, chrome always generate 800 x 800
            // with this rule we remove margins & padding from body
            CSSRule rule =
                    css.addRule(styleSheetId, "body, html { margin: 0 !important; padding: 0 !important; }", location);
            // ****************************************************************

            // Important: Don't use session.captureScreenshot() which's override devices metrics internally
            byte[] data = page.captureScreenshot(ImageFormat.Png, 1, clip, Boolean.TRUE, Boolean.TRUE, null);

            // remove custom css rule (optional)
            css.setStyleSheetText(rule.getStyleSheetId(), "");

            write(file, data);
        } finally {
            launcher.kill();
        }

        if (isDesktopSupported()) {
            System.out.println(file);
            getDesktop().open(file.toFile());
        }
    }
}
