// SPDX-License-Identifier: MIT
package com.cdp4j.dom;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class SendKeys {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.getCommand().getNetwork().enable();
            session.navigate("https://google.com");
            session.waitDocumentReady();
            session.sendKeys("google");
            session.sendEnter();
            session.wait(2000);
        } finally {
            launcher.kill();
        }
    }
}
