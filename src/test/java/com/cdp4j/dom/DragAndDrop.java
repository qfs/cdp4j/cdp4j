// SPDX-License-Identifier: MIT
package com.cdp4j.dom;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.net.URL;

public class DragAndDrop {

    public static void main(String[] args) {
        URL url = DragAndDrop.class.getResource("/drag-and-drop.html");

        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate(url.toString());
            session.waitDocumentReady();

            session.dragAndDrop("#drag", "#drop");

            assertEquals(true, session.evaluate("window.didDragStart"));
            assertEquals(true, session.evaluate("window.didDragEnter"));
            assertEquals(true, session.evaluate("window.didDragOver"));
            assertEquals(true, session.evaluate("window.didDrop"));
        } finally {
            launcher.kill();
        }
    }
}
