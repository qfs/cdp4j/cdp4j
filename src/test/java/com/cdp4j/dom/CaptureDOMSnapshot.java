// SPDX-License-Identifier: MIT
package com.cdp4j.dom;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;

public class CaptureDOMSnapshot {

    public static void main(String[] args) throws IOException {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://cnn.com");
            session.waitDocumentReady();
            // Returns a document snapshot, including the full DOM tree of the root node (including iframes,
            // template contents, and imported documents).
            String snapshot = session.getDOMSnapshot();
            System.out.println(snapshot);
        } finally {
            launcher.kill();
        }
    }
}
