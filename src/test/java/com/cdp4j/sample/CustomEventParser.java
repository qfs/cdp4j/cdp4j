// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import static com.cdp4j.event.Events.NetworkRequestWillBeSent;

import com.cdp4j.JsonLibrary;
import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.event.Events;
import com.cdp4j.serialization.DefaultEventParser;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;

public class CustomEventParser {

    private static class RequestInfo {
        public String requestId, url;

        @Override
        public String toString() {
            return "RequestInfo [requestId=" + requestId + ", url=" + url + "]";
        }
    }

    public static void main(String[] args) {
        Launcher launcher =
                new Launcher(Options.builder().jsonLibrary(JsonLibrary.Jackson).build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            session.getCommand().getNetwork().enable();

            // this custom event parser skip unused/unwanted fields
            // this usage help to decrease memory and CPU usage
            session.setEventParser(new DefaultEventParser() {

                @Override
                @SuppressWarnings("unused")
                public Object parse(Events event, Session session, JsonParser parser, ObjectReader reader)
                        throws IOException {
                    switch (event) {
                        case NetworkRequestWillBeSent:
                            RequestInfo nws = new RequestInfo();
                            JsonToken token = null;
                            while ((token = parser.nextToken()) != null) {
                                String name = parser.currentName();
                                if ("requestId".equals(name)) {
                                    nws.requestId = parser.getValueAsString();
                                } else if ("url".equals(name)) {
                                    nws.url = parser.getValueAsString();
                                }
                            }
                            return nws;
                        default:
                            return super.parse(event, session, parser, reader);
                    }
                }
            });

            session.addEventListener((event, value) -> {
                if (event == NetworkRequestWillBeSent) {
                    RequestInfo ri = (RequestInfo) value;
                    System.out.println(ri);
                }
            });

            session.navigate("https://yahoo.com");
            session.waitDocumentReady();
        } finally {
            launcher.kill();
        }
    }
}
