// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import static com.cdp4j.JsonLibrary.Jackson;
import static com.cdp4j.SelectorEngine.Playwright;
import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.session.WaitUntil.NetworkIdle;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class AutomationExample {

    public static void main(String[] args) {
        Options options = Options.builder()
                .jsonLibrary(Jackson)
                .createNewUserDataDir(true)
                .loggerType(Console)
                .selectorEngine(Playwright)
                .build();

        Launcher launcher = new Launcher(options);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            session.enableNetworkLog();

            session.navigate("https://danube-webshop.herokuapp.com/");
            session.waitDocumentReady();

            // focus to search form
            session.focus(":left-of(button:has-text('%s'))", "SEARCH");
            // type the product name
            session.sendKeys("Parry Hotter");
            // click to search button
            session.click("button:has-text('%s')", "SEARCH");

            session.waitUntil(NetworkIdle);

            session.click("li:has-text('%s')", "Parry Hotter");

            // wait until product page load
            boolean productPageLoaded = session.waitUntil(s -> s.matches("button:has-text('%s')", "ADD TO CART"));
            assert productPageLoaded;
            session.click("button:has-text('%s')", "ADD TO CART");

            // wait until shopping cart load
            boolean addToCartPageLoaded = session.waitUntil(s -> s.matches("button:has-text('%s')", "CHECKOUT"));
            assert addToCartPageLoaded;
            session.click("button:has-text('%s')", "CHECKOUT");

            // wait until checkout page load
            boolean checkoutPageLoaded = session.waitUntil(s -> s.matches("h1:has-text('%s')", "Checkout"));
            assert checkoutPageLoaded;

            // fill the form
            session.focus("[placeholder='%s']", "Name");
            session.sendKeys("Foo");
            session.focus("[placeholder='%s']", "Surname");
            session.sendKeys("Bar");
            session.focus("[placeholder='%s']", "Address");
            session.sendKeys("My Address");
            session.focus("[placeholder='%s']", "Zipcode");
            session.sendKeys("11111");
            session.focus("[placeholder='%s']", "City");
            session.sendKeys("My City");
            session.sendTab();
            session.sendKeys("My Company");

            // click to radio button
            session.click(":left-of(:has-text('%s'))", "as soon as possible");

            // focus & click to buy button
            session.focus("button:has-text('%s')", "BUY");
            session.sendEnter();

            boolean loadedDonePage = session.waitUntil(s -> s.matches("button:has-text('%s')", "SHOP MORE"));
            assert loadedDonePage;

            // we are done, test executed successfully
        } finally {
            launcher.kill();
        }
    }
}
