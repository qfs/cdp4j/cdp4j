// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class HelloWorld {

    public static void main(String[] args) {
        Launcher launcher = new Launcher(Options.builder().headless(true).build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://google.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            launcher.kill();
        }
    }
}
