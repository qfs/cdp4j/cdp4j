// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.Launcher;
import com.cdp4j.command.Tracing;
import com.cdp4j.event.Events;
import com.cdp4j.event.tracing.DataCollected;
import com.cdp4j.listener.EventListener;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.constant.TracingTransferMode;
import com.cdp4j.type.tracing.StreamFormat;
import com.cdp4j.type.tracing.TracingBackend;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

public class Profiling {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            Tracing tracing = session.getCommand().getTracing();
            tracing.start(
                    "*", // * => trace all types of categories
                    "sampling-frequency=10000",
                    500D,
                    TracingTransferMode.ReportEvents,
                    StreamFormat.Json,
                    null,
                    null,
                    null,
                    TracingBackend.Auto);

            session.navigate("https://google.com");
            session.waitDocumentReady();

            CountDownLatch tracingLatch = new CountDownLatch(1);

            session.addEventListener(new EventListener() {

                @Override
                public void onEvent(Events event, Object value) {
                    if (Events.TracingDataCollected.equals(event)) {
                        DataCollected dataCollected = (DataCollected) value;
                        //
                        // Tracing Event is not part of the published DevTools protocol as of version 1.1
                        //
                        // See http://chromedriver.chromium.org/logging/performance-log for more details
                        //
                        for (Map<String, Object> next : dataCollected.getValue()) {
                            System.out.println("Tracing Event: " + next);
                        }
                    } else if (Events.TracingTracingComplete.equals(event)) {
                        tracingLatch.countDown();
                    }
                }
            });

            tracing.end();
            tracingLatch.await();

            System.out.println("Tracing ended");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            launcher.kill();
        }
    }
}
