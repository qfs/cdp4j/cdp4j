// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import static java.lang.System.getProperty;
import static java.nio.file.Paths.get;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.Path;
import java.util.Random;

public class MultiProcess {

    public static void main(String[] args) throws InterruptedException {
        new Thread() {

            public void run() {
                Path remoteProfileData =
                        get(getProperty("java.io.tmpdir")).resolve("remote-profile-" + new Random().nextInt());
                Options options =
                        Options.builder().userDataDir(remoteProfileData).build();
                Launcher launcher = new Launcher(options);

                SessionFactory factory = launcher.launch();

                try (SessionFactory sf = factory) {
                    try (Session session = sf.create()) {
                        session.navigate("https://google.com");
                        session.waitDocumentReady();
                        System.err.println(
                                "Content Length: " + session.getContent().length());
                    }
                }
            }
        }.start();

        new Thread() {

            public void run() {
                Path remoteProfileData =
                        get(getProperty("java.io.tmpdir")).resolve("remote-profile-" + new Random().nextInt());
                Options options =
                        Options.builder().userDataDir(remoteProfileData).build();
                Launcher launcher = new Launcher(options);

                SessionFactory factory = launcher.launch();

                try (SessionFactory sf = factory) {
                    try (Session session = sf.create()) {
                        session.navigate("https://google.com");
                        session.waitDocumentReady();
                        System.err.println(
                                "Content Length: " + session.getContent().length());
                    }
                }
            }
        }.start();
    }

    protected static int getFreePort(int portNumber) {
        try (ServerSocket socket = new ServerSocket(portNumber)) {
            int freePort = socket.getLocalPort();
            return freePort;
        } catch (IOException e) {
            return getFreePort(portNumber + 1);
        }
    }
}
