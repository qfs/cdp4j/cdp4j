// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.JsonLibrary;
import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class JacksonExample {

    public static void main(String[] args) {
        Options options = Options.builder().jsonLibrary(JsonLibrary.Jackson).build();

        Launcher launcher = new Launcher(options);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://google.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            launcher.kill();
        }
    }
}
