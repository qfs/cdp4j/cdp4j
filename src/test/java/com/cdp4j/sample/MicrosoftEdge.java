// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import static com.cdp4j.Browser.MicrosoftEdge;
import static com.cdp4j.Constant.WINDOWS;
import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.logger.CdpLogggerLevel.Info;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;

public class MicrosoftEdge {

    public static void main(String[] args) throws IOException {
        if (!WINDOWS) {
            return;
        }
        Options options = Options.builder()
                .browser(MicrosoftEdge)
                .createNewUserDataDir(true)
                .loggerType(Console)
                .consoleLoggerLevel(Info)
                .build();

        Launcher launcher = new Launcher(options);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://google.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            launcher.kill();
        }
    }
}
