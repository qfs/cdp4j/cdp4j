// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import static com.cdp4j.JsonLibrary.Jackson;
import static com.cdp4j.chromium.ChromiumChannel.stable;
import static com.cdp4j.logger.CdpLoggerType.Console;
import static java.util.EnumSet.of;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.chromium.Chromium;
import com.cdp4j.chromium.ChromiumOptions;
import com.cdp4j.chromium.ChromiumVersion;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.nio.file.Path;

public class ChromiumDownloader {

    public static void main(String[] args) {
        ChromiumOptions options = ChromiumOptions.builder()
                .jsonLibrary(Jackson)
                .loggerType(Console)
                .build();

        Chromium chromium = new Chromium(options);
        final long daysAgo = 30;
        // download the chromium stable version if it's released before 10 days ago
        ChromiumVersion version = chromium.download(stable, daysAgo);
        // delete the stable version if it's released before 15 days ago
        chromium.deleteOldVersions(of(stable), daysAgo + 5);
        Path executablePath = chromium.getExecutablePath(version);

        Launcher launcher = new Launcher(Options.builder()
                .browserExecutablePath(executablePath.toString())
                .build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://yahoo.com");
            session.waitDocumentReady();
        } finally {
            launcher.kill();
        }
    }
}
