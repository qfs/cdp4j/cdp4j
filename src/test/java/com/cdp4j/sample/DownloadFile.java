// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.Launcher;
import com.cdp4j.command.Browser;
import com.cdp4j.command.Page;
import com.cdp4j.event.Events;
import com.cdp4j.event.page.DownloadProgress;
import com.cdp4j.event.page.DownloadWillBegin;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.constant.DownloadBehavior;
import com.cdp4j.type.constant.DownloadState;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CountDownLatch;

public class DownloadFile {

    public static void main(String[] args) throws IOException, InterruptedException {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html");
            session.waitDocumentReady();

            Page page = session.getCommand().getPage();
            // this is required to capture download events
            page.enable();

            Browser browser = session.getCommand().getBrowser();

            Path tempDir = Files.createTempDirectory("download").toAbsolutePath();
            System.out.println(tempDir);

            browser.setDownloadBehavior(DownloadBehavior.Allow, null, tempDir.toString(), Boolean.TRUE);

            // link must be visible before downloading the file
            session.evaluate("document.querySelector(\"code\").scrollIntoView()");
            // click the download link
            session.click("code");
            CountDownLatch latch = new CountDownLatch(1);
            session.addEventListener((event, value) -> {
                // important!
                //
                // if chrome version is equal less than 90, chrome triggers PageDownloadProgress
                // otherwise chrome triggers BrowserDownloadProgress (chrome version >= 92)
                // based on your chrome version, you can simplify this logic
                //
                if (Events.PageDownloadProgress == event) {
                    DownloadProgress dp1 = (DownloadProgress) value;
                    System.out.println(
                            "download state: " + dp1.getState() + ", rceivedBytes: " + dp1.getReceivedBytes());
                    if (DownloadState.Completed == dp1.getState()) {
                        latch.countDown();
                    }
                } else if (Events.BrowserDownloadProgress == event) {
                    com.cdp4j.event.browser.DownloadProgress dp2 = (com.cdp4j.event.browser.DownloadProgress) value;
                    System.out.println(
                            "download state: " + dp2.getState() + ", rceivedBytes: " + dp2.getReceivedBytes());
                    if (DownloadState.Completed == dp2.getState()) {
                        latch.countDown();
                    }
                } else if (Events.PageDownloadWillBegin == event) {
                    DownloadWillBegin begin1 = (DownloadWillBegin) value;
                    System.out.println(
                            "download started: " + begin1.getSuggestedFilename() + ", url: " + begin1.getUrl());
                } else if (Events.BrowserDownloadWillBegin == event) {
                    com.cdp4j.event.browser.DownloadWillBegin begin2 =
                            (com.cdp4j.event.browser.DownloadWillBegin) value;
                    System.out.println(
                            "download started: " + begin2.getSuggestedFilename() + ", url: " + begin2.getUrl());
                }
            });
            latch.await();
        } finally {
            launcher.kill();
        }
    }
}
