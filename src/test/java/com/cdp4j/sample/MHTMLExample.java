// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import static com.cdp4j.type.constant.SnapshotType.Mhtml;
import static java.nio.charset.StandardCharsets.UTF_8;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class MHTMLExample {

    public static void main(String[] args) throws IOException {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://wikipedia.org");
            session.waitDocumentReady();
            String page = session.getCommand().getPage().captureSnapshot(Mhtml);
            Path tempFile = Files.createTempFile("wikipedia", ".mhtml");
            Files.write(tempFile, page.getBytes(UTF_8));

            Session dummySession = factory.create();
            dummySession.navigate("file:///" + tempFile.toString());
            dummySession.wait(5_000);
            dummySession.close();
        } finally {
            launcher.kill();
        }
    }
}
