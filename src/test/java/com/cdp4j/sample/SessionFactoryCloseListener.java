// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.Launcher;
import com.cdp4j.session.CloseListener;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class SessionFactoryCloseListener {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            // this event listener triggered when
            // SessionFactory is closed or Launcher.kill() method is called
            factory.addCloseListener(new CloseListener() {

                @Override
                public void closed() {
                    System.out.println("SessionFactory closed");
                }
            });

            // this event listener triggered when
            // session destroyed, detached or crashed
            session.addCloseListener(new CloseListener() {

                @Override
                public void closed() {
                    System.out.println("session closed");
                }
            });

            session.navigate("https://bing.com");
            session.waitDocumentReady();
        } finally {
            launcher.kill();
        }
    }
}
