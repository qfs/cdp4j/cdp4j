// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.Launcher;
import com.cdp4j.dom.Attributes;
import com.cdp4j.event.Events;
import com.cdp4j.event.page.JavascriptDialogOpening;
import com.cdp4j.listener.EventListener;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.page.DialogType;
import java.net.URL;

public class Dialog {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        URL url = Attributes.class.getResource("/alert.html");

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate(url.toString());
            session.waitDocumentReady();

            session.addEventListener(new EventListener() {

                @Override
                public void onEvent(Events event, Object value) {
                    if (Events.PageJavascriptDialogOpening.equals(event)) {
                        JavascriptDialogOpening jdo = (JavascriptDialogOpening) value;
                        if (DialogType.Beforeunload.equals(jdo.getType())) {
                            session.getCommand().getPage().handleJavaScriptDialog(true);
                        }
                    }
                }
            });

            session.click("a");
            session.waitDocumentReady();
            // This must print google.com
            System.out.println(session.getLocation());
        } finally {
            launcher.kill();
        }
    }
}
