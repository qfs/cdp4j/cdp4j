// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.logger.CdpLoggerType;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.session.WaitUntil;

public class SharedSession {

    public static void main(String[] args) {
        Options options = Options.builder().loggerType(CdpLoggerType.Slf4j).build();

        Launcher launcher = new Launcher(options);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            try (Session firstSession = factory.create()) {
                firstSession.navigateAndWait("https://httpbin.org/cookies/set?SESSION_ID=1", WaitUntil.NetworkIdle);
                String session1 = (String) firstSession.evaluate("window.document.body.textContent");
                System.out.println(session1);
            }

            try (Session secondSession = factory.create()) {
                secondSession.navigateAndWait("https://httpbin.org/cookies", WaitUntil.NetworkIdle);
                String session2 = (String) secondSession.evaluate("window.document.body.textContent");
                System.out.println(session2);
            }
        } finally {
            launcher.kill();
        }
    }
}
