// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.logger.CdpLogggerLevel.Info;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class Logging {

    public static void main(String[] args) {
        Launcher launcher = new Launcher(
                Options.builder().consoleLoggerLevel(Info).loggerType(Console).build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("about:blank");
            session.waitDocumentReady();

            // logs javascript messages
            session.enableConsoleLog();

            session.evaluate("console.info('info message')");
            session.evaluate("console.error('error message')");
            session.evaluate("console.warn('warning message')");

            // logs newtwork, violation, security, storage and deprecation messages
            session.enableDetailLog();

            session.evaluate("fetch('https://google.com')");
            session.wait(2000);
        } finally {
            launcher.kill();
        }
    }
}
