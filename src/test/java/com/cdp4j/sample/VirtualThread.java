// SPDX-License-Identifier: MIT
package com.cdp4j.sample;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.channel.JreWebSocketFactory;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VirtualThread {

    public static void main(String[] args) throws Exception {
        // Use Executors.newVirtualThreadPerTaskExecutor() directly if JDK 19+ is available
        Method method = Executors.class.getDeclaredMethod("newVirtualThreadPerTaskExecutor");
        ExecutorService executor = (ExecutorService) method.invoke(null);

        JreWebSocketFactory webSocketFactory = new JreWebSocketFactory(executor);

        Launcher launcher = new Launcher(
                Options.builder()
                        .headless(true)
                        .eventHandlerThreadPool(executor)
                        .workerThreadPool(executor)
                        .shutdownThreadPoolOnClose(false)
                        .build(),
                webSocketFactory);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://google.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            launcher.kill();
        }

        executor.shutdown();
    }
}
