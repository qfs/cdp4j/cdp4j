// SPDX-License-Identifier: MIT
package com.cdp4j.js;

import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.logger.CdpLogggerLevel.Info;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.command.Page;
import com.cdp4j.dom.Select;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.net.URL;

public class EvaluateOnNewDocument {

    public static void main(String[] args) {
        Launcher launcher = new Launcher(
                Options.builder().consoleLoggerLevel(Info).loggerType(Console).build());

        URL url = Select.class.getResource("/inject-script.html");

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            Page page = session.getCommand().getPage();

            // addScriptToEvaluateOnNewDocument() must be called before Session.navigate()
            page.addScriptToEvaluateOnNewDocument("window.dummyMessage = 'hello, world!'");

            session.enableConsoleLog();

            session.navigate(url.toString());

            session.wait(1000);
        } finally {
            launcher.kill();
        }
    }
}
