// SPDX-License-Identifier: MIT
package com.cdp4j.js;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.logger.CdpLoggerType;
import com.cdp4j.logger.CdpLogggerLevel;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class ExecuteJavascript {

    public static void main(String[] args) {
        Launcher launcher = new Launcher(Options.builder()
                .loggerType(CdpLoggerType.Console)
                .consoleLoggerLevel(CdpLogggerLevel.Info)
                .headless(true)
                .build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            session.navigate("about:blank");
            session.waitDocumentReady();
            Number result = (Number) session.evaluate("var foo = function() { return 2 + 2; }; foo();");
            System.out.println(result);

            session.evaluate("var bar = {}; bar.myFunc = function(s1, s2) { return s1 + ' ' + s2; }");
            String message = session.callFunction("bar.myFunc", String.class, "hello", "world");
            System.out.println(message);

            Integer intResult = session.callFunction("foo", Integer.class);
            System.out.println(intResult);
        } finally {
            launcher.kill();
        }
    }
}
