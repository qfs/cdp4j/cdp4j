// SPDX-License-Identifier: MIT
package com.cdp4j.js;

import static java.lang.String.format;

import com.cdp4j.Launcher;
import com.cdp4j.command.Profiler;
import com.cdp4j.session.Command;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.profiler.CoverageRange;
import com.cdp4j.type.profiler.FunctionCoverage;
import com.cdp4j.type.profiler.ScriptCoverage;
import java.net.URL;
import java.util.List;

public class CodeCoverage {

    public static void main(String[] args) {

        URL url = CodeCoverage.class.getResource("/code-coverage.html");

        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            Command command = session.getCommand();
            Profiler profiler = command.getProfiler();

            session.navigate(url.toString());
            session.waitDocumentReady();

            profiler.enable();

            profiler.start();
            session.callFunction("fibonacci", Double.class, 20);
            profiler.stop();

            List<ScriptCoverage> list = profiler.getBestEffortCoverage();

            for (ScriptCoverage coverage : list) {

                if (!coverage.getUrl().endsWith("test-lib.js")) {
                    continue;
                }

                String libName = coverage.getUrl()
                        .substring(
                                coverage.getUrl().lastIndexOf("/") + 1,
                                coverage.getUrl().length());

                List<FunctionCoverage> functions = coverage.getFunctions();
                System.out.println("");
                System.out.println("======================================================================");
                System.out.println(String.format(" %-40s %s %s", "[Function]", "[Start Offset]", "[End Offset]"));
                System.out.println("======================================================================");
                for (FunctionCoverage functionCoverage : functions) {
                    if (functionCoverage.getFunctionName().isEmpty()) {
                        continue;
                    }
                    for (CoverageRange range : functionCoverage.getRanges()) {
                        System.out.println(format(
                                " %-46s %-11d %d",
                                libName + "/" + functionCoverage.getFunctionName(),
                                range.getStartOffset(),
                                range.getEndOffset()));
                    }
                }
                System.out.println("");
            }

            profiler.disable();
        } finally {
            launcher.kill();
        }
    }
}
