// SPDX-License-Identifier: MIT
package com.cdp4j.js;

import static com.cdp4j.JsonLibrary.Jackson;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.exception.JsPromiseException;
import com.cdp4j.logger.CdpLoggerType;
import com.cdp4j.logger.CdpLogggerLevel;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.net.URL;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class InvokeJsPromise {

    private static class MyObject {
        public String foo;
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Launcher launcher = new Launcher(Options.builder()
                .jsonLibrary(Jackson)
                .loggerType(CdpLoggerType.Console)
                .consoleLoggerLevel(CdpLogggerLevel.Info)
                .headless(true)
                .build());

        URL url = InvokeJsPromise.class.getResource("/js-promise.html");

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.enableDetailLog();
            session.navigate(url.toString());
            session.waitDocumentReady();

            CompletableFuture<String> future1 = session.callPromise("promise1", String.class);
            if (!"done".equals(future1.get())) {
                throw new IllegalStateException("panic: this should be never happen!");
            }
            System.out.println("promise1 - ok");

            CompletableFuture<String> future2 = session.callPromise("promise2", String.class);
            try {
                future2.get();
            } catch (ExecutionException e) {
                JsPromiseException jsPromiseException = (JsPromiseException) e.getCause();
                if (!"ERROR!".equals(jsPromiseException.getMessage())) {
                    throw new IllegalStateException("panic: this should be never happen!");
                }
            }
            System.out.println("promise2 - ok");

            CompletableFuture<MyObject> future3 = session.callPromise("promise3", MyObject.class, 2000);
            MyObject myObject = future3.get();
            if (!"bar".equals(myObject.foo)) {
                throw new IllegalStateException("panic: this should be never happen!");
            }
            System.out.println("promise3 - ok");

        } finally {
            launcher.kill();
        }
    }
}
