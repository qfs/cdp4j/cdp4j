// SPDX-License-Identifier: MIT
package com.cdp4j.selector;

import static com.cdp4j.SelectorEngine.Playwright;
import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.logger.CdpLogggerLevel.Info;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.channel.NvWebSocketFactory;
import com.cdp4j.dom.Attributes;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.session.SessionSettings;
import java.net.URL;

public class PlaywrightSelector {

    public static void main(String[] args) throws InterruptedException {
        Launcher launcher = new Launcher(
                Options.builder()
                        .loggerType(Console)
                        .consoleLoggerLevel(Info)
                        .selectorEngine(Playwright) // <- set the selector engine globally
                        .build(),
                new NvWebSocketFactory());

        URL url = Attributes.class.getResource("/playwright.html");

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create(SessionSettings.builder()
                        .selectorEngine(Playwright) // <- set the selector engine per session
                        .build())) {
            session.enableConsoleLog();

            session.navigate(url.toString());
            session.waitDocumentReady();
            session.setValue("[placeholder='click here to search']", "my search criteria");
            session.wait(1_000);
            // Text selector locates elements that contain passed text.
            session.click("text=Search...");
            session.wait(1_000);
            // Selecting elements based on layout
            session.setValue("input:right-of(:text('Username'))", "my username");
            session.wait(1_000);

            // Playwright selector documentation: https://playwright.dev/docs/selectors
        } finally {
            launcher.kill();
        }
    }
}
