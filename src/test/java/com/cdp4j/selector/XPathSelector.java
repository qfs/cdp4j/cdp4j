// SPDX-License-Identifier: MIT
package com.cdp4j.selector;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class XPathSelector {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://google.com").waitDocumentReady();

            String title = session.getText("//title");

            System.out.println(title);
        } finally {
            launcher.kill();
        }
    }
}
