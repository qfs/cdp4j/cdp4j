// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static com.cdp4j.channel.ChannelFactory.MAX_PAYLOAD_SIZE;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import io.netty.channel.EventLoopGroup;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;

public class VertxWebSocketConnection {

    public static void main(String[] args) {

        final boolean useNetty = true;

        Vertx vertx = Vertx.vertx();

        ChannelFactory channelFactory;

        HttpClient httpClient = null;
        if (!useNetty) {
            HttpClientOptions options = new HttpClientOptions()
                    .setMaxWebSocketMessageSize(MAX_PAYLOAD_SIZE)
                    .setMaxWebSocketFrameSize(MAX_PAYLOAD_SIZE);
            httpClient = vertx.createHttpClient(options);
            channelFactory = new VertxWebSocketFactory(httpClient);
        } else {
            EventLoopGroup eventLoopGroup = vertx.nettyEventLoopGroup();
            channelFactory = new NettyWebSocketChannelFactory(eventLoopGroup);
        }

        Launcher launcher = new Launcher(channelFactory);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("https://example.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            if (httpClient != null) {
                httpClient.close();
            }
            vertx.close();
            launcher.kill();
        }
    }
}
