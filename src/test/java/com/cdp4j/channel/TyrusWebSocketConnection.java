// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static org.glassfish.tyrus.client.ClientProperties.MASKING_KEY_GENERATOR;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.core.MaskingKeyGenerator;

public class TyrusWebSocketConnection {

    public static void main(String[] args) {
        ClientManager client = ClientManager.createClient();

        client.getProperties().put(MASKING_KEY_GENERATOR, (MaskingKeyGenerator) () -> 0);

        // Optionally use JakartaWebSocketFactory for jakarta compatible api
        StandardWebSocketFactory channelFactory = new StandardWebSocketFactory(client);

        Launcher launcher = new Launcher(channelFactory);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            factory.addCloseListener(() -> client.shutdown());
            session.navigate("https://example.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            launcher.kill();
        }
    }
}
