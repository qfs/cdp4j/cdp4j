// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class TooTallNateWebSocketConnection {

    public static void main(String[] args) {
        TooTallNateWebSocketFactory channelFactory = new TooTallNateWebSocketFactory();

        Launcher launcher = new Launcher(channelFactory);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.addCloseListener(() -> channelFactory.close());
            session.navigate("https://example.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            launcher.kill();
        }
    }
}
