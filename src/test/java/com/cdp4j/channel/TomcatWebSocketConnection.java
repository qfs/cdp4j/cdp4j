// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import org.apache.tomcat.websocket.WsWebSocketContainer;

public class TomcatWebSocketConnection {

    public static void main(String[] args) {
        WsWebSocketContainer container = new WsWebSocketContainer();

        // Use StandardWebSocketFactory (javax) if tomcat version is <= 9
        JakartaWebSocketFactory channelFactory = new JakartaWebSocketFactory(container);

        Launcher launcher = new Launcher(channelFactory);

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.addCloseListener(() -> container.destroy());
            session.navigate("https://example.com");
            session.waitDocumentReady();
            String content = session.getContent();
            System.out.println(content);
        } finally {
            launcher.kill();
        }
    }
}
