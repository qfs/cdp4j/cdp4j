// SPDX-License-Identifier: MIT
package com.cdp4j.connection;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.channel.ChannelFactory;
import com.cdp4j.channel.DevToolsProfileConnection;
import com.cdp4j.channel.JreWebSocketFactory;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.nio.file.Path;

// This example is similar to DevToolsConnectionExample except that we don't need to know remote Debugging Port
// directly.
// If you don't specify the remote Debugging Port, Chrome will use random free port.
// Chrome writes the free port and websocket debugger to DevToolsActivePort file in the user profile directory.
// If you know the user profile directory, you could re-connect to existing DevTools server.
public class DevToolsProfileConnectionExample {

    public static void main(String[] args) throws InterruptedException {
        Launcher launcher = new Launcher();

        SessionFactory dummySessionFactory = launcher.launch();

        // --------------------------------------------------------------------
        // connect an existing devtools server
        // --------------------------------------------------------------------

        Path userProfileDirectory = launcher.getUserDataDir();
        DevToolsProfileConnection connection = new DevToolsProfileConnection(userProfileDirectory);

        // check the connection if it's valid (optional)
        if (!connection.isValid()) {
            throw new IllegalStateException();
        }

        Options options = Options.builder().build();

        ChannelFactory webSocketFactory = new JreWebSocketFactory();

        SessionFactory factory = new SessionFactory(options, webSocketFactory, connection);

        Session session = factory.create();
        session.navigate("https://example.com");

        // --------------------------------------------------------------------

        Thread.sleep(5_000);

        dummySessionFactory.close();

        factory.close();

        launcher.kill();
    }
}
