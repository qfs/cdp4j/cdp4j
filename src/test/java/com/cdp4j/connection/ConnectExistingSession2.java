// SPDX-License-Identifier: MIT
package com.cdp4j.connection;

import static com.cdp4j.logger.CdpLoggerType.Log4j;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.target.TargetInfo;
import java.net.URL;
import java.util.List;

public class ConnectExistingSession2 {

    public static void main(String[] args) {
        Options options = Options.builder().loggerType(Log4j).build();

        Launcher launcher = new Launcher(options);

        URL url = ConnectExistingSession2.class.getResource("/connect-existing-session.html");

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate(url.toString());
            session.waitDocumentReady();
            session.click("a");

            // prints "1", getSessions() returns only connected sessions
            System.out.println(factory.getSessions().size());

            // list all pages & windows
            List<TargetInfo> targets = session.getCommand().getTarget().getTargets();
            // this will print "2"
            System.out.println(targets.size());

            TargetInfo target = targets.stream()
                    .filter(p -> p.getUrl().contains("yahoo"))
                    .findFirst()
                    .get();

            Session yahooSession = factory.connect(target.getTargetId());
            yahooSession.waitDocumentReady();

            // prints "2"
            System.out.println(factory.getSessions().size());

            yahooSession.close();

            // prints "1"
            System.out.println(factory.getSessions().size());

            session.wait(1000);
        } finally {
            launcher.kill();
        }
    }
}
