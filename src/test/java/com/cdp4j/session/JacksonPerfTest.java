// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.session.JacksonMessageHandler.InputType.ByteArray;
import static com.cdp4j.session.JacksonMessageHandler.InputType.InputStream;
import static com.cdp4j.session.JacksonMessageHandler.InputType.String;
import static java.nio.charset.StandardCharsets.UTF_8;

import com.cdp4j.serialization.JacksonMapper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

@TestClassOrder(value = ClassOrderer.OrderAnnotation.class)
public class JacksonPerfTest {

    private static List<String> lines;

    private static List<byte[]> linesByteArray = new ArrayList<>();

    private static List<InputStream> linesInputStream = new ArrayList<>();

    private static JacksonMessageHandler handler;

    @BeforeAll
    public static void init() throws IOException {
        lines = Files.readAllLines(Paths.get("src/test/resources/example-dump.json"));
        for (String next : lines) {
            linesByteArray.add(next.getBytes(UTF_8));
            linesInputStream.add(new ByteArrayInputStream(next.getBytes(UTF_8)));
        }
        JacksonMapper mapper = new JacksonMapper();
        handler = new JacksonMessageHandler(mapper, null, null, null, null);
    }

    @Test
    @Order(1)
    public void testFilterSessionIdString() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_000; i++) {
            for (String next : lines) {
                handler.filterSessionId(next);
            }
        }
        System.out.println("filterSessionIdString / " + (System.currentTimeMillis() - start) + " ms");
    }

    @Test
    @Order(2)
    public void testFilterSessionIdByteArray() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_000; i++) {
            for (byte[] next : linesByteArray) {
                handler.filterSessionId(next);
            }
        }
        System.out.println("filterSessionIdByteArray / " + (System.currentTimeMillis() - start) + " ms");
    }

    @Test
    @Order(3)
    public void testFilterSessionIdStringJackson() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_000; i++) {
            for (String next : lines) {
                handler.parseSessionId(next, String);
            }
        }
        System.out.println("filterSessionIdJackson / String / " + (System.currentTimeMillis() - start) + " ms");
    }

    @Test
    @Order(4)
    public void testFilterSessionIdByteArrayJackson() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_000; i++) {
            for (byte[] next : linesByteArray) {
                handler.parseSessionId(next, ByteArray);
            }
        }
        System.out.println("filterSessionIdJackson / byte[] / " + (System.currentTimeMillis() - start) + " ms");
    }

    @Test
    @Order(5)
    public void testFilterSessionIdInputStreamJackson() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10_000; i++) {
            for (InputStream next : linesInputStream) {
                handler.parseSessionId(next, InputStream);
            }
        }
        System.out.println("filterSessionIdJackson / InputStream / " + (System.currentTimeMillis() - start) + " ms");
    }
}
