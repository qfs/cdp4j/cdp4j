// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static com.cdp4j.session.WaitUntil.DomContentLoad;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.cdp4j.command.DOMStorage;
import com.cdp4j.junit.CdpExtension;
import com.cdp4j.session.Session;
import com.cdp4j.type.domstorage.StorageId;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("DOMStorage")
public class DomStorageTest {

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    @Test
    @DisplayName("test getInnerHtml")
    public void testInnerHTML(Session session) {
        session.navigateAndWait("https://example.com", DomContentLoad);
        DOMStorage domStorage = session.getCommand().getDOMStorage();
        domStorage.enable();

        String securityOrigin =
                session.getCommand().getPage().getFrameTree().getFrame().getSecurityOrigin();

        StorageId storageId = new StorageId();
        storageId.setIsLocalStorage(Boolean.TRUE);
        storageId.setSecurityOrigin(securityOrigin);

        domStorage.setDOMStorageItem(storageId, "key1", "foo");
        domStorage.setDOMStorageItem(storageId, "key2", "bar");

        List<List<String>> items = domStorage.getDOMStorageItems(storageId);
        System.out.println("Items: " + items);

        List<String> item2 = items.get(0);
        assertEquals("key1", item2.get(0));
        assertEquals("foo", item2.get(1));

        List<String> item1 = items.get(1);
        assertEquals("key2", item1.get(0));
        assertEquals("bar", item1.get(1));

        domStorage.removeDOMStorageItem(storageId, "key2");

        items = domStorage.getDOMStorageItems(storageId);

        assertEquals(1, items.size());
        assertEquals("key1", items.get(0).get(0));
        assertEquals("foo", items.get(0).get(1));
    }
}
