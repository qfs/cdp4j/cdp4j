// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.cdp4j.junit.CdpExtension;
import com.cdp4j.session.Session;
import com.cdp4j.session.VideoRecorder;
import com.cdp4j.session.VideoRecorderOptions;
import com.cdp4j.session.VideoRecorderResult;
import java.nio.file.Files;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("VideoRecorderTest")
public class VideoRecorderTest {

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    @Test
    @DisplayName("test VideoRecorder")
    @Disabled
    public void test(Session session) throws Exception {
        VideoRecorder recorder =
                session.createVideoRecoder(VideoRecorderOptions.builder().build());
        recorder.start();
        session.navigate("https://webkit.org/blog-files/3d-transforms/poster-circle.html")
                .waitDocumentReady()
                .wait(2_000);
        recorder.stop();
        VideoRecorderResult result = recorder.encode();
        assertNotNull(result);
        assertTrue(result.isSucceess());
        assertNotNull(result.getVideoFile());
        assertTrue(Files.exists(result.getVideoFile()));
        System.out.println(result.getVideoFile());
        assertTrue(Files.size(result.getVideoFile()) > 0L);
    }
}
