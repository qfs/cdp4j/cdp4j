// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static com.cdp4j.JsonLibrary.Gson;
import static com.cdp4j.JsonLibrary.Jackson;

import com.cdp4j.JsonLibrary;
import com.cdp4j.junit.CdpExtension;

public class ExtensionFactory {

    public static CdpExtension create() {
        return create(false);
    }

    public static CdpExtension create(boolean useJackson) {
        return new CdpExtension() {

            @Override
            protected JsonLibrary jsonLibrary() {
                return useJackson ? Jackson : Gson;
            }

            @Override
            protected boolean headless() {
                return true;
            }

            @Override
            protected boolean createNewUserDataDir() {
                return true;
            }

            @Override
            protected String browserExecutablePath() {
                String chrome = System.getenv("CHROME_EXECUTABLE");
                if (chrome != null && !chrome.trim().isEmpty()) {
                    return chrome.trim();
                } else {
                    return null;
                }
            }
        };
    }
}
