// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static org.junit.jupiter.api.Assertions.*;

import com.cdp4j.command.DOMSnapshot;
import com.cdp4j.exception.CdpReadTimeoutException;
import com.cdp4j.junit.CdpExtension;
import com.cdp4j.session.Session;
import com.cdp4j.session.WaitUntil;
import java.net.URISyntaxException;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("AdaptTimeout")
public class AdaptTimeoutTest {

    private static String URI;

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    @BeforeAll
    public static void init() {
        try {
            URI = AdaptTimeoutTest.class.getResource("/dom-test.html").toURI().toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @DisplayName("test with default timeout")
    public void testDefaultTimeout(Session session) {
        navigate(session);
        final DOMSnapshot snapshot = session.getCommand().getDOMSnapshot();
        snapshot.enable();
        snapshot.captureSnapshot(new ArrayList<String>());
    }

    @Test
    @DisplayName("test with changed timeout")
    public void testChangedTimeout(Session session) {
        navigate(session);
        final DOMSnapshot snapshot = session.getCommand().getDOMSnapshot();
        snapshot.enable();
        final DOMSnapshot snapshotWithTimeout = snapshot.withReadTimeout(0);
        assertNotNull(snapshotWithTimeout);
        assertThrows(CdpReadTimeoutException.class, () -> snapshotWithTimeout.captureSnapshot(new ArrayList<String>()));
    }

    @Test
    @DisplayName("test with changed timeout factor")
    public void testChangedTimeoutFactor(Session session) {
        navigate(session);
        final DOMSnapshot snapshot = session.getCommand().getDOMSnapshot();
        snapshot.enable();
        final DOMSnapshot snapshotWithTimeout = snapshot.withReadTimeoutFactor(0.0000001);
        assertNotNull(snapshotWithTimeout);
        assertThrows(CdpReadTimeoutException.class, () -> snapshotWithTimeout.captureSnapshot(new ArrayList<String>()));
    }

    @Test
    @DisplayName("test with changed default timeout")
    public void testChangedDefaultTimeout(Session session) {
        navigate(session);
        int timeout = session.getReadTimeout();
        try {
            final DOMSnapshot snapshot = session.getCommand().getDOMSnapshot();
            snapshot.enable();
            session.setReadTimeout(0);
            assertThrows(CdpReadTimeoutException.class, () -> snapshot.captureSnapshot(new ArrayList<String>()));
        } finally {
            session.setReadTimeout(timeout);
        }
    }

    private void navigate(Session session) {
        session.navigateAndWait(URI, WaitUntil.DomContentLoad);
    }
}
