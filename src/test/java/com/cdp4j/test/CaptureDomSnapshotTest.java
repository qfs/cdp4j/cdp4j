// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import com.cdp4j.JsonLibrary;
import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("CaptureDomSnapshotTest")
public class CaptureDomSnapshotTest {

    private static final int REPEAT_COUNT = 10;

    private static final int WARMUP_COUNT = 10;

    private static final String TEST_URL = "https://news.ycombinator.com";

    @Test
    @DisplayName("test")
    @Disabled
    public void test() throws Exception {

        Launcher launcher = new Launcher(Options.builder().headless(true).build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate(TEST_URL);
            session.wait(5_000);
            session.stop();
            session.getCommand().getRuntime().terminateExecution();
            session.getCommand().getDebugger().enable();
            session.getCommand().getDebugger().pause();

            // warmup
            for (int j = 0; j < WARMUP_COUNT; j++) {
                session.getDocumentSnapshot();
                session.getDOMSnapshot();
            }

            long sum = 0;
            for (int i = 0; i < REPEAT_COUNT; i++) {
                long start = System.currentTimeMillis();
                session.getDocumentSnapshot();
                long end = System.currentTimeMillis();
                sum += end - start;
            }

            System.out.println("getDocumentSnapshot() (Gson): " + sum);

            sum = 0;
            for (int i = 0; i < REPEAT_COUNT; i++) {
                long start = System.currentTimeMillis();
                session.getDOMSnapshot();
                long end = System.currentTimeMillis();
                sum += end - start;
            }

            System.out.println("getDOMSnapshot() (Gson): " + sum);
        } finally {
            launcher.kill();
        }

        Launcher launcherJackson = new Launcher(Options.builder()
                .jsonLibrary(JsonLibrary.Jackson)
                .headless(true)
                .build());

        try (SessionFactory factory = launcherJackson.launch();
                Session session = factory.create()) {
            session.navigate(TEST_URL);
            session.wait(5_000);
            session.stop();

            // warmup
            for (int j = 0; j < WARMUP_COUNT; j++) {
                session.getDocumentSnapshot();
                session.getDOMSnapshot();
            }

            long sum = 0;
            for (int i = 0; i < REPEAT_COUNT; i++) {
                long start = System.currentTimeMillis();
                session.getDocumentSnapshot();
                long end = System.currentTimeMillis();
                sum += end - start;
            }

            System.out.println("getDocumentSnapshot() (Jackson): " + sum);

            sum = 0;
            for (int i = 0; i < REPEAT_COUNT; i++) {
                long start = System.currentTimeMillis();
                session.getDOMSnapshot();
                long end = System.currentTimeMillis();
                sum += end - start;
            }

            System.out.println("getDOMSnapshot() (Jackson): " + sum);
        } finally {
            launcher.kill();
        }
    }
}
