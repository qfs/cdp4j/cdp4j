// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.cdp4j.exception.CdpException;
import com.cdp4j.junit.CdpExtension;
import com.cdp4j.session.Session;
import com.cdp4j.session.WaitUntil;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("DOM")
public class DomTest {

    private class MyLatch {

        private final CountDownLatch latch = new CountDownLatch(1);

        public Object data;

        public void sleep() {
            try {
                latch.await(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                throw new CdpException(e);
            }
        }

        public void dispose() {
            latch.countDown();
        }
    }

    private static String URI;

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    @BeforeAll
    public static void init() {
        try {
            URI = DomTest.class.getResource("/dom-test.html").toURI().toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @DisplayName("test getInnerHtml")
    public void testInnerHTML(Session session) {
        navigate(session);
        String innerHtml = session.getInnerHtml("#foo").trim();
        assertEquals("<div>bar</div>", innerHtml);
    }

    @Test
    public void testSessionCloseListener(Session session) {
        AtomicBoolean closed = new AtomicBoolean(false);
        MyLatch latch = new MyLatch();
        session.addCloseListener(() -> {
            closed.set(true);
            latch.dispose();
        });
        session.close();
        latch.sleep();
        assertTrue(closed.get());
    }

    @Test
    public void testAsyncEventListener(Session session) {
        MyLatch latch = new MyLatch();
        session.addEventListener((event, value) -> {
            latch.data = event;
            latch.dispose();
        });
        navigate(session);
        latch.sleep();
        assertNotNull(latch.data);
    }

    @Test
    public void testSyncEventListener(Session session) {
        MyLatch latch = new MyLatch();
        session.addSyncEventListener((event, value) -> {
            latch.data = event;
            latch.dispose();
        });
        navigate(session);
        latch.sleep();
        assertNotNull(latch.data);
    }

    @Test
    public void testBackButton(Session session) {
        navigate(session);
        session.navigate("about:blank");
        session.wait(10);
        session.back();
        session.wait(100);
        assertTrue(session.getLocation().endsWith("/dom-test.html"));
    }

    @Test
    public void testCallFunction(Session session) {
        navigate(session);
        String result = session.callFunction("dummyFunc", String.class);
        assertEquals("foo", result);
    }

    @Test
    public void testCaptureScreenshot(Session session) {
        navigate(session);
        byte[] screenshot = session.captureScreenshot();
        assertTrue(screenshot.length > 1024);
    }

    // TODO: test real world scenario
    @Test
    public void testCleaCache(Session session) {
        assertTrue(session.clearCache());
    }

    // TODO: test real world scenario
    @Test
    public void testClearCookies(Session session) {
        assertTrue(session.clearCookies());
    }

    private void navigate(Session session) {
        session.navigateAndWait(URI, WaitUntil.DomContentLoad);
    }
}
