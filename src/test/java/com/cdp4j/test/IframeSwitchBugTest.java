// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.cdp4j.junit.CdpExtension;
import com.cdp4j.junit.MultiFrame;
import com.cdp4j.session.Session;
import com.cdp4j.session.WaitUntil;
import java.net.URISyntaxException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("IframeSwitchBug")
public class IframeSwitchBugTest {

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    private static String URI;

    @BeforeAll
    public static void init() {
        try {
            URI = DomTest.class.getResource("/iframe2.html").toURI().toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @DisplayName("test setAttribute and getAttribute within iframe mode without switching to child frame")
    public void testInnerHTML(@MultiFrame Session session) {
        session.navigateAndWait(URI, WaitUntil.DomContentLoad);
        session.setAttribute("#dummy-txt-iframe-2", "test-attribute", "foobar");
        String value = session.getAttribute("#dummy-txt-iframe-2", "test-attribute");
        assertEquals("foobar", value);
    }
}
