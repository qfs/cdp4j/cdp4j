// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.cdp4j.command.DOMSnapshot;
import com.cdp4j.junit.CdpExtension;
import com.cdp4j.session.Session;
import com.cdp4j.type.domsnapshot.CaptureSnapshotResult;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("Snapshot")
public class SnapshotTest {

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    @Test
    @DisplayName("test DOM captureSnapshot")
    public void testCaptureSnapshot(Session session) throws URISyntaxException {
        String url = getClass().getResource("/snapshot.html").toURI().toString();
        session.navigate(url);
        session.wait(100);
        DOMSnapshot snapshot = session.getCommand().getDOMSnapshot();
        snapshot.enable();
        CaptureSnapshotResult result = snapshot.captureSnapshot(new ArrayList<String>());
        assertEquals(1, result.getDocuments().size());
        List<List<Double>> textBoxBounds =
                result.getDocuments().get(0).getTextBoxes().getBounds();
        assertEquals(1, textBoxBounds.size());
        assertEquals(4, textBoxBounds.get(0).size());
    }

    @Test
    @DisplayName("test DOM Layout")
    public void testIncludeDOMRects(Session session) throws URISyntaxException {
        String url = getClass().getResource("/snapshot.html").toURI().toString();
        session.navigate(url);
        session.wait(100);
        DOMSnapshot snapshot = session.getCommand().getDOMSnapshot();
        snapshot.enable();
        CaptureSnapshotResult result = snapshot.captureSnapshot(new ArrayList<String>(), false, true, false, false);
        assertEquals(1, result.getDocuments().size());
        List<List<Double>> textBoxBounds =
                result.getDocuments().get(0).getTextBoxes().getBounds();
        assertEquals(1, textBoxBounds.size());
        assertEquals(4, textBoxBounds.get(0).size());
        assertTrue(result.getDocuments().get(0).getLayout().getOffsetRects().size() > 0);
        assertTrue(result.getDocuments().get(0).getLayout().getClientRects().size() > 0);
    }

    @Test
    @DisplayName("test DOM Snapshot getStrings")
    public void testIncludeStrings(Session session) throws URISyntaxException {
        String uri = getClass().getResource("/snapshot.html").toURI().toString();
        session.navigate(uri);
        session.wait(100);
        DOMSnapshot snapshot = session.getCommand().getDOMSnapshot();
        snapshot.enable();
        CaptureSnapshotResult result = snapshot.captureSnapshot(new ArrayList<String>(), false, true, false, false);
        assertEquals(1, result.getDocuments().size());
        assertTrue(result.getStrings().size() > 0);
        Integer htmlIndex =
                result.getDocuments().get(0).getNodes().getNodeName().get(1);
        assertEquals("HTML", result.getStrings().get(htmlIndex));
    }
}
