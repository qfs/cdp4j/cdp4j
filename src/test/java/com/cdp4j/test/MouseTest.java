// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.cdp4j.junit.CdpExtension;
import com.cdp4j.junit.Incognito;
import com.cdp4j.session.Session;
import com.cdp4j.session.WaitUntil;
import com.cdp4j.type.input.DragData;
import com.cdp4j.type.util.Point;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("Mouse")
public class MouseTest {

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    @Test
    @DisplayName("mouse click")
    public void testMouseClick(@Incognito("private-session-1") Session session) throws Exception {
        String uri = getClass().getResource("/mouse-click.html").toURI().toString();
        session.navigateAndWait(uri, WaitUntil.DomContentLoad);
        session.click("#mybutton");
        Boolean clicked = session.getVariable("clicked", Boolean.class);
        assertTrue(clicked);
    }

    @Test
    @DisplayName("mouse move")
    public void testMouseMove(@Incognito("private-session-1") Session session) throws Exception {
        String uri = getClass().getResource("/mouse-move.html").toURI().toString();
        session.enableConsoleLog();
        session.navigate(uri);
        session.wait(100);
        session.move(20, 20);
        session.move(21, 21);
        session.move(22, 22);
        session.move(23, 23);
        @SuppressWarnings("unchecked")
        List<Double> positionsX = session.getVariable("positionsX", List.class);
        @SuppressWarnings("unchecked")
        List<Double> positionsY = session.getVariable("positionsY", List.class);
        System.out.println(positionsX);
        System.out.println(positionsY);
        assertTrue(positionsX.size() >= 4);
        assertTrue(positionsY.size() >= 4);
    }

    @Test
    @DisplayName("mouse down")
    public void testMouseDown(@Incognito("private-session-1") Session session) throws Exception {
        session.enableDetailLog();
        session.enableConsoleLog();
        String uri = getClass().getResource("/mouse-click.html").toURI().toString();
        session.navigate(uri);
        session.waitDocumentReady();
        Point point = session.getClickablePoint("#mouseDownDiv");
        session.down(point.x, point.y);
        assertEquals("true", session.getAttribute("#mouseDownDiv", "mouse-down-clicked"));
    }

    @Test
    @DisplayName("mouse up")
    public void testMouseUp(@Incognito("private-session-1") Session session) throws Exception {
        String uri = getClass().getResource("/mouse-click.html").toURI().toString();
        session.navigate(uri);
        session.waitDocumentReady();
        Point point = session.getClickablePoint("#mouseUpDiv");
        session.up(point.x, point.y);
        assertEquals("true", session.getAttribute("#mouseUpDiv", "mouse-up-clicked"));
    }

    @Test
    @DisplayName("mouse drag")
    public void testMouseDrag(@Incognito("private-session-1") Session session) throws Exception {
        session.enableConsoleLog();
        session.enableDetailLog();
        String uri = getClass().getResource("/mouse-drag.html").toURI().toString();
        session.navigate(uri);
        session.waitDocumentReady();
        Point start = session.getClickablePoint("#drag");
        Point end = session.getClickablePoint("#drop");
        DragData data = session.drag(start, end);
        assertEquals(true, session.evaluate("window.didDragStart"));
        assertNotNull(data);
        assertNotNull(data.getItems());
        assertEquals(1, data.getItems().size());
        assertEquals("drag", data.getItems().get(0).getData());
    }

    @Test
    @DisplayName("mouse drag and drop")
    public void testMouseDragAndDrop(@Incognito("private-session-1") Session session) throws Exception {
        session.enableConsoleLog();
        session.enableDetailLog();
        String uri = getClass().getResource("/mouse-drag.html").toURI().toString();
        session.navigate(uri);
        session.waitDocumentReady();
        Point dragPoint = session.getClickablePoint("#drag");
        Point dropPoint = session.getClickablePoint("#drop");
        session.dragAndDrop(dragPoint, dropPoint);
        assertEquals(true, session.evaluate("window.didDragStart"));
        assertEquals(true, session.evaluate("window.didDragEnter"));
        assertEquals(true, session.evaluate("window.didDragOver"));
        assertEquals(true, session.evaluate("window.didDrop"));
    }
}
