// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static com.cdp4j.JsonLibrary.Jackson;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("PartitionedCookieTest")
public class PartitionedCookieTest {

    @Test
    public void testGetDataWithPartitionedCookies() throws Exception {
        Options options = Options.builder()
                .jsonLibrary(Jackson)
                .headless(true)
                .createNewUserDataDir(true)
                .build();

        Launcher launcher = new Launcher(options);
        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.getCommand().getNetwork().enable();
            session.navigate("https://chips-site-a.glitch.me/");
            Thread.sleep(500);
        } finally {
            launcher.kill();
        }
    }
}
