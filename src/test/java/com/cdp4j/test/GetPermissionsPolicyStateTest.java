// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static com.cdp4j.JsonLibrary.Gson;
import static com.cdp4j.JsonLibrary.Jackson;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.page.PermissionsPolicyFeatureState;
import java.util.List;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
public class GetPermissionsPolicyStateTest {

    private static long countUnknownFeaturesJackson;

    private static long countUnknownFeaturesGson;

    @Test
    @Order(1)
    public void testGetPermissionsPolicyStateGson() {
        Options options = Options.builder()
                .jsonLibrary(Gson)
                .createNewUserDataDir(true)
                .headless(true)
                .build();

        Launcher launcher = new Launcher(options);
        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("about:blank");
            List<PermissionsPolicyFeatureState> permissionsGson =
                    session.getCommand().getPage().getPermissionsPolicyState(session.getFrameId());
            countUnknownFeaturesGson = permissionsGson.stream()
                    .filter(p -> p.getFeature().value.equals("_UNKNOWN_"))
                    .count();
        } finally {
            launcher.kill();
        }
    }

    @Test
    @Order(2)
    public void testGetPermissionsPolicyStateJackson() {
        /*   Chromium downloader =
                        new Chromium(ChromiumOptions.builder().loggerType(Console).build());
                final long daysAgo = 30;
                // download chromium stable version of it's released before 30 days ago
                ChromiumVersion chromium = downloader.download(dev, daysAgo);
                // delete installed stable versions of it's released before 30 days ago
                downloader.deleteOldVersions(of(dev), daysAgo);
                Path executablePath = downloader.getExecutablePath(chromium);
        */
        Options options = Options.builder()
                //                .browserExecutablePath(executablePath.toString())
                .jsonLibrary(Jackson)
                .headless(true)
                .createNewUserDataDir(true)
                .build();

        Launcher launcher = new Launcher(options);
        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.navigate("about:blank");
            List<PermissionsPolicyFeatureState> permissionsJackson =
                    session.getCommand().getPage().getPermissionsPolicyState(session.getFrameId());
            countUnknownFeaturesJackson = permissionsJackson.stream()
                    .filter(p -> p.getFeature().value.equals("_UNKNOWN_"))
                    .count();
        } finally {
            launcher.kill();
        }
    }

    @Test
    @Order(3)
    public void test() {
        assertEquals(countUnknownFeaturesGson, countUnknownFeaturesJackson);
    }
}
