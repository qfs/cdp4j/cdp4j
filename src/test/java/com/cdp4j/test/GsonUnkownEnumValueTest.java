// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static com.cdp4j.type.network.PrivateNetworkRequestPolicy.Allow;
import static com.cdp4j.type.network.PrivateNetworkRequestPolicy._UNKNOWN_;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.slf4j.helpers.MessageFormatter.arrayFormat;

import com.cdp4j.logger.CdpLogger;
import com.cdp4j.serialization.GsonMapper;
import com.cdp4j.type.network.PrivateNetworkRequestPolicy;
import java.io.ByteArrayInputStream;
import org.junit.jupiter.api.Test;
import org.slf4j.helpers.FormattingTuple;

public class GsonUnkownEnumValueTest {

    private static class EnumTestClass {
        private PrivateNetworkRequestPolicy value;

        public PrivateNetworkRequestPolicy getValue() {
            return value;
        }

        @SuppressWarnings("unused")
        public void setValue(PrivateNetworkRequestPolicy value) {
            this.value = value;
        }
    }

    @Test
    public void testUnkownValue() {
        StringBuilder logMessage = new StringBuilder();

        GsonMapper gsonMapper = new GsonMapper(new CdpLogger() {

            @Override
            public void warn(String message, Object... args) {
                FormattingTuple tuple = arrayFormat(message, args);
                logMessage.append(tuple.getMessage());
            }

            @Override
            public boolean isDebugEnabled() {
                return false;
            }

            @Override
            public void info(String message, Object... args) {}

            @Override
            public void error(String message, Throwable t) {}

            @Override
            public void error(String message, Object... args) {}

            @Override
            public void debug(String message, Object... args) {}
        });

        EnumTestClass allow =
                gsonMapper.fromJson(new ByteArrayInputStream("{\"value\":\"Allow\"}".getBytes()), EnumTestClass.class);
        assertEquals(Allow, allow.getValue());

        EnumTestClass vNull1 =
                gsonMapper.fromJson(new ByteArrayInputStream("{\"value\": null}".getBytes()), EnumTestClass.class);
        assertNull(vNull1.getValue());

        EnumTestClass vNull2 = gsonMapper.fromJson(new ByteArrayInputStream("{}".getBytes()), EnumTestClass.class);
        assertNull(vNull2.getValue());

        EnumTestClass unkown =
                gsonMapper.fromJson(new ByteArrayInputStream("{\"value\":\"FooBar\"}".getBytes()), EnumTestClass.class);
        assertEquals(_UNKNOWN_, unkown.getValue());

        assertEquals(
                "Missing enum constant [PrivateNetworkRequestPolicy.FooBar]. [FooBar] mapped to [_UNKNOWN_].",
                logMessage.toString());
    }
}
