// SPDX-License-Identifier: MIT
package com.cdp4j.test;

import static com.cdp4j.session.WaitUntil.DomContentLoad;
import static java.lang.Double.valueOf;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.cdp4j.JsFunction;
import com.cdp4j.junit.CdpExtension;
import com.cdp4j.session.Session;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

@DisplayName("JavaScript")
public class JsFunctionTest {

    @RegisterExtension
    public static CdpExtension CDP_EXTENSION = ExtensionFactory.create();

    public interface MyJsFunction {

        @JsFunction(
                "let attributes = []; document.querySelectorAll(selector).forEach(e => { attributes.push(e.getAttribute(attributeName)); }); return attributes;")
        List<String> listAttributes(String selector, String attributeName);

        @JsFunction("")
        void dummy();

        @JsFunction("console.error(message);")
        void consoleError(String message);

        @JsFunction("return a + b")
        int sum(int a, int b);

        @JsFunction("return str1 + str2")
        String concat(String str1, String str2);

        @JsFunction("let list = []; numbers.forEach(i => list.push(i + inc)); return list;")
        List<Double> increment(List<Integer> numbers, int inc);
    }

    @Test
    @DisplayName("test registerJsFunction")
    public void test(Session session) throws Exception {
        String uri = getClass().getResource("/js-function-test.html").toURI().toString();
        // Important!
        // Register the JsFunction before the navigate method
        session.registerJsFunction(MyJsFunction.class);
        session.enableConsoleLog();
        session.navigateAndWait(uri, DomContentLoad);
        MyJsFunction utility = session.getJsFunction(MyJsFunction.class);
        List<String> attributes = utility.listAttributes("img", "src");
        assertEquals(2, attributes.size());
        assertEquals("image1.png", attributes.get(0));
        assertEquals("image2.png", attributes.get(1));
        utility.dummy();
        utility.consoleError("panic!");
        assertEquals(4, utility.sum(2, 2));
        assertEquals("foobar", utility.concat("foo", "bar"));
        List<Double> list = utility.increment(asList(0, 1, 2, 3), 1);
        assertEquals(4, list.size());
        assertEquals(valueOf("1"), list.get(0));
        assertEquals(valueOf("2"), list.get(1));
        assertEquals(valueOf("3"), list.get(2));
        assertEquals(valueOf("4"), list.get(3));
        session.wait(500);
    }
}
