// SPDX-License-Identifier: MIT
package com.cdp4j.network;

import com.cdp4j.Launcher;
import com.cdp4j.command.Network;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;

public class NetworkConditionEmulation {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            Network network = session.getCommand().getNetwork();
            // 3G connection
            Boolean offline = false;
            Double latencyMs = 150 * 3.75;
            Double downloadThroughput = ((1.6 * 1000 * 1000) / 8) * 0.9; // bytes seconds
            Double uploadThroughput = ((750 * 1000) / 8) * 0.9; // bytes seconds
            network.emulateNetworkConditions(offline, latencyMs, downloadThroughput, uploadThroughput);
            session.navigate("https://aol.com");
            session.waitDocumentReady(60_000);
        } finally {
            launcher.kill();
        }
    }
}
