// SPDX-License-Identifier: MIT
package com.cdp4j.network;

import com.cdp4j.Launcher;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.google.gson.Gson;
import java.util.Map;

public class UserAgent {

    @SuppressWarnings("rawtypes")
    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            session.setUserAgent("My Browser");
            session.navigate("https://httpbin.org/headers");
            session.waitDocumentReady();
            String response = (String) session.evaluate("document.body.textContent");

            Gson gson = new Gson();
            Map json = gson.fromJson(response, Map.class);
            Map headers = (Map) json.get("headers");

            System.out.println(headers.get("User-Agent"));
        } finally {
            launcher.kill();
        }
    }
}
