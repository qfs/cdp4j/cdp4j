package com.cdp4j.network;

import static com.cdp4j.session.WaitUntil.NetworkIdle;

import com.cdp4j.Launcher;
import com.cdp4j.command.Network;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.network.Cookie;
import com.cdp4j.type.network.CookieParam;
import java.util.Arrays;
import java.util.List;

public class Cookies {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch()) {
            try (Session session = factory.create()) {
                session.navigateAndWait("https://httpbin.org/cookies/set?foo=bar", NetworkIdle);
                Network network = session.getCommand().getNetwork();
                List<Cookie> cookies = network.getCookies();
                Cookie cookie = cookies.get(0);
                System.out.println("Cookie Domain=" + cookie.getDomain());
                System.out.println("name=" + cookie.getName());
                System.out.println("value=" + cookie.getValue());

                CookieParam param = new CookieParam();
                param.setDomain(cookie.getDomain());
                param.setName(cookie.getName());
                param.setValue("new-value");
                param.setPath(cookie.getPath());

                network.setCookies(Arrays.asList(param));

                session.navigateAndWait("https://httpbin.org/cookies", NetworkIdle);
                cookie = network.getCookies().get(0);

                System.out.println("Cookie Domain=" + cookie.getDomain());
                System.out.println("name=" + cookie.getName());
                System.out.println("value=" + cookie.getValue());
            }
        } finally {
            launcher.kill();
        }
    }
}
