// SPDX-License-Identifier: MIT
package com.cdp4j.network;

import static com.cdp4j.event.Events.NetworkLoadingFinished;
import static com.cdp4j.event.Events.NetworkResponseReceived;

import com.cdp4j.Launcher;
import com.cdp4j.event.network.LoadingFinished;
import com.cdp4j.event.network.ResponseReceived;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.network.GetResponseBodyResult;
import com.cdp4j.type.network.ResourceType;
import com.cdp4j.type.network.Response;
import java.util.HashSet;
import java.util.Set;

public class NetworkResponse {

    public static void main(String[] args) {

        Launcher launcher = new Launcher();

        Set<String> finished = new HashSet<>();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {
            session.getCommand().getNetwork().enable();
            session.addEventListener((e, d) -> {
                if (NetworkLoadingFinished.equals(e)) {
                    LoadingFinished lf = (LoadingFinished) d;
                    finished.add(lf.getRequestId());
                }
                if (NetworkResponseReceived.equals(e)) {
                    ResponseReceived rr = (ResponseReceived) d;
                    Response response = rr.getResponse();
                    System.out.println("----------------------------------------");
                    System.out.println("URL       : " + response.getUrl());
                    System.out.println(
                            "Status    : HTTP " + response.getStatus().intValue() + " " + response.getStatusText());
                    System.out.println("Mime Type : " + response.getMimeType());
                    if (finished.contains(rr.getRequestId()) && ResourceType.Document.equals(rr.getType())) {
                        GetResponseBodyResult rb =
                                session.getCommand().getNetwork().getResponseBody(rr.getRequestId());
                        if (rb != null) {
                            String body = rb.getBody();
                            System.out.println(
                                    "Content   : " + body.substring(0, body.length() > 1024 ? 1024 : body.length()));
                        }
                    }
                }
            });
            session.navigate("http://cnn.com");
            session.waitDocumentReady();
        } finally {
            launcher.kill();
        }
    }
}
