// SPDX-License-Identifier: MIT
package com.cdp4j.network;

import static com.cdp4j.event.Events.FetchRequestPaused;
import static com.cdp4j.session.WaitUntil.NetworkAlmostIdle;
import static com.cdp4j.type.network.ErrorReason.BlockedByClient;
import static com.cdp4j.type.network.ResourceType.Image;
import static java.util.Arrays.asList;

import com.cdp4j.Launcher;
import com.cdp4j.command.Fetch;
import com.cdp4j.event.fetch.RequestPaused;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.fetch.RequestPattern;

public class URLFiltering {

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch()) {
            try (Session session = factory.create()) {

                Fetch fetch = session.getCommand().getFetch();
                RequestPattern pattern = new RequestPattern();
                pattern.setUrlPattern("*");
                // intercept images
                pattern.setResourceType(Image);

                fetch.enable(asList(pattern), null);

                session.addEventListener((e, v) -> {
                    if (FetchRequestPaused.equals(e)) {
                        RequestPaused rp = (RequestPaused) v;
                        String url = rp.getRequest().getUrl();
                        // do not allow to download jpg files
                        if (url.endsWith(".jpg")) {
                            fetch.failRequest(rp.getRequestId(), BlockedByClient);
                        }
                    }
                });

                session.navigate("https://cnn.com");
                session.waitUntil(NetworkAlmostIdle);
            }
        } finally {
            launcher.kill();
        }
    }
}
