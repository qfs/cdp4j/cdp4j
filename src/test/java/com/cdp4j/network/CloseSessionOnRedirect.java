// SPDX-License-Identifier: MIT
package com.cdp4j.network;

import static com.cdp4j.event.Events.NetworkRequestWillBeSent;

import com.cdp4j.Launcher;
import com.cdp4j.command.Network;
import com.cdp4j.event.network.RequestWillBeSent;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.type.network.Response;

public class CloseSessionOnRedirect {

    private static boolean terminateSession;

    public static void main(String[] args) {
        Launcher launcher = new Launcher();

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            Network network = session.getCommand().getNetwork();
            network.enable();

            session.addEventListener((e, d) -> {
                if (NetworkRequestWillBeSent.equals(e)) {
                    RequestWillBeSent rws = (RequestWillBeSent) d;
                    Response rr = rws.getRedirectResponse();

                    boolean isRedirect = rr != null && rr.getStatus() != null;

                    if (isRedirect) {
                        terminateSession = true;
                        session.stop();
                        session.close();

                        System.out.println("------------------------------------------------------------------------");
                        System.out.println(
                                "Redirect URL          : " + rws.getRequest().getUrl());
                        System.out.println(
                                "Redirect URL Fragment : " + rws.getRequest().getUrlFragment());
                        System.out.println("Redirect Status Code  : " + rr.getStatus());
                        System.out.println("Redirect Header       : "
                                + rws.getRedirectResponse().getHeaders());
                        System.out.println("------------------------------------------------------------------------");
                    }
                }
            });

            session.navigate("https://httpbin.org/redirect-to?url=https://google.com");

            if (!terminateSession) {
                session.waitDocumentReady();
            }
        } finally {
            launcher.kill();
        }
    }
}
