// SPDX-License-Identifier: MIT
package com.cdp4j.pdf;

import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.write;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;
import java.nio.file.Path;

public class PrintToPDF {

    public static void main(String[] args) throws IOException {

        Path file = createTempFile("cdp4j", ".pdf");

        Options options = Options.builder().headless(true).build();

        Launcher launcher = new Launcher(options);

        try (SessionFactory factory = launcher.launch()) {

            String context = factory.createBrowserContext();
            try (Session session = factory.create(context)) {

                session.navigate("https://example.com");
                session.waitDocumentReady();

                byte[] content = session.printToPDF();
                write(file, content);
            }

            factory.disposeBrowserContext(context);
        }

        if (isDesktopSupported()) {
            getDesktop().open(file.toFile());
        }

        launcher.kill();
    }
}
