// SPDX-License-Identifier: MIT
package com.cdp4j.pdf;

import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;
import static java.nio.file.Files.createTempFile;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;
import java.nio.file.Path;

public class PrintPDFtoFile {

    public static void main(String[] args) throws IOException {
        Path file = createTempFile("cdp4j", ".pdf");

        int timeout = 60_000; // 60 seconds

        Options options = Options.builder().headless(true).readTimeout(timeout).build();

        Launcher launcher = new Launcher(options);

        try (SessionFactory factory = launcher.launch()) {

            String context = factory.createBrowserContext();
            try (Session session = factory.create(context)) {

                session.navigate("https://docs.jboss.org/resteasy/docs/4.0.0.Final/userguide/html_single/index.html");
                session.waitDocumentReady(timeout);
                session.printToPDF(file);
            }

            factory.disposeBrowserContext(context);
        }

        if (isDesktopSupported()) {
            getDesktop().open(file.toFile());
        }

        launcher.kill();
    }
}
