// SPDX-License-Identifier: MIT
package com.cdp4j.video;

import static com.cdp4j.JsonLibrary.Jackson;
import static com.cdp4j.event.Events.PageScreencastFrame;
import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.logger.CdpLogggerLevel.Info;
import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;
import static java.util.EnumSet.of;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.session.VideoRecorder;
import com.cdp4j.session.VideoRecorderOptions;
import com.cdp4j.session.VideoRecorderResult;
import java.io.IOException;

// Unlike to FFmpegVideoRecorder, this examples encodes the recorded video in realtime.
// cdp4j uses Cisco's openh264 encoder and IHMC Java Video Codecs library.
//
// openh264   : https://github.com/cisco/openh264
// JNI binding: https://github.com/ihmcrobotics/ihmc-video-codecs
//
public class VideoRecorderRealtimeExample {

    public static void main(String[] args) {
        Launcher launcher = new Launcher(Options.builder()
                .loggerType(Console)
                .consoleLoggerLevel(Info)
                .jsonLibrary(Jackson)
                .build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            session.setRegisteredEventListeners(of(PageScreencastFrame));

            VideoRecorder recorder = session.createRealtimeVideoRecoder(VideoRecorderOptions.builder()
                    .videoFileName(
                            "out.mp4") // default file extension is webm, don't forget to change the file extension
                    .build());

            recorder.start();

            session.navigate("https://webkit.org/blog-files/3d-transforms/poster-circle.html")
                    .waitDocumentReady()
                    .wait(5_000);

            recorder.stop();

            VideoRecorderResult result = recorder.encode();

            if (result.isSucceess() && isDesktopSupported()) {
                try {
                    getDesktop().open(result.getVideoFile().toFile());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
