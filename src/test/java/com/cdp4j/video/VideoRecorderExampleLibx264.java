// SPDX-License-Identifier: MIT
package com.cdp4j.video;

import static com.cdp4j.JsonLibrary.Jackson;
import static com.cdp4j.event.Events.PageScreencastFrame;
import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.logger.CdpLogggerLevel.Info;
import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;
import static java.util.Arrays.asList;
import static java.util.EnumSet.of;

import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.session.VideoRecorder;
import com.cdp4j.session.VideoRecorderOptions;
import com.cdp4j.session.VideoRecorderResult;
import java.io.IOException;
import java.util.List;

//
// Record video and encode with vp8 encoder
//
// This example uses installed ffmpeg from host environment
// Make sure that ffmpeg exist in PATH environment variable
//
public class VideoRecorderExampleLibx264 {

    public static void main(String[] args) {
        System.out.println("Make sure that ffmpeg executable exist in PATH variable!");

        Launcher launcher = new Launcher(Options.builder()
                .loggerType(Console)
                .consoleLoggerLevel(Info)
                .jsonLibrary(Jackson)
                .build());

        try (SessionFactory factory = launcher.launch();
                Session session = factory.create()) {

            session.setRegisteredEventListeners(of(PageScreencastFrame));

            // encoding parameters for libx264
            List<String> libx264Args = asList(new String[] {
                "-vcodec", "libx264", "-pix_fmt", "yuv420p", "-r", "25", "-an",
            });

            VideoRecorder recorder = session.createVideoRecoder(VideoRecorderOptions.builder()
                    .ffmpegExecutable("ffmpeg") // use ffmpeg from env path
                    .videoFileName("video.mp4") // use mp4 extension
                    .ffmpegArgs(libx264Args)
                    .build());

            recorder.start();

            session.navigate("https://webkit.org/blog-files/3d-transforms/poster-circle.html")
                    .waitDocumentReady()
                    .wait(5_000);

            recorder.stop();

            VideoRecorderResult result = recorder.encode();
            if (result.isSucceess() && isDesktopSupported()) {
                try {
                    getDesktop().open(result.getVideoFile().toFile());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
