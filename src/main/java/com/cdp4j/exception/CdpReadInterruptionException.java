// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

public class CdpReadInterruptionException extends CdpReadTimeoutException {

    private static final long serialVersionUID = -7581526063885886707L;

    public CdpReadInterruptionException(long timeout) {
        super(timeout);
    }
}
