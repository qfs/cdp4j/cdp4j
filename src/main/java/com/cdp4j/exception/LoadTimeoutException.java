// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

public class LoadTimeoutException extends CdpException {

    private static final long serialVersionUID = 2319366102554237916L;

    public LoadTimeoutException(String message) {
        super(message);
    }

    public LoadTimeoutException(Throwable e) {
        super(e);
    }
}
