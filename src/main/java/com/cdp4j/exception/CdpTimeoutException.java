// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

public class CdpTimeoutException extends CdpException {

    private static final long serialVersionUID = -7581526063885886708L;

    public CdpTimeoutException(String message) {
        super(message);
    }
}
