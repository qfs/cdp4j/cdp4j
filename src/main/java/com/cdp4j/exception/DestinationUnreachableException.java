// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

public class DestinationUnreachableException extends CdpException {

    private static final long serialVersionUID = 7372187865435989609L;

    public DestinationUnreachableException(String message) {
        super(message);
    }
}
