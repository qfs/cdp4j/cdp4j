// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

public class CommandException extends CdpException {

    private static final long serialVersionUID = -815753477843472988L;

    private final int code;

    public CommandException(Throwable e) {
        super(e);
        this.code = 0;
    }

    public CommandException(final int code, final String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
