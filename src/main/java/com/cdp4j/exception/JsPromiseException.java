// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

public class JsPromiseException extends CdpException {

    private static final long serialVersionUID = 1L;

    public JsPromiseException(String message) {
        super(message);
    }
}
