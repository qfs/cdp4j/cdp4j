// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

import java.util.function.Consumer;

/**
 * The CdpReadTimeoutExceptionHandler allows to handle a {@link CdpReadTimeoutException}
 * independently of the CDP command which triggered the exception.
 * <br />
 * To ignore the exception, simply swallow it, otherwise rethrow the exception provided as argument.
 */
@FunctionalInterface
public interface CdpReadTimeoutExceptionHandler extends Consumer<CdpReadTimeoutException> {}
