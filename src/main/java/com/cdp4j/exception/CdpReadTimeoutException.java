// SPDX-License-Identifier: MIT
package com.cdp4j.exception;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;

public class CdpReadTimeoutException extends CdpException {

    private static final long serialVersionUID = -7581526063885886708L;

    private long timeout;

    /* Optional contextual data */
    private DomainCommand command;
    private CommandReturnType crt;
    private String[] argNames;
    private Object[] args;
    private String commandJson;

    public CdpReadTimeoutException(String message) {
        super(message);
    }

    public CdpReadTimeoutException(long timeout) {
        super(timeout + " ms");
        this.timeout = timeout;
    }

    public long getTimeout() {
        return timeout;
    }

    public DomainCommand getCommand() {
        return command;
    }

    public CommandReturnType getCommandReturnType() {
        return crt;
    }

    public String[] getArgNames() {
        return argNames;
    }

    public Object[] getArgs() {
        return args;
    }

    public String getCommandJson() {
        return commandJson;
    }

    public void setContextualData(
            DomainCommand command, CommandReturnType crt, String[] argNames, Object[] args, String commandJson) {
        this.command = command;
        this.crt = crt;
        this.argNames = argNames;
        this.args = args;
        this.commandJson = commandJson;
    }
}
