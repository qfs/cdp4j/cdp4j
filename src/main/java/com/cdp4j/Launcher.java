// SPDX-License-Identifier: MIT
package com.cdp4j;

import static com.cdp4j.Constant.JAVA_8;
import static com.cdp4j.Constant.MACOS;
import static com.cdp4j.Constant.MACOS_ARM;
import static com.cdp4j.Constant.WINDOWS;
import static java.lang.Long.toHexString;
import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.lang.System.getenv;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.isExecutable;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Locale.ENGLISH;
import static java.util.concurrent.ThreadLocalRandom.current;

import com.cdp4j.channel.ChannelFactory;
import com.cdp4j.channel.Connection;
import com.cdp4j.channel.DevToolsConnection;
import com.cdp4j.channel.DevToolsProfileConnection;
import com.cdp4j.channel.JreWebSocketFactory;
import com.cdp4j.channel.NvWebSocketFactory;
import com.cdp4j.channel.WebSocketConnection;
import com.cdp4j.exception.CdpException;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.CdpLoggerFactory;
import com.cdp4j.process.CdpProcess;
import com.cdp4j.session.SessionFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * A class that is able to create and launch a browser instance.
 *
 * If you launch the browser externally,
 * use {@link DevToolsConnection} or {@link DevToolsProfileConnection} class to connect via a WebSocket connection.
 */
public class Launcher {

    private final Options options;

    private final ChannelFactory channelFactory;

    private final CdpLogger log;

    private Path userDataDir;

    private SessionFactory factory;

    public Launcher(Options options, ChannelFactory channelFactory) {
        this.options = options;
        this.channelFactory = channelFactory;
        this.log = new CdpLoggerFactory(options).getLogger("cdp4j.launcher");
        log.info("ChannelFactory: {}", channelFactory.getClass().getSimpleName());
    }

    public Launcher(ChannelFactory channelFactory) {
        this(Options.builder().build(), channelFactory);
    }

    public Launcher(Options options) {
        this(options, createChannelFactory());
    }

    public Launcher() {
        this(Options.builder().build(), createChannelFactory());
    }

    private static ChannelFactory createChannelFactory() {
        if (JAVA_8) {
            return new NvWebSocketFactory();
        } else {
            return new JreWebSocketFactory();
        }
    }

    protected String findChrome() {
        String executablePath = options.browserExecutablePath();
        if (executablePath != null && !executablePath.trim().isEmpty()) {
            log.info("Browser executablePath: {}", executablePath);
            return executablePath;
        } else {
            if (WINDOWS) {
                String executable = findChromeWinPath();
                log.info("Browser executablePath [Windows]: {}", executable);
                if (executable == null) {
                    throw new CdpException("Chromium/Chrome browser not found");
                } else {
                    return executable;
                }
            } else if (MACOS) {
                String executable = findChromeOsxPath();
                log.info("Browser executablePath [macOS]: {}", executable);
                if (executable == null) {
                    throw new CdpException("Chromium/Chrome browser not found");
                } else {
                    return executable;
                }
            }
            executablePath = "google-chrome";
            log.info("Browser executablePath [GNU/Linux]: {}", executablePath);
            return executablePath;
        }
    }

    protected String findChromeWinPath() {
        try {
            for (String next : getChromeWinPaths()) {
                Path path = Paths.get(next);
                boolean exist = exists(path) && isExecutable(path);
                if (exist) {
                    return next;
                }
            }
        } catch (Throwable e) {
            // ignore
        }
        return null;
    }

    /**
     * Tests whether chrome/chromium is installed.
     *
     * @return {@code true} if browser is found on predefined paths
     */
    public boolean isChromeInstalled() {
        return findChrome() != null ? true : false;
    }

    protected List<String> getChromeWinPaths() {
        Browser browser = options.browser();
        List<String> prefixes = asList(getenv("localappdata"), getenv("programfiles"), getenv("programfiles(x86)"));
        List<String> suffixes = emptyList();
        switch (browser) {
            case Any:
                suffixes = asList(
                        "\\Google\\Chrome Dev\\Application\\chrome.exe", // Chrome Dev
                        "\\Google\\Chrome SxS\\Application\\chrome.exe", // Chrome Canary
                        "\\Google\\Chrome\\Application\\chrome.exe", // Chrome
                        "\\Microsoft\\Edge\\Application\\msedge.exe"); // Microsoft Edge
            default:
            case Chrome:
                suffixes = asList("\\Google\\Chrome\\Application\\chrome.exe");
                break;
            case ChromeCanary:
                suffixes = asList("\\Google\\Chrome SxS\\Application\\chrome.exe");
                break;
            case ChromeDev:
                suffixes = asList("\\Google\\Chrome Dev\\Application\\chrome.exe");
                break;
            case MicrosoftEdge:
                suffixes = asList("\\Microsoft\\Edge\\Application\\msedge.exe");
                break;
        }
        List<String> installations = new ArrayList<String>(prefixes.size() * suffixes.size());
        for (String prefix : prefixes) {
            for (String suffix : suffixes) {
                installations.add(prefix + suffix);
            }
        }
        return installations;
    }

    protected String findChromeOsxPath() {
        for (String path : getChromeOsxPaths()) {
            final File chrome = new File(path);
            if (chrome.exists() && chrome.canExecute()) {
                return chrome.toString();
            }
        }
        return null;
    }

    protected List<String> getChromeOsxPaths() {
        return asList(
                "/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary", // Chrome Canary
                "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" // Chrome Stable
                );
    }

    protected List<String> getCommonParameters(String chromeExecutablePath, List<String> arguments) {
        List<String> list = new ArrayList<>();

        final String safeProcessPath = options.safeProcessPath();
        if (!MACOS_ARM && safeProcessPath != null && !safeProcessPath.trim().isEmpty()) {
            list.add(safeProcessPath);
            list.add("--");
        }

        list.add(chromeExecutablePath);
        // Disable built-in Google Translate service
        list.add("--disable-features=Translate");
        // Disable all chrome extensions entirely
        list.add("--disable-extensions");
        // Disable some extensions that aren't affected by --disable-extensions
        list.add("--disable-component-extensions-with-background-pages");
        // Disable various background network services, including extension updating,
        // safe browsing service, upgrade detector, translate, UMA
        list.add("--disable-background-networking");
        // Disable fetching safebrowsing lists, likely redundant due to disable-background-networking
        list.add("--safebrowsing-disable-auto-update");
        // Don't update the browser 'components' listed at chrome://components/
        list.add("--disable-component-update");
        // Disables client-side phishing detection.
        list.add("--disable-client-side-phishing-detection");
        // Disable syncing to a Google account
        list.add("--disable-sync");
        // Disable reporting to UMA, but allows for collection
        list.add("--metrics-recording-only");
        // Disable installation of default apps on first run
        list.add("--disable-default-apps");
        // Mute any audio
        list.add("--mute-audio");
        // Skip first run wizards
        list.add("--no-first-run");
        // Disable renderer process backgrounding
        list.add("--disable-renderer-backgrounding");
        // Disable task throttling of timer tasks from background pages.
        list.add("--disable-background-timer-throttling");
        // Disable the default throttling of IPC between renderer & browser processes.
        list.add("--disable-ipc-flooding-protection");
        // Disable the default browser check, do not prompt to set it as such
        list.add("--no-default-browser-check");
        // Use mock keychain on Mac to prevent blocking permissions dialogs
        list.add("--use-mock-keychain");
        // Extra flags
        list.add("--disable-breakpad");
        // Disable backgrounding renders for occluded windows
        list.add("--disable-backgrounding-occluded-windows");
        // Avoid potential instability of using Gnome Keyring or KDE wallet. crbug.com/571003 crbug.com/991424
        list.add("--password-store=basic");
        // Disable background tracing (aka slow reports & deep reports) to avoid 'Tracing already started'
        list.add("--force-fieldtrials=*BackgroundTracing/default/");
        // others
        list.add("--disable-hang-monitor");
        list.add("--disable-dev-shm-usage");
        list.add("--disable-plugin-power-saver");
        list.add("--disable-popup-blocking");
        list.add("--disable-prompt-on-repost");
        if (!arguments.isEmpty()) {
            list.addAll(arguments);
        }
        return list;
    }

    protected String toString(InputStream is) {
        try (Scanner scanner = new Scanner(is)) {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }

    public SessionFactory launch() {
        List<String> arguments = getCommonParameters(findChrome(), options.arguments());
        appendStartPage(arguments);
        userDataDir = options.userDataDir() != null
                ? options.userDataDir()
                : get(getProperty("java.io.tmpdir")).resolve("remote-profile");
        if (!options.useDefaultUserDataDir() && userDataDir != null) {
            log.info("user-data-dir: {}", userDataDir.toString());
            arguments.add(format("--user-data-dir=%s", userDataDir.toString()));
        }
        boolean inUse = options.useDefaultUserDataDir() ? false : isInUse(userDataDir);
        if (inUse) {
            throw new CdpException("--user-data-dir [" + userDataDir.toString() + "] is used by another process.");
        }
        if (options.headless()) {
            log.info("headless: {}", options.headless());
            arguments.add("--headless=new");
        }
        SessionFactory factory = null;
        if (options.remoteDebuggingPort() > 0) {
            log.info("remote-debugging-port: {}", options.remoteDebuggingPort());
            arguments.add(format("--remote-debugging-port=%d", options.remoteDebuggingPort()));
        } else {
            arguments.add("--remote-debugging-port=0");
        }
        log.info("launcher: {}", "ProcessBuilder");
        factory = launchWithProcessBuilder(arguments);
        return this.factory = factory;
    }

    protected void appendStartPage(List<String> arguments) {
        if (!arguments.get(arguments.size() - 1).equals("about:blank")) {
            arguments.add("about:blank");
        }
    }

    /**
     * Read the DevToolsActivePort file, which contains the WebSocket port.
     *
     * @return {@code true} if the WebSocket connection is in use.
     */
    protected boolean isInUse(Path userDataDir) {
        Path devToolsActivePort = userDataDir.resolve("DevToolsActivePort");
        if (exists(devToolsActivePort)) {
            List<String> lines = emptyList();
            try {
                lines = readAllLines(devToolsActivePort);
            } catch (IOException e) {
                throw new CdpException(e);
            }
            if (lines.size() >= 1) {
                int port = Integer.parseInt(lines.get(0));
                if (port > 0) {
                    try (ServerSocket ignored = new ServerSocket(port)) {
                        return false;
                    } catch (IOException e) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected SessionFactory launchWithProcessBuilder(List<String> arguments) {
        String cdp4jId = toHexString(current().nextLong());
        arguments.add(format("--cdp4jId=%s", cdp4jId));
        Connection connection = null;
        ProcessBuilder builder = new ProcessBuilder(arguments);
        builder.environment().put("CDP4J_ID", cdp4jId);
        try {
            Process process = builder.start();
            try (Scanner scanner = new Scanner(process.getErrorStream())) {
                while (scanner.hasNext()) {
                    String line = scanner.nextLine().trim();
                    if (line.isEmpty()) {
                        continue;
                    }
                    if (line.toLowerCase(ENGLISH).startsWith("devtools listening on")) {
                        int start = line.indexOf("ws://");
                        connection = new WebSocketConnection(line.substring(start, line.length()));
                        break;
                    }
                }
                if (connection == null) {
                    throw new CdpException("WebSocket connection url is required!");
                }
            }
            if (!process.isAlive()) {
                throw new CdpException("No process: the chrome process is not alive.");
            }
            options.processManager().setProcess(new CdpProcess(process, cdp4jId));
        } catch (IOException e) {
            throw new CdpException(e);
        }
        try {
            URL url = new URL(connection.getUrl().replace("ws://", "http://"));
            log.info("DevTools remote debugging URL: http://{}:{}", url.getHost(), url.getPort());
        } catch (MalformedURLException e) {
            throw new CdpException(e);
        }
        SessionFactory factory = new SessionFactory(options, channelFactory, connection);
        return factory;
    }

    /**
     * Closes the SessionFactory and kills the Browser process.
     *
     * @return {@code true} if the process is terminated successfully.
     */
    public boolean kill() {
        if (factory != null) {
            factory.close();
        }
        return options.processManager().kill();
    }

    public Options getOptions() {
        return options;
    }

    public Path getUserDataDir() {
        return userDataDir;
    }
}
