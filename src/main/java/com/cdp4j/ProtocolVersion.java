// SPDX-License-Identifier: MIT
package com.cdp4j;

/**
 * Based on this enum, some features of cdp4j during connection and session setup are enabled or disabled.
 */
public enum ProtocolVersion {
    /** @see https://chromedevtools.github.io/devtools-protocol/tot/ */
    ToT,

    /** @see https://chromedevtools.github.io/devtools-protocol/v8/ */
    V8
}
