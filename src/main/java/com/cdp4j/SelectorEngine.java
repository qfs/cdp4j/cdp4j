// SPDX-License-Identifier: MIT
package com.cdp4j;

public enum SelectorEngine {
    /**
     * Use w3c's native selector engine.
     */
    Native,
    /**
     * Use Playwright's selector engine.
     *
     * Playwright supports multiple selector engines used to query elements in the
     * web page. Selector is a string that consists of one or more clauses separated
     * by >> token, e.g. clause1 >> clause2 >> clause3. When multiple clauses are
     * present, next one is queried relative to the previous one's result.
     *
     * @see <a href=
     *      "https://playwright.dev/docs/1.18/selectors">https://playwright.dev/docs/1.18/selectors</a>
     */
    Playwright
}
