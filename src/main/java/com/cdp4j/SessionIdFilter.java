// SPDX-License-Identifier: MIT
package com.cdp4j;

public enum SessionIdFilter {
    SimpleParser,
    JsonParser
}
