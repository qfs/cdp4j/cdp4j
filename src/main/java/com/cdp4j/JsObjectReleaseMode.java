// SPDX-License-Identifier: MIT
package com.cdp4j;

public enum JsObjectReleaseMode {
    sync,
    async
}
