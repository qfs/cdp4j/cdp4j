// SPDX-License-Identifier: MIT
package com.cdp4j;

public enum JsonLibrary {
    Gson,
    Jackson
}
