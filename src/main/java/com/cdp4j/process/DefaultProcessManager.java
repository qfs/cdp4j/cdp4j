// SPDX-License-Identifier: MIT
package com.cdp4j.process;

import static java.lang.ProcessHandle.of;

import java.lang.ProcessHandle.Info;
import java.time.Instant;
import java.util.Optional;
import java.util.stream.Stream;

public class DefaultProcessManager implements ProcessManager {

    protected long pid;

    protected Instant startTime;

    protected String command;

    @Override
    public void setProcess(CdpProcess process) {
        ProcessHandle handle = ((Process) process.getProcess()).toHandle();
        Info info = handle.info();
        startTime = info.startInstant().get();
        command = info.command().get();
        pid = handle.pid();
    }

    @Override
    public boolean kill() {
        if (pid <= 0) {
            return false;
        }
        Optional<ProcessHandle> process = of(pid);
        if (process.isPresent()) {
            ProcessHandle handle = process.get();
            Info info = handle.info();
            if (handle.isAlive()
                    && info.startInstant().isPresent()
                    && info.startInstant().get().equals(startTime)
                    && info.command().isPresent()
                    && info.command().get().equals(command)) {
                try (Stream<ProcessHandle> descendants = handle.descendants()) {
                    descendants.forEach(ph -> {
                        try {
                            if (ph.isAlive()) {
                                ph.destroyForcibly();
                            }
                        } catch (Exception ignored) {
                            // no op
                        }
                    });
                }
                boolean success = handle.destroyForcibly();
                if (success) {
                    handle.onExit().join();
                }
                return success;
            }
        }
        return false;
    }
}
