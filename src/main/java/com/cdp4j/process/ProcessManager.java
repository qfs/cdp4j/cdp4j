// SPDX-License-Identifier: MIT
package com.cdp4j.process;

public abstract interface ProcessManager {

    void setProcess(CdpProcess process);

    boolean kill();
}
