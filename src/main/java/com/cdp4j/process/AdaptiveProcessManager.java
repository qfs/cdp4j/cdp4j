// SPDX-License-Identifier: MIT
package com.cdp4j.process;

import static com.cdp4j.Constant.JAVA_8;
import static com.cdp4j.Constant.MACOS;
import static com.cdp4j.Constant.WINDOWS;

public class AdaptiveProcessManager implements ProcessManager {

    private ProcessManager processManager;

    public AdaptiveProcessManager() {
        processManager = init();
    }

    protected ProcessManager init() {
        if (JAVA_8) {
            if (WINDOWS) {
                return new TaskKillProcessManager();
            } else if (MACOS) {
                return new MacOsProcessManager();
            } else {
                return new LinuxProcessManager();
            }
        } else {
            return new DefaultProcessManager();
        }
    }

    @Override
    public void setProcess(CdpProcess process) {
        processManager.setProcess(process);
    }

    @Override
    public boolean kill() {
        return processManager.kill();
    }
}
