// SPDX-License-Identifier: MIT
package com.cdp4j.process;

import static java.lang.Class.forName;
import static java.lang.String.valueOf;

import com.cdp4j.exception.CdpException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MacOsProcessManager implements ProcessManager {

    private int pid;

    private String cdp4jId;

    private CdpProcess process;

    @Override
    public void setProcess(CdpProcess process) {
        if (this.process != null) {
            throw new IllegalStateException();
        }
        try {
            Field pidField = process.getProcess().getClass().getDeclaredField("pid");
            pidField.setAccessible(true);
            this.pid = (int) pidField.get(process.getProcess());
            this.process = process;
        } catch (Throwable e) {
            throw new CdpException(e);
        }
        this.cdp4jId = process.getCdp4jProcessId();
    }

    @Override
    public boolean kill() {
        if (process == null) {
            return false;
        }
        ProcessBuilder builder = new ProcessBuilder("ps", "auxww");
        boolean found = false;
        try {
            Process process = builder.start();
            try (BufferedReader scanner = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                String line = scanner.readLine();
                String processId = "cdp4jId=" + cdp4jId;
                String strPid = " " + valueOf(pid) + " ";
                while ((line = scanner.readLine()) != null) {
                    if (line.contains(processId) && line.contains(strPid)) {
                        found = true;
                        break;
                    }
                }
            }
        } catch (Throwable e) {
            return false;
        }
        if (!found) {
            return false;
        }
        try {
            Class<?> clazz = forName("java.lang.UNIXProcess");
            Method destroyProcess = clazz.getDeclaredMethod("destroyProcess", int.class, boolean.class);
            destroyProcess.setAccessible(true);
            boolean force = false;
            destroyProcess.invoke(null, pid, force);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }
}
