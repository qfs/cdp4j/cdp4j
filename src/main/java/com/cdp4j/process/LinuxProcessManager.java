// SPDX-License-Identifier: MIT
package com.cdp4j.process;

import static java.lang.Class.forName;

import com.cdp4j.exception.CdpException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Scanner;

public class LinuxProcessManager implements ProcessManager {

    private int pid;

    private String cdp4jId;

    private CdpProcess process;

    @Override
    public void setProcess(CdpProcess process) {
        if (this.process != null) {
            throw new IllegalStateException();
        }
        try {
            Field pidField = process.getProcess().getClass().getDeclaredField("pid");
            pidField.setAccessible(true);
            this.pid = (int) pidField.get(process.getProcess());
            this.process = process;
        } catch (Throwable e) {
            throw new CdpException(e);
        }
        this.cdp4jId = process.getCdp4jProcessId();
    }

    @Override
    public boolean kill() {
        if (process == null) {
            return false;
        }
        ProcessBuilder builder = new ProcessBuilder("strings", "-a", "/proc/" + pid + "/cmdline");
        try {
            Process process = builder.start();
            String stdout = toString(process.getInputStream());
            if (!stdout.contains("cdp4jId=" + cdp4jId)) {
                return false;
            }
        } catch (Throwable e) {
            return false;
        }
        try {
            Class<?> clazz = forName("java.lang.UNIXProcess");
            Method destroyProcess = clazz.getDeclaredMethod("destroyProcess", int.class, boolean.class);
            destroyProcess.setAccessible(true);
            boolean force = false;
            destroyProcess.invoke(null, pid, force);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    protected String toString(InputStream is) {
        try (Scanner scanner = new Scanner(is)) {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }
}
