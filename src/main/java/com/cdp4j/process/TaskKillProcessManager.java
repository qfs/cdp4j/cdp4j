// SPDX-License-Identifier: MIT
package com.cdp4j.process;

import static java.lang.String.valueOf;
import static java.util.concurrent.TimeUnit.SECONDS;

import com.cdp4j.exception.CdpException;
import java.io.IOException;
import java.lang.reflect.Field;

public class TaskKillProcessManager implements ProcessManager {

    private CdpProcess process;

    private long pid;

    @Override
    public void setProcess(CdpProcess process) {
        this.process = process;
        Field handleField;
        try {
            handleField = process.getProcess().getClass().getDeclaredField("handle");
        } catch (NoSuchFieldException | SecurityException e) {
            throw new CdpException(e);
        }
        handleField.setAccessible(true);
        try {
            pid = (long) handleField.get(process.getProcess());
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public boolean kill() {
        if (process == null) {
            return false;
        }
        try {
            Process process =
                    Runtime.getRuntime().exec(new String[] {"cmd", "/c", "taskkill", "/pid", valueOf(pid), "/T", "/F"});
            return process.waitFor(10, SECONDS) && process.exitValue() == 0;
        } catch (IOException | InterruptedException e) {
            throw new CdpException(e);
        }
    }
}
