// SPDX-License-Identifier: MIT
package com.cdp4j.process;

public class CdpProcess {

    private final Object process;

    private final String cdp4jProcessId;

    public CdpProcess(Object process, String cdp4jProcessId) {
        this.process = process;
        this.cdp4jProcessId = cdp4jProcessId;
    }

    public Object getProcess() {
        return process;
    }

    public String getCdp4jProcessId() {
        return cdp4jProcessId;
    }
}
