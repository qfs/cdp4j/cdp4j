// SPDX-License-Identifier: MIT
package com.cdp4j.junit;

import static com.cdp4j.JsonLibrary.Gson;
import static com.cdp4j.logger.CdpLoggerType.Console;
import static com.cdp4j.logger.CdpLogggerLevel.Info;

import com.cdp4j.Browser;
import com.cdp4j.JsonLibrary;
import com.cdp4j.Launcher;
import com.cdp4j.Options;
import com.cdp4j.Options.Builder;
import com.cdp4j.logger.CdpLoggerType;
import com.cdp4j.logger.CdpLogggerLevel;
import com.cdp4j.session.Session;
import com.cdp4j.session.SessionFactory;
import com.cdp4j.session.SessionSettings;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class CdpExtension
        implements ParameterResolver, BeforeEachCallback, AfterEachCallback, BeforeAllCallback, AfterAllCallback {

    private static final Namespace NS_CDP4J = Namespace.create("cdp4j");

    protected static final String KEY_LAUNCHER = "launcher";

    protected static final String KEY_FACTORY = "factory";

    protected static final String KEY_SESSIONS = "sessions";

    protected static final String KEY_DUMMY_SESSION = "dummy-session";

    protected static final String KEY_BROWSER_CONTEXT_IDS = "browser-context-ids";

    private final Options options;

    public CdpExtension() {
        this.options = createOptions();
    }

    public CdpExtension(Options options) {
        this.options = options;
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext context)
            throws ParameterResolutionException {
        return parameterContext.getParameter().getType().equals(Session.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext context)
            throws ParameterResolutionException {
        Store store = context.getStore(NS_CDP4J);
        if (store == null) {
            return null;
        }
        SessionFactory factory = (SessionFactory) store.get(KEY_FACTORY);
        if (factory == null) {
            return null;
        }
        boolean incognito = parameterContext.isAnnotated(Incognito.class);
        boolean multiFrame = parameterContext.isAnnotated(MultiFrame.class);
        Session session = null;
        if (incognito) {
            Incognito annotation = parameterContext.getParameter().getAnnotation(Incognito.class);
            String incognitoId = annotation.value();
            context.getStore(NS_CDP4J).get(KEY_BROWSER_CONTEXT_IDS);
            @SuppressWarnings("unchecked")
            Map<String, String> browserContextIds = (Map<String, String>) store.get(KEY_BROWSER_CONTEXT_IDS);
            browserContextIds.computeIfAbsent(incognitoId, id -> factory.createBrowserContext());
            if (multiFrame) {
                SessionSettings settings =
                        SessionSettings.builder().multiFrameMode(true).build();
                session = factory.create(browserContextIds.get(incognitoId), settings);
            } else {
                session = factory.create(browserContextIds.get(incognitoId));
            }
        } else {
            if (multiFrame) {
                SessionSettings settings =
                        SessionSettings.builder().multiFrameMode(true).build();
                session = factory.create(settings);
            } else {
                session = factory.create();
            }
        }
        @SuppressWarnings("unchecked")
        List<Session> sessions = (List<Session>) store.get(KEY_SESSIONS);
        sessions.add(session);
        return session;
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        Store store = context.getStore(NS_CDP4J);
        store.put(KEY_SESSIONS, new CopyOnWriteArrayList<>());
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        Store store = context.getStore(NS_CDP4J);
        @SuppressWarnings("unchecked")
        List<Session> sessions = (List<Session>) store.remove(KEY_SESSIONS);
        if (sessions == null) {
            return;
        }
        for (Session next : sessions) {
            next.close();
        }
        sessions.clear();
    }

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        Store store = context.getStore(NS_CDP4J);
        Launcher launcher = new Launcher(options);
        store.put(KEY_LAUNCHER, launcher);
        SessionFactory factory = launcher.launch();
        store.put(KEY_FACTORY, factory);
        store.put(KEY_SESSIONS, new CopyOnWriteArrayList<>());
        Session dummySession = factory.create();
        store.put(KEY_DUMMY_SESSION, dummySession);
        store.put(KEY_BROWSER_CONTEXT_IDS, new ConcurrentHashMap<>(1));
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        Store store = context.getStore(NS_CDP4J);
        if (store == null) {
            return;
        }
        Session dummySession = (Session) store.get(KEY_DUMMY_SESSION);
        if (dummySession != null) {
            dummySession.close();
        }
        SessionFactory factory = (SessionFactory) store.remove(KEY_FACTORY);
        if (factory != null) {
            @SuppressWarnings("unchecked")
            Map<String, String> browserContextIds = (Map<String, String>) store.remove(KEY_BROWSER_CONTEXT_IDS);
            if (browserContextIds != null) {
                browserContextIds.values().stream()
                        .forEach(browserContextId -> factory.disposeBrowserContext(browserContextId));
            }
            factory.close();
        }
        Launcher launcher = (Launcher) store.remove(KEY_LAUNCHER);
        if (launcher != null) {
            launcher.kill();
        }
    }

    protected Options createOptions() {
        Builder builder = Options.builder()
                .loggerType(loggerType())
                .consoleLoggerLevel(consoleLoggerLevel())
                .headless(headless())
                .userProfileCleanerMaxSleepTime(userProfileCleanerMaxSleepTime())
                .createNewUserDataDir(createNewUserDataDir())
                .jsonLibrary(jsonLibrary());
        if (browser() != null) {
            builder.browser(browser());
        }
        if (browserExecutablePath() != null && !browserExecutablePath().trim().isEmpty()) {
            builder.browserExecutablePath(browserExecutablePath());
        }
        return builder.build();
    }

    protected boolean createNewUserDataDir() {
        return true;
    }

    protected CdpLoggerType loggerType() {
        return Console;
    }

    protected CdpLogggerLevel consoleLoggerLevel() {
        return Info;
    }

    protected boolean headless() {
        return false;
    }

    protected JsonLibrary jsonLibrary() {
        return Gson;
    }

    protected Browser browser() {
        return null;
    }

    protected int userProfileCleanerMaxSleepTime() {
        return 2000; // ms
    }

    protected String browserExecutablePath() {
        return null;
    }
}
