// SPDX-License-Identifier: MIT
package com.cdp4j.junit;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({TYPE, PARAMETER})
@Retention(RUNTIME)
public @interface MultiFrame {}
