// SPDX-License-Identifier: MIT
package com.cdp4j;

import static com.cdp4j.Browser.Any;
import static com.cdp4j.Constant.MACOS;
import static com.cdp4j.Constant.WINDOWS;
import static com.cdp4j.JsObjectReleaseMode.async;
import static com.cdp4j.JsonLibrary.Gson;
import static com.cdp4j.SelectorEngine.Native;
import static com.cdp4j.SessionIdFilter.SimpleParser;
import static com.cdp4j.logger.CdpLoggerType.Null;
import static com.cdp4j.logger.CdpLogggerLevel.Info;
import static com.cdp4j.session.WaitingStrategy.Semaphore;
import static java.lang.Boolean.TRUE;
import static java.lang.System.getProperty;
import static java.nio.file.Files.copy;
import static java.nio.file.Files.createDirectory;
import static java.nio.file.Files.createFile;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.exists;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Collections.emptyList;
import static java.util.concurrent.Executors.newSingleThreadExecutor;

import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.CdpReadTimeoutExceptionHandler;
import com.cdp4j.logger.CdpLoggerType;
import com.cdp4j.logger.CdpLogggerLevel;
import com.cdp4j.process.AdaptiveProcessManager;
import com.cdp4j.process.ProcessManager;
import com.cdp4j.session.WaitingStrategy;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;

public class Options {

    private static final int DEFAULT_READ_TIMEOUT = 10 * 1000; // 10 seconds

    private static final int DEFAULT_SCREEN_WIDTH = 1366; // WXGA width

    private static final int DEFAULT_SCREEN_HEIGHT = 768; // WXGA height

    private CdpLoggerType loggerType;

    private String loggerNamePrefix;

    private Executor workerThreadPool;

    private Executor eventHandlerThreadPool;

    private Integer readTimeout;

    private CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler;

    private List<String> arguments;

    private Path userDataDir;

    private boolean useDefaultUserDataDir;

    private int userProfileCleanerMaxSleepTime = 1000 * 30; // 30 seconds

    private boolean createNewUserDataDir;

    private boolean headless;

    private Integer screenWidth;

    private Integer screenHeight;

    private CdpLogggerLevel consoleLoggerLevel;

    private Boolean shutdownThreadPoolOnClose;

    private WaitingStrategy waitingStrategy;

    private String browserExecutablePath;

    private Browser browser;

    private ProcessManager processManager;

    private SelectorEngine selectorEngine;

    private int remoteDebuggingPort;

    private JsonLibrary jsonLibrary;

    private ProtocolVersion protocolVersion;

    private JsObjectReleaseMode jsObjectReleaseMode;

    private SessionIdFilter sessionIdFilter;

    private String safeProcessPath;

    private static final String SAFE_PROCESS_PATH; // might be null

    static class CdpThreadFactory implements ThreadFactory {

        private final String name;

        CdpThreadFactory(String name) {
            this.name = name;
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r, name);
            thread.setDaemon(true);
            return thread;
        }
    }

    static {
        Path tmpdir = get(getProperty("java.io.tmpdir")).toAbsolutePath();
        String executable;
        if (WINDOWS) {
            executable = "safe-process.exe";
        } else if (MACOS) {
            executable = "safe-process-mac";
        } else {
            executable = "safe-process";
        }
        Path safeProcess = tmpdir.resolve("cdp4j").resolve(executable);
        String safeProcessPathCandidate = null;
        if (!exists(safeProcess) || !safeProcess.toFile().canExecute()) {
            try (InputStream is = Options.class.getResourceAsStream("/META-INF/" + executable)) {
                if (is != null) { // if executable is bundled
                    if (!exists(safeProcess.getParent())) {
                        createDirectory(safeProcess.getParent());
                    }
                    if (!exists(safeProcess)) {
                        createFile(safeProcess);
                    }
                    copy(is, safeProcess, REPLACE_EXISTING);
                    safeProcess.toFile().setExecutable(true);
                    safeProcessPathCandidate = safeProcess.toAbsolutePath().toString();
                }
            } catch (Exception e) {
                System.err.println("Could not setup safe-process execution: " + e.toString());
            }
        } else {
            safeProcessPathCandidate = safeProcess.toAbsolutePath().toString();
        }
        SAFE_PROCESS_PATH = safeProcessPathCandidate;
    }

    private Options() {
        // no op
    }

    public static Builder builder() {
        return new Options.Builder();
    }

    public static class Builder {

        private Options options = new Options();

        private Builder() {
            // no op
        }

        public Builder loggerType(CdpLoggerType loggerType) {
            options.loggerType = loggerType;
            return this;
        }

        public Builder loggerNamePrefix(String loggerNamePrefix) {
            options.loggerNamePrefix = loggerNamePrefix;
            return this;
        }

        public Builder workerThreadPool(Executor workerThreadPool) {
            options.workerThreadPool = workerThreadPool;
            return this;
        }

        public Builder eventHandlerThreadPool(Executor eventHandlerThreadPool) {
            options.eventHandlerThreadPool = eventHandlerThreadPool;
            return this;
        }

        public Builder arguments(List<String> arguments) {
            options.arguments = arguments;
            return this;
        }

        public Builder userDataDir(Path userDataDir) {
            options.userDataDir = userDataDir;
            return this;
        }

        public Builder headless(boolean headless) {
            options.headless = headless;
            return this;
        }

        public Builder consoleLoggerLevel(CdpLogggerLevel consoleLoggerLevel) {
            options.consoleLoggerLevel = consoleLoggerLevel;
            return this;
        }

        public Builder readTimeout(int readTimeout) {
            options.readTimeout = readTimeout;
            return this;
        }

        public Builder readTimeoutExceptionHandler(final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler) {
            options.readTimeoutExceptionHandler = readTimeoutExceptionHandler;
            return this;
        }

        public Builder browserExecutablePath(String browserExecutablePath) {
            if (browserExecutablePath == null || browserExecutablePath.trim().isEmpty()) {
                throw new IllegalArgumentException();
            }
            options.browserExecutablePath = browserExecutablePath;
            return this;
        }

        public Builder shutdownThreadPoolOnClose(boolean shutdownThreadPoolOnClose) {
            options.shutdownThreadPoolOnClose = shutdownThreadPoolOnClose;
            return this;
        }

        public Builder browser(Browser browser) {
            options.browser = browser;
            return this;
        }

        public Builder selectorEngine(SelectorEngine selectorEngine) {
            options.selectorEngine = selectorEngine;
            return this;
        }

        public Builder useDefaultUserDataDir(boolean useDefaultUserDataDir) {
            options.useDefaultUserDataDir = useDefaultUserDataDir;
            return this;
        }

        public Builder waitingStrategy(WaitingStrategy waitingStrategy) {
            options.waitingStrategy = waitingStrategy;
            return this;
        }

        public Builder processManager(ProcessManager processManager) {
            options.processManager = processManager;
            return this;
        }

        public Builder remoteDebuggingPort(int remoteDebuggingPort) {
            options.remoteDebuggingPort = remoteDebuggingPort;
            return this;
        }

        public Builder createNewUserDataDir(boolean createNewUserDataDir) {
            options.createNewUserDataDir = createNewUserDataDir;
            return this;
        }

        public Builder safeProcessPath(String safeProcessPath) {
            options.safeProcessPath = safeProcessPath;
            return this;
        }

        public Builder userProfileCleanerMaxSleepTime(int userProfileCleanerMaxSleepTime) {
            if (userProfileCleanerMaxSleepTime < 1000) {
                throw new IllegalArgumentException();
            }
            options.userProfileCleanerMaxSleepTime = userProfileCleanerMaxSleepTime;
            return this;
        }

        public Builder jsonLibrary(JsonLibrary jsonLibrary) {
            options.jsonLibrary = jsonLibrary;
            return this;
        }

        public Builder protocolVersion(ProtocolVersion protocolVersion) {
            options.protocolVersion = protocolVersion;
            return this;
        }

        public Builder jsObjectReleaseMode(JsObjectReleaseMode jsObjectReleaseMode) {
            options.jsObjectReleaseMode = jsObjectReleaseMode;
            return this;
        }

        public Builder sessionIdFilter(SessionIdFilter sessionIdFilter) {
            options.sessionIdFilter = sessionIdFilter;
            return this;
        }

        public Options build() {
            if (options.loggerType == null) {
                options.loggerType = Null;
            }
            if (options.consoleLoggerLevel == null) {
                options.consoleLoggerLevel = Info;
            }
            if (options.workerThreadPool == null) {
                options.workerThreadPool = newSingleThreadExecutor(new CdpThreadFactory("CdpWorkerThread"));
            }
            if (options.eventHandlerThreadPool == null) {
                options.eventHandlerThreadPool = newSingleThreadExecutor(new CdpThreadFactory("CdpEventHandlerThread"));
            }
            if (options.arguments == null) {
                options.arguments = emptyList();
            }
            if (options.readTimeout == null) {
                options.readTimeout = DEFAULT_READ_TIMEOUT;
            }
            if (options.screenHeight == null) {
                options.screenHeight = DEFAULT_SCREEN_HEIGHT;
            }
            if (options.screenWidth == null) {
                options.screenWidth = DEFAULT_SCREEN_WIDTH;
            }
            if (options.shutdownThreadPoolOnClose == null) {
                options.shutdownThreadPoolOnClose = TRUE;
            }
            if (options.waitingStrategy == null) {
                options.waitingStrategy = Semaphore;
            }
            if (options.browser == null) {
                options.browser = Any;
            }
            if (options.processManager == null) {
                options.processManager = new AdaptiveProcessManager();
            }
            if (options.protocolVersion == null) {
                options.protocolVersion = ProtocolVersion.ToT;
            }
            if (options.selectorEngine == null) {
                options.selectorEngine = Native;
            }
            if (options.jsonLibrary == null) {
                options.jsonLibrary = Gson;
            }
            if (options.jsObjectReleaseMode == null) {
                options.jsObjectReleaseMode = async;
            }
            if (options.sessionIdFilter == null) {
                options.sessionIdFilter = SimpleParser;
            }
            if (options.safeProcessPath == null) {
                options.safeProcessPath = SAFE_PROCESS_PATH;
            }
            if (options.createNewUserDataDir) {
                if (options.useDefaultUserDataDir || options.userDataDir != null) {
                    throw new IllegalStateException();
                }
                try {
                    options.userDataDir = createTempDirectory("cdp4j-user-profile");
                } catch (IOException e) {
                    throw new CdpException(e);
                }
            }
            return options;
        }
    }

    public CdpLoggerType loggerType() {
        return loggerType;
    }

    public String loggerNamePrefix() {
        return loggerNamePrefix;
    }

    public Executor workerThreadPool() {
        return workerThreadPool;
    }

    public Executor eventHandlerThreadPool() {
        return eventHandlerThreadPool;
    }

    public List<String> arguments() {
        return arguments;
    }

    public Path userDataDir() {
        return userDataDir;
    }

    public Integer readTimeout() {
        return readTimeout;
    }

    public CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler() {
        return readTimeoutExceptionHandler;
    }

    public boolean headless() {
        return headless;
    }

    public Integer screenWidth() {
        return screenWidth;
    }

    public Integer screenHeight() {
        return screenHeight;
    }

    public CdpLogggerLevel consoleLoggerLevel() {
        return consoleLoggerLevel;
    }

    public boolean shutdownThreadPoolOnClose() {
        return shutdownThreadPoolOnClose.booleanValue();
    }

    public WaitingStrategy waitingStrategy() {
        return waitingStrategy;
    }

    public String browserExecutablePath() {
        return browserExecutablePath;
    }

    public Browser browser() {
        return browser;
    }

    public ProcessManager processManager() {
        return processManager;
    }

    public SelectorEngine selectorEngine() {
        return selectorEngine;
    }

    public boolean useDefaultUserDataDir() {
        return useDefaultUserDataDir;
    }

    public int remoteDebuggingPort() {
        return remoteDebuggingPort;
    }

    public boolean createNewUserDataDir() {
        return createNewUserDataDir;
    }

    public int userProfileCleanerMaxSleepTime() {
        return userProfileCleanerMaxSleepTime;
    }

    public JsonLibrary jsonLibrary() {
        return jsonLibrary;
    }

    public ProtocolVersion protocolVersion() {
        return protocolVersion;
    }

    public JsObjectReleaseMode jsObjectReleaseMode() {
        return jsObjectReleaseMode;
    }

    public SessionIdFilter jsonIdFilter() {
        return sessionIdFilter;
    }

    public String safeProcessPath() {
        return safeProcessPath; // might be null
    }
}
