// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

import static com.cdp4j.logger.CdpLogggerLevel.Debug;
import static com.cdp4j.logger.CdpLogggerLevel.Error;
import static com.cdp4j.logger.CdpLogggerLevel.Info;
import static com.cdp4j.logger.CdpLogggerLevel.Warn;
import static com.cdp4j.logger.MessageFormatter.arrayFormat;

class CdpConsoleLogger implements CdpLogger {

    private final CdpLogggerLevel loggerLevel;

    public CdpConsoleLogger() {
        this(Info);
    }

    public CdpConsoleLogger(final CdpLogggerLevel loggerLevel) {
        this.loggerLevel = loggerLevel;
    }

    @Override
    public void info(String message, Object... args) {
        if (Info == loggerLevel || Debug == loggerLevel) {
            FormattingTuple tuple = arrayFormat(message, args);
            System.out.println("[INFO] " + tuple.getMessage());
        }
    }

    @Override
    public void debug(String message, Object... args) {
        if (Debug == loggerLevel) {
            FormattingTuple tuple = arrayFormat(message, args);
            System.out.println("[DEBUG] " + tuple.getMessage());
        }
    }

    @Override
    public void warn(String message, Object... args) {
        if (Info == loggerLevel || Warn == loggerLevel || Debug == loggerLevel) {
            FormattingTuple tuple = arrayFormat(message, args);
            System.out.println("[WARN] " + tuple.getMessage());
        }
    }

    @Override
    public void error(String message, Object... args) {
        if (Info == loggerLevel || Warn == loggerLevel || Error == loggerLevel || Debug == loggerLevel) {
            FormattingTuple tuple = arrayFormat(message, args);
            System.out.println("[ERROR] " + tuple.getMessage());
        }
    }

    @Override
    public void error(String message, Throwable t) {
        if (Info == loggerLevel || Warn == loggerLevel || Error == loggerLevel || Debug == loggerLevel) {
            System.err.println("[ERROR] " + message);
            if (t != null) {
                t.printStackTrace();
            }
        }
    }

    @Override
    public boolean isDebugEnabled() {
        return Debug == loggerLevel;
    }
}
