// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

public enum CdpLogggerLevel {
    Info,
    Debug,
    Warn,
    Error
}
