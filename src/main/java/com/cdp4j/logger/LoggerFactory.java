// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

public interface LoggerFactory {

    CdpLogger getLogger(String name);
}
