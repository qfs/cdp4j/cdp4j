// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

import com.cdp4j.Options;

public class CdpLoggerFactory implements LoggerFactory {

    private final CdpLoggerType loggerType;

    private final CdpLogggerLevel consoleLoggerLevel;

    private final String loggerNamePrefix;

    private static CdpLogger NULL_LOGGER = new CdpLogger() {

        @Override
        public void info(String message, Object... args) {}

        @Override
        public void debug(String message, Object... args) {}

        @Override
        public void error(String message, Object... args) {}

        @Override
        public void warn(String message, Object... args) {}

        @Override
        public void error(String message, Throwable t) {}

        @Override
        public boolean isDebugEnabled() {
            return false;
        }
    };

    public CdpLoggerFactory(Options options) {
        this.loggerType = options.loggerType();
        this.consoleLoggerLevel = options.consoleLoggerLevel();
        this.loggerNamePrefix = options.loggerNamePrefix();
    }

    public CdpLoggerFactory(CdpLoggerType loggerType, CdpLogggerLevel consoleLogggerLevel, String loggerNamePrefix) {
        this.loggerType = loggerType;
        this.consoleLoggerLevel = consoleLogggerLevel;
        this.loggerNamePrefix = loggerNamePrefix;
    }

    @Override
    public CdpLogger getLogger(String name) {
        if (loggerNamePrefix != null) {
            name = loggerNamePrefix + name;
        }

        switch (loggerType) {
            case Slf4j:
                return new CdpSlf4jLogger(name);
            case Console:
                return new CdpConsoleLogger(consoleLoggerLevel);
            case Log4j:
                return new CdpLog4jLogger(name);
            case Log4j2:
                return new CdpLog4j2Logger(name);
            case Jul:
                return new CdpJulLogger(name);
            case Null:
            default:
                return NULL_LOGGER;
        }
    }
}
