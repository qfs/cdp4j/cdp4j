// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

import static org.apache.logging.log4j.LogManager.getLogger;

import org.apache.logging.log4j.Logger;

class CdpLog4j2Logger implements CdpLogger {

    private final Logger log;

    private final boolean debugEnabled;

    public CdpLog4j2Logger(String name) {
        this.log = getLogger(name);
        debugEnabled = log.isDebugEnabled();
    }

    @Override
    public void info(String message, Object... args) {
        if (log.isInfoEnabled()) {
            log.info(message, args);
        }
    }

    @Override
    public void debug(String message, Object... args) {
        if (log.isDebugEnabled()) {
            log.debug(message, args);
        }
    }

    @Override
    public void warn(String message, Object... args) {
        if (log.isWarnEnabled()) {
            log.warn(message, args);
        }
    }

    @Override
    public void error(String message, Object... args) {
        if (log.isErrorEnabled()) {
            log.error(message, args);
        }
    }

    @Override
    public void error(String message, Throwable t) {
        if (log.isErrorEnabled()) {
            log.error(message, t);
        }
    }

    @Override
    public boolean isDebugEnabled() {
        return debugEnabled;
    }
}
