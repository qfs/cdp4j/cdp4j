// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

public enum CdpLoggerType {
    Null,
    Slf4j,
    Console,
    Log4j,
    Log4j2,
    Jul
}
