// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

import static com.cdp4j.logger.MessageFormatter.arrayFormat;
import static org.apache.log4j.Level.ERROR;
import static org.apache.log4j.Level.WARN;

import org.apache.log4j.Logger;

class CdpLog4jLogger implements CdpLogger {

    private final Logger log;

    private final boolean isDebugEnabled;

    public CdpLog4jLogger(final String name) {
        log = Logger.getLogger(name);
        isDebugEnabled = log.isDebugEnabled();
    }

    @Override
    public void info(String message, Object... args) {
        if (log.isInfoEnabled()) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.info(tuple.getMessage());
        }
    }

    @Override
    public void debug(String message, Object... args) {
        if (log.isDebugEnabled()) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.debug(tuple.getMessage());
        }
    }

    @Override
    public void warn(String message, Object... args) {
        if (log.isEnabledFor(WARN)) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.log(WARN, tuple.getMessage());
        }
    }

    @Override
    public void error(String message, Object... args) {
        if (log.isEnabledFor(ERROR)) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.error(tuple.getMessage());
        }
    }

    @Override
    public void error(String message, Throwable t) {
        if (log.isEnabledFor(ERROR)) {
            log.error(message, t);
        }
    }

    @Override
    public boolean isDebugEnabled() {
        return isDebugEnabled;
    }
}
