// SPDX-License-Identifier: MIT
package com.cdp4j.logger;

import static com.cdp4j.logger.MessageFormatter.arrayFormat;
import static java.util.logging.Level.FINE;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Level.WARNING;

import java.util.logging.Logger;

class CdpJulLogger implements CdpLogger {

    private final Logger log;

    private final boolean isDebugEnabled;

    public CdpJulLogger(final String name) {
        log = Logger.getLogger(name);
        isDebugEnabled = log.isLoggable(FINE);
    }

    @Override
    public void info(String message, Object... args) {
        if (log.isLoggable(INFO)) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.info(tuple.getMessage());
        }
    }

    @Override
    public void debug(String message, Object... args) {
        if (log.isLoggable(FINE)) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.fine(tuple.getMessage());
        }
    }

    @Override
    public void warn(String message, Object... args) {
        if (log.isLoggable(WARNING)) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.warning(tuple.getMessage());
        }
    }

    @Override
    public void error(String message, Object... args) {
        if (log.isLoggable(SEVERE)) {
            FormattingTuple tuple = arrayFormat(message, args);
            log.severe(tuple.getMessage());
        }
    }

    @Override
    public void error(String message, Throwable t) {
        if (log.isLoggable(SEVERE)) {
            log.log(SEVERE, message, t);
        }
    }

    @Override
    public boolean isDebugEnabled() {
        return isDebugEnabled;
    }
}
