// SPDX-License-Identifier: MIT
package com.cdp4j;

import static java.io.File.pathSeparator;
import static java.lang.System.getProperty;
import static java.util.Locale.ENGLISH;

public interface Constant {

    static final boolean JAVA_8 = getProperty("java.version", "").startsWith("1.8.");

    static final String OS_NAME = getProperty("os.name").toLowerCase(ENGLISH);

    static final boolean WINDOWS = ";".equals(pathSeparator);

    static final boolean MACOS = OS_NAME.startsWith("mac");

    static final boolean MACOS_ARM = MACOS
            && getProperty("os.arch") != null
            && getProperty("os.arch").toLowerCase(ENGLISH).startsWith("aarch64");

    static final boolean LINUX = "linux".contains(OS_NAME);
}
