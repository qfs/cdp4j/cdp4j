// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.JsonLibrary.Jackson;
import static com.cdp4j.SelectorEngine.Native;
import static com.cdp4j.SelectorEngine.Playwright;
import static com.cdp4j.event.Events.RuntimeExecutionContextCreated;
import static com.cdp4j.event.Events.RuntimeExecutionContextDestroyed;
import static com.cdp4j.event.Events.TargetTargetCreated;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.System.lineSeparator;
import static java.util.Collections.unmodifiableMap;
import static java.util.Locale.ENGLISH;
import static java.util.stream.Collectors.joining;

import com.cdp4j.Options;
import com.cdp4j.ProtocolVersion;
import com.cdp4j.SelectorEngine;
import com.cdp4j.channel.Channel;
import com.cdp4j.channel.ChannelFactory;
import com.cdp4j.channel.Connection;
import com.cdp4j.channel.DevToolsConnection;
import com.cdp4j.command.Page;
import com.cdp4j.command.Target;
import com.cdp4j.event.Events;
import com.cdp4j.event.runtime.ExecutionContextCreated;
import com.cdp4j.event.runtime.ExecutionContextDestroyed;
import com.cdp4j.event.target.TargetCreated;
import com.cdp4j.exception.CdpException;
import com.cdp4j.listener.EventListener;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.CdpLoggerFactory;
import com.cdp4j.logger.CdpLoggerType;
import com.cdp4j.logger.LoggerFactory;
import com.cdp4j.serialization.GsonMapper;
import com.cdp4j.serialization.JacksonMapper;
import com.cdp4j.serialization.JsonMapper;
import com.cdp4j.type.target.TargetInfo;
import com.cdp4j.type.target.WindowState;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class SessionFactory implements AutoCloseable {

    private final LoggerFactory loggerFactory;

    private final Options options;

    private final Map<String, Session> sessions = new ConcurrentHashMap<>();

    private final List<String> browserContexts = new CopyOnWriteArrayList<>();

    private final AtomicBoolean closed = new AtomicBoolean(false);

    private final List<CloseListener> closeListeners = new CopyOnWriteArrayList<>();

    private final Channel channel;

    private final JsonMapper mapper;

    private volatile String browserTargetId;

    private volatile Session browserSession;

    private volatile Boolean headless;

    private volatile int majorVersion;

    private final boolean externalProcess;

    private static String playwrightEngine = "throw new Error('playwright-engine not available')";

    static {
        try (InputStream is = SessionFactory.class.getResourceAsStream("/META-INF/cdp4j-playwright-engine.js")) {
            if (is != null) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                    playwrightEngine = reader.lines().collect(joining(lineSeparator()));
                }
            }
        } catch (Exception e) {
            System.err.println("Could not setup playwright-engine: " + e.toString());
        }
    }

    private class BrowserSessionListener implements EventListener {

        private final SessionFactory sessionFactory;

        private BrowserSessionListener(SessionFactory sessionFactory) {
            this.sessionFactory = sessionFactory;
        }

        @Override
        public void onEvent(Events event, Object value) {
            if (TargetTargetCreated.equals(event)) {
                TargetCreated tc = (TargetCreated) value;
                TargetInfo ti = tc.getTargetInfo();
                if ("browser".equalsIgnoreCase(ti.getType())) {
                    sessionFactory.browserTargetId = ti.getTargetId();
                    sessionFactory.browserSession.removeEventEventListener(this);
                }
            }
        }
    }

    public SessionFactory(Options options, ChannelFactory channelFactory, Connection connection) {
        this(options, channelFactory, connection, true);
    }

    public SessionFactory(Options options, ChannelFactory channelFactory, Connection connection, boolean init) {
        this.options = options;
        this.loggerFactory = createLoggerFactory(options.loggerType());
        this.externalProcess = connection instanceof DevToolsConnection;
        MessageHandler handler = null;
        if (Jackson.equals(options.jsonLibrary())) {
            this.mapper = new JacksonMapper();
            handler = new JacksonMessageHandler(
                    this.mapper,
                    this,
                    options.workerThreadPool(),
                    options.eventHandlerThreadPool(),
                    loggerFactory.getLogger("cdp4j.ws.response"));
        } else {
            this.mapper = new GsonMapper();
            handler = new GsonMessageHandler(
                    this.mapper,
                    this,
                    options.workerThreadPool(),
                    options.eventHandlerThreadPool(),
                    loggerFactory.getLogger("cdp4j.ws.response"));
        }
        channel = channelFactory.createChannel(connection, this, handler);
        if (init) {
            connect();
        }
        if (options.createNewUserDataDir()) {
            addCloseListener(new UserProfileDirectoryCleaner(options));
        }
    }

    public void connect() {
        if (browserTargetId == null) {
            channel.connect();
            initBrowserSession();
        } else {
            throw new IllegalStateException();
        }
    }

    /**
     * Creates a new page
     *
     * @return this
     */
    public Session create() {
        return create(null, null);
    }

    /**
     * Creates a new page
     *
     * @param settings
     *
     * @return this
     */
    public Session create(SessionSettings settings) {
        return create(null, settings);
    }

    /**
     * Creates a new page
     *
     * @param browserContextId incognito browser context id
     *
     * @return this
     */
    public Session create(String browserContextId) {
        return create(browserContextId, null);
    }

    /**
     * Creates a new page
     *
     * @param browserContextId incognito browser context id
     * @param settings
     *
     * @return this
     */
    public Session create(String browserContextId, SessionSettings settings) {
        Session browserSession = getBrowserSession();
        Target target = browserSession.getCommand().getTarget();
        if (settings == null || TRUE.equals(settings.getReuseEmptyTab())) {
            // Try to use blank page on the first launch
            // Do not reuse the blank page if the headless mode is enabled or chrome started externally
            // If more than one clients try to reuse the same blank page, unexpected race conditions might occur.
            if (sessions.isEmpty() && browserContextId == null && options.headless() == false && !externalProcess) {
                TargetInfo blankPage = null;
                List<TargetInfo> targets = target.getTargets();
                if (targets != null) {
                    for (TargetInfo next : targets) {
                        if (isEmptyTarget(next)) {
                            blankPage = next;
                            break;
                        }
                    }
                }
                if (blankPage != null) {
                    return connect(
                            blankPage.getTargetId(),
                            blankPage.getBrowserContextId(),
                            null,
                            true,
                            settings != null ? settings.isMultiFrameMode() : FALSE,
                            settings != null ? settings.getSelectorEngine() : options.selectorEngine());
                }
            }
        }
        Integer left = settings != null ? settings.getScreenLeft() : null;
        Integer top = settings != null ? settings.getScreenWidth() : null;
        int width = settings != null ? settings.getScreenWidth() : options.screenWidth();
        int height = settings != null ? settings.getScreenHeight() : options.screenHeight();
        Boolean multiFrameMode = settings != null ? settings.isMultiFrameMode() : FALSE;
        WindowState windowState = settings != null ? settings.getWindowState() : null;
        String targetId = target.createTarget(
                "about:blank", left, top, width, height, windowState, browserContextId, false, null, null, null);
        if (targetId == null) {
            throw new CdpException("Couldn't create a new session");
        }
        return connect(
                targetId,
                browserContextId,
                null,
                true,
                multiFrameMode,
                settings != null ? settings.getSelectorEngine() : options.selectorEngine());
    }

    /**
     * Connect to existing session
     *
     * @return this
     */
    public Session connect(String targetId) {
        return connect(targetId, null);
    }

    private boolean isEmptyTarget(TargetInfo targetInfo) {
        String url = targetInfo.getUrl();
        String type = targetInfo.getType();
        if ("page".equals(type)
                && (url.isEmpty()
                        || "about:blank".equals(url)
                        || "chrome://welcome/".equals(url)
                        || "chrome://newtab/".equals(url)
                        || "edge://newtab/".equals(url)
                        || "edge://welcome/".equals(url)
                        || url.startsWith("chrome://welcome-win10"))) {
            return true;
        }
        return false;
    }

    public Session connect(String targetId, String browserContextId) {
        return connect(targetId, browserContextId, null);
    }

    public Session connect(String targetId, String browserContextId, String sessionId) {
        return connect(targetId, browserContextId, sessionId, true);
    }

    public Session connect(String targetId, String browserContextId, String sessionId, boolean isPage) {
        return connect(targetId, browserContextId, sessionId, isPage, FALSE);
    }

    public Session connect(
            String targetId, String browserContextId, String sessionId, boolean isPage, Boolean multiFrameMode) {
        return connect(targetId, browserContextId, sessionId, isPage, multiFrameMode, options.selectorEngine());
    }

    public Session connect(
            String targetId,
            String browserContextId,
            String sessionId,
            boolean isPage,
            Boolean multiFrameMode,
            SelectorEngine selectorEngine) {
        return connect(targetId, browserContextId, sessionId, isPage, multiFrameMode, selectorEngine, true);
    }

    public Session connect(
            String targetId,
            String browserContextId,
            String sessionId,
            boolean isPage,
            Boolean multiFrameMode,
            SelectorEngine selectorEngine,
            boolean initSession) {
        Session bs = getBrowserSession();

        if (browserContextId == null) {
            TargetInfo found = null;
            List<TargetInfo> targets = bs.getCommand().getTarget().getTargets();
            for (TargetInfo next : targets) {
                if (next.getTargetId().equals(targetId)) {
                    found = next;
                }
            }
            if (found == null) {
                throw new CdpException("Target not found: " + targetId);
            }
            browserContextId = found.getBrowserContextId();
        }

        Target target = bs.getCommand().getTarget();
        if (sessionId == null) {
            sessionId = target.attachToTarget(targetId, TRUE);
        }

        Map<Integer, Context> contexts = new ConcurrentHashMap<>();
        List<EventListener> eventListeners = new CopyOnWriteArrayList<>();

        Session session = new Session(
                options,
                mapper,
                sessionId,
                targetId,
                browserContextId,
                channel,
                contexts,
                this,
                eventListeners,
                loggerFactory,
                multiFrameMode,
                selectorEngine != null ? selectorEngine : options.selectorEngine());
        sessions.put(sessionId, session);

        session.addEventListener((event, value) -> {
            if (RuntimeExecutionContextCreated.equals(event)) {
                ExecutionContextCreated ecc = (ExecutionContextCreated) value;
                String frameId = (String) ecc.getContext().getAuxData().get("frameId");
                Integer executionContextId = ecc.getContext().getId();
                if (targetId.equals(frameId)) {
                    session.setExecutionContextId(executionContextId);
                } else if (session.isMultiFrameMode()) {
                    CdpFrame frame = new CdpFrame(frameId, executionContextId);
                    session.addFrame(frame);
                }
            } else if (RuntimeExecutionContextDestroyed.equals(event)) {
                ExecutionContextDestroyed ecd = (ExecutionContextDestroyed) value;
                if (ecd.getExecutionContextId() != null
                        && ecd.getExecutionContextId().equals(session.getExecutionContextId())) {
                    session.setExecutionContextId(null);
                } else if (session.isMultiFrameMode() && ecd.getExecutionContextId() != null) {
                    session.removeFrame(ecd.getExecutionContextId());
                }
            }
        });

        if (initSession) {
            initNewSession(session, isPage);
        }

        return session;
    }

    public void initNewSession(final Session session, final boolean isPage) {
        session.getCommand().getRuntime().enable();

        if (isPage) {
            Command command = session.getCommand();
            Page page = command.getPage();

            page.enable();
            page.setLifecycleEventsEnabled(true);

            if (Playwright.equals(session.getSelectorEngine())) {
                session.getAsyncCommand()
                        .getPage()
                        .addScriptToEvaluateOnNewDocument(playwrightEngine)
                        .exceptionally(t -> {
                            loggerFactory
                                    .getLogger("cdp4j.playwright")
                                    .error("An error occurred while loading Playwright selector engine", t);
                            return null;
                        })
                        .thenAccept(f -> {
                            CdpLogger log = loggerFactory.getLogger("cdp4j.playwright");
                            if (log.isDebugEnabled()) {
                                log.debug("Playwright selector engine loaded successfully");
                            }
                        });
            }
        }
    }

    private void initBrowserSession() {
        Map<Integer, Context> contexts = new ConcurrentHashMap<>();
        List<EventListener> eventlisteners = new CopyOnWriteArrayList<>();
        browserSession = new Session(
                options,
                mapper,
                null,
                null,
                null,
                channel,
                contexts,
                this,
                eventlisteners,
                loggerFactory,
                FALSE,
                Native);

        if (options.protocolVersion() != ProtocolVersion.V8) {
            browserSession.addSyncEventListener(new TargetListener(sessions));
            Target target = browserSession.getCommand().getTarget();
            browserSession.addSyncEventListener(new BrowserSessionListener(this));
            target.setDiscoverTargets(TRUE);
        }
    }

    public Session getBrowserSession() {
        return browserSession;
    }

    public Map<String, Session> getSessions() {
        return unmodifiableMap(sessions);
    }

    Session getSession(String sessionId) {
        return sessions.get(sessionId);
    }

    void close(Session session) {
        if (!session.isDisposed() && options.protocolVersion() != ProtocolVersion.V8) {
            session.getCommand().getPage().close();
        }
        remove(session);
    }

    void remove(Session session) {
        session.dispose();
        final String sessionId = session.getId();
        if (sessionId != null) {
            sessions.remove(sessionId);
        }
    }

    /**
     * Get major version of the browser.
     *
     * @return major version number
     */
    public int getMajorVersion() {
        if (majorVersion == 0) {
            String[] product = browserSession
                    .getCommand()
                    .getBrowser()
                    .getVersion()
                    .getProduct()
                    .split("/");
            if (product.length == 2) {
                String[] version = product[1].split("\\.");
                if (version.length > 2) {
                    majorVersion = Integer.parseInt(version[0]);
                }
            }
        }
        return majorVersion;
    }

    /**
     * Terminates this instance of SessionFactory,
     * will also close all created browsers if they are still running.
     *
     * The factory object itself is considered disposed and cannot be used anymore.
     */
    @Override
    @SuppressWarnings("resource")
    public void close() {
        if (closed.compareAndSet(false, true)) {
            Target target = browserSession.getCommand().getTarget();
            if (channel.isOpen() && browserSession != null) {
                for (String next : browserContexts) {
                    target.disposeBrowserContext(next);
                }
                for (TargetInfo next : target.getTargets()) {
                    if (browserTargetId != null && browserTargetId.equals(next.getTargetId())) {
                        target.closeTarget(browserTargetId);
                        break;
                    }
                }
            }
            browserSession.dispose();
            channel.disconnect();
            for (Session session : sessions.values()) {
                session.dispose();
            }
            sessions.clear();
            browserContexts.clear();
            if (options.shutdownThreadPoolOnClose()) {
                Executor wp = options.workerThreadPool();
                if (wp instanceof ExecutorService) {
                    ExecutorService wps = (ExecutorService) wp;
                    if (!wps.isShutdown()) {
                        wps.shutdownNow();
                    }
                }
                Executor ep = options.eventHandlerThreadPool();
                if (ep instanceof ExecutorService) {
                    ExecutorService eps = (ExecutorService) ep;
                    if (!eps.isShutdown()) {
                        eps.shutdownNow();
                    }
                }
            }
            browserSession = null;
            for (CloseListener listener : closeListeners) {
                listener.closed();
            }
            closeListeners.clear();
        }
    }

    /**
     * Activate this browser window
     *
     * @param sessionId session identifier
     */
    public void activate(String sessionId) {
        Session session = sessions.get(sessionId);
        if (session != null) {
            browserSession.getCommand().getTarget().activateTarget(session.getTargetId());
        }
    }

    /**
     * Tests whether or not a Browser launched with headless argument.
     *
     * @return {@code true} if browser launched with headless argument.
     */
    public boolean isHeadless() {
        if (headless == null) {
            headless = getBrowserSession()
                    .getCommand()
                    .getBrowser()
                    .getVersion()
                    .getProduct()
                    .toLowerCase(ENGLISH)
                    .contains("headless");
        }
        return headless.booleanValue();
    }

    /**
     * Creates a new incognito browser context.
     *
     * This won't share cookies/cache with other browser contexts.
     */
    public String createBrowserContext() {
        String browserContextId = getBrowserSession().getCommand().getTarget().createBrowserContext();
        browserContexts.add(browserContextId);
        return browserContextId;
    }

    /**
     * Creates a new incognito browser context.
     *
     * @param proxyServer Proxy server, similar to the one passed to --proxy-server
     * @param proxyBypassList Proxy bypass list, similar to the one passed to --proxy-bypass-list
     *
     * This won't share cookies/cache with other browser contexts.
     */
    public String createBrowserContext(String proxyServer, String proxyBypassList) {
        String browserContextId = getBrowserSession()
                .getCommand()
                .getTarget()
                .createBrowserContext(null, proxyServer, proxyBypassList, null);
        browserContexts.add(browserContextId);
        return browserContextId;
    }

    /**
     * Dispose incoginto browser context.
     */
    public void disposeBrowserContext(final String browserContextId) {
        if (browserContexts.contains(browserContextId)) {
            getBrowserSession().getCommand().getTarget().disposeBrowserContext(browserContextId);
            browserContexts.remove(browserContextId);
        }
    }

    public boolean closed() {
        return closed.get();
    }

    protected LoggerFactory createLoggerFactory(CdpLoggerType loggerType) {
        return new CdpLoggerFactory(options);
    }

    public boolean addCloseListener(CloseListener listener) {
        if (listener == null) {
            return false;
        }
        if (closeListeners.contains(listener)) {
            return false;
        }
        return closeListeners.add(listener);
    }

    public boolean removeCloseListener(CloseListener listener) {
        return closeListeners.remove(listener);
    }

    public Options getOptions() {
        return options;
    }

    @Override
    public String toString() {
        return "SessionFactory [sessions=" + sessions + "]";
    }
}
