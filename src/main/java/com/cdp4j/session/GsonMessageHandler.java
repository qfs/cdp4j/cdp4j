// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Base64.getDecoder;
import static java.util.Collections.unmodifiableMap;

import com.cdp4j.event.Events;
import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.CommandException;
import com.cdp4j.listener.EventListener;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.serialization.JsonMapper;
import com.cdp4j.serialization.LoggingInputStream;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

final class GsonMessageHandler implements MessageHandler {

    enum InputType {
        String,
        ByteArray,
        InputStream
    }

    private static final Map<String, Events> EVENTS = listEvents();

    private final Gson gson;

    private final Executor workerThreadPool;

    private final Executor eventHandlerThreadPool;

    private final CdpLogger log;

    private final SessionFactory factory;

    GsonMessageHandler(
            final JsonMapper mapper,
            final SessionFactory factory,
            final Executor workerThreadPool,
            final Executor eventHandlerThreadPool,
            final CdpLogger log) {
        this.gson = (Gson) mapper.getMapper();
        this.factory = factory;
        this.workerThreadPool = workerThreadPool;
        this.eventHandlerThreadPool = eventHandlerThreadPool;
        this.log = log;
    }

    @Override
    public void process(final String content) {
        process(content, InputType.String);
    }

    @Override
    public void process(final byte[] content) {
        process(content, InputType.ByteArray);
    }

    @Override
    public void process(final InputStream is) {
        process(is, InputType.InputStream);
    }

    @SuppressWarnings("unchecked")
    protected void process(final Object content, InputType it) {
        Runnable runnable = () -> {
            JsonElement json = null;
            switch (it) {
                case ByteArray:
                    if (log.isDebugEnabled()) {
                        log.debug("<-- {}", new String((byte[]) content, UTF_8));
                    }
                    try (Reader reader = new InputStreamReader(new ByteArrayInputStream((byte[]) content), UTF_8)) {
                        json = gson.fromJson(reader, JsonElement.class);
                    } catch (IOException e) {
                        throw new CdpException(e);
                    }
                    break;
                case InputStream:
                    InputStream inputStream = (InputStream) content;
                    if (log.isDebugEnabled()) {
                        inputStream = new LoggingInputStream(inputStream, log, "<-- {}");
                    }
                    try (Reader reader = new InputStreamReader(inputStream, UTF_8)) {
                        json = gson.fromJson(reader, JsonElement.class);
                    } catch (IOException e) {
                        throw new CdpException(e);
                    }
                    break;
                default:
                case String:
                    if (log.isDebugEnabled()) {
                        log.debug("<-- {}", (String) content);
                    }
                    json = gson.fromJson((String) content, JsonElement.class);
                    break;
            }
            JsonObject object = json.getAsJsonObject();
            JsonElement idElement = object.get(FIELD_ID);
            if (idElement != null) {
                // Process command response
                Integer valId = Integer.valueOf(idElement.getAsInt());
                String sessionId = object.has(FIELD_SESSION_ID)
                        ? object.get(FIELD_SESSION_ID).getAsString()
                        : null;
                Session session = sessionId == null ? factory.getBrowserSession() : factory.getSession(sessionId);
                if (session == null) {
                    return;
                }
                Context context = session.pullContext(valId);
                if (context == null) {
                    return;
                }
                try {
                    JsonObject error = object.getAsJsonObject(FIELD_ERROR);
                    if (error == null) {
                        Object result = toJavaObject(context.getCommandReturnType(), json);
                        if (context.getPromise() != null) {
                            context.getPromise().complete(result);
                        } else {
                            context.setData(result);
                        }
                    } else {
                        int code = error.getAsJsonPrimitive(FIELD_CODE).getAsInt();
                        String message = error.getAsJsonPrimitive(FIELD_MESSAGE).getAsString();
                        JsonElement messageData = error.get(FIELD_DATA);
                        String errorMessage = context.getDomainCommand().domain.name() + "."
                                + context.getDomainCommand().command + " " + message
                                + (messageData != null && messageData.isJsonPrimitive()
                                        ? ". " + messageData.getAsString()
                                        : "");
                        context.setError(new CommandException(code, errorMessage));
                        if (context.getPromise() != null) {
                            log.error(errorMessage);
                            context.getPromise().completeExceptionally(context.getError());
                        }
                    }
                } finally {
                    if (context != null) {
                        context.release();
                    }
                    if (it == InputType.InputStream) {
                        try {
                            ((InputStream) content).close();
                        } catch (IOException e) {
                            log.warn(e.getMessage());
                        }
                    }
                }
            } else {
                // Process event response
                JsonElement method = object.get(FIELD_METHOD);
                if (method == null || !method.isJsonPrimitive()) {
                    return;
                }
                String eventName = method.getAsString();
                Events event = EVENTS.get(eventName);
                String sessionId = object.has(FIELD_SESSION_ID)
                        ? object.get(FIELD_SESSION_ID).getAsString()
                        : null;
                Session session = sessionId == null ? factory.getBrowserSession() : factory.getSession(sessionId);
                if (session != null
                        && !MANDATORY_EVENT_LISTENERS.contains(event)
                        && !session.getRegisteredEventListeners().contains(event)) {
                    // skip unregistered event
                    event = null;
                }
                if (event == null) {
                    return;
                }
                JsonElement params = object.get(FIELD_PARAMS);
                Object value = gson.fromJson(params, event.klass);
                if (session == null) {
                    return;
                }
                for (EventListener next : session.getSyncListeners()) {
                    next.onEvent(event, value);
                }
                final Events fEvent = event;
                eventHandlerThreadPool.execute(() -> {
                    for (EventListener next : session.getListeners()) {
                        next.onEvent(fEvent, value);
                    }
                });
            }
        };
        workerThreadPool.execute(runnable);
    }

    Object toJavaObject(CommandReturnType crt, JsonElement data) {
        Type returnType = crt.returnType;

        if (void.class.equals(returnType)) {
            return null;
        }

        if (!data.isJsonObject()) {
            throw new CdpException("Invalid JSON response");
        }

        final JsonObject object = data.getAsJsonObject();
        final JsonElement result = object.get("result");

        if (result == null || !result.isJsonObject()) {
            throw new CdpException("Invalid result value");
        }

        final JsonObject resultObject = result.getAsJsonObject();

        String returns = crt.returns;
        if (returns != null) {
            JsonElement jsonElement = resultObject.get(returns);
            if (jsonElement != null) {
                if (jsonElement.isJsonPrimitive()) {
                    if (String.class.equals(returnType)) {
                        return resultObject.get(returns).getAsString();
                    } else if (Boolean.class.equals(returnType)) {
                        return resultObject.get(returns).getAsBoolean() ? TRUE : FALSE;
                    } else if (Integer.class.equals(returnType)) {
                        return resultObject.get(returns).getAsInt();
                    } else if (Double.class.equals(returnType)) {
                        return resultObject.get(returns).getAsDouble();
                    }
                }
                if (byte[].class.equals(returnType)) {
                    String encoded = gson.fromJson(jsonElement, String.class);
                    if (encoded == null || encoded.trim().isEmpty()) {
                        return null;
                    } else {
                        return getDecoder().decode(encoded);
                    }
                }
            }
            if (List.class.equals(returnType)) {
                JsonArray jsonArray = jsonElement.getAsJsonArray();
                return gson.fromJson(jsonArray, crt.typeReference.getType());
            } else {
                return gson.fromJson(jsonElement, returnType);
            }
        } else {
            return gson.fromJson(resultObject, returnType);
        }
    }

    private static Map<String, Events> listEvents() {
        Events[] values = Events.values();
        Map<String, Events> map = new HashMap<>(values.length);
        for (Events next : values) {
            map.put(next.domain + "." + next.name, next);
        }
        return unmodifiableMap(map);
    }
}
