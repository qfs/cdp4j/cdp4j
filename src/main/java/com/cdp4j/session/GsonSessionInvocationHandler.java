// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.session.FutureUtils.orTimeout;
import static com.cdp4j.session.WaitingStrategy.Semaphore;
import static java.lang.Integer.valueOf;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import com.cdp4j.channel.Channel;
import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.CdpReadTimeoutException;
import com.cdp4j.exception.CdpReadTimeoutExceptionHandler;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.serialization.JsonMapper;
import com.cdp4j.serialization.ResponseParser;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

class GsonSessionInvocationHandler implements SessionInvocationHandler, HandlerConstants {

    private final AtomicInteger counter = new AtomicInteger(0);

    private final Gson gson;

    private final Channel channel;

    private final Map<Integer, Context> contexts;

    private final CdpLogger log;

    private final Session session;

    private final String sessionId;

    private int readTimeout;

    private final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler;

    private final WaitingStrategy waitingStrategy;

    private final Executor workerThreadPool;

    GsonSessionInvocationHandler(
            final JsonMapper mapper,
            final Channel channel,
            final Map<Integer, Context> contexts,
            final Session session,
            final CdpLogger log,
            final String sessionId,
            final int readTimeOut,
            final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler,
            final WaitingStrategy waitingStrategy,
            final Executor workerThreadPool) {
        this.gson = (Gson) mapper.getMapper();
        this.channel = channel;
        this.contexts = contexts;
        this.session = session;
        this.log = log;
        this.sessionId = sessionId;
        this.readTimeout = readTimeOut;
        this.readTimeoutExceptionHandler = readTimeoutExceptionHandler;
        this.waitingStrategy = waitingStrategy;
        this.workerThreadPool = workerThreadPool;
    }

    @Override
    public Object invoke(
            ParameterizedCommand<?> parameterizedCommand,
            DomainCommand command,
            CommandReturnType crt,
            String[] argNames,
            Object[] args,
            boolean sync) {
        if (!session.isConnected() || session.isDisposed()) {
            throw new CdpException(
                    session.isDisposed() ? "Session was disposed." : "WebSocket connection is not alive.");
        }
        final Integer id = valueOf(counter.incrementAndGet());

        final int readTimeout;
        final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler;
        if (parameterizedCommand == null) {
            readTimeout = this.readTimeout;
            readTimeoutExceptionHandler = this.readTimeoutExceptionHandler;
        } else {
            int commandReadTimeout = parameterizedCommand.getReadTimeout();
            commandReadTimeout = commandReadTimeout < 0 ? this.readTimeout : commandReadTimeout;
            readTimeout = (int) Math.round(commandReadTimeout * parameterizedCommand.getReadTimeoutFactor());
            final CdpReadTimeoutExceptionHandler commandReadTimeoutExceptionHandler =
                    parameterizedCommand.getReadTimeoutExceptionHandler();
            readTimeoutExceptionHandler = commandReadTimeoutExceptionHandler == null
                    ? this.readTimeoutExceptionHandler
                    : commandReadTimeoutExceptionHandler;
        }

        String json = toJson(command.method, id, argNames, args);
        if (log.isDebugEnabled()) {
            log.debug("--> {}", json);
        }

        CompletableFuture<Object> promise = null;
        final Context context = sync
                ? (Semaphore == waitingStrategy
                        ? new SemaphoreContext(command, crt, null)
                        : new ThreadContext(command, crt, null))
                : new PromiseContext(
                        command,
                        crt,
                        promise = orTimeout(
                                new CompletableFuture<>(),
                                readTimeout,
                                MILLISECONDS,
                                (timeout) -> {
                                    final CdpReadTimeoutException timeoutException =
                                            new CdpReadTimeoutException(timeout);
                                    timeoutException.setContextualData(command, crt, argNames, args, json);
                                    return FutureUtils.handleOrReturnException(
                                            timeoutException,
                                            readTimeoutExceptionHandler,
                                            CdpReadTimeoutException.class);
                                },
                                workerThreadPool,
                                () -> {
                                    if (session != null) {
                                        // executor that will complete the future
                                        // exceptionally after the timeout is reached
                                        Context c = session.pullContext(id);
                                        if (c != null) {
                                            c.release();
                                        }
                                    }
                                },
                                log,
                                command),
                        null);
        contexts.put(id, context);

        if (sync) {
            try {
                channel.sendText(json);
                context.await(readTimeout);
            } catch (CdpReadTimeoutException ex) {
                ex.setContextualData(command, crt, argNames, args, json);
                if (readTimeoutExceptionHandler != null) {
                    readTimeoutExceptionHandler.accept(ex);
                } else {
                    throw ex;
                }
            } finally {
                session.pullContext(id);
            }
        } else {
            channel.sendText(json);
            return promise;
        }

        if (context.getError() != null) {
            throw context.getError();
        }

        return context.getData();
    }

    String toJson(final String method, final Integer id, final String[] argNames, final Object[] argValues) {
        final JsonObject argsJson = new JsonObject();
        if (argValues.length > 0) {
            for (int i = 0; i < argValues.length; i++) {
                argsJson.add(argNames[i], gson.toJsonTree(argValues[i]));
            }
        }
        final JsonObject payload = new JsonObject();
        payload.add(FIELD_ID, new JsonPrimitive(id));
        if (sessionId != null) {
            payload.add(FIELD_SESSION_ID, new JsonPrimitive(sessionId));
        }
        payload.add(FIELD_METHOD, new JsonPrimitive(method));
        if (argValues.length > 0) {
            payload.add(FIELD_PARAMS, argsJson);
        }
        return gson.toJson(payload);
    }

    @Override
    public void dispose() {
        for (Context context : contexts.values()) {
            try {
                context.release();
            } catch (Throwable t) {
                // ignore
            }
        }
        contexts.clear();
    }

    @Override
    public Object invokeWithCustomParser(
            ParameterizedCommand<?> parameterizedCommand,
            DomainCommand command,
            CommandReturnType crt,
            String[] argNames,
            Object[] args,
            boolean sync,
            ResponseParser parser) {
        throw new CdpException("not supported");
    }

    @Override
    public int getReadTimeout() {
        return this.readTimeout;
    }

    @Override
    public void setReadTimeout(final int readTimeout) {
        if (readTimeout < 0) {
            throw new IllegalArgumentException("Read timeout must be >= 0");
        }

        this.readTimeout = readTimeout;
    }
}
