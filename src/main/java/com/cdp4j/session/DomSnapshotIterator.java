// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import com.cdp4j.type.domsnapshot.DOMNode;
import com.cdp4j.type.domsnapshot.NameValue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DomSnapshotIterator {

    private static final short ELEMENT_NODE = 1;
    private static final short TEXT_NODE = 3;
    private static final short DOCUMENT_NODE = 9;

    // https://www.w3.org/TR/2012/WD-html-markup-20121025/syntax.html#syntax-elements
    private static final Set<String> VOID_ELEMENTS = new HashSet<>(asList(
            "AREA",
            "BASE",
            "BASEFONT",
            "BGSOUND",
            "BR",
            "COL",
            "COMMAND",
            "EMBED",
            "FRAME",
            "HR",
            "IMAGE",
            "IMG",
            "INPUT",
            "ISINDEX",
            "KEYGEN",
            "LINK",
            "MENUITEM",
            "META",
            "NEXTID",
            "PARAM",
            "SOURCE",
            "TRACK",
            "WBR"));

    // DOMSnapshot.getSnapshot() returns flatten list.
    // Convert the flatten list to tree and return the html string.
    public static List<DocumentSnapshot> toHtml(List<DOMNode> nodes) {
        if (nodes.isEmpty()) {
            return emptyList();
        }
        List<DocumentSnapshot> documents = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            DOMNode node = nodes.get(i);
            if (node.getNodeType() == DOCUMENT_NODE) {
                StringBuilder builder = new StringBuilder(2048);
                iterate(nodes, node, builder);
                DocumentSnapshot snapshot = new DocumentSnapshot(
                        builder.toString(), node.getBaseURL(), node.getDocumentURL(), node.getFrameId());
                documents.add(snapshot);
            }
        }
        return documents;
    }

    private static void iterate(List<DOMNode> nodes, DOMNode next, StringBuilder builder) {
        String nodeName = next.getNodeName();
        Integer nodeType = next.getNodeType();
        if (nodeType == ELEMENT_NODE || next.getNodeType() == DOCUMENT_NODE) {
            if (next.getNodeType() == ELEMENT_NODE && !nodeName.startsWith("::")) {
                builder.append("<" + nodeName);
                List<NameValue> attributes = next.getAttributes();
                if (attributes.isEmpty()) {
                    builder.append(">");
                } else {
                    for (NameValue nv : attributes) {
                        builder.append(" " + nv.getName());
                        if (nv.getValue() != null && !nv.getValue().isEmpty()) {
                            builder.append("=\"" + nv.getValue() + "\"");
                        }
                    }
                    builder.append(">");
                }
            }
            for (Integer childIndex : next.getChildNodeIndexes()) {
                DOMNode child = nodes.get(childIndex);
                iterate(nodes, child, builder);
            }
            if (ELEMENT_NODE == nodeType && !VOID_ELEMENTS.contains(nodeName) && !nodeName.startsWith("::")) {
                builder.append("</" + nodeName + ">");
            }
        } else if (next.getNodeType() == TEXT_NODE) {
            builder.append(next.getNodeValue());
        }
    }
}
