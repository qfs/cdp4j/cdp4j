// SPDX-License-Identifier: MIT
package com.cdp4j.session;

class CdpFrame {

    private final String id;

    private final Integer executionContextId;

    public CdpFrame(String id) {
        this(id, 0);
    }

    public CdpFrame(String frameId, Integer executionContextId) {
        this.id = frameId;
        this.executionContextId = executionContextId;
    }

    public String getId() {
        return id;
    }

    public Integer getExecutionContextId() {
        return executionContextId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        CdpFrame other = (CdpFrame) obj;
        if (id == null) {
            if (other.id != null) return false;
        } else if (!id.equals(other.id)) return false;
        return true;
    }

    @Override
    public String toString() {
        return "CdpFrame [id=" + id + ", executionContextId=" + executionContextId + "]";
    }
}
