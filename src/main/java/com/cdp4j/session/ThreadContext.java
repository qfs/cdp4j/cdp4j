// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.currentThread;
import static java.util.concurrent.locks.LockSupport.parkUntil;
import static java.util.concurrent.locks.LockSupport.unpark;

import com.cdp4j.exception.CdpReadInterruptionException;
import com.cdp4j.exception.CdpReadTimeoutException;
import com.cdp4j.exception.CommandException;
import com.cdp4j.serialization.ResponseParser;
import java.util.concurrent.CompletableFuture;

class ThreadContext implements Context {

    private Object data;

    private CommandException error;

    private final Thread thread;

    private final DomainCommand domainCommand;

    private final CommandReturnType commandReturnType;

    private final long start;

    private final ResponseParser responseParser;

    public ThreadContext(
            DomainCommand domainCommand, CommandReturnType commandReturnType, ResponseParser responseParser) {
        thread = currentThread();
        this.domainCommand = domainCommand;
        this.commandReturnType = commandReturnType;
        this.responseParser = responseParser;
        this.start = currentTimeMillis();
    }

    @Override
    public void await(final int timeout) {
        parkUntil(this.start + timeout);
        if (data == null && error == null && (currentTimeMillis() - start) >= timeout) {
            if (Thread.currentThread().isInterrupted()) {
                throw new CdpReadInterruptionException(timeout);
            } else {
                throw new CdpReadTimeoutException(timeout);
            }
        }
    }

    @Override
    public void setData(final Object data) {
        this.data = data;
    }

    @Override
    public Object getData() {
        return data;
    }

    @Override
    public void setError(final CommandException error) {
        this.error = error;
    }

    @Override
    public CommandException getError() {
        return error;
    }

    @Override
    public void release() {
        unpark(thread);
    }

    @Override
    public DomainCommand getDomainCommand() {
        return domainCommand;
    }

    @Override
    public CommandReturnType getCommandReturnType() {
        return commandReturnType;
    }

    @Override
    public CompletableFuture<Object> getPromise() {
        return null;
    }

    @Override
    public ResponseParser getResponseParser() {
        return responseParser;
    }
}
