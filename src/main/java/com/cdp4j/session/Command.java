// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.command.Accessibility;
import com.cdp4j.command.Animation;
import com.cdp4j.command.Audits;
import com.cdp4j.command.Autofill;
import com.cdp4j.command.BackgroundService;
import com.cdp4j.command.BluetoothEmulation;
import com.cdp4j.command.Browser;
import com.cdp4j.command.CSS;
import com.cdp4j.command.CacheStorage;
import com.cdp4j.command.Cast;
import com.cdp4j.command.CommandFactory;
import com.cdp4j.command.Console;
import com.cdp4j.command.CustomCommand;
import com.cdp4j.command.DOM;
import com.cdp4j.command.DOMDebugger;
import com.cdp4j.command.DOMSnapshot;
import com.cdp4j.command.DOMStorage;
import com.cdp4j.command.Debugger;
import com.cdp4j.command.DeviceAccess;
import com.cdp4j.command.DeviceOrientation;
import com.cdp4j.command.Emulation;
import com.cdp4j.command.EventBreakpoints;
import com.cdp4j.command.Extensions;
import com.cdp4j.command.FedCm;
import com.cdp4j.command.Fetch;
import com.cdp4j.command.FileSystem;
import com.cdp4j.command.HeadlessExperimental;
import com.cdp4j.command.HeapProfiler;
import com.cdp4j.command.IO;
import com.cdp4j.command.IndexedDB;
import com.cdp4j.command.Input;
import com.cdp4j.command.Inspector;
import com.cdp4j.command.LayerTree;
import com.cdp4j.command.Log;
import com.cdp4j.command.Media;
import com.cdp4j.command.Memory;
import com.cdp4j.command.Network;
import com.cdp4j.command.Overlay;
import com.cdp4j.command.PWA;
import com.cdp4j.command.Page;
import com.cdp4j.command.Performance;
import com.cdp4j.command.PerformanceTimeline;
import com.cdp4j.command.Preload;
import com.cdp4j.command.Profiler;
import com.cdp4j.command.Runtime;
import com.cdp4j.command.Schema;
import com.cdp4j.command.Security;
import com.cdp4j.command.ServiceWorker;
import com.cdp4j.command.Storage;
import com.cdp4j.command.SystemInfo;
import com.cdp4j.command.Target;
import com.cdp4j.command.Tethering;
import com.cdp4j.command.Tracing;
import com.cdp4j.command.WebAudio;
import com.cdp4j.command.WebAuthn;

public class Command {
    private Accessibility accessibility;

    private Animation animation;

    private Audits audits;

    private Autofill autofill;

    private BackgroundService backgroundService;

    private BluetoothEmulation bluetoothEmulation;

    private Browser browser;

    private CSS css;

    private CacheStorage cacheStorage;

    private Cast cast;

    private Console console;

    private CustomCommand customCommand;

    private DOM dom;

    private DOMDebugger domDebugger;

    private DOMSnapshot domSnapshot;

    private DOMStorage domStorage;

    private Debugger debugger;

    private DeviceAccess deviceAccess;

    private DeviceOrientation deviceOrientation;

    private Emulation emulation;

    private EventBreakpoints eventBreakpoints;

    private Extensions extensions;

    private FedCm fedCm;

    private Fetch fetch;

    private FileSystem fileSystem;

    private HeadlessExperimental headlessExperimental;

    private HeapProfiler heapProfiler;

    private IO io;

    private IndexedDB indexedDB;

    private Input input;

    private Inspector inspector;

    private LayerTree layerTree;

    private Log log;

    private Media media;

    private Memory memory;

    private Network network;

    private Overlay overlay;

    private PWA pwa;

    private Page page;

    private Performance performance;

    private PerformanceTimeline performanceTimeline;

    private Preload preload;

    private Profiler profiler;

    private Runtime runtime;

    private Schema schema;

    private Security security;

    private ServiceWorker serviceWorker;

    private Storage storage;

    private SystemInfo systemInfo;

    private Target target;

    private Tethering tethering;

    private Tracing tracing;

    private WebAudio webAudio;

    private WebAuthn webAuthn;

    private final CommandFactory delegate;

    public Command(final SessionInvocationHandler handler) {
        this.delegate = new CommandFactory(handler);
    }

    public Accessibility getAccessibility() {
        if (accessibility == null) {
            return accessibility = delegate.getAccessibility();
        }
        return accessibility;
    }

    public Animation getAnimation() {
        if (animation == null) {
            return animation = delegate.getAnimation();
        }
        return animation;
    }

    public Audits getAudits() {
        if (audits == null) {
            return audits = delegate.getAudits();
        }
        return audits;
    }

    public Autofill getAutofill() {
        if (autofill == null) {
            return autofill = delegate.getAutofill();
        }
        return autofill;
    }

    public BackgroundService getBackgroundService() {
        if (backgroundService == null) {
            return backgroundService = delegate.getBackgroundService();
        }
        return backgroundService;
    }

    public BluetoothEmulation getBluetoothEmulation() {
        if (bluetoothEmulation == null) {
            return bluetoothEmulation = delegate.getBluetoothEmulation();
        }
        return bluetoothEmulation;
    }

    public Browser getBrowser() {
        if (browser == null) {
            return browser = delegate.getBrowser();
        }
        return browser;
    }

    public CSS getCSS() {
        if (css == null) {
            return css = delegate.getCSS();
        }
        return css;
    }

    public CacheStorage getCacheStorage() {
        if (cacheStorage == null) {
            return cacheStorage = delegate.getCacheStorage();
        }
        return cacheStorage;
    }

    public Cast getCast() {
        if (cast == null) {
            return cast = delegate.getCast();
        }
        return cast;
    }

    public Console getConsole() {
        if (console == null) {
            return console = delegate.getConsole();
        }
        return console;
    }

    public CustomCommand getCustomCommand() {
        if (customCommand == null) {
            return customCommand = delegate.getCustomCommand();
        }
        return customCommand;
    }

    public DOM getDOM() {
        if (dom == null) {
            return dom = delegate.getDOM();
        }
        return dom;
    }

    public DOMDebugger getDOMDebugger() {
        if (domDebugger == null) {
            return domDebugger = delegate.getDOMDebugger();
        }
        return domDebugger;
    }

    public DOMSnapshot getDOMSnapshot() {
        if (domSnapshot == null) {
            return domSnapshot = delegate.getDOMSnapshot();
        }
        return domSnapshot;
    }

    public DOMStorage getDOMStorage() {
        if (domStorage == null) {
            return domStorage = delegate.getDOMStorage();
        }
        return domStorage;
    }

    public Debugger getDebugger() {
        if (debugger == null) {
            return debugger = delegate.getDebugger();
        }
        return debugger;
    }

    public DeviceAccess getDeviceAccess() {
        if (deviceAccess == null) {
            return deviceAccess = delegate.getDeviceAccess();
        }
        return deviceAccess;
    }

    public DeviceOrientation getDeviceOrientation() {
        if (deviceOrientation == null) {
            return deviceOrientation = delegate.getDeviceOrientation();
        }
        return deviceOrientation;
    }

    public Emulation getEmulation() {
        if (emulation == null) {
            return emulation = delegate.getEmulation();
        }
        return emulation;
    }

    public EventBreakpoints getEventBreakpoints() {
        if (eventBreakpoints == null) {
            return eventBreakpoints = delegate.getEventBreakpoints();
        }
        return eventBreakpoints;
    }

    public Extensions getExtensions() {
        if (extensions == null) {
            return extensions = delegate.getExtensions();
        }
        return extensions;
    }

    public FedCm getFedCm() {
        if (fedCm == null) {
            return fedCm = delegate.getFedCm();
        }
        return fedCm;
    }

    public Fetch getFetch() {
        if (fetch == null) {
            return fetch = delegate.getFetch();
        }
        return fetch;
    }

    public FileSystem getFileSystem() {
        if (fileSystem == null) {
            return fileSystem = delegate.getFileSystem();
        }
        return fileSystem;
    }

    public HeadlessExperimental getHeadlessExperimental() {
        if (headlessExperimental == null) {
            return headlessExperimental = delegate.getHeadlessExperimental();
        }
        return headlessExperimental;
    }

    public HeapProfiler getHeapProfiler() {
        if (heapProfiler == null) {
            return heapProfiler = delegate.getHeapProfiler();
        }
        return heapProfiler;
    }

    public IO getIO() {
        if (io == null) {
            return io = delegate.getIO();
        }
        return io;
    }

    public IndexedDB getIndexedDB() {
        if (indexedDB == null) {
            return indexedDB = delegate.getIndexedDB();
        }
        return indexedDB;
    }

    public Input getInput() {
        if (input == null) {
            return input = delegate.getInput();
        }
        return input;
    }

    public Inspector getInspector() {
        if (inspector == null) {
            return inspector = delegate.getInspector();
        }
        return inspector;
    }

    public LayerTree getLayerTree() {
        if (layerTree == null) {
            return layerTree = delegate.getLayerTree();
        }
        return layerTree;
    }

    public Log getLog() {
        if (log == null) {
            return log = delegate.getLog();
        }
        return log;
    }

    public Media getMedia() {
        if (media == null) {
            return media = delegate.getMedia();
        }
        return media;
    }

    public Memory getMemory() {
        if (memory == null) {
            return memory = delegate.getMemory();
        }
        return memory;
    }

    public Network getNetwork() {
        if (network == null) {
            return network = delegate.getNetwork();
        }
        return network;
    }

    public Overlay getOverlay() {
        if (overlay == null) {
            return overlay = delegate.getOverlay();
        }
        return overlay;
    }

    public PWA getPWA() {
        if (pwa == null) {
            return pwa = delegate.getPWA();
        }
        return pwa;
    }

    public Page getPage() {
        if (page == null) {
            return page = delegate.getPage();
        }
        return page;
    }

    public Performance getPerformance() {
        if (performance == null) {
            return performance = delegate.getPerformance();
        }
        return performance;
    }

    public PerformanceTimeline getPerformanceTimeline() {
        if (performanceTimeline == null) {
            return performanceTimeline = delegate.getPerformanceTimeline();
        }
        return performanceTimeline;
    }

    public Preload getPreload() {
        if (preload == null) {
            return preload = delegate.getPreload();
        }
        return preload;
    }

    public Profiler getProfiler() {
        if (profiler == null) {
            return profiler = delegate.getProfiler();
        }
        return profiler;
    }

    public Runtime getRuntime() {
        if (runtime == null) {
            return runtime = delegate.getRuntime();
        }
        return runtime;
    }

    public Schema getSchema() {
        if (schema == null) {
            return schema = delegate.getSchema();
        }
        return schema;
    }

    public Security getSecurity() {
        if (security == null) {
            return security = delegate.getSecurity();
        }
        return security;
    }

    public ServiceWorker getServiceWorker() {
        if (serviceWorker == null) {
            return serviceWorker = delegate.getServiceWorker();
        }
        return serviceWorker;
    }

    public Storage getStorage() {
        if (storage == null) {
            return storage = delegate.getStorage();
        }
        return storage;
    }

    public SystemInfo getSystemInfo() {
        if (systemInfo == null) {
            return systemInfo = delegate.getSystemInfo();
        }
        return systemInfo;
    }

    public Target getTarget() {
        if (target == null) {
            return target = delegate.getTarget();
        }
        return target;
    }

    public Tethering getTethering() {
        if (tethering == null) {
            return tethering = delegate.getTethering();
        }
        return tethering;
    }

    public Tracing getTracing() {
        if (tracing == null) {
            return tracing = delegate.getTracing();
        }
        return tracing;
    }

    public WebAudio getWebAudio() {
        if (webAudio == null) {
            return webAudio = delegate.getWebAudio();
        }
        return webAudio;
    }

    public WebAuthn getWebAuthn() {
        if (webAuthn == null) {
            return webAuthn = delegate.getWebAuthn();
        }
        return webAuthn;
    }
}
