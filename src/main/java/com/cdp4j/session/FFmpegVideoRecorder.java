// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.event.Events.PageScreencastFrame;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.EncodingDone;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.EncodingError;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.ErrorPageEnable;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.ErrorStartScreencast;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedEncoding;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedPageEnabled;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedScreencast;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedStopped;
import static com.cdp4j.type.constant.ImageFormat.Jpeg;
import static java.lang.System.currentTimeMillis;
import static java.lang.System.lineSeparator;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.createFile;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.deleteIfExists;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.isDirectory;
import static java.nio.file.Files.isRegularFile;
import static java.nio.file.Files.walk;
import static java.nio.file.Files.write;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.util.Arrays.asList;
import static java.util.Base64.getDecoder;
import static java.util.Locale.ENGLISH;
import static java.util.concurrent.CompletableFuture.supplyAsync;

import com.cdp4j.command.PageAsync;
import com.cdp4j.event.Events;
import com.cdp4j.event.page.ScreencastFrame;
import com.cdp4j.exception.CdpException;
import com.cdp4j.listener.EventListener;
import com.cdp4j.logger.CdpLogger;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

class FFmpegVideoRecorder implements VideoRecorder, EventListener {

    private static final String LINE_SEPERATOR = lineSeparator();

    private static final String TEMP_PATH_PREFIX = "screencast-";

    private static final String INPUT_FILE_NAME = "input.txt";

    private static final String DEFAULT_FPS = "25";

    private static final int DEFAULT_NTH_FRAME = 1;

    private final Session session;

    private final VideoRecorderOptions options;

    private final CdpLogger log;

    private final PageAsync page;

    private final DeleteTempFileTask deleteTempFileTask;

    private final CloseListener sessionCloseListener;

    private final Path videoFile;

    private Path tempPath;

    private Path inputFile;

    private volatile String lastFrame;

    private volatile String lastFrameFileName;

    private volatile VideoRecorderState state;

    private volatile long lastFrameTimestamp;

    private final Lock encoderLock = new ReentrantLock();

    private final AtomicInteger frameCounter = new AtomicInteger();

    private static class DeleteTempFileTask implements Runnable {

        private final FFmpegVideoRecorder recorder;

        private AtomicBoolean executed = new AtomicBoolean(false);

        public DeleteTempFileTask(FFmpegVideoRecorder recorder) {
            this.recorder = recorder;
        }

        @Override
        public void run() {
            if (this.executed.get()) {
                return;
            }
            // ffmpeg is already executed, temp files will be deleted by encode() method
            if (recorder.state == InvokedEncoding) {
                return;
            }
            // something might be goes wrong, make sure that root temp folder is exist
            if (!isDirectory(recorder.tempPath) && exists(recorder.tempPath)) {
                return;
            }
            this.executed.set(true);
            // iterate all temp files
            try (Stream<Path> stream = walk(recorder.tempPath)) {
                Iterator<Path> iter = stream.iterator();
                while (iter.hasNext()) {
                    Path path = iter.next();
                    String fileName = path.getFileName().toString();
                    // delete all temp files except the video file
                    if (isRegularFile(path) && !fileName.equals(recorder.options.videoFileName())) {
                        try {
                            deleteIfExists(path);
                        } catch (Throwable t) {
                            // skip if we can't delete temp file
                            recorder.log.warn(t.getMessage(), t);
                        }
                    }
                }
            } catch (IOException e) {
                recorder.log.warn(e.getMessage(), e);
            }
            // video encoder is not executed
            // it's safe the delete temp directory
            if (recorder.state != InvokedEncoding) {
                if (isDirectory(recorder.tempPath) && exists(recorder.tempPath) && !exists(recorder.videoFile)) {
                    try {
                        deleteIfExists(recorder.tempPath);
                    } catch (IOException e) {
                        recorder.log.warn(e.getMessage(), e);
                    }
                }
            }
        }
    }

    FFmpegVideoRecorder(Session session, VideoRecorderOptions options, CdpLogger log) {
        this.session = session;
        this.options = options;
        this.log = log;
        page = session.getAsyncCommand().getPage();
        session.addEventListener(this);
        try {
            tempPath = createTempDirectory(TEMP_PATH_PREFIX);
            if (log.isDebugEnabled()) {
                log.debug(
                        "Temporary directory created: [{}]",
                        tempPath.toAbsolutePath().toString());
            }
            inputFile = tempPath.resolve(INPUT_FILE_NAME);
            createFile(inputFile);
        } catch (IOException e) {
            throw new CdpException(e);
        }
        this.videoFile = tempPath.resolve(options.videoFileName());
        this.deleteTempFileTask = new DeleteTempFileTask(this);
        this.sessionCloseListener = () -> deleteTempFileTask.run();
        session.addCloseListener(sessionCloseListener);
    }

    @Override
    public void start() {
        boolean startable = state == null || state == InvokedStopped;
        if (!startable) {
            throw new IllegalArgumentException("state=[" + state + "]");
        }
        page.enable()
                .thenAccept(rE -> {
                    state = InvokedPageEnabled;
                    page.startScreencast(Jpeg, options.imageQuality(), null, null, DEFAULT_NTH_FRAME)
                            .thenAccept(rC -> {
                                state = InvokedScreencast;
                            })
                            .exceptionally(t -> {
                                state = ErrorStartScreencast;
                                log.error(t.getMessage(), t);
                                session.removeEventEventListener(this);
                                session.removeCloseListener(sessionCloseListener);
                                return null;
                            });
                })
                .exceptionally(t -> {
                    state = ErrorPageEnable;
                    log.error(t.getMessage(), t);
                    session.removeEventEventListener(this);
                    session.removeCloseListener(sessionCloseListener);
                    return null;
                });
    }

    @Override
    public void stop() {
        state = InvokedStopped;
        page.stopScreencast().exceptionally(e -> {
            // This is not a critical problem
            // onEvents method already checks if screencast is stopped with state variable
            log.warn(e.getMessage(), e);
            return null;
        });
    }

    public VideoRecorderState getState() {
        return state;
    }

    @Override
    public void onEvent(Events event, Object value) {
        if (PageScreencastFrame != event) {
            return;
        }
        if (state == InvokedStopped || state == InvokedEncoding) {
            return;
        }
        final int count = frameCounter.incrementAndGet();
        final ScreencastFrame sc = (ScreencastFrame) value;
        final long frameTimestamp = (long) (sc.getMetadata().getTimestamp().doubleValue() * 1000);
        page.screencastFrameAck(sc.getSessionId()).exceptionally(t -> {
            log.warn(t.getMessage(), t);
            return null;
        });
        processFrame(sc.getData(), frameTimestamp, count, false);
    }

    protected void processFrame(final String frame, final long frameTimestamp, final int count, final boolean done) {
        encoderLock.lock();
        try {
            double frameDuration = (frameTimestamp - lastFrameTimestamp) / 1000D; // seconds format: 0.00, 1.25 vs...
            if (lastFrame != null) {
                byte[] decoded = null;
                try {
                    decoded = getDecoder().decode(lastFrame);
                } catch (IllegalArgumentException e) {
                    log.error(e.getMessage(), e);
                    return;
                }
                Path imageFile = tempPath.resolve(lastFrameFileName);
                try {
                    write(imageFile, decoded);
                    String lineFileName = "file '" + lastFrameFileName + "'";
                    String durationLine = "duration " + frameDuration;
                    String line = frameDuration > 0
                            ? lineFileName + LINE_SEPERATOR + durationLine + LINE_SEPERATOR
                            : lineFileName + LINE_SEPERATOR;
                    if (done) {
                        // Due to a quirk, the last image has to be specified twice
                        // the 2nd time without any duration directive
                        //
                        // https://trac.ffmpeg.org/wiki/Slideshow
                        line += lineFileName;
                    }
                    write(inputFile, line.getBytes(UTF_8), APPEND);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if (!done) {
                lastFrame = frame;
                lastFrameTimestamp = frameTimestamp;
                lastFrameFileName = "image-" + count + ".jpg";
            } else {
                lastFrame = null;
                lastFrameFileName = null;
            }
        } finally {
            encoderLock.unlock();
        }
    }

    public CompletableFuture<VideoRecorderResult> encodeAsync() {
        state = InvokedEncoding;
        if (options.encoderThreadPool() == null) {
            return supplyAsync(() -> encode());
        } else {
            return supplyAsync(() -> encode(), options.encoderThreadPool());
        }
    }

    @Override
    public VideoRecorderResult encode() {
        if (state == null) {
            throw new IllegalStateException("Invoke VideoRecorder.start() before video encoding");
        }
        state = InvokedEncoding;
        if (lastFrame != null) {
            processFrame(lastFrame, currentTimeMillis(), 0, true);
        }
        Process process = null;
        try {
            if (log.isDebugEnabled()) {
                log.debug("FFmpeg executable path: [{}]", options.ffmpegExecutable());
            }
            if (options.ffmpegExecutable() == null) {
                throw new CdpException("ffmpeg executable not found");
            }
            final List<String> encodingArgs = options.ffmpegArgs().isEmpty()
                    ? asList(new String[] { // uses default encoding (vp8)
                        "-avioflags",
                        "direct",
                        "-fpsprobesize",
                        "0",
                        "-probesize",
                        "32",
                        "-analyzeduration",
                        "0",
                        "-y",
                        "-an",
                        "-r",
                        DEFAULT_FPS,
                        "-c:v",
                        "vp8",
                        "-qmin",
                        "0",
                        "-qmax",
                        "50",
                        "-crf",
                        "8",
                        "-deadline",
                        "realtime",
                        "-speed",
                        "8",
                        "-b:v",
                        "1M",
                        "-threads",
                        "1",
                    })
                    : options.ffmpegArgs(); // uses custom encoding (libx264 etc...)
            final List<String> args = new ArrayList<>(asList(new String[] {
                options.ffmpegExecutable(),
                "-f",
                "concat",
                "-c:v",
                "mjpeg",
                "-i",
                "input.txt",
                "-loglevel",
                options.encoderLogLevel().name().toLowerCase(ENGLISH)
            }));
            args.addAll(encodingArgs);
            args.add(options.videoFileName());
            ProcessBuilder builder = new ProcessBuilder(args);
            builder.directory(tempPath.toFile());
            process = builder.start();
            try (Scanner scanner = new Scanner(process.getErrorStream())) {
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    switch (options.encoderLogLevel()) {
                        case Debug:
                            log.debug("{}", line);
                            break;
                        case Error:
                            log.error("{}", line);
                            break;
                        case Info:
                            log.info("{}", line);
                            break;
                        case Warn:
                            log.warn("{}", line);
                            break;
                    }
                }
            }
            process.waitFor();
            boolean done = process.exitValue() == 0;
            if (done) {
                state = EncodingDone;
            } else {
                state = EncodingError;
            }
            return new VideoRecorderResult(
                    done, tempPath.resolve(options.videoFileName()).toAbsolutePath());
        } catch (Throwable e) {
            state = EncodingError;
            throw new CdpException(e);
        } finally {
            if (process != null) {
                try {
                    if (process.isAlive()) {
                        process.destroy();
                    }
                } catch (Throwable t) {
                    log.error(t.getMessage(), t);
                } finally {
                    this.deleteTempFileTask.run();
                }
            }
            if (session.isConnected()) {
                session.removeCloseListener(sessionCloseListener);
                session.removeEventEventListener(this);
            }
        }
    }
}
