// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;

import com.cdp4j.command.Runtime;
import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.JsPromiseException;
import com.cdp4j.type.constant.ObjectType;
import com.cdp4j.type.runtime.CallArgument;
import com.cdp4j.type.runtime.CallFunctionOnResult;
import com.cdp4j.type.runtime.EvaluateResult;
import com.cdp4j.type.runtime.RemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;

public interface JavaScript {

    /**
     * Evaluates JavaScript expression in global scope.
     *
     * @param expression JavaScript expression
     *
     * @return execution result
     */
    default Object evaluate(String expression) {
        Runtime runtime = getThis().getCommand().getRuntime();
        Integer contextId = getThis().getExecutionContextId();
        EvaluateResult result = runtime.evaluate(
                expression,
                null,
                null,
                null,
                contextId,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);
        if (result == null) {
            return null;
        }
        RemoteObject remoteObject = result.getResult();
        if (remoteObject == null) {
            return null;
        }
        String objectId = result.getResult().getObjectId();
        if (objectId != null) {
            getThis().releaseObject(objectId);
        }
        Object value = remoteObject.getValue();
        getThis().logExit("evaluate", expression, value);
        return value;
    }

    /**
     * Calls JavaScript function.
     *
     * <p>
     * Function must be declared at the global {@literal (window object)} scope.
     * You can use <strong>dot notation</strong> for function name.
     * </p>
     *
     * @param name function name
     */
    default void callFunction(String name) {
        callFunction(name, void.class, Constant.EMPTY_ARGS);
    }

    default <T> T callFunction(String name, Class<T> returnType) {
        return callFunction(name, returnType, Constant.EMPTY_ARGS);
    }

    /**
     * Calls JavaScript function.
     *
     * <p>
     * Function must be declared at the global {@literal (window object)} scope.
     * You can use <strong>dot notation</strong> for function name.
     * </p>
     *
     * @param name function name
     * @param returnType return type of function
     * @param arguments function arguments
     *
     * @return function result
     */
    @SuppressWarnings("unchecked")
    default <T> T callFunction(String name, Class<T> returnType, Object... arguments) {
        CallArgument objArgument = new CallArgument();
        objArgument.setValue(name);

        CallFunctionOnResult funcObj = getThis()
                .getCommand()
                .getRuntime()
                .callFunctionOn(
                        "function(functionName) { return functionName.split('.').reduce((o, i) => o[i], this); }",
                        null,
                        asList(objArgument),
                        FALSE,
                        FALSE,
                        FALSE,
                        FALSE,
                        FALSE,
                        getThis().getExecutionContextId(),
                        null,
                        null,
                        null,
                        null);

        if (funcObj.getExceptionDetails() != null
                && funcObj.getExceptionDetails().getException() != null) {
            getThis().releaseObject(funcObj.getExceptionDetails().getException().getObjectId());
            throw new CdpException(funcObj.getExceptionDetails().getException().getDescription());
        }

        if (ObjectType.Undefined == funcObj.getResult().getType()) {
            getThis().releaseObject(funcObj.getResult().getObjectId());
            throw new CdpException(format("Function [%s] is not defined", name));
        }

        StringJoiner argNames = new StringJoiner(", ");

        List<CallArgument> argsFunc = arguments != null ? new ArrayList<>(arguments.length) : null;

        int i = 0;
        if (arguments != null && arguments.length > 0) {
            for (Object argument : arguments) {
                CallArgument ca = new CallArgument();
                argsFunc.add(ca);
                if (argument != null) {
                    if (getThis().isPrimitive(argument.getClass())) {
                        ca.setValue(argument);
                    } else {
                        ca.setUnserializableValue(getThis().getJsonMapper().toJson(argument));
                    }
                }
                argNames.add("arg" + i++);
            }
        }

        CallFunctionOnResult func = getThis()
                .getCommand()
                .getRuntime()
                .callFunctionOn(
                        format(
                                "function(%s) { const result = this.apply(this, Array.prototype.slice.call(arguments)); "
                                        + "return typeof result === 'undefined' ? undefined : JSON.stringify({ result : result }); }",
                                argNames.toString()),
                        funcObj.getResult().getObjectId(),
                        argsFunc,
                        FALSE,
                        TRUE,
                        FALSE,
                        FALSE,
                        FALSE,
                        null,
                        null,
                        null,
                        null,
                        null);

        getThis().releaseObject(funcObj.getResult().getObjectId());
        getThis().releaseObject(func.getResult().getObjectId());

        if (func.getExceptionDetails() != null && func.getExceptionDetails().getException() != null) {
            getThis().releaseObject(func.getExceptionDetails().getException().getObjectId());
            throw new CdpException(func.getExceptionDetails().getException().getDescription());
        }

        Object value = null;
        if (ObjectType.String == func.getResult().getType() && !returnType.equals(void.class)) {
            String json = valueOf(func.getResult().getValue());
            value = getThis().getJsonMapper().fromJsonResult(json, returnType);
        } else if (ObjectType.Undefined == func.getResult().getType()) {
            value = void.class;
        }

        getThis()
                .logExit(
                        "callFunction",
                        "function name = " + name
                                + (arguments == null || arguments.length == 0
                                        ? ""
                                        : ", arguments = " + "(\""
                                                + stream(arguments)
                                                        .map(o -> valueOf(o))
                                                        .collect(joining("\", \"")) + "\")"),
                        valueOf(value).replace("\n", "").replace("\r", ""));

        return !void.class.equals(value) ? (T) value : null;
    }

    /**
     * Calls JavaScript Promise function.
     *
     * <p>
     * Function must be declared at the global {@literal (window object)} scope.
     * You can use <strong>dot notation</strong> for function name.
     * </p>
     *
     * @param name function name
     * @param returnType return type of function
     * @param arguments function arguments
     *
     * @return function result
     */
    @SuppressWarnings("unchecked")
    default <T, U> CompletableFuture<T> callPromise(String name, Class<T> returnType, Object... arguments) {
        CallArgument objArgument = new CallArgument();
        objArgument.setValue(name);

        CallFunctionOnResult funcObj = getThis()
                .getCommand()
                .getRuntime()
                .callFunctionOn(
                        "function(functionName) { return functionName.split('.').reduce((o, i) => o[i], this); }",
                        null,
                        asList(objArgument),
                        FALSE,
                        FALSE,
                        FALSE,
                        FALSE,
                        TRUE,
                        getThis().getExecutionContextId(),
                        null,
                        null,
                        null,
                        null);

        if (funcObj.getExceptionDetails() != null
                && funcObj.getExceptionDetails().getException() != null) {
            getThis().releaseObject(funcObj.getExceptionDetails().getException().getObjectId());
            throw new CdpException(funcObj.getExceptionDetails().getException().getDescription());
        }

        if (ObjectType.Undefined == funcObj.getResult().getType()) {
            getThis().releaseObject(funcObj.getResult().getObjectId());
            throw new CdpException(format("Function [%s] is not defined", name));
        }

        StringJoiner argNames = new StringJoiner(", ");

        List<CallArgument> argsFunc = arguments != null ? new ArrayList<>(arguments.length) : null;

        int i = 0;
        if (arguments != null && arguments.length > 0) {
            for (Object argument : arguments) {
                CallArgument ca = new CallArgument();
                argsFunc.add(ca);
                if (argument != null) {
                    if (getThis().isPrimitive(argument.getClass())) {
                        ca.setValue(argument);
                    } else {
                        ca.setUnserializableValue(getThis().getJsonMapper().toJson(argument));
                    }
                }
                argNames.add("arg" + i++);
            }
        }

        CompletableFuture<CallFunctionOnResult> future = getThis()
                .getAsyncCommand()
                .getRuntime()
                .callFunctionOn(
                        format(
                                "function(%s) { const result = this.apply(this, Array.prototype.slice.call(arguments)); "
                                        + "if ( ! (result instanceof Promise) ) { throw Error('Expected promise but return value is not a promise type [' + (typeof result) + ']'); } else { return result; } }",
                                argNames.toString()),
                        funcObj.getResult().getObjectId(),
                        argsFunc,
                        FALSE,
                        TRUE,
                        FALSE,
                        FALSE,
                        TRUE,
                        null,
                        null,
                        null,
                        null,
                        null);

        CompletableFuture<T> promise = new CompletableFuture<>();

        getThis().addCloseListener(() -> {
            if (!promise.isDone()) {
                promise.cancel(true);
            }
            if (!future.isDone()) {
                future.cancel(true);
            }
        });

        future.whenCompleteAsync((result, exception) -> {
            if (exception != null) {
                promise.completeExceptionally(exception);
                return;
            }
            getThis().releaseObject(funcObj.getResult().getObjectId());
            getThis().releaseObject(result.getResult().getObjectId());

            if (result.getExceptionDetails() != null
                    && result.getExceptionDetails().getException() != null) {
                getThis()
                        .releaseObject(
                                result.getExceptionDetails().getException().getObjectId());
                String msg = result.getExceptionDetails().getException().getDescription();
                if (result.getResult().getValue() != null) {
                    msg = String.valueOf(result.getResult().getValue());
                } else if (msg == null) {
                    msg = result.getExceptionDetails().getText();
                }
                if (!promise.isDone()) {
                    promise.completeExceptionally(new JsPromiseException(msg));
                }
                return;
            }

            Object value = null;
            ObjectType rt = result.getResult().getType();
            if (ObjectType.Object == rt) {
                try {
                    value = getThis()
                            .getJsonMapper()
                            .fromObject(result.getResult().getValue(), returnType);
                } catch (Throwable t) {
                    promise.completeExceptionally(t);
                    return;
                }
            } else if (ObjectType.Undefined == rt) {
                value = null;
            } else {
                value = result.getResult().getValue();
            }

            if (!promise.isDone()) {
                getThis()
                        .logExit(
                                "callPromise",
                                "function name = " + name
                                        + (arguments == null || arguments.length == 0
                                                ? ""
                                                : ", arguments = " + "(\""
                                                        + stream(arguments)
                                                                .map(o -> valueOf(o))
                                                                .collect(joining("\", \""))
                                                        + "\")"),
                                valueOf(value).replace("\n", "").replace("\r", ""));
                final Object fValue = value;
                promise.completeAsync(() -> (T) fValue);
            }
        });
        return promise;
    }

    /**
     * Gets JavaScript variable.
     *
     * <p>
     * Variable must be declared at the global {@literal (window object)} scope.
     * You can use <strong>dot notation</strong> for variable name.
     * </p>
     *
     * @param name variable name
     * @param returnType variable type
     *
     * @return variable value
     */
    @SuppressWarnings("unchecked")
    public default <T> T getVariable(String name, Class<T> returnType) {
        CallArgument objArgument = new CallArgument();
        objArgument.setValue(name);

        CallFunctionOnResult obj = getThis()
                .getCommand()
                .getRuntime()
                .callFunctionOn(
                        "function(functionName) { const result = functionName.split('.').reduce((o, i) => o[i], this); "
                                + "return typeof result === 'undefined' ? undefined : JSON.stringify({ result : result }); }",
                        null,
                        asList(objArgument),
                        FALSE,
                        FALSE,
                        FALSE,
                        FALSE,
                        FALSE,
                        getThis().getExecutionContextId(),
                        null,
                        null,
                        null,
                        null);

        if (obj.getExceptionDetails() != null && obj.getExceptionDetails().getException() != null) {
            getThis().releaseObject(obj.getExceptionDetails().getException().getObjectId());
            throw new CdpException(obj.getExceptionDetails().getException().getDescription());
        }

        if (ObjectType.Undefined == obj.getResult().getType()) {
            getThis().releaseObject(obj.getResult().getObjectId());
            throw new CdpException(format("Variable [%s] is not defined", name));
        }

        Object value = null;
        if (ObjectType.String == obj.getResult().getType() && !returnType.equals(void.class)) {
            String json = valueOf(obj.getResult().getValue());
            value = getThis().getJsonMapper().fromJsonResult(json, returnType);
        } else if (ObjectType.Undefined == obj.getResult().getType()) {
            value = void.class;
        }

        getThis().releaseObject(obj.getResult().getObjectId());

        return (T) value;
    }

    /**
     * Sets JavaScript variable.
     *
     * <p>
     * Variable must be declared at the global {@literal (window object)} scope.
     * You can use <strong>dot notation</strong> for variable name.
     * </p>
     *
     * @param name variable name
     * @param newValue value
     */
    public default void setVariable(String name, Object newValue) {
        EvaluateResult windowResult = getThis().getCommand().getRuntime().evaluate("window");

        if (windowResult == null) {
            return;
        }

        if (windowResult.getExceptionDetails() != null
                && windowResult.getExceptionDetails().getException() != null) {
            getThis()
                    .releaseObject(
                            windowResult.getExceptionDetails().getException().getObjectId());
            throw new CdpException(
                    windowResult.getExceptionDetails().getException().getDescription());
        }

        CallArgument argVariableName = new CallArgument();
        argVariableName.setValue(name);
        CallArgument argVariableValue = new CallArgument();
        if (newValue != null) {
            if (getThis().isPrimitive(newValue.getClass())) {
                argVariableValue.setValue(newValue);
            } else {
                argVariableValue.setValue(getThis().getJsonMapper().toJson(newValue));
            }
        }

        CallFunctionOnResult obj = getThis()
                .getCommand()
                .getRuntime()
                .callFunctionOn(
                        "function(is, value) { function index(obj, is, value) { if (typeof is == 'string') return index(obj, is.split('.'), value); "
                                + "else if (is.length === 1 && value !== undefined) return obj[is[0]] = value; else if (is.length === 0) "
                                + "return obj; else return index(obj[is[0]], is.slice(1), value); } index(window, is, value); }",
                        windowResult.getResult().getObjectId(),
                        asList(argVariableName, argVariableValue),
                        FALSE,
                        FALSE,
                        FALSE,
                        FALSE,
                        FALSE,
                        null,
                        null,
                        null,
                        null,
                        null);

        getThis().releaseObject(windowResult.getResult().getObjectId());

        if (obj.getExceptionDetails() != null && obj.getExceptionDetails().getException() != null) {
            getThis().releaseObject(obj.getExceptionDetails().getException().getObjectId());
            throw new CdpException(obj.getExceptionDetails().getException().getDescription());
        }

        getThis().releaseObject(obj.getResult().getObjectId());
    }

    public Session getThis();
}
