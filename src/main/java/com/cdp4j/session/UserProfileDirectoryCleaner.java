// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.walkFileTree;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.locks.LockSupport.parkNanos;

import com.cdp4j.Options;
import com.cdp4j.exception.CdpException;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.CdpLoggerFactory;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicBoolean;

class UserProfileDirectoryCleaner implements CloseListener {

    public class ProfileDirectoryVisitor extends SimpleFileVisitor<Path> {

        private final Path root;

        public ProfileDirectoryVisitor(Path root) {
            this.root = root;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            if (dir.startsWith(root)) {
                return CONTINUE;
            }
            return SKIP_SUBTREE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            delete(file);
            return CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            if (dir.startsWith(root)) {
                delete(dir);
            }
            return CONTINUE;
        }
    }

    private static final Path TEMP_DIRECTORY;

    private final AtomicBoolean executed = new AtomicBoolean(false);

    private final Path userProfileDirectory;

    private final CdpLogger log;

    private final int maxSleepTimeMs;

    static {
        try {
            Path dummyTempDir = createTempDirectory("cdp4j");
            TEMP_DIRECTORY = dummyTempDir.getParent();
            delete(dummyTempDir);
        } catch (IOException e) {
            throw new CdpException(e);
        }
    }

    public UserProfileDirectoryCleaner(Options options) {
        CdpLoggerFactory factory = new CdpLoggerFactory(options);
        log = factory.getLogger("cdp4j.user.profile.cleaner");
        Path userProfileDirectory = options.userDataDir();
        if (userProfileDirectory == null) {
            throw new IllegalArgumentException();
        }
        if (TEMP_DIRECTORY == null) {
            throw new IllegalStateException();
        }
        // This is very highly critical checkpoint.
        // Cleaner delete all files under the specified directory.
        // It might be dangerous to use arbitrary directory.
        // We make sure that that user profile directory created in temporary folder.
        if (!userProfileDirectory.startsWith(TEMP_DIRECTORY)) {
            throw new CdpException(
                    "User profile directory must be specifically located in default temporary folder which is:"
                            + TEMP_DIRECTORY.toString());
        }
        this.userProfileDirectory = userProfileDirectory;
        this.maxSleepTimeMs = options.userProfileCleanerMaxSleepTime();
    }

    private boolean clean() {
        if (executed.compareAndSet(false, true)) {
            log.info("trying to delete user profile directory: {}", userProfileDirectory.toString());
            if (!exists(userProfileDirectory)) {
                return true;
            }
            int totalSleepTimeMs = 0;
            boolean done = false;
            int delayMs = getSleepTime();
            long delayNanos = MILLISECONDS.toNanos(delayMs);
            while (!done && totalSleepTimeMs < maxSleepTimeMs) {
                try {
                    walkFileTree(userProfileDirectory, new ProfileDirectoryVisitor(userProfileDirectory));
                    done = true;
                } catch (IOException e) {
                    // Chrome process might still use files under user profile directory.
                    // This method retry's to delete files until the timeout occurs.
                    parkNanos(delayNanos);
                    totalSleepTimeMs += delayMs;
                }
            }
        }
        return !exists(userProfileDirectory);
    }

    private final int getSleepTime() {
        return 100; // 100 ms
    }

    @Override
    public void closed() {
        boolean done = clean();
        if (done) {
            log.info("User profile directory deleted: {}", userProfileDirectory.toString());
        }
    }
}
