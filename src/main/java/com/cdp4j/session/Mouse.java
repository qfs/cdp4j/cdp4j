// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.event.Events.InputDragIntercepted;
import static com.cdp4j.type.constant.DragEvent.DragEnter;
import static com.cdp4j.type.constant.DragEvent.DragOver;
import static com.cdp4j.type.constant.DragEvent.Drop;
import static com.cdp4j.type.constant.MouseEventType.MouseMoved;
import static com.cdp4j.type.constant.MouseEventType.MousePressed;
import static com.cdp4j.type.constant.MouseEventType.MouseReleased;
import static com.cdp4j.type.constant.PointerType.Mouse;
import static com.cdp4j.type.input.MouseButton.Left;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.SECONDS;

import com.cdp4j.command.Input;
import com.cdp4j.event.input.DragIntercepted;
import com.cdp4j.exception.CdpException;
import com.cdp4j.listener.EventListener;
import com.cdp4j.type.input.DragData;
import com.cdp4j.type.util.Point;
import java.util.concurrent.CountDownLatch;

/**
 * Interface representing basic mouse operations.
 */
public interface Mouse {

    /**
     * Click on the specified element.
     *
     * There are some preconditions for an element to be clicked.
     * The element must be visible and it must have a height and width greater then 0.
     *
     * @param selector css or xpath selector
     *
     * @return this
     */
    default Session click(final String selector) {
        return click(selector, Constant.EMPTY_ARGS);
    }

    /**
     * Click on the specified element.
     *
     * There are some preconditions for an element to be clicked. The element
     * must be visible and it must have a height and width greater then 0.
     *
     * @param selector
     *            css or xpath selector
     * @param args
     *            format string
     *
     * @return this
     */
    default Session click(final String selector, final Object... args) {
        getThis().logEntry("click", format(selector, args));
        String objectId = getThis().getObjectId(selector, args);
        Point point = null;
        try {
            getThis().scrollIntoViewIfNeededWithObjectId(selector, objectId, args);
            point = getThis().getClickablePointWithObjectId(selector, objectId, args);
        } finally {
            if (objectId != null) {
                getThis().releaseObject(objectId);
            }
        }
        move(point.x, point.y);
        down(point.x, point.y);
        up(point.x, point.y);
        return getThis();
    }

    /**
     * Dispatches a mousemove event.
     *
     * @param x X coordinate of the event relative to the main frame's viewport in CSS pixels.
     * @param y Y coordinate of the event relative to the main frame's viewport in CSS pixels. 0 refers to
     * the top of the viewport and Y increases as it proceeds towards the bottom of the viewport.
     *
     * @return this
     */
    default Session move(double x, double y) {
        Input input = getThis().getCommand().getInput();
        input.dispatchMouseEvent(
                MouseMoved, x, y, null, null, Left, null, null, null, null, null, null, null, null, null, Mouse);
        return getThis();
    }

    /**
     * Dispatches a mousedown event.
     *
     * @param x X coordinate of the event relative to the main frame's viewport in CSS pixels.
     * @param y Y coordinate of the event relative to the main frame's viewport in CSS pixels. 0 refers to
     * the top of the viewport and Y increases as it proceeds towards the bottom of the viewport.
     *
     * @return this
     */
    default Session down(double x, double y) {
        Input input = getThis().getCommand().getInput();
        Integer clickCount = 1;
        input.dispatchMouseEvent(
                MousePressed,
                x,
                y,
                null,
                null,
                Left,
                null,
                clickCount,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                Mouse);
        return getThis();
    }

    /**
     * Dispatches a mouseup event.
     *
     * @param x X coordinate of the event relative to the main frame's viewport in CSS pixels.
     * @param y Y coordinate of the event relative to the main frame's viewport in CSS pixels. 0 refers to
     * the top of the viewport and Y increases as it proceeds towards the bottom of the viewport.
     *
     * @return this
     */
    default Session up(double x, double y) {
        Input input = getThis().getCommand().getInput();
        Integer clickCount = 1;
        input.dispatchMouseEvent(
                MouseReleased,
                x,
                y,
                null,
                null,
                Left,
                null,
                clickCount,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                Mouse);
        return getThis();
    }

    /**
     * Dispatches a drag event.
     *
     * @param start starting point for drag
     * @param target point to drag to
     *
     * @return {@link DragData}
     */
    default DragData drag(Point start, Point target) {
        Input input = getThis().getCommand().getInput();
        input.setInterceptDrags(TRUE);
        DragData[] data = new DragData[1];
        CountDownLatch latch = new CountDownLatch(1);
        EventListener[] listener = new EventListener[1];
        try {
            listener[0] = (event, value) -> {
                if (InputDragIntercepted == event) {
                    data[0] = ((DragIntercepted) value).getData();
                    latch.countDown();
                }
            };
            getThis().addEventListener(listener[0]);
            move(start.x, start.y);
            down(start.x, start.y);
            move(target.x, target.y);
            latch.await(10, SECONDS);
        } catch (Throwable e) {
            latch.countDown();
            throw new CdpException(e);
        } finally {
            getThis().removeEventEventListener(listener[0]);
        }
        return data[0];
    }

    /**
     * Dispatches a dragenter event.
     *
     * @param target point for emitting dragenter event
     * @param data drag data containing items and operations mask
     *
     * @return this
     */
    default Session dragEnter(Point target, DragData data) {
        getThis().getCommand().getInput().dispatchDragEvent(DragEnter, target.x, target.y, data);
        return getThis();
    }

    /**
     * Dispatches a dragover event.
     *
     * @param target point for emitting dragover event
     * @param data drag data containing items and operations mask
     *
     * @return this
     */
    default Session dragOver(Point target, DragData data) {
        getThis().getCommand().getInput().dispatchDragEvent(DragOver, target.x, target.y, data);
        return getThis();
    }

    /**
     * Performs a dragenter, dragover, and drop in sequence.
     *
     * @param target point to drop on
     * @param data drag data containing items and operations mask
     *
     * @return this
     */
    default Session drop(Point target, DragData data) {
        getThis().getCommand().getInput().dispatchDragEvent(Drop, target.x, target.y, data);
        return getThis();
    }

    /**
     * Performs a drag, dragenter, dragover, and drop in sequence.
     *
     * @param start point to drag from
     * @param target point to drop on
     *
     * @return this
     */
    default Session dragAndDrop(Point start, Point target) {
        DragData data = drag(start, target);
        dragEnter(target, data);
        dragOver(target, data);
        drop(target, data);
        up(target.x, target.y);
        return getThis();
    }

    /**
     * Performs drag and drop
     *
     * @param draggable css or xpath selector of draggable element
     * @param droppable css or xpath selector of droppable element
     * @return
     */
    default Session dragAndDrop(String draggable, String droppable) {
        Point start = getThis().getClickablePoint(draggable);
        Point target = getThis().getClickablePoint(droppable);
        return dragAndDrop(start, target);
    }

    Session getThis();
}
