// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.serialization.ResponseParser;

public interface SessionInvocationHandler {

    Object invoke(
            ParameterizedCommand<?> parameterizedCommand,
            DomainCommand command,
            CommandReturnType crt,
            String[] argNames,
            Object[] args,
            boolean sync);

    Object invokeWithCustomParser(
            ParameterizedCommand<?> parameterizedCommand,
            DomainCommand command,
            CommandReturnType crt,
            String[] argNames,
            Object[] args,
            boolean sync,
            ResponseParser responseParser);

    void dispose();

    int getReadTimeout();

    void setReadTimeout(int readTimeout);
}
