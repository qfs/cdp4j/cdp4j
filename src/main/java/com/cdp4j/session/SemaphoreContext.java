// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import com.cdp4j.exception.CdpReadInterruptionException;
import com.cdp4j.exception.CdpReadTimeoutException;
import com.cdp4j.exception.CommandException;
import com.cdp4j.serialization.ResponseParser;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Semaphore;

class SemaphoreContext implements Context {

    private Object data;

    private CommandException error;

    private final Semaphore semaphore = new Semaphore(0);

    private final DomainCommand domainCommand;

    private final CommandReturnType commandReturnType;

    private final ResponseParser responseParser;

    public SemaphoreContext(
            DomainCommand domainCommand, CommandReturnType commandReturnType, ResponseParser responseParser) {
        this.domainCommand = domainCommand;
        this.commandReturnType = commandReturnType;
        this.responseParser = responseParser;
    }

    @Override
    public void await(final int timeout) {
        try {
            boolean acquired = semaphore.tryAcquire(timeout, MILLISECONDS);
            if (!acquired) {
                throw new CdpReadTimeoutException(timeout);
            }
        } catch (InterruptedException e) {
            throw new CdpReadInterruptionException(timeout);
        }
    }

    @Override
    public void setData(final Object data) {
        this.data = data;
    }

    @Override
    public Object getData() {
        return data;
    }

    @Override
    public void setError(final CommandException error) {
        this.error = error;
    }

    @Override
    public CommandException getError() {
        return error;
    }

    @Override
    public void release() {
        semaphore.release();
    }

    public DomainCommand getDomainCommand() {
        return domainCommand;
    }

    @Override
    public CommandReturnType getCommandReturnType() {
        return commandReturnType;
    }

    @Override
    public CompletableFuture<Object> getPromise() {
        return null;
    }

    @Override
    public ResponseParser getResponseParser() {
        return responseParser;
    }
}
