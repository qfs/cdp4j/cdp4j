// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.TypeReference;
import java.lang.reflect.Type;

public class CommandReturnType {

    final String returns;

    final Type returnType;

    final TypeReference<?> typeReference;

    public CommandReturnType(String returns, Type returnType, TypeReference<?> typeReference) {
        this.returns = returns;
        this.returnType = returnType;
        this.typeReference = typeReference;
    }

    @Override
    public String toString() {
        return "CommandReturnType [returns=" + returns + ", returnType=" + returnType + ", typeReference="
                + typeReference + "]";
    }
}
