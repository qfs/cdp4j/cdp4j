// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import java.nio.file.Path;

public class VideoRecorderResult {

    private final boolean succeess;

    private final Path videoFile;

    public VideoRecorderResult(boolean success, Path videoFile) {
        this.succeess = success;
        this.videoFile = videoFile;
    }

    public boolean isSucceess() {
        return succeess;
    }

    public Path getVideoFile() {
        return videoFile;
    }

    @Override
    public String toString() {
        return "VideoRecorderResult [succeess=" + succeess + ", videoFile=" + videoFile + "]";
    }
}
