// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.session.FutureUtils.orTimeout;
import static com.cdp4j.session.WaitingStrategy.Semaphore;
import static java.lang.Integer.valueOf;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import com.cdp4j.channel.Channel;
import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.CdpReadTimeoutException;
import com.cdp4j.exception.CdpReadTimeoutExceptionHandler;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.serialization.JsonMapper;
import com.cdp4j.serialization.ResponseParser;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

class JacksonSessionInvocationHandler implements SessionInvocationHandler, HandlerConstants {

    private static final int DEFAULT_BUFFER_SIZE = 256;

    private final AtomicInteger counter = new AtomicInteger(0);

    private final Channel channel;

    private final Map<Integer, Context> contexts;

    private final CdpLogger log;

    private final Session session;

    private final String sessionId;

    private int readTimeout;
    private final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler;

    private final WaitingStrategy waitingStrategy;

    private final ObjectWriter writer;

    private final Executor workerThreadPool;

    JacksonSessionInvocationHandler(
            final JsonMapper mapper,
            final Channel channel,
            final Map<Integer, Context> contexts,
            final Session session,
            final CdpLogger log,
            final String sessionId,
            final int readTimeOut,
            final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler,
            final WaitingStrategy waitingStrategy,
            final Executor workerThreadPool) {
        this.writer = (ObjectWriter) mapper.getWriter();
        this.channel = channel;
        this.contexts = contexts;
        this.session = session;
        this.log = log;
        this.sessionId = sessionId;
        this.readTimeout = readTimeOut;
        this.readTimeoutExceptionHandler = readTimeoutExceptionHandler;
        this.waitingStrategy = waitingStrategy;
        this.workerThreadPool = workerThreadPool;
    }

    @Override
    public Object invokeWithCustomParser(
            ParameterizedCommand<?> parameterizedCommand,
            DomainCommand command,
            CommandReturnType crt,
            String[] argNames,
            Object[] args,
            boolean sync,
            ResponseParser responseParser) {
        if (!session.isConnected() || session.isDisposed()) {
            throw new CdpException(
                    session.isDisposed() ? "Session was disposed." : "WebSocket connection is not alive.");
        }
        final Integer id = valueOf(counter.incrementAndGet());

        final int readTimeout;
        final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler;
        if (parameterizedCommand == null) {
            readTimeout = this.readTimeout;
            readTimeoutExceptionHandler = this.readTimeoutExceptionHandler;
        } else {
            int commandReadTimeout = parameterizedCommand.getReadTimeout();
            commandReadTimeout = commandReadTimeout < 0 ? this.readTimeout : commandReadTimeout;
            readTimeout = (int) Math.round(commandReadTimeout * parameterizedCommand.getReadTimeoutFactor());
            final CdpReadTimeoutExceptionHandler commandReadTimeoutExceptionHandler =
                    parameterizedCommand.getReadTimeoutExceptionHandler();
            readTimeoutExceptionHandler = commandReadTimeoutExceptionHandler == null
                    ? this.readTimeoutExceptionHandler
                    : commandReadTimeoutExceptionHandler;
        }

        byte[] json;
        try {
            json = toJson(command.method, id, argNames, args);
        } catch (IOException e) {
            throw new CdpException(e);
        }

        if (log.isDebugEnabled()) {
            log.debug("--> {}", new String(json, UTF_8));
        }

        CompletableFuture<Object> promise = null;
        final Context context = sync
                ? (Semaphore == waitingStrategy
                        ? new SemaphoreContext(command, crt, responseParser)
                        : new ThreadContext(command, crt, responseParser))
                : new PromiseContext(
                        command,
                        crt,
                        promise = orTimeout(
                                new CompletableFuture<>(),
                                readTimeout,
                                MILLISECONDS,
                                (timeout) -> {
                                    final CdpReadTimeoutException timeoutException =
                                            new CdpReadTimeoutException(timeout);
                                    timeoutException.setContextualData(
                                            command, crt, argNames, args, new String(json, StandardCharsets.UTF_8));
                                    return FutureUtils.handleOrReturnException(
                                            timeoutException,
                                            readTimeoutExceptionHandler,
                                            CdpReadTimeoutException.class);
                                },
                                workerThreadPool,
                                () -> {
                                    if (session != null) {
                                        // executor that will complete the future
                                        // exceptionally after the timeout is reached
                                        Context c = session.pullContext(id);
                                        if (c != null) {
                                            c.release();
                                        }
                                    }
                                },
                                log,
                                command),
                        responseParser);
        contexts.put(id, context);

        if (sync) {
            try {
                channel.sendText(json);
                context.await(readTimeout);
            } catch (CdpReadTimeoutException ex) {
                ex.setContextualData(command, crt, argNames, args, new String(json, StandardCharsets.UTF_8));
                if (readTimeoutExceptionHandler != null) {
                    readTimeoutExceptionHandler.accept(ex);
                } else {
                    throw ex;
                }
            } finally {
                session.pullContext(id);
            }
        } else {
            channel.sendText(json);
            return promise;
        }

        if (context.getError() != null) {
            throw context.getError();
        }

        return context.getData();
    }

    byte[] toJson(final String method, final Integer id, final String[] parameters, final Object[] args)
            throws IOException {
        try (ByteArrayOutputStream bs = new ByteArrayOutputStream(DEFAULT_BUFFER_SIZE);
                JsonGenerator generator = writer.createGenerator(bs)) {
            generator.writeStartObject();
            generator.writeNumberField(FIELD_ID, id.intValue());
            if (sessionId != null) {
                generator.writeStringField(FIELD_SESSION_ID, sessionId);
            }
            generator.writeStringField(FIELD_METHOD, method);
            if (args.length > 0) {
                generator.writeFieldName(FIELD_PARAMS);
                generator.writeStartObject();
                for (int i = 0; i < args.length; i++) {
                    Object value = args[i];
                    if (value != null) {
                        generator.writeObjectField(parameters[i], value);
                    }
                }
                generator.writeEndObject();
            }
            generator.writeEndObject();
            generator.close();
            return bs.toByteArray();
        }
    }

    @Override
    public void dispose() {
        for (Context context : contexts.values()) {
            try {
                context.release();
            } catch (Throwable t) {
                // ignore
            }
        }
        contexts.clear();
    }

    @Override
    public Object invoke(
            ParameterizedCommand<?> parameterizedCommand,
            DomainCommand command,
            CommandReturnType crt,
            String[] argNames,
            Object[] args,
            boolean sync) {
        return this.invokeWithCustomParser(parameterizedCommand, command, crt, argNames, args, sync, null);
    }

    @Override
    public int getReadTimeout() {
        return this.readTimeout;
    }

    @Override
    public void setReadTimeout(final int readTimeout) {
        if (readTimeout < 0) {
            throw new IllegalArgumentException("Read timeout must be >= 0");
        }

        this.readTimeout = readTimeout;
    }
}
