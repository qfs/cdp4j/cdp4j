// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.exception.CommandException;
import com.cdp4j.serialization.ResponseParser;
import java.util.concurrent.CompletableFuture;

interface Context {

    void await(int timeout);

    void release();

    void setData(Object data);

    Object getData();

    void setError(CommandException error);

    CommandException getError();

    DomainCommand getDomainCommand();

    CommandReturnType getCommandReturnType();

    CompletableFuture<Object> getPromise();

    ResponseParser getResponseParser();
}
