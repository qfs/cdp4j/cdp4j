// SPDX-License-Identifier: MIT
package com.cdp4j.session;

public enum WaitingStrategy {
    Semaphore,
    ParkThread
}
