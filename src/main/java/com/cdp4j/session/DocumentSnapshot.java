// SPDX-License-Identifier: MIT
package com.cdp4j.session;

public class DocumentSnapshot {

    private final String content;

    private final String baseUrl;

    private final String documentUrl;

    private final String frameId;

    DocumentSnapshot(String content, String baseUrl, String documentUrl, String frameId) {
        this.content = content;
        this.baseUrl = baseUrl;
        this.documentUrl = documentUrl;
        this.frameId = frameId;
    }

    public String getContent() {
        return content;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public String getFrameId() {
        return frameId;
    }

    @Override
    public String toString() {
        return "DocumentSnapshot [content=" + content + ", baseUrl=" + baseUrl + ", documentUrl=" + documentUrl
                + ", frameId=" + frameId + "]";
    }
}
