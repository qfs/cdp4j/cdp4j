// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.event.Events.PageScreencastFrame;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.EncodingDone;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.ErrorPageEnable;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.ErrorStartScreencast;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedEncoding;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedPageEnabled;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedScreencast;
import static com.cdp4j.session.VideoRecorder.VideoRecorderState.InvokedStopped;
import static com.cdp4j.type.constant.ImageFormat.Jpeg;
import static java.lang.Math.max;
import static java.lang.Math.round;
import static java.lang.System.currentTimeMillis;
import static java.nio.ByteBuffer.wrap;
import static java.nio.file.Files.createTempFile;
import static java.util.Base64.getDecoder;
import static us.ihmc.codecs.generated.EProfileIdc.PRO_BASELINE;
import static us.ihmc.codecs.generated.EUsageType.SCREEN_CONTENT_REAL_TIME;
import static us.ihmc.codecs.generated.RC_MODES.RC_OFF_MODE;

import com.cdp4j.command.PageAsync;
import com.cdp4j.event.Events;
import com.cdp4j.event.page.ScreencastFrame;
import com.cdp4j.exception.CdpException;
import com.cdp4j.listener.EventListener;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.type.page.ScreencastFrameMetadata;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import us.ihmc.codecs.builder.H264Settings;
import us.ihmc.codecs.builder.MP4H264MovieBuilder;
import us.ihmc.codecs.builder.MovieBuilder;
import us.ihmc.codecs.generated.YUVPicture;
import us.ihmc.codecs.yuv.JPEGDecoder;

class CiscoOpenH264VideoRecorder implements VideoRecorder, EventListener {

    private static final int DEFAULT_FPS = 25;

    private static final int DEFAULT_NTH_FRAME = 1;

    private static final H264Settings H264_SETTINGS;

    private final Session session;

    private final CdpLogger log;

    private final JPEGDecoder decoder;

    private final PageAsync page;

    // openh264 encoder is not thread safe
    private final Lock encoderLock = new ReentrantLock();

    private final CloseListener sessionCloseListener;

    private final Path videoFile;

    private final VideoRecorderOptions options;

    private volatile MovieBuilder movieBuilder;

    private volatile long lastFrameTimestamp;

    private volatile VideoRecorderState state;

    private volatile boolean disposed;

    private byte[] lastFrame;

    static {
        H264_SETTINGS = new H264Settings();
        H264_SETTINGS.setUsageType(SCREEN_CONTENT_REAL_TIME);
        H264_SETTINGS.setRcMode(RC_OFF_MODE);
        H264_SETTINGS.setProfileIdc(PRO_BASELINE);
    }

    CiscoOpenH264VideoRecorder(Session session, VideoRecorderOptions options, CdpLogger log) {
        this.session = session;
        this.options = options;
        this.log = log;
        try {
            this.videoFile = createTempFile("", options.videoFileName());
        } catch (IOException e) {
            throw new CdpException(e);
        }
        this.decoder = new JPEGDecoder();
        session.addEventListener(this);
        page = session.getAsyncCommand().getPage();
        sessionCloseListener = () -> {
            if (state != InvokedEncoding) {
                dispose();
            }
        };
        session.addCloseListener(sessionCloseListener);
    }

    @Override
    public void start() {
        boolean startable = state == null || state == InvokedStopped;
        if (!startable) {
            throw new IllegalArgumentException("state=[" + state + "]");
        }
        page.enable()
                .thenAccept(rE -> {
                    state = InvokedPageEnabled;
                    page.startScreencast(Jpeg, options.imageQuality(), null, null, DEFAULT_NTH_FRAME)
                            .thenAccept(rC -> {
                                state = InvokedScreencast;
                            })
                            .exceptionally(t -> {
                                state = ErrorStartScreencast;
                                log.error(t.getMessage(), t);
                                session.removeEventEventListener(this);
                                session.removeCloseListener(sessionCloseListener);
                                return null;
                            });
                })
                .exceptionally(t -> {
                    state = ErrorPageEnable;
                    log.error(t.getMessage(), t);
                    session.removeEventEventListener(this);
                    session.removeCloseListener(sessionCloseListener);
                    return null;
                });
    }

    @Override
    public void stop() {
        state = InvokedStopped;
        page.stopScreencast().exceptionally(e -> {
            // This is not a critical problem
            // onEvents method already checks if screencast is stopped with state variable
            log.warn(e.getMessage(), e);
            return null;
        });
    }

    @Override
    public VideoRecorderResult encode() {
        if (state == null) {
            throw new IllegalStateException("Invoke VideoRecorder.start() before video encoding");
        }
        try {
            state = InvokedEncoding;
            process(null, currentTimeMillis(), true);
        } finally {
            dispose();
            state = EncodingDone;
            if (session.isConnected()) {
                session.removeEventEventListener(this);
                session.removeCloseListener(sessionCloseListener);
            }
        }
        return new VideoRecorderResult(true, videoFile);
    }

    @Override
    public CompletableFuture<VideoRecorderResult> encodeAsync() {
        throw new CdpException("Not supported");
    }

    @Override
    public void onEvent(Events event, Object value) {
        if (PageScreencastFrame != event) {
            return;
        }
        if (state == InvokedStopped || state == InvokedEncoding) {
            return;
        }
        final ScreencastFrame sc = (ScreencastFrame) value;
        if (movieBuilder == null) {
            ScreencastFrameMetadata meta = sc.getMetadata();
            try {
                movieBuilder = new MP4H264MovieBuilder(
                        videoFile.toFile(),
                        meta.getDeviceWidth().intValue(),
                        meta.getDeviceHeight().intValue(),
                        DEFAULT_FPS,
                        H264_SETTINGS);
            } catch (IOException e) {
                throw new CdpException(e);
            }
        }
        long frameTimestamp = (long) (sc.getMetadata().getTimestamp().doubleValue() * 1000);
        process(sc.getData(), frameTimestamp, false);
        page.screencastFrameAck(sc.getSessionId()).exceptionally(t -> {
            log.warn(t.getMessage(), t);
            return null;
        });
    }

    protected void process(String frame, long frameTimestamp, boolean done) {
        encoderLock.lock();
        try {
            long duration = frameTimestamp - lastFrameTimestamp;
            int repeatCount = max(1, round(duration * DEFAULT_FPS / 1000));
            if (lastFrame != null) {
                YUVPicture pic = null;
                try {
                    pic = decoder.decode(wrap(lastFrame));
                    for (int i = 0; i < repeatCount; i++) {
                        movieBuilder.encodeFrame(pic);
                    }
                } catch (IOException e) {
                    throw new CdpException(e);
                } finally {
                    if (pic != null) {
                        pic.delete();
                    }
                    lastFrame = null;
                }
            } else {
                if (!done) {
                    lastFrameTimestamp = frameTimestamp;
                    try {
                        lastFrame = getDecoder().decode(frame);
                    } catch (IllegalArgumentException e) {
                        log.error(e.getMessage(), e);
                        return;
                    }
                }
            }
        } finally {
            encoderLock.unlock();
        }
    }

    void dispose() {
        encoderLock.lock();
        try {
            if (disposed) {
                return;
            }
            disposed = true;
            if (decoder != null) {
                decoder.delete();
            }
            if (movieBuilder != null) {
                try {
                    movieBuilder.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        } finally {
            encoderLock.unlock();
        }
    }
}
