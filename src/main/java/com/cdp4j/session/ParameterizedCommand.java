// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.exception.CdpReadTimeoutExceptionHandler;

/**
 * Container to parameterize the command execution
 *
 */
public interface ParameterizedCommand<T extends ParameterizedCommand> extends Cloneable {
    int getReadTimeout();

    double getReadTimeoutFactor();

    CdpReadTimeoutExceptionHandler getReadTimeoutExceptionHandler();

    /**
     * Creates a clone of the given command domain with a different timeout setting for the executed commands.
     * The created object is not cached automatically, this has to be done on client side.
     *
     * @return the command clone
     */
    T withReadTimeout(int readTimeout);

    /**
     * Creates a clone of the given command domain with a different timeout factor setting for the executed commands.
     * The specified or default read timeout is multiplied with the factor to calculate the final read timeout
     * for the command methods issued with this object.
     * The created object is not cached automatically, this has to be done on client side.
     *
     * @return the command clone
     */
    T withReadTimeoutFactor(double readTimeoutFactor);

    /**
     * Creates a clone of the given command domain with a different readTimeoutExceptionHandler setting for the executed commands.
     * The created object is not cached automatically, this has to be done on client side.
     *
     * @return the command clone
     */
    T withReadTimeoutExceptionHandler(CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler);
}
