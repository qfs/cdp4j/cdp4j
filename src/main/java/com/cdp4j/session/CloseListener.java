// SPDX-License-Identifier: MIT
package com.cdp4j.session;

@FunctionalInterface
public interface CloseListener {

    void closed();
}
