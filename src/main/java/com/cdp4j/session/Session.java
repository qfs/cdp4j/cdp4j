// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.JsonLibrary.Jackson;
import static com.cdp4j.event.Events.LogEntryAdded;
import static com.cdp4j.event.Events.NetworkResponseReceived;
import static com.cdp4j.event.Events.PageLifecycleEvent;
import static com.cdp4j.event.Events.RuntimeConsoleAPICalled;
import static com.cdp4j.session.WaitUntil.DomReady;
import static com.cdp4j.session.WaitUntil.Load;
import static com.cdp4j.type.constant.ImageFormat.Png;
import static com.cdp4j.type.network.ResourceType.Document;
import static com.cdp4j.type.network.ResourceType.XHR;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.Math.floor;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.lang.reflect.Proxy.newProxyInstance;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.util.Arrays.asList;
import static java.util.Base64.getDecoder;
import static java.util.Collections.emptyList;
import static java.util.Locale.ENGLISH;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import com.cdp4j.JsFunction;
import com.cdp4j.JsObjectReleaseMode;
import com.cdp4j.Options;
import com.cdp4j.SelectorEngine;
import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.channel.Channel;
import com.cdp4j.command.Emulation;
import com.cdp4j.command.IO;
import com.cdp4j.command.Page;
import com.cdp4j.event.Events;
import com.cdp4j.event.log.EntryAdded;
import com.cdp4j.event.network.ResponseReceived;
import com.cdp4j.event.page.LifecycleEvent;
import com.cdp4j.event.runtime.ConsoleAPICalled;
import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.CdpTimeoutException;
import com.cdp4j.exception.DestinationUnreachableException;
import com.cdp4j.exception.LoadTimeoutException;
import com.cdp4j.listener.EventListener;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.LoggerFactory;
import com.cdp4j.serialization.DefaultEventParser;
import com.cdp4j.serialization.EventParser;
import com.cdp4j.serialization.JsonMapper;
import com.cdp4j.type.constant.ImageFormat;
import com.cdp4j.type.constant.TransferMode;
import com.cdp4j.type.dom.Rect;
import com.cdp4j.type.io.ReadResult;
import com.cdp4j.type.log.LogEntry;
import com.cdp4j.type.network.Response;
import com.cdp4j.type.page.Frame;
import com.cdp4j.type.page.FrameTree;
import com.cdp4j.type.page.GetLayoutMetricsResult;
import com.cdp4j.type.page.NavigateResult;
import com.cdp4j.type.page.PrintToPDFResult;
import com.cdp4j.type.page.ReferrerPolicy;
import com.cdp4j.type.page.TransitionType;
import com.cdp4j.type.page.Viewport;
import com.cdp4j.type.runtime.RemoteObject;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;

public class Session implements AutoCloseable, Selector, Keyboard, Mouse, Navigator, JavaScript, Dom {

    public final List<CdpFrame> frames;

    private final AtomicBoolean connected = new AtomicBoolean(true);

    private final AtomicBoolean disposed = new AtomicBoolean(false);

    private EventParser eventParser = new DefaultEventParser();

    private final JsonMapper mapper;

    private final List<EventListener> listeners;

    private final List<EventListener> syncListeners;

    private final SessionInvocationHandler invocationHandler;

    private final SessionFactory sesessionFactory;

    private final Channel channel;

    private final CdpLogger log;

    private final CdpLogger logFlow;

    private final String targetId;

    private final String sessionId;

    private final Boolean multiFrameMode;

    private final ReentrantLock lock = new ReentrantLock(true);

    private String browserContextId;

    private Command command;

    private AsyncCommand asyncCommand;

    private volatile String frameId;

    private volatile Integer executionContextId;

    private volatile Integer childFrameExecutionContextId;

    private volatile String childFrameId;

    private final Map<Class<?>, Object> jsFunctions;

    private final Map<Integer, Context> contexts;

    private final SelectorEngine selectorEngine;

    private final JsObjectReleaseMode jsObjectReleaseMode;

    private final List<CloseListener> closeListeners = new CopyOnWriteArrayList<>();

    private volatile EnumSet<Events> registeredEventListeners = EnumSet.allOf(Events.class);

    private final LoggerFactory loggerFactory;

    private static final AtomicBoolean ENABLE_ENTRY_EXIT_LOG = new AtomicBoolean(true);

    Session(
            final Options options,
            final JsonMapper mapper,
            final String sessionId,
            final String targetId,
            final String browserContextId,
            final Channel channel,
            final Map<Integer, Context> contexts,
            final SessionFactory sessionFactory,
            final List<EventListener> eventListeners,
            final LoggerFactory loggerFactory,
            final Boolean multiFrameMode,
            final SelectorEngine selectorEngine) {
        this.mapper = mapper;
        this.sessionId = sessionId;
        this.browserContextId = browserContextId;
        this.contexts = contexts;
        if (Jackson.equals(options.jsonLibrary())) {
            this.invocationHandler = new JacksonSessionInvocationHandler(
                    mapper,
                    channel,
                    contexts,
                    this,
                    loggerFactory.getLogger("cdp4j.ws.request"),
                    sessionId,
                    options.readTimeout(),
                    options.readTimeoutExceptionHandler(),
                    options.waitingStrategy(),
                    options.workerThreadPool());
        } else {
            this.invocationHandler = new GsonSessionInvocationHandler(
                    mapper,
                    channel,
                    contexts,
                    this,
                    loggerFactory.getLogger("cdp4j.ws.request"),
                    sessionId,
                    options.readTimeout(),
                    options.readTimeoutExceptionHandler(),
                    options.waitingStrategy(),
                    options.workerThreadPool());
        }

        this.targetId = targetId;
        this.sesessionFactory = sessionFactory;
        this.listeners = eventListeners;
        this.syncListeners = new CopyOnWriteArrayList<>();
        this.channel = channel;
        this.log = loggerFactory.getLogger("cdp4j.session");
        this.logFlow = loggerFactory.getLogger("cdp4j.flow");
        this.jsFunctions = new ConcurrentHashMap<>();
        this.command = new Command(invocationHandler);
        this.asyncCommand = new AsyncCommand(invocationHandler);
        this.selectorEngine = selectorEngine;
        this.multiFrameMode = multiFrameMode;
        this.frames = multiFrameMode ? new CopyOnWriteArrayList<>() : emptyList();
        this.jsObjectReleaseMode = options.jsObjectReleaseMode();
        this.loggerFactory = loggerFactory;
    }

    /**
     * Gets the session identifier.
     */
    public String getId() {
        return sessionId;
    }

    /**
     * Close the this browser window
     */
    @Override
    public void close() {
        logEntry("close");
        if (connected.get()) {
            if (channel.isOpen()) {
                try {
                    sesessionFactory.close(this);
                } finally {
                    connected.set(false);
                }
            } else {
                dispose();
            }
        }
    }

    /**
     * Removes a session from the management (without sending the close command)
     */
    public void remove() {
        logEntry("close");
        if (connected.get()) {
            if (channel.isOpen()) {
                try {
                    sesessionFactory.remove(this);
                } finally {
                    connected.set(false);
                }
            } else {
                dispose();
            }
        }
    }

    /**
     * @return {@code true} if browser is connected.
     */
    public boolean isConnected() {
        return connected.get();
    }

    /**
     * @return {@code true} if the session was disposed.
     */
    public boolean isDisposed() {
        return disposed.get();
    }

    /**
     * Activate this browser window
     */
    public void activate() {
        logEntry("activate");
        sesessionFactory.activate(sessionId);
    }

    public void addEventListener(EventListener eventListener) {
        listeners.add(eventListener);
    }

    public void removeEventEventListener(EventListener eventListener) {
        if (eventListener != null) {
            listeners.remove(eventListener);
        }
    }

    public void addSyncEventListener(EventListener eventListener) {
        syncListeners.add(eventListener);
    }

    public void removeSyncEventEventListener(EventListener eventListener) {
        if (eventListener != null) {
            syncListeners.remove(eventListener);
        }
    }
    /**
     * waits until document is ready
     *
     * @return this
     */
    public Session waitDocumentReady() {
        return waitDocumentReady(WAIT_TIMEOUT);
    }

    /**
     * waits until document is ready
     *
     * @param timeout the maximum time to wait in milliseconds
     *
     * @return this
     */
    public Session waitDocumentReady(final int timeout) {
        return navigateAndWait(null, DomReady, timeout);
    }

    public boolean waitUntil(final Predicate<Session> predicate) {
        return waitUntil(predicate, WAIT_TIMEOUT, WAIT_PERIOD);
    }

    public boolean waitUntil(final Predicate<Session> predicate, final int timeout) {
        return waitUntil(predicate, timeout, WAIT_PERIOD, true);
    }

    public boolean waitUntil(final Predicate<Session> predicate, final int timeout, final int period) {
        return waitUntil(predicate, timeout, period, true);
    }

    public boolean waitUntil(
            final Predicate<Session> predicate, final int timeout, final int period, final boolean log) {
        final int count = (int) floor(timeout / period);
        for (int i = 0; i < count; i++) {
            final boolean wakeup = predicate.test(getThis());
            if (wakeup) {
                return true;
            } else {
                if (!isConnected()) {
                    return false;
                } else {
                    wait(period, log);
                }
            }
        }
        return false;
    }

    /**
     * Navigates to an url
     *
     * @param url URL to navigate page to.
     *  The url should include scheme, e.g. https://.
     *
     *  @return this
     */
    public Session navigate(final String url) {
        return navigate(url, null, null, null, null);
    }

    /**
     * Navigates to an url. All parameters of {@link Page#navigate(String, String, TransitionType, String, ReferrerPolicy)}
     * can be specified.
     *
     * @param url URL to navigate page to.
     * @param referrer Referrer URL.
     * @param transitionType Intended transition type.
     * @param frameId Frame id to navigate, if not specified navigates the top frame.
     * @param referrerPolicy Referrer-policy used for the navigation.
     *
     *  @return this
     */
    public Session navigate(
            final String url,
            final String referrer,
            final TransitionType transitionType,
            final String frameId,
            final ReferrerPolicy referrerPolicy) {
        return navigate(url, referrer, transitionType, frameId, referrerPolicy, command.getPage());
    }

    /**
     * Navigates to an url. All parameters of {@link Page#navigate(String, String, TransitionType, String, ReferrerPolicy)}
     * can be specified, and the (possibly parameterized) Page object to call the navigate method on.
     *
     * @param url URL to navigate page to.
     * @param referrer Referrer URL.
     * @param transitionType Intended transition type.
     * @param frameId Frame id to navigate, if not specified navigates the top frame.
     * @param referrerPolicy Referrer-policy used for the navigation.
     * @param pageCommand Page command to use for navigating
     *
     *  @return this
     */
    public Session navigate(
            final String url,
            final String referrer,
            final TransitionType transitionType,
            final String frameId,
            final ReferrerPolicy referrerPolicy,
            final Page pageCommand) {
        logEntry("navigate", url);
        NavigateResult navigate = pageCommand.navigate(url, referrer, transitionType, frameId, referrerPolicy);
        if (navigate != null) {
            this.frameId = navigate.getFrameId();
        } else {
            throw new DestinationUnreachableException(url);
        }
        return this;
    }

    /**
     *
     * waits until the specified condition meets
     *
     * @param condition
     *
     * @throws CdpTimeoutException if timeout value exceeds 10 seconds
     *
     * @return this
     */
    public Session waitUntil(WaitUntil condition) {
        return navigateAndWait(null, condition);
    }

    /**
     *
     * waits until the specified condition meets
     *
     * @param condition
     *
     * @param timeout Maximum navigation time in milliseconds,
     * defaults to 10 seconds.
     *
     * @throws CdpTimeoutException if timeout value exceeds the specified value
     *
     * @return this
     */
    public Session waitUntil(WaitUntil condition, int timeout) {
        return navigateAndWait(null, condition, timeout);
    }

    /**
     * Navigates to an url
     *
     * @param url URL to navigate page to.
     * The url should include scheme, e.g. https://.
     *
     * @param condition When to consider navigation succeeded.
     *
     * @throws LoadTimeoutException if timeout value exceeds the specified value
     *
     * @return this
     */
    public Session navigateAndWait(final String url, WaitUntil condition) {
        return navigateAndWait(url, condition, 10_000);
    }

    /**
     * Navigates to an url
     *
     * @param url URL to navigate page to.
     *  The url should include scheme, e.g. https://.
     *
     * @param condition When to consider navigation succeeded.
     *
     * @param timeout Maximum navigation time in milliseconds,
     * defaults to 10 seconds.
     *
     * @throws LoadTimeoutException if timeout value exceeds the specified value
     *
     * @return this
     */
    public Session navigateAndWait(final String url, final WaitUntil condition, final int timeout) {

        long start = System.currentTimeMillis();

        final WaitUntil waitUntil = DomReady.equals(condition) ? Load : condition;

        if (url != null) {
            logEntry("navigateAndWait", format("[url=%s, waitUntil=%s, timeout=%d]", url, condition.name(), timeout));
            NavigateResult result = command.getPage().navigate(url);
            if (result != null) {
                this.frameId = result.getFrameId();
            }
        }

        CountDownLatch latch = new CountDownLatch(1);

        EventListener loadListener = (e, d) -> {
            if (PageLifecycleEvent.equals(e)) {
                LifecycleEvent le = (LifecycleEvent) d;
                if (waitUntil.value.equals(le.getName())) {
                    if (this.frameId == null) {
                        this.frameId = le.getFrameId();
                    }
                    latch.countDown();
                }
            }
        };

        addSyncEventListener(loadListener);

        try {
            latch.await(timeout, MILLISECONDS);
        } catch (InterruptedException e) {
            throw new LoadTimeoutException(e);
        } finally {
            removeEventEventListener(loadListener);
        }

        long elapsedTime = System.currentTimeMillis() - start;
        if (elapsedTime > timeout) {
            if (url != null) {
                throw new LoadTimeoutException("Page not loaded within " + timeout + " ms");
            } else {
                throw new CdpTimeoutException(timeout + " ms");
            }
        }

        if (url != null && this.frameId == null) {
            FrameTree tree = getThis().getCommand().getPage().getFrameTree();
            if (tree != null && tree.getFrame() != null && tree.getFrame().getId() != null) {
                this.frameId = tree.getFrame().getId();
            } else {
                throw new DestinationUnreachableException(url);
            }
        }

        if (DomReady.equals(condition) && !isDomReady()) {
            try {
                disableFlowLog();
                boolean ready = waitUntil(p -> isDomReady(), timeout - (int) elapsedTime, 10);
                if (!ready) {
                    throw new LoadTimeoutException("Page not loaded within " + timeout + " ms");
                }
            } finally {
                enableFlowLog();
            }
        }

        return this;
    }

    /**
     * Redirects javascript console logs to cdp4j logger.
     *
     * @return this
     */
    public Session enableConsoleLog() {
        if (!getRegisteredEventListeners().contains(RuntimeConsoleAPICalled)) {
            throw new CdpException("Register RuntimeConsoleAPICalled to enable console log. "
                    + "tip: call session.setRegisteredEventListeners(EnumSet.of(RuntimeConsoleAPICalled))");
        }
        getCommand().getRuntime().enable();
        addEventListener((e, d) -> {
            if (RuntimeConsoleAPICalled.equals(e)) {
                ConsoleAPICalled ca = (ConsoleAPICalled) d;
                for (RemoteObject next : ca.getArgs()) {
                    Object value = next.getValue();
                    String type = ca.getType().toString().toUpperCase(ENGLISH);
                    switch (ca.getType()) {
                        case Log:
                        case Info:
                            log.info("[console] [{}] {}", new Object[] {type, valueOf(value)});
                            break;
                        case Error:
                            log.info("[console] [{}] {}", new Object[] {type, valueOf(value)});
                            break;
                        case Warning:
                            log.info("[console] [{}] {}", new Object[] {type, valueOf(value)});
                            break;
                        default:
                            break;
                    }
                }
            }
        });
        return getThis();
    }

    /**
     * Redirects runtime logs (network, security, storage etc..) to cdp4j logger
     *
     * @return this
     */
    public Session enableDetailLog() {
        if (!getRegisteredEventListeners().contains(LogEntryAdded)) {
            throw new CdpException("Register LogEntryAdded to enable detail log. "
                    + "tip: call session.setRegisteredEventListeners(EnumSet.of(LogEntryAdded))");
        }
        getCommand().getLog().enable();
        addEventListener((e, d) -> {
            if (LogEntryAdded.equals(e)) {
                EntryAdded entryAdded = (EntryAdded) d;
                LogEntry entry = entryAdded.getEntry();
                String level = entry.getLevel().toString().toUpperCase(ENGLISH);
                switch (entry.getLevel()) {
                    case Verbose:
                        log.info("[{}] [{}] {}", entry.getSource(), level, entry.getText());
                        break;
                    case Info:
                        log.info("[{}] [{}] {}", entry.getSource(), level, entry.getText());
                        break;
                    case Warning:
                        log.info("[{}] [{}] {}", entry.getSource(), level, entry.getText());
                        break;
                    case Error:
                        log.info("[{}] [{}] {}", entry.getSource(), level, entry.getText());
                        break;
                }
            }
        });
        return getThis();
    }

    /**
     * Redirects network logs to cdp4j logger
     *
     * @return this
     */
    public Session enableNetworkLog() {
        if (!getRegisteredEventListeners().contains(NetworkResponseReceived)) {
            throw new CdpException("Register NetworkResponseReceived to enable network log. "
                    + "tip: call session.setRegisteredEventListeners(EnumSet.of(NetworkResponseReceived))");
        }
        getCommand().getNetwork().enable();
        addEventListener((e, d) -> {
            if (NetworkResponseReceived.equals(e)) {
                ResponseReceived rr = (ResponseReceived) d;
                Response response = rr.getResponse();
                final String url = response.getUrl();
                final int status = response.getStatus().intValue();
                final String mimeType = response.getMimeType();
                if (Document.equals(rr.getType()) || XHR.equals(rr.getType())) {
                    log.info("[{}] [{}] [{}] [{}] [{}]", new Object[] {
                        rr.getType().toString().toUpperCase(ENGLISH),
                        rr.getResponse().getProtocol().toUpperCase(ENGLISH),
                        status,
                        mimeType,
                        url
                    });
                }
            }
        });
        return getThis();
    }

    public Session getThis() {
        return this;
    }

    public String getFrameId() {
        return frameId;
    }

    /**
     * Capture page screenshot.
     */
    public byte[] captureScreenshot() {
        return captureScreenshot(Png, null, null, true);
    }

    /**
     * Takes a screenshot of the page.
     *
     * @param format Image compression format (defaults to png).
     * @param quality Compression quality from range [0..100] (jpeg only).
     * @param clip Capture the screenshot of a given region only.
     * @param fromSurface Capture the screenshot from the surface, rather than the view. Defaults to true.
     */
    public byte[] captureScreenshot(
            @Optional ImageFormat format,
            @Optional Integer quality,
            @Optional Viewport clip,
            @Experimental @Optional Boolean fromSurface) {
        Page page = getThis().getCommand().getPage();
        GetLayoutMetricsResult metrics = page.getLayoutMetrics();
        Rect cs = metrics.getCssContentSize();
        Emulation emulation = getThis().getCommand().getEmulation();
        emulation.setDeviceMetricsOverride(
                cs.getWidth().intValue(), cs.getHeight().intValue(), 0D, false);
        byte[] data = page.captureScreenshot(format, quality, clip, fromSurface, Boolean.TRUE, null);
        emulation.clearDeviceMetricsOverride();
        emulation.resetPageScaleFactor();
        return data;
    }

    /**
     * Print page as PDF.
     *
     * <strong>Performance tip</strong>: Prefer to use {@link #printToPDF(Path)} if pdf content is to big.
     *
     * @return pdf content as a byte array
     */
    public byte[] printToPDF() {
        PrintToPDFResult result = getCommand().getPage().printToPDF();
        return result.getData();
    }

    /**
     * Print PDF content to a file
     *
     * @param file pdf file path
     */
    public void printToPDF(Path file) {
        PrintToPDFResult pdfResult = getCommand()
                .getPage()
                .printToPDF(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        TransferMode.ReturnAsStream,
                        null,
                        null);
        IO io = getCommand().getIO();
        String stream = pdfResult.getStream();
        boolean eof = false;
        try {
            while (!eof) {
                ReadResult streamResult = io.read(stream);
                eof = streamResult.getEof();
                if (streamResult.getBase64Encoded()) {
                    if (streamResult.getData() != null
                            && !streamResult.getData().isEmpty()) {
                        byte[] content = getDecoder().decode(streamResult.getData());
                        try {
                            Files.write(file, content, APPEND);
                        } catch (IOException e) {
                            throw new CdpException(e);
                        }
                    }
                } else {
                    throw new CdpException("Inavlid content encoding: it must be base64");
                }
            }
        } finally {
            io.close(stream);
        }
    }

    /**
     * Causes the current thread to wait until waiting time elapses.
     *
     * @param timeout the maximum time to wait in milliseconds
     *
     * @throws CdpException if the session held by another thread at the time of invocation.
     *
     * @return this
     */
    public Session wait(int timeout) {
        return wait(timeout, true);
    }

    /**
     * Causes the current thread to wait until waiting time elapses.
     *
     * @param timeout the maximum time to wait in milliseconds
     *
     * @throws CdpException if the session held by another thread at the time of invocation.
     *
     * @return this
     */
    public Session wait(int timeout, boolean log) {
        if (lock.tryLock()) {
            Condition condition = lock.newCondition();
            try {
                if (log) {
                    logEntry("wait", timeout + "ms");
                }
                condition.await(timeout, MILLISECONDS);
            } catch (InterruptedException e) {
                if (channel.isOpen() && connected.get()) {
                    throw new CdpException(e);
                }
            } finally {
                if (lock.isLocked()) {
                    lock.unlock();
                }
            }
        } else {
            throw new CdpException("Unable to acquire lock");
        }
        return getThis();
    }

    public Command getCommand() {
        return command;
    }

    public AsyncCommand getAsyncCommand() {
        return asyncCommand;
    }

    private void getFrames(List<Frame> frames, FrameTree child) {
        frames.add(child.getFrame());
        for (FrameTree next : child.getChildFrames()) {
            getFrames(frames, next);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Session other = (Session) obj;
        if (sessionId == null) {
            if (other.sessionId != null) return false;
        } else if (!sessionId.equals(other.sessionId)) return false;
        return true;
    }

    void dispose() {
        if (disposed.compareAndSet(false, true)) {
            for (CloseListener next : closeListeners) {
                next.closed();
            }
            frames.clear();
            listeners.clear();
            jsFunctions.clear();
            invocationHandler.dispose();
            listeners.clear();
            closeListeners.clear();
            registeredEventListeners.clear();
        }
    }

    void info(final String message, final Object... args) {
        log.info(message, args);
    }

    void error(final String message, final Object... args) {
        log.error(message, args);
    }

    void logEntry(final String method) {
        logEntry(method, null);
    }

    void logEntry(final String method, final String args) {
        if (!ENABLE_ENTRY_EXIT_LOG.get()) {
            return;
        }
        boolean hasArgs = args != null ? true : false;
        logFlow.info(
                "{}({}{}{})", new Object[] {method, hasArgs ? "\"" : "", hasArgs ? args : "", hasArgs ? "\"" : ""});
    }

    void logExit(final String method, final Object retValue) {
        logExit(method, null, retValue);
    }

    void logExit(final String method, final String args, final Object retValue) {
        if (!ENABLE_ENTRY_EXIT_LOG.get()) {
            return;
        }
        boolean hasArgs = args != null ? true : false;
        logFlow.info(
                "{}({}{}{}): {}",
                new Object[] {method, hasArgs ? "\"" : "", hasArgs ? args : "", hasArgs ? "\"" : "", retValue});
    }

    void disableFlowLog() {
        ENABLE_ENTRY_EXIT_LOG.set(FALSE);
    }

    void enableFlowLog() {
        ENABLE_ENTRY_EXIT_LOG.set(TRUE);
    }

    Context pullContext(Integer id) {
        return contexts.remove(id);
    }

    List<EventListener> getListeners() {
        return listeners;
    }

    List<EventListener> getSyncListeners() {
        return syncListeners;
    }

    boolean addFrame(CdpFrame frame) {
        if (!frames.contains(frame)) {
            return frames.add(frame);
        }
        return false;
    }

    boolean removeFrame(int executionContextId) {
        CdpFrame found = null;
        for (CdpFrame next : frames) {
            if (next.getExecutionContextId().equals(executionContextId)) {
                found = next;
                break;
            }
        }
        if (found != null) {
            return frames.remove(found);
        }
        return false;
    }

    /**
     * Switch to child frame
     */
    public boolean switchFrame(Frame frame) {
        return switchFrame(frame == null ? null : frame.getId());
    }

    /**
     * Switch to child frame
     */
    public boolean switchFrame(String frameId) {
        if (FALSE == multiFrameMode) {
            throw new CdpException("Iframe support is not enabled. Please create a session with iframe support.");
        }
        if (frameId == null) {
            return false;
        }
        if (this.frameId.equals(frameId)) {
            switchToRootFrame();
            return true;
        }
        int idx = frames.indexOf(new CdpFrame(frameId));
        if (idx >= 0) {
            CdpFrame found = frames.get(idx);
            childFrameId = found.getId();
            childFrameExecutionContextId = found.getExecutionContextId();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return the child frame id for the context.
     * Returns null if current context is root frame.
     */
    public String getChildFrameId() {
        return childFrameId;
    }

    /**
     * Return the executionContextId of the child frame.
     * Returns null if current context is root frame.
     */
    public Integer getChildFrameExecutionContextId() {
        return childFrameExecutionContextId;
    }

    /**
     * Switch to root frame.
     */
    public void switchToRootFrame() {
        childFrameId = null;
        childFrameExecutionContextId = null;
    }

    /**
     * Returns true if current context is root frame.
     */
    public boolean isRootFrame() {
        if (multiFrameMode) {
            return childFrameId == null;
        } else {
            return true;
        }
    }

    /**
     * Returns flat list of child frames.
     */
    public List<Frame> getFrames() {
        List<Frame> list = new ArrayList<>();
        FrameTree tree = getCommand().getPage().getFrameTree();
        for (FrameTree next : tree.getChildFrames()) {
            getFrames(list, next);
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public <T> T registerJsFunction(Class<T> klass) {
        if (!klass.isInterface()) {
            throw new CdpException("Class must be interface: " + klass.getName());
        }
        if (asList(klass.getMethods()).stream()
                        .filter(p -> p.isAnnotationPresent(JsFunction.class))
                        .count()
                == 0) {
            throw new CdpException("Interface must be contain at least one @JsFunction");
        }
        if (jsFunctions.containsKey(klass)) {
            throw new CdpException("Duplicate Registration is not allowed: " + klass);
        }
        if (jsFunctions.keySet().stream()
                        .filter(p -> p.getSimpleName().equals(klass.getSimpleName()))
                        .count()
                > 0) {
            throw new CdpException("Duplicate class name is not allowed: " + klass.getSimpleName());
        }
        Method[] methods = klass.getMethods();
        StringBuilder builder = new StringBuilder();
        builder.append(format("document.%s = document.%s || {};", klass.getSimpleName(), klass.getSimpleName()));
        for (Method next : methods) {
            JsFunction function = next.getAnnotation(JsFunction.class);
            if (function == null) {
                continue;
            }
            StringBuilder jsMethod = new StringBuilder();
            jsMethod.append("document.");
            jsMethod.append(klass.getSimpleName());
            jsMethod.append(".");
            jsMethod.append(next.getName());
            jsMethod.append(" = function(");
            int count = next.getParameterCount();
            StringJoiner joiner = new StringJoiner(", ");
            for (int i = 0; i < count; i++) {
                Parameter parameter = next.getParameters()[i];
                joiner.add(parameter.getName());
            }
            jsMethod.append(joiner.toString());
            jsMethod.append(") { ");
            jsMethod.append(function.value());
            jsMethod.append(" };");
            builder.append(jsMethod.toString());
        }
        Page page = getCommand().getPage();
        page.addScriptToEvaluateOnNewDocument(builder.toString());
        Object instance = newProxyInstance(
                getClass().getClassLoader(), new Class<?>[] {klass}, (InvocationHandler) (proxy, method, args) -> {
                    String className = method.getDeclaringClass().getSimpleName();
                    String methodName = method.getName();
                    Class<?> returnType = method.getReturnType();
                    if ((void.class.equals(returnType) || Void.class.equals(returnType))
                            && (args == null || args.length == 0)) {
                        callFunction("document." + className + "." + methodName);
                        return null;
                    } else {
                        Object result = callFunction("document." + className + "." + methodName, returnType, args);
                        return result;
                    }
                });
        jsFunctions.put(klass, instance);
        return (T) instance;
    }

    @SuppressWarnings("unchecked")
    public <T> T getJsFunction(Class<T> klass) {
        return (T) jsFunctions.get(klass);
    }

    boolean isPrimitive(Class<?> klass) {
        if (String.class.equals(klass)) {
            return true;
        } else if (boolean.class.equals(klass) || Boolean.class.equals(klass)) {
            return true;
        } else if (void.class.equals(klass) || Void.class.equals(klass)) {
            return true;
        } else if (int.class.equals(klass) || Integer.class.equals(klass)) {
            return true;
        } else if (double.class.equals(klass) || Double.class.equals(klass)) {
            return true;
        } else if (long.class.equals(klass) || Long.class.equals(klass)) {
            return true;
        } else if (float.class.equals(klass) || Float.class.equals(klass)) {
            return true;
        } else if (char.class.equals(klass) || Character.class.equals(klass)) {
            return true;
        } else if (byte.class.equals(klass) || Byte.class.equals(klass)) {
            return true;
        } else if (short.class.equals(klass) || Short.class.equals(klass)) {
            return true;
        }
        return false;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getBrowserContextId() {
        return browserContextId;
    }

    public Integer getExecutionContextId() {
        if (FALSE == multiFrameMode) {
            return executionContextId;
        } else {
            if (childFrameId == null) {
                return executionContextId;
            } else {
                return childFrameExecutionContextId;
            }
        }
    }

    void setExecutionContextId(Integer executionContextId) {
        this.executionContextId = executionContextId;
    }

    public SelectorEngine getSelectorEngine() {
        return selectorEngine;
    }

    public boolean addCloseListener(CloseListener listener) {
        if (listener == null) {
            return false;
        }
        if (closeListeners.contains(listener)) {
            return false;
        }
        return closeListeners.add(listener);
    }

    public boolean removeCloseListener(CloseListener listener) {
        return closeListeners.remove(listener);
    }

    public JsonMapper getJsonMapper() {
        return mapper;
    }

    public Boolean isMultiFrameMode() {
        return multiFrameMode;
    }

    public void setRegisteredEventListeners(EnumSet<Events> registeredEventListeners) {
        this.registeredEventListeners = registeredEventListeners;
    }

    public EnumSet<Events> getRegisteredEventListeners() {
        return this.registeredEventListeners;
    }

    JsObjectReleaseMode jsObjectReleaseMode() {
        return jsObjectReleaseMode;
    }

    public VideoRecorder createVideoRecoder(VideoRecorderOptions options) {
        return new FFmpegVideoRecorder(this, options, loggerFactory.getLogger("cdp4j.videoRecorder"));
    }

    public VideoRecorder createRealtimeVideoRecoder(VideoRecorderOptions options) {
        int start = options.videoFileName().indexOf('.');
        String extension = options.videoFileName().substring(start + 1);
        if (!"mp4".equals(extension)) {
            throw new CdpException("invalid video file extension[" + extension + "]. File extension must be [mp4]");
        }
        return new CiscoOpenH264VideoRecorder(
                getThis(), options, loggerFactory.getLogger("cdp4j.realtimeVideoRecorder"));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sessionId == null) ? 0 : sessionId.hashCode());
        return result;
    }

    public EventParser getEventParser() {
        return eventParser;
    }

    public void setEventParser(EventParser eventParser) {
        this.eventParser = eventParser;
    }

    @Override
    public String toString() {
        return "Session [sessionId=" + sessionId + "]";
    }

    public int getReadTimeout() {
        return invocationHandler.getReadTimeout();
    }

    public void setReadTimeout(final int readTimeout) {
        invocationHandler.setReadTimeout(readTimeout);
    }
}
