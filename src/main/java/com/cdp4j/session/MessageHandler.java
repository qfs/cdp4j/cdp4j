// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.event.Events.PageLifecycleEvent;
import static com.cdp4j.event.Events.RuntimeExecutionContextCreated;
import static com.cdp4j.event.Events.RuntimeExecutionContextDestroyed;
import static com.cdp4j.event.Events.TargetDetachedFromTarget;
import static com.cdp4j.event.Events.TargetTargetCreated;
import static com.cdp4j.event.Events.TargetTargetDestroyed;
import static java.util.EnumSet.of;

import com.cdp4j.event.Events;
import java.io.InputStream;
import java.util.EnumSet;

public interface MessageHandler extends HandlerConstants {

    void process(String content);

    void process(byte[] content);

    void process(InputStream is);

    static final EnumSet<Events> MANDATORY_EVENT_LISTENERS = of(
            PageLifecycleEvent,
            TargetTargetDestroyed,
            TargetDetachedFromTarget,
            TargetTargetCreated,
            RuntimeExecutionContextCreated,
            RuntimeExecutionContextDestroyed);
}
