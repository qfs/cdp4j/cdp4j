// SPDX-License-Identifier: MIT
package com.cdp4j.session;

interface HandlerConstants {

    static final String FIELD_ID = "id";
    static final String FIELD_RESULT = "result";
    static final String FIELD_SESSION_ID = "sessionId";
    static final String FIELD_DATA = "data";
    static final String FIELD_MESSAGE = "message";
    static final String FIELD_CODE = "code";
    static final String FIELD_METHOD = "method";
    static final String FIELD_ERROR = "error";
    static final String FIELD_PARAMS = "params";
}
