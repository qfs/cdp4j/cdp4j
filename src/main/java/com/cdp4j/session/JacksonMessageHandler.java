// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.SessionIdFilter.SimpleParser;
import static com.fasterxml.jackson.core.JsonFactory.Feature.CANONICALIZE_FIELD_NAMES;
import static com.fasterxml.jackson.core.JsonFactory.Feature.INTERN_FIELD_NAMES;
import static com.fasterxml.jackson.core.JsonToken.END_OBJECT;
import static com.fasterxml.jackson.core.JsonToken.START_OBJECT;
import static com.fasterxml.jackson.core.JsonToken.VALUE_NUMBER_INT;
import static com.fasterxml.jackson.core.JsonToken.VALUE_STRING;
import static com.fasterxml.jackson.core.StreamReadFeature.AUTO_CLOSE_SOURCE;
import static com.fasterxml.jackson.core.filter.TokenFilter.Inclusion.ONLY_INCLUDE_ALL;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.Integer.valueOf;
import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Base64.getDecoder;
import static java.util.Collections.unmodifiableMap;

import com.cdp4j.TypeReference;
import com.cdp4j.channel.InputStreamExtension;
import com.cdp4j.event.Events;
import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.CommandException;
import com.cdp4j.listener.EventListener;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.serialization.JsonMapper;
import com.cdp4j.serialization.LoggingInputStream;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonFactoryBuilder;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.filter.FilteringParserDelegate;
import com.fasterxml.jackson.core.filter.TokenFilter;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

class JacksonMessageHandler implements MessageHandler {

    enum InputType {
        String,
        ByteArray,
        InputStream
    }

    private static final int RESPONSE_WITH_SESSION_ID_MIN_LEN = 60;

    private static final Map<String, Events> EVENTS = listEvents();

    private final ObjectReader reader;

    private final JsonFactory jsonFactory;

    private final TypeFactory typeFactory;

    private final Executor workerThreadPool;

    private final Executor eventHandlerThreadPool;

    private final CdpLogger log;

    private final SessionFactory factory;

    private final boolean useSimpleFilter;

    private static final TokenFilter SESSION_ID_FILTER = new SessionIdFilter();

    private static class SessionIdFilter extends TokenFilter {

        @Override
        public TokenFilter filterStartArray() {
            return null;
        }

        @Override
        public TokenFilter includeProperty(String name) {
            return FIELD_SESSION_ID.equals(name) ? INCLUDE_ALL : null;
        }
    }

    JacksonMessageHandler(
            final JsonMapper mapper,
            final SessionFactory factory,
            final Executor workerThreadPool,
            final Executor eventHandlerThreadPool,
            final CdpLogger log) {
        this.reader = (ObjectReader) mapper.getReader();
        this.jsonFactory = new JsonFactoryBuilder()
                .disable(CANONICALIZE_FIELD_NAMES)
                .disable(INTERN_FIELD_NAMES)
                .disable(AUTO_CLOSE_SOURCE)
                .build();
        this.typeFactory = reader.getTypeFactory();
        this.factory = factory;
        this.workerThreadPool = workerThreadPool;
        this.eventHandlerThreadPool = eventHandlerThreadPool;
        this.useSimpleFilter = factory == null /* null check control is required for unit test */
                || SimpleParser == factory.getOptions().jsonIdFilter();
        this.log = log;
    }

    @Override
    public void process(final String content) {
        process(content, InputType.String);
    }

    @Override
    public void process(final byte[] content) {
        process(content, InputType.ByteArray);
    }

    @Override
    public void process(final InputStream content) {
        process(content, InputType.InputStream);
    }

    protected String filterSessionId(final Object content, final InputType it) throws IOException {
        switch (it) {
            default:
            case String:
                return filterSessionId((String) content);
            case ByteArray:
                return filterSessionId((byte[]) content);
            case InputStream:
                return filterSessionId((InputStream) content);
        }
    }

    protected String parseSessionId(final Object content, final InputType it) {
        String sessionId = null;
        JsonParser parser = null;
        try {
            switch (it) {
                case ByteArray:
                    parser = jsonFactory.createParser((byte[]) content);
                    break;
                case String:
                    parser = jsonFactory.createParser((String) content);
                    break;
                case InputStream:
                    parser = jsonFactory.createParser((InputStream) content);
                    break;
            }
        } catch (IOException e) {
            throw new CdpException(e);
        }
        try (JsonParser jsonParser = new FilteringParserDelegate(parser, SESSION_ID_FILTER, ONLY_INCLUDE_ALL, true)) {
            JsonToken token = null;
            while ((token = jsonParser.nextToken()) != null) {
                if (token.isScalarValue()) {
                    sessionId = jsonParser.getValueAsString();
                }
            }
        } catch (IOException e) {
            throw new CdpException(e);
        } finally {
            if (InputType.InputStream == it) {
                try {
                    ((InputStream) content).reset();
                } catch (IOException e) {
                    throw new CdpException(e);
                }
            }
        }
        return sessionId;
    }

    protected String filterSessionId(final String content) {
        if (content.length() <= 2) {
            return null;
        }
        // skip if response is command without sessionId (browserSession)
        if (content.charAt(content.length() - 2) == '}' && content.charAt(content.length() - 1) == '}') {
            return null;
        }
        final int[] offsets = new int[] {0, 0, 0, 0};
        int cursor = offsets.length;
        for (int i = content.length() - 1; i > 0 && cursor > 0; i--) {
            if (content.charAt(i) == '"') {
                offsets[--cursor] = i;
            }
        }
        if (cursor != 0) {
            return null;
        }
        return content.charAt(offsets[0] + 1) == FIELD_SESSION_ID.charAt(0)
                        && content.charAt(offsets[0] + 2) == FIELD_SESSION_ID.charAt(1)
                        && content.charAt(offsets[0] + 3) == FIELD_SESSION_ID.charAt(2)
                        && content.charAt(offsets[0] + 4) == FIELD_SESSION_ID.charAt(3)
                        && content.charAt(offsets[0] + 5) == FIELD_SESSION_ID.charAt(4)
                        && content.charAt(offsets[0] + 6) == FIELD_SESSION_ID.charAt(5)
                        && content.charAt(offsets[0] + 7) == FIELD_SESSION_ID.charAt(6)
                        && content.charAt(offsets[0] + 8) == FIELD_SESSION_ID.charAt(7)
                        && content.charAt(offsets[0] + 9) == FIELD_SESSION_ID.charAt(8)
                ? content.substring(offsets[2] + 1, offsets[3])
                : null;
    }

    protected String filterSessionId(final byte[] content) {
        if (content.length <= 2) {
            return null;
        }
        // skip if response is command without sessionId (browserSession)
        if (content[content.length - 2] == '}' && content[content.length - 1] == '}') {
            return null;
        }
        final int[] offsets = new int[] {0, 0, 0, 0};
        int cursor = offsets.length;
        for (int i = content.length - 1; i > 0 && cursor > 0; i--) {
            if (content[i] == '"') {
                offsets[--cursor] = i;
            }
        }
        if (cursor != 0) {
            return null;
        }
        if (content[offsets[0] + 1] == FIELD_SESSION_ID.charAt(0)
                && content[offsets[0] + 2] == FIELD_SESSION_ID.charAt(1)
                && content[offsets[0] + 3] == FIELD_SESSION_ID.charAt(2)
                && content[offsets[0] + 4] == FIELD_SESSION_ID.charAt(3)
                && content[offsets[0] + 5] == FIELD_SESSION_ID.charAt(4)
                && content[offsets[0] + 6] == FIELD_SESSION_ID.charAt(5)
                && content[offsets[0] + 7] == FIELD_SESSION_ID.charAt(6)
                && content[offsets[0] + 8] == FIELD_SESSION_ID.charAt(7)
                && content[offsets[0] + 9] == FIELD_SESSION_ID.charAt(8)) {
            StringBuilder builder = new StringBuilder(offsets[3] - offsets[2]);
            for (int i = offsets[2] + 1; i < offsets[3]; i++) {
                builder.append((char) content[i]);
            }
            return builder.toString();
        } else {
            return null;
        }
    }

    protected String filterSessionId(final InputStream content) throws IOException {
        String sessionId = null;
        if (content instanceof InputStreamExtension) {
            int length = content.available();
            if (length <= RESPONSE_WITH_SESSION_ID_MIN_LEN) {
                return null;
            }
            InputStreamExtension ise = (InputStreamExtension) content;
            byte[] chunk = ise.readLastNBytes(RESPONSE_WITH_SESSION_ID_MIN_LEN);
            sessionId = filterSessionId(chunk);
        } else {
            int length = content.available();
            if (length <= RESPONSE_WITH_SESSION_ID_MIN_LEN) {
                return null;
            }
            int skip = (int) content.skip(length - RESPONSE_WITH_SESSION_ID_MIN_LEN);
            byte[] chunk = new byte[length - skip];
            content.read(chunk, 0, length - skip);
            sessionId = filterSessionId(chunk);
        }
        content.reset();
        return sessionId;
    }

    protected void process(final Object content, InputType it) {
        Runnable runnable = () -> {
            // commons
            JsonParser parser = null;
            Context context = null;
            // comand response
            Integer id = 0;
            Object result = null;
            // event response
            Events event = null;
            Object eventValue = null;
            // error
            Throwable parseException = null;
            Integer errorCode = null;
            String errorMessage = null;
            String errorData = null;

            String sessionId;
            try {
                sessionId = useSimpleFilter ? filterSessionId(content, it) : parseSessionId(content, it);
            } catch (IOException e) {
                // It's not possible to forward the exception if the sessionId can't be parsed
                // swallow the exception
                log.error(e.getMessage(), e);
                return;
            }

            final Session session = sessionId == null ? factory.getBrowserSession() : factory.getSession(sessionId);

            try {
                switch (it) {
                    case ByteArray:
                        if (log.isDebugEnabled()) {
                            log.debug("<-- {}", new String((byte[]) content, UTF_8));
                        }
                        parser = jsonFactory.createParser((byte[]) content);
                        break;
                    case InputStream:
                        InputStream inputStream = (InputStream) content;
                        if (log.isDebugEnabled()) {
                            inputStream = new LoggingInputStream(inputStream, log, "<-- {}");
                        }
                        parser = jsonFactory.createParser(inputStream);
                        break;
                    default:
                    case String:
                        if (log.isDebugEnabled()) {
                            log.debug("<-- {}", content);
                        }
                        parser = jsonFactory.createParser((String) content);
                        break;
                }
                JsonToken token = null;
                while ((token = parser.nextToken()) != null) {
                    final String name = parser.currentName();
                    final int idx = parser.getParsingContext().getCurrentIndex();
                    if (idx == 0) {
                        if (VALUE_NUMBER_INT == token && FIELD_ID.equals(name)) {
                            id = valueOf(parser.getValueAsInt());
                            context = session.pullContext(id);
                        } else if (START_OBJECT == token && FIELD_RESULT.equals(name)) {
                            CommandReturnType crt = context != null ? context.getCommandReturnType() : null;
                            if (crt != null) {
                                if (context.getResponseParser() == null) {
                                    result = fromJson(crt.returns, crt.returnType, crt.typeReference, parser);
                                } else {
                                    result = context.getResponseParser().parse(session, parser);
                                }
                            }
                        } else if (VALUE_STRING == token && FIELD_METHOD.equals(name)) {
                            String eventName = parser.getValueAsString();
                            event = EVENTS.get(eventName);
                            if (session != null
                                    && !MANDATORY_EVENT_LISTENERS.contains(event)
                                    && !session.getRegisteredEventListeners().contains(event)) {
                                // skip unregistered event
                                event = null;
                            }
                        } else if (START_OBJECT == token && FIELD_PARAMS.equals(name) && event != null) {
                            eventValue = session.getEventParser()
                                    .parse(
                                            event, session,
                                            parser, reader);
                        } else if (START_OBJECT == token && FIELD_ERROR.equals(name)) {
                            // parser error message
                            while ((token = parser.nextToken()) != END_OBJECT) {
                                final String currentName = parser.currentName();
                                if (VALUE_NUMBER_INT == token && FIELD_CODE.equals(currentName)) {
                                    errorCode = parser.getIntValue();
                                } else if (VALUE_STRING == token && FIELD_MESSAGE.equals(currentName)) {
                                    errorMessage = parser.getValueAsString();
                                } else if (VALUE_STRING == token && FIELD_DATA.equals(currentName)) {
                                    errorData = parser.getValueAsString();
                                }
                            }
                        }
                    } else if (idx == 1
                            && errorCode != null
                            && VALUE_NUMBER_INT == token
                            && FIELD_ID.equals(name)) { // id might follow error object
                        id = valueOf(parser.getValueAsInt());
                        context = session.pullContext(id);
                    }
                }
            } catch (Throwable e) {
                parseException = e;
            } finally {
                if (parser != null && parseException != null) {
                    try {
                        parser.close();
                    } catch (IOException e) {
                        // ignore
                    }
                }
                if (it == InputType.InputStream) {
                    try {
                        ((InputStream) content).close();
                    } catch (IOException e) {
                        log.warn(e.getMessage());
                    }
                }
            }
            if (session == null) {
                return;
            }
            // Process command response
            if (context != null) {
                try {
                    if (parseException != null) {
                        context.setError(new CommandException(parseException));
                    } else if (errorCode != null) {
                        int code = errorCode.intValue();
                        String message = format(
                                "Command [%s.%s] failed. %s%s",
                                context.getDomainCommand().domain.name(),
                                context.getDomainCommand().command,
                                errorMessage,
                                errorData != null ? ". " + errorData : "");
                        context.setError(new CommandException(code, message));
                        if (context.getPromise() != null) {
                            log.error(errorMessage);
                            context.getPromise().completeExceptionally(context.getError());
                        }
                    } else {
                        if (context.getPromise() != null) {
                            context.getPromise().complete(result);
                        } else {
                            context.setData(result);
                        }
                    }
                } finally {
                    context.release();
                }
            } else if (eventValue != null) {
                // Process event response
                for (EventListener next : session.getSyncListeners()) {
                    next.onEvent(event, eventValue);
                }
                final Object fEventValue = eventValue;
                final Events fEvent = event;
                eventHandlerThreadPool.execute(() -> {
                    for (EventListener next : session.getListeners()) {
                        next.onEvent(fEvent, fEventValue);
                    }
                });
            } else if (parseException != null) {
                throw new CdpException(parseException);
            }
        };
        workerThreadPool.execute(runnable);
    }

    Object fromJson(
            final String returns, final Type returnType, final TypeReference<?> typeArgument, final JsonParser parser)
            throws IOException {
        if (returns != null) {
            if (typeArgument != null) {
                JsonToken token = null;
                while ((token = parser.nextToken()) != null) {
                    if (token.isStructStart() && returns.equals(parser.currentName())) { // List<?>, Map<?, ?> ...
                        JavaType javaType = typeFactory.constructType(typeArgument.getType());
                        Object value = reader.readValue(parser, javaType);
                        return value;
                    }
                }
            } else if (returnType != null) {
                JsonToken token = null;
                while ((token = parser.nextToken()) != null) {
                    if (returns.equals(parser.currentName())) {
                        if (token.isScalarValue()) { // value type (String, int, double...)
                            switch (token) {
                                case VALUE_NULL:
                                    return null;
                                case VALUE_STRING:
                                    if (byte[].class.equals(returnType)) {
                                        return getDecoder().decode(parser.getValueAsString());
                                    } else {
                                        return parser.getValueAsString();
                                    }
                                case VALUE_FALSE:
                                    return FALSE;
                                case VALUE_TRUE:
                                    return TRUE;
                                case VALUE_NUMBER_INT:
                                    return parser.getValueAsInt();
                                case VALUE_NUMBER_FLOAT:
                                    return parser.getValueAsDouble();
                                default:
                                    return parser.getValueAsString();
                            }
                        } else if (token == START_OBJECT) {
                            JavaType javaType = typeFactory.constructType((Class<?>) returnType);
                            Object value = reader.readValue(parser, javaType);
                            return value;
                        }
                    }
                }
            }
        } else if (returnType != null) {
            return reader.readValue(parser, (Class<?>) returnType);
        }
        throw new IllegalStateException();
    }

    private static Map<String, Events> listEvents() {
        Events[] values = Events.values();
        Map<String, Events> map = new HashMap<>(values.length);
        for (Events next : values) {
            map.put(next.domain + "." + next.name, next);
        }
        return unmodifiableMap(map);
    }
}
