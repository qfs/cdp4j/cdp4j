/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cdp4j.session;

import static java.util.concurrent.TimeUnit.SECONDS;

import com.cdp4j.exception.CdpReadTimeoutException;
import com.cdp4j.logger.CdpLogger;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

//
// COPIED FROM:
// https://github.com/apache/flink/blob/master/flink-core/src/main/java/org/apache/flink/util/concurrent/FutureUtils.java
//

/** A collection of utilities that expand the usage of {@link CompletableFuture}. */
class FutureUtils {

    private static class CdpThreadFactory implements ThreadFactory {

        private static final AtomicInteger THREAD_SEQUENCE_NUMBER = new AtomicInteger(1);

        private final String name;

        public CdpThreadFactory(String name) {
            this.name = name;
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread thread = new Thread(r, name + "-" + THREAD_SEQUENCE_NUMBER.getAndIncrement());
            thread.setDaemon(true);
            return thread;
        }
    }

    /**
     * Times the given future out after the timeout.
     *
     * @param future to time out
     * @param timeout after which the given future is timed out
     * @param timeUnit time unit of the timeout
     * @param readTimeoutSupplier a factory that creates a CdpReadTimeoutException from a given timeout
     * @param timeoutFailExecutor executor that will complete the future exceptionally after the
     *     timeout is reached
     * @param <T> type of the given future
     * @return The timeout enriched future
     */
    public static <T> CompletableFuture<T> orTimeout(
            CompletableFuture<T> future,
            long timeout,
            TimeUnit timeUnit,
            Function<Long, CdpReadTimeoutException> readTimeoutSupplier,
            Executor timeoutFailExecutor,
            Runnable cleaner,
            CdpLogger logger,
            DomainCommand command) {

        if (!future.isDone()) {
            final ScheduledFuture<?> timeoutFuture = Delayer.delay(
                    () -> timeoutFailExecutor.execute(
                            new Timeout(future, timeout, readTimeoutSupplier, cleaner, logger, command)),
                    timeout,
                    timeUnit);

            future.whenComplete((T value, Throwable throwable) -> {
                if (!timeoutFuture.isDone()) {
                    timeoutFuture.cancel(false);
                }
            });
        }

        return future;
    }

    /** Runnable to complete the given future with a {@link CdpReadTimeoutException}. */
    private static final class Timeout implements Runnable {

        private final CompletableFuture<?> future;
        private final long timeout;
        private final Function<Long, CdpReadTimeoutException> readTimeoutSupplier;
        private final Runnable cleaner;
        private final CdpLogger log;
        private final DomainCommand command;

        private Timeout(
                CompletableFuture<?> future,
                long timeout,
                Function<Long, CdpReadTimeoutException> readTimeoutSupplier,
                Runnable cleaner,
                CdpLogger log,
                DomainCommand command) {
            this.future = future;
            this.timeout = timeout;
            this.readTimeoutSupplier = readTimeoutSupplier;
            this.cleaner = cleaner;
            this.log = log;
            this.command = command;
        }

        @Override
        public void run() {
            log.error("TimeoutError [{}] ({} ms)", command.method, timeout);

            CdpReadTimeoutException timeoutException = readTimeoutSupplier.apply(timeout);
            if (timeoutException != null) {
                future.completeExceptionally(timeoutException);
            }
            cleaner.run();
        }
    }

    /**
     * Delay scheduler used to timeout futures.
     *
     * <p>This class creates a singleton scheduler used to run the provided actions.
     */
    private enum Delayer {
        ;
        static final ScheduledThreadPoolExecutor DELAYER;

        static {
            DELAYER = new ScheduledThreadPoolExecutor(0, new CdpThreadFactory("CdpCompletableFutureDelayScheduler"));
            // NOTE:
            // default value of readTimeout is 10 seconds (Options.DEFAULT_READ_TIMEOUT)
            // set an optimal value to prevent unnecessary thread creation
            // setKeepAliveTime helps to terminate core thread after the specified time
            DELAYER.setKeepAliveTime(60, SECONDS);
            DELAYER.setRemoveOnCancelPolicy(true);
        }

        /**
         * Delay the given action by the given delay.
         *
         * @param runnable to execute after the given delay
         * @param delay after which to execute the runnable
         * @param timeUnit time unit of the delay
         * @return Future of the scheduled action
         */
        private static ScheduledFuture<?> delay(Runnable runnable, long delay, TimeUnit timeUnit) {
            return DELAYER.schedule(runnable, delay, timeUnit);
        }
    }

    /**
     * Helper method used to return an exception probably thrown by a consumer
     */
    @SuppressWarnings("unchecked")
    public static <R extends RuntimeException, T extends R> R handleOrReturnException(
            final T exception, final Consumer<T> exceptionHandler, final Class<R> returnTypeClass) {
        if (exceptionHandler != null) {
            try {
                exceptionHandler.accept(exception);
                return null;
            } catch (RuntimeException handlerException) {
                if (returnTypeClass.isAssignableFrom(handlerException.getClass())) {
                    return (R) handlerException;
                }
                throw handlerException;
            }
        }
        return exception;
    }
}
