// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import java.util.concurrent.CompletableFuture;

public interface VideoRecorder {

    public static enum VideoRecorderState {
        ErrorPageEnable,
        ErrorStartScreencast,
        InvokedPageEnabled,
        InvokedScreencast,
        InvokedStopped,
        InvokedEncoding,
        EncodingDone,
        EncodingTimeout,
        EncodingError
    }

    void start();

    void stop();

    VideoRecorderResult encode();

    CompletableFuture<VideoRecorderResult> encodeAsync();
}
