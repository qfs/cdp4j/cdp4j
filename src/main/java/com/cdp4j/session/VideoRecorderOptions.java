// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.logger.CdpLogggerLevel.Info;
import static java.lang.Long.valueOf;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;
import static java.util.concurrent.TimeUnit.SECONDS;

import com.cdp4j.logger.CdpLogggerLevel;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

public class VideoRecorderOptions {

    private static final Integer DEFAULT_IMAGE_QUALITY = 80;

    private static final String DEFAULT_VIDEO_FILE_NAME = "video.webm";

    private static final Integer DEFAULT_ENCODING_TIMEOUT =
            valueOf(SECONDS.toMillis(30)).intValue();

    private static final CdpLogggerLevel DEFAULT_ENCODER_LOG_LEVEL = Info;

    private String ffmpegExecutable;

    private Integer imageQuality;

    private String videoFileName;

    private Integer encodingTimeout;

    private CdpLogggerLevel encoderLogLevel;

    private Executor encoderThreadPool;

    private List<String> ffmpegArgs = emptyList();

    public static class Builder {
        private VideoRecorderOptions options = new VideoRecorderOptions();

        private Builder() {
            // no op
        }

        public Builder ffmpegExecutable(String ffmpegExecutable) {
            if (ffmpegExecutable == null || ffmpegExecutable.trim().isEmpty()) {
                throw new IllegalArgumentException();
            }
            this.options.ffmpegExecutable = ffmpegExecutable;
            return this;
        }

        public Builder ffmpegArgs(List<String> ffmpegArgs) {
            if (ffmpegArgs == null) {
                throw new IllegalArgumentException();
            }
            this.options.ffmpegArgs = unmodifiableList(ffmpegArgs);
            return this;
        }

        public Builder imageQuality(int imageQuality) {
            this.options.imageQuality = Integer.valueOf(imageQuality);
            return this;
        }

        public Builder videoFileName(String videoFileName) {
            if (videoFileName != null) {
                if (videoFileName.contains("\"") || videoFileName.contains("/")) {
                    throw new IllegalArgumentException("Absolute or relative path is not supported");
                }
                if (videoFileName.indexOf(".") <= 0) {
                    throw new IllegalArgumentException("missing file extension");
                }
            }
            this.options.videoFileName = videoFileName;
            return this;
        }

        public Builder encodingTimeout(int timeout, TimeUnit timeUnit) {
            this.options.encodingTimeout = valueOf(timeUnit.toMillis(timeout)).intValue();
            return this;
        }

        public Builder encoderLogLevel(CdpLogggerLevel encoderLogLevel) {
            this.options.encoderLogLevel = encoderLogLevel;
            return this;
        }

        public Builder encoderThreadPool(Executor encoderThreadPool) {
            this.options.encoderThreadPool = encoderThreadPool;
            return this;
        }

        public VideoRecorderOptions build() {
            if (options.imageQuality == null) {
                options.imageQuality = DEFAULT_IMAGE_QUALITY;
            }
            if (options.videoFileName == null) {
                options.videoFileName = DEFAULT_VIDEO_FILE_NAME;
            }
            if (options.encodingTimeout == null) {
                options.encodingTimeout = DEFAULT_ENCODING_TIMEOUT;
            }
            if (options.encoderLogLevel == null) {
                options.encoderLogLevel = DEFAULT_ENCODER_LOG_LEVEL;
            }
            if (options.ffmpegExecutable == null) {
                options.ffmpegExecutable = "ffmpeg";
            }
            return options;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public String ffmpegExecutable() {
        return ffmpegExecutable;
    }

    public List<String> ffmpegArgs() {
        return ffmpegArgs;
    }

    public int imageQuality() {
        return imageQuality;
    }

    public String videoFileName() {
        return videoFileName;
    }

    public int encodingTimeout() {
        return encodingTimeout;
    }

    public CdpLogggerLevel encoderLogLevel() {
        return encoderLogLevel;
    }

    public Executor encoderThreadPool() {
        return encoderThreadPool;
    }

    @Override
    public String toString() {
        return "VideoRecorderOptions [ffmpegExecutable=" + ffmpegExecutable + ", imageQuality=" + imageQuality
                + ", videoFileName=" + videoFileName + ", encodingTimeout=" + encodingTimeout + ", encoderLogLevel="
                + encoderLogLevel + ", encoderThreadPool=" + encoderThreadPool + ", ffmpegArgs=" + ffmpegArgs + "]";
    }
}
