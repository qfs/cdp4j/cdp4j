// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.command.AccessibilityAsync;
import com.cdp4j.command.AnimationAsync;
import com.cdp4j.command.AsyncCommandFactory;
import com.cdp4j.command.AuditsAsync;
import com.cdp4j.command.AutofillAsync;
import com.cdp4j.command.BackgroundServiceAsync;
import com.cdp4j.command.BluetoothEmulationAsync;
import com.cdp4j.command.BrowserAsync;
import com.cdp4j.command.CSSAsync;
import com.cdp4j.command.CacheStorageAsync;
import com.cdp4j.command.CastAsync;
import com.cdp4j.command.ConsoleAsync;
import com.cdp4j.command.CustomCommandAsync;
import com.cdp4j.command.DOMAsync;
import com.cdp4j.command.DOMDebuggerAsync;
import com.cdp4j.command.DOMSnapshotAsync;
import com.cdp4j.command.DOMStorageAsync;
import com.cdp4j.command.DebuggerAsync;
import com.cdp4j.command.DeviceAccessAsync;
import com.cdp4j.command.DeviceOrientationAsync;
import com.cdp4j.command.EmulationAsync;
import com.cdp4j.command.EventBreakpointsAsync;
import com.cdp4j.command.ExtensionsAsync;
import com.cdp4j.command.FedCmAsync;
import com.cdp4j.command.FetchAsync;
import com.cdp4j.command.FileSystemAsync;
import com.cdp4j.command.HeadlessExperimentalAsync;
import com.cdp4j.command.HeapProfilerAsync;
import com.cdp4j.command.IOAsync;
import com.cdp4j.command.IndexedDBAsync;
import com.cdp4j.command.InputAsync;
import com.cdp4j.command.InspectorAsync;
import com.cdp4j.command.LayerTreeAsync;
import com.cdp4j.command.LogAsync;
import com.cdp4j.command.MediaAsync;
import com.cdp4j.command.MemoryAsync;
import com.cdp4j.command.NetworkAsync;
import com.cdp4j.command.OverlayAsync;
import com.cdp4j.command.PWAAsync;
import com.cdp4j.command.PageAsync;
import com.cdp4j.command.PerformanceAsync;
import com.cdp4j.command.PerformanceTimelineAsync;
import com.cdp4j.command.PreloadAsync;
import com.cdp4j.command.ProfilerAsync;
import com.cdp4j.command.RuntimeAsync;
import com.cdp4j.command.SchemaAsync;
import com.cdp4j.command.SecurityAsync;
import com.cdp4j.command.ServiceWorkerAsync;
import com.cdp4j.command.StorageAsync;
import com.cdp4j.command.SystemInfoAsync;
import com.cdp4j.command.TargetAsync;
import com.cdp4j.command.TetheringAsync;
import com.cdp4j.command.TracingAsync;
import com.cdp4j.command.WebAudioAsync;
import com.cdp4j.command.WebAuthnAsync;

public class AsyncCommand {
    private AccessibilityAsync accessibility;

    private AnimationAsync animation;

    private AuditsAsync audits;

    private AutofillAsync autofill;

    private BackgroundServiceAsync backgroundService;

    private BluetoothEmulationAsync bluetoothEmulation;

    private BrowserAsync browser;

    private CSSAsync css;

    private CacheStorageAsync cacheStorage;

    private CastAsync cast;

    private ConsoleAsync console;

    private CustomCommandAsync customCommand;

    private DOMAsync dom;

    private DOMDebuggerAsync domDebugger;

    private DOMSnapshotAsync domSnapshot;

    private DOMStorageAsync domStorage;

    private DebuggerAsync debugger;

    private DeviceAccessAsync deviceAccess;

    private DeviceOrientationAsync deviceOrientation;

    private EmulationAsync emulation;

    private EventBreakpointsAsync eventBreakpoints;

    private ExtensionsAsync extensions;

    private FedCmAsync fedCm;

    private FetchAsync fetch;

    private FileSystemAsync fileSystem;

    private HeadlessExperimentalAsync headlessExperimental;

    private HeapProfilerAsync heapProfiler;

    private IOAsync io;

    private IndexedDBAsync indexedDB;

    private InputAsync input;

    private InspectorAsync inspector;

    private LayerTreeAsync layerTree;

    private LogAsync log;

    private MediaAsync media;

    private MemoryAsync memory;

    private NetworkAsync network;

    private OverlayAsync overlay;

    private PWAAsync pwa;

    private PageAsync page;

    private PerformanceAsync performance;

    private PerformanceTimelineAsync performanceTimeline;

    private PreloadAsync preload;

    private ProfilerAsync profiler;

    private RuntimeAsync runtime;

    private SchemaAsync schema;

    private SecurityAsync security;

    private ServiceWorkerAsync serviceWorker;

    private StorageAsync storage;

    private SystemInfoAsync systemInfo;

    private TargetAsync target;

    private TetheringAsync tethering;

    private TracingAsync tracing;

    private WebAudioAsync webAudio;

    private WebAuthnAsync webAuthn;

    private final AsyncCommandFactory delegate;

    public AsyncCommand(final SessionInvocationHandler handler) {
        this.delegate = new AsyncCommandFactory(handler);
    }

    public AccessibilityAsync getAccessibility() {
        if (accessibility == null) {
            return accessibility = delegate.getAccessibility();
        }
        return accessibility;
    }

    public AnimationAsync getAnimation() {
        if (animation == null) {
            return animation = delegate.getAnimation();
        }
        return animation;
    }

    public AuditsAsync getAudits() {
        if (audits == null) {
            return audits = delegate.getAudits();
        }
        return audits;
    }

    public AutofillAsync getAutofill() {
        if (autofill == null) {
            return autofill = delegate.getAutofill();
        }
        return autofill;
    }

    public BackgroundServiceAsync getBackgroundService() {
        if (backgroundService == null) {
            return backgroundService = delegate.getBackgroundService();
        }
        return backgroundService;
    }

    public BluetoothEmulationAsync getBluetoothEmulation() {
        if (bluetoothEmulation == null) {
            return bluetoothEmulation = delegate.getBluetoothEmulation();
        }
        return bluetoothEmulation;
    }

    public BrowserAsync getBrowser() {
        if (browser == null) {
            return browser = delegate.getBrowser();
        }
        return browser;
    }

    public CSSAsync getCSS() {
        if (css == null) {
            return css = delegate.getCSS();
        }
        return css;
    }

    public CacheStorageAsync getCacheStorage() {
        if (cacheStorage == null) {
            return cacheStorage = delegate.getCacheStorage();
        }
        return cacheStorage;
    }

    public CastAsync getCast() {
        if (cast == null) {
            return cast = delegate.getCast();
        }
        return cast;
    }

    public ConsoleAsync getConsole() {
        if (console == null) {
            return console = delegate.getConsole();
        }
        return console;
    }

    public CustomCommandAsync getCustomCommand() {
        if (customCommand == null) {
            return customCommand = delegate.getCustomCommand();
        }
        return customCommand;
    }

    public DOMAsync getDOM() {
        if (dom == null) {
            return dom = delegate.getDOM();
        }
        return dom;
    }

    public DOMDebuggerAsync getDOMDebugger() {
        if (domDebugger == null) {
            return domDebugger = delegate.getDOMDebugger();
        }
        return domDebugger;
    }

    public DOMSnapshotAsync getDOMSnapshot() {
        if (domSnapshot == null) {
            return domSnapshot = delegate.getDOMSnapshot();
        }
        return domSnapshot;
    }

    public DOMStorageAsync getDOMStorage() {
        if (domStorage == null) {
            return domStorage = delegate.getDOMStorage();
        }
        return domStorage;
    }

    public DebuggerAsync getDebugger() {
        if (debugger == null) {
            return debugger = delegate.getDebugger();
        }
        return debugger;
    }

    public DeviceAccessAsync getDeviceAccess() {
        if (deviceAccess == null) {
            return deviceAccess = delegate.getDeviceAccess();
        }
        return deviceAccess;
    }

    public DeviceOrientationAsync getDeviceOrientation() {
        if (deviceOrientation == null) {
            return deviceOrientation = delegate.getDeviceOrientation();
        }
        return deviceOrientation;
    }

    public EmulationAsync getEmulation() {
        if (emulation == null) {
            return emulation = delegate.getEmulation();
        }
        return emulation;
    }

    public EventBreakpointsAsync getEventBreakpoints() {
        if (eventBreakpoints == null) {
            return eventBreakpoints = delegate.getEventBreakpoints();
        }
        return eventBreakpoints;
    }

    public ExtensionsAsync getExtensions() {
        if (extensions == null) {
            return extensions = delegate.getExtensions();
        }
        return extensions;
    }

    public FedCmAsync getFedCm() {
        if (fedCm == null) {
            return fedCm = delegate.getFedCm();
        }
        return fedCm;
    }

    public FetchAsync getFetch() {
        if (fetch == null) {
            return fetch = delegate.getFetch();
        }
        return fetch;
    }

    public FileSystemAsync getFileSystem() {
        if (fileSystem == null) {
            return fileSystem = delegate.getFileSystem();
        }
        return fileSystem;
    }

    public HeadlessExperimentalAsync getHeadlessExperimental() {
        if (headlessExperimental == null) {
            return headlessExperimental = delegate.getHeadlessExperimental();
        }
        return headlessExperimental;
    }

    public HeapProfilerAsync getHeapProfiler() {
        if (heapProfiler == null) {
            return heapProfiler = delegate.getHeapProfiler();
        }
        return heapProfiler;
    }

    public IOAsync getIO() {
        if (io == null) {
            return io = delegate.getIO();
        }
        return io;
    }

    public IndexedDBAsync getIndexedDB() {
        if (indexedDB == null) {
            return indexedDB = delegate.getIndexedDB();
        }
        return indexedDB;
    }

    public InputAsync getInput() {
        if (input == null) {
            return input = delegate.getInput();
        }
        return input;
    }

    public InspectorAsync getInspector() {
        if (inspector == null) {
            return inspector = delegate.getInspector();
        }
        return inspector;
    }

    public LayerTreeAsync getLayerTree() {
        if (layerTree == null) {
            return layerTree = delegate.getLayerTree();
        }
        return layerTree;
    }

    public LogAsync getLog() {
        if (log == null) {
            return log = delegate.getLog();
        }
        return log;
    }

    public MediaAsync getMedia() {
        if (media == null) {
            return media = delegate.getMedia();
        }
        return media;
    }

    public MemoryAsync getMemory() {
        if (memory == null) {
            return memory = delegate.getMemory();
        }
        return memory;
    }

    public NetworkAsync getNetwork() {
        if (network == null) {
            return network = delegate.getNetwork();
        }
        return network;
    }

    public OverlayAsync getOverlay() {
        if (overlay == null) {
            return overlay = delegate.getOverlay();
        }
        return overlay;
    }

    public PWAAsync getPWA() {
        if (pwa == null) {
            return pwa = delegate.getPWA();
        }
        return pwa;
    }

    public PageAsync getPage() {
        if (page == null) {
            return page = delegate.getPage();
        }
        return page;
    }

    public PerformanceAsync getPerformance() {
        if (performance == null) {
            return performance = delegate.getPerformance();
        }
        return performance;
    }

    public PerformanceTimelineAsync getPerformanceTimeline() {
        if (performanceTimeline == null) {
            return performanceTimeline = delegate.getPerformanceTimeline();
        }
        return performanceTimeline;
    }

    public PreloadAsync getPreload() {
        if (preload == null) {
            return preload = delegate.getPreload();
        }
        return preload;
    }

    public ProfilerAsync getProfiler() {
        if (profiler == null) {
            return profiler = delegate.getProfiler();
        }
        return profiler;
    }

    public RuntimeAsync getRuntime() {
        if (runtime == null) {
            return runtime = delegate.getRuntime();
        }
        return runtime;
    }

    public SchemaAsync getSchema() {
        if (schema == null) {
            return schema = delegate.getSchema();
        }
        return schema;
    }

    public SecurityAsync getSecurity() {
        if (security == null) {
            return security = delegate.getSecurity();
        }
        return security;
    }

    public ServiceWorkerAsync getServiceWorker() {
        if (serviceWorker == null) {
            return serviceWorker = delegate.getServiceWorker();
        }
        return serviceWorker;
    }

    public StorageAsync getStorage() {
        if (storage == null) {
            return storage = delegate.getStorage();
        }
        return storage;
    }

    public SystemInfoAsync getSystemInfo() {
        if (systemInfo == null) {
            return systemInfo = delegate.getSystemInfo();
        }
        return systemInfo;
    }

    public TargetAsync getTarget() {
        if (target == null) {
            return target = delegate.getTarget();
        }
        return target;
    }

    public TetheringAsync getTethering() {
        if (tethering == null) {
            return tethering = delegate.getTethering();
        }
        return tethering;
    }

    public TracingAsync getTracing() {
        if (tracing == null) {
            return tracing = delegate.getTracing();
        }
        return tracing;
    }

    public WebAudioAsync getWebAudio() {
        if (webAudio == null) {
            return webAudio = delegate.getWebAudio();
        }
        return webAudio;
    }

    public WebAuthnAsync getWebAuthn() {
        if (webAuthn == null) {
            return webAuthn = delegate.getWebAuthn();
        }
        return webAuthn;
    }
}
