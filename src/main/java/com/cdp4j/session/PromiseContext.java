// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.exception.CommandException;
import com.cdp4j.serialization.ResponseParser;
import java.util.concurrent.CompletableFuture;

class PromiseContext implements Context {

    private Object data;

    private CommandException error;

    private final DomainCommand domainCommand;

    private final CommandReturnType commandReturnType;

    private final CompletableFuture<Object> promise;

    private final ResponseParser responseParser;

    public PromiseContext(
            DomainCommand domainCommand,
            CommandReturnType commandReturnType,
            CompletableFuture<Object> promise,
            ResponseParser responseParser) {
        this.domainCommand = domainCommand;
        this.commandReturnType = commandReturnType;
        this.promise = promise;
        this.responseParser = responseParser;
    }

    @Override
    public void await(final int timeout) {
        // no op
    }

    @Override
    public void setData(final Object data) {
        this.data = data;
    }

    @Override
    public Object getData() {
        return data;
    }

    @Override
    public void setError(final CommandException error) {
        this.error = error;
    }

    @Override
    public CommandException getError() {
        return error;
    }

    @Override
    public void release() {
        // no op
    }

    public DomainCommand getDomainCommand() {
        return domainCommand;
    }

    @Override
    public CommandReturnType getCommandReturnType() {
        return commandReturnType;
    }

    @Override
    public CompletableFuture<Object> getPromise() {
        return promise;
    }

    @Override
    public ResponseParser getResponseParser() {
        return responseParser;
    }
}
