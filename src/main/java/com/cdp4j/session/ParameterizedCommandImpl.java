// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.exception.CdpReadTimeoutExceptionHandler;

@SuppressWarnings("unchecked")
public class ParameterizedCommandImpl<T extends ParameterizedCommand<?>> implements ParameterizedCommand<T> {
    protected int readTimeout = -1; // < 0 = use default
    protected double readTimeoutFactor = 1.0;
    protected CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler = null;

    @Override
    public int getReadTimeout() {
        return readTimeout;
    }

    @Override
    public double getReadTimeoutFactor() {
        return readTimeoutFactor;
    }

    @Override
    public CdpReadTimeoutExceptionHandler getReadTimeoutExceptionHandler() {
        return readTimeoutExceptionHandler;
    }

    @Override
    public T withReadTimeout(final int readTimeout) {
        final T clonedCommand = clone();
        ((ParameterizedCommandImpl<T>) clonedCommand).readTimeout = readTimeout;
        return clonedCommand;
    }

    @Override
    public T withReadTimeoutFactor(final double readTimeoutFactor) {
        if (readTimeoutFactor <= 0) {
            throw new IllegalArgumentException("read timeout factor must be >= 0");
        }

        final T clonedCommand = clone();
        ((ParameterizedCommandImpl<T>) clonedCommand).readTimeoutFactor = readTimeoutFactor;
        return clonedCommand;
    }

    @Override
    public T withReadTimeoutExceptionHandler(final CdpReadTimeoutExceptionHandler readTimeoutExceptionHandler) {
        final T clonedCommand = clone();
        ((ParameterizedCommandImpl<T>) clonedCommand).readTimeoutExceptionHandler = readTimeoutExceptionHandler;
        return clonedCommand;
    }

    @Override
    protected T clone() {
        try {
            return (T) super.clone();
        } catch (final CloneNotSupportedException e) {
            assert false : "Never happens";
            throw new RuntimeException(e);
        }
    }
}
