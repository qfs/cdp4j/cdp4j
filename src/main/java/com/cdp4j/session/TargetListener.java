// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import com.cdp4j.event.Events;
import com.cdp4j.event.target.DetachedFromTarget;
import com.cdp4j.event.target.TargetDestroyed;
import com.cdp4j.listener.EventListener;
import java.util.Map;

class TargetListener implements EventListener {

    private final Map<String, Session> sessions;

    TargetListener(Map<String, Session> sessions) {
        this.sessions = sessions;
    }

    @Override
    @SuppressWarnings("incomplete-switch")
    public void onEvent(Events event, Object value) {
        switch (event) {
            case TargetTargetDestroyed:
                TargetDestroyed destroyed = (TargetDestroyed) value;
                for (Session next : sessions.values()) {
                    if (destroyed.getTargetId().equals(next.getTargetId())) {
                        if (sessions.remove(next.getId()) != null) {
                            next.dispose();
                        }
                    }
                }
                break;
            case TargetDetachedFromTarget:
                DetachedFromTarget detached = (DetachedFromTarget) value;
                Session removed = null;
                if ((removed = sessions.remove(detached.getSessionId())) != null) {
                    removed.dispose();
                }
                break;
        }
    }
}
