// SPDX-License-Identifier: MIT
package com.cdp4j.session;

import static com.cdp4j.SelectorEngine.Native;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

import com.cdp4j.SelectorEngine;
import com.cdp4j.type.target.WindowState;

public class SessionSettings {

    private static final Integer DEFAULT_SCREEN_WIDTH = 1366; // WXGA width

    private static final Integer DEFAULT_SCREEN_HEIGHT = 768; // WXGA height

    private final Integer screenLeft;

    private final Integer screenTop;

    private final int screenWidth;

    private final int screenHeight;

    private final Boolean multiFrameMode;

    private final WindowState windowState;

    private final SelectorEngine selectorEngine;

    private final Boolean reuseEmptyTab;

    public static class Builder {

        private Integer screenLeft;

        private Integer screenTop;

        private Integer screenWidth;

        private Integer screenHeight;

        private Boolean multiFrameMode;

        private WindowState windowState;

        private SelectorEngine selectorEngine;

        private Boolean reuseEmptyTab;

        private Builder() {
            // no op
        }

        public Builder screenLeft(Integer screenLeft) {
            this.screenLeft = screenLeft;
            return this;
        }

        public Builder screenTop(Integer screenTop) {
            this.screenTop = screenTop;
            return this;
        }

        public Builder screenWidth(int screenWidth) {
            this.screenWidth = screenWidth;
            return this;
        }

        public Builder screenHeight(int screenHeight) {
            this.screenHeight = screenHeight;
            return this;
        }

        public Builder multiFrameMode(Boolean multiFrameMode) {
            this.multiFrameMode = multiFrameMode;
            return this;
        }

        public Builder windowState(WindowState windowState) {
            this.windowState = windowState;
            return this;
        }

        public Builder selectorEngine(SelectorEngine selectorEngine) {
            this.selectorEngine = selectorEngine;
            return this;
        }

        public Builder reuseEmptyTab(Boolean reuseEmptyTab) {
            this.reuseEmptyTab = reuseEmptyTab;
            return this;
        }

        public SessionSettings build() {
            return new SessionSettings(
                    screenLeft,
                    screenTop,
                    screenWidth == null ? DEFAULT_SCREEN_WIDTH : screenWidth,
                    screenHeight == null ? DEFAULT_SCREEN_HEIGHT : screenHeight,
                    multiFrameMode == null ? FALSE : multiFrameMode,
                    windowState,
                    selectorEngine == null ? Native : selectorEngine,
                    reuseEmptyTab == null ? TRUE : reuseEmptyTab);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public SessionSettings() {
        this(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT);
    }

    public SessionSettings(Boolean multiFrameMode) {
        this(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, multiFrameMode);
    }

    public SessionSettings(int screenWidth, int screenHeight) {
        this(screenWidth, screenHeight, FALSE);
    }

    public SessionSettings(int screenWidth, int screenHeight, Boolean multiFrameMode) {
        this.screenLeft = null;
        this.screenTop = null;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.multiFrameMode = multiFrameMode;
        this.windowState = null;
        this.selectorEngine = Native;
        this.reuseEmptyTab = TRUE;
    }

    private SessionSettings(
            Integer screenLeft,
            Integer screenTop,
            int screenWidth,
            int screenHeight,
            Boolean multiFrameMode,
            WindowState windowState,
            SelectorEngine selectorEngine,
            Boolean reuseEmptyTab) {
        this.screenLeft = screenLeft;
        this.screenTop = screenTop;
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        this.multiFrameMode = multiFrameMode;
        this.windowState = windowState;
        this.selectorEngine = selectorEngine;
        this.reuseEmptyTab = reuseEmptyTab;
    }

    public Integer getScreenLeft() {
        return screenLeft;
    }

    public Integer getScreenTop() {
        return screenTop;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public Boolean isMultiFrameMode() {
        return multiFrameMode;
    }

    public WindowState getWindowState() {
        return windowState;
    }

    public SelectorEngine getSelectorEngine() {
        return selectorEngine;
    }

    public Boolean getReuseEmptyTab() {
        return reuseEmptyTab;
    }

    @Override
    public String toString() {
        return "SessionSettings [screenWidth=" + screenWidth + ", screenHeight=" + screenHeight + ", multiFrameMode="
                + multiFrameMode + ", selectorEngine=" + selectorEngine + ", reuseEmptyTab=" + reuseEmptyTab + "]";
    }
}
