// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

class ListInputStream extends InputStream implements InputStreamExtension {

    private static final int EOF = -1;

    private final List<InputStream> streams;

    private final AutoCloseable closeable;

    private Iterator<InputStream> iterator;

    private InputStream current;

    public ListInputStream(AutoCloseable closeable, List<InputStream> streams) {
        this.closeable = closeable;
        this.streams = streams;
        this.iterator = streams.iterator();
        peekNextStream();
    }

    final void nextStream() throws IOException {
        if (current != null) {
            current.close();
        }
        peekNextStream();
    }

    private void peekNextStream() {
        if (iterator.hasNext()) {
            current = (InputStream) iterator.next();
            if (current == null) throw new NullPointerException();
        } else {
            current = null;
        }
    }

    public int available() throws IOException {
        if (current == null) {
            return 0;
        }
        return current.available();
    }

    public int read() throws IOException {
        while (current != null) {
            int c = current.read();
            if (c != EOF) {
                return c;
            }
            nextStream();
        }
        return EOF;
    }

    public int read(byte b[], int off, int len) throws IOException {
        if (current == null) {
            return EOF;
        } else if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        do {
            int n = current.read(b, off, len);
            if (n > 0) {
                return n;
            }
            nextStream();
        } while (current != null);
        return EOF;
    }

    public void close() throws IOException {
        try {
            closeable.close();
        } catch (Exception e) {
            // ignore
        }
    }

    @Override
    public void reset() throws IOException {
        for (InputStream is : streams) {
            is.reset();
        }
        iterator = streams.iterator();
        peekNextStream();
    }

    @Override
    public byte[] readLastNBytes(int length) throws IOException {
        return readLastNBytesOfStream(streams.size() - 1, length, 0);
    }

    private byte[] readLastNBytesOfStream(
            int streamIndex, final int requestedLength, final int numberOfBytesReservedForCaller) throws IOException {
        InputStream is = streams.get(streamIndex);
        int availableBytesInThisStream = is.available();

        byte[] data = null;
        final int numberOfBytesToReadFromThisStream;
        if (requestedLength > availableBytesInThisStream) { // This stream is too short
            numberOfBytesToReadFromThisStream = availableBytesInThisStream;
            if (streamIndex > 0) { // Get remaining bytes from previous stream
                final int numberOfBytesToReserveInData =
                        numberOfBytesToReadFromThisStream + numberOfBytesReservedForCaller;
                data = readLastNBytesOfStream(
                        streamIndex - 1, requestedLength - numberOfBytesToReserveInData, numberOfBytesToReserveInData);
            }
        } else {
            numberOfBytesToReadFromThisStream = requestedLength;
        }

        if (data == null) {
            data = new byte[numberOfBytesToReadFromThisStream + numberOfBytesReservedForCaller];
        }

        int writePositionInData = data.length - numberOfBytesToReadFromThisStream - numberOfBytesReservedForCaller;

        is.skip(availableBytesInThisStream - numberOfBytesToReadFromThisStream);
        for (int i = 0; i < numberOfBytesToReadFromThisStream; i++) {
            data[i + writePositionInData] = (byte) is.read();
        }
        return data;
    }
}
