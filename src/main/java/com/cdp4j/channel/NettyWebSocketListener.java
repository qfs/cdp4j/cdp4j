// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.CdpLoggerFactory;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketHandshakeException;

public class NettyWebSocketListener extends SimpleChannelInboundHandler<Object> {

    private final WebSocketClientHandshaker handshaker;

    private final SessionFactory factory;

    private final MessageHandler handler;

    private ChannelPromise handshakeFuture;

    private final CdpLogger log;

    public NettyWebSocketListener(
            SessionFactory factory, MessageHandler handler, WebSocketClientHandshaker handshaker) {
        this.factory = factory;
        this.handler = handler;
        this.handshaker = handshaker;
        log = new CdpLoggerFactory(factory.getOptions()).getLogger("cdp.netty");
    }

    public ChannelFuture handshakeFuture() {
        return handshakeFuture;
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        handshakeFuture = ctx.newPromise();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        handshaker.handshake(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        factory.close();
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (!handshaker.isHandshakeComplete()) {
            try {
                handshaker.finishHandshake(ctx.channel(), (FullHttpResponse) msg);
                handshakeFuture.setSuccess();
            } catch (WebSocketHandshakeException e) {
                handshakeFuture.setFailure(e);
            }
            return;
        }
        WebSocketFrame frame = (WebSocketFrame) msg;
        if (frame instanceof TextWebSocketFrame) {
            TextWebSocketFrame text = (TextWebSocketFrame) frame;
            handler.process(new ByteBufInputStream(text.content().retain(), true));
        } else if (frame instanceof CloseWebSocketFrame) {
            factory.close();
            ctx.channel().close();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable t) {
        if (!factory.closed()) {
            log.error(t.getMessage(), t);
        }
        if (!handshakeFuture.isDone()) {
            handshakeFuture.setFailure(t);
        }
        ctx.close();
    }
}
