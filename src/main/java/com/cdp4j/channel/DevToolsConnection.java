// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.lang.String.format;
import static java.util.Collections.emptyMap;

import com.cdp4j.exception.CdpException;
import com.cdp4j.serialization.GsonMapper;
import com.cdp4j.serialization.JsonMapper;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class DevToolsConnection implements Connection {

    private final int port;

    private final String host;

    private final JsonMapper mapper;

    private static final int HTTP_OK = 200;

    public DevToolsConnection(int port) {
        this(port, "127.0.0.1");
    }

    public DevToolsConnection(int port, JsonMapper mapper) {
        this(port, "127.0.0.1", mapper);
    }

    public DevToolsConnection(int port, String host) {
        this(port, host, new GsonMapper());
    }

    protected int getTimeout() {
        return 1000; // ms
    }

    public DevToolsConnection(int port, String host, JsonMapper mapper) {
        this.port = port;
        this.host = host;
        this.mapper = mapper;
    }

    @Override
    public String getUrl() {
        String sessions = format("http://%s:%d/json/version", host, port);
        try {
            URL url = new URL(sessions);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            final int timeout = getTimeout();
            conn.setConnectTimeout(timeout);
            conn.setReadTimeout(timeout);
            if (conn.getResponseCode() != HTTP_OK) {
                throw new CdpException("Invalid http status code: " + conn.getResponseCode());
            }
            int contentLength = conn.getContentLength();
            if (contentLength < 0) {
                throw new CdpException("Invalid http response. Content-Length must be greater than 0.");
            }
            @SuppressWarnings("rawtypes")
            Map map = emptyMap();
            try (InputStream is = conn.getInputStream()) {
                map = mapper.fromJson(is, Map.class);
            }
            String webSocketDebuggerUrl = (String) map.get("webSocketDebuggerUrl");
            if (webSocketDebuggerUrl == null) {
                throw new CdpException("JSON response must contain webSocketDebuggerUrl property.");
            }
            if (!webSocketDebuggerUrl.startsWith("ws://")) {
                throw new CdpException("Invalid protocol. webSocketDebuggerUrl must be valid websocket URL.");
            }
            return webSocketDebuggerUrl;
        } catch (ConnectException e) {
            throw new CdpException(e);
        } catch (IOException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public boolean isValid() {
        String url = null;
        try {
            url = getUrl();
            return url.startsWith("ws://");
        } catch (Throwable t) {
            return false;
        }
    }
}
