// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static com.cdp4j.JsonLibrary.Jackson;
import static io.netty.buffer.Unpooled.wrappedBuffer;
import static io.vertx.core.http.WebSocketFrameType.TEXT;
import static java.util.concurrent.TimeUnit.SECONDS;

import com.cdp4j.exception.CdpException;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.CdpLoggerFactory;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocket;
import io.vertx.core.http.WebSocketConnectOptions;
import io.vertx.core.http.impl.ws.WebSocketFrameImpl;
import java.util.concurrent.Semaphore;

class VertxWebSocketChannel implements Channel {

    private final SessionFactory factory;

    private final MessageHandler handler;

    private final HttpClient httpClient;

    private final WebSocketConnectOptions options;

    private final CdpLogger log;

    private WebSocket webSocket;

    public VertxWebSocketChannel(
            SessionFactory factory, HttpClient httpClient, MessageHandler handler, WebSocketConnectOptions options) {
        this.factory = factory;
        this.httpClient = httpClient;
        this.handler = handler;
        this.options = options;
        this.log = new CdpLoggerFactory(factory.getOptions()).getLogger("cdp.vertx");
    }

    @Override
    public boolean isOpen() {
        return !webSocket.isClosed();
    }

    @Override
    public void disconnect() {
        if (!webSocket.isClosed()) {
            webSocket.close(CLOSE_STATUS_CODE, CLOSE_REASON_TEXT, event -> factory.close());
        }
    }

    @Override
    public void sendText(String message) {
        webSocket.writeFinalTextFrame(message);
    }

    @Override
    public void sendText(byte[] message) {
        WebSocketFrameImpl frame = new WebSocketFrameImpl(TEXT, wrappedBuffer(message), true);
        webSocket.writeFrame(frame);
    }

    @Override
    public void connect() {
        Semaphore semaphore = new Semaphore(0);
        httpClient.webSocket(options, (Handler<AsyncResult<WebSocket>>) event -> {
            if (event.succeeded()) {
                webSocket = event.result();
                webSocket.exceptionHandler(onError -> semaphore.release());
                if (Jackson.equals(factory.getOptions().jsonLibrary())) {
                    webSocket.frameHandler(evnt -> {
                        if (evnt.isText() && evnt.isFinal()) {
                            handler.process(evnt.binaryData().getBytes());
                        }
                    });
                } else {
                    webSocket.textMessageHandler(content -> handler.process(content));
                }
                webSocket.closeHandler(onCloseEvent -> factory.close());
            } else if (event.failed()) {
                if (event.cause() != null) {
                    log.error(event.cause().getMessage(), event.cause());
                }
            }
            semaphore.release();
        });
        try {
            semaphore.tryAcquire(10, SECONDS);
        } catch (InterruptedException e) {
            throw new CdpException(e);
        }
        if (webSocket == null) {
            throw new CdpException("Can not establish WebSocket connection.");
        }
    }
}
