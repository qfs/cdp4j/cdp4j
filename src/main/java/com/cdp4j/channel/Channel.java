// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

public interface Channel {

    public static final short CLOSE_STATUS_CODE = 1000;

    public static final String CLOSE_REASON_TEXT = "done";

    boolean isOpen();

    void disconnect();

    void sendText(String message);

    void sendText(byte[] message);

    void connect();
}
