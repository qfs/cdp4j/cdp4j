// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.concurrent.TimeUnit.SECONDS;

import com.cdp4j.exception.CdpException;
import com.cdp4j.exception.CdpTimeoutException;
import java.net.http.WebSocket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.ReentrantLock;

public class JreWebSocketChannel implements Channel {

    private final ReentrantLock lock = new ReentrantLock(true);

    private final CompletableFuture<WebSocket> future;

    private WebSocket webSocket;

    public JreWebSocketChannel(CompletableFuture<WebSocket> future) {
        this.future = future;
    }

    @Override
    public boolean isOpen() {
        return !webSocket.isInputClosed() && !webSocket.isOutputClosed();
    }

    @Override
    public void disconnect() {
        if (isOpen()) {
            webSocket.sendClose(CLOSE_STATUS_CODE, CLOSE_REASON_TEXT).thenRun(() -> webSocket.abort());
        }
    }

    @Override
    public void sendText(String message) {
        if (!webSocket.isInputClosed()) {
            try {
                if (lock.tryLock(5, SECONDS)) {
                    webSocket.sendText(message, true);
                } else {
                    throw new CdpTimeoutException(message);
                }
            } catch (InterruptedException e) {
                throw new CdpException(e);
            } finally {
                if (lock.isLocked()) {
                    lock.unlock();
                }
            }
        }
    }

    @Override
    public void sendText(byte[] message) {
        this.sendText(new String(message, UTF_8));
    }

    @Override
    public void connect() {
        webSocket = future.join();
    }
}
