// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static io.undertow.websockets.client.WebSocketClient.connectionBuilder;
import static org.xnio.Options.THREAD_DAEMON;
import static org.xnio.Options.USE_DIRECT_BUFFERS;
import static org.xnio.Options.WORKER_IO_THREADS;
import static org.xnio.Options.WORKER_NAME;
import static org.xnio.Options.WORKER_READ_THREADS;
import static org.xnio.Options.WORKER_TASK_MAX_THREADS;

import com.cdp4j.exception.CdpException;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import io.undertow.connector.ByteBufferPool;
import io.undertow.server.DefaultByteBufferPool;
import io.undertow.websockets.client.WebSocketClient.ConnectionBuilder;
import io.undertow.websockets.core.WebSocketChannel;
import java.net.URI;
import java.net.URISyntaxException;
import org.xnio.IoFuture;
import org.xnio.OptionMap;
import org.xnio.Xnio;
import org.xnio.XnioWorker;

public class UndertowWebSocketFactory implements ChannelFactory, AutoCloseable {

    private static final int DEFAULT_POOL_BUFFER_SIZE = 16 * 1024;

    private final XnioWorker worker;

    private final ByteBufferPool pool;

    public UndertowWebSocketFactory() {
        this(false, DEFAULT_POOL_BUFFER_SIZE);
    }

    public UndertowWebSocketFactory(boolean useDirectByteBuffer) {
        this(false, DEFAULT_POOL_BUFFER_SIZE);
    }

    public UndertowWebSocketFactory(boolean useDirectByteBuffer, int poolBufferSize) {
        OptionMap options = OptionMap.builder()
                .set(THREAD_DAEMON, true)
                .set(WORKER_TASK_MAX_THREADS, 1)
                .set(WORKER_IO_THREADS, 1)
                .set(WORKER_NAME, "CdpWebSocketWorker")
                .set(USE_DIRECT_BUFFERS, useDirectByteBuffer)
                .set(WORKER_READ_THREADS, 1)
                .getMap();
        worker = Xnio.getInstance()
                .createWorkerBuilder()
                .populateFromOptions(options)
                .setCoreWorkerPoolSize(1)
                .setMaxWorkerPoolSize(1)
                .setDaemon(true)
                .setWorkerName("CdpWebSocketWorker")
                .build();
        pool = new DefaultByteBufferPool(useDirectByteBuffer, poolBufferSize);
    }

    public UndertowWebSocketFactory(XnioWorker worker, ByteBufferPool pool) {
        this.worker = worker;
        this.pool = pool;
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        String url = connection.getUrl();
        URI uri;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            throw new CdpException(e);
        }
        ConnectionBuilder builder = connectionBuilder(worker, pool, uri);
        IoFuture<WebSocketChannel> future = builder.connect();
        return new UndertowWebSocketChannel(future, factory, handler);
    }

    @Override
    public void close() {
        if (worker != null) {
            worker.shutdownNow();
        }
        if (pool != null) {
            pool.close();
        }
    }
}
