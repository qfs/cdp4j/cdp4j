// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.nio.ByteBuffer.wrap;

import com.cdp4j.exception.CdpException;
import java.util.concurrent.TimeUnit;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.framing.TextFrame;

public class TooTallNateWebSocketChannel implements Channel {

    private final WebSocketClient webSocket;

    public TooTallNateWebSocketChannel(WebSocketClient webSocket) {
        this.webSocket = webSocket;
    }

    @Override
    public boolean isOpen() {
        return webSocket.isOpen();
    }

    @Override
    public void disconnect() {
        if (webSocket.isOpen()) {
            webSocket.close(CLOSE_STATUS_CODE, CLOSE_REASON_TEXT);
        }
    }

    @Override
    public void sendText(String message) {
        webSocket.send(message);
    }

    @Override
    public void sendText(byte[] message) {
        TextFrame frame = new TextFrame();
        frame.setFin(true);
        frame.setPayload(wrap(message));
        webSocket.sendFrame(frame);
    }

    @Override
    public void connect() {
        try {
            webSocket.connectBlocking(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new CdpException(e);
        }
    }
}
