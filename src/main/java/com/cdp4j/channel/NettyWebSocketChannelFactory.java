// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.net.URI.create;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;

public class NettyWebSocketChannelFactory implements ChannelFactory {

    private final EventLoopGroup loop;

    private final boolean performMasking;

    public NettyWebSocketChannelFactory() {
        this(new NioEventLoopGroup(), false);
    }

    public NettyWebSocketChannelFactory(boolean performMasking) {
        this(new NioEventLoopGroup(), performMasking);
    }

    public NettyWebSocketChannelFactory(EventLoopGroup loop) {
        this(loop, false);
    }

    public NettyWebSocketChannelFactory(EventLoopGroup loop, boolean performMasking) {
        this.loop = loop;
        this.performMasking = performMasking;
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        return new NettyWebSocketChannel(factory, handler, create(connection.getUrl()), loop, performMasking);
    }

    public void close() {
        loop.shutdownGracefully(10, 10, MILLISECONDS);
    }
}
