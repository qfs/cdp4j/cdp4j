// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import java.net.URI;
import java.net.URISyntaxException;

public class WebSocketConnection implements Connection {

    private final String url;

    public WebSocketConnection(String url) {
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean isValid() {
        String url = null;
        try {
            url = getUrl();
        } catch (Throwable t) {
            if (url == null) {
                return false;
            }
        }
        try {
            URI uri = new URI(url);
            DevToolsConnection conn = new DevToolsConnection(uri.getPort(), uri.getHost());
            return conn.isValid();
        } catch (URISyntaxException e) {
            return false;
        }
    }

    @Override
    public String toString() {
        return "WebSocketConnection [url=" + url + "]";
    }
}
