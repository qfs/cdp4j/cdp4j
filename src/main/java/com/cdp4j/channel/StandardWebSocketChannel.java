// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.RemoteEndpoint.Async;
import javax.websocket.Session;

public class StandardWebSocketChannel implements Channel {

    private final Session session;

    private final Async remote;

    public StandardWebSocketChannel(Session session) {
        this.session = session;
        this.remote = session.getAsyncRemote();
    }

    @Override
    public boolean isOpen() {
        return session.isOpen();
    }

    @Override
    public void disconnect() {
        if (session.isOpen()) {
            try {
                session.close(new CloseReason(CloseCodes.getCloseCode(CLOSE_STATUS_CODE), CLOSE_REASON_TEXT));
            } catch (IOException e) {
                // ignore
            }
        }
    }

    @Override
    public void sendText(String message) {
        remote.sendText(message);
    }

    @Override
    public void sendText(byte[] message) {
        remote.sendText(new String(message, UTF_8));
    }

    @Override
    public void connect() {
        // no op
    }
}
