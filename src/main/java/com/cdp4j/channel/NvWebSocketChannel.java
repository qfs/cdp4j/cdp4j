// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static com.neovisionaries.ws.client.WebSocketCloseCode.NORMAL;
import static com.neovisionaries.ws.client.WebSocketOpcode.TEXT;

import com.cdp4j.exception.CdpException;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;

public class NvWebSocketChannel implements Channel {

    private final WebSocket webSocket;

    NvWebSocketChannel(WebSocket webSocket) {
        this.webSocket = webSocket;
        this.webSocket.setDirectTextMessage(true);
        this.webSocket.setAutoFlush(true);
    }

    @Override
    public boolean isOpen() {
        return webSocket.isOpen();
    }

    @Override
    public void disconnect() {
        if (webSocket.isOpen()) {
            try {
                webSocket.sendClose(CLOSE_STATUS_CODE, CLOSE_REASON_TEXT);
                webSocket.disconnect(NORMAL, null, 100); // max delay to close ws connection
            } catch (Throwable t) {
                // ignore
            }
        }
    }

    @Override
    public void sendText(String message) {
        webSocket.sendText(message);
    }

    @Override
    public void sendText(byte[] message) {
        WebSocketFrame frame =
                new WebSocketFrame().setPayload(message).setOpcode(TEXT).setFin(true);
        webSocket.sendFrame(frame);
    }

    @Override
    public void connect() {
        try {
            webSocket.connect();
        } catch (WebSocketException e) {
            throw new CdpException(e);
        }
    }
}
