// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.net.URI.create;
import static javax.websocket.ClientEndpointConfig.Builder.create;

import com.cdp4j.exception.CdpException;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

public class StandardWebSocketFactory implements ChannelFactory {

    private final WebSocketContainer webSocketContainer;

    public StandardWebSocketFactory(WebSocketContainer webSocketContainer) {
        this.webSocketContainer = webSocketContainer;
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        try {
            StandardWebSocketListener listener = new StandardWebSocketListener(factory, handler);
            Session session =
                    webSocketContainer.connectToServer(listener, create().build(), create(connection.getUrl()));
            session.addMessageHandler(listener);
            return new StandardWebSocketChannel(session);
        } catch (DeploymentException | IOException e) {
            throw new CdpException(e);
        }
    }
}
