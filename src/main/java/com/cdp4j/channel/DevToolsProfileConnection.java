// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.nio.file.Files.readAllLines;
import static java.util.Collections.emptyList;

import com.cdp4j.exception.CdpException;
import com.cdp4j.serialization.GsonMapper;
import com.cdp4j.serialization.JsonMapper;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class DevToolsProfileConnection implements Connection {

    private final Path userProfileDirectory;

    private final JsonMapper mapper;

    public DevToolsProfileConnection(Path userProfileDirectory) {
        this(userProfileDirectory, new GsonMapper());
    }

    public DevToolsProfileConnection(Path userProfileDirectory, JsonMapper mapper) {
        this.userProfileDirectory = userProfileDirectory;
        this.mapper = mapper;
    }

    protected String getHost() {
        return "127.0.0.1";
    }

    @Override
    public String getUrl() {
        Path devToolsActivePort = userProfileDirectory.resolve("DevToolsActivePort");
        if (!Files.exists(devToolsActivePort)) {
            throw new CdpException("File not found: " + devToolsActivePort.toString());
        }
        List<String> lines = emptyList();
        try {
            lines = readAllLines(devToolsActivePort);
        } catch (IOException e) {
            throw new CdpException(e);
        }
        if (lines.size() < 2) {
            throw new CdpException("Invalid DevToolsActivePort file.");
        }
        int port = parseInt(lines.get(0));
        String connectionUri = lines.get(1);
        if (!connectionUri.startsWith("/")) {
            throw new CdpException("webSocketDebuggerUrl must start with /");
        }
        String webSocketDebuggerUrl = format("ws://%s:%d%s", getHost(), port, connectionUri);
        return webSocketDebuggerUrl;
    }

    @Override
    public boolean isValid() {
        String url = null;
        try {
            url = getUrl();
        } catch (Throwable t) {
            if (url == null) {
                return false;
            }
        }
        try {
            URI uri = new URI(url);
            DevToolsConnection conn = new DevToolsConnection(uri.getPort(), uri.getHost(), mapper);
            return conn.isValid();
        } catch (URISyntaxException e) {
            return false;
        }
    }
}
