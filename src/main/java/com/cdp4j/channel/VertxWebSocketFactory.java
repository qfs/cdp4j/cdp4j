// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.lang.Boolean.FALSE;
import static java.net.URI.create;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.WebSocketConnectOptions;
import java.net.URI;

public class VertxWebSocketFactory implements ChannelFactory {

    private final HttpClient httpClient;

    public VertxWebSocketFactory(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        WebSocketConnectOptions options = new WebSocketConnectOptions();
        URI uri = create(connection.getUrl());
        options.setHost(uri.getHost());
        options.setPort(uri.getPort());
        options.setURI(uri.getPath());
        options.setSsl(FALSE);
        VertxWebSocketChannel channel = new VertxWebSocketChannel(factory, httpClient, handler, options);
        return channel;
    }
}
