// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.lang.Math.min;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

class ByteBufferBackedInputStream extends InputStream {

    private final AutoCloseable closeable;

    private final ByteBuffer buffer;

    private final boolean close;

    public ByteBufferBackedInputStream(AutoCloseable closeable, ByteBuffer buffer, boolean close) {
        this.closeable = closeable;
        this.buffer = buffer;
        this.close = close;
    }

    @Override
    public int available() {
        return buffer.remaining();
    }

    @Override
    public int read() throws IOException {
        return buffer.hasRemaining() ? (buffer.get() & 0xFF) : -1;
    }

    @Override
    public int read(byte[] bytes, int off, int len) throws IOException {
        if (!buffer.hasRemaining()) {
            return -1;
        }
        len = min(len, buffer.remaining());
        buffer.get(bytes, off, len);
        return len;
    }

    @Override
    public long skip(long n) {
        long k = min(n, available());
        if (k < 0) {
            k = 0;
        }
        ((java.nio.Buffer) buffer)
                .position((int) (this.buffer.position() + k)); // Buffer cast required for execution with JDK 8
        return k;
    }

    @Override
    public void reset() throws IOException {
        ((java.nio.Buffer) buffer).position(0); // Buffer cast required for execution with JDK 8
    }

    @Override
    public void close() throws IOException {
        if (close) {
            try {
                closeable.close();
            } catch (Exception e) {
                // ignore
            }
        }
    }
}
