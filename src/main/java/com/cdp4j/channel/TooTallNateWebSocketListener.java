// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import java.net.URI;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class TooTallNateWebSocketListener extends WebSocketClient {

    private final SessionFactory factory;

    private final MessageHandler handler;

    public TooTallNateWebSocketListener(URI serverUri, SessionFactory factory, MessageHandler handler) {
        super(serverUri);
        this.factory = factory;
        this.handler = handler;
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        // ignore
    }

    @Override
    public void onMessage(String message) {
        handler.process(message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        factory.close();
    }

    @Override
    public void onError(Exception ex) {
        // ignore
    }
}
