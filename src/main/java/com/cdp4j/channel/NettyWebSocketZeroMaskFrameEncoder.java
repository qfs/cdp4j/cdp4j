// https://github.com/netty/netty/blob/4.1/LICENSE.txt
package com.cdp4j.channel;

import com.cdp4j.exception.CdpException;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.ContinuationWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrameEncoder;
import java.nio.ByteBuffer;
import java.util.List;

// modified version of io.netty.handler.codec.http.websocketx.WebSocket08FrameEncoder
public class NettyWebSocketZeroMaskFrameEncoder extends MessageToMessageEncoder<WebSocketFrame>
        implements WebSocketFrameEncoder {

    private static final byte OPCODE_CONT = 0x0;
    private static final byte OPCODE_TEXT = 0x1;
    private static final byte OPCODE_BINARY = 0x2;
    private static final byte OPCODE_CLOSE = 0x8;
    private static final byte OPCODE_PING = 0x9;
    private static final byte OPCODE_PONG = 0xA;

    private static byte[] ZERO_MASK = ByteBuffer.allocate(4).putInt(0).array();

    @Override
    protected void encode(ChannelHandlerContext ctx, WebSocketFrame msg, List<Object> out) throws Exception {
        final ByteBuf data = msg.content();

        byte opcode;
        if (msg instanceof TextWebSocketFrame) {
            opcode = OPCODE_TEXT;
        } else if (msg instanceof PingWebSocketFrame) {
            opcode = OPCODE_PING;
        } else if (msg instanceof PongWebSocketFrame) {
            opcode = OPCODE_PONG;
        } else if (msg instanceof CloseWebSocketFrame) {
            opcode = OPCODE_CLOSE;
        } else if (msg instanceof BinaryWebSocketFrame) {
            opcode = OPCODE_BINARY;
        } else if (msg instanceof ContinuationWebSocketFrame) {
            opcode = OPCODE_CONT;
        } else {
            throw new CdpException(
                    "Cannot encode frame of type: " + msg.getClass().getName());
        }

        int length = data.readableBytes();

        int b0 = 0;
        if (msg.isFinalFragment()) {
            b0 |= 1 << 7;
        }
        b0 |= msg.rsv() % 8 << 4;
        b0 |= opcode % 128;

        boolean release = true;
        ByteBuf buf = null;
        try {
            int maskLength = 4;
            if (length <= 125) {
                int size = 2 + maskLength;
                size += length;
                buf = ctx.alloc().buffer(size);
                buf.writeByte(b0);
                byte b = (byte) (0x80 | (byte) length);
                buf.writeByte(b);
            } else if (length <= 0xFFFF) {
                int size = 4 + maskLength;
                size += length;
                buf = ctx.alloc().buffer(size);
                buf.writeByte(b0);
                buf.writeByte(0xFE);
                buf.writeByte(length >>> 8 & 0xFF);
                buf.writeByte(length & 0xFF);
            } else {
                int size = 10 + maskLength;
                size += length;
                buf = ctx.alloc().buffer(size);
                buf.writeByte(b0);
                buf.writeByte(0xFF);
                buf.writeLong(length);
            }

            // Write payload
            buf.writeBytes(ZERO_MASK);

            if (buf.writableBytes() >= data.readableBytes()) {
                // merge buffers as this is cheaper then a gathering write if the payload is small enough
                buf.writeBytes(data);
                out.add(buf);
            } else {
                out.add(buf);
                out.add(data.retain());
            }

            release = false;
        } finally {
            if (release && buf != null) {
                buf.release();
            }
        }
    }
}
