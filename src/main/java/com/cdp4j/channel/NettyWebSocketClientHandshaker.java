// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker13;
import io.netty.handler.codec.http.websocketx.WebSocketFrameEncoder;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;
import java.net.URI;

public class NettyWebSocketClientHandshaker extends WebSocketClientHandshaker13 {

    public NettyWebSocketClientHandshaker(
            URI webSocketURL,
            WebSocketVersion version,
            String subprotocol,
            boolean allowExtensions,
            HttpHeaders customHeaders,
            int maxFramePayloadLength,
            boolean performMasking,
            boolean allowMaskMismatch) {
        super(
                webSocketURL,
                version,
                subprotocol,
                allowExtensions,
                customHeaders,
                maxFramePayloadLength,
                performMasking,
                allowMaskMismatch);
    }

    @Override
    protected WebSocketFrameEncoder newWebSocketEncoder() {
        return new NettyWebSocketZeroMaskFrameEncoder();
    }
}
