// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.net.URI.create;
import static java.net.http.HttpClient.newBuilder;
import static java.time.Duration.ofMillis;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Builder;
import java.net.http.WebSocket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class JreWebSocketFactory implements ChannelFactory {

    private static final int CONNECTION_TIMEOUT = 10_000; // 10 seconds

    private final HttpClient client;

    public JreWebSocketFactory() {
        this(null);
    }

    public JreWebSocketFactory(Executor executor) {
        this(executor, CONNECTION_TIMEOUT);
    }

    public JreWebSocketFactory(Executor executor, int connectionTimeout) {
        Builder builder = newBuilder();
        if (executor != null) {
            builder.executor(executor);
        }
        client = builder.connectTimeout(ofMillis(connectionTimeout)).build();
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        String url = connection.getUrl();
        CompletableFuture<WebSocket> future =
                client.newWebSocketBuilder().buildAsync(create(url), new JreWebSocketListener(factory, handler));
        return new JreWebSocketChannel(future);
    }
}
