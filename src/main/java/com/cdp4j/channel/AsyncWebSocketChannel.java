// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.util.concurrent.CompletableFuture;
import org.asynchttpclient.netty.ws.NettyWebSocket;

class AsyncWebSocketChannel implements Channel {

    private final CompletableFuture<NettyWebSocket> future;

    private NettyWebSocket webSocket;

    public AsyncWebSocketChannel(CompletableFuture<NettyWebSocket> future) {
        this.future = future;
    }

    @Override
    public boolean isOpen() {
        return webSocket.isOpen();
    }

    @Override
    public void disconnect() {
        if (isOpen()) {
            webSocket.sendCloseFrame(CLOSE_STATUS_CODE, CLOSE_REASON_TEXT);
        }
    }

    @Override
    public void sendText(String message) {
        webSocket.sendTextFrame(message);
    }

    @Override
    public void sendText(byte[] message) {
        webSocket.sendTextFrame(new String(message, UTF_8));
    }

    @Override
    public void connect() {
        webSocket = future.join();
    }
}
