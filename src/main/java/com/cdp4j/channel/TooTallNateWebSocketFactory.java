// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.net.URI.create;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;

public class TooTallNateWebSocketFactory implements ChannelFactory, AutoCloseable {

    private TooTallNateWebSocketListener webSocket;

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        if (webSocket != null) {
            throw new IllegalStateException();
        }
        webSocket = new TooTallNateWebSocketListener(create(connection.getUrl()), factory, handler);
        TooTallNateWebSocketChannel channel = new TooTallNateWebSocketChannel(webSocket);
        return channel;
    }

    @Override
    public void close() {
        if (webSocket != null && webSocket.isOpen()) {
            webSocket.close();
        }
    }
}
