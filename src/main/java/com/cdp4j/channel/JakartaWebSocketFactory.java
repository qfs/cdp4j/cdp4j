// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static jakarta.websocket.ClientEndpointConfig.Builder.create;
import static java.net.URI.create;

import com.cdp4j.exception.CdpException;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import jakarta.websocket.DeploymentException;
import jakarta.websocket.Session;
import jakarta.websocket.WebSocketContainer;
import java.io.IOException;

public class JakartaWebSocketFactory implements ChannelFactory {

    private final WebSocketContainer webSocketContainer;

    public JakartaWebSocketFactory(WebSocketContainer webSocketContainer) {
        this.webSocketContainer = webSocketContainer;
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        try {
            JakartaWebSocketListener listener = new JakartaWebSocketListener(factory, handler);
            Session session =
                    webSocketContainer.connectToServer(listener, create().build(), create(connection.getUrl()));
            session.addMessageHandler(listener);
            return new JakartaWebSocketChannel(session);
        } catch (DeploymentException | IOException e) {
            throw new CdpException(e);
        }
    }
}
