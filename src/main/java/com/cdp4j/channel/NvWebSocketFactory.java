// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static com.neovisionaries.ws.client.DualStackMode.IPV4_ONLY;

import com.cdp4j.exception.CdpException;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketFactory;
import java.io.IOException;

public class NvWebSocketFactory implements ChannelFactory, AutoCloseable {

    private static final int CONNECTION_TIMEOUT = 10_000; // 10 seconds

    private final WebSocketFactory factory = new WebSocketFactory();

    private WebSocket webSocket;

    public NvWebSocketFactory() {
        this(CONNECTION_TIMEOUT);
    }

    public NvWebSocketFactory(int connectionTimeout) {
        factory.setConnectionTimeout(connectionTimeout);
        factory.setDualStackMode(IPV4_ONLY);
        factory.setVerifyHostname(false);
        factory.setSocketTimeout(connectionTimeout);
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        if (webSocket != null) {
            throw new IllegalStateException();
        }
        String url = connection.getUrl();
        try {
            webSocket = this.factory.createSocket(url);
        } catch (IOException e) {
            throw new CdpException(e);
        }
        webSocket.addListener(new NvWebSocketListener(factory, handler));
        return new NvWebSocketChannel(webSocket);
    }

    @Override
    public void close() {
        if (webSocket != null && webSocket.isOpen()) {
            webSocket.disconnect();
        }
    }
}
