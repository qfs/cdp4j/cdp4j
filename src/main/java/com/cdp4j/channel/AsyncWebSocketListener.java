// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import org.asynchttpclient.ws.WebSocket;
import org.asynchttpclient.ws.WebSocketListener;

public class AsyncWebSocketListener implements WebSocketListener {

    private final SessionFactory factory;

    private final MessageHandler handler;

    public AsyncWebSocketListener(SessionFactory factory, MessageHandler handler) {
        this.factory = factory;
        this.handler = handler;
    }

    @Override
    public void onTextFrame(String payload, boolean finalFragment, int rsv) {
        handler.process(payload);
    }

    @Override
    public void onOpen(WebSocket websocket) {
        // ignore
    }

    @Override
    public void onClose(WebSocket websocket, int code, String reason) {
        factory.close();
    }

    @Override
    public void onError(Throwable t) {
        // ignore
    }
}
