// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import java.net.http.WebSocket;
import java.net.http.WebSocket.Listener;
import java.util.concurrent.CompletionStage;

public class JreWebSocketListener implements Listener {

    private final SessionFactory factory;

    private final MessageHandler handler;

    private final StringBuilder buffer = new StringBuilder(0);

    public JreWebSocketListener(SessionFactory factory, MessageHandler handler) {
        this.factory = factory;
        this.handler = handler;
    }

    @Override
    public CompletionStage<?> onText(WebSocket webSocket, CharSequence data, boolean last) {
        if (last) {
            if (buffer.length() == 0) {
                handler.process(data.toString());
            } else {
                buffer.append(data);
                String message = buffer.toString();
                buffer.setLength(0);
                handler.process(message);
            }
        } else {
            buffer.append(data);
        }
        webSocket.request(1);
        return null;
    }

    @Override
    public CompletionStage<?> onClose(WebSocket webSocket, int statusCode, String reason) {
        factory.close();
        buffer.setLength(0);
        return Listener.super.onClose(webSocket, statusCode, reason);
    }
}
