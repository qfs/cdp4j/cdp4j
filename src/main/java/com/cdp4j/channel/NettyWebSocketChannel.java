// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static com.cdp4j.channel.ChannelFactory.MAX_PAYLOAD_SIZE;
import static io.netty.buffer.Unpooled.wrappedBuffer;
import static io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory.newHandshaker;
import static io.netty.handler.codec.http.websocketx.WebSocketCloseStatus.NORMAL_CLOSURE;
import static io.netty.handler.codec.http.websocketx.WebSocketVersion.V13;

import com.cdp4j.exception.CdpException;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;
import java.net.URI;

public class NettyWebSocketChannel implements Channel {

    private final Bootstrap bootstrap;

    private final URI uri;

    private io.netty.channel.Channel channel;

    public NettyWebSocketChannel(
            SessionFactory factory, MessageHandler handler, URI uri, EventLoopGroup group, boolean performMasking) {
        this.uri = uri;
        bootstrap = new Bootstrap();
        WebSocketClientHandshaker handshaker = performMasking
                ? newHandshaker(uri, V13, null, false, null, MAX_PAYLOAD_SIZE)
                : new NettyWebSocketClientHandshaker(
                        uri, V13, null, false, null, MAX_PAYLOAD_SIZE, performMasking, performMasking);
        final NettyWebSocketListener webSocketHandler = new NettyWebSocketListener(factory, handler, handshaker);
        bootstrap.group(group).channel(NioSocketChannel.class).handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) {
                ch.pipeline().addLast(new HttpClientCodec(), new HttpObjectAggregator(8192), webSocketHandler);
            }
        });
    }

    @Override
    public boolean isOpen() {
        return channel.isOpen();
    }

    @Override
    public void disconnect() {
        if (channel.isOpen()) {
            try {
                channel.writeAndFlush(new CloseWebSocketFrame(NORMAL_CLOSURE)).sync();
            } catch (InterruptedException e) {
                // ignore
            } finally {
                channel.close();
            }
        }
    }

    @Override
    public void sendText(String message) {
        channel.writeAndFlush(new TextWebSocketFrame(message));
    }

    @Override
    public void sendText(byte[] message) {
        channel.writeAndFlush(new TextWebSocketFrame(wrappedBuffer(message)));
    }

    @Override
    public void connect() {
        try {
            channel = bootstrap.connect(uri.getHost(), uri.getPort()).sync().channel();
        } catch (InterruptedException e) {
            throw new CdpException(e);
        }
    }
}
