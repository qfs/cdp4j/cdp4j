// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedBinaryMessage;
import io.undertow.websockets.core.StreamSourceFrameChannel;
import io.undertow.websockets.core.WebSocketCallback;
import io.undertow.websockets.core.WebSocketChannel;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.xnio.Pooled;

@SuppressWarnings("deprecation")
public class UndertowWebSocketListener extends AbstractReceiveListener {

    private final SessionFactory factory;

    private final MessageHandler handler;

    private static class CdpWebSocketCallback implements WebSocketCallback<BufferedBinaryMessage> {

        private final UndertowWebSocketListener listener;

        CdpWebSocketCallback(UndertowWebSocketListener listener) {
            this.listener = listener;
        }

        @Override
        public void complete(WebSocketChannel channel, BufferedBinaryMessage message) {
            try {
                Pooled<ByteBuffer[]> pooled = message.getData();
                final ByteBuffer[] resources = pooled.getResource();
                if (resources.length == 1) {
                    listener.handler.process(new ByteBufferBackedInputStream(pooled, resources[0], true));
                } else {
                    List<InputStream> streams = new ArrayList<>(resources.length);
                    for (ByteBuffer next : resources) {
                        streams.add(new ByteBufferBackedInputStream(pooled, next, false));
                    }
                    listener.handler.process(new ListInputStream(pooled, streams));
                }
            } catch (Throwable e) {
                listener.onError(channel, e);
            }
        }

        @Override
        public void onError(WebSocketChannel channel, BufferedBinaryMessage context, Throwable throwable) {
            context.getData().close();
            listener.onError(channel, throwable);
        }
    }

    public UndertowWebSocketListener(SessionFactory factory, MessageHandler handler) {
        this.factory = factory;
        this.handler = handler;
    }

    protected void onText(WebSocketChannel webSocketChannel, StreamSourceFrameChannel messageChannel)
            throws IOException {
        BufferedBinaryMessage buffer = new BufferedBinaryMessage(getMaxTextBufferSize(), true);
        buffer.read(messageChannel, new CdpWebSocketCallback(this));
    }

    @Override
    protected void onFullCloseMessage(final WebSocketChannel channel, BufferedBinaryMessage message)
            throws IOException {
        factory.close();
    }
}
