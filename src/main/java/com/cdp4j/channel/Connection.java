// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

public interface Connection {

    String getUrl();

    boolean isValid();
}
