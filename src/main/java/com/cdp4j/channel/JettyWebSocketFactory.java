// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.net.URI.create;

import com.cdp4j.exception.CdpException;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import java.io.IOException;
import java.util.concurrent.Future;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.WebSocketClient;

public class JettyWebSocketFactory implements ChannelFactory, AutoCloseable {

    private final WebSocketClient client;

    public JettyWebSocketFactory() {
        client = new WebSocketClient();
        try {
            client.start();
        } catch (Exception e) {
            throw new CdpException(e);
        }
    }

    public JettyWebSocketFactory(WebSocketClient client) {
        this.client = client;
    }

    @Override
    public Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler) {
        String url = connection.getUrl();
        Future<Session> future = null;
        try {
            future = client.connect(new JettyWebSocketListener(factory, handler), create(url));
        } catch (IOException e) {
            throw new CdpException(e);
        }
        return new JettyWebSocketChannel(future);
    }

    @Override
    public void close() {
        if (client != null && !client.isStopped()) {
            try {
                client.stop();
            } catch (Exception e) {
                // ignore
            }
        }
    }
}
