// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;

public interface ChannelFactory {

    // https://cs.chromium.org/chromium/src/content/browser/devtools/devtools_http_handler.cc?type=cs&q=kSendBufferSizeForDevTools&sq=package:chromium&g=0&l=83
    public static final int MAX_PAYLOAD_SIZE = 256 * 1024 * 1024; // 256Mb

    Channel createChannel(Connection connection, SessionFactory factory, MessageHandler handler);
}
