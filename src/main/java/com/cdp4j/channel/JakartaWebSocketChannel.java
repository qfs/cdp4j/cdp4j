// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.nio.charset.StandardCharsets.UTF_8;

import jakarta.websocket.CloseReason;
import jakarta.websocket.CloseReason.CloseCodes;
import jakarta.websocket.RemoteEndpoint.Async;
import jakarta.websocket.Session;
import java.io.IOException;

public class JakartaWebSocketChannel implements Channel {

    private final Session session;

    private final Async remote;

    public JakartaWebSocketChannel(Session session) {
        this.session = session;
        remote = session.getAsyncRemote();
    }

    @Override
    public boolean isOpen() {
        return session.isOpen();
    }

    @Override
    public void disconnect() {
        if (session.isOpen()) {
            try {
                session.close(new CloseReason(CloseCodes.getCloseCode(CLOSE_STATUS_CODE), CLOSE_REASON_TEXT));
            } catch (IOException e) {
                // ignore
            }
        }
    }

    @Override
    public void sendText(String message) {
        remote.sendText(message);
    }

    @Override
    public void sendText(byte[] message) {
        remote.sendText(new String(message, UTF_8));
    }

    @Override
    public void connect() {
        // no op
    }
}
