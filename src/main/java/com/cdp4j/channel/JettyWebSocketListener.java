// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static com.cdp4j.channel.ChannelFactory.MAX_PAYLOAD_SIZE;

import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.CdpLoggerFactory;
import com.cdp4j.session.MessageHandler;
import com.cdp4j.session.SessionFactory;
import java.nio.channels.ClosedChannelException;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket(maxTextMessageSize = MAX_PAYLOAD_SIZE)
public class JettyWebSocketListener {

    private final SessionFactory factory;

    private final MessageHandler handler;

    private final CdpLogger log;

    public JettyWebSocketListener(SessionFactory factory, MessageHandler handler) {
        this.factory = factory;
        this.handler = handler;
        log = new CdpLoggerFactory(factory.getOptions()).getLogger("cdp.jetty");
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        factory.close();
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        if (!(t instanceof ClosedChannelException)) {
            log.error(t.getMessage(), t);
        }
    }

    @OnWebSocketMessage
    public void onMessage(String data) {
        handler.process(data);
    }
}
