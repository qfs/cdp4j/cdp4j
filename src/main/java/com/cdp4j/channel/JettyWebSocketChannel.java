// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.concurrent.TimeUnit.SECONDS;

import com.cdp4j.exception.CdpException;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;

public class JettyWebSocketChannel implements Channel {

    private static final int DEFAULT_TIMEOUT = 10;

    private final Future<Session> future;

    private Session webSocket;

    private RemoteEndpoint remote;

    public JettyWebSocketChannel(Future<Session> future) {
        this.future = future;
    }

    @Override
    public boolean isOpen() {
        return webSocket.isOpen();
    }

    @Override
    public void disconnect() {
        if (webSocket.isOpen()) {
            webSocket.close(CLOSE_STATUS_CODE, CLOSE_REASON_TEXT);
            webSocket.disconnect();
        }
    }

    @Override
    public void sendText(String message) {
        try {
            remote.sendString(message);
        } catch (IOException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public void sendText(byte[] message) {
        sendText(new String(message, UTF_8));
    }

    @Override
    public void connect() {
        try {
            this.webSocket = future.get(DEFAULT_TIMEOUT, SECONDS);
            remote = webSocket.getRemote();
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new CdpException(e);
        }
    }
}
