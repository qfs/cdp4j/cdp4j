// SPDX-License-Identifier: MIT
package com.cdp4j.channel;

import java.io.IOException;

public interface InputStreamExtension {

    byte[] readLastNBytes(int length) throws IOException;
}
