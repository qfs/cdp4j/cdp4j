// SPDX-License-Identifier: MIT
package com.cdp4j;

/**
 * You can use this enum to choose the Chrome version that you want to use.
 *
 * This option is available for only the Windows platform and ignored for GNU/Linux and macOS.
 */
public enum Browser {
    /**
     * cdp4j search the location of Chrome and launches the found browser.
     *
     * cdp4j uses the below list for the search order:
     *
     * 1. Chrome Dev
     * 2. Chrome Canary
     * 3. Chrome
     * 4. Microsoft Edge (on Windows)
     *
     * @see {@link Launcher#getChromeWinPaths()}
     */
    Any,
    Chrome,
    ChromeCanary,
    ChromeDev,
    MicrosoftEdge
}
