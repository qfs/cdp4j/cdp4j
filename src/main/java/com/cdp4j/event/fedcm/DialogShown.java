// SPDX-License-Identifier: MIT
package com.cdp4j.event.fedcm;

import com.cdp4j.type.fedcm.Account;
import com.cdp4j.type.fedcm.DialogType;
import java.util.List;

public class DialogShown {
    private String dialogId;

    private DialogType dialogType;

    private List<Account> accounts;

    private String title;

    private String subtitle;

    public String getDialogId() {
        return dialogId;
    }

    public void setDialogId(String dialogId) {
        this.dialogId = dialogId;
    }

    public DialogType getDialogType() {
        return dialogType;
    }

    public void setDialogType(DialogType dialogType) {
        this.dialogType = dialogType;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    /**
     * These exist primarily so that the caller can verify the RP context was used
     * appropriately.
     */
    public String getTitle() {
        return title;
    }

    /**
     * These exist primarily so that the caller can verify the RP context was used
     * appropriately.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String toString() {
        return "DialogShown [dialogId=" + dialogId + ", dialogType=" + dialogType + ", accounts=" + accounts
                + ", title=" + title + ", subtitle=" + subtitle + "]";
    }
}
