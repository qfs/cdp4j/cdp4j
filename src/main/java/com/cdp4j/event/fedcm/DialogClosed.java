// SPDX-License-Identifier: MIT
package com.cdp4j.event.fedcm;

/**
 * Triggered when a dialog is closed, either by user action, JS abort, or a
 * command below.
 */
public class DialogClosed {
    private String dialogId;

    public String getDialogId() {
        return dialogId;
    }

    public void setDialogId(String dialogId) {
        this.dialogId = dialogId;
    }

    public String toString() {
        return "DialogClosed [dialogId=" + dialogId + "]";
    }
}
