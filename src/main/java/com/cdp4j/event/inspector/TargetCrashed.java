// SPDX-License-Identifier: MIT
package com.cdp4j.event.inspector;

/**
 * Fired when debugging target has crashed
 */
public class TargetCrashed {}
