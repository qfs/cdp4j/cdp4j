// SPDX-License-Identifier: MIT
package com.cdp4j.event.inspector;

/**
 * Fired when debugging target has reloaded after crash
 */
public class TargetReloadedAfterCrash {}
