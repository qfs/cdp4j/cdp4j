// SPDX-License-Identifier: MIT
package com.cdp4j.event.audits;

import com.cdp4j.type.audits.InspectorIssue;

public class IssueAdded {
    private InspectorIssue issue;

    public InspectorIssue getIssue() {
        return issue;
    }

    public void setIssue(InspectorIssue issue) {
        this.issue = issue;
    }

    public String toString() {
        return "IssueAdded [issue=" + issue + "]";
    }
}
