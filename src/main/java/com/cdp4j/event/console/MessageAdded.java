// SPDX-License-Identifier: MIT
package com.cdp4j.event.console;

import com.cdp4j.type.console.ConsoleMessage;

/**
 * Issued when new console message is added.
 */
public class MessageAdded {
    private ConsoleMessage message;

    /**
     * Console message that has been added.
     */
    public ConsoleMessage getMessage() {
        return message;
    }

    /**
     * Console message that has been added.
     */
    public void setMessage(ConsoleMessage message) {
        this.message = message;
    }

    public String toString() {
        return "MessageAdded [message=" + message + "]";
    }
}
