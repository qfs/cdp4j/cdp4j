// SPDX-License-Identifier: MIT
package com.cdp4j.event.performancetimeline;

import com.cdp4j.type.performancetimeline.TimelineEvent;

/**
 * Sent when a performance timeline event is added. See
 * reportPerformanceTimeline method.
 */
public class TimelineEventAdded {
    private TimelineEvent event;

    public TimelineEvent getEvent() {
        return event;
    }

    public void setEvent(TimelineEvent event) {
        this.event = event;
    }

    public String toString() {
        return "TimelineEventAdded [event=" + event + "]";
    }
}
