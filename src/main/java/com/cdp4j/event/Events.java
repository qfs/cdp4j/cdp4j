// SPDX-License-Identifier: MIT
package com.cdp4j.event;

import com.cdp4j.event.accessibility.LoadComplete;
import com.cdp4j.event.accessibility.NodesUpdated;
import com.cdp4j.event.animation.AnimationCanceled;
import com.cdp4j.event.animation.AnimationCreated;
import com.cdp4j.event.animation.AnimationStarted;
import com.cdp4j.event.animation.AnimationUpdated;
import com.cdp4j.event.audits.IssueAdded;
import com.cdp4j.event.autofill.AddressFormFilled;
import com.cdp4j.event.backgroundservice.BackgroundServiceEventReceived;
import com.cdp4j.event.backgroundservice.RecordingStateChanged;
import com.cdp4j.event.browser.DownloadProgress;
import com.cdp4j.event.browser.DownloadWillBegin;
import com.cdp4j.event.cast.IssueUpdated;
import com.cdp4j.event.cast.SinksUpdated;
import com.cdp4j.event.console.MessageAdded;
import com.cdp4j.event.css.ComputedStyleUpdated;
import com.cdp4j.event.css.FontsUpdated;
import com.cdp4j.event.css.MediaQueryResultChanged;
import com.cdp4j.event.css.StyleSheetAdded;
import com.cdp4j.event.css.StyleSheetChanged;
import com.cdp4j.event.css.StyleSheetRemoved;
import com.cdp4j.event.debugger.BreakpointResolved;
import com.cdp4j.event.debugger.Paused;
import com.cdp4j.event.debugger.Resumed;
import com.cdp4j.event.debugger.ScriptFailedToParse;
import com.cdp4j.event.debugger.ScriptParsed;
import com.cdp4j.event.deviceaccess.DeviceRequestPrompted;
import com.cdp4j.event.dom.AttributeModified;
import com.cdp4j.event.dom.AttributeRemoved;
import com.cdp4j.event.dom.CharacterDataModified;
import com.cdp4j.event.dom.ChildNodeCountUpdated;
import com.cdp4j.event.dom.ChildNodeInserted;
import com.cdp4j.event.dom.ChildNodeRemoved;
import com.cdp4j.event.dom.DistributedNodesUpdated;
import com.cdp4j.event.dom.DocumentUpdated;
import com.cdp4j.event.dom.InlineStyleInvalidated;
import com.cdp4j.event.dom.PseudoElementAdded;
import com.cdp4j.event.dom.PseudoElementRemoved;
import com.cdp4j.event.dom.ScrollableFlagUpdated;
import com.cdp4j.event.dom.SetChildNodes;
import com.cdp4j.event.dom.ShadowRootPopped;
import com.cdp4j.event.dom.ShadowRootPushed;
import com.cdp4j.event.dom.TopLayerElementsUpdated;
import com.cdp4j.event.domstorage.DomStorageItemAdded;
import com.cdp4j.event.domstorage.DomStorageItemRemoved;
import com.cdp4j.event.domstorage.DomStorageItemUpdated;
import com.cdp4j.event.domstorage.DomStorageItemsCleared;
import com.cdp4j.event.emulation.VirtualTimeBudgetExpired;
import com.cdp4j.event.fedcm.DialogClosed;
import com.cdp4j.event.fedcm.DialogShown;
import com.cdp4j.event.fetch.AuthRequired;
import com.cdp4j.event.fetch.RequestPaused;
import com.cdp4j.event.heapprofiler.AddHeapSnapshotChunk;
import com.cdp4j.event.heapprofiler.HeapStatsUpdate;
import com.cdp4j.event.heapprofiler.LastSeenObjectId;
import com.cdp4j.event.heapprofiler.ReportHeapSnapshotProgress;
import com.cdp4j.event.heapprofiler.ResetProfiles;
import com.cdp4j.event.input.DragIntercepted;
import com.cdp4j.event.inspector.Detached;
import com.cdp4j.event.inspector.TargetCrashed;
import com.cdp4j.event.inspector.TargetReloadedAfterCrash;
import com.cdp4j.event.layertree.LayerPainted;
import com.cdp4j.event.layertree.LayerTreeDidChange;
import com.cdp4j.event.log.EntryAdded;
import com.cdp4j.event.media.PlayerErrorsRaised;
import com.cdp4j.event.media.PlayerEventsAdded;
import com.cdp4j.event.media.PlayerMessagesLogged;
import com.cdp4j.event.media.PlayerPropertiesChanged;
import com.cdp4j.event.media.PlayersCreated;
import com.cdp4j.event.network.DataReceived;
import com.cdp4j.event.network.EventSourceMessageReceived;
import com.cdp4j.event.network.LoadingFailed;
import com.cdp4j.event.network.LoadingFinished;
import com.cdp4j.event.network.PolicyUpdated;
import com.cdp4j.event.network.ReportingApiEndpointsChangedForOrigin;
import com.cdp4j.event.network.ReportingApiReportAdded;
import com.cdp4j.event.network.ReportingApiReportUpdated;
import com.cdp4j.event.network.RequestIntercepted;
import com.cdp4j.event.network.RequestServedFromCache;
import com.cdp4j.event.network.RequestWillBeSent;
import com.cdp4j.event.network.RequestWillBeSentExtraInfo;
import com.cdp4j.event.network.ResourceChangedPriority;
import com.cdp4j.event.network.ResponseReceived;
import com.cdp4j.event.network.ResponseReceivedEarlyHints;
import com.cdp4j.event.network.ResponseReceivedExtraInfo;
import com.cdp4j.event.network.SignedExchangeReceived;
import com.cdp4j.event.network.SubresourceWebBundleInnerResponseError;
import com.cdp4j.event.network.SubresourceWebBundleInnerResponseParsed;
import com.cdp4j.event.network.SubresourceWebBundleMetadataError;
import com.cdp4j.event.network.SubresourceWebBundleMetadataReceived;
import com.cdp4j.event.network.TrustTokenOperationDone;
import com.cdp4j.event.network.WebSocketClosed;
import com.cdp4j.event.network.WebSocketCreated;
import com.cdp4j.event.network.WebSocketFrameError;
import com.cdp4j.event.network.WebSocketFrameReceived;
import com.cdp4j.event.network.WebSocketFrameSent;
import com.cdp4j.event.network.WebSocketHandshakeResponseReceived;
import com.cdp4j.event.network.WebSocketWillSendHandshakeRequest;
import com.cdp4j.event.network.WebTransportClosed;
import com.cdp4j.event.network.WebTransportConnectionEstablished;
import com.cdp4j.event.network.WebTransportCreated;
import com.cdp4j.event.overlay.InspectModeCanceled;
import com.cdp4j.event.overlay.InspectNodeRequested;
import com.cdp4j.event.overlay.NodeHighlightRequested;
import com.cdp4j.event.overlay.ScreenshotRequested;
import com.cdp4j.event.page.BackForwardCacheNotUsed;
import com.cdp4j.event.page.CompilationCacheProduced;
import com.cdp4j.event.page.DocumentOpened;
import com.cdp4j.event.page.DomContentEventFired;
import com.cdp4j.event.page.FileChooserOpened;
import com.cdp4j.event.page.FrameAttached;
import com.cdp4j.event.page.FrameClearedScheduledNavigation;
import com.cdp4j.event.page.FrameDetached;
import com.cdp4j.event.page.FrameNavigated;
import com.cdp4j.event.page.FrameRequestedNavigation;
import com.cdp4j.event.page.FrameResized;
import com.cdp4j.event.page.FrameScheduledNavigation;
import com.cdp4j.event.page.FrameStartedLoading;
import com.cdp4j.event.page.FrameStartedNavigating;
import com.cdp4j.event.page.FrameStoppedLoading;
import com.cdp4j.event.page.FrameSubtreeWillBeDetached;
import com.cdp4j.event.page.InterstitialHidden;
import com.cdp4j.event.page.InterstitialShown;
import com.cdp4j.event.page.JavascriptDialogClosed;
import com.cdp4j.event.page.JavascriptDialogOpening;
import com.cdp4j.event.page.LifecycleEvent;
import com.cdp4j.event.page.LoadEventFired;
import com.cdp4j.event.page.NavigatedWithinDocument;
import com.cdp4j.event.page.ScreencastFrame;
import com.cdp4j.event.page.ScreencastVisibilityChanged;
import com.cdp4j.event.page.WindowOpen;
import com.cdp4j.event.performance.Metrics;
import com.cdp4j.event.performancetimeline.TimelineEventAdded;
import com.cdp4j.event.preload.PrefetchStatusUpdated;
import com.cdp4j.event.preload.PreloadEnabledStateUpdated;
import com.cdp4j.event.preload.PreloadingAttemptSourcesUpdated;
import com.cdp4j.event.preload.PrerenderStatusUpdated;
import com.cdp4j.event.preload.RuleSetRemoved;
import com.cdp4j.event.preload.RuleSetUpdated;
import com.cdp4j.event.profiler.ConsoleProfileFinished;
import com.cdp4j.event.profiler.ConsoleProfileStarted;
import com.cdp4j.event.profiler.PreciseCoverageDeltaUpdate;
import com.cdp4j.event.runtime.BindingCalled;
import com.cdp4j.event.runtime.ConsoleAPICalled;
import com.cdp4j.event.runtime.ExceptionRevoked;
import com.cdp4j.event.runtime.ExceptionThrown;
import com.cdp4j.event.runtime.ExecutionContextCreated;
import com.cdp4j.event.runtime.ExecutionContextDestroyed;
import com.cdp4j.event.runtime.ExecutionContextsCleared;
import com.cdp4j.event.runtime.InspectRequested;
import com.cdp4j.event.security.CertificateError;
import com.cdp4j.event.security.SecurityStateChanged;
import com.cdp4j.event.security.VisibleSecurityStateChanged;
import com.cdp4j.event.serviceworker.WorkerErrorReported;
import com.cdp4j.event.serviceworker.WorkerRegistrationUpdated;
import com.cdp4j.event.serviceworker.WorkerVersionUpdated;
import com.cdp4j.event.storage.AttributionReportingSourceRegistered;
import com.cdp4j.event.storage.AttributionReportingTriggerRegistered;
import com.cdp4j.event.storage.CacheStorageContentUpdated;
import com.cdp4j.event.storage.CacheStorageListUpdated;
import com.cdp4j.event.storage.IndexedDBContentUpdated;
import com.cdp4j.event.storage.IndexedDBListUpdated;
import com.cdp4j.event.storage.InterestGroupAccessed;
import com.cdp4j.event.storage.InterestGroupAuctionEventOccurred;
import com.cdp4j.event.storage.InterestGroupAuctionNetworkRequestCreated;
import com.cdp4j.event.storage.SharedStorageAccessed;
import com.cdp4j.event.storage.StorageBucketCreatedOrUpdated;
import com.cdp4j.event.storage.StorageBucketDeleted;
import com.cdp4j.event.target.AttachedToTarget;
import com.cdp4j.event.target.DetachedFromTarget;
import com.cdp4j.event.target.ReceivedMessageFromTarget;
import com.cdp4j.event.target.TargetCreated;
import com.cdp4j.event.target.TargetDestroyed;
import com.cdp4j.event.target.TargetInfoChanged;
import com.cdp4j.event.tethering.Accepted;
import com.cdp4j.event.tracing.BufferUsage;
import com.cdp4j.event.tracing.DataCollected;
import com.cdp4j.event.tracing.TracingComplete;
import com.cdp4j.event.webaudio.AudioListenerCreated;
import com.cdp4j.event.webaudio.AudioListenerWillBeDestroyed;
import com.cdp4j.event.webaudio.AudioNodeCreated;
import com.cdp4j.event.webaudio.AudioNodeWillBeDestroyed;
import com.cdp4j.event.webaudio.AudioParamCreated;
import com.cdp4j.event.webaudio.AudioParamWillBeDestroyed;
import com.cdp4j.event.webaudio.ContextChanged;
import com.cdp4j.event.webaudio.ContextCreated;
import com.cdp4j.event.webaudio.ContextWillBeDestroyed;
import com.cdp4j.event.webaudio.NodeParamConnected;
import com.cdp4j.event.webaudio.NodeParamDisconnected;
import com.cdp4j.event.webaudio.NodesConnected;
import com.cdp4j.event.webaudio.NodesDisconnected;
import com.cdp4j.event.webauthn.CredentialAdded;
import com.cdp4j.event.webauthn.CredentialAsserted;
import com.cdp4j.event.webauthn.CredentialDeleted;
import com.cdp4j.event.webauthn.CredentialUpdated;

@SuppressWarnings("rawtypes")
public enum Events {
    /**
     * The loadComplete event mirrors the load complete event sent by the browser to assistive
     * technology when the web page has finished loading.
     */
    AccessibilityLoadComplete("Accessibility", "loadComplete", LoadComplete.class),

    /**
     * The nodesUpdated event is sent every time a previously requested node has changed the in tree.
     */
    AccessibilityNodesUpdated("Accessibility", "nodesUpdated", NodesUpdated.class),

    /**
     * Event for when an animation has been cancelled.
     */
    AnimationAnimationCanceled("Animation", "animationCanceled", AnimationCanceled.class),

    /**
     * Event for each animation that has been created.
     */
    AnimationAnimationCreated("Animation", "animationCreated", AnimationCreated.class),

    /**
     * Event for animation that has been started.
     */
    AnimationAnimationStarted("Animation", "animationStarted", AnimationStarted.class),

    /**
     * Event for animation that has been updated.
     */
    AnimationAnimationUpdated("Animation", "animationUpdated", AnimationUpdated.class),

    AuditsIssueAdded("Audits", "issueAdded", IssueAdded.class),

    /**
     * Emitted when an address form is filled.
     */
    AutofillAddressFormFilled("Autofill", "addressFormFilled", AddressFormFilled.class),

    /**
     * Called when the recording state for the service has been updated.
     */
    BackgroundServiceRecordingStateChanged("BackgroundService", "recordingStateChanged", RecordingStateChanged.class),

    /**
     * Called with all existing backgroundServiceEvents when enabled, and all new
     * events afterwards if enabled and recording.
     */
    BackgroundServiceBackgroundServiceEventReceived(
            "BackgroundService", "backgroundServiceEventReceived", BackgroundServiceEventReceived.class),

    /**
     * Fired when page is about to start a download.
     */
    BrowserDownloadWillBegin("Browser", "downloadWillBegin", DownloadWillBegin.class),

    /**
     * Fired when download makes progress. Last call has |done| == true.
     */
    BrowserDownloadProgress("Browser", "downloadProgress", DownloadProgress.class),

    /**
     * Fires whenever a web font is updated.  A non-empty font parameter indicates a successfully loaded
     * web font.
     */
    CSSFontsUpdated("CSS", "fontsUpdated", FontsUpdated.class),

    /**
     * Fires whenever a MediaQuery result changes (for example, after a browser window has been
     * resized.) The current implementation considers only viewport-dependent media features.
     */
    CSSMediaQueryResultChanged("CSS", "mediaQueryResultChanged", MediaQueryResultChanged.class),

    /**
     * Fired whenever an active document stylesheet is added.
     */
    CSSStyleSheetAdded("CSS", "styleSheetAdded", StyleSheetAdded.class),

    /**
     * Fired whenever a stylesheet is changed as a result of the client operation.
     */
    CSSStyleSheetChanged("CSS", "styleSheetChanged", StyleSheetChanged.class),

    /**
     * Fired whenever an active document stylesheet is removed.
     */
    CSSStyleSheetRemoved("CSS", "styleSheetRemoved", StyleSheetRemoved.class),

    CSSComputedStyleUpdated("CSS", "computedStyleUpdated", ComputedStyleUpdated.class),

    /**
     * This is fired whenever the list of available sinks changes. A sink is a
     * device or a software surface that you can cast to.
     */
    CastSinksUpdated("Cast", "sinksUpdated", SinksUpdated.class),

    /**
     * This is fired whenever the outstanding issue/error message changes.
     * |issueMessage| is empty if there is no issue.
     */
    CastIssueUpdated("Cast", "issueUpdated", IssueUpdated.class),

    /**
     * Fired when Element's attribute is modified.
     */
    DOMAttributeModified("DOM", "attributeModified", AttributeModified.class),

    /**
     * Fired when Element's attribute is removed.
     */
    DOMAttributeRemoved("DOM", "attributeRemoved", AttributeRemoved.class),

    /**
     * Mirrors DOMCharacterDataModified event.
     */
    DOMCharacterDataModified("DOM", "characterDataModified", CharacterDataModified.class),

    /**
     * Fired when Container's child node count has changed.
     */
    DOMChildNodeCountUpdated("DOM", "childNodeCountUpdated", ChildNodeCountUpdated.class),

    /**
     * Mirrors DOMNodeInserted event.
     */
    DOMChildNodeInserted("DOM", "childNodeInserted", ChildNodeInserted.class),

    /**
     * Mirrors DOMNodeRemoved event.
     */
    DOMChildNodeRemoved("DOM", "childNodeRemoved", ChildNodeRemoved.class),

    /**
     * Called when distribution is changed.
     */
    DOMDistributedNodesUpdated("DOM", "distributedNodesUpdated", DistributedNodesUpdated.class),

    /**
     * Fired when Document has been totally updated. Node ids are no longer valid.
     */
    DOMDocumentUpdated("DOM", "documentUpdated", DocumentUpdated.class),

    /**
     * Fired when Element's inline style is modified via a CSS property modification.
     */
    DOMInlineStyleInvalidated("DOM", "inlineStyleInvalidated", InlineStyleInvalidated.class),

    /**
     * Called when a pseudo element is added to an element.
     */
    DOMPseudoElementAdded("DOM", "pseudoElementAdded", PseudoElementAdded.class),

    /**
     * Called when top layer elements are changed.
     */
    DOMTopLayerElementsUpdated("DOM", "topLayerElementsUpdated", TopLayerElementsUpdated.class),

    /**
     * Fired when a node's scrollability state changes.
     */
    DOMScrollableFlagUpdated("DOM", "scrollableFlagUpdated", ScrollableFlagUpdated.class),

    /**
     * Called when a pseudo element is removed from an element.
     */
    DOMPseudoElementRemoved("DOM", "pseudoElementRemoved", PseudoElementRemoved.class),

    /**
     * Fired when backend wants to provide client with the missing DOM structure. This happens upon
     * most of the calls requesting node ids.
     */
    DOMSetChildNodes("DOM", "setChildNodes", SetChildNodes.class),

    /**
     * Called when shadow root is popped from the element.
     */
    DOMShadowRootPopped("DOM", "shadowRootPopped", ShadowRootPopped.class),

    /**
     * Called when shadow root is pushed into the element.
     */
    DOMShadowRootPushed("DOM", "shadowRootPushed", ShadowRootPushed.class),

    DOMStorageDomStorageItemAdded("DOMStorage", "domStorageItemAdded", DomStorageItemAdded.class),

    DOMStorageDomStorageItemRemoved("DOMStorage", "domStorageItemRemoved", DomStorageItemRemoved.class),

    DOMStorageDomStorageItemUpdated("DOMStorage", "domStorageItemUpdated", DomStorageItemUpdated.class),

    DOMStorageDomStorageItemsCleared("DOMStorage", "domStorageItemsCleared", DomStorageItemsCleared.class),

    /**
     * Notification sent after the virtual time budget for the current VirtualTimePolicy has run out.
     */
    EmulationVirtualTimeBudgetExpired("Emulation", "virtualTimeBudgetExpired", VirtualTimeBudgetExpired.class),

    /**
     * Emitted only when Input.setInterceptDrags is enabled. Use this data with Input.dispatchDragEvent to
     * restore normal drag and drop behavior.
     */
    InputDragIntercepted("Input", "dragIntercepted", DragIntercepted.class),

    /**
     * Fired when remote debugging connection is about to be terminated. Contains detach reason.
     */
    InspectorDetached("Inspector", "detached", Detached.class),

    /**
     * Fired when debugging target has crashed
     */
    InspectorTargetCrashed("Inspector", "targetCrashed", TargetCrashed.class),

    /**
     * Fired when debugging target has reloaded after crash
     */
    InspectorTargetReloadedAfterCrash("Inspector", "targetReloadedAfterCrash", TargetReloadedAfterCrash.class),

    LayerTreeLayerPainted("LayerTree", "layerPainted", LayerPainted.class),

    LayerTreeLayerTreeDidChange("LayerTree", "layerTreeDidChange", LayerTreeDidChange.class),

    /**
     * Issued when new message was logged.
     */
    LogEntryAdded("Log", "entryAdded", EntryAdded.class),

    /**
     * Fired when data chunk was received over the network.
     */
    NetworkDataReceived("Network", "dataReceived", DataReceived.class),

    /**
     * Fired when EventSource message is received.
     */
    NetworkEventSourceMessageReceived("Network", "eventSourceMessageReceived", EventSourceMessageReceived.class),

    /**
     * Fired when HTTP request has failed to load.
     */
    NetworkLoadingFailed("Network", "loadingFailed", LoadingFailed.class),

    /**
     * Fired when HTTP request has finished loading.
     */
    NetworkLoadingFinished("Network", "loadingFinished", LoadingFinished.class),

    /**
     * Details of an intercepted HTTP request, which must be either allowed, blocked, modified or
     * mocked.
     * Deprecated, use Fetch.requestPaused instead.
     */
    NetworkRequestIntercepted("Network", "requestIntercepted", RequestIntercepted.class),

    /**
     * Fired if request ended up loading from cache.
     */
    NetworkRequestServedFromCache("Network", "requestServedFromCache", RequestServedFromCache.class),

    /**
     * Fired when page is about to send HTTP request.
     */
    NetworkRequestWillBeSent("Network", "requestWillBeSent", RequestWillBeSent.class),

    /**
     * Fired when resource loading priority is changed
     */
    NetworkResourceChangedPriority("Network", "resourceChangedPriority", ResourceChangedPriority.class),

    /**
     * Fired when a signed exchange was received over the network
     */
    NetworkSignedExchangeReceived("Network", "signedExchangeReceived", SignedExchangeReceived.class),

    /**
     * Fired when HTTP response is available.
     */
    NetworkResponseReceived("Network", "responseReceived", ResponseReceived.class),

    /**
     * Fired when WebSocket is closed.
     */
    NetworkWebSocketClosed("Network", "webSocketClosed", WebSocketClosed.class),

    /**
     * Fired upon WebSocket creation.
     */
    NetworkWebSocketCreated("Network", "webSocketCreated", WebSocketCreated.class),

    /**
     * Fired when WebSocket message error occurs.
     */
    NetworkWebSocketFrameError("Network", "webSocketFrameError", WebSocketFrameError.class),

    /**
     * Fired when WebSocket message is received.
     */
    NetworkWebSocketFrameReceived("Network", "webSocketFrameReceived", WebSocketFrameReceived.class),

    /**
     * Fired when WebSocket message is sent.
     */
    NetworkWebSocketFrameSent("Network", "webSocketFrameSent", WebSocketFrameSent.class),

    /**
     * Fired when WebSocket handshake response becomes available.
     */
    NetworkWebSocketHandshakeResponseReceived(
            "Network", "webSocketHandshakeResponseReceived", WebSocketHandshakeResponseReceived.class),

    /**
     * Fired when WebSocket is about to initiate handshake.
     */
    NetworkWebSocketWillSendHandshakeRequest(
            "Network", "webSocketWillSendHandshakeRequest", WebSocketWillSendHandshakeRequest.class),

    /**
     * Fired upon WebTransport creation.
     */
    NetworkWebTransportCreated("Network", "webTransportCreated", WebTransportCreated.class),

    /**
     * Fired when WebTransport handshake is finished.
     */
    NetworkWebTransportConnectionEstablished(
            "Network", "webTransportConnectionEstablished", WebTransportConnectionEstablished.class),

    /**
     * Fired when WebTransport is disposed.
     */
    NetworkWebTransportClosed("Network", "webTransportClosed", WebTransportClosed.class),

    /**
     * Fired when additional information about a requestWillBeSent event is available from the
     * network stack. Not every requestWillBeSent event will have an additional
     * requestWillBeSentExtraInfo fired for it, and there is no guarantee whether requestWillBeSent
     * or requestWillBeSentExtraInfo will be fired first for the same request.
     */
    NetworkRequestWillBeSentExtraInfo("Network", "requestWillBeSentExtraInfo", RequestWillBeSentExtraInfo.class),

    /**
     * Fired when additional information about a responseReceived event is available from the network
     * stack. Not every responseReceived event will have an additional responseReceivedExtraInfo for
     * it, and responseReceivedExtraInfo may be fired before or after responseReceived.
     */
    NetworkResponseReceivedExtraInfo("Network", "responseReceivedExtraInfo", ResponseReceivedExtraInfo.class),

    /**
     * Fired when 103 Early Hints headers is received in addition to the common response.
     * Not every responseReceived event will have an responseReceivedEarlyHints fired.
     * Only one responseReceivedEarlyHints may be fired for eached responseReceived event.
     */
    NetworkResponseReceivedEarlyHints("Network", "responseReceivedEarlyHints", ResponseReceivedEarlyHints.class),

    /**
     * Fired exactly once for each Trust Token operation. Depending on
     * the type of the operation and whether the operation succeeded or
     * failed, the event is fired before the corresponding request was sent
     * or after the response was received.
     */
    NetworkTrustTokenOperationDone("Network", "trustTokenOperationDone", TrustTokenOperationDone.class),

    /**
     * Fired once security policy has been updated.
     */
    NetworkPolicyUpdated("Network", "policyUpdated", PolicyUpdated.class),

    /**
     * Fired once when parsing the .wbn file has succeeded.
     * The event contains the information about the web bundle contents.
     */
    NetworkSubresourceWebBundleMetadataReceived(
            "Network", "subresourceWebBundleMetadataReceived", SubresourceWebBundleMetadataReceived.class),

    /**
     * Fired once when parsing the .wbn file has failed.
     */
    NetworkSubresourceWebBundleMetadataError(
            "Network", "subresourceWebBundleMetadataError", SubresourceWebBundleMetadataError.class),

    /**
     * Fired when handling requests for resources within a .wbn file.
     * Note: this will only be fired for resources that are requested by the webpage.
     */
    NetworkSubresourceWebBundleInnerResponseParsed(
            "Network", "subresourceWebBundleInnerResponseParsed", SubresourceWebBundleInnerResponseParsed.class),

    /**
     * Fired when request for resources within a .wbn file failed.
     */
    NetworkSubresourceWebBundleInnerResponseError(
            "Network", "subresourceWebBundleInnerResponseError", SubresourceWebBundleInnerResponseError.class),

    /**
     * Is sent whenever a new report is added.
     * And after 'enableReportingApi' for all existing reports.
     */
    NetworkReportingApiReportAdded("Network", "reportingApiReportAdded", ReportingApiReportAdded.class),

    NetworkReportingApiReportUpdated("Network", "reportingApiReportUpdated", ReportingApiReportUpdated.class),

    NetworkReportingApiEndpointsChangedForOrigin(
            "Network", "reportingApiEndpointsChangedForOrigin", ReportingApiEndpointsChangedForOrigin.class),

    /**
     * Fired when the node should be inspected. This happens after call to setInspectMode or when
     * user manually inspects an element.
     */
    OverlayInspectNodeRequested("Overlay", "inspectNodeRequested", InspectNodeRequested.class),

    /**
     * Fired when the node should be highlighted. This happens after call to setInspectMode.
     */
    OverlayNodeHighlightRequested("Overlay", "nodeHighlightRequested", NodeHighlightRequested.class),

    /**
     * Fired when user asks to capture screenshot of some area on the page.
     */
    OverlayScreenshotRequested("Overlay", "screenshotRequested", ScreenshotRequested.class),

    /**
     * Fired when user cancels the inspect mode.
     */
    OverlayInspectModeCanceled("Overlay", "inspectModeCanceled", InspectModeCanceled.class),

    PageDomContentEventFired("Page", "domContentEventFired", DomContentEventFired.class),

    /**
     * Emitted only when page.interceptFileChooser is enabled.
     */
    PageFileChooserOpened("Page", "fileChooserOpened", FileChooserOpened.class),

    /**
     * Fired when frame has been attached to its parent.
     */
    PageFrameAttached("Page", "frameAttached", FrameAttached.class),

    /**
     * Fired when frame no longer has a scheduled navigation.
     */
    PageFrameClearedScheduledNavigation(
            "Page", "frameClearedScheduledNavigation", FrameClearedScheduledNavigation.class),

    /**
     * Fired when frame has been detached from its parent.
     */
    PageFrameDetached("Page", "frameDetached", FrameDetached.class),

    /**
     * Fired before frame subtree is detached. Emitted before any frame of the
     * subtree is actually detached.
     */
    PageFrameSubtreeWillBeDetached("Page", "frameSubtreeWillBeDetached", FrameSubtreeWillBeDetached.class),

    /**
     * Fired once navigation of the frame has completed. Frame is now associated with the new loader.
     */
    PageFrameNavigated("Page", "frameNavigated", FrameNavigated.class),

    /**
     * Fired when opening document to write to.
     */
    PageDocumentOpened("Page", "documentOpened", DocumentOpened.class),

    PageFrameResized("Page", "frameResized", FrameResized.class),

    /**
     * Fired when a navigation starts. This event is fired for both
     * renderer-initiated and browser-initiated navigations. For renderer-initiated
     * navigations, the event is fired after frameRequestedNavigation.
     * Navigation may still be cancelled after the event is issued. Multiple events
     * can be fired for a single navigation, for example, when a same-document
     * navigation becomes a cross-document navigation (such as in the case of a
     * frameset).
     */
    PageFrameStartedNavigating("Page", "frameStartedNavigating", FrameStartedNavigating.class),

    /**
     * Fired when a renderer-initiated navigation is requested.
     * Navigation may still be cancelled after the event is issued.
     */
    PageFrameRequestedNavigation("Page", "frameRequestedNavigation", FrameRequestedNavigation.class),

    /**
     * Fired when frame schedules a potential navigation.
     */
    PageFrameScheduledNavigation("Page", "frameScheduledNavigation", FrameScheduledNavigation.class),

    /**
     * Fired when frame has started loading.
     */
    PageFrameStartedLoading("Page", "frameStartedLoading", FrameStartedLoading.class),

    /**
     * Fired when frame has stopped loading.
     */
    PageFrameStoppedLoading("Page", "frameStoppedLoading", FrameStoppedLoading.class),

    /**
     * Fired when page is about to start a download.
     * Deprecated. Use Browser.downloadWillBegin instead.
     */
    PageDownloadWillBegin("Page", "downloadWillBegin", com.cdp4j.event.page.DownloadWillBegin.class),

    /**
     * Fired when download makes progress. Last call has |done| == true.
     * Deprecated. Use Browser.downloadProgress instead.
     */
    PageDownloadProgress("Page", "downloadProgress", com.cdp4j.event.page.DownloadProgress.class),

    /**
     * Fired when interstitial page was hidden
     */
    PageInterstitialHidden("Page", "interstitialHidden", InterstitialHidden.class),

    /**
     * Fired when interstitial page was shown
     */
    PageInterstitialShown("Page", "interstitialShown", InterstitialShown.class),

    /**
     * Fired when a JavaScript initiated dialog (alert, confirm, prompt, or onbeforeunload) has been
     * closed.
     */
    PageJavascriptDialogClosed("Page", "javascriptDialogClosed", JavascriptDialogClosed.class),

    /**
     * Fired when a JavaScript initiated dialog (alert, confirm, prompt, or onbeforeunload) is about to
     * open.
     */
    PageJavascriptDialogOpening("Page", "javascriptDialogOpening", JavascriptDialogOpening.class),

    /**
     * Fired for lifecycle events (navigation, load, paint, etc) in the current
     * target (including local frames).
     */
    PageLifecycleEvent("Page", "lifecycleEvent", LifecycleEvent.class),

    /**
     * Fired for failed bfcache history navigations if BackForwardCache feature is enabled. Do
     * not assume any ordering with the Page.frameNavigated event. This event is fired only for
     * main-frame history navigation where the document changes (non-same-document navigations),
     * when bfcache navigation fails.
     */
    PageBackForwardCacheNotUsed("Page", "backForwardCacheNotUsed", BackForwardCacheNotUsed.class),

    PageLoadEventFired("Page", "loadEventFired", LoadEventFired.class),

    /**
     * Fired when same-document navigation happens, e.g. due to history API usage or anchor navigation.
     */
    PageNavigatedWithinDocument("Page", "navigatedWithinDocument", NavigatedWithinDocument.class),

    /**
     * Compressed image data requested by the startScreencast.
     */
    PageScreencastFrame("Page", "screencastFrame", ScreencastFrame.class),

    /**
     * Fired when the page with currently enabled screencast was shown or hidden .
     */
    PageScreencastVisibilityChanged("Page", "screencastVisibilityChanged", ScreencastVisibilityChanged.class),

    /**
     * Fired when a new window is going to be opened, via window.open(), link click, form submission,
     * etc.
     */
    PageWindowOpen("Page", "windowOpen", WindowOpen.class),

    /**
     * Issued for every compilation cache generated. Is only available
     * if Page.setGenerateCompilationCache is enabled.
     */
    PageCompilationCacheProduced("Page", "compilationCacheProduced", CompilationCacheProduced.class),

    /**
     * Current values of the metrics.
     */
    PerformanceMetrics("Performance", "metrics", Metrics.class),

    /**
     * Sent when a performance timeline event is added. See reportPerformanceTimeline method.
     */
    PerformanceTimelineTimelineEventAdded("PerformanceTimeline", "timelineEventAdded", TimelineEventAdded.class),

    /**
     * There is a certificate error. If overriding certificate errors is enabled, then it should be
     * handled with the handleCertificateError command. Note: this event does not fire if the
     * certificate error has been allowed internally. Only one client per target should override
     * certificate errors at the same time.
     */
    SecurityCertificateError("Security", "certificateError", CertificateError.class),

    /**
     * The security state of the page changed.
     */
    SecurityVisibleSecurityStateChanged("Security", "visibleSecurityStateChanged", VisibleSecurityStateChanged.class),

    /**
     * The security state of the page changed. No longer being sent.
     */
    SecuritySecurityStateChanged("Security", "securityStateChanged", SecurityStateChanged.class),

    ServiceWorkerWorkerErrorReported("ServiceWorker", "workerErrorReported", WorkerErrorReported.class),

    ServiceWorkerWorkerRegistrationUpdated(
            "ServiceWorker", "workerRegistrationUpdated", WorkerRegistrationUpdated.class),

    ServiceWorkerWorkerVersionUpdated("ServiceWorker", "workerVersionUpdated", WorkerVersionUpdated.class),

    /**
     * A cache's contents have been modified.
     */
    StorageCacheStorageContentUpdated("Storage", "cacheStorageContentUpdated", CacheStorageContentUpdated.class),

    /**
     * A cache has been added/deleted.
     */
    StorageCacheStorageListUpdated("Storage", "cacheStorageListUpdated", CacheStorageListUpdated.class),

    /**
     * The origin's IndexedDB object store has been modified.
     */
    StorageIndexedDBContentUpdated("Storage", "indexedDBContentUpdated", IndexedDBContentUpdated.class),

    /**
     * The origin's IndexedDB database list has been modified.
     */
    StorageIndexedDBListUpdated("Storage", "indexedDBListUpdated", IndexedDBListUpdated.class),

    /**
     * One of the interest groups was accessed. Note that these events are global
     * to all targets sharing an interest group store.
     */
    StorageInterestGroupAccessed("Storage", "interestGroupAccessed", InterestGroupAccessed.class),

    /**
     * An auction involving interest groups is taking place. These events are
     * target-specific.
     */
    StorageInterestGroupAuctionEventOccurred(
            "Storage", "interestGroupAuctionEventOccurred", InterestGroupAuctionEventOccurred.class),

    /**
     * Specifies which auctions a particular network fetch may be related to, and
     * in what role. Note that it is not ordered with respect to
     * Network.requestWillBeSent (but will happen before loadingFinished
     * loadingFailed).
     */
    StorageInterestGroupAuctionNetworkRequestCreated(
            "Storage", "interestGroupAuctionNetworkRequestCreated", InterestGroupAuctionNetworkRequestCreated.class),

    /**
     * Shared storage was accessed by the associated page.
     * The following parameters are included in all events.
     */
    StorageSharedStorageAccessed("Storage", "sharedStorageAccessed", SharedStorageAccessed.class),

    StorageStorageBucketCreatedOrUpdated(
            "Storage", "storageBucketCreatedOrUpdated", StorageBucketCreatedOrUpdated.class),

    StorageStorageBucketDeleted("Storage", "storageBucketDeleted", StorageBucketDeleted.class),

    StorageAttributionReportingSourceRegistered(
            "Storage", "attributionReportingSourceRegistered", AttributionReportingSourceRegistered.class),

    StorageAttributionReportingTriggerRegistered(
            "Storage", "attributionReportingTriggerRegistered", AttributionReportingTriggerRegistered.class),

    /**
     * Issued when attached to target because of auto-attach or attachToTarget command.
     */
    TargetAttachedToTarget("Target", "attachedToTarget", AttachedToTarget.class),

    /**
     * Issued when detached from target for any reason (including detachFromTarget command). Can be
     * issued multiple times per target if multiple sessions have been attached to it.
     */
    TargetDetachedFromTarget("Target", "detachedFromTarget", DetachedFromTarget.class),

    /**
     * Notifies about a new protocol message received from the session (as reported in
     * attachedToTarget event).
     */
    TargetReceivedMessageFromTarget("Target", "receivedMessageFromTarget", ReceivedMessageFromTarget.class),

    /**
     * Issued when a possible inspection target is created.
     */
    TargetTargetCreated("Target", "targetCreated", TargetCreated.class),

    /**
     * Issued when a target is destroyed.
     */
    TargetTargetDestroyed("Target", "targetDestroyed", TargetDestroyed.class),

    /**
     * Issued when a target has crashed.
     */
    TargetTargetCrashed("Target", "targetCrashed", com.cdp4j.event.target.TargetCrashed.class),

    /**
     * Issued when some information about a target has changed. This only happens between
     * targetCreated and targetDestroyed.
     */
    TargetTargetInfoChanged("Target", "targetInfoChanged", TargetInfoChanged.class),

    /**
     * Informs that port was successfully bound and got a specified connection id.
     */
    TetheringAccepted("Tethering", "accepted", Accepted.class),

    TracingBufferUsage("Tracing", "bufferUsage", BufferUsage.class),

    /**
     * Contains a bucket of collected trace events. When tracing is stopped collected events will be
     * sent as a sequence of dataCollected events followed by tracingComplete event.
     */
    TracingDataCollected("Tracing", "dataCollected", DataCollected.class),

    /**
     * Signals that tracing is stopped and there is no trace buffers pending flush, all data were
     * delivered via dataCollected events.
     */
    TracingTracingComplete("Tracing", "tracingComplete", TracingComplete.class),

    /**
     * Issued when the domain is enabled and the request URL matches the
     * specified filter. The request is paused until the client responds
     * with one of continueRequest, failRequest or fulfillRequest.
     * The stage of the request can be determined by presence of responseErrorReason
     * and responseStatusCode -- the request is at the response stage if either
     * of these fields is present and in the request stage otherwise.
     * Redirect responses and subsequent requests are reported similarly to regular
     * responses and requests. Redirect responses may be distinguished by the value
     * of responseStatusCode (which is one of 301, 302, 303, 307, 308) along with
     * presence of the location header. Requests resulting from a redirect will
     * have redirectedRequestId field set.
     */
    FetchRequestPaused("Fetch", "requestPaused", RequestPaused.class),

    /**
     * Issued when the domain is enabled with handleAuthRequests set to true.
     * The request is paused until client responds with continueWithAuth.
     */
    FetchAuthRequired("Fetch", "authRequired", AuthRequired.class),

    /**
     * Notifies that a new BaseAudioContext has been created.
     */
    WebAudioContextCreated("WebAudio", "contextCreated", ContextCreated.class),

    /**
     * Notifies that an existing BaseAudioContext will be destroyed.
     */
    WebAudioContextWillBeDestroyed("WebAudio", "contextWillBeDestroyed", ContextWillBeDestroyed.class),

    /**
     * Notifies that existing BaseAudioContext has changed some properties (id stays the same)..
     */
    WebAudioContextChanged("WebAudio", "contextChanged", ContextChanged.class),

    /**
     * Notifies that the construction of an AudioListener has finished.
     */
    WebAudioAudioListenerCreated("WebAudio", "audioListenerCreated", AudioListenerCreated.class),

    /**
     * Notifies that a new AudioListener has been created.
     */
    WebAudioAudioListenerWillBeDestroyed(
            "WebAudio", "audioListenerWillBeDestroyed", AudioListenerWillBeDestroyed.class),

    /**
     * Notifies that a new AudioNode has been created.
     */
    WebAudioAudioNodeCreated("WebAudio", "audioNodeCreated", AudioNodeCreated.class),

    /**
     * Notifies that an existing AudioNode has been destroyed.
     */
    WebAudioAudioNodeWillBeDestroyed("WebAudio", "audioNodeWillBeDestroyed", AudioNodeWillBeDestroyed.class),

    /**
     * Notifies that a new AudioParam has been created.
     */
    WebAudioAudioParamCreated("WebAudio", "audioParamCreated", AudioParamCreated.class),

    /**
     * Notifies that an existing AudioParam has been destroyed.
     */
    WebAudioAudioParamWillBeDestroyed("WebAudio", "audioParamWillBeDestroyed", AudioParamWillBeDestroyed.class),

    /**
     * Notifies that two AudioNodes are connected.
     */
    WebAudioNodesConnected("WebAudio", "nodesConnected", NodesConnected.class),

    /**
     * Notifies that AudioNodes are disconnected. The destination can be null, and it means all the outgoing connections from the source are disconnected.
     */
    WebAudioNodesDisconnected("WebAudio", "nodesDisconnected", NodesDisconnected.class),

    /**
     * Notifies that an AudioNode is connected to an AudioParam.
     */
    WebAudioNodeParamConnected("WebAudio", "nodeParamConnected", NodeParamConnected.class),

    /**
     * Notifies that an AudioNode is disconnected to an AudioParam.
     */
    WebAudioNodeParamDisconnected("WebAudio", "nodeParamDisconnected", NodeParamDisconnected.class),

    /**
     * Triggered when a credential is added to an authenticator.
     */
    WebAuthnCredentialAdded("WebAuthn", "credentialAdded", CredentialAdded.class),

    /**
     * Triggered when a credential is deleted, e.g. through
     * PublicKeyCredential.signalUnknownCredential().
     */
    WebAuthnCredentialDeleted("WebAuthn", "credentialDeleted", CredentialDeleted.class),

    /**
     * Triggered when a credential is updated, e.g. through
     * PublicKeyCredential.signalCurrentUserDetails().
     */
    WebAuthnCredentialUpdated("WebAuthn", "credentialUpdated", CredentialUpdated.class),

    /**
     * Triggered when a credential is used in a webauthn assertion.
     */
    WebAuthnCredentialAsserted("WebAuthn", "credentialAsserted", CredentialAsserted.class),

    /**
     * This can be called multiple times, and can be used to set / override /
     * remove player properties. A null propValue indicates removal.
     */
    MediaPlayerPropertiesChanged("Media", "playerPropertiesChanged", PlayerPropertiesChanged.class),

    /**
     * Send events as a list, allowing them to be batched on the browser for less
     * congestion. If batched, events must ALWAYS be in chronological order.
     */
    MediaPlayerEventsAdded("Media", "playerEventsAdded", PlayerEventsAdded.class),

    /**
     * Send a list of any messages that need to be delivered.
     */
    MediaPlayerMessagesLogged("Media", "playerMessagesLogged", PlayerMessagesLogged.class),

    /**
     * Send a list of any errors that need to be delivered.
     */
    MediaPlayerErrorsRaised("Media", "playerErrorsRaised", PlayerErrorsRaised.class),

    /**
     * Called whenever a player is created, or when a new agent joins and receives
     * a list of active players. If an agent is restored, it will receive the full
     * list of player ids and all events again.
     */
    MediaPlayersCreated("Media", "playersCreated", PlayersCreated.class),

    /**
     * A device request opened a user prompt to select a device. Respond with the
     * selectPrompt or cancelPrompt command.
     */
    DeviceAccessDeviceRequestPrompted("DeviceAccess", "deviceRequestPrompted", DeviceRequestPrompted.class),

    /**
     * Upsert. Currently, it is only emitted when a rule set added.
     */
    PreloadRuleSetUpdated("Preload", "ruleSetUpdated", RuleSetUpdated.class),

    PreloadRuleSetRemoved("Preload", "ruleSetRemoved", RuleSetRemoved.class),

    /**
     * Fired when a preload enabled state is updated.
     */
    PreloadPreloadEnabledStateUpdated("Preload", "preloadEnabledStateUpdated", PreloadEnabledStateUpdated.class),

    /**
     * Fired when a prefetch attempt is updated.
     */
    PreloadPrefetchStatusUpdated("Preload", "prefetchStatusUpdated", PrefetchStatusUpdated.class),

    /**
     * Fired when a prerender attempt is updated.
     */
    PreloadPrerenderStatusUpdated("Preload", "prerenderStatusUpdated", PrerenderStatusUpdated.class),

    /**
     * Send a list of sources for all preloading attempts in a document.
     */
    PreloadPreloadingAttemptSourcesUpdated(
            "Preload", "preloadingAttemptSourcesUpdated", PreloadingAttemptSourcesUpdated.class),

    FedCmDialogShown("FedCm", "dialogShown", DialogShown.class),

    /**
     * Triggered when a dialog is closed, either by user action, JS abort,
     * or a command below.
     */
    FedCmDialogClosed("FedCm", "dialogClosed", DialogClosed.class),

    /**
     * Issued when new console message is added.
     */
    ConsoleMessageAdded("Console", "messageAdded", MessageAdded.class),

    /**
     * Fired when breakpoint is resolved to an actual script and location.
     * Deprecated in favor of resolvedBreakpoints in the scriptParsed event.
     */
    DebuggerBreakpointResolved("Debugger", "breakpointResolved", BreakpointResolved.class),

    /**
     * Fired when the virtual machine stopped on breakpoint or exception or any other stop criteria.
     */
    DebuggerPaused("Debugger", "paused", Paused.class),

    /**
     * Fired when the virtual machine resumed execution.
     */
    DebuggerResumed("Debugger", "resumed", Resumed.class),

    /**
     * Fired when virtual machine fails to parse the script.
     */
    DebuggerScriptFailedToParse("Debugger", "scriptFailedToParse", ScriptFailedToParse.class),

    /**
     * Fired when virtual machine parses script. This event is also fired for all known and uncollected
     * scripts upon enabling debugger.
     */
    DebuggerScriptParsed("Debugger", "scriptParsed", ScriptParsed.class),

    HeapProfilerAddHeapSnapshotChunk("HeapProfiler", "addHeapSnapshotChunk", AddHeapSnapshotChunk.class),

    /**
     * If heap objects tracking has been started then backend may send update for one or more fragments
     */
    HeapProfilerHeapStatsUpdate("HeapProfiler", "heapStatsUpdate", HeapStatsUpdate.class),

    /**
     * If heap objects tracking has been started then backend regularly sends a current value for last
     * seen object id and corresponding timestamp. If the were changes in the heap since last event
     * then one or more heapStatsUpdate events will be sent before a new lastSeenObjectId event.
     */
    HeapProfilerLastSeenObjectId("HeapProfiler", "lastSeenObjectId", LastSeenObjectId.class),

    HeapProfilerReportHeapSnapshotProgress(
            "HeapProfiler", "reportHeapSnapshotProgress", ReportHeapSnapshotProgress.class),

    HeapProfilerResetProfiles("HeapProfiler", "resetProfiles", ResetProfiles.class),

    ProfilerConsoleProfileFinished("Profiler", "consoleProfileFinished", ConsoleProfileFinished.class),

    /**
     * Sent when new profile recording is started using console.profile() call.
     */
    ProfilerConsoleProfileStarted("Profiler", "consoleProfileStarted", ConsoleProfileStarted.class),

    /**
     * Reports coverage delta since the last poll (either from an event like this, or from
     * takePreciseCoverage for the current isolate. May only be sent if precise code
     * coverage has been started. This event can be trigged by the embedder to, for example,
     * trigger collection of coverage data immediately at a certain point in time.
     */
    ProfilerPreciseCoverageDeltaUpdate("Profiler", "preciseCoverageDeltaUpdate", PreciseCoverageDeltaUpdate.class),

    /**
     * Notification is issued every time when binding is called.
     */
    RuntimeBindingCalled("Runtime", "bindingCalled", BindingCalled.class),

    /**
     * Issued when console API was called.
     */
    RuntimeConsoleAPICalled("Runtime", "consoleAPICalled", ConsoleAPICalled.class),

    /**
     * Issued when unhandled exception was revoked.
     */
    RuntimeExceptionRevoked("Runtime", "exceptionRevoked", ExceptionRevoked.class),

    /**
     * Issued when exception was thrown and unhandled.
     */
    RuntimeExceptionThrown("Runtime", "exceptionThrown", ExceptionThrown.class),

    /**
     * Issued when new execution context is created.
     */
    RuntimeExecutionContextCreated("Runtime", "executionContextCreated", ExecutionContextCreated.class),

    /**
     * Issued when execution context is destroyed.
     */
    RuntimeExecutionContextDestroyed("Runtime", "executionContextDestroyed", ExecutionContextDestroyed.class),

    /**
     * Issued when all executionContexts were cleared in browser
     */
    RuntimeExecutionContextsCleared("Runtime", "executionContextsCleared", ExecutionContextsCleared.class),

    /**
     * Issued when object should be inspected (for example, as a result of inspect() command line API
     * call).
     */
    RuntimeInspectRequested("Runtime", "inspectRequested", InspectRequested.class);

    public final String domain;

    public final String name;

    public final Class klass;

    Events(String domain, String name, Class klass) {
        this.domain = domain;
        this.name = name;
        this.klass = klass;
    }

    @Override
    public String toString() {
        return domain + "." + name;
    }
}
