// SPDX-License-Identifier: MIT
package com.cdp4j.event.runtime;

import com.cdp4j.type.runtime.ExecutionContextDescription;

/**
 * Issued when new execution context is created.
 */
public class ExecutionContextCreated {
    private ExecutionContextDescription context;

    /**
     * A newly created execution context.
     */
    public ExecutionContextDescription getContext() {
        return context;
    }

    /**
     * A newly created execution context.
     */
    public void setContext(ExecutionContextDescription context) {
        this.context = context;
    }

    public String toString() {
        return "ExecutionContextCreated [context=" + context + "]";
    }
}
