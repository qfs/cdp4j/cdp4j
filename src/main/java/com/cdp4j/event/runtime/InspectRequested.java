// SPDX-License-Identifier: MIT
package com.cdp4j.event.runtime;

import com.cdp4j.type.runtime.RemoteObject;

/**
 * Issued when object should be inspected (for example, as a result of inspect()
 * command line API call).
 */
public class InspectRequested {
    private RemoteObject object;

    private Object hints;

    private Integer executionContextId;

    public RemoteObject getObject() {
        return object;
    }

    public void setObject(RemoteObject object) {
        this.object = object;
    }

    public Object getHints() {
        return hints;
    }

    public void setHints(Object hints) {
        this.hints = hints;
    }

    /**
     * Identifier of the context where the call was made.
     */
    public Integer getExecutionContextId() {
        return executionContextId;
    }

    /**
     * Identifier of the context where the call was made.
     */
    public void setExecutionContextId(Integer executionContextId) {
        this.executionContextId = executionContextId;
    }

    public String toString() {
        return "InspectRequested [object=" + object + ", hints=" + hints + ", executionContextId=" + executionContextId
                + "]";
    }
}
