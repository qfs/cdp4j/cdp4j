// SPDX-License-Identifier: MIT
package com.cdp4j.event.runtime;

import com.cdp4j.type.constant.ConsoleApiCallType;
import com.cdp4j.type.runtime.RemoteObject;
import com.cdp4j.type.runtime.StackTrace;
import java.util.List;

/**
 * Issued when console API was called.
 */
public class ConsoleAPICalled {
    private ConsoleApiCallType type;

    private List<RemoteObject> args;

    private Integer executionContextId;

    private Double timestamp;

    private StackTrace stackTrace;

    private String context;

    /**
     * Type of the call.
     */
    public ConsoleApiCallType getType() {
        return type;
    }

    /**
     * Type of the call.
     */
    public void setType(ConsoleApiCallType type) {
        this.type = type;
    }

    /**
     * Call arguments.
     */
    public List<RemoteObject> getArgs() {
        return args;
    }

    /**
     * Call arguments.
     */
    public void setArgs(List<RemoteObject> args) {
        this.args = args;
    }

    /**
     * Identifier of the context where the call was made.
     */
    public Integer getExecutionContextId() {
        return executionContextId;
    }

    /**
     * Identifier of the context where the call was made.
     */
    public void setExecutionContextId(Integer executionContextId) {
        this.executionContextId = executionContextId;
    }

    /**
     * Call timestamp.
     */
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * Call timestamp.
     */
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Stack trace captured when the call was made. The async stack chain is
     * automatically reported for the following call types: assert, error, trace,
     * warning. For other types the async call chain can be retrieved using
     * Debugger.getStackTrace and stackTrace.parentId field.
     */
    public StackTrace getStackTrace() {
        return stackTrace;
    }

    /**
     * Stack trace captured when the call was made. The async stack chain is
     * automatically reported for the following call types: assert, error, trace,
     * warning. For other types the async call chain can be retrieved using
     * Debugger.getStackTrace and stackTrace.parentId field.
     */
    public void setStackTrace(StackTrace stackTrace) {
        this.stackTrace = stackTrace;
    }

    /**
     * Console context descriptor for calls on non-default console context (not
     * console.*): 'anonymous#unique-logger-id' for call on unnamed context,
     * 'name#unique-logger-id' for call on named context.
     */
    public String getContext() {
        return context;
    }

    /**
     * Console context descriptor for calls on non-default console context (not
     * console.*): 'anonymous#unique-logger-id' for call on unnamed context,
     * 'name#unique-logger-id' for call on named context.
     */
    public void setContext(String context) {
        this.context = context;
    }

    public String toString() {
        return "ConsoleAPICalled [type=" + type + ", args=" + args + ", executionContextId=" + executionContextId
                + ", timestamp=" + timestamp + ", stackTrace=" + stackTrace + ", context=" + context + "]";
    }
}
