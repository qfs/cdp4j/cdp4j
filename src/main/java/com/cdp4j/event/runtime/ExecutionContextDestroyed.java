// SPDX-License-Identifier: MIT
package com.cdp4j.event.runtime;

/**
 * Issued when execution context is destroyed.
 */
public class ExecutionContextDestroyed {
    private Integer executionContextId;

    private String executionContextUniqueId;

    /**
     * Id of the destroyed context
     */
    public Integer getExecutionContextId() {
        return executionContextId;
    }

    /**
     * Id of the destroyed context
     */
    public void setExecutionContextId(Integer executionContextId) {
        this.executionContextId = executionContextId;
    }

    /**
     * Unique Id of the destroyed context
     */
    public String getExecutionContextUniqueId() {
        return executionContextUniqueId;
    }

    /**
     * Unique Id of the destroyed context
     */
    public void setExecutionContextUniqueId(String executionContextUniqueId) {
        this.executionContextUniqueId = executionContextUniqueId;
    }

    public String toString() {
        return "ExecutionContextDestroyed [executionContextId=" + executionContextId + ", executionContextUniqueId="
                + executionContextUniqueId + "]";
    }
}
