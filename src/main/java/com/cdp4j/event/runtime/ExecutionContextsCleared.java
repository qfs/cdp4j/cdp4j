// SPDX-License-Identifier: MIT
package com.cdp4j.event.runtime;

/**
 * Issued when all executionContexts were cleared in browser
 */
public class ExecutionContextsCleared {}
