// SPDX-License-Identifier: MIT
package com.cdp4j.event.target;

import com.cdp4j.type.target.TargetInfo;

/**
 * Issued when a possible inspection target is created.
 */
public class TargetCreated {
    private TargetInfo targetInfo;

    public TargetInfo getTargetInfo() {
        return targetInfo;
    }

    public void setTargetInfo(TargetInfo targetInfo) {
        this.targetInfo = targetInfo;
    }

    public String toString() {
        return "TargetCreated [targetInfo=" + targetInfo + "]";
    }
}
