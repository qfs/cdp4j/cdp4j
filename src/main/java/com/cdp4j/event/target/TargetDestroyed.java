// SPDX-License-Identifier: MIT
package com.cdp4j.event.target;

/**
 * Issued when a target is destroyed.
 */
public class TargetDestroyed {
    private String targetId;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String toString() {
        return "TargetDestroyed [targetId=" + targetId + "]";
    }
}
