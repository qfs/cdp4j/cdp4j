// SPDX-License-Identifier: MIT
package com.cdp4j.event.target;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.target.TargetInfo;

/**
 * Issued when attached to target because of auto-attach or attachToTarget
 * command.
 */
@Experimental
public class AttachedToTarget {
    private String sessionId;

    private TargetInfo targetInfo;

    private Boolean waitingForDebugger;

    /**
     * Identifier assigned to the session used to send/receive messages.
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Identifier assigned to the session used to send/receive messages.
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public TargetInfo getTargetInfo() {
        return targetInfo;
    }

    public void setTargetInfo(TargetInfo targetInfo) {
        this.targetInfo = targetInfo;
    }

    public Boolean isWaitingForDebugger() {
        return waitingForDebugger;
    }

    public void setWaitingForDebugger(Boolean waitingForDebugger) {
        this.waitingForDebugger = waitingForDebugger;
    }

    public String toString() {
        return "AttachedToTarget [sessionId=" + sessionId + ", targetInfo=" + targetInfo + ", waitingForDebugger="
                + waitingForDebugger + "]";
    }
}
