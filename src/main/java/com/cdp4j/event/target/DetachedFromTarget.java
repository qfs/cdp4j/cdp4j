// SPDX-License-Identifier: MIT
package com.cdp4j.event.target;

import com.cdp4j.annotation.Experimental;

/**
 * Issued when detached from target for any reason (including detachFromTarget
 * command). Can be issued multiple times per target if multiple sessions have
 * been attached to it.
 */
@Experimental
public class DetachedFromTarget {
    private String sessionId;

    private String targetId;

    /**
     * Detached session identifier.
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Detached session identifier.
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Deprecated.
     */
    public String getTargetId() {
        return targetId;
    }

    /**
     * Deprecated.
     */
    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String toString() {
        return "DetachedFromTarget [sessionId=" + sessionId + ", targetId=" + targetId + "]";
    }
}
