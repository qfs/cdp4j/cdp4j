// SPDX-License-Identifier: MIT
package com.cdp4j.event.target;

import com.cdp4j.type.target.TargetInfo;

/**
 * Issued when some information about a target has changed. This only happens
 * between targetCreated and targetDestroyed.
 */
public class TargetInfoChanged {
    private TargetInfo targetInfo;

    public TargetInfo getTargetInfo() {
        return targetInfo;
    }

    public void setTargetInfo(TargetInfo targetInfo) {
        this.targetInfo = targetInfo;
    }

    public String toString() {
        return "TargetInfoChanged [targetInfo=" + targetInfo + "]";
    }
}
