// SPDX-License-Identifier: MIT
package com.cdp4j.event.animation;

import com.cdp4j.type.animation.Animation;

/**
 * Event for animation that has been updated.
 */
public class AnimationUpdated {
    private Animation animation;

    /**
     * Animation that was updated.
     */
    public Animation getAnimation() {
        return animation;
    }

    /**
     * Animation that was updated.
     */
    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public String toString() {
        return "AnimationUpdated [animation=" + animation + "]";
    }
}
