// SPDX-License-Identifier: MIT
package com.cdp4j.event.animation;

/**
 * Event for when an animation has been cancelled.
 */
public class AnimationCanceled {
    private String id;

    /**
     * Id of the animation that was cancelled.
     */
    public String getId() {
        return id;
    }

    /**
     * Id of the animation that was cancelled.
     */
    public void setId(String id) {
        this.id = id;
    }

    public String toString() {
        return "AnimationCanceled [id=" + id + "]";
    }
}
