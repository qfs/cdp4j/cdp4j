// SPDX-License-Identifier: MIT
package com.cdp4j.event.animation;

import com.cdp4j.type.animation.Animation;

/**
 * Event for animation that has been started.
 */
public class AnimationStarted {
    private Animation animation;

    /**
     * Animation that was started.
     */
    public Animation getAnimation() {
        return animation;
    }

    /**
     * Animation that was started.
     */
    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    public String toString() {
        return "AnimationStarted [animation=" + animation + "]";
    }
}
