// SPDX-License-Identifier: MIT
package com.cdp4j.event.backgroundservice;

import com.cdp4j.type.backgroundservice.BackgroundServiceEvent;

/**
 * Called with all existing backgroundServiceEvents when enabled, and all new
 * events afterwards if enabled and recording.
 */
public class BackgroundServiceEventReceived {
    private BackgroundServiceEvent backgroundServiceEvent;

    public BackgroundServiceEvent getBackgroundServiceEvent() {
        return backgroundServiceEvent;
    }

    public void setBackgroundServiceEvent(BackgroundServiceEvent backgroundServiceEvent) {
        this.backgroundServiceEvent = backgroundServiceEvent;
    }

    public String toString() {
        return "BackgroundServiceEventReceived [backgroundServiceEvent=" + backgroundServiceEvent + "]";
    }
}
