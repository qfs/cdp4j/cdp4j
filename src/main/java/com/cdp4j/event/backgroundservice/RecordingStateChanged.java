// SPDX-License-Identifier: MIT
package com.cdp4j.event.backgroundservice;

import com.cdp4j.type.backgroundservice.ServiceName;

/**
 * Called when the recording state for the service has been updated.
 */
public class RecordingStateChanged {
    private Boolean isRecording;

    private ServiceName service;

    public Boolean isIsRecording() {
        return isRecording;
    }

    public void setIsRecording(Boolean isRecording) {
        this.isRecording = isRecording;
    }

    public ServiceName getService() {
        return service;
    }

    public void setService(ServiceName service) {
        this.service = service;
    }

    public String toString() {
        return "RecordingStateChanged [isRecording=" + isRecording + ", service=" + service + "]";
    }
}
