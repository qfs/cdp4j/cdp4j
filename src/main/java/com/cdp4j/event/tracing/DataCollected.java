// SPDX-License-Identifier: MIT
package com.cdp4j.event.tracing;

import com.cdp4j.annotation.Experimental;
import java.util.List;
import java.util.Map;

/**
 * Contains a bucket of collected trace events. When tracing is stopped
 * collected events will be sent as a sequence of dataCollected events followed
 * by tracingComplete event.
 */
@Experimental
public class DataCollected {
    private List<Map<String, Object>> value;

    public List<Map<String, Object>> getValue() {
        return value;
    }

    public void setValue(List<Map<String, Object>> value) {
        this.value = value;
    }

    public String toString() {
        return "DataCollected [value=" + value + "]";
    }
}
