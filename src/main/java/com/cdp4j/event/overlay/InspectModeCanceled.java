// SPDX-License-Identifier: MIT
package com.cdp4j.event.overlay;

/**
 * Fired when user cancels the inspect mode.
 */
public class InspectModeCanceled {}
