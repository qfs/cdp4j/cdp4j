// SPDX-License-Identifier: MIT
package com.cdp4j.event.overlay;

/**
 * Fired when the node should be highlighted. This happens after call to
 * setInspectMode.
 */
public class NodeHighlightRequested {
    private Integer nodeId;

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public String toString() {
        return "NodeHighlightRequested [nodeId=" + nodeId + "]";
    }
}
