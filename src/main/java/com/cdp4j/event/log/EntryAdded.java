// SPDX-License-Identifier: MIT
package com.cdp4j.event.log;

import com.cdp4j.type.log.LogEntry;

/**
 * Issued when new message was logged.
 */
public class EntryAdded {
    private LogEntry entry;

    /**
     * The entry.
     */
    public LogEntry getEntry() {
        return entry;
    }

    /**
     * The entry.
     */
    public void setEntry(LogEntry entry) {
        this.entry = entry;
    }

    public String toString() {
        return "EntryAdded [entry=" + entry + "]";
    }
}
