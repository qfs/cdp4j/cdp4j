// SPDX-License-Identifier: MIT
package com.cdp4j.event.serviceworker;

import com.cdp4j.type.serviceworker.ServiceWorkerVersion;
import java.util.List;

public class WorkerVersionUpdated {
    private List<ServiceWorkerVersion> versions;

    public List<ServiceWorkerVersion> getVersions() {
        return versions;
    }

    public void setVersions(List<ServiceWorkerVersion> versions) {
        this.versions = versions;
    }

    public String toString() {
        return "WorkerVersionUpdated [versions=" + versions + "]";
    }
}
