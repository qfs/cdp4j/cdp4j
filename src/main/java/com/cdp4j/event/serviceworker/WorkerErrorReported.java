// SPDX-License-Identifier: MIT
package com.cdp4j.event.serviceworker;

import com.cdp4j.type.serviceworker.ServiceWorkerErrorMessage;

public class WorkerErrorReported {
    private ServiceWorkerErrorMessage errorMessage;

    public ServiceWorkerErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(ServiceWorkerErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String toString() {
        return "WorkerErrorReported [errorMessage=" + errorMessage + "]";
    }
}
