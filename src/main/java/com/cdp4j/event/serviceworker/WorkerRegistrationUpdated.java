// SPDX-License-Identifier: MIT
package com.cdp4j.event.serviceworker;

import com.cdp4j.type.serviceworker.ServiceWorkerRegistration;
import java.util.List;

public class WorkerRegistrationUpdated {
    private List<ServiceWorkerRegistration> registrations;

    public List<ServiceWorkerRegistration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<ServiceWorkerRegistration> registrations) {
        this.registrations = registrations;
    }

    public String toString() {
        return "WorkerRegistrationUpdated [registrations=" + registrations + "]";
    }
}
