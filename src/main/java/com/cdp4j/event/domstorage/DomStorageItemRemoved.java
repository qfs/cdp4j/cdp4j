// SPDX-License-Identifier: MIT
package com.cdp4j.event.domstorage;

import com.cdp4j.type.domstorage.StorageId;

public class DomStorageItemRemoved {
    private StorageId storageId;

    private String key;

    public StorageId getStorageId() {
        return storageId;
    }

    public void setStorageId(StorageId storageId) {
        this.storageId = storageId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String toString() {
        return "DomStorageItemRemoved [storageId=" + storageId + ", key=" + key + "]";
    }
}
