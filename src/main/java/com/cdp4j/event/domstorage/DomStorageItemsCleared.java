// SPDX-License-Identifier: MIT
package com.cdp4j.event.domstorage;

import com.cdp4j.type.domstorage.StorageId;

public class DomStorageItemsCleared {
    private StorageId storageId;

    public StorageId getStorageId() {
        return storageId;
    }

    public void setStorageId(StorageId storageId) {
        this.storageId = storageId;
    }

    public String toString() {
        return "DomStorageItemsCleared [storageId=" + storageId + "]";
    }
}
