// SPDX-License-Identifier: MIT
package com.cdp4j.event.preload;

public class RuleSetRemoved {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String toString() {
        return "RuleSetRemoved [id=" + id + "]";
    }
}
