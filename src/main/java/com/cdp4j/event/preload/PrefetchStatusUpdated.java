// SPDX-License-Identifier: MIT
package com.cdp4j.event.preload;

import com.cdp4j.type.preload.PrefetchStatus;
import com.cdp4j.type.preload.PreloadingAttemptKey;
import com.cdp4j.type.preload.PreloadingStatus;

/**
 * Fired when a prefetch attempt is updated.
 */
public class PrefetchStatusUpdated {
    private PreloadingAttemptKey key;

    private String pipelineId;

    private String initiatingFrameId;

    private String prefetchUrl;

    private PreloadingStatus status;

    private PrefetchStatus prefetchStatus;

    private String requestId;

    public PreloadingAttemptKey getKey() {
        return key;
    }

    public void setKey(PreloadingAttemptKey key) {
        this.key = key;
    }

    public String getPipelineId() {
        return pipelineId;
    }

    public void setPipelineId(String pipelineId) {
        this.pipelineId = pipelineId;
    }

    /**
     * The frame id of the frame initiating prefetch.
     */
    public String getInitiatingFrameId() {
        return initiatingFrameId;
    }

    /**
     * The frame id of the frame initiating prefetch.
     */
    public void setInitiatingFrameId(String initiatingFrameId) {
        this.initiatingFrameId = initiatingFrameId;
    }

    public String getPrefetchUrl() {
        return prefetchUrl;
    }

    public void setPrefetchUrl(String prefetchUrl) {
        this.prefetchUrl = prefetchUrl;
    }

    public PreloadingStatus getStatus() {
        return status;
    }

    public void setStatus(PreloadingStatus status) {
        this.status = status;
    }

    public PrefetchStatus getPrefetchStatus() {
        return prefetchStatus;
    }

    public void setPrefetchStatus(PrefetchStatus prefetchStatus) {
        this.prefetchStatus = prefetchStatus;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String toString() {
        return "PrefetchStatusUpdated [key=" + key + ", pipelineId=" + pipelineId + ", initiatingFrameId="
                + initiatingFrameId + ", prefetchUrl=" + prefetchUrl + ", status=" + status + ", prefetchStatus="
                + prefetchStatus + ", requestId=" + requestId + "]";
    }
}
