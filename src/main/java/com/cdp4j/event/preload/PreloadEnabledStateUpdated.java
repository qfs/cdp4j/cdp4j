// SPDX-License-Identifier: MIT
package com.cdp4j.event.preload;

/**
 * Fired when a preload enabled state is updated.
 */
public class PreloadEnabledStateUpdated {
    private Boolean disabledByPreference;

    private Boolean disabledByDataSaver;

    private Boolean disabledByBatterySaver;

    private Boolean disabledByHoldbackPrefetchSpeculationRules;

    private Boolean disabledByHoldbackPrerenderSpeculationRules;

    public Boolean isDisabledByPreference() {
        return disabledByPreference;
    }

    public void setDisabledByPreference(Boolean disabledByPreference) {
        this.disabledByPreference = disabledByPreference;
    }

    public Boolean isDisabledByDataSaver() {
        return disabledByDataSaver;
    }

    public void setDisabledByDataSaver(Boolean disabledByDataSaver) {
        this.disabledByDataSaver = disabledByDataSaver;
    }

    public Boolean isDisabledByBatterySaver() {
        return disabledByBatterySaver;
    }

    public void setDisabledByBatterySaver(Boolean disabledByBatterySaver) {
        this.disabledByBatterySaver = disabledByBatterySaver;
    }

    public Boolean isDisabledByHoldbackPrefetchSpeculationRules() {
        return disabledByHoldbackPrefetchSpeculationRules;
    }

    public void setDisabledByHoldbackPrefetchSpeculationRules(Boolean disabledByHoldbackPrefetchSpeculationRules) {
        this.disabledByHoldbackPrefetchSpeculationRules = disabledByHoldbackPrefetchSpeculationRules;
    }

    public Boolean isDisabledByHoldbackPrerenderSpeculationRules() {
        return disabledByHoldbackPrerenderSpeculationRules;
    }

    public void setDisabledByHoldbackPrerenderSpeculationRules(Boolean disabledByHoldbackPrerenderSpeculationRules) {
        this.disabledByHoldbackPrerenderSpeculationRules = disabledByHoldbackPrerenderSpeculationRules;
    }

    public String toString() {
        return "PreloadEnabledStateUpdated [disabledByPreference=" + disabledByPreference + ", disabledByDataSaver="
                + disabledByDataSaver + ", disabledByBatterySaver=" + disabledByBatterySaver
                + ", disabledByHoldbackPrefetchSpeculationRules=" + disabledByHoldbackPrefetchSpeculationRules
                + ", disabledByHoldbackPrerenderSpeculationRules=" + disabledByHoldbackPrerenderSpeculationRules + "]";
    }
}
