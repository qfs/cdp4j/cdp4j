// SPDX-License-Identifier: MIT
package com.cdp4j.event.preload;

import com.cdp4j.type.preload.PreloadingAttemptSource;
import java.util.List;

/**
 * Send a list of sources for all preloading attempts in a document.
 */
public class PreloadingAttemptSourcesUpdated {
    private String loaderId;

    private List<PreloadingAttemptSource> preloadingAttemptSources;

    public String getLoaderId() {
        return loaderId;
    }

    public void setLoaderId(String loaderId) {
        this.loaderId = loaderId;
    }

    public List<PreloadingAttemptSource> getPreloadingAttemptSources() {
        return preloadingAttemptSources;
    }

    public void setPreloadingAttemptSources(List<PreloadingAttemptSource> preloadingAttemptSources) {
        this.preloadingAttemptSources = preloadingAttemptSources;
    }

    public String toString() {
        return "PreloadingAttemptSourcesUpdated [loaderId=" + loaderId + ", preloadingAttemptSources="
                + preloadingAttemptSources + "]";
    }
}
