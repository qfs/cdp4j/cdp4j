// SPDX-License-Identifier: MIT
package com.cdp4j.event.preload;

import com.cdp4j.type.preload.RuleSet;

/**
 * Upsert. Currently, it is only emitted when a rule set added.
 */
public class RuleSetUpdated {
    private RuleSet ruleSet;

    public RuleSet getRuleSet() {
        return ruleSet;
    }

    public void setRuleSet(RuleSet ruleSet) {
        this.ruleSet = ruleSet;
    }

    public String toString() {
        return "RuleSetUpdated [ruleSet=" + ruleSet + "]";
    }
}
