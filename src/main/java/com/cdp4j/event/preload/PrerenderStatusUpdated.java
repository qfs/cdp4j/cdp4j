// SPDX-License-Identifier: MIT
package com.cdp4j.event.preload;

import com.cdp4j.type.preload.PreloadingAttemptKey;
import com.cdp4j.type.preload.PreloadingStatus;
import com.cdp4j.type.preload.PrerenderFinalStatus;
import com.cdp4j.type.preload.PrerenderMismatchedHeaders;
import java.util.List;

/**
 * Fired when a prerender attempt is updated.
 */
public class PrerenderStatusUpdated {
    private PreloadingAttemptKey key;

    private String pipelineId;

    private PreloadingStatus status;

    private PrerenderFinalStatus prerenderStatus;

    private String disallowedMojoInterface;

    private List<PrerenderMismatchedHeaders> mismatchedHeaders;

    public PreloadingAttemptKey getKey() {
        return key;
    }

    public void setKey(PreloadingAttemptKey key) {
        this.key = key;
    }

    public String getPipelineId() {
        return pipelineId;
    }

    public void setPipelineId(String pipelineId) {
        this.pipelineId = pipelineId;
    }

    public PreloadingStatus getStatus() {
        return status;
    }

    public void setStatus(PreloadingStatus status) {
        this.status = status;
    }

    public PrerenderFinalStatus getPrerenderStatus() {
        return prerenderStatus;
    }

    public void setPrerenderStatus(PrerenderFinalStatus prerenderStatus) {
        this.prerenderStatus = prerenderStatus;
    }

    /**
     * This is used to give users more information about the name of Mojo interface
     * that is incompatible with prerender and has caused the cancellation of the
     * attempt.
     */
    public String getDisallowedMojoInterface() {
        return disallowedMojoInterface;
    }

    /**
     * This is used to give users more information about the name of Mojo interface
     * that is incompatible with prerender and has caused the cancellation of the
     * attempt.
     */
    public void setDisallowedMojoInterface(String disallowedMojoInterface) {
        this.disallowedMojoInterface = disallowedMojoInterface;
    }

    public List<PrerenderMismatchedHeaders> getMismatchedHeaders() {
        return mismatchedHeaders;
    }

    public void setMismatchedHeaders(List<PrerenderMismatchedHeaders> mismatchedHeaders) {
        this.mismatchedHeaders = mismatchedHeaders;
    }

    public String toString() {
        return "PrerenderStatusUpdated [key=" + key + ", pipelineId=" + pipelineId + ", status=" + status
                + ", prerenderStatus=" + prerenderStatus + ", disallowedMojoInterface=" + disallowedMojoInterface
                + ", mismatchedHeaders=" + mismatchedHeaders + "]";
    }
}
