// SPDX-License-Identifier: MIT
package com.cdp4j.event.emulation;

import com.cdp4j.annotation.Experimental;

/**
 * Notification sent after the virtual time budget for the current VirtualTimePolicy has run out.
 */
@Experimental
public class VirtualTimeBudgetExpired {}
