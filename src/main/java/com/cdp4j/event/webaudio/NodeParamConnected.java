// SPDX-License-Identifier: MIT
package com.cdp4j.event.webaudio;

/**
 * Notifies that an AudioNode is connected to an AudioParam.
 */
public class NodeParamConnected {
    private String contextId;

    private String sourceId;

    private String destinationId;

    private Double sourceOutputIndex;

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public Double getSourceOutputIndex() {
        return sourceOutputIndex;
    }

    public void setSourceOutputIndex(Double sourceOutputIndex) {
        this.sourceOutputIndex = sourceOutputIndex;
    }

    public String toString() {
        return "NodeParamConnected [contextId=" + contextId + ", sourceId=" + sourceId + ", destinationId="
                + destinationId + ", sourceOutputIndex=" + sourceOutputIndex + "]";
    }
}
