// SPDX-License-Identifier: MIT
package com.cdp4j.event.webaudio;

import com.cdp4j.type.webaudio.AudioNode;

/**
 * Notifies that a new AudioNode has been created.
 */
public class AudioNodeCreated {
    private AudioNode node;

    public AudioNode getNode() {
        return node;
    }

    public void setNode(AudioNode node) {
        this.node = node;
    }

    public String toString() {
        return "AudioNodeCreated [node=" + node + "]";
    }
}
