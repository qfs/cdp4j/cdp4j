// SPDX-License-Identifier: MIT
package com.cdp4j.event.webaudio;

import com.cdp4j.type.webaudio.AudioListener;

/**
 * Notifies that the construction of an AudioListener has finished.
 */
public class AudioListenerCreated {
    private AudioListener listener;

    public AudioListener getListener() {
        return listener;
    }

    public void setListener(AudioListener listener) {
        this.listener = listener;
    }

    public String toString() {
        return "AudioListenerCreated [listener=" + listener + "]";
    }
}
