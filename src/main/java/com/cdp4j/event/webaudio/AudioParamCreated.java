// SPDX-License-Identifier: MIT
package com.cdp4j.event.webaudio;

import com.cdp4j.type.webaudio.AudioParam;

/**
 * Notifies that a new AudioParam has been created.
 */
public class AudioParamCreated {
    private AudioParam param;

    public AudioParam getParam() {
        return param;
    }

    public void setParam(AudioParam param) {
        this.param = param;
    }

    public String toString() {
        return "AudioParamCreated [param=" + param + "]";
    }
}
