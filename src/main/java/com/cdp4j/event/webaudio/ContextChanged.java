// SPDX-License-Identifier: MIT
package com.cdp4j.event.webaudio;

import com.cdp4j.type.webaudio.BaseAudioContext;

/**
 * Notifies that existing BaseAudioContext has changed some properties (id stays
 * the same)..
 */
public class ContextChanged {
    private BaseAudioContext context;

    public BaseAudioContext getContext() {
        return context;
    }

    public void setContext(BaseAudioContext context) {
        this.context = context;
    }

    public String toString() {
        return "ContextChanged [context=" + context + "]";
    }
}
