// SPDX-License-Identifier: MIT
package com.cdp4j.event.webaudio;

/**
 * Notifies that an existing BaseAudioContext will be destroyed.
 */
public class ContextWillBeDestroyed {
    private String contextId;

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String toString() {
        return "ContextWillBeDestroyed [contextId=" + contextId + "]";
    }
}
