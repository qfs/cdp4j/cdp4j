// SPDX-License-Identifier: MIT
package com.cdp4j.event.webaudio;

import com.cdp4j.type.webaudio.BaseAudioContext;

/**
 * Notifies that a new BaseAudioContext has been created.
 */
public class ContextCreated {
    private BaseAudioContext context;

    public BaseAudioContext getContext() {
        return context;
    }

    public void setContext(BaseAudioContext context) {
        this.context = context;
    }

    public String toString() {
        return "ContextCreated [context=" + context + "]";
    }
}
