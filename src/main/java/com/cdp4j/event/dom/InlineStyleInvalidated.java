// SPDX-License-Identifier: MIT
package com.cdp4j.event.dom;

import com.cdp4j.annotation.Experimental;
import java.util.List;

/**
 * Fired when Element's inline style is modified via a CSS property
 * modification.
 */
@Experimental
public class InlineStyleInvalidated {
    private List<Integer> nodeIds;

    /**
     * Ids of the nodes for which the inline styles have been invalidated.
     */
    public List<Integer> getNodeIds() {
        return nodeIds;
    }

    /**
     * Ids of the nodes for which the inline styles have been invalidated.
     */
    public void setNodeIds(List<Integer> nodeIds) {
        this.nodeIds = nodeIds;
    }

    public String toString() {
        return "InlineStyleInvalidated [nodeIds=" + nodeIds + "]";
    }
}
