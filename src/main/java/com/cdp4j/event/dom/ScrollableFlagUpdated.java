// SPDX-License-Identifier: MIT
package com.cdp4j.event.dom;

import com.cdp4j.annotation.Experimental;

/**
 * Fired when a node's scrollability state changes.
 */
@Experimental
public class ScrollableFlagUpdated {
    private Integer nodeId;

    private Boolean isScrollable;

    /**
     * The id of the node.
     */
    public Integer getNodeId() {
        return nodeId;
    }

    /**
     * The id of the node.
     */
    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * If the node is scrollable.
     */
    public Boolean isIsScrollable() {
        return isScrollable;
    }

    /**
     * If the node is scrollable.
     */
    public void setIsScrollable(Boolean isScrollable) {
        this.isScrollable = isScrollable;
    }

    public String toString() {
        return "ScrollableFlagUpdated [nodeId=" + nodeId + ", isScrollable=" + isScrollable + "]";
    }
}
