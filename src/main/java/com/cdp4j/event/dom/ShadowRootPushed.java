// SPDX-License-Identifier: MIT
package com.cdp4j.event.dom;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.dom.Node;

/**
 * Called when shadow root is pushed into the element.
 */
@Experimental
public class ShadowRootPushed {
    private Integer hostId;

    private Node root;

    /**
     * Host element id.
     */
    public Integer getHostId() {
        return hostId;
    }

    /**
     * Host element id.
     */
    public void setHostId(Integer hostId) {
        this.hostId = hostId;
    }

    /**
     * Shadow root.
     */
    public Node getRoot() {
        return root;
    }

    /**
     * Shadow root.
     */
    public void setRoot(Node root) {
        this.root = root;
    }

    public String toString() {
        return "ShadowRootPushed [hostId=" + hostId + ", root=" + root + "]";
    }
}
