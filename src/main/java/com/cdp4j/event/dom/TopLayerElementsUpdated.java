// SPDX-License-Identifier: MIT
package com.cdp4j.event.dom;

import com.cdp4j.annotation.Experimental;

/**
 * Called when top layer elements are changed.
 */
@Experimental
public class TopLayerElementsUpdated {}
