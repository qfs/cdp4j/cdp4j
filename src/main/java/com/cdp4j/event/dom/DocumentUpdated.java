// SPDX-License-Identifier: MIT
package com.cdp4j.event.dom;

/**
 * Fired when Document has been totally updated. Node ids are no longer valid.
 */
public class DocumentUpdated {}
