// SPDX-License-Identifier: MIT
package com.cdp4j.event.accessibility;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.accessibility.AXNode;

/**
 * The loadComplete event mirrors the load complete event sent by the browser to
 * assistive technology when the web page has finished loading.
 */
@Experimental
public class LoadComplete {
    private AXNode root;

    /**
     * New document root node.
     */
    public AXNode getRoot() {
        return root;
    }

    /**
     * New document root node.
     */
    public void setRoot(AXNode root) {
        this.root = root;
    }

    public String toString() {
        return "LoadComplete [root=" + root + "]";
    }
}
