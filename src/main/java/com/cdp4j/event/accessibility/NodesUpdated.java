// SPDX-License-Identifier: MIT
package com.cdp4j.event.accessibility;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.accessibility.AXNode;
import java.util.List;

/**
 * The nodesUpdated event is sent every time a previously requested node has
 * changed the in tree.
 */
@Experimental
public class NodesUpdated {
    private List<AXNode> nodes;

    /**
     * Updated node data.
     */
    public List<AXNode> getNodes() {
        return nodes;
    }

    /**
     * Updated node data.
     */
    public void setNodes(List<AXNode> nodes) {
        this.nodes = nodes;
    }

    public String toString() {
        return "NodesUpdated [nodes=" + nodes + "]";
    }
}
