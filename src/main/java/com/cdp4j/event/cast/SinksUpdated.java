// SPDX-License-Identifier: MIT
package com.cdp4j.event.cast;

import com.cdp4j.type.cast.Sink;
import java.util.List;

/**
 * This is fired whenever the list of available sinks changes. A sink is a
 * device or a software surface that you can cast to.
 */
public class SinksUpdated {
    private List<Sink> sinks;

    public List<Sink> getSinks() {
        return sinks;
    }

    public void setSinks(List<Sink> sinks) {
        this.sinks = sinks;
    }

    public String toString() {
        return "SinksUpdated [sinks=" + sinks + "]";
    }
}
