// SPDX-License-Identifier: MIT
package com.cdp4j.event.cast;

/**
 * This is fired whenever the outstanding issue/error message changes.
 * |issueMessage| is empty if there is no issue.
 */
public class IssueUpdated {
    private String issueMessage;

    public String getIssueMessage() {
        return issueMessage;
    }

    public void setIssueMessage(String issueMessage) {
        this.issueMessage = issueMessage;
    }

    public String toString() {
        return "IssueUpdated [issueMessage=" + issueMessage + "]";
    }
}
