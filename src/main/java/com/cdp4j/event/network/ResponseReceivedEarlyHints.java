// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import java.util.Map;

/**
 * Fired when 103 Early Hints headers is received in addition to the common
 * response. Not every responseReceived event will have an
 * responseReceivedEarlyHints fired. Only one responseReceivedEarlyHints may be
 * fired for eached responseReceived event.
 */
@Experimental
public class ResponseReceivedEarlyHints {
    private String requestId;

    private Map<String, Object> headers;

    /**
     * Request identifier. Used to match this information to another
     * responseReceived event.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier. Used to match this information to another
     * responseReceived event.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Raw response headers as they were received over the wire. Duplicate headers
     * in the response are represented as a single key with their values
     * concatentated using \n as the separator. See also headersText that contains
     * verbatim text for HTTP/1.*.
     */
    public Map<String, Object> getHeaders() {
        return headers;
    }

    /**
     * Raw response headers as they were received over the wire. Duplicate headers
     * in the response are represented as a single key with their values
     * concatentated using \n as the separator. See also headersText that contains
     * verbatim text for HTTP/1.*.
     */
    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public String toString() {
        return "ResponseReceivedEarlyHints [requestId=" + requestId + ", headers=" + headers + "]";
    }
}
