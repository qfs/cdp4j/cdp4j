// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.type.network.BlockedReason;
import com.cdp4j.type.network.CorsErrorStatus;
import com.cdp4j.type.network.ResourceType;

/**
 * Fired when HTTP request has failed to load.
 */
public class LoadingFailed {
    private String requestId;

    private Double timestamp;

    private ResourceType type;

    private String errorText;

    private Boolean canceled;

    private BlockedReason blockedReason;

    private CorsErrorStatus corsErrorStatus;

    /**
     * Request identifier.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Timestamp.
     */
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * Timestamp.
     */
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Resource type.
     */
    public ResourceType getType() {
        return type;
    }

    /**
     * Resource type.
     */
    public void setType(ResourceType type) {
        this.type = type;
    }

    /**
     * Error message. List of network errors:
     * https://cs.chromium.org/chromium/src/net/base/net_error_list.h
     */
    public String getErrorText() {
        return errorText;
    }

    /**
     * Error message. List of network errors:
     * https://cs.chromium.org/chromium/src/net/base/net_error_list.h
     */
    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    /**
     * True if loading was canceled.
     */
    public Boolean isCanceled() {
        return canceled;
    }

    /**
     * True if loading was canceled.
     */
    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    /**
     * The reason why loading was blocked, if any.
     */
    public BlockedReason getBlockedReason() {
        return blockedReason;
    }

    /**
     * The reason why loading was blocked, if any.
     */
    public void setBlockedReason(BlockedReason blockedReason) {
        this.blockedReason = blockedReason;
    }

    /**
     * The reason why loading was blocked by CORS, if any.
     */
    public CorsErrorStatus getCorsErrorStatus() {
        return corsErrorStatus;
    }

    /**
     * The reason why loading was blocked by CORS, if any.
     */
    public void setCorsErrorStatus(CorsErrorStatus corsErrorStatus) {
        this.corsErrorStatus = corsErrorStatus;
    }

    public String toString() {
        return "LoadingFailed [requestId=" + requestId + ", timestamp=" + timestamp + ", type=" + type + ", errorText="
                + errorText + ", canceled=" + canceled + ", blockedReason=" + blockedReason + ", corsErrorStatus="
                + corsErrorStatus + "]";
    }
}
