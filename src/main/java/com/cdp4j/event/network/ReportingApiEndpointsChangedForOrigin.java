// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.network.ReportingApiEndpoint;
import java.util.List;

@Experimental
public class ReportingApiEndpointsChangedForOrigin {
    private String origin;

    private List<ReportingApiEndpoint> endpoints;

    /**
     * Origin of the document(s) which configured the endpoints.
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Origin of the document(s) which configured the endpoints.
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public List<ReportingApiEndpoint> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(List<ReportingApiEndpoint> endpoints) {
        this.endpoints = endpoints;
    }

    public String toString() {
        return "ReportingApiEndpointsChangedForOrigin [origin=" + origin + ", endpoints=" + endpoints + "]";
    }
}
