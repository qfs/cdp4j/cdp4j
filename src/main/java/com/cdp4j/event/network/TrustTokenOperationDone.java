// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.constant.TrustTokenStatus;
import com.cdp4j.type.network.TrustTokenOperationType;

/**
 * Fired exactly once for each Trust Token operation. Depending on the type of
 * the operation and whether the operation succeeded or failed, the event is
 * fired before the corresponding request was sent or after the response was
 * received.
 */
@Experimental
public class TrustTokenOperationDone {
    private TrustTokenStatus status;

    private TrustTokenOperationType type;

    private String requestId;

    private String topLevelOrigin;

    private String issuerOrigin;

    private Integer issuedTokenCount;

    /**
     * Detailed success or error status of the operation. 'AlreadyExists' also
     * signifies a successful operation, as the result of the operation already
     * exists und thus, the operation was abort preemptively (e.g. a cache hit).
     */
    public TrustTokenStatus getStatus() {
        return status;
    }

    /**
     * Detailed success or error status of the operation. 'AlreadyExists' also
     * signifies a successful operation, as the result of the operation already
     * exists und thus, the operation was abort preemptively (e.g. a cache hit).
     */
    public void setStatus(TrustTokenStatus status) {
        this.status = status;
    }

    public TrustTokenOperationType getType() {
        return type;
    }

    public void setType(TrustTokenOperationType type) {
        this.type = type;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Top level origin. The context in which the operation was attempted.
     */
    public String getTopLevelOrigin() {
        return topLevelOrigin;
    }

    /**
     * Top level origin. The context in which the operation was attempted.
     */
    public void setTopLevelOrigin(String topLevelOrigin) {
        this.topLevelOrigin = topLevelOrigin;
    }

    /**
     * Origin of the issuer in case of a "Issuance" or "Redemption" operation.
     */
    public String getIssuerOrigin() {
        return issuerOrigin;
    }

    /**
     * Origin of the issuer in case of a "Issuance" or "Redemption" operation.
     */
    public void setIssuerOrigin(String issuerOrigin) {
        this.issuerOrigin = issuerOrigin;
    }

    /**
     * The number of obtained Trust Tokens on a successful "Issuance" operation.
     */
    public Integer getIssuedTokenCount() {
        return issuedTokenCount;
    }

    /**
     * The number of obtained Trust Tokens on a successful "Issuance" operation.
     */
    public void setIssuedTokenCount(Integer issuedTokenCount) {
        this.issuedTokenCount = issuedTokenCount;
    }

    public String toString() {
        return "TrustTokenOperationDone [status=" + status + ", type=" + type + ", requestId=" + requestId
                + ", topLevelOrigin=" + topLevelOrigin + ", issuerOrigin=" + issuerOrigin + ", issuedTokenCount="
                + issuedTokenCount + "]";
    }
}
