// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.network.AssociatedCookie;
import com.cdp4j.type.network.ClientSecurityState;
import com.cdp4j.type.network.ConnectTiming;
import java.util.List;
import java.util.Map;

/**
 * Fired when additional information about a requestWillBeSent event is
 * available from the network stack. Not every requestWillBeSent event will have
 * an additional requestWillBeSentExtraInfo fired for it, and there is no
 * guarantee whether requestWillBeSent or requestWillBeSentExtraInfo will be
 * fired first for the same request.
 */
@Experimental
public class RequestWillBeSentExtraInfo {
    private String requestId;

    private List<AssociatedCookie> associatedCookies;

    private Map<String, Object> headers;

    private ConnectTiming connectTiming;

    private ClientSecurityState clientSecurityState;

    private Boolean siteHasCookieInOtherPartition;

    /**
     * Request identifier. Used to match this information to an existing
     * requestWillBeSent event.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier. Used to match this information to an existing
     * requestWillBeSent event.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * A list of cookies potentially associated to the requested URL. This includes
     * both cookies sent with the request and the ones not sent; the latter are
     * distinguished by having blockedReasons field set.
     */
    public List<AssociatedCookie> getAssociatedCookies() {
        return associatedCookies;
    }

    /**
     * A list of cookies potentially associated to the requested URL. This includes
     * both cookies sent with the request and the ones not sent; the latter are
     * distinguished by having blockedReasons field set.
     */
    public void setAssociatedCookies(List<AssociatedCookie> associatedCookies) {
        this.associatedCookies = associatedCookies;
    }

    /**
     * Raw request headers as they will be sent over the wire.
     */
    public Map<String, Object> getHeaders() {
        return headers;
    }

    /**
     * Raw request headers as they will be sent over the wire.
     */
    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    /**
     * Connection timing information for the request.
     */
    public ConnectTiming getConnectTiming() {
        return connectTiming;
    }

    /**
     * Connection timing information for the request.
     */
    public void setConnectTiming(ConnectTiming connectTiming) {
        this.connectTiming = connectTiming;
    }

    /**
     * The client security state set for the request.
     */
    public ClientSecurityState getClientSecurityState() {
        return clientSecurityState;
    }

    /**
     * The client security state set for the request.
     */
    public void setClientSecurityState(ClientSecurityState clientSecurityState) {
        this.clientSecurityState = clientSecurityState;
    }

    /**
     * Whether the site has partitioned cookies stored in a partition different than
     * the current one.
     */
    public Boolean isSiteHasCookieInOtherPartition() {
        return siteHasCookieInOtherPartition;
    }

    /**
     * Whether the site has partitioned cookies stored in a partition different than
     * the current one.
     */
    public void setSiteHasCookieInOtherPartition(Boolean siteHasCookieInOtherPartition) {
        this.siteHasCookieInOtherPartition = siteHasCookieInOtherPartition;
    }

    public String toString() {
        return "RequestWillBeSentExtraInfo [requestId=" + requestId + ", associatedCookies=" + associatedCookies
                + ", headers=" + headers + ", connectTiming=" + connectTiming + ", clientSecurityState="
                + clientSecurityState + ", siteHasCookieInOtherPartition=" + siteHasCookieInOtherPartition + "]";
    }
}
