// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.network.SignedExchangeInfo;

/**
 * Fired when a signed exchange was received over the network
 */
@Experimental
public class SignedExchangeReceived {
    private String requestId;

    private SignedExchangeInfo info;

    /**
     * Request identifier.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Information about the signed exchange response.
     */
    public SignedExchangeInfo getInfo() {
        return info;
    }

    /**
     * Information about the signed exchange response.
     */
    public void setInfo(SignedExchangeInfo info) {
        this.info = info;
    }

    public String toString() {
        return "SignedExchangeReceived [requestId=" + requestId + ", info=" + info + "]";
    }
}
