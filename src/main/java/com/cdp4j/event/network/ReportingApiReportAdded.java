// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.network.ReportingApiReport;

/**
 * Is sent whenever a new report is added. And after 'enableReportingApi' for
 * all existing reports.
 */
@Experimental
public class ReportingApiReportAdded {
    private ReportingApiReport report;

    public ReportingApiReport getReport() {
        return report;
    }

    public void setReport(ReportingApiReport report) {
        this.report = report;
    }

    public String toString() {
        return "ReportingApiReportAdded [report=" + report + "]";
    }
}
