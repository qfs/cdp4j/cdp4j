// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.network.ReportingApiReport;

@Experimental
public class ReportingApiReportUpdated {
    private ReportingApiReport report;

    public ReportingApiReport getReport() {
        return report;
    }

    public void setReport(ReportingApiReport report) {
        this.report = report;
    }

    public String toString() {
        return "ReportingApiReportUpdated [report=" + report + "]";
    }
}
