// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.network.BlockedSetCookieWithReason;
import com.cdp4j.type.network.CookiePartitionKey;
import com.cdp4j.type.network.ExemptedSetCookieWithReason;
import com.cdp4j.type.network.IPAddressSpace;
import java.util.List;
import java.util.Map;

/**
 * Fired when additional information about a responseReceived event is available
 * from the network stack. Not every responseReceived event will have an
 * additional responseReceivedExtraInfo for it, and responseReceivedExtraInfo
 * may be fired before or after responseReceived.
 */
@Experimental
public class ResponseReceivedExtraInfo {
    private String requestId;

    private List<BlockedSetCookieWithReason> blockedCookies;

    private Map<String, Object> headers;

    private IPAddressSpace resourceIPAddressSpace;

    private Integer statusCode;

    private String headersText;

    private CookiePartitionKey cookiePartitionKey;

    private Boolean cookiePartitionKeyOpaque;

    private List<ExemptedSetCookieWithReason> exemptedCookies;

    /**
     * Request identifier. Used to match this information to another
     * responseReceived event.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier. Used to match this information to another
     * responseReceived event.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * A list of cookies which were not stored from the response along with the
     * corresponding reasons for blocking. The cookies here may not be valid due to
     * syntax errors, which are represented by the invalid cookie line string
     * instead of a proper cookie.
     */
    public List<BlockedSetCookieWithReason> getBlockedCookies() {
        return blockedCookies;
    }

    /**
     * A list of cookies which were not stored from the response along with the
     * corresponding reasons for blocking. The cookies here may not be valid due to
     * syntax errors, which are represented by the invalid cookie line string
     * instead of a proper cookie.
     */
    public void setBlockedCookies(List<BlockedSetCookieWithReason> blockedCookies) {
        this.blockedCookies = blockedCookies;
    }

    /**
     * Raw response headers as they were received over the wire. Duplicate headers
     * in the response are represented as a single key with their values
     * concatentated using \n as the separator. See also headersText that contains
     * verbatim text for HTTP/1.*.
     */
    public Map<String, Object> getHeaders() {
        return headers;
    }

    /**
     * Raw response headers as they were received over the wire. Duplicate headers
     * in the response are represented as a single key with their values
     * concatentated using \n as the separator. See also headersText that contains
     * verbatim text for HTTP/1.*.
     */
    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    /**
     * The IP address space of the resource. The address space can only be
     * determined once the transport established the connection, so we can't send it
     * in requestWillBeSentExtraInfo.
     */
    public IPAddressSpace getResourceIPAddressSpace() {
        return resourceIPAddressSpace;
    }

    /**
     * The IP address space of the resource. The address space can only be
     * determined once the transport established the connection, so we can't send it
     * in requestWillBeSentExtraInfo.
     */
    public void setResourceIPAddressSpace(IPAddressSpace resourceIPAddressSpace) {
        this.resourceIPAddressSpace = resourceIPAddressSpace;
    }

    /**
     * The status code of the response. This is useful in cases the request failed
     * and no responseReceived event is triggered, which is the case for, e.g., CORS
     * errors. This is also the correct status code for cached requests, where the
     * status in responseReceived is a 200 and this will be 304.
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * The status code of the response. This is useful in cases the request failed
     * and no responseReceived event is triggered, which is the case for, e.g., CORS
     * errors. This is also the correct status code for cached requests, where the
     * status in responseReceived is a 200 and this will be 304.
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Raw response header text as it was received over the wire. The raw text may
     * not always be available, such as in the case of HTTP/2 or QUIC.
     */
    public String getHeadersText() {
        return headersText;
    }

    /**
     * Raw response header text as it was received over the wire. The raw text may
     * not always be available, such as in the case of HTTP/2 or QUIC.
     */
    public void setHeadersText(String headersText) {
        this.headersText = headersText;
    }

    /**
     * The cookie partition key that will be used to store partitioned cookies set
     * in this response. Only sent when partitioned cookies are enabled.
     */
    public CookiePartitionKey getCookiePartitionKey() {
        return cookiePartitionKey;
    }

    /**
     * The cookie partition key that will be used to store partitioned cookies set
     * in this response. Only sent when partitioned cookies are enabled.
     */
    public void setCookiePartitionKey(CookiePartitionKey cookiePartitionKey) {
        this.cookiePartitionKey = cookiePartitionKey;
    }

    /**
     * True if partitioned cookies are enabled, but the partition key is not
     * serializable to string.
     */
    public Boolean isCookiePartitionKeyOpaque() {
        return cookiePartitionKeyOpaque;
    }

    /**
     * True if partitioned cookies are enabled, but the partition key is not
     * serializable to string.
     */
    public void setCookiePartitionKeyOpaque(Boolean cookiePartitionKeyOpaque) {
        this.cookiePartitionKeyOpaque = cookiePartitionKeyOpaque;
    }

    /**
     * A list of cookies which should have been blocked by 3PCD but are exempted and
     * stored from the response with the corresponding reason.
     */
    public List<ExemptedSetCookieWithReason> getExemptedCookies() {
        return exemptedCookies;
    }

    /**
     * A list of cookies which should have been blocked by 3PCD but are exempted and
     * stored from the response with the corresponding reason.
     */
    public void setExemptedCookies(List<ExemptedSetCookieWithReason> exemptedCookies) {
        this.exemptedCookies = exemptedCookies;
    }

    public String toString() {
        return "ResponseReceivedExtraInfo [requestId=" + requestId + ", blockedCookies=" + blockedCookies + ", headers="
                + headers + ", resourceIPAddressSpace=" + resourceIPAddressSpace + ", statusCode=" + statusCode
                + ", headersText=" + headersText + ", cookiePartitionKey=" + cookiePartitionKey
                + ", cookiePartitionKeyOpaque=" + cookiePartitionKeyOpaque + ", exemptedCookies=" + exemptedCookies
                + "]";
    }
}
