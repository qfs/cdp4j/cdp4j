// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;

/**
 * Fired once security policy has been updated.
 */
@Experimental
public class PolicyUpdated {}
