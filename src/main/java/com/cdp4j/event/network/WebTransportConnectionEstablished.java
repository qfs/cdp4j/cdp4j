// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

/**
 * Fired when WebTransport handshake is finished.
 */
public class WebTransportConnectionEstablished {
    private String transportId;

    private Double timestamp;

    /**
     * WebTransport identifier.
     */
    public String getTransportId() {
        return transportId;
    }

    /**
     * WebTransport identifier.
     */
    public void setTransportId(String transportId) {
        this.transportId = transportId;
    }

    /**
     * Timestamp.
     */
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * Timestamp.
     */
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        return "WebTransportConnectionEstablished [transportId=" + transportId + ", timestamp=" + timestamp + "]";
    }
}
