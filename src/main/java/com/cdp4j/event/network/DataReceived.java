// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

/**
 * Fired when data chunk was received over the network.
 */
public class DataReceived {
    private String requestId;

    private Double timestamp;

    private Integer dataLength;

    private Integer encodedDataLength;

    private String data;

    /**
     * Request identifier.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Timestamp.
     */
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * Timestamp.
     */
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Data chunk length.
     */
    public Integer getDataLength() {
        return dataLength;
    }

    /**
     * Data chunk length.
     */
    public void setDataLength(Integer dataLength) {
        this.dataLength = dataLength;
    }

    /**
     * Actual bytes received (might be less than dataLength for compressed
     * encodings).
     */
    public Integer getEncodedDataLength() {
        return encodedDataLength;
    }

    /**
     * Actual bytes received (might be less than dataLength for compressed
     * encodings).
     */
    public void setEncodedDataLength(Integer encodedDataLength) {
        this.encodedDataLength = encodedDataLength;
    }

    /**
     * Data that was received. (Encoded as a base64 string when passed over JSON)
     */
    public String getData() {
        return data;
    }

    /**
     * Data that was received. (Encoded as a base64 string when passed over JSON)
     */
    public void setData(String data) {
        this.data = data;
    }

    public String toString() {
        return "DataReceived [requestId=" + requestId + ", timestamp=" + timestamp + ", dataLength=" + dataLength
                + ", encodedDataLength=" + encodedDataLength + ", data=" + data + "]";
    }
}
