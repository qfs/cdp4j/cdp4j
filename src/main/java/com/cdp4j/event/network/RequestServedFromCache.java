// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

/**
 * Fired if request ended up loading from cache.
 */
public class RequestServedFromCache {
    private String requestId;

    /**
     * Request identifier.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String toString() {
        return "RequestServedFromCache [requestId=" + requestId + "]";
    }
}
