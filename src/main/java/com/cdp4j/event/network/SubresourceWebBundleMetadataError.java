// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.annotation.Experimental;

/**
 * Fired once when parsing the .wbn file has failed.
 */
@Experimental
public class SubresourceWebBundleMetadataError {
    private String requestId;

    private String errorMessage;

    /**
     * Request identifier. Used to match this information to another event.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Request identifier. Used to match this information to another event.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Error message
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String toString() {
        return "SubresourceWebBundleMetadataError [requestId=" + requestId + ", errorMessage=" + errorMessage + "]";
    }
}
