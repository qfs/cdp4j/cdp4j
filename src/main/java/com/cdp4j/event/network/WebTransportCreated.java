// SPDX-License-Identifier: MIT
package com.cdp4j.event.network;

import com.cdp4j.type.network.Initiator;

/**
 * Fired upon WebTransport creation.
 */
public class WebTransportCreated {
    private String transportId;

    private String url;

    private Double timestamp;

    private Initiator initiator;

    /**
     * WebTransport identifier.
     */
    public String getTransportId() {
        return transportId;
    }

    /**
     * WebTransport identifier.
     */
    public void setTransportId(String transportId) {
        this.transportId = transportId;
    }

    /**
     * WebTransport request URL.
     */
    public String getUrl() {
        return url;
    }

    /**
     * WebTransport request URL.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Timestamp.
     */
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * Timestamp.
     */
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Request initiator.
     */
    public Initiator getInitiator() {
        return initiator;
    }

    /**
     * Request initiator.
     */
    public void setInitiator(Initiator initiator) {
        this.initiator = initiator;
    }

    public String toString() {
        return "WebTransportCreated [transportId=" + transportId + ", url=" + url + ", timestamp=" + timestamp
                + ", initiator=" + initiator + "]";
    }
}
