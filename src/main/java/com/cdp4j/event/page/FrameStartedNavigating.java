// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.constant.FrameStartedNavigatingNavigationType;

/**
 * Fired when a navigation starts. This event is fired for both
 * renderer-initiated and browser-initiated navigations. For renderer-initiated
 * navigations, the event is fired after frameRequestedNavigation. Navigation
 * may still be cancelled after the event is issued. Multiple events can be
 * fired for a single navigation, for example, when a same-document navigation
 * becomes a cross-document navigation (such as in the case of a frameset).
 */
@Experimental
public class FrameStartedNavigating {
    private String frameId;

    private String url;

    private String loaderId;

    private FrameStartedNavigatingNavigationType navigationType;

    /**
     * ID of the frame that is being navigated.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * ID of the frame that is being navigated.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    /**
     * The URL the navigation started with. The final URL can be different.
     */
    public String getUrl() {
        return url;
    }

    /**
     * The URL the navigation started with. The final URL can be different.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Loader identifier. Even though it is present in case of same-document
     * navigation, the previously committed loaderId would not change unless the
     * navigation changes from a same-document to a cross-document navigation.
     */
    public String getLoaderId() {
        return loaderId;
    }

    /**
     * Loader identifier. Even though it is present in case of same-document
     * navigation, the previously committed loaderId would not change unless the
     * navigation changes from a same-document to a cross-document navigation.
     */
    public void setLoaderId(String loaderId) {
        this.loaderId = loaderId;
    }

    public FrameStartedNavigatingNavigationType getNavigationType() {
        return navigationType;
    }

    public void setNavigationType(FrameStartedNavigatingNavigationType navigationType) {
        this.navigationType = navigationType;
    }

    public String toString() {
        return "FrameStartedNavigating [frameId=" + frameId + ", url=" + url + ", loaderId=" + loaderId
                + ", navigationType=" + navigationType + "]";
    }
}
