// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;

/**
 * Fired when frame has stopped loading.
 */
@Experimental
public class FrameStoppedLoading {
    private String frameId;

    /**
     * Id of the frame that has stopped loading.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * Id of the frame that has stopped loading.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public String toString() {
        return "FrameStoppedLoading [frameId=" + frameId + "]";
    }
}
