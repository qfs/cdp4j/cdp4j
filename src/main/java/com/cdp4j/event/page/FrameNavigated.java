// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.type.page.Frame;
import com.cdp4j.type.page.NavigationType;

/**
 * Fired once navigation of the frame has completed. Frame is now associated
 * with the new loader.
 */
public class FrameNavigated {
    private Frame frame;

    private NavigationType type;

    /**
     * Frame object.
     */
    public Frame getFrame() {
        return frame;
    }

    /**
     * Frame object.
     */
    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public NavigationType getType() {
        return type;
    }

    public void setType(NavigationType type) {
        this.type = type;
    }

    public String toString() {
        return "FrameNavigated [frame=" + frame + ", type=" + type + "]";
    }
}
