// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

/**
 * Fired when frame no longer has a scheduled navigation.
 */
public class FrameClearedScheduledNavigation {
    private String frameId;

    /**
     * Id of the frame that has cleared its scheduled navigation.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * Id of the frame that has cleared its scheduled navigation.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public String toString() {
        return "FrameClearedScheduledNavigation [frameId=" + frameId + "]";
    }
}
