// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.constant.NavigationWithinDocumentType;

/**
 * Fired when same-document navigation happens, e.g. due to history API usage or
 * anchor navigation.
 */
@Experimental
public class NavigatedWithinDocument {
    private String frameId;

    private String url;

    private NavigationWithinDocumentType navigationType;

    /**
     * Id of the frame.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * Id of the frame.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    /**
     * Frame's new url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Frame's new url.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Navigation type
     */
    public NavigationWithinDocumentType getNavigationType() {
        return navigationType;
    }

    /**
     * Navigation type
     */
    public void setNavigationType(NavigationWithinDocumentType navigationType) {
        this.navigationType = navigationType;
    }

    public String toString() {
        return "NavigatedWithinDocument [frameId=" + frameId + ", url=" + url + ", navigationType=" + navigationType
                + "]";
    }
}
