// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;

/**
 * Fired when the page with currently enabled screencast was shown or hidden .
 */
@Experimental
public class ScreencastVisibilityChanged {
    private Boolean visible;

    /**
     * True if the page is visible.
     */
    public Boolean isVisible() {
        return visible;
    }

    /**
     * True if the page is visible.
     */
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public String toString() {
        return "ScreencastVisibilityChanged [visible=" + visible + "]";
    }
}
