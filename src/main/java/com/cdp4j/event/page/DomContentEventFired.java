// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

public class DomContentEventFired {
    private Double timestamp;

    public Double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        return "DomContentEventFired [timestamp=" + timestamp + "]";
    }
}
