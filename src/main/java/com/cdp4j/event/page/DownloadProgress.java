// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.constant.DownloadState;

/**
 * Fired when download makes progress. Last call has |done| == true. Deprecated.
 * Use Browser.downloadProgress instead.
 */
@Experimental
public class DownloadProgress {
    private String guid;

    private Double totalBytes;

    private Double receivedBytes;

    private DownloadState state;

    /**
     * Global unique identifier of the download.
     */
    public String getGuid() {
        return guid;
    }

    /**
     * Global unique identifier of the download.
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * Total expected bytes to download.
     */
    public Double getTotalBytes() {
        return totalBytes;
    }

    /**
     * Total expected bytes to download.
     */
    public void setTotalBytes(Double totalBytes) {
        this.totalBytes = totalBytes;
    }

    /**
     * Total bytes received.
     */
    public Double getReceivedBytes() {
        return receivedBytes;
    }

    /**
     * Total bytes received.
     */
    public void setReceivedBytes(Double receivedBytes) {
        this.receivedBytes = receivedBytes;
    }

    /**
     * Download status.
     */
    public DownloadState getState() {
        return state;
    }

    /**
     * Download status.
     */
    public void setState(DownloadState state) {
        this.state = state;
    }

    public String toString() {
        return "DownloadProgress [guid=" + guid + ", totalBytes=" + totalBytes + ", receivedBytes=" + receivedBytes
                + ", state=" + state + "]";
    }
}
