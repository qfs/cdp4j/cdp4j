// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.type.constant.FrameDetachedReason;

/**
 * Fired when frame has been detached from its parent.
 */
public class FrameDetached {
    private String frameId;

    private FrameDetachedReason reason;

    /**
     * Id of the frame that has been detached.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * Id of the frame that has been detached.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public FrameDetachedReason getReason() {
        return reason;
    }

    public void setReason(FrameDetachedReason reason) {
        this.reason = reason;
    }

    public String toString() {
        return "FrameDetached [frameId=" + frameId + ", reason=" + reason + "]";
    }
}
