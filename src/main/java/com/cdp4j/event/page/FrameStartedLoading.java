// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;

/**
 * Fired when frame has started loading.
 */
@Experimental
public class FrameStartedLoading {
    private String frameId;

    /**
     * Id of the frame that has started loading.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * Id of the frame that has started loading.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public String toString() {
        return "FrameStartedLoading [frameId=" + frameId + "]";
    }
}
