// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

/**
 * Fired when interstitial page was shown
 */
public class InterstitialShown {}
