// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.page.Frame;

/**
 * Fired when opening document to write to.
 */
@Experimental
public class DocumentOpened {
    private Frame frame;

    /**
     * Frame object.
     */
    public Frame getFrame() {
        return frame;
    }

    /**
     * Frame object.
     */
    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    public String toString() {
        return "DocumentOpened [frame=" + frame + "]";
    }
}
