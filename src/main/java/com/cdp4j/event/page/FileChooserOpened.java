// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.type.constant.FileChooserInputMode;

/**
 * Emitted only when page.interceptFileChooser is enabled.
 */
public class FileChooserOpened {
    private String frameId;

    private FileChooserInputMode mode;

    private Integer backendNodeId;

    /**
     * Id of the frame containing input node.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * Id of the frame containing input node.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    /**
     * Input mode.
     */
    public FileChooserInputMode getMode() {
        return mode;
    }

    /**
     * Input mode.
     */
    public void setMode(FileChooserInputMode mode) {
        this.mode = mode;
    }

    /**
     * Input node id. Only present for file choosers opened via an
     * <input type="file"> element.
     */
    public Integer getBackendNodeId() {
        return backendNodeId;
    }

    /**
     * Input node id. Only present for file choosers opened via an
     * <input type="file"> element.
     */
    public void setBackendNodeId(Integer backendNodeId) {
        this.backendNodeId = backendNodeId;
    }

    public String toString() {
        return "FileChooserOpened [frameId=" + frameId + ", mode=" + mode + ", backendNodeId=" + backendNodeId + "]";
    }
}
