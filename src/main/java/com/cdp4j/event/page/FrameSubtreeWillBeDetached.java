// SPDX-License-Identifier: MIT
package com.cdp4j.event.page;

import com.cdp4j.annotation.Experimental;

/**
 * Fired before frame subtree is detached. Emitted before any frame of the
 * subtree is actually detached.
 */
@Experimental
public class FrameSubtreeWillBeDetached {
    private String frameId;

    /**
     * Id of the frame that is the root of the subtree that will be detached.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * Id of the frame that is the root of the subtree that will be detached.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public String toString() {
        return "FrameSubtreeWillBeDetached [frameId=" + frameId + "]";
    }
}
