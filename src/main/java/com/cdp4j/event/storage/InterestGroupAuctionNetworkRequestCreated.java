// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

import com.cdp4j.type.storage.InterestGroupAuctionFetchType;
import java.util.List;

/**
 * Specifies which auctions a particular network fetch may be related to, and in
 * what role. Note that it is not ordered with respect to
 * Network.requestWillBeSent (but will happen before loadingFinished
 * loadingFailed).
 */
public class InterestGroupAuctionNetworkRequestCreated {
    private InterestGroupAuctionFetchType type;

    private String requestId;

    private List<String> auctions;

    public InterestGroupAuctionFetchType getType() {
        return type;
    }

    public void setType(InterestGroupAuctionFetchType type) {
        this.type = type;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * This is the set of the auctions using the worklet that issued this request.
     * In the case of trusted signals, it's possible that only some of them actually
     * care about the keys being queried.
     */
    public List<String> getAuctions() {
        return auctions;
    }

    /**
     * This is the set of the auctions using the worklet that issued this request.
     * In the case of trusted signals, it's possible that only some of them actually
     * care about the keys being queried.
     */
    public void setAuctions(List<String> auctions) {
        this.auctions = auctions;
    }

    public String toString() {
        return "InterestGroupAuctionNetworkRequestCreated [type=" + type + ", requestId=" + requestId + ", auctions="
                + auctions + "]";
    }
}
