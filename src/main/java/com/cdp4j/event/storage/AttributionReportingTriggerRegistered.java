// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.storage.AttributionReportingAggregatableResult;
import com.cdp4j.type.storage.AttributionReportingEventLevelResult;
import com.cdp4j.type.storage.AttributionReportingTriggerRegistration;

@Experimental
public class AttributionReportingTriggerRegistered {
    private AttributionReportingTriggerRegistration registration;

    private AttributionReportingEventLevelResult eventLevel;

    private AttributionReportingAggregatableResult aggregatable;

    public AttributionReportingTriggerRegistration getRegistration() {
        return registration;
    }

    public void setRegistration(AttributionReportingTriggerRegistration registration) {
        this.registration = registration;
    }

    public AttributionReportingEventLevelResult getEventLevel() {
        return eventLevel;
    }

    public void setEventLevel(AttributionReportingEventLevelResult eventLevel) {
        this.eventLevel = eventLevel;
    }

    public AttributionReportingAggregatableResult getAggregatable() {
        return aggregatable;
    }

    public void setAggregatable(AttributionReportingAggregatableResult aggregatable) {
        this.aggregatable = aggregatable;
    }

    public String toString() {
        return "AttributionReportingTriggerRegistered [registration=" + registration + ", eventLevel=" + eventLevel
                + ", aggregatable=" + aggregatable + "]";
    }
}
