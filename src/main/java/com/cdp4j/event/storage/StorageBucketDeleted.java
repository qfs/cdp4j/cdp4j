// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

public class StorageBucketDeleted {
    private String bucketId;

    public String getBucketId() {
        return bucketId;
    }

    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String toString() {
        return "StorageBucketDeleted [bucketId=" + bucketId + "]";
    }
}
