// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.storage.AttributionReportingSourceRegistration;
import com.cdp4j.type.storage.AttributionReportingSourceRegistrationResult;

@Experimental
public class AttributionReportingSourceRegistered {
    private AttributionReportingSourceRegistration registration;

    private AttributionReportingSourceRegistrationResult result;

    public AttributionReportingSourceRegistration getRegistration() {
        return registration;
    }

    public void setRegistration(AttributionReportingSourceRegistration registration) {
        this.registration = registration;
    }

    public AttributionReportingSourceRegistrationResult getResult() {
        return result;
    }

    public void setResult(AttributionReportingSourceRegistrationResult result) {
        this.result = result;
    }

    public String toString() {
        return "AttributionReportingSourceRegistered [registration=" + registration + ", result=" + result + "]";
    }
}
