// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

import com.cdp4j.type.storage.InterestGroupAuctionEventType;

/**
 * An auction involving interest groups is taking place. These events are
 * target-specific.
 */
public class InterestGroupAuctionEventOccurred {
    private Double eventTime;

    private InterestGroupAuctionEventType type;

    private String uniqueAuctionId;

    private String parentAuctionId;

    private Object auctionConfig;

    public Double getEventTime() {
        return eventTime;
    }

    public void setEventTime(Double eventTime) {
        this.eventTime = eventTime;
    }

    public InterestGroupAuctionEventType getType() {
        return type;
    }

    public void setType(InterestGroupAuctionEventType type) {
        this.type = type;
    }

    public String getUniqueAuctionId() {
        return uniqueAuctionId;
    }

    public void setUniqueAuctionId(String uniqueAuctionId) {
        this.uniqueAuctionId = uniqueAuctionId;
    }

    /**
     * Set for child auctions.
     */
    public String getParentAuctionId() {
        return parentAuctionId;
    }

    /**
     * Set for child auctions.
     */
    public void setParentAuctionId(String parentAuctionId) {
        this.parentAuctionId = parentAuctionId;
    }

    /**
     * Set for started and configResolved
     */
    public Object getAuctionConfig() {
        return auctionConfig;
    }

    /**
     * Set for started and configResolved
     */
    public void setAuctionConfig(Object auctionConfig) {
        this.auctionConfig = auctionConfig;
    }

    public String toString() {
        return "InterestGroupAuctionEventOccurred [eventTime=" + eventTime + ", type=" + type + ", uniqueAuctionId="
                + uniqueAuctionId + ", parentAuctionId=" + parentAuctionId + ", auctionConfig=" + auctionConfig + "]";
    }
}
