// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

import com.cdp4j.type.storage.InterestGroupAccessType;

/**
 * One of the interest groups was accessed. Note that these events are global to
 * all targets sharing an interest group store.
 */
public class InterestGroupAccessed {
    private Double accessTime;

    private InterestGroupAccessType type;

    private String ownerOrigin;

    private String name;

    private String componentSellerOrigin;

    private Double bid;

    private String bidCurrency;

    private String uniqueAuctionId;

    public Double getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(Double accessTime) {
        this.accessTime = accessTime;
    }

    public InterestGroupAccessType getType() {
        return type;
    }

    public void setType(InterestGroupAccessType type) {
        this.type = type;
    }

    public String getOwnerOrigin() {
        return ownerOrigin;
    }

    public void setOwnerOrigin(String ownerOrigin) {
        this.ownerOrigin = ownerOrigin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * For topLevelBid/topLevelAdditionalBid, and when appropriate, win and
     * additionalBidWin
     */
    public String getComponentSellerOrigin() {
        return componentSellerOrigin;
    }

    /**
     * For topLevelBid/topLevelAdditionalBid, and when appropriate, win and
     * additionalBidWin
     */
    public void setComponentSellerOrigin(String componentSellerOrigin) {
        this.componentSellerOrigin = componentSellerOrigin;
    }

    /**
     * For bid or somethingBid event, if done locally and not on a server.
     */
    public Double getBid() {
        return bid;
    }

    /**
     * For bid or somethingBid event, if done locally and not on a server.
     */
    public void setBid(Double bid) {
        this.bid = bid;
    }

    public String getBidCurrency() {
        return bidCurrency;
    }

    public void setBidCurrency(String bidCurrency) {
        this.bidCurrency = bidCurrency;
    }

    /**
     * For non-global events --- links to interestGroupAuctionEvent
     */
    public String getUniqueAuctionId() {
        return uniqueAuctionId;
    }

    /**
     * For non-global events --- links to interestGroupAuctionEvent
     */
    public void setUniqueAuctionId(String uniqueAuctionId) {
        this.uniqueAuctionId = uniqueAuctionId;
    }

    public String toString() {
        return "InterestGroupAccessed [accessTime=" + accessTime + ", type=" + type + ", ownerOrigin=" + ownerOrigin
                + ", name=" + name + ", componentSellerOrigin=" + componentSellerOrigin + ", bid=" + bid
                + ", bidCurrency=" + bidCurrency + ", uniqueAuctionId=" + uniqueAuctionId + "]";
    }
}
