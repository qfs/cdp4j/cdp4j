// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

/**
 * A cache has been added/deleted.
 */
public class CacheStorageListUpdated {
    private String origin;

    private String storageKey;

    private String bucketId;

    /**
     * Origin to update.
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Origin to update.
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     * Storage key to update.
     */
    public String getStorageKey() {
        return storageKey;
    }

    /**
     * Storage key to update.
     */
    public void setStorageKey(String storageKey) {
        this.storageKey = storageKey;
    }

    /**
     * Storage bucket to update.
     */
    public String getBucketId() {
        return bucketId;
    }

    /**
     * Storage bucket to update.
     */
    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String toString() {
        return "CacheStorageListUpdated [origin=" + origin + ", storageKey=" + storageKey + ", bucketId=" + bucketId
                + "]";
    }
}
