// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

import com.cdp4j.type.storage.StorageBucketInfo;

public class StorageBucketCreatedOrUpdated {
    private StorageBucketInfo bucketInfo;

    public StorageBucketInfo getBucketInfo() {
        return bucketInfo;
    }

    public void setBucketInfo(StorageBucketInfo bucketInfo) {
        this.bucketInfo = bucketInfo;
    }

    public String toString() {
        return "StorageBucketCreatedOrUpdated [bucketInfo=" + bucketInfo + "]";
    }
}
