// SPDX-License-Identifier: MIT
package com.cdp4j.event.storage;

/**
 * The origin's IndexedDB object store has been modified.
 */
public class IndexedDBContentUpdated {
    private String origin;

    private String storageKey;

    private String bucketId;

    private String databaseName;

    private String objectStoreName;

    /**
     * Origin to update.
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Origin to update.
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     * Storage key to update.
     */
    public String getStorageKey() {
        return storageKey;
    }

    /**
     * Storage key to update.
     */
    public void setStorageKey(String storageKey) {
        this.storageKey = storageKey;
    }

    /**
     * Storage bucket to update.
     */
    public String getBucketId() {
        return bucketId;
    }

    /**
     * Storage bucket to update.
     */
    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    /**
     * Database to update.
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * Database to update.
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * ObjectStore to update.
     */
    public String getObjectStoreName() {
        return objectStoreName;
    }

    /**
     * ObjectStore to update.
     */
    public void setObjectStoreName(String objectStoreName) {
        this.objectStoreName = objectStoreName;
    }

    public String toString() {
        return "IndexedDBContentUpdated [origin=" + origin + ", storageKey=" + storageKey + ", bucketId=" + bucketId
                + ", databaseName=" + databaseName + ", objectStoreName=" + objectStoreName + "]";
    }
}
