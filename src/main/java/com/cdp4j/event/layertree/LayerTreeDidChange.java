// SPDX-License-Identifier: MIT
package com.cdp4j.event.layertree;

import com.cdp4j.type.layertree.Layer;
import java.util.List;

public class LayerTreeDidChange {
    private List<Layer> layers;

    /**
     * Layer tree, absent if not in the compositing mode.
     */
    public List<Layer> getLayers() {
        return layers;
    }

    /**
     * Layer tree, absent if not in the compositing mode.
     */
    public void setLayers(List<Layer> layers) {
        this.layers = layers;
    }

    public String toString() {
        return "LayerTreeDidChange [layers=" + layers + "]";
    }
}
