// SPDX-License-Identifier: MIT
package com.cdp4j.event.layertree;

import com.cdp4j.type.dom.Rect;

public class LayerPainted {
    private String layerId;

    private Rect clip;

    /**
     * The id of the painted layer.
     */
    public String getLayerId() {
        return layerId;
    }

    /**
     * The id of the painted layer.
     */
    public void setLayerId(String layerId) {
        this.layerId = layerId;
    }

    /**
     * Clip rectangle.
     */
    public Rect getClip() {
        return clip;
    }

    /**
     * Clip rectangle.
     */
    public void setClip(Rect clip) {
        this.clip = clip;
    }

    public String toString() {
        return "LayerPainted [layerId=" + layerId + ", clip=" + clip + "]";
    }
}
