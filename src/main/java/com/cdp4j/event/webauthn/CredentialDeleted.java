// SPDX-License-Identifier: MIT
package com.cdp4j.event.webauthn;

/**
 * Triggered when a credential is deleted, e.g. through
 * PublicKeyCredential.signalUnknownCredential().
 */
public class CredentialDeleted {
    private String authenticatorId;

    private String credentialId;

    public String getAuthenticatorId() {
        return authenticatorId;
    }

    public void setAuthenticatorId(String authenticatorId) {
        this.authenticatorId = authenticatorId;
    }

    public String getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(String credentialId) {
        this.credentialId = credentialId;
    }

    public String toString() {
        return "CredentialDeleted [authenticatorId=" + authenticatorId + ", credentialId=" + credentialId + "]";
    }
}
