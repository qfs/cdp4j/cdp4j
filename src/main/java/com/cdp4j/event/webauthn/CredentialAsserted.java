// SPDX-License-Identifier: MIT
package com.cdp4j.event.webauthn;

import com.cdp4j.type.webauthn.Credential;

/**
 * Triggered when a credential is used in a webauthn assertion.
 */
public class CredentialAsserted {
    private String authenticatorId;

    private Credential credential;

    public String getAuthenticatorId() {
        return authenticatorId;
    }

    public void setAuthenticatorId(String authenticatorId) {
        this.authenticatorId = authenticatorId;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public String toString() {
        return "CredentialAsserted [authenticatorId=" + authenticatorId + ", credential=" + credential + "]";
    }
}
