// SPDX-License-Identifier: MIT
package com.cdp4j.event.security;

import com.cdp4j.type.security.InsecureContentStatus;
import com.cdp4j.type.security.SecurityState;
import com.cdp4j.type.security.SecurityStateExplanation;
import java.util.List;

/**
 * The security state of the page changed. No longer being sent.
 */
public class SecurityStateChanged {
    private SecurityState securityState;

    private Boolean schemeIsCryptographic;

    private List<SecurityStateExplanation> explanations;

    private InsecureContentStatus insecureContentStatus;

    private String summary;

    /**
     * Security state.
     */
    public SecurityState getSecurityState() {
        return securityState;
    }

    /**
     * Security state.
     */
    public void setSecurityState(SecurityState securityState) {
        this.securityState = securityState;
    }

    /**
     * True if the page was loaded over cryptographic transport such as HTTPS.
     */
    public Boolean isSchemeIsCryptographic() {
        return schemeIsCryptographic;
    }

    /**
     * True if the page was loaded over cryptographic transport such as HTTPS.
     */
    public void setSchemeIsCryptographic(Boolean schemeIsCryptographic) {
        this.schemeIsCryptographic = schemeIsCryptographic;
    }

    /**
     * Previously a list of explanations for the security state. Now always empty.
     */
    public List<SecurityStateExplanation> getExplanations() {
        return explanations;
    }

    /**
     * Previously a list of explanations for the security state. Now always empty.
     */
    public void setExplanations(List<SecurityStateExplanation> explanations) {
        this.explanations = explanations;
    }

    /**
     * Information about insecure content on the page.
     */
    public InsecureContentStatus getInsecureContentStatus() {
        return insecureContentStatus;
    }

    /**
     * Information about insecure content on the page.
     */
    public void setInsecureContentStatus(InsecureContentStatus insecureContentStatus) {
        this.insecureContentStatus = insecureContentStatus;
    }

    /**
     * Overrides user-visible description of the state. Always omitted.
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Overrides user-visible description of the state. Always omitted.
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String toString() {
        return "SecurityStateChanged [securityState=" + securityState + ", schemeIsCryptographic="
                + schemeIsCryptographic + ", explanations=" + explanations + ", insecureContentStatus="
                + insecureContentStatus + ", summary=" + summary + "]";
    }
}
