// SPDX-License-Identifier: MIT
package com.cdp4j.event.security;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.security.VisibleSecurityState;

/**
 * The security state of the page changed.
 */
@Experimental
public class VisibleSecurityStateChanged {
    private VisibleSecurityState visibleSecurityState;

    /**
     * Security state information about the page.
     */
    public VisibleSecurityState getVisibleSecurityState() {
        return visibleSecurityState;
    }

    /**
     * Security state information about the page.
     */
    public void setVisibleSecurityState(VisibleSecurityState visibleSecurityState) {
        this.visibleSecurityState = visibleSecurityState;
    }

    public String toString() {
        return "VisibleSecurityStateChanged [visibleSecurityState=" + visibleSecurityState + "]";
    }
}
