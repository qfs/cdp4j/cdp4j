// SPDX-License-Identifier: MIT
package com.cdp4j.event.css;

/**
 * Fires whenever a MediaQuery result changes (for example, after a browser window has been
 * resized.) The current implementation considers only viewport-dependent media features.
 */
public class MediaQueryResultChanged {}
