// SPDX-License-Identifier: MIT
package com.cdp4j.event.css;

/**
 * Fired whenever a stylesheet is changed as a result of the client operation.
 */
public class StyleSheetChanged {
    private String styleSheetId;

    public String getStyleSheetId() {
        return styleSheetId;
    }

    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    public String toString() {
        return "StyleSheetChanged [styleSheetId=" + styleSheetId + "]";
    }
}
