// SPDX-License-Identifier: MIT
package com.cdp4j.event.css;

/**
 * Fired whenever an active document stylesheet is removed.
 */
public class StyleSheetRemoved {
    private String styleSheetId;

    /**
     * Identifier of the removed stylesheet.
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * Identifier of the removed stylesheet.
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    public String toString() {
        return "StyleSheetRemoved [styleSheetId=" + styleSheetId + "]";
    }
}
