// SPDX-License-Identifier: MIT
package com.cdp4j.event.css;

import com.cdp4j.type.css.CSSStyleSheetHeader;

/**
 * Fired whenever an active document stylesheet is added.
 */
public class StyleSheetAdded {
    private CSSStyleSheetHeader header;

    /**
     * Added stylesheet metainfo.
     */
    public CSSStyleSheetHeader getHeader() {
        return header;
    }

    /**
     * Added stylesheet metainfo.
     */
    public void setHeader(CSSStyleSheetHeader header) {
        this.header = header;
    }

    public String toString() {
        return "StyleSheetAdded [header=" + header + "]";
    }
}
