// SPDX-License-Identifier: MIT
package com.cdp4j.event.css;

import com.cdp4j.annotation.Experimental;

@Experimental
public class ComputedStyleUpdated {
    private Integer nodeId;

    /**
     * The node id that has updated computed styles.
     */
    public Integer getNodeId() {
        return nodeId;
    }

    /**
     * The node id that has updated computed styles.
     */
    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public String toString() {
        return "ComputedStyleUpdated [nodeId=" + nodeId + "]";
    }
}
