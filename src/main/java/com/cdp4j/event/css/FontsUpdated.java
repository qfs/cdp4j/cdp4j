// SPDX-License-Identifier: MIT
package com.cdp4j.event.css;

import com.cdp4j.type.css.FontFace;

/**
 * Fires whenever a web font is updated. A non-empty font parameter indicates a
 * successfully loaded web font.
 */
public class FontsUpdated {
    private FontFace font;

    /**
     * The web font that has loaded.
     */
    public FontFace getFont() {
        return font;
    }

    /**
     * The web font that has loaded.
     */
    public void setFont(FontFace font) {
        this.font = font;
    }

    public String toString() {
        return "FontsUpdated [font=" + font + "]";
    }
}
