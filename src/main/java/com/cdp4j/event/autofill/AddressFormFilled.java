// SPDX-License-Identifier: MIT
package com.cdp4j.event.autofill;

import com.cdp4j.type.autofill.AddressUI;
import com.cdp4j.type.autofill.FilledField;
import java.util.List;

/**
 * Emitted when an address form is filled.
 */
public class AddressFormFilled {
    private List<FilledField> filledFields;

    private AddressUI addressUi;

    /**
     * Information about the fields that were filled
     */
    public List<FilledField> getFilledFields() {
        return filledFields;
    }

    /**
     * Information about the fields that were filled
     */
    public void setFilledFields(List<FilledField> filledFields) {
        this.filledFields = filledFields;
    }

    /**
     * An UI representation of the address used to fill the form. Consists of a 2D
     * array where each child represents an address/profile line.
     */
    public AddressUI getAddressUi() {
        return addressUi;
    }

    /**
     * An UI representation of the address used to fill the form. Consists of a 2D
     * array where each child represents an address/profile line.
     */
    public void setAddressUi(AddressUI addressUi) {
        this.addressUi = addressUi;
    }

    public String toString() {
        return "AddressFormFilled [filledFields=" + filledFields + ", addressUi=" + addressUi + "]";
    }
}
