// SPDX-License-Identifier: MIT
package com.cdp4j.event.input;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.input.DragData;

/**
 * Emitted only when Input.setInterceptDrags is enabled. Use this data with
 * Input.dispatchDragEvent to restore normal drag and drop behavior.
 */
@Experimental
public class DragIntercepted {
    private DragData data;

    public DragData getData() {
        return data;
    }

    public void setData(DragData data) {
        this.data = data;
    }

    public String toString() {
        return "DragIntercepted [data=" + data + "]";
    }
}
