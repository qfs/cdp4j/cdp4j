// SPDX-License-Identifier: MIT
package com.cdp4j.event.heapprofiler;

public class ReportHeapSnapshotProgress {
    private Integer done;

    private Integer total;

    private Boolean finished;

    public Integer getDone() {
        return done;
    }

    public void setDone(Integer done) {
        this.done = done;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Boolean isFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public String toString() {
        return "ReportHeapSnapshotProgress [done=" + done + ", total=" + total + ", finished=" + finished + "]";
    }
}
