// SPDX-License-Identifier: MIT
package com.cdp4j.event.heapprofiler;

public class AddHeapSnapshotChunk {
    private String chunk;

    public String getChunk() {
        return chunk;
    }

    public void setChunk(String chunk) {
        this.chunk = chunk;
    }

    public String toString() {
        return "AddHeapSnapshotChunk [chunk=" + chunk + "]";
    }
}
