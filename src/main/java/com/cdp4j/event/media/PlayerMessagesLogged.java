// SPDX-License-Identifier: MIT
package com.cdp4j.event.media;

import com.cdp4j.type.media.PlayerMessage;
import java.util.List;

/**
 * Send a list of any messages that need to be delivered.
 */
public class PlayerMessagesLogged {
    private String playerId;

    private List<PlayerMessage> messages;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public List<PlayerMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<PlayerMessage> messages) {
        this.messages = messages;
    }

    public String toString() {
        return "PlayerMessagesLogged [playerId=" + playerId + ", messages=" + messages + "]";
    }
}
