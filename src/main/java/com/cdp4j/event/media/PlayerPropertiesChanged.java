// SPDX-License-Identifier: MIT
package com.cdp4j.event.media;

import com.cdp4j.type.media.PlayerProperty;
import java.util.List;

/**
 * This can be called multiple times, and can be used to set / override / remove
 * player properties. A null propValue indicates removal.
 */
public class PlayerPropertiesChanged {
    private String playerId;

    private List<PlayerProperty> properties;

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public List<PlayerProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<PlayerProperty> properties) {
        this.properties = properties;
    }

    public String toString() {
        return "PlayerPropertiesChanged [playerId=" + playerId + ", properties=" + properties + "]";
    }
}
