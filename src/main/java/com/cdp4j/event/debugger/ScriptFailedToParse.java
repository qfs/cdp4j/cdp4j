// SPDX-License-Identifier: MIT
package com.cdp4j.event.debugger;

import com.cdp4j.type.debugger.ScriptLanguage;
import com.cdp4j.type.runtime.StackTrace;

/**
 * Fired when virtual machine fails to parse the script.
 */
public class ScriptFailedToParse {
    private String scriptId;

    private String url;

    private Integer startLine;

    private Integer startColumn;

    private Integer endLine;

    private Integer endColumn;

    private Integer executionContextId;

    private String hash;

    private String buildId;

    private Object executionContextAuxData;

    private String sourceMapURL;

    private Boolean hasSourceURL;

    private Boolean isModule;

    private Integer length;

    private StackTrace stackTrace;

    private Integer codeOffset;

    private ScriptLanguage scriptLanguage;

    private String embedderName;

    /**
     * Identifier of the script parsed.
     */
    public String getScriptId() {
        return scriptId;
    }

    /**
     * Identifier of the script parsed.
     */
    public void setScriptId(String scriptId) {
        this.scriptId = scriptId;
    }

    /**
     * URL or name of the script parsed (if any).
     */
    public String getUrl() {
        return url;
    }

    /**
     * URL or name of the script parsed (if any).
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Line offset of the script within the resource with given URL (for script
     * tags).
     */
    public Integer getStartLine() {
        return startLine;
    }

    /**
     * Line offset of the script within the resource with given URL (for script
     * tags).
     */
    public void setStartLine(Integer startLine) {
        this.startLine = startLine;
    }

    /**
     * Column offset of the script within the resource with given URL.
     */
    public Integer getStartColumn() {
        return startColumn;
    }

    /**
     * Column offset of the script within the resource with given URL.
     */
    public void setStartColumn(Integer startColumn) {
        this.startColumn = startColumn;
    }

    /**
     * Last line of the script.
     */
    public Integer getEndLine() {
        return endLine;
    }

    /**
     * Last line of the script.
     */
    public void setEndLine(Integer endLine) {
        this.endLine = endLine;
    }

    /**
     * Length of the last line of the script.
     */
    public Integer getEndColumn() {
        return endColumn;
    }

    /**
     * Length of the last line of the script.
     */
    public void setEndColumn(Integer endColumn) {
        this.endColumn = endColumn;
    }

    /**
     * Specifies script creation context.
     */
    public Integer getExecutionContextId() {
        return executionContextId;
    }

    /**
     * Specifies script creation context.
     */
    public void setExecutionContextId(Integer executionContextId) {
        this.executionContextId = executionContextId;
    }

    /**
     * Content hash of the script, SHA-256.
     */
    public String getHash() {
        return hash;
    }

    /**
     * Content hash of the script, SHA-256.
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * For Wasm modules, the content of the build_id custom section.
     */
    public String getBuildId() {
        return buildId;
    }

    /**
     * For Wasm modules, the content of the build_id custom section.
     */
    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    /**
     * Embedder-specific auxiliary data likely matching {isDefault: boolean, type:
     * 'default'|'isolated'|'worker', frameId: string}
     */
    public Object getExecutionContextAuxData() {
        return executionContextAuxData;
    }

    /**
     * Embedder-specific auxiliary data likely matching {isDefault: boolean, type:
     * 'default'|'isolated'|'worker', frameId: string}
     */
    public void setExecutionContextAuxData(Object executionContextAuxData) {
        this.executionContextAuxData = executionContextAuxData;
    }

    /**
     * URL of source map associated with script (if any).
     */
    public String getSourceMapURL() {
        return sourceMapURL;
    }

    /**
     * URL of source map associated with script (if any).
     */
    public void setSourceMapURL(String sourceMapURL) {
        this.sourceMapURL = sourceMapURL;
    }

    /**
     * True, if this script has sourceURL.
     */
    public Boolean isHasSourceURL() {
        return hasSourceURL;
    }

    /**
     * True, if this script has sourceURL.
     */
    public void setHasSourceURL(Boolean hasSourceURL) {
        this.hasSourceURL = hasSourceURL;
    }

    /**
     * True, if this script is ES6 module.
     */
    public Boolean isIsModule() {
        return isModule;
    }

    /**
     * True, if this script is ES6 module.
     */
    public void setIsModule(Boolean isModule) {
        this.isModule = isModule;
    }

    /**
     * This script length.
     */
    public Integer getLength() {
        return length;
    }

    /**
     * This script length.
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * JavaScript top stack frame of where the script parsed event was triggered if
     * available.
     */
    public StackTrace getStackTrace() {
        return stackTrace;
    }

    /**
     * JavaScript top stack frame of where the script parsed event was triggered if
     * available.
     */
    public void setStackTrace(StackTrace stackTrace) {
        this.stackTrace = stackTrace;
    }

    /**
     * If the scriptLanguage is WebAssembly, the code section offset in the module.
     */
    public Integer getCodeOffset() {
        return codeOffset;
    }

    /**
     * If the scriptLanguage is WebAssembly, the code section offset in the module.
     */
    public void setCodeOffset(Integer codeOffset) {
        this.codeOffset = codeOffset;
    }

    /**
     * The language of the script.
     */
    public ScriptLanguage getScriptLanguage() {
        return scriptLanguage;
    }

    /**
     * The language of the script.
     */
    public void setScriptLanguage(ScriptLanguage scriptLanguage) {
        this.scriptLanguage = scriptLanguage;
    }

    /**
     * The name the embedder supplied for this script.
     */
    public String getEmbedderName() {
        return embedderName;
    }

    /**
     * The name the embedder supplied for this script.
     */
    public void setEmbedderName(String embedderName) {
        this.embedderName = embedderName;
    }

    public String toString() {
        return "ScriptFailedToParse [scriptId=" + scriptId + ", url=" + url + ", startLine=" + startLine
                + ", startColumn=" + startColumn + ", endLine=" + endLine + ", endColumn=" + endColumn
                + ", executionContextId=" + executionContextId + ", hash=" + hash + ", buildId=" + buildId
                + ", executionContextAuxData=" + executionContextAuxData + ", sourceMapURL=" + sourceMapURL
                + ", hasSourceURL=" + hasSourceURL + ", isModule=" + isModule + ", length=" + length + ", stackTrace="
                + stackTrace + ", codeOffset=" + codeOffset + ", scriptLanguage=" + scriptLanguage + ", embedderName="
                + embedderName + "]";
    }
}
