// SPDX-License-Identifier: MIT
package com.cdp4j.event.debugger;

import com.cdp4j.type.constant.PauseReason;
import com.cdp4j.type.debugger.CallFrame;
import com.cdp4j.type.runtime.StackTrace;
import com.cdp4j.type.runtime.StackTraceId;
import java.util.List;

/**
 * Fired when the virtual machine stopped on breakpoint or exception or any
 * other stop criteria.
 */
public class Paused {
    private List<CallFrame> callFrames;

    private PauseReason reason;

    private Object data;

    private List<String> hitBreakpoints;

    private StackTrace asyncStackTrace;

    private StackTraceId asyncStackTraceId;

    private StackTraceId asyncCallStackTraceId;

    /**
     * Call stack the virtual machine stopped on.
     */
    public List<CallFrame> getCallFrames() {
        return callFrames;
    }

    /**
     * Call stack the virtual machine stopped on.
     */
    public void setCallFrames(List<CallFrame> callFrames) {
        this.callFrames = callFrames;
    }

    /**
     * Pause reason.
     */
    public PauseReason getReason() {
        return reason;
    }

    /**
     * Pause reason.
     */
    public void setReason(PauseReason reason) {
        this.reason = reason;
    }

    /**
     * Object containing break-specific auxiliary properties.
     */
    public Object getData() {
        return data;
    }

    /**
     * Object containing break-specific auxiliary properties.
     */
    public void setData(Object data) {
        this.data = data;
    }

    /**
     * Hit breakpoints IDs
     */
    public List<String> getHitBreakpoints() {
        return hitBreakpoints;
    }

    /**
     * Hit breakpoints IDs
     */
    public void setHitBreakpoints(List<String> hitBreakpoints) {
        this.hitBreakpoints = hitBreakpoints;
    }

    /**
     * Async stack trace, if any.
     */
    public StackTrace getAsyncStackTrace() {
        return asyncStackTrace;
    }

    /**
     * Async stack trace, if any.
     */
    public void setAsyncStackTrace(StackTrace asyncStackTrace) {
        this.asyncStackTrace = asyncStackTrace;
    }

    /**
     * Async stack trace, if any.
     */
    public StackTraceId getAsyncStackTraceId() {
        return asyncStackTraceId;
    }

    /**
     * Async stack trace, if any.
     */
    public void setAsyncStackTraceId(StackTraceId asyncStackTraceId) {
        this.asyncStackTraceId = asyncStackTraceId;
    }

    /**
     * Never present, will be removed.
     */
    public StackTraceId getAsyncCallStackTraceId() {
        return asyncCallStackTraceId;
    }

    /**
     * Never present, will be removed.
     */
    public void setAsyncCallStackTraceId(StackTraceId asyncCallStackTraceId) {
        this.asyncCallStackTraceId = asyncCallStackTraceId;
    }

    public String toString() {
        return "Paused [callFrames=" + callFrames + ", reason=" + reason + ", data=" + data + ", hitBreakpoints="
                + hitBreakpoints + ", asyncStackTrace=" + asyncStackTrace + ", asyncStackTraceId=" + asyncStackTraceId
                + ", asyncCallStackTraceId=" + asyncCallStackTraceId + "]";
    }
}
