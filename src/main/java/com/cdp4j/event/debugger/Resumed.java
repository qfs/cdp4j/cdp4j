// SPDX-License-Identifier: MIT
package com.cdp4j.event.debugger;

/**
 * Fired when the virtual machine resumed execution.
 */
public class Resumed {}
