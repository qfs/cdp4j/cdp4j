// SPDX-License-Identifier: MIT
package com.cdp4j.event.debugger;

import com.cdp4j.type.debugger.Location;

/**
 * Fired when breakpoint is resolved to an actual script and location.
 * Deprecated in favor of resolvedBreakpoints in the scriptParsed event.
 */
public class BreakpointResolved {
    private String breakpointId;

    private Location location;

    /**
     * Breakpoint unique identifier.
     */
    public String getBreakpointId() {
        return breakpointId;
    }

    /**
     * Breakpoint unique identifier.
     */
    public void setBreakpointId(String breakpointId) {
        this.breakpointId = breakpointId;
    }

    /**
     * Actual breakpoint location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Actual breakpoint location.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    public String toString() {
        return "BreakpointResolved [breakpointId=" + breakpointId + ", location=" + location + "]";
    }
}
