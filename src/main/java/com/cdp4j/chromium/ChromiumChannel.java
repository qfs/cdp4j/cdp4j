// SPDX-License-Identifier: MIT
package com.cdp4j.chromium;

public enum ChromiumChannel {
    /**
     * Canary builds are the bleeding edge. Released daily, this build has not been
     * tested or used, it's released as soon as it's built.
     */
    canary,
    /**
     * If you want to see what's happening quickly, then you want the Dev channel.
     * The Dev channel gets updated once or twice weekly, and it shows what we're
     * working on right now. There's no lag between major versions, whatever code
     * we've got, you will get. While this build does get tested, it is still
     * subject to bugs, as we want people to see what's new as soon as possible.
     */
    dev,
    /**
     * If you are interested in seeing what's next, with minimal risk, Beta channel
     * is the place to be. It's updated every week roughly, with major updates
     * coming every six weeks, more than a month before the Stable channel will get
     * them.
     */
    beta,
    /**
     * This channel has gotten the full testing and blessing of the Chrome test
     * team, and is the best bet to avoid crashes and other issues. It's updated
     * roughly every two-three weeks for minor releases, and every 6 weeks for major
     * releases.
     */
    stable
}
