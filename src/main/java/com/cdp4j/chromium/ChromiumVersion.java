// SPDX-License-Identifier: MIT
package com.cdp4j.chromium;

import static java.time.LocalDate.of;
import static java.time.temporal.ChronoUnit.DAYS;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public final class ChromiumVersion implements Comparable<ChromiumVersion> {

    private static class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

        @Override
        @SuppressWarnings("unchecked")
        public LocalDate deserialize(JsonParser p, DeserializationContext ctxt)
                throws IOException, JsonProcessingException {
            Map<String, Object> map = ctxt.readValue(p, Map.class);
            int year = (int) map.get("year");
            int month = (int) map.get("month");
            int dayOfMonth = (int) map.get("day");
            LocalDate date = of(year, month, dayOfMonth);
            return date;
        }
    }

    private static class LocalDateSerializer extends JsonSerializer<LocalDate> {

        @Override
        public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("year", value.getYear());
            map.put("month", value.getMonthValue());
            map.put("day", value.getDayOfMonth());
            gen.writeObject(map);
        }
    }

    private final String version;

    private final ChromiumChannel channel;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private final LocalDate releaseDate;

    private final Long position;

    @JsonCreator
    ChromiumVersion(
            @JsonProperty("version") String version,
            @JsonProperty("channel") ChromiumChannel channel,
            @JsonProperty("releaseDate") LocalDate releaseDate,
            @JsonProperty("position") Long position) {
        this.version = version;
        this.channel = channel;
        this.releaseDate = releaseDate;
        this.position = position;
    }

    public String getVersion() {
        return version;
    }

    public ChromiumChannel getChannel() {
        return channel;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @Override
    public int compareTo(ChromiumVersion o) {
        return releaseDate.compareTo(o.getReleaseDate());
    }

    public Long getPosition() {
        return position;
    }

    public long daysBetween(LocalDate date) {
        return DAYS.between(releaseDate, date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(channel, position, releaseDate, version);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ChromiumVersion other = (ChromiumVersion) obj;
        return channel == other.channel
                && Objects.equals(position, other.position)
                && Objects.equals(releaseDate, other.releaseDate)
                && Objects.equals(version, other.version);
    }

    @Override
    public String toString() {
        return "ChromiumVersion [version=" + version + ", channel=" + channel + ", releaseDate=" + releaseDate + "]";
    }
}
