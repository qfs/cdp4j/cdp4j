// SPDX-License-Identifier: MIT
package com.cdp4j.chromium;

import static com.cdp4j.Constant.LINUX;
import static com.cdp4j.Constant.MACOS;
import static com.cdp4j.Constant.WINDOWS;
import static com.cdp4j.chromium.ChromiumChannel.beta;
import static com.cdp4j.chromium.ChromiumChannel.canary;
import static com.cdp4j.chromium.ChromiumChannel.dev;
import static com.cdp4j.chromium.ChromiumChannel.stable;
import static java.lang.Integer.compare;
import static java.lang.Long.parseLong;
import static java.lang.Math.round;
import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.lang.Thread.currentThread;
import static java.net.URI.create;
import static java.net.http.HttpClient.Redirect.ALWAYS;
import static java.net.http.HttpClient.Version.HTTP_1_1;
import static java.net.http.HttpRequest.BodyPublishers.noBody;
import static java.net.http.HttpResponse.BodyHandlers.discarding;
import static java.net.http.HttpResponse.BodyHandlers.ofByteArray;
import static java.net.http.HttpResponse.BodyHandlers.ofFile;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;
import static java.nio.file.Files.createDirectory;
import static java.nio.file.Files.createFile;
import static java.nio.file.Files.delete;
import static java.nio.file.Files.exists;
import static java.nio.file.Files.isDirectory;
import static java.nio.file.Files.isExecutable;
import static java.nio.file.Files.list;
import static java.nio.file.Files.size;
import static java.nio.file.Files.walkFileTree;
import static java.nio.file.Files.write;
import static java.time.LocalDate.now;
import static java.time.LocalDate.parse;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Arrays.asList;
import static java.util.Collections.sort;
import static java.util.Collections.unmodifiableList;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.concurrent.locks.LockSupport.parkNanos;
import static java.util.concurrent.locks.LockSupport.unpark;
import static java.util.stream.Collectors.toList;

import com.cdp4j.exception.CdpException;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.logger.CdpLoggerFactory;
import com.cdp4j.serialization.GsonMapper;
import com.cdp4j.serialization.JacksonMapper;
import com.cdp4j.serialization.JsonMapper;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Builder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Chromium {

    /* begin - json fields */
    private static final String FIELD_BRANCH_BASE_POSITION = "branch_base_position";

    private static final String FIELD_CURRENT_RELDATE = "current_reldate";

    private static final String FIELD_CURRENT_VERSION = "current_version";

    private static final String FIELD_CHANNEL = "channel";

    private static final String FIELD_VERSIONS = "versions";

    private static final String FIELD_OS = "os";
    /* end - json fields */

    /* begin - channels */
    private static final String CHANNEL_CANARY = "canary";

    private static final String CHANNEL_STABLE = "stable";

    private static final String CHANNEL_BETA = "beta";

    private static final String CHANNEL_DEV = "dev";
    /* end - channels */

    private static final int HTTP_OK = 200;

    private static final int ZIP_EXTRACT_TIMEOUT = 60;

    private static final int MAX_RETRY = 100;

    private static final String CHROMIUM_DIRECTORY_NAME = "chromium-%s-%s";

    private static final String CHROMIUM_ARCHIVE_FILE = "chromium-%s-%s.zip";

    private static final String VERSION_INFO_FILE_EXTENSION = ".json";

    /* begin - Chromium offical download links */
    private static final String CHROMIUM_LATEST_VERSION_LIST_URI = "https://omahaproxy.appspot.com/all.json";

    private static final String WIN64_DOWNLOAD_LINK =
            "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Win_x64%%2F%d%%2Fchrome-win.zip?alt=media";

    private static final String LINUX_DOWNLOAD_LINK =
            "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%%2F%d%%2Fchrome-linux.zip?alt=media";

    private static final String MAC_DOWNLOAD_LINK =
            "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Mac%%2F%d%%2Fchrome-mac.zip?alt=media";

    private static final String MAC_ARM_DOWNLOAD_LINK =
            "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Mac_Arm%%2F%d%%2Fchrome-mac.zip?alt=media";
    /* end - Chromium offical download links */

    private final JsonMapper mapper;

    private final String os;

    private final Path downloadPath;

    private final String downloadLink;

    private final CdpLogger log;

    private final ProxySelector proxySelector;

    private HttpClient client;

    private static class DirectoryCleanerVisitor extends SimpleFileVisitor<Path> {

        private final Path root;

        public DirectoryCleanerVisitor(Path root) {
            this.root = root;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            if (dir.startsWith(root)) {
                return CONTINUE;
            }
            return SKIP_SUBTREE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            delete(file);
            return CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            if (dir.startsWith(root)) {
                delete(dir);
            }
            return CONTINUE;
        }
    }

    private static class DownloadProgressTask implements Runnable {

        private final Path archive;

        private final long expectedFileSize;

        private final long waitTime;

        private final CdpLogger log;

        private boolean done;

        private Thread currentThread;

        private long lastProgress;

        DownloadProgressTask(Path archive, long expectedFileSize, CdpLogger log) {
            this.archive = archive;
            this.expectedFileSize = expectedFileSize;
            this.log = log;
            this.waitTime = MILLISECONDS.toNanos(250);
        }

        @Override
        public void run() {
            currentThread = currentThread();
            while (!done) {
                parkNanos(waitTime);
                if (exists(archive)) {
                    try {
                        long actualFileSize = size(archive);
                        if (actualFileSize > 0) {
                            long progress = round(((double) actualFileSize / expectedFileSize) * 100);
                            if (lastProgress != progress) {
                                log.info("Download progress: %{}", progress);
                                lastProgress = progress;
                            }
                            if (actualFileSize == expectedFileSize) {
                                done = true;
                            }
                        }
                    } catch (IOException e) {
                        // ignore
                    }
                }
            }
            currentThread = null;
        }

        public void dispose() {
            if (currentThread != null) {
                unpark(currentThread);
                done = true;
                currentThread = null;
            }
        }
    }

    private static class Artifact {
        public URI uri;
        private long length;
    }

    public Chromium(ChromiumOptions o) {
        this.downloadPath = o.downloadPath();
        this.log = new CdpLoggerFactory(o.loggerType(), o.consoleLoggerLevel(), o.loggerNamePrefix())
                .getLogger("cdp.chromium");
        switch (o.jsonLibrary()) {
            case Jackson:
                mapper = new JacksonMapper();
                break;
            default:
            case Gson:
                mapper = new GsonMapper();
                break;
        }
        if (WINDOWS) {
            os = "win64";
            downloadLink = WIN64_DOWNLOAD_LINK;
        } else if (MACOS) {
            String arch = getProperty("os.arch");
            if (arch != null && arch.contains("aarch")) {
                downloadLink = MAC_ARM_DOWNLOAD_LINK;
                os = "mac_arm64";
            } else {
                downloadLink = MAC_DOWNLOAD_LINK;
                os = "mac";
            }
        } else {
            downloadLink = LINUX_DOWNLOAD_LINK;
            os = "linux";
        }
        if (!exists(downloadPath)) {
            try {
                log.info("Creating Chromium download directory [{}]", downloadPath.toString());
                createDirectory(downloadPath);
            } catch (IOException e) {
                throw new CdpException(e);
            }
        }
        this.proxySelector = o.proxySelector();
    }

    @SuppressWarnings("unchecked")
    public List<ChromiumVersion> listLatestVersions() {
        URI uri = create(CHROMIUM_LATEST_VERSION_LIST_URI);
        HttpRequest request = HttpRequest.newBuilder(uri).GET().build();
        HttpResponse<byte[]> response = null;
        try {
            log.info("Listing latest Chromium versions (HTTP GET: [{}])", uri.toString());
            response = getHttpClient().send(request, ofByteArray());
        } catch (IOException | InterruptedException e) {
            throw new CdpException(e);
        }
        byte[] content = response.body();
        if (log.isDebugEnabled()) {
            log.debug("Latest Chromium versions (HTTP Response): {}", new String(content));
        }
        List<Map<String, Object>> data = mapper.fromJson(new ByteArrayInputStream(content), List.class);
        List<ChromiumVersion> latestVersions = new ArrayList<>();
        DateTimeFormatter releaseDatePattern = ofPattern("MM/dd/yy");
        for (Map<String, Object> next : data) {
            if (os.equals(next.get(FIELD_OS))) {
                List<Map<String, Object>> chromiumVersions = (List<Map<String, Object>>) next.get(FIELD_VERSIONS);
                for (Map<String, Object> chromiumVersion : chromiumVersions) {
                    String channelStr = (String) chromiumVersion.get(FIELD_CHANNEL);
                    ChromiumChannel channel = null;
                    if (CHANNEL_DEV.equals(channelStr)) {
                        channel = dev;
                    } else if (CHANNEL_BETA.equals(channelStr)) {
                        channel = beta;
                    } else if (CHANNEL_STABLE.equals(channelStr)) {
                        channel = stable;
                    } else if (CHANNEL_CANARY.equals(channelStr)) {
                        channel = canary;
                    } else {
                        continue;
                    }
                    String releaseDateStr = (String) chromiumVersion.get(FIELD_CURRENT_RELDATE);
                    LocalDate releaseDate = parse(releaseDateStr, releaseDatePattern);
                    ChromiumVersion c = new ChromiumVersion(
                            (String) chromiumVersion.get(FIELD_CURRENT_VERSION),
                            channel,
                            releaseDate,
                            parseLong((String) chromiumVersion.get(FIELD_BRANCH_BASE_POSITION)));
                    latestVersions.add(c);
                }
            }
        }
        sort(latestVersions);
        sort(
                latestVersions,
                (o1, o2) -> compare(o2.getChannel().ordinal(), o1.getChannel().ordinal()));
        unmodifiableList(latestVersions);
        if (!latestVersions.isEmpty()) {
            log.info("Latest Chromium versions:");
            for (ChromiumVersion v : latestVersions) {
                log.info("{}", v);
            }
        }
        return latestVersions;
    }

    public ChromiumVersion download(ChromiumChannel channel) {
        List<ChromiumVersion> latestVersions = listLatestVersions();
        Optional<ChromiumVersion> found =
                latestVersions.stream().filter(c -> channel == c.getChannel()).findFirst();
        if (!found.isPresent()) {
            throw new CdpException("Chromium channel [" + channel + "] not found");
        }
        ChromiumVersion latestChromium = found.get();
        List<ChromiumVersion> installedVersions = listInstalledVersions();
        Optional<ChromiumVersion> installed = installedVersions.stream()
                .filter(c -> c.getChannel() == channel)
                .findFirst();
        if (!installedVersions.isEmpty()) {
            log.info("Installed Chromium versions:");
            for (ChromiumVersion v : installedVersions) {
                log.info("{}", v);
            }
        }
        // is latest version already installed?
        if (!installed.isEmpty()) {
            ChromiumVersion installedChromium = installed.get();
            if (installedChromium.equals(latestChromium)) {
                Path executablePath = getExecutablePath(installedChromium);
                if (executablePath != null) {
                    log.info(
                            "Skipping download, latest Chromium version is already installed: {}",
                            latestChromium.toString());
                    return latestChromium;
                }
            }
        }
        download(latestChromium);
        extract(latestChromium);
        return latestChromium;
    }

    public ChromiumVersion download(ChromiumChannel channel, long daysAgo) {
        ChromiumVersion c = isInstalled(channel, daysAgo);
        if (c != null) {
            log.info("Chromium is already installed: {}", c);
            return c;
        }
        return download(channel);
    }

    public Path getExecutablePath(ChromiumVersion c) {
        Path path = downloadPath.resolve(format(CHROMIUM_DIRECTORY_NAME, c.getVersion(), c.getChannel()));
        if (WINDOWS) {
            path = path.resolve("chrome.exe");
        } else if (LINUX) {
            path = path.resolve("chrome");
        } else {
            path = path.resolve("Chromium.app")
                    .resolve("Contents")
                    .resolve("MacOS")
                    .resolve("Chromium");
        }
        try {
            if (isExecutable(path)) {
                return path;
            }
        } catch (Throwable t) {
            log.error("Chromium executable not found: [{}]", path.toString());
            log.error(t.getMessage(), t);
            return null;
        }
        return null;
    }

    public List<ChromiumVersion> listInstalledVersions() {
        List<ChromiumVersion> versions = new ArrayList<>();
        File[] files = downloadPath.toFile().listFiles((f, n) -> n.endsWith(VERSION_INFO_FILE_EXTENSION));
        for (File next : files) {
            try (InputStream is = new FileInputStream(next)) {
                ChromiumVersion c = mapper.fromJson(is, ChromiumVersion.class);
                if (getExecutablePath(c) != null) {
                    versions.add(c);
                }
            } catch (IOException e) {
                throw new CdpException(e);
            }
        }
        sort(versions);
        sort(
                versions,
                (o1, o2) -> compare(o2.getChannel().ordinal(), o1.getChannel().ordinal()));
        unmodifiableList(versions);
        return versions;
    }

    public ChromiumVersion isInstalled(ChromiumChannel channel, long daysAgo) {
        if (daysAgo < 0) {
            throw new IllegalArgumentException("daysAgo");
        }
        ChromiumVersion found = null;
        Optional<ChromiumVersion> installedVersion = listInstalledVersions().stream()
                .filter(c -> c.getChannel() == channel)
                .findFirst();
        if (installedVersion.isPresent()) {
            ChromiumVersion c = installedVersion.get();
            long diff = c.daysBetween(now());
            if (diff <= daysAgo) {
                found = c;
            }
        }
        log.info(
                "is Chromium [{}] version is already installed and released before at most [{}] days ago? [{}]",
                channel,
                daysAgo,
                found != null);
        return found;
    }

    private void download(ChromiumVersion c) {
        Artifact artifact = getDownloadUri(c);
        if (artifact.uri == null) {
            throw new CdpException("Invalid URI");
        }
        if (!exists(downloadPath)) {
            try {
                log.info("Creating Chromium download directory: [{}]", downloadPath.toString());
                createDirectory(downloadPath);
            } catch (IOException e) {
                throw new CdpException(e);
            }
        }
        String chromiumArchiveFile = format(CHROMIUM_ARCHIVE_FILE, c.getVersion(), c.getChannel());
        Path chromiumArchive = downloadPath.resolve(chromiumArchiveFile);
        if (exists(chromiumArchive)) {
            if (chromiumArchive.toFile().length() == artifact.length) {
                log.info("Skipping download, Chromium archive is already exist: [{}]", chromiumArchiveFile.toString());
                return;
            } else {
                try {
                    log.info(
                            "Deleting existing Chromium archive file: [{}]. Expected file size: [{}], actual file size: [{}]",
                            chromiumArchiveFile.toString(),
                            artifact.length,
                            size(chromiumArchive));
                    delete(chromiumArchive);
                } catch (IOException e) {
                    throw new CdpException(e);
                }
            }
        }
        try {
            log.info(
                    "Downloading Chromium version: [{}], channel: [{}], release date: [{}]",
                    c.getVersion(),
                    c.getChannel(),
                    c.getReleaseDate());
            DownloadProgressTask progressTask = null;
            if (artifact.length > 0) {
                Thread t = new Thread(progressTask = new DownloadProgressTask(chromiumArchive, artifact.length, log));
                t.setDaemon(true);
                t.setName("CdpChromiumDownloadProgressTask");
                t.start();
            }
            HttpResponse<Path> response = getHttpClient()
                    .send(HttpRequest.newBuilder().GET().uri(artifact.uri).build(), ofFile(chromiumArchive));
            if (progressTask != null) {
                progressTask.dispose();
            }
            if (response.statusCode() != HTTP_OK) {
                throw new CdpException("Download error, HTTP status code: " + response.statusCode());
            } else {
                log.info("Chromium archive file downloaded successfully: [{}]", chromiumArchive.toString());
            }
        } catch (IOException | InterruptedException e) {
            throw new CdpException(e);
        }
    }

    private Artifact getDownloadUri(ChromiumVersion version) {
        final Artifact artifact = new Artifact();
        long position = version.getPosition().longValue();
        int count = 0;
        while (artifact.uri == null && count < MAX_RETRY) {
            String url = format(downloadLink, position);
            HttpRequest request =
                    HttpRequest.newBuilder(create(url)).method("HEAD", noBody()).build();
            HttpResponse<Void> response = null;
            try {
                response = getHttpClient().send(request, discarding());
                if (response.statusCode() == HTTP_OK) {
                    artifact.uri = response.uri();
                    response.headers().firstValueAsLong("content-length").ifPresent(value -> artifact.length = value);
                    break;
                }
                position -= 1;
            } catch (IOException | InterruptedException e) {
                throw new CdpException(e);
            }
            count += 1;
        }
        log.info("Chromium download uri: [{}]", artifact.uri);
        return artifact;
    }

    private Path extract(ChromiumVersion c) {
        Path chromiumArchive = downloadPath.resolve(format(CHROMIUM_ARCHIVE_FILE, c.getVersion(), c.getChannel()));
        Path installPath = downloadPath.resolve(format(CHROMIUM_DIRECTORY_NAME, c.getVersion(), c.getChannel()));
        try {
            if (exists(installPath)) {
                List<Path> files = list(installPath).collect(toList());
                if (!files.isEmpty()) {
                    return installPath;
                }
            } else {
                createDirectory(installPath);
            }
            String json = mapper.toJson(c);
            Path jsonFile = downloadPath.resolve(installPath.getFileName().toString() + VERSION_INFO_FILE_EXTENSION);
            if (exists(jsonFile)) {
                delete(jsonFile);
            }
            createFile(jsonFile);
            write(jsonFile, json.getBytes());
        } catch (IOException e) {
            throw new CdpException(e);
        }
        try {
            List<String> args = new ArrayList<>();
            if (WINDOWS) {
                args.add("cmd");
                args.add("/c");
            }
            if (LINUX) {
                args.add("unzip");
                args.add("-qq");
                args.add(chromiumArchive.getFileName().toString());
            } else {
                args.addAll(asList(
                        "tar",
                        "-xf",
                        chromiumArchive.getFileName().toString(),
                        "-C",
                        installPath.getFileName().toString(),
                        "--strip-components=1"));
            }
            log.info(
                    "Extracting Chromium archive file: [{}] to directory: [{}]",
                    chromiumArchive.toString(),
                    installPath.toString());
            ProcessBuilder pb = new ProcessBuilder(args);
            pb.directory(downloadPath.toFile());
            Process process = pb.start();
            process.waitFor(ZIP_EXTRACT_TIMEOUT, SECONDS);
            if (process.exitValue() != 0) {
                throw new CdpException("Unable to extract archive file: " + chromiumArchive.toString());
            }
            if (exists(chromiumArchive)) {
                delete(chromiumArchive);
            }
            if (LINUX) {
                downloadPath.resolve("chrome-linux").toFile().renameTo(installPath.toFile());
            }
            return installPath;
        } catch (IOException | InterruptedException e) {
            throw new CdpException(e);
        }
    }

    private HttpClient getHttpClient() {
        if (client == null) {
            Builder builder = HttpClient.newBuilder().version(HTTP_1_1).followRedirects(ALWAYS);
            if (proxySelector != null) {
                builder.proxy(proxySelector);
            }
            client = builder.build();
        }
        return client;
    }

    public void deleteOldVersions(EnumSet<ChromiumChannel> channels, long daysAgo) {
        LocalDate releaseDate = now().minusDays(daysAgo);
        for (ChromiumVersion next : listInstalledVersions()) {
            long diff = DAYS.between(releaseDate, next.getReleaseDate());
            if (diff < 0 && channels.contains(next.getChannel())) {
                Path installPath =
                        downloadPath.resolve(format(CHROMIUM_DIRECTORY_NAME, next.getVersion(), next.getChannel()));
                if (exists(installPath) && isDirectory(installPath)) {
                    try {
                        log.info("Deleting old Chromium version, install path: [{}]", installPath.toString());
                        walkFileTree(installPath, new DirectoryCleanerVisitor(installPath));
                        Path jsonMetaDataFile = downloadPath.resolve(
                                installPath.getFileName().toString() + VERSION_INFO_FILE_EXTENSION);
                        if (exists(jsonMetaDataFile)) {
                            delete(jsonMetaDataFile);
                        }
                    } catch (IOException e) {
                        throw new CdpException(e);
                    }
                }
            }
        }
    }
}
