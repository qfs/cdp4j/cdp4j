// SPDX-License-Identifier: MIT
package com.cdp4j.chromium;

import static com.cdp4j.JsonLibrary.Gson;
import static com.cdp4j.logger.CdpLoggerType.Null;
import static com.cdp4j.logger.CdpLogggerLevel.Info;
import static java.lang.System.getProperty;
import static java.nio.file.Paths.get;

import com.cdp4j.JsonLibrary;
import com.cdp4j.logger.CdpLoggerType;
import com.cdp4j.logger.CdpLogggerLevel;
import java.net.ProxySelector;
import java.nio.file.Path;

public class ChromiumOptions {

    private CdpLoggerType loggerType;

    private CdpLogggerLevel consoleLoggerLevel;

    private String loggerNamePrefix;

    private JsonLibrary jsonLibrary;

    private Path downloadPath;

    private ProxySelector proxySelector;

    private ChromiumOptions() {
        // no op
    }

    public static Builder builder() {
        return new ChromiumOptions.Builder();
    }

    public static class Builder {

        private ChromiumOptions options = new ChromiumOptions();

        private Builder() {
            // no op
        }

        public Builder loggerType(CdpLoggerType loggerType) {
            options.loggerType = loggerType;
            return this;
        }

        public Builder consoleLoggerLevel(CdpLogggerLevel consoleLoggerLevel) {
            options.consoleLoggerLevel = consoleLoggerLevel;
            return this;
        }

        public Builder loggerNamePrefix(String loggerNamePrefix) {
            options.loggerNamePrefix = loggerNamePrefix;
            return this;
        }

        public Builder jsonLibrary(JsonLibrary jsonLibrary) {
            options.jsonLibrary = jsonLibrary;
            return this;
        }

        public Builder proxySelector(ProxySelector proxySelector) {
            options.proxySelector = proxySelector;
            return this;
        }

        public ChromiumOptions build() {
            if (options.loggerType == null) {
                options.loggerType = Null;
            }
            if (options.consoleLoggerLevel == null) {
                options.consoleLoggerLevel = Info;
            }
            if (options.jsonLibrary == null) {
                options.jsonLibrary = Gson;
            }
            if (options.downloadPath == null) {
                options.downloadPath =
                        get(getProperty("java.io.tmpdir")).toAbsolutePath().resolve("cdp4j-chromium");
            }
            return options;
        }
    }

    public CdpLoggerType loggerType() {
        return loggerType;
    }

    public String loggerNamePrefix() {
        return loggerNamePrefix;
    }

    public CdpLogggerLevel consoleLoggerLevel() {
        return consoleLoggerLevel;
    }

    public JsonLibrary jsonLibrary() {
        return jsonLibrary;
    }

    public Path downloadPath() {
        return downloadPath;
    }

    public ProxySelector proxySelector() {
        return proxySelector;
    }

    @Override
    public String toString() {
        return "ChromiumOptions [loggerType=" + loggerType + ", consoleLoggerLevel=" + consoleLoggerLevel
                + ", loggerNamePrefix=" + loggerNamePrefix + ", jsonLibrary=" + jsonLibrary + ", downloadPath="
                + downloadPath + ", proxySelector=" + proxySelector + "]";
    }
}
