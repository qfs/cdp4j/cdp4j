// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import com.cdp4j.session.Session;
import com.cdp4j.type.domsnapshot.DOMNode;
import com.cdp4j.type.domsnapshot.GetSnapshotResult;
import com.cdp4j.type.domsnapshot.NameValue;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* private            */
/* used only by cdp4j */
public class GetSnapshotParser implements ResponseParser {

    private static final String FIELD_NODE_TYPE = "nodeType";
    private static final String FIELD_VALUE = "value";
    private static final String FIELD_NODE_VALUE = "nodeValue";
    private static final String FIELD_ATTRIBUTES = "attributes";
    private static final String FIELD_CHILD_NODE_INDEXES = "childNodeIndexes";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_NODE_NAME = "nodeName";
    private static final String FIELD_DOM_NODES = "domNodes";

    public static final GetSnapshotParser INSTANCE = new GetSnapshotParser();

    public Object parse(Session session, JsonParser parser) throws IOException {
        GetSnapshotResult result = new GetSnapshotResult();
        List<DOMNode> domNodes = null;
        DOMNode domNode = null;
        List<Integer> childNodeIndexes = null;
        List<NameValue> attributes = null;
        NameValue attributeNameValue = null;
        boolean skipUnusedFields = false;
        JsonToken token = null;
        while ((token = parser.nextToken()) != null) {
            if (skipUnusedFields) {
                parser.skipChildren();
                continue;
            }
            String name = parser.currentName();
            if (domNodes == null && FIELD_DOM_NODES.equals(name) && JsonToken.START_ARRAY == token) {
                domNodes = new ArrayList<>(1024);
                continue;
            }
            if (domNodes != null && FIELD_DOM_NODES.equals(name) && JsonToken.END_ARRAY == token) {
                result.setDomNodes(domNodes);
                skipUnusedFields = true;
            }
            if (domNodes != null && domNode == null && JsonToken.START_OBJECT == token) {
                domNode = new DOMNode();
                continue;
            }
            if (domNodes != null && domNode != null && JsonToken.END_OBJECT == token) {
                if (domNode.getNodeType() != null) {
                    domNodes.add(domNode);
                }
                domNode = null;
            }
            if (domNode != null) {
                String fieldName = parser.currentName();
                if (token == JsonToken.VALUE_NUMBER_INT) {
                    if (FIELD_NODE_TYPE.equals(fieldName)) {
                        domNode.setNodeType(parser.getNumberValue().intValue());
                    } else if (childNodeIndexes != null) {
                        childNodeIndexes.add(parser.getNumberValue().intValue());
                    }
                } else if (token == JsonToken.VALUE_STRING) {
                    if (FIELD_NODE_NAME.equals(fieldName)) {
                        domNode.setNodeName(parser.getValueAsString());
                    } else if (FIELD_NODE_VALUE.equals(fieldName)) {
                        domNode.setNodeValue(parser.getValueAsString());
                    } else if (attributes != null) {
                        if (attributeNameValue == null) {
                            attributeNameValue = new NameValue();
                        }
                        if (FIELD_NAME.equals(fieldName)) {
                            attributeNameValue.setName(parser.getValueAsString());
                        } else if (FIELD_VALUE.equals(fieldName)) {
                            attributeNameValue.setValue(parser.getValueAsString());
                        }
                        if (attributeNameValue.getName() != null && attributeNameValue.getValue() != null) {
                            attributes.add(attributeNameValue);
                            attributeNameValue = null;
                            continue;
                        }
                    }
                } else if (token == JsonToken.START_ARRAY) {
                    if (FIELD_CHILD_NODE_INDEXES.equals(fieldName)) {
                        childNodeIndexes = new ArrayList<>();
                        domNode.setChildNodeIndexes(childNodeIndexes);
                        continue;
                    } else if (FIELD_ATTRIBUTES.equals(fieldName)) {
                        attributes = new ArrayList<>();
                        domNode.setAttributes(attributes);
                        continue;
                    }
                } else if (token == JsonToken.END_ARRAY) {
                    if (FIELD_CHILD_NODE_INDEXES.equals(fieldName)) {
                        if (childNodeIndexes != null) {
                            domNode.setChildNodeIndexes(childNodeIndexes);
                            childNodeIndexes = null;
                            continue;
                        }
                    } else if (FIELD_ATTRIBUTES.equals(fieldName)) {
                        if (attributes != null) {
                            domNode.setAttributes(attributes);
                            attributes = null;
                            continue;
                        }
                    }
                } else if (token == JsonToken.START_OBJECT) {
                    if (attributes != null) {
                        attributeNameValue = new NameValue();
                        continue;
                    }
                } else if (token == JsonToken.END_OBJECT) {
                    if (attributes != null) {
                        attributeNameValue = null;
                        continue;
                    }
                }
            }
        }
        return result;
    }
}
