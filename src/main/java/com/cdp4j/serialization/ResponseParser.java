// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import com.cdp4j.session.Session;
import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;

/* private            */
/* used only by cdp4j */
@FunctionalInterface
public interface ResponseParser {

    Object parse(Session session, JsonParser parser) throws IOException;
}
