// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import com.cdp4j.event.Events;
import com.cdp4j.session.Session;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;

@FunctionalInterface
public interface EventParser {

    Object parse(Events event, Session session, JsonParser parser, ObjectReader reader) throws IOException;
}
