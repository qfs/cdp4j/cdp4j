// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import static java.lang.Enum.valueOf;

import com.cdp4j.logger.CdpLogger;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.DeserializationProblemHandler;
import java.io.IOException;

class JacksonDeserializationProblemHandler extends DeserializationProblemHandler {

    private final CdpLogger log;

    private static final String ENUM_UNKNOWN_CONSTANT = "_UNKNOWN_";

    private static final String CDP_ENUM_PACKAGE_PREFIX = "com.cdp4j.type";

    public JacksonDeserializationProblemHandler(CdpLogger log) {
        this.log = log;
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Object handleWeirdStringValue(
            DeserializationContext ctxt, Class targetType, String valueToConvert, String failureMsg)
            throws IOException {
        if (targetType.isEnum() && targetType.getName().startsWith(CDP_ENUM_PACKAGE_PREFIX)) {
            Enum unkown = valueOf(targetType, ENUM_UNKNOWN_CONSTANT);
            if (log != null) {
                log.warn(
                        "Missing enum constant [{}.{}]. [{}] mapped to [_UNKNOWN_]. {}",
                        targetType.getSimpleName(),
                        valueToConvert,
                        valueToConvert,
                        failureMsg);
            }
            return unkown;
        } else {
            return super.handleWeirdStringValue(ctxt, targetType, valueToConvert, failureMsg);
        }
    }
}
