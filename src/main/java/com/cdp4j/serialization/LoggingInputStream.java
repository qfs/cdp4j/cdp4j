// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import com.cdp4j.logger.CdpLogger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * This is a wrapper around an input stream which logs the UTF-8 text content as soon as the end of the input stream is reached;
 *
 */
public class LoggingInputStream extends InputStream {
    private final InputStream wrappedStream;
    private final CdpLogger log;
    private final ByteArrayOutputStream buffer;
    private final String message;
    private boolean didLog = false;

    public LoggingInputStream(final InputStream wrappedStream, final CdpLogger log, final String message) {
        this.wrappedStream = wrappedStream;
        this.log = log;
        this.buffer = new ByteArrayOutputStream();
        this.message = message;
    }

    @Override
    public int read() throws IOException {
        try {
            final int res = wrappedStream.read();
            if (res < 0) {
                doLog();
            } else {
                buffer.write(res);
            }
            return res;
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    @Override
    public int read(byte[] b) throws IOException {
        try {
            final int res = wrappedStream.read(b);
            if (res < 0) {
                doLog();
            } else {
                buffer.write(b);
            }
            return res;
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        try {
            final int res = wrappedStream.read(b, off, len);
            if (res < 0) {
                doLog();
            } else {
                buffer.write(b, off, res);
            }
            return res;
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    @Override
    public byte[] readNBytes(int len) throws IOException {
        try {
            return wrappedStream.readNBytes(len);
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    @Override
    public int readNBytes(byte[] b, int off, int len) throws IOException {
        try {
            return wrappedStream.readNBytes(b, off, len);
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    @Override
    public long skip(long n) throws IOException {
        try {
            return wrappedStream.skip(n);
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    @Override
    public int available() throws IOException {
        try {
            return wrappedStream.available();
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    @Override
    public void close() throws IOException {
        try {
            wrappedStream.close();
            super.close();
            doLog();
        } catch (final IOException ex) {
            doLog();
            throw ex;
        }
    }

    private void doLog() {
        if (didLog) return;
        String content;
        try {
            content = buffer.toString("UTF-8");
        } catch (UnsupportedEncodingException e) {
            content = buffer.toString();
        }
        if (message != null) {
            log.debug(message, content);
        } else {
            log.debug(content);
        }

        didLog = true;
    }
}
