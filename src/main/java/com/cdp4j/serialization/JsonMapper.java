// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import com.cdp4j.JsonLibrary;
import com.cdp4j.session.Option;
import java.io.InputStream;
import java.util.List;

public interface JsonMapper {

    Object fromJsonResult(String json, Class<?> returnType);

    String toJson(Object argument);

    Object getMapper();

    Object getReader();

    Object getWriter();

    List<Option> jsonToOptions(String json);

    List<List<Object>> jsonToListOfList(String json);

    <T> T fromJson(InputStream is, Class<T> klass);

    JsonLibrary getType();

    <T> T fromObject(Object value, Class<T> returnType);
}
