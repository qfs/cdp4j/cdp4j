// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import com.cdp4j.logger.CdpLogger;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;

// This implementation copied from: com.google.gson.internal.bind.TypeAdapters.ENUM_FACTORY
class GsonEnumTypeAdapterFactory implements TypeAdapterFactory {

    private final CdpLogger log;

    private static final String CDP_ENUM_PACKAGE_PREFIX = "com.cdp4j.type";

    GsonEnumTypeAdapterFactory(CdpLogger log) {
        this.log = log;
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Class<? super T> rawType = typeToken.getRawType();
        if (!Enum.class.isAssignableFrom(rawType)
                || rawType == Enum.class
                || !rawType.getName().startsWith(CDP_ENUM_PACKAGE_PREFIX)) {
            return null;
        }
        if (!rawType.isEnum()) {
            rawType = rawType.getSuperclass();
        }
        return (TypeAdapter<T>) new GsonEnumTypeAdapter(rawType, log);
    }
}
