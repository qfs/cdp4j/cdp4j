// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Base64.getDecoder;
import static java.util.Base64.getEncoder;

import com.cdp4j.JsonLibrary;
import com.cdp4j.exception.CdpException;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.session.Option;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

public class GsonMapper implements JsonMapper {

    private final Gson gson;

    private static TypeToken<List<Option>> LIST_OPTION = new TypeToken<List<Option>>() {};

    private static TypeToken<List<List<Object>>> LIST_LIST_OBJECT = new TypeToken<List<List<Object>>>() {};

    private static final GsonByteArrayTypeAdapter GSON_TYPE_ARRAY_ADAPTER = new GsonByteArrayTypeAdapter();

    private static class GsonByteArrayTypeAdapter implements JsonDeserializer<byte[]>, JsonSerializer<byte[]> {

        @Override
        public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(getEncoder().encodeToString(src));
        }

        @Override
        public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            return getDecoder().decode(json.getAsString());
        }
    }

    public GsonMapper() {
        this(null);
    }

    public GsonMapper(CdpLogger log) {
        GsonBuilder builder = new GsonBuilder()
                .registerTypeHierarchyAdapter(byte[].class, GSON_TYPE_ARRAY_ADAPTER)
                .registerTypeAdapterFactory(new GsonEnumTypeAdapterFactory(log))
                .disableHtmlEscaping();
        this.gson = builder.create();
    }

    @Override
    public List<Option> jsonToOptions(String json) {
        return gson.fromJson(json, LIST_OPTION.getType());
    }

    @Override
    public Object fromJsonResult(String json, Class<?> returnType) {
        JsonObject object = gson.fromJson(json, JsonObject.class);
        JsonElement result = object.get("result");
        Object value = gson.fromJson(result, returnType);
        return value;
    }

    @Override
    public String toJson(Object argument) {
        return gson.toJson(argument);
    }

    @Override
    public List<List<Object>> jsonToListOfList(String json) {
        return gson.fromJson(json, LIST_LIST_OBJECT.getType());
    }

    @Override
    public Object getMapper() {
        return gson;
    }

    @Override
    public Object getReader() {
        throw new CdpException("not supported");
    }

    @Override
    public Object getWriter() {
        throw new CdpException("not supported");
    }

    @Override
    public <T> T fromJson(InputStream is, Class<T> klass) {
        try (InputStreamReader reader = new InputStreamReader(is, UTF_8)) {
            return gson.fromJson(reader, klass);
        } catch (IOException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public JsonLibrary getType() {
        return JsonLibrary.Gson;
    }

    @Override
    public <T> T fromObject(Object value, Class<T> returnType) {
        throw new CdpException("not supported");
    }
}
