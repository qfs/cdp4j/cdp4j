// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

import com.cdp4j.JsonLibrary;
import com.cdp4j.exception.CdpException;
import com.cdp4j.logger.CdpLogger;
import com.cdp4j.session.Option;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class JacksonMapper implements JsonMapper {

    private final ObjectMapper mapper;

    private final ObjectReader reader;

    private final ObjectWriter writer;

    private static TypeReference<List<Option>> LIST_OPTION = new TypeReference<List<Option>>() {};

    private static TypeReference<List<List<Object>>> LIST_LIST_OBJECT = new TypeReference<List<List<Object>>>() {};

    public JacksonMapper() {
        this(null);
    }

    public JacksonMapper(CdpLogger log) {
        mapper = new ObjectMapper();
        mapper.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.setSerializationInclusion(NON_NULL);
        mapper.addHandler(new JacksonDeserializationProblemHandler(log));
        this.reader = mapper.reader();
        this.writer = mapper.writer();
    }

    @Override
    public Object getMapper() {
        return this.mapper;
    }

    @Override
    public List<Option> jsonToOptions(String json) {
        try {
            return mapper.readValue(json, LIST_OPTION);
        } catch (JsonProcessingException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public Object fromJsonResult(String json, Class<?> returnType) {
        try {
            ObjectNode objectNode = mapper.readValue(json, ObjectNode.class);
            JsonNode result = objectNode.get("result");
            return mapper.convertValue(result, returnType);
        } catch (JsonProcessingException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public String toJson(Object argument) {
        try {
            return mapper.writeValueAsString(argument);
        } catch (JsonProcessingException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public List<List<Object>> jsonToListOfList(String json) {
        try {
            return mapper.readValue(json, LIST_LIST_OBJECT);
        } catch (JsonProcessingException e) {
            throw new CdpException(e);
        }
    }

    @Override
    public Object getReader() {
        return reader;
    }

    @Override
    public Object getWriter() {
        return writer;
    }

    @Override
    public <T> T fromJson(InputStream is, Class<T> klass) {
        try {
            return mapper.readValue(is, klass);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public JsonLibrary getType() {
        return JsonLibrary.Jackson;
    }

    @Override
    public <T> T fromObject(Object value, Class<T> returnType) {
        return mapper.convertValue(value, returnType);
    }
}
