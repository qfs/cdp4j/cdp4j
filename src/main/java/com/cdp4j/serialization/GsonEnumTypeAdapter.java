// SPDX-License-Identifier: MIT
package com.cdp4j.serialization;

import com.cdp4j.logger.CdpLogger;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// This implementation copied from: com.google.gson.internal.bind.TypeAdapters.EnumTypeAdapter
class GsonEnumTypeAdapter<T extends Enum<T>> extends TypeAdapter<T> {

    private final CdpLogger log;

    private final Class<T> enumClass;

    private final Map<String, T> nameToConstant;

    private final Map<T, String> constantToName;

    private static final String ENUM_UNKNOWN_CONSTANT = "_UNKNOWN_";

    GsonEnumTypeAdapter(Class<T> enumClass, CdpLogger log) {
        this.log = log;
        this.enumClass = enumClass;
        try {
            T[] constants = enumClass.getEnumConstants();
            nameToConstant = new HashMap<String, T>(constants.length);
            constantToName = new HashMap<T, String>(constants.length);
            for (T constant : constants) {
                String name = constant.name();
                SerializedName annotation = enumClass.getField(name).getAnnotation(SerializedName.class);
                if (annotation != null) {
                    name = annotation.value();
                    for (String alternate : annotation.alternate()) {
                        nameToConstant.put(alternate, constant);
                    }
                }
                nameToConstant.put(name, constant);
                constantToName.put(constant, name);
            }
        } catch (NoSuchFieldException e) {
            throw new AssertionError(e);
        }
    }

    @Override
    public void write(JsonWriter out, T value) throws IOException {
        out.value(value == null ? null : constantToName.get(value));
    }

    @Override
    public T read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }
        String valueStr = in.nextString();
        T value = nameToConstant.get(valueStr);
        if (value == null) {
            if (log != null) {
                log.warn(
                        "Missing enum constant [{}.{}]. [{}] mapped to [_UNKNOWN_].",
                        enumClass.getSimpleName(),
                        valueStr,
                        valueStr);
            }
            value = nameToConstant.get(ENUM_UNKNOWN_CONSTANT);
        }
        return value;
    }
}
