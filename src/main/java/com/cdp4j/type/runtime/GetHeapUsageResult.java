// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

public class GetHeapUsageResult {
    private Double usedSize;

    private Double totalSize;

    private Double embedderHeapUsedSize;

    private Double backingStorageSize;

    /**
     * Used JavaScript heap size in bytes.
     */
    public Double getUsedSize() {
        return usedSize;
    }

    /**
     * Used JavaScript heap size in bytes.
     */
    public void setUsedSize(Double usedSize) {
        this.usedSize = usedSize;
    }

    /**
     * Allocated JavaScript heap size in bytes.
     */
    public Double getTotalSize() {
        return totalSize;
    }

    /**
     * Allocated JavaScript heap size in bytes.
     */
    public void setTotalSize(Double totalSize) {
        this.totalSize = totalSize;
    }

    /**
     * Used size in bytes in the embedder's garbage-collected heap.
     */
    public Double getEmbedderHeapUsedSize() {
        return embedderHeapUsedSize;
    }

    /**
     * Used size in bytes in the embedder's garbage-collected heap.
     */
    public void setEmbedderHeapUsedSize(Double embedderHeapUsedSize) {
        this.embedderHeapUsedSize = embedderHeapUsedSize;
    }

    /**
     * Size in bytes of backing storage for array buffers and external strings.
     */
    public Double getBackingStorageSize() {
        return backingStorageSize;
    }

    /**
     * Size in bytes of backing storage for array buffers and external strings.
     */
    public void setBackingStorageSize(Double backingStorageSize) {
        this.backingStorageSize = backingStorageSize;
    }

    public String toString() {
        return "GetHeapUsageResult [usedSize=" + usedSize + ", totalSize=" + totalSize + ", embedderHeapUsedSize="
                + embedderHeapUsedSize + ", backingStorageSize=" + backingStorageSize + "]";
    }
}
