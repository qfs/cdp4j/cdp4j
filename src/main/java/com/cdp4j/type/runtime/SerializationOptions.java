// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

import com.cdp4j.type.constant.SerializationOptionsType;

/**
 * Represents options for serialization. Overrides generatePreview and
 * returnByValue.
 */
public class SerializationOptions {
    private SerializationOptionsType serialization;

    private Integer maxDepth;

    private Object additionalParameters;

    public SerializationOptionsType getSerialization() {
        return serialization;
    }

    public void setSerialization(SerializationOptionsType serialization) {
        this.serialization = serialization;
    }

    /**
     * Deep serialization depth. Default is full depth. Respected only in deep
     * serialization mode.
     */
    public Integer getMaxDepth() {
        return maxDepth;
    }

    /**
     * Deep serialization depth. Default is full depth. Respected only in deep
     * serialization mode.
     */
    public void setMaxDepth(Integer maxDepth) {
        this.maxDepth = maxDepth;
    }

    /**
     * Embedder-specific parameters. For example if connected to V8 in Chrome these
     * control DOM serialization via maxNodeDepth: integer and includeShadowTree:
     * "none" | "open" | "all". Values can be only of type string or integer.
     */
    public Object getAdditionalParameters() {
        return additionalParameters;
    }

    /**
     * Embedder-specific parameters. For example if connected to V8 in Chrome these
     * control DOM serialization via maxNodeDepth: integer and includeShadowTree:
     * "none" | "open" | "all". Values can be only of type string or integer.
     */
    public void setAdditionalParameters(Object additionalParameters) {
        this.additionalParameters = additionalParameters;
    }

    public String toString() {
        return "SerializationOptions [serialization=" + serialization + ", maxDepth=" + maxDepth
                + ", additionalParameters=" + additionalParameters + "]";
    }
}
