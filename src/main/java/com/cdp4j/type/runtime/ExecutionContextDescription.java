// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

import java.util.Map;

/**
 * Description of an isolated world.
 */
public class ExecutionContextDescription {
    private Integer id;

    private String origin;

    private String name;

    private String uniqueId;

    private Map<String, Object> auxData;

    /**
     * Unique id of the execution context. It can be used to specify in which
     * execution context script evaluation should be performed.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Unique id of the execution context. It can be used to specify in which
     * execution context script evaluation should be performed.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Execution context origin.
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Execution context origin.
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     * Human readable name describing given context.
     */
    public String getName() {
        return name;
    }

    /**
     * Human readable name describing given context.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * A system-unique execution context identifier. Unlike the id, this is unique
     * across multiple processes, so can be reliably used to identify specific
     * context while backend performs a cross-process navigation.
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * A system-unique execution context identifier. Unlike the id, this is unique
     * across multiple processes, so can be reliably used to identify specific
     * context while backend performs a cross-process navigation.
     */
    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Embedder-specific auxiliary data likely matching {isDefault: boolean, type:
     * 'default'|'isolated'|'worker', frameId: string}
     */
    public Map<String, Object> getAuxData() {
        return auxData;
    }

    /**
     * Embedder-specific auxiliary data likely matching {isDefault: boolean, type:
     * 'default'|'isolated'|'worker', frameId: string}
     */
    public void setAuxData(Map<String, Object> auxData) {
        this.auxData = auxData;
    }

    public String toString() {
        return "ExecutionContextDescription [id=" + id + ", origin=" + origin + ", name=" + name + ", uniqueId="
                + uniqueId + ", auxData=" + auxData + "]";
    }
}
