// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

import com.cdp4j.type.constant.DeepSerializedValueType;

/**
 * Represents deep serialized value.
 */
public class DeepSerializedValue {
    private DeepSerializedValueType type;

    private Object value;

    private String objectId;

    private Integer weakLocalObjectReference;

    public DeepSerializedValueType getType() {
        return type;
    }

    public void setType(DeepSerializedValueType type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Set if value reference met more then once during serialization. In such case,
     * value is provided only to one of the serialized values. Unique per value in
     * the scope of one CDP call.
     */
    public Integer getWeakLocalObjectReference() {
        return weakLocalObjectReference;
    }

    /**
     * Set if value reference met more then once during serialization. In such case,
     * value is provided only to one of the serialized values. Unique per value in
     * the scope of one CDP call.
     */
    public void setWeakLocalObjectReference(Integer weakLocalObjectReference) {
        this.weakLocalObjectReference = weakLocalObjectReference;
    }

    public String toString() {
        return "DeepSerializedValue [type=" + type + ", value=" + value + ", objectId=" + objectId
                + ", weakLocalObjectReference=" + weakLocalObjectReference + "]";
    }
}
