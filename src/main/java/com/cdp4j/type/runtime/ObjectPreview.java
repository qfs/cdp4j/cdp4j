// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.constant.ObjectSubtypeHint;
import com.cdp4j.type.constant.ObjectType;
import java.util.List;

/**
 * Object containing abbreviated remote object value.
 */
@Experimental
public class ObjectPreview {
    private ObjectType type;

    private ObjectSubtypeHint subtype;

    private String description;

    private Boolean overflow;

    private List<PropertyPreview> properties = emptyList();

    private List<EntryPreview> entries = emptyList();

    /**
     * Object type.
     */
    public ObjectType getType() {
        return type;
    }

    /**
     * Object type.
     */
    public void setType(ObjectType type) {
        this.type = type;
    }

    /**
     * Object subtype hint. Specified for object type values only.
     */
    public ObjectSubtypeHint getSubtype() {
        return subtype;
    }

    /**
     * Object subtype hint. Specified for object type values only.
     */
    public void setSubtype(ObjectSubtypeHint subtype) {
        this.subtype = subtype;
    }

    /**
     * String representation of the object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * String representation of the object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * True iff some of the properties or entries of the original object did not
     * fit.
     */
    public Boolean isOverflow() {
        return overflow;
    }

    /**
     * True iff some of the properties or entries of the original object did not
     * fit.
     */
    public void setOverflow(Boolean overflow) {
        this.overflow = overflow;
    }

    /**
     * List of the properties.
     */
    public List<PropertyPreview> getProperties() {
        return properties;
    }

    /**
     * List of the properties.
     */
    public void setProperties(List<PropertyPreview> properties) {
        this.properties = properties;
    }

    /**
     * List of the entries. Specified for map and set subtype values only.
     */
    public List<EntryPreview> getEntries() {
        return entries;
    }

    /**
     * List of the entries. Specified for map and set subtype values only.
     */
    public void setEntries(List<EntryPreview> entries) {
        this.entries = entries;
    }

    public String toString() {
        return "ObjectPreview [type=" + type + ", subtype=" + subtype + ", description=" + description + ", overflow="
                + overflow + ", properties=" + properties + ", entries=" + entries + "]";
    }
}
