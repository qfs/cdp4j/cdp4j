// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

public class CompileScriptResult {
    private String scriptId;

    private ExceptionDetails exceptionDetails;

    /**
     * Id of the script.
     */
    public String getScriptId() {
        return scriptId;
    }

    /**
     * Id of the script.
     */
    public void setScriptId(String scriptId) {
        this.scriptId = scriptId;
    }

    /**
     * Exception details.
     */
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }

    /**
     * Exception details.
     */
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String toString() {
        return "CompileScriptResult [scriptId=" + scriptId + ", exceptionDetails=" + exceptionDetails + "]";
    }
}
