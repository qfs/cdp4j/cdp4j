// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

public class RunScriptResult {
    private RemoteObject result;

    private ExceptionDetails exceptionDetails;

    /**
     * Run result.
     */
    public RemoteObject getResult() {
        return result;
    }

    /**
     * Run result.
     */
    public void setResult(RemoteObject result) {
        this.result = result;
    }

    /**
     * Exception details.
     */
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }

    /**
     * Exception details.
     */
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String toString() {
        return "RunScriptResult [result=" + result + ", exceptionDetails=" + exceptionDetails + "]";
    }
}
