// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

public class AwaitPromiseResult {
    private RemoteObject result;

    private ExceptionDetails exceptionDetails;

    /**
     * Promise result. Will contain rejected value if promise was rejected.
     */
    public RemoteObject getResult() {
        return result;
    }

    /**
     * Promise result. Will contain rejected value if promise was rejected.
     */
    public void setResult(RemoteObject result) {
        this.result = result;
    }

    /**
     * Exception details if stack strace is available.
     */
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }

    /**
     * Exception details if stack strace is available.
     */
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String toString() {
        return "AwaitPromiseResult [result=" + result + ", exceptionDetails=" + exceptionDetails + "]";
    }
}
