// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

public class CallFunctionOnResult {
    private RemoteObject result;

    private ExceptionDetails exceptionDetails;

    /**
     * Call result.
     */
    public RemoteObject getResult() {
        return result;
    }

    /**
     * Call result.
     */
    public void setResult(RemoteObject result) {
        this.result = result;
    }

    /**
     * Exception details.
     */
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }

    /**
     * Exception details.
     */
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String toString() {
        return "CallFunctionOnResult [result=" + result + ", exceptionDetails=" + exceptionDetails + "]";
    }
}
