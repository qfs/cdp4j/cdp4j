// SPDX-License-Identifier: MIT
package com.cdp4j.type.runtime;

public class EvaluateResult {
    private RemoteObject result;

    private ExceptionDetails exceptionDetails;

    /**
     * Evaluation result.
     */
    public RemoteObject getResult() {
        return result;
    }

    /**
     * Evaluation result.
     */
    public void setResult(RemoteObject result) {
        this.result = result;
    }

    /**
     * Exception details.
     */
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }

    /**
     * Exception details.
     */
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String toString() {
        return "EvaluateResult [result=" + result + ", exceptionDetails=" + exceptionDetails + "]";
    }
}
