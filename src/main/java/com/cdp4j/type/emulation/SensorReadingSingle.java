// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.annotation.Experimental;

@Experimental
public class SensorReadingSingle {
    private Double value;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String toString() {
        return "SensorReadingSingle [value=" + value + "]";
    }
}
