// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.annotation.Experimental;

@Experimental
public class SensorReadingQuaternion {
    private Double x;

    private Double y;

    private Double z;

    private Double w;

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Double getZ() {
        return z;
    }

    public void setZ(Double z) {
        this.z = z;
    }

    public Double getW() {
        return w;
    }

    public void setW(Double w) {
        this.w = w;
    }

    public String toString() {
        return "SensorReadingQuaternion [x=" + x + ", y=" + y + ", z=" + z + ", w=" + w + "]";
    }
}
