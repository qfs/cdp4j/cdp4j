// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.annotation.Experimental;

/**
 * Used to specify User Agent Client Hints to emulate. See
 * https://wicg.github.io/ua-client-hints
 */
@Experimental
public class UserAgentBrandVersion {
    private String brand;

    private String version;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String toString() {
        return "UserAgentBrandVersion [brand=" + brand + ", version=" + version + "]";
    }
}
