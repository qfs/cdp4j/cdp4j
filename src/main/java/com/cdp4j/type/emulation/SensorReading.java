// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.annotation.Experimental;

@Experimental
public class SensorReading {
    private SensorReadingSingle single;

    private SensorReadingXYZ xyz;

    private SensorReadingQuaternion quaternion;

    public SensorReadingSingle getSingle() {
        return single;
    }

    public void setSingle(SensorReadingSingle single) {
        this.single = single;
    }

    public SensorReadingXYZ getXyz() {
        return xyz;
    }

    public void setXyz(SensorReadingXYZ xyz) {
        this.xyz = xyz;
    }

    public SensorReadingQuaternion getQuaternion() {
        return quaternion;
    }

    public void setQuaternion(SensorReadingQuaternion quaternion) {
        this.quaternion = quaternion;
    }

    public String toString() {
        return "SensorReading [single=" + single + ", xyz=" + xyz + ", quaternion=" + quaternion + "]";
    }
}
