// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

/**
 * Used to specify User Agent Client Hints to emulate. See
 * https://wicg.github.io/ua-client-hints Missing optional values will be filled
 * in by the target with what it would normally use.
 */
@Experimental
public class UserAgentMetadata {
    private List<UserAgentBrandVersion> brands = emptyList();

    private List<UserAgentBrandVersion> fullVersionList = emptyList();

    private String fullVersion;

    private String platform;

    private String platformVersion;

    private String architecture;

    private String model;

    private Boolean mobile;

    private String bitness;

    private Boolean wow64;

    /**
     * Brands appearing in Sec-CH-UA.
     */
    public List<UserAgentBrandVersion> getBrands() {
        return brands;
    }

    /**
     * Brands appearing in Sec-CH-UA.
     */
    public void setBrands(List<UserAgentBrandVersion> brands) {
        this.brands = brands;
    }

    /**
     * Brands appearing in Sec-CH-UA-Full-Version-List.
     */
    public List<UserAgentBrandVersion> getFullVersionList() {
        return fullVersionList;
    }

    /**
     * Brands appearing in Sec-CH-UA-Full-Version-List.
     */
    public void setFullVersionList(List<UserAgentBrandVersion> fullVersionList) {
        this.fullVersionList = fullVersionList;
    }

    @Deprecated
    public String getFullVersion() {
        return fullVersion;
    }

    @Deprecated
    public void setFullVersion(String fullVersion) {
        this.fullVersion = fullVersion;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public String getArchitecture() {
        return architecture;
    }

    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Boolean isMobile() {
        return mobile;
    }

    public void setMobile(Boolean mobile) {
        this.mobile = mobile;
    }

    public String getBitness() {
        return bitness;
    }

    public void setBitness(String bitness) {
        this.bitness = bitness;
    }

    public Boolean isWow64() {
        return wow64;
    }

    public void setWow64(Boolean wow64) {
        this.wow64 = wow64;
    }

    public String toString() {
        return "UserAgentMetadata [brands=" + brands + ", fullVersionList=" + fullVersionList + ", fullVersion="
                + fullVersion + ", platform=" + platform + ", platformVersion=" + platformVersion + ", architecture="
                + architecture + ", model=" + model + ", mobile=" + mobile + ", bitness=" + bitness + ", wow64=" + wow64
                + "]";
    }
}
