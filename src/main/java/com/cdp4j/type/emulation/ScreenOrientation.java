// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.type.constant.PortraitType;

/**
 * Screen orientation.
 */
public class ScreenOrientation {
    private PortraitType type;

    private Integer angle;

    /**
     * Orientation type.
     */
    public PortraitType getType() {
        return type;
    }

    /**
     * Orientation type.
     */
    public void setType(PortraitType type) {
        this.type = type;
    }

    /**
     * Orientation angle.
     */
    public Integer getAngle() {
        return angle;
    }

    /**
     * Orientation angle.
     */
    public void setAngle(Integer angle) {
        this.angle = angle;
    }

    public String toString() {
        return "ScreenOrientation [type=" + type + ", angle=" + angle + "]";
    }
}
