// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Used to specify sensor types to emulate.
 * See https://w3c.github.io/sensors/#automation for more information.
 */
public enum SensorType {
    @SerializedName("absolute-orientation")
    @JsonProperty("absolute-orientation")
    AbsoluteOrientation("absolute-orientation"),

    @SerializedName("accelerometer")
    @JsonProperty("accelerometer")
    Accelerometer("accelerometer"),

    @SerializedName("ambient-light")
    @JsonProperty("ambient-light")
    AmbientLight("ambient-light"),

    @SerializedName("gravity")
    @JsonProperty("gravity")
    Gravity("gravity"),

    @SerializedName("gyroscope")
    @JsonProperty("gyroscope")
    Gyroscope("gyroscope"),

    @SerializedName("linear-acceleration")
    @JsonProperty("linear-acceleration")
    LinearAcceleration("linear-acceleration"),

    @SerializedName("magnetometer")
    @JsonProperty("magnetometer")
    Magnetometer("magnetometer"),

    @SerializedName("relative-orientation")
    @JsonProperty("relative-orientation")
    RelativeOrientation("relative-orientation"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SensorType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
