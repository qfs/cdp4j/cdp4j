// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

public class MediaFeature {
    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return "MediaFeature [name=" + name + ", value=" + value + "]";
    }
}
