// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PressureState {
    @SerializedName("nominal")
    @JsonProperty("nominal")
    Nominal("nominal"),

    @SerializedName("fair")
    @JsonProperty("fair")
    Fair("fair"),

    @SerializedName("serious")
    @JsonProperty("serious")
    Serious("serious"),

    @SerializedName("critical")
    @JsonProperty("critical")
    Critical("critical"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PressureState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
