// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PressureSource {
    @SerializedName("cpu")
    @JsonProperty("cpu")
    Cpu("cpu"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PressureSource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
