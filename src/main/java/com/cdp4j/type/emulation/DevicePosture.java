// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.type.constant.DevicePostureType;

public class DevicePosture {
    private DevicePostureType type;

    /**
     * Current posture of the device
     */
    public DevicePostureType getType() {
        return type;
    }

    /**
     * Current posture of the device
     */
    public void setType(DevicePostureType type) {
        this.type = type;
    }

    public String toString() {
        return "DevicePosture [type=" + type + "]";
    }
}
