// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.annotation.Experimental;

@Experimental
public class SensorMetadata {
    private Boolean available;

    private Double minimumFrequency;

    private Double maximumFrequency;

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Double getMinimumFrequency() {
        return minimumFrequency;
    }

    public void setMinimumFrequency(Double minimumFrequency) {
        this.minimumFrequency = minimumFrequency;
    }

    public Double getMaximumFrequency() {
        return maximumFrequency;
    }

    public void setMaximumFrequency(Double maximumFrequency) {
        this.maximumFrequency = maximumFrequency;
    }

    public String toString() {
        return "SensorMetadata [available=" + available + ", minimumFrequency=" + minimumFrequency
                + ", maximumFrequency=" + maximumFrequency + "]";
    }
}
