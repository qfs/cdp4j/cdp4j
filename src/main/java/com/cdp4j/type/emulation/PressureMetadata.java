// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.cdp4j.annotation.Experimental;

@Experimental
public class PressureMetadata {
    private Boolean available;

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String toString() {
        return "PressureMetadata [available=" + available + "]";
    }
}
