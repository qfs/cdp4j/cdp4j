// SPDX-License-Identifier: MIT
package com.cdp4j.type.emulation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of image types that can be disabled.
 */
public enum DisabledImageType {
    @SerializedName("avif")
    @JsonProperty("avif")
    Avif("avif"),

    @SerializedName("webp")
    @JsonProperty("webp")
    Webp("webp"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    DisabledImageType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
