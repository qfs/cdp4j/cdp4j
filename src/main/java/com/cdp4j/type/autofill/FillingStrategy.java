// SPDX-License-Identifier: MIT
package com.cdp4j.type.autofill;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Specified whether a filled field was done so by using the html autocomplete attribute or autofill heuristics.
 */
public enum FillingStrategy {
    @SerializedName("autocompleteAttribute")
    @JsonProperty("autocompleteAttribute")
    AutocompleteAttribute("autocompleteAttribute"),

    @SerializedName("autofillInferred")
    @JsonProperty("autofillInferred")
    AutofillInferred("autofillInferred"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    FillingStrategy(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
