// SPDX-License-Identifier: MIT
package com.cdp4j.type.autofill;

public class CreditCard {
    private String number;

    private String name;

    private String expiryMonth;

    private String expiryYear;

    private String cvc;

    /**
     * 16-digit credit card number.
     */
    public String getNumber() {
        return number;
    }

    /**
     * 16-digit credit card number.
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * Name of the credit card owner.
     */
    public String getName() {
        return name;
    }

    /**
     * Name of the credit card owner.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 2-digit expiry month.
     */
    public String getExpiryMonth() {
        return expiryMonth;
    }

    /**
     * 2-digit expiry month.
     */
    public void setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
    }

    /**
     * 4-digit expiry year.
     */
    public String getExpiryYear() {
        return expiryYear;
    }

    /**
     * 4-digit expiry year.
     */
    public void setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
    }

    /**
     * 3-digit card verification code.
     */
    public String getCvc() {
        return cvc;
    }

    /**
     * 3-digit card verification code.
     */
    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String toString() {
        return "CreditCard [number=" + number + ", name=" + name + ", expiryMonth=" + expiryMonth + ", expiryYear="
                + expiryYear + ", cvc=" + cvc + "]";
    }
}
