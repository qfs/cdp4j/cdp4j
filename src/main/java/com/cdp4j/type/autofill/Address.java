// SPDX-License-Identifier: MIT
package com.cdp4j.type.autofill;

import static java.util.Collections.emptyList;

import java.util.List;

public class Address {
    private List<AddressField> fields = emptyList();

    /**
     * fields and values defining an address.
     */
    public List<AddressField> getFields() {
        return fields;
    }

    /**
     * fields and values defining an address.
     */
    public void setFields(List<AddressField> fields) {
        this.fields = fields;
    }

    public String toString() {
        return "Address [fields=" + fields + "]";
    }
}
