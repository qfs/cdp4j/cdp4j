// SPDX-License-Identifier: MIT
package com.cdp4j.type.autofill;

public class AddressField {
    private String name;

    private String value;

    /**
     * address field name, for example GIVEN_NAME.
     */
    public String getName() {
        return name;
    }

    /**
     * address field name, for example GIVEN_NAME.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * address field value, for example Jon Doe.
     */
    public String getValue() {
        return value;
    }

    /**
     * address field value, for example Jon Doe.
     */
    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return "AddressField [name=" + name + ", value=" + value + "]";
    }
}
