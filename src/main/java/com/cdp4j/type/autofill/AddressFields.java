// SPDX-License-Identifier: MIT
package com.cdp4j.type.autofill;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * A list of address fields.
 */
public class AddressFields {
    private List<AddressField> fields = emptyList();

    public List<AddressField> getFields() {
        return fields;
    }

    public void setFields(List<AddressField> fields) {
        this.fields = fields;
    }

    public String toString() {
        return "AddressFields [fields=" + fields + "]";
    }
}
