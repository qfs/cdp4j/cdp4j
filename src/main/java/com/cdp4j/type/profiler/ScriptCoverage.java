// SPDX-License-Identifier: MIT
package com.cdp4j.type.profiler;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Coverage data for a JavaScript script.
 */
public class ScriptCoverage {
    private String scriptId;

    private String url;

    private List<FunctionCoverage> functions = emptyList();

    /**
     * JavaScript script id.
     */
    public String getScriptId() {
        return scriptId;
    }

    /**
     * JavaScript script id.
     */
    public void setScriptId(String scriptId) {
        this.scriptId = scriptId;
    }

    /**
     * JavaScript script name or url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * JavaScript script name or url.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Functions contained in the script that has coverage data.
     */
    public List<FunctionCoverage> getFunctions() {
        return functions;
    }

    /**
     * Functions contained in the script that has coverage data.
     */
    public void setFunctions(List<FunctionCoverage> functions) {
        this.functions = functions;
    }

    public String toString() {
        return "ScriptCoverage [scriptId=" + scriptId + ", url=" + url + ", functions=" + functions + "]";
    }
}
