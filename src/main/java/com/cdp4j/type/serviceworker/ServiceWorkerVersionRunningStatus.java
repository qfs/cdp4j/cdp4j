// SPDX-License-Identifier: MIT
package com.cdp4j.type.serviceworker;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ServiceWorkerVersionRunningStatus {
    @SerializedName("stopped")
    @JsonProperty("stopped")
    Stopped("stopped"),

    @SerializedName("starting")
    @JsonProperty("starting")
    Starting("starting"),

    @SerializedName("running")
    @JsonProperty("running")
    Running("running"),

    @SerializedName("stopping")
    @JsonProperty("stopping")
    Stopping("stopping"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ServiceWorkerVersionRunningStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
