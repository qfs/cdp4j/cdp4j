// SPDX-License-Identifier: MIT
package com.cdp4j.type.serviceworker;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ServiceWorkerVersionStatus {
    @SerializedName("new")
    @JsonProperty("new")
    New("new"),

    @SerializedName("installing")
    @JsonProperty("installing")
    Installing("installing"),

    @SerializedName("installed")
    @JsonProperty("installed")
    Installed("installed"),

    @SerializedName("activating")
    @JsonProperty("activating")
    Activating("activating"),

    @SerializedName("activated")
    @JsonProperty("activated")
    Activated("activated"),

    @SerializedName("redundant")
    @JsonProperty("redundant")
    Redundant("redundant"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ServiceWorkerVersionStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
