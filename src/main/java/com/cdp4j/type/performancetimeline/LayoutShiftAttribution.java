// SPDX-License-Identifier: MIT
package com.cdp4j.type.performancetimeline;

import com.cdp4j.type.dom.Rect;

public class LayoutShiftAttribution {
    private Rect previousRect;

    private Rect currentRect;

    private Integer nodeId;

    public Rect getPreviousRect() {
        return previousRect;
    }

    public void setPreviousRect(Rect previousRect) {
        this.previousRect = previousRect;
    }

    public Rect getCurrentRect() {
        return currentRect;
    }

    public void setCurrentRect(Rect currentRect) {
        this.currentRect = currentRect;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public String toString() {
        return "LayoutShiftAttribution [previousRect=" + previousRect + ", currentRect=" + currentRect + ", nodeId="
                + nodeId + "]";
    }
}
