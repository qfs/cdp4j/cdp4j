// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import com.cdp4j.annotation.Experimental;

/**
 * CSS Supports at-rule descriptor.
 */
@Experimental
public class CSSSupports {
    private String text;

    private Boolean active;

    private SourceRange range;

    private String styleSheetId;

    /**
     * Supports rule text.
     */
    public String getText() {
        return text;
    }

    /**
     * Supports rule text.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Whether the supports condition is satisfied.
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * Whether the supports condition is satisfied.
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * The associated rule header range in the enclosing stylesheet (if available).
     */
    public SourceRange getRange() {
        return range;
    }

    /**
     * The associated rule header range in the enclosing stylesheet (if available).
     */
    public void setRange(SourceRange range) {
        this.range = range;
    }

    /**
     * Identifier of the stylesheet containing this object (if exists).
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * Identifier of the stylesheet containing this object (if exists).
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    public String toString() {
        return "CSSSupports [text=" + text + ", active=" + active + ", range=" + range + ", styleSheetId="
                + styleSheetId + "]";
    }
}
