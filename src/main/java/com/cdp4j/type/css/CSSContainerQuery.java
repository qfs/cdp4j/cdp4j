// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.dom.LogicalAxes;
import com.cdp4j.type.dom.PhysicalAxes;

/**
 * CSS container query rule descriptor.
 */
@Experimental
public class CSSContainerQuery {
    private String text;

    private SourceRange range;

    private String styleSheetId;

    private String name;

    private PhysicalAxes physicalAxes;

    private LogicalAxes logicalAxes;

    private Boolean queriesScrollState;

    /**
     * Container query text.
     */
    public String getText() {
        return text;
    }

    /**
     * Container query text.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * The associated rule header range in the enclosing stylesheet (if available).
     */
    public SourceRange getRange() {
        return range;
    }

    /**
     * The associated rule header range in the enclosing stylesheet (if available).
     */
    public void setRange(SourceRange range) {
        this.range = range;
    }

    /**
     * Identifier of the stylesheet containing this object (if exists).
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * Identifier of the stylesheet containing this object (if exists).
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    /**
     * Optional name for the container.
     */
    public String getName() {
        return name;
    }

    /**
     * Optional name for the container.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Optional physical axes queried for the container.
     */
    public PhysicalAxes getPhysicalAxes() {
        return physicalAxes;
    }

    /**
     * Optional physical axes queried for the container.
     */
    public void setPhysicalAxes(PhysicalAxes physicalAxes) {
        this.physicalAxes = physicalAxes;
    }

    /**
     * Optional logical axes queried for the container.
     */
    public LogicalAxes getLogicalAxes() {
        return logicalAxes;
    }

    /**
     * Optional logical axes queried for the container.
     */
    public void setLogicalAxes(LogicalAxes logicalAxes) {
        this.logicalAxes = logicalAxes;
    }

    /**
     * true if the query contains scroll-state() queries.
     */
    public Boolean isQueriesScrollState() {
        return queriesScrollState;
    }

    /**
     * true if the query contains scroll-state() queries.
     */
    public void setQueriesScrollState(Boolean queriesScrollState) {
        this.queriesScrollState = queriesScrollState;
    }

    public String toString() {
        return "CSSContainerQuery [text=" + text + ", range=" + range + ", styleSheetId=" + styleSheetId + ", name="
                + name + ", physicalAxes=" + physicalAxes + ", logicalAxes=" + logicalAxes + ", queriesScrollState="
                + queriesScrollState + "]";
    }
}
