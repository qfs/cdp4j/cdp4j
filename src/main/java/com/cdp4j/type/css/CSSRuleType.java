// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum indicating the type of a CSS rule, used to represent the order of a style rule's ancestors.
 * This list only contains rule types that are collected during the ancestor rule collection.
 */
public enum CSSRuleType {
    @SerializedName("MediaRule")
    @JsonProperty("MediaRule")
    MediaRule("MediaRule"),

    @SerializedName("SupportsRule")
    @JsonProperty("SupportsRule")
    SupportsRule("SupportsRule"),

    @SerializedName("ContainerRule")
    @JsonProperty("ContainerRule")
    ContainerRule("ContainerRule"),

    @SerializedName("LayerRule")
    @JsonProperty("LayerRule")
    LayerRule("LayerRule"),

    @SerializedName("ScopeRule")
    @JsonProperty("ScopeRule")
    ScopeRule("ScopeRule"),

    @SerializedName("StyleRule")
    @JsonProperty("StyleRule")
    StyleRule("StyleRule"),

    @SerializedName("StartingStyleRule")
    @JsonProperty("StartingStyleRule")
    StartingStyleRule("StartingStyleRule"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CSSRuleType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
