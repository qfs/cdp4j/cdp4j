// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

/**
 * CSS style coming from animations with the name of the animation.
 */
public class CSSAnimationStyle {
    private String name;

    private CSSStyle style;

    /**
     * The name of the animation.
     */
    public String getName() {
        return name;
    }

    /**
     * The name of the animation.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The style coming from the animation.
     */
    public CSSStyle getStyle() {
        return style;
    }

    /**
     * The style coming from the animation.
     */
    public void setStyle(CSSStyle style) {
        this.style = style;
    }

    public String toString() {
        return "CSSAnimationStyle [name=" + name + ", style=" + style + "]";
    }
}
