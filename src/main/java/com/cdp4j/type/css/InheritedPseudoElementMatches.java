// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Inherited pseudo element matches from pseudos of an ancestor node.
 */
public class InheritedPseudoElementMatches {
    private List<PseudoElementMatches> pseudoElements = emptyList();

    /**
     * Matches of pseudo styles from the pseudos of an ancestor node.
     */
    public List<PseudoElementMatches> getPseudoElements() {
        return pseudoElements;
    }

    /**
     * Matches of pseudo styles from the pseudos of an ancestor node.
     */
    public void setPseudoElements(List<PseudoElementMatches> pseudoElements) {
        this.pseudoElements = pseudoElements;
    }

    public String toString() {
        return "InheritedPseudoElementMatches [pseudoElements=" + pseudoElements + "]";
    }
}
