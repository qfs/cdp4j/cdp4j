// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import java.util.List;

public class GetMatchedStylesForNodeResult {
    private CSSStyle inlineStyle;

    private CSSStyle attributesStyle;

    private List<RuleMatch> matchedCSSRules;

    private List<PseudoElementMatches> pseudoElements;

    private List<InheritedStyleEntry> inherited;

    private List<InheritedPseudoElementMatches> inheritedPseudoElements;

    private List<CSSKeyframesRule> cssKeyframesRules;

    private List<CSSPositionTryRule> cssPositionTryRules;

    private Integer activePositionFallbackIndex;

    private List<CSSPropertyRule> cssPropertyRules;

    private List<CSSPropertyRegistration> cssPropertyRegistrations;

    private CSSFontPaletteValuesRule cssFontPaletteValuesRule;

    private Integer parentLayoutNodeId;

    /**
     * Inline style for the specified DOM node.
     */
    public CSSStyle getInlineStyle() {
        return inlineStyle;
    }

    /**
     * Inline style for the specified DOM node.
     */
    public void setInlineStyle(CSSStyle inlineStyle) {
        this.inlineStyle = inlineStyle;
    }

    /**
     * Attribute-defined element style (e.g. resulting from "width=20 height=100%").
     */
    public CSSStyle getAttributesStyle() {
        return attributesStyle;
    }

    /**
     * Attribute-defined element style (e.g. resulting from "width=20 height=100%").
     */
    public void setAttributesStyle(CSSStyle attributesStyle) {
        this.attributesStyle = attributesStyle;
    }

    /**
     * CSS rules matching this node, from all applicable stylesheets.
     */
    public List<RuleMatch> getMatchedCSSRules() {
        return matchedCSSRules;
    }

    /**
     * CSS rules matching this node, from all applicable stylesheets.
     */
    public void setMatchedCSSRules(List<RuleMatch> matchedCSSRules) {
        this.matchedCSSRules = matchedCSSRules;
    }

    /**
     * Pseudo style matches for this node.
     */
    public List<PseudoElementMatches> getPseudoElements() {
        return pseudoElements;
    }

    /**
     * Pseudo style matches for this node.
     */
    public void setPseudoElements(List<PseudoElementMatches> pseudoElements) {
        this.pseudoElements = pseudoElements;
    }

    /**
     * A chain of inherited styles (from the immediate node parent up to the DOM
     * tree root).
     */
    public List<InheritedStyleEntry> getInherited() {
        return inherited;
    }

    /**
     * A chain of inherited styles (from the immediate node parent up to the DOM
     * tree root).
     */
    public void setInherited(List<InheritedStyleEntry> inherited) {
        this.inherited = inherited;
    }

    /**
     * A chain of inherited pseudo element styles (from the immediate node parent up
     * to the DOM tree root).
     */
    public List<InheritedPseudoElementMatches> getInheritedPseudoElements() {
        return inheritedPseudoElements;
    }

    /**
     * A chain of inherited pseudo element styles (from the immediate node parent up
     * to the DOM tree root).
     */
    public void setInheritedPseudoElements(List<InheritedPseudoElementMatches> inheritedPseudoElements) {
        this.inheritedPseudoElements = inheritedPseudoElements;
    }

    /**
     * A list of CSS keyframed animations matching this node.
     */
    public List<CSSKeyframesRule> getCssKeyframesRules() {
        return cssKeyframesRules;
    }

    /**
     * A list of CSS keyframed animations matching this node.
     */
    public void setCssKeyframesRules(List<CSSKeyframesRule> cssKeyframesRules) {
        this.cssKeyframesRules = cssKeyframesRules;
    }

    /**
     * A list of CSS @position-try rules matching this node, based on the
     * position-try-fallbacks property.
     */
    public List<CSSPositionTryRule> getCssPositionTryRules() {
        return cssPositionTryRules;
    }

    /**
     * A list of CSS @position-try rules matching this node, based on the
     * position-try-fallbacks property.
     */
    public void setCssPositionTryRules(List<CSSPositionTryRule> cssPositionTryRules) {
        this.cssPositionTryRules = cssPositionTryRules;
    }

    /**
     * Index of the active fallback in the applied position-try-fallback property,
     * will not be set if there is no active position-try fallback.
     */
    public Integer getActivePositionFallbackIndex() {
        return activePositionFallbackIndex;
    }

    /**
     * Index of the active fallback in the applied position-try-fallback property,
     * will not be set if there is no active position-try fallback.
     */
    public void setActivePositionFallbackIndex(Integer activePositionFallbackIndex) {
        this.activePositionFallbackIndex = activePositionFallbackIndex;
    }

    /**
     * A list of CSS at-property rules matching this node.
     */
    public List<CSSPropertyRule> getCssPropertyRules() {
        return cssPropertyRules;
    }

    /**
     * A list of CSS at-property rules matching this node.
     */
    public void setCssPropertyRules(List<CSSPropertyRule> cssPropertyRules) {
        this.cssPropertyRules = cssPropertyRules;
    }

    /**
     * A list of CSS property registrations matching this node.
     */
    public List<CSSPropertyRegistration> getCssPropertyRegistrations() {
        return cssPropertyRegistrations;
    }

    /**
     * A list of CSS property registrations matching this node.
     */
    public void setCssPropertyRegistrations(List<CSSPropertyRegistration> cssPropertyRegistrations) {
        this.cssPropertyRegistrations = cssPropertyRegistrations;
    }

    /**
     * A font-palette-values rule matching this node.
     */
    public CSSFontPaletteValuesRule getCssFontPaletteValuesRule() {
        return cssFontPaletteValuesRule;
    }

    /**
     * A font-palette-values rule matching this node.
     */
    public void setCssFontPaletteValuesRule(CSSFontPaletteValuesRule cssFontPaletteValuesRule) {
        this.cssFontPaletteValuesRule = cssFontPaletteValuesRule;
    }

    /**
     * Id of the first parent element that does not have display: contents.
     */
    public Integer getParentLayoutNodeId() {
        return parentLayoutNodeId;
    }

    /**
     * Id of the first parent element that does not have display: contents.
     */
    public void setParentLayoutNodeId(Integer parentLayoutNodeId) {
        this.parentLayoutNodeId = parentLayoutNodeId;
    }

    public String toString() {
        return "GetMatchedStylesForNodeResult [inlineStyle=" + inlineStyle + ", attributesStyle=" + attributesStyle
                + ", matchedCSSRules=" + matchedCSSRules + ", pseudoElements=" + pseudoElements + ", inherited="
                + inherited + ", inheritedPseudoElements=" + inheritedPseudoElements + ", cssKeyframesRules="
                + cssKeyframesRules + ", cssPositionTryRules=" + cssPositionTryRules + ", activePositionFallbackIndex="
                + activePositionFallbackIndex + ", cssPropertyRules=" + cssPropertyRules + ", cssPropertyRegistrations="
                + cssPropertyRegistrations + ", cssFontPaletteValuesRule=" + cssFontPaletteValuesRule
                + ", parentLayoutNodeId=" + parentLayoutNodeId + "]";
    }
}
