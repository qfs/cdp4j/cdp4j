// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

/**
 * CSS @position-try rule representation.
 */
public class CSSPositionTryRule {
    private Value name;

    private String styleSheetId;

    private StyleSheetOrigin origin;

    private CSSStyle style;

    private Boolean active;

    /**
     * The prelude dashed-ident name
     */
    public Value getName() {
        return name;
    }

    /**
     * The prelude dashed-ident name
     */
    public void setName(Value name) {
        this.name = name;
    }

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    /**
     * Parent stylesheet's origin.
     */
    public StyleSheetOrigin getOrigin() {
        return origin;
    }

    /**
     * Parent stylesheet's origin.
     */
    public void setOrigin(StyleSheetOrigin origin) {
        this.origin = origin;
    }

    /**
     * Associated style declaration.
     */
    public CSSStyle getStyle() {
        return style;
    }

    /**
     * Associated style declaration.
     */
    public void setStyle(CSSStyle style) {
        this.style = style;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String toString() {
        return "CSSPositionTryRule [name=" + name + ", styleSheetId=" + styleSheetId + ", origin=" + origin + ", style="
                + style + ", active=" + active + "]";
    }
}
