// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

public class GetBackgroundColorsResult {
    private String computedFontSize;

    private String computedFontWeight;

    /**
     * The computed font size for this node, as a CSS computed value string (e.g.
     * '12px').
     */
    public String getComputedFontSize() {
        return computedFontSize;
    }

    /**
     * The computed font size for this node, as a CSS computed value string (e.g.
     * '12px').
     */
    public void setComputedFontSize(String computedFontSize) {
        this.computedFontSize = computedFontSize;
    }

    /**
     * The computed font weight for this node, as a CSS computed value string (e.g.
     * 'normal' or '100').
     */
    public String getComputedFontWeight() {
        return computedFontWeight;
    }

    /**
     * The computed font weight for this node, as a CSS computed value string (e.g.
     * 'normal' or '100').
     */
    public void setComputedFontWeight(String computedFontWeight) {
        this.computedFontWeight = computedFontWeight;
    }

    public String toString() {
        return "GetBackgroundColorsResult [computedFontSize=" + computedFontSize + ", computedFontWeight="
                + computedFontWeight + "]";
    }
}
