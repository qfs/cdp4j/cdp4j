// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

/**
 * Representation of a custom property registration through CSS.registerProperty
 */
public class CSSPropertyRegistration {
    private String propertyName;

    private Value initialValue;

    private Boolean inherits;

    private String syntax;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public Value getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Value initialValue) {
        this.initialValue = initialValue;
    }

    public Boolean isInherits() {
        return inherits;
    }

    public void setInherits(Boolean inherits) {
        this.inherits = inherits;
    }

    public String getSyntax() {
        return syntax;
    }

    public void setSyntax(String syntax) {
        this.syntax = syntax;
    }

    public String toString() {
        return "CSSPropertyRegistration [propertyName=" + propertyName + ", initialValue=" + initialValue
                + ", inherits=" + inherits + ", syntax=" + syntax + "]";
    }
}
