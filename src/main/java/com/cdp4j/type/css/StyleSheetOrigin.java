// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Stylesheet type: "injected" for stylesheets injected via extension, "user-agent" for user-agent
 * stylesheets, "inspector" for stylesheets created by the inspector (i.e. those holding the "via
 * inspector" rules), "regular" for regular stylesheets.
 */
public enum StyleSheetOrigin {
    @SerializedName("injected")
    @JsonProperty("injected")
    Injected("injected"),

    @SerializedName("user-agent")
    @JsonProperty("user-agent")
    UserAgent("user-agent"),

    @SerializedName("inspector")
    @JsonProperty("inspector")
    Inspector("inspector"),

    @SerializedName("regular")
    @JsonProperty("regular")
    Regular("regular"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    StyleSheetOrigin(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
