// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

public class GetInlineStylesForNodeResult {
    private CSSStyle inlineStyle;

    private CSSStyle attributesStyle;

    /**
     * Inline style for the specified DOM node.
     */
    public CSSStyle getInlineStyle() {
        return inlineStyle;
    }

    /**
     * Inline style for the specified DOM node.
     */
    public void setInlineStyle(CSSStyle inlineStyle) {
        this.inlineStyle = inlineStyle;
    }

    /**
     * Attribute-defined element style (e.g. resulting from "width=20 height=100%").
     */
    public CSSStyle getAttributesStyle() {
        return attributesStyle;
    }

    /**
     * Attribute-defined element style (e.g. resulting from "width=20 height=100%").
     */
    public void setAttributesStyle(CSSStyle attributesStyle) {
        this.attributesStyle = attributesStyle;
    }

    public String toString() {
        return "GetInlineStylesForNodeResult [inlineStyle=" + inlineStyle + ", attributesStyle=" + attributesStyle
                + "]";
    }
}
