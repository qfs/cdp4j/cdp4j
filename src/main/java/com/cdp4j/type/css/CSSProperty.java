// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * CSS property declaration data.
 */
public class CSSProperty {
    private String name;

    private String value;

    private Boolean important;

    private Boolean implicit;

    private String text;

    private Boolean parsedOk;

    private Boolean disabled;

    private SourceRange range;

    private List<CSSProperty> longhandProperties = emptyList();

    /**
     * The property name.
     */
    public String getName() {
        return name;
    }

    /**
     * The property name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The property value.
     */
    public String getValue() {
        return value;
    }

    /**
     * The property value.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Whether the property has "!important" annotation (implies false if absent).
     */
    public Boolean isImportant() {
        return important;
    }

    /**
     * Whether the property has "!important" annotation (implies false if absent).
     */
    public void setImportant(Boolean important) {
        this.important = important;
    }

    /**
     * Whether the property is implicit (implies false if absent).
     */
    public Boolean isImplicit() {
        return implicit;
    }

    /**
     * Whether the property is implicit (implies false if absent).
     */
    public void setImplicit(Boolean implicit) {
        this.implicit = implicit;
    }

    /**
     * The full property text as specified in the style.
     */
    public String getText() {
        return text;
    }

    /**
     * The full property text as specified in the style.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Whether the property is understood by the browser (implies true if absent).
     */
    public Boolean isParsedOk() {
        return parsedOk;
    }

    /**
     * Whether the property is understood by the browser (implies true if absent).
     */
    public void setParsedOk(Boolean parsedOk) {
        this.parsedOk = parsedOk;
    }

    /**
     * Whether the property is disabled by the user (present for source-based
     * properties only).
     */
    public Boolean isDisabled() {
        return disabled;
    }

    /**
     * Whether the property is disabled by the user (present for source-based
     * properties only).
     */
    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * The entire property range in the enclosing style declaration (if available).
     */
    public SourceRange getRange() {
        return range;
    }

    /**
     * The entire property range in the enclosing style declaration (if available).
     */
    public void setRange(SourceRange range) {
        this.range = range;
    }

    /**
     * Parsed longhand components of this property if it is a shorthand. This field
     * will be empty if the given property is not a shorthand.
     */
    public List<CSSProperty> getLonghandProperties() {
        return longhandProperties;
    }

    /**
     * Parsed longhand components of this property if it is a shorthand. This field
     * will be empty if the given property is not a shorthand.
     */
    public void setLonghandProperties(List<CSSProperty> longhandProperties) {
        this.longhandProperties = longhandProperties;
    }

    public String toString() {
        return "CSSProperty [name=" + name + ", value=" + value + ", important=" + important + ", implicit=" + implicit
                + ", text=" + text + ", parsedOk=" + parsedOk + ", disabled=" + disabled + ", range=" + range
                + ", longhandProperties=" + longhandProperties + "]";
    }
}
