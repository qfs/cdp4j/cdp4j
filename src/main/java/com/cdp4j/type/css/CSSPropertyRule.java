// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

/**
 * CSS property at-rule representation.
 */
public class CSSPropertyRule {
    private String styleSheetId;

    private StyleSheetOrigin origin;

    private Value propertyName;

    private CSSStyle style;

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    /**
     * Parent stylesheet's origin.
     */
    public StyleSheetOrigin getOrigin() {
        return origin;
    }

    /**
     * Parent stylesheet's origin.
     */
    public void setOrigin(StyleSheetOrigin origin) {
        this.origin = origin;
    }

    /**
     * Associated property name.
     */
    public Value getPropertyName() {
        return propertyName;
    }

    /**
     * Associated property name.
     */
    public void setPropertyName(Value propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * Associated style declaration.
     */
    public CSSStyle getStyle() {
        return style;
    }

    /**
     * Associated style declaration.
     */
    public void setStyle(CSSStyle style) {
        this.style = style;
    }

    public String toString() {
        return "CSSPropertyRule [styleSheetId=" + styleSheetId + ", origin=" + origin + ", propertyName=" + propertyName
                + ", style=" + style + "]";
    }
}
