// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Selector list data.
 */
public class SelectorList {
    private List<Value> selectors = emptyList();

    private String text;

    /**
     * Selectors in the list.
     */
    public List<Value> getSelectors() {
        return selectors;
    }

    /**
     * Selectors in the list.
     */
    public void setSelectors(List<Value> selectors) {
        this.selectors = selectors;
    }

    /**
     * Rule selector text.
     */
    public String getText() {
        return text;
    }

    /**
     * Rule selector text.
     */
    public void setText(String text) {
        this.text = text;
    }

    public String toString() {
        return "SelectorList [selectors=" + selectors + ", text=" + text + "]";
    }
}
