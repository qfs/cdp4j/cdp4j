// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import com.cdp4j.annotation.Experimental;

/**
 * Specificity: https://drafts.csswg.org/selectors/#specificity-rules
 */
@Experimental
public class Specificity {
    private Integer a;

    private Integer b;

    private Integer c;

    /**
     * The a component, which represents the number of ID selectors.
     */
    public Integer getA() {
        return a;
    }

    /**
     * The a component, which represents the number of ID selectors.
     */
    public void setA(Integer a) {
        this.a = a;
    }

    /**
     * The b component, which represents the number of class selectors, attributes
     * selectors, and pseudo-classes.
     */
    public Integer getB() {
        return b;
    }

    /**
     * The b component, which represents the number of class selectors, attributes
     * selectors, and pseudo-classes.
     */
    public void setB(Integer b) {
        this.b = b;
    }

    /**
     * The c component, which represents the number of type selectors and
     * pseudo-elements.
     */
    public Integer getC() {
        return c;
    }

    /**
     * The c component, which represents the number of type selectors and
     * pseudo-elements.
     */
    public void setC(Integer c) {
        this.c = c;
    }

    public String toString() {
        return "Specificity [a=" + a + ", b=" + b + ", c=" + c + "]";
    }
}
