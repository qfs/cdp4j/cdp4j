// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Inherited CSS style collection for animated styles from ancestor node.
 */
public class InheritedAnimatedStyleEntry {
    private List<CSSAnimationStyle> animationStyles = emptyList();

    private CSSStyle transitionsStyle;

    /**
     * Styles coming from the animations of the ancestor, if any, in the style
     * inheritance chain.
     */
    public List<CSSAnimationStyle> getAnimationStyles() {
        return animationStyles;
    }

    /**
     * Styles coming from the animations of the ancestor, if any, in the style
     * inheritance chain.
     */
    public void setAnimationStyles(List<CSSAnimationStyle> animationStyles) {
        this.animationStyles = animationStyles;
    }

    /**
     * The style coming from the transitions of the ancestor, if any, in the style
     * inheritance chain.
     */
    public CSSStyle getTransitionsStyle() {
        return transitionsStyle;
    }

    /**
     * The style coming from the transitions of the ancestor, if any, in the style
     * inheritance chain.
     */
    public void setTransitionsStyle(CSSStyle transitionsStyle) {
        this.transitionsStyle = transitionsStyle;
    }

    public String toString() {
        return "InheritedAnimatedStyleEntry [animationStyles=" + animationStyles + ", transitionsStyle="
                + transitionsStyle + "]";
    }
}
