// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import java.util.List;

public class GetAnimatedStylesForNodeResult {
    private List<CSSAnimationStyle> animationStyles;

    private CSSStyle transitionsStyle;

    private List<InheritedAnimatedStyleEntry> inherited;

    /**
     * Styles coming from animations.
     */
    public List<CSSAnimationStyle> getAnimationStyles() {
        return animationStyles;
    }

    /**
     * Styles coming from animations.
     */
    public void setAnimationStyles(List<CSSAnimationStyle> animationStyles) {
        this.animationStyles = animationStyles;
    }

    /**
     * Style coming from transitions.
     */
    public CSSStyle getTransitionsStyle() {
        return transitionsStyle;
    }

    /**
     * Style coming from transitions.
     */
    public void setTransitionsStyle(CSSStyle transitionsStyle) {
        this.transitionsStyle = transitionsStyle;
    }

    /**
     * Inherited style entries for animationsStyle and transitionsStyle from the
     * inheritance chain of the element.
     */
    public List<InheritedAnimatedStyleEntry> getInherited() {
        return inherited;
    }

    /**
     * Inherited style entries for animationsStyle and transitionsStyle from the
     * inheritance chain of the element.
     */
    public void setInherited(List<InheritedAnimatedStyleEntry> inherited) {
        this.inherited = inherited;
    }

    public String toString() {
        return "GetAnimatedStylesForNodeResult [animationStyles=" + animationStyles + ", transitionsStyle="
                + transitionsStyle + ", inherited=" + inherited + "]";
    }
}
