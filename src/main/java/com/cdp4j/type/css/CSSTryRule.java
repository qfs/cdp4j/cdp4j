// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

/**
 * CSS try rule representation.
 */
public class CSSTryRule {
    private String styleSheetId;

    private StyleSheetOrigin origin;

    private CSSStyle style;

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    /**
     * Parent stylesheet's origin.
     */
    public StyleSheetOrigin getOrigin() {
        return origin;
    }

    /**
     * Parent stylesheet's origin.
     */
    public void setOrigin(StyleSheetOrigin origin) {
        this.origin = origin;
    }

    /**
     * Associated style declaration.
     */
    public CSSStyle getStyle() {
        return style;
    }

    /**
     * Associated style declaration.
     */
    public void setStyle(CSSStyle style) {
        this.style = style;
    }

    public String toString() {
        return "CSSTryRule [styleSheetId=" + styleSheetId + ", origin=" + origin + ", style=" + style + "]";
    }
}
