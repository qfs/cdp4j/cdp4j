// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

/**
 * CSS font-palette-values rule representation.
 */
public class CSSFontPaletteValuesRule {
    private String styleSheetId;

    private StyleSheetOrigin origin;

    private Value fontPaletteName;

    private CSSStyle style;

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * The css style sheet identifier (absent for user agent stylesheet and
     * user-specified stylesheet rules) this rule came from.
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    /**
     * Parent stylesheet's origin.
     */
    public StyleSheetOrigin getOrigin() {
        return origin;
    }

    /**
     * Parent stylesheet's origin.
     */
    public void setOrigin(StyleSheetOrigin origin) {
        this.origin = origin;
    }

    /**
     * Associated font palette name.
     */
    public Value getFontPaletteName() {
        return fontPaletteName;
    }

    /**
     * Associated font palette name.
     */
    public void setFontPaletteName(Value fontPaletteName) {
        this.fontPaletteName = fontPaletteName;
    }

    /**
     * Associated style declaration.
     */
    public CSSStyle getStyle() {
        return style;
    }

    /**
     * Associated style declaration.
     */
    public void setStyle(CSSStyle style) {
        this.style = style;
    }

    public String toString() {
        return "CSSFontPaletteValuesRule [styleSheetId=" + styleSheetId + ", origin=" + origin + ", fontPaletteName="
                + fontPaletteName + ", style=" + style + "]";
    }
}
