// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

/**
 * Information about amount of glyphs that were rendered with given font.
 */
public class PlatformFontUsage {
    private String familyName;

    private String postScriptName;

    private Boolean isCustomFont;

    private Double glyphCount;

    /**
     * Font's family name reported by platform.
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Font's family name reported by platform.
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    /**
     * Font's PostScript name reported by platform.
     */
    public String getPostScriptName() {
        return postScriptName;
    }

    /**
     * Font's PostScript name reported by platform.
     */
    public void setPostScriptName(String postScriptName) {
        this.postScriptName = postScriptName;
    }

    /**
     * Indicates if the font was downloaded or resolved locally.
     */
    public Boolean isIsCustomFont() {
        return isCustomFont;
    }

    /**
     * Indicates if the font was downloaded or resolved locally.
     */
    public void setIsCustomFont(Boolean isCustomFont) {
        this.isCustomFont = isCustomFont;
    }

    /**
     * Amount of glyphs that were rendered with this font.
     */
    public Double getGlyphCount() {
        return glyphCount;
    }

    /**
     * Amount of glyphs that were rendered with this font.
     */
    public void setGlyphCount(Double glyphCount) {
        this.glyphCount = glyphCount;
    }

    public String toString() {
        return "PlatformFontUsage [familyName=" + familyName + ", postScriptName=" + postScriptName + ", isCustomFont="
                + isCustomFont + ", glyphCount=" + glyphCount + "]";
    }
}
