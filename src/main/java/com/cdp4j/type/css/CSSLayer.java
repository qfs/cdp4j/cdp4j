// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import com.cdp4j.annotation.Experimental;

/**
 * CSS Layer at-rule descriptor.
 */
@Experimental
public class CSSLayer {
    private String text;

    private SourceRange range;

    private String styleSheetId;

    /**
     * Layer name.
     */
    public String getText() {
        return text;
    }

    /**
     * Layer name.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * The associated rule header range in the enclosing stylesheet (if available).
     */
    public SourceRange getRange() {
        return range;
    }

    /**
     * The associated rule header range in the enclosing stylesheet (if available).
     */
    public void setRange(SourceRange range) {
        this.range = range;
    }

    /**
     * Identifier of the stylesheet containing this object (if exists).
     */
    public String getStyleSheetId() {
        return styleSheetId;
    }

    /**
     * Identifier of the stylesheet containing this object (if exists).
     */
    public void setStyleSheetId(String styleSheetId) {
        this.styleSheetId = styleSheetId;
    }

    public String toString() {
        return "CSSLayer [text=" + text + ", range=" + range + ", styleSheetId=" + styleSheetId + "]";
    }
}
