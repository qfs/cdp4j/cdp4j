// SPDX-License-Identifier: MIT
package com.cdp4j.type.css;

import java.util.List;

public class TakeCoverageDeltaResult {
    private List<RuleUsage> coverage;

    private Double timestamp;

    public List<RuleUsage> getCoverage() {
        return coverage;
    }

    public void setCoverage(List<RuleUsage> coverage) {
        this.coverage = coverage;
    }

    /**
     * Monotonically increasing time, in seconds.
     */
    public Double getTimestamp() {
        return timestamp;
    }

    /**
     * Monotonically increasing time, in seconds.
     */
    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        return "TakeCoverageDeltaResult [coverage=" + coverage + ", timestamp=" + timestamp + "]";
    }
}
