// SPDX-License-Identifier: MIT
package com.cdp4j.type.cast;

public class Sink {
    private String name;

    private String id;

    private String session;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Text describing the current session. Present only if there is an active
     * session on the sink.
     */
    public String getSession() {
        return session;
    }

    /**
     * Text describing the current session. Present only if there is an active
     * session on the sink.
     */
    public void setSession(String session) {
        this.session = session;
    }

    public String toString() {
        return "Sink [name=" + name + ", id=" + id + ", session=" + session + "]";
    }
}
