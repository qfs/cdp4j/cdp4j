// SPDX-License-Identifier: MIT
package com.cdp4j.type.bluetoothemulation;

/**
 * Stores the manufacturer data
 */
public class ManufacturerData {
    private Integer key;

    private String data;

    /**
     * Company identifier
     * https://bitbucket.org/bluetooth-SIG/public/src/main/assigned_numbers/company_identifiers/company_identifiers.yaml
     * https://usb.org/developers
     */
    public Integer getKey() {
        return key;
    }

    /**
     * Company identifier
     * https://bitbucket.org/bluetooth-SIG/public/src/main/assigned_numbers/company_identifiers/company_identifiers.yaml
     * https://usb.org/developers
     */
    public void setKey(Integer key) {
        this.key = key;
    }

    /**
     * Manufacturer-specific data (Encoded as a base64 string when passed over JSON)
     */
    public String getData() {
        return data;
    }

    /**
     * Manufacturer-specific data (Encoded as a base64 string when passed over JSON)
     */
    public void setData(String data) {
        this.data = data;
    }

    public String toString() {
        return "ManufacturerData [key=" + key + ", data=" + data + "]";
    }
}
