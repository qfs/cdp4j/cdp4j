// SPDX-License-Identifier: MIT
package com.cdp4j.type.bluetoothemulation;

/**
 * Stores the advertisement packet information that is sent by a Bluetooth
 * device.
 */
public class ScanEntry {
    private String deviceAddress;

    private Integer rssi;

    private ScanRecord scanRecord;

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public Integer getRssi() {
        return rssi;
    }

    public void setRssi(Integer rssi) {
        this.rssi = rssi;
    }

    public ScanRecord getScanRecord() {
        return scanRecord;
    }

    public void setScanRecord(ScanRecord scanRecord) {
        this.scanRecord = scanRecord;
    }

    public String toString() {
        return "ScanEntry [deviceAddress=" + deviceAddress + ", rssi=" + rssi + ", scanRecord=" + scanRecord + "]";
    }
}
