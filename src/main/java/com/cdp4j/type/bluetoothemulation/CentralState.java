// SPDX-License-Identifier: MIT
package com.cdp4j.type.bluetoothemulation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Indicates the various states of Central.
 */
public enum CentralState {
    @SerializedName("absent")
    @JsonProperty("absent")
    Absent("absent"),

    @SerializedName("powered-off")
    @JsonProperty("powered-off")
    PoweredOff("powered-off"),

    @SerializedName("powered-on")
    @JsonProperty("powered-on")
    PoweredOn("powered-on"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CentralState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
