// SPDX-License-Identifier: MIT
package com.cdp4j.type.bluetoothemulation;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Stores the byte data of the advertisement packet sent by a Bluetooth device.
 */
public class ScanRecord {
    private String name;

    private List<String> uuids = emptyList();

    private Integer appearance;

    private Integer txPower;

    private List<ManufacturerData> manufacturerData = emptyList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUuids() {
        return uuids;
    }

    public void setUuids(List<String> uuids) {
        this.uuids = uuids;
    }

    /**
     * Stores the external appearance description of the device.
     */
    public Integer getAppearance() {
        return appearance;
    }

    /**
     * Stores the external appearance description of the device.
     */
    public void setAppearance(Integer appearance) {
        this.appearance = appearance;
    }

    /**
     * Stores the transmission power of a broadcasting device.
     */
    public Integer getTxPower() {
        return txPower;
    }

    /**
     * Stores the transmission power of a broadcasting device.
     */
    public void setTxPower(Integer txPower) {
        this.txPower = txPower;
    }

    /**
     * Key is the company identifier and the value is an array of bytes of
     * manufacturer specific data.
     */
    public List<ManufacturerData> getManufacturerData() {
        return manufacturerData;
    }

    /**
     * Key is the company identifier and the value is an array of bytes of
     * manufacturer specific data.
     */
    public void setManufacturerData(List<ManufacturerData> manufacturerData) {
        this.manufacturerData = manufacturerData;
    }

    public String toString() {
        return "ScanRecord [name=" + name + ", uuids=" + uuids + ", appearance=" + appearance + ", txPower=" + txPower
                + ", manufacturerData=" + manufacturerData + "]";
    }
}
