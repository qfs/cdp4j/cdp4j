// SPDX-License-Identifier: MIT
package com.cdp4j.type.cachestorage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * type of HTTP response cached
 */
public enum CachedResponseType {
    @SerializedName("basic")
    @JsonProperty("basic")
    Basic("basic"),

    @SerializedName("cors")
    @JsonProperty("cors")
    Cors("cors"),

    @SerializedName("default")
    @JsonProperty("default")
    Default("default"),

    @SerializedName("error")
    @JsonProperty("error")
    Error("error"),

    @SerializedName("opaqueResponse")
    @JsonProperty("opaqueResponse")
    OpaqueResponse("opaqueResponse"),

    @SerializedName("opaqueRedirect")
    @JsonProperty("opaqueRedirect")
    OpaqueRedirect("opaqueRedirect"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CachedResponseType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
