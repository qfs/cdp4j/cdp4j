// SPDX-License-Identifier: MIT
package com.cdp4j.type.cachestorage;

public class Header {
    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return "Header [name=" + name + ", value=" + value + "]";
    }
}
