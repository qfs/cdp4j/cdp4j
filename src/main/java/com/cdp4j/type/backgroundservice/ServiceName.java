// SPDX-License-Identifier: MIT
package com.cdp4j.type.backgroundservice;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The Background Service that will be associated with the commands/events.
 * Every Background Service operates independently, but they share the same
 * API.
 */
public enum ServiceName {
    @SerializedName("backgroundFetch")
    @JsonProperty("backgroundFetch")
    BackgroundFetch("backgroundFetch"),

    @SerializedName("backgroundSync")
    @JsonProperty("backgroundSync")
    BackgroundSync("backgroundSync"),

    @SerializedName("pushMessaging")
    @JsonProperty("pushMessaging")
    PushMessaging("pushMessaging"),

    @SerializedName("notifications")
    @JsonProperty("notifications")
    Notifications("notifications"),

    @SerializedName("paymentHandler")
    @JsonProperty("paymentHandler")
    PaymentHandler("paymentHandler"),

    @SerializedName("periodicBackgroundSync")
    @JsonProperty("periodicBackgroundSync")
    PeriodicBackgroundSync("periodicBackgroundSync"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ServiceName(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
