// SPDX-License-Identifier: MIT
package com.cdp4j.type.fedcm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The buttons on the FedCM dialog.
 */
public enum DialogButton {
    @SerializedName("ConfirmIdpLoginContinue")
    @JsonProperty("ConfirmIdpLoginContinue")
    ConfirmIdpLoginContinue("ConfirmIdpLoginContinue"),

    @SerializedName("ErrorGotIt")
    @JsonProperty("ErrorGotIt")
    ErrorGotIt("ErrorGotIt"),

    @SerializedName("ErrorMoreDetails")
    @JsonProperty("ErrorMoreDetails")
    ErrorMoreDetails("ErrorMoreDetails"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    DialogButton(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
