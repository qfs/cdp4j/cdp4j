// SPDX-License-Identifier: MIT
package com.cdp4j.type.fedcm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The types of FedCM dialogs.
 */
public enum DialogType {
    @SerializedName("AccountChooser")
    @JsonProperty("AccountChooser")
    AccountChooser("AccountChooser"),

    @SerializedName("AutoReauthn")
    @JsonProperty("AutoReauthn")
    AutoReauthn("AutoReauthn"),

    @SerializedName("ConfirmIdpLogin")
    @JsonProperty("ConfirmIdpLogin")
    ConfirmIdpLogin("ConfirmIdpLogin"),

    @SerializedName("Error")
    @JsonProperty("Error")
    Error("Error"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    DialogType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
