// SPDX-License-Identifier: MIT
package com.cdp4j.type.fedcm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The URLs that each account has
 */
public enum AccountUrlType {
    @SerializedName("TermsOfService")
    @JsonProperty("TermsOfService")
    TermsOfService("TermsOfService"),

    @SerializedName("PrivacyPolicy")
    @JsonProperty("PrivacyPolicy")
    PrivacyPolicy("PrivacyPolicy"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AccountUrlType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
