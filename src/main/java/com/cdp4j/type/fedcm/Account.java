// SPDX-License-Identifier: MIT
package com.cdp4j.type.fedcm;

/**
 * Corresponds to IdentityRequestAccount
 */
public class Account {
    private String accountId;

    private String email;

    private String name;

    private String givenName;

    private String pictureUrl;

    private String idpConfigUrl;

    private String idpLoginUrl;

    private LoginState loginState;

    private String termsOfServiceUrl;

    private String privacyPolicyUrl;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getIdpConfigUrl() {
        return idpConfigUrl;
    }

    public void setIdpConfigUrl(String idpConfigUrl) {
        this.idpConfigUrl = idpConfigUrl;
    }

    public String getIdpLoginUrl() {
        return idpLoginUrl;
    }

    public void setIdpLoginUrl(String idpLoginUrl) {
        this.idpLoginUrl = idpLoginUrl;
    }

    public LoginState getLoginState() {
        return loginState;
    }

    public void setLoginState(LoginState loginState) {
        this.loginState = loginState;
    }

    /**
     * These two are only set if the loginState is signUp
     */
    public String getTermsOfServiceUrl() {
        return termsOfServiceUrl;
    }

    /**
     * These two are only set if the loginState is signUp
     */
    public void setTermsOfServiceUrl(String termsOfServiceUrl) {
        this.termsOfServiceUrl = termsOfServiceUrl;
    }

    public String getPrivacyPolicyUrl() {
        return privacyPolicyUrl;
    }

    public void setPrivacyPolicyUrl(String privacyPolicyUrl) {
        this.privacyPolicyUrl = privacyPolicyUrl;
    }

    public String toString() {
        return "Account [accountId=" + accountId + ", email=" + email + ", name=" + name + ", givenName=" + givenName
                + ", pictureUrl=" + pictureUrl + ", idpConfigUrl=" + idpConfigUrl + ", idpLoginUrl=" + idpLoginUrl
                + ", loginState=" + loginState + ", termsOfServiceUrl=" + termsOfServiceUrl + ", privacyPolicyUrl="
                + privacyPolicyUrl + "]";
    }
}
