// SPDX-License-Identifier: MIT
package com.cdp4j.type.fedcm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Whether this is a sign-up or sign-in action for this account, i.e.
 * whether this account has ever been used to sign in to this RP before.
 */
public enum LoginState {
    @SerializedName("SignIn")
    @JsonProperty("SignIn")
    SignIn("SignIn"),

    @SerializedName("SignUp")
    @JsonProperty("SignUp")
    SignUp("SignUp"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    LoginState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
