// SPDX-License-Identifier: MIT
package com.cdp4j.type.domstorage;

/**
 * DOM Storage identifier.
 */
public class StorageId {
    private String securityOrigin;

    private String storageKey;

    private Boolean isLocalStorage;

    /**
     * Security origin for the storage.
     */
    public String getSecurityOrigin() {
        return securityOrigin;
    }

    /**
     * Security origin for the storage.
     */
    public void setSecurityOrigin(String securityOrigin) {
        this.securityOrigin = securityOrigin;
    }

    /**
     * Represents a key by which DOM Storage keys its CachedStorageAreas
     */
    public String getStorageKey() {
        return storageKey;
    }

    /**
     * Represents a key by which DOM Storage keys its CachedStorageAreas
     */
    public void setStorageKey(String storageKey) {
        this.storageKey = storageKey;
    }

    /**
     * Whether the storage is local storage (not session storage).
     */
    public Boolean isIsLocalStorage() {
        return isLocalStorage;
    }

    /**
     * Whether the storage is local storage (not session storage).
     */
    public void setIsLocalStorage(Boolean isLocalStorage) {
        this.isLocalStorage = isLocalStorage;
    }

    public String toString() {
        return "StorageId [securityOrigin=" + securityOrigin + ", storageKey=" + storageKey + ", isLocalStorage="
                + isLocalStorage + "]";
    }
}
