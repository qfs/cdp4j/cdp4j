// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.dom.RGBA;

public class IsolationModeHighlightConfig {
    private RGBA resizerColor;

    private RGBA resizerHandleColor;

    private RGBA maskColor;

    /**
     * The fill color of the resizers (default: transparent).
     */
    public RGBA getResizerColor() {
        return resizerColor;
    }

    /**
     * The fill color of the resizers (default: transparent).
     */
    public void setResizerColor(RGBA resizerColor) {
        this.resizerColor = resizerColor;
    }

    /**
     * The fill color for resizer handles (default: transparent).
     */
    public RGBA getResizerHandleColor() {
        return resizerHandleColor;
    }

    /**
     * The fill color for resizer handles (default: transparent).
     */
    public void setResizerHandleColor(RGBA resizerHandleColor) {
        this.resizerHandleColor = resizerHandleColor;
    }

    /**
     * The fill color for the mask covering non-isolated elements (default:
     * transparent).
     */
    public RGBA getMaskColor() {
        return maskColor;
    }

    /**
     * The fill color for the mask covering non-isolated elements (default:
     * transparent).
     */
    public void setMaskColor(RGBA maskColor) {
        this.maskColor = maskColor;
    }

    public String toString() {
        return "IsolationModeHighlightConfig [resizerColor=" + resizerColor + ", resizerHandleColor="
                + resizerHandleColor + ", maskColor=" + maskColor + "]";
    }
}
