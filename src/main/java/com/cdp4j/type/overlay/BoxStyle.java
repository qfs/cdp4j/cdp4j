// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.dom.RGBA;

/**
 * Style information for drawing a box.
 */
public class BoxStyle {
    private RGBA fillColor;

    private RGBA hatchColor;

    /**
     * The background color for the box (default: transparent)
     */
    public RGBA getFillColor() {
        return fillColor;
    }

    /**
     * The background color for the box (default: transparent)
     */
    public void setFillColor(RGBA fillColor) {
        this.fillColor = fillColor;
    }

    /**
     * The hatching color for the box (default: transparent)
     */
    public RGBA getHatchColor() {
        return hatchColor;
    }

    /**
     * The hatching color for the box (default: transparent)
     */
    public void setHatchColor(RGBA hatchColor) {
        this.hatchColor = hatchColor;
    }

    public String toString() {
        return "BoxStyle [fillColor=" + fillColor + ", hatchColor=" + hatchColor + "]";
    }
}
