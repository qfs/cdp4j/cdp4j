// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.constant.LineStylePattern;
import com.cdp4j.type.dom.RGBA;

/**
 * Style information for drawing a line.
 */
public class LineStyle {
    private RGBA color;

    private LineStylePattern pattern;

    /**
     * The color of the line (default: transparent)
     */
    public RGBA getColor() {
        return color;
    }

    /**
     * The color of the line (default: transparent)
     */
    public void setColor(RGBA color) {
        this.color = color;
    }

    /**
     * The line pattern (default: solid)
     */
    public LineStylePattern getPattern() {
        return pattern;
    }

    /**
     * The line pattern (default: solid)
     */
    public void setPattern(LineStylePattern pattern) {
        this.pattern = pattern;
    }

    public String toString() {
        return "LineStyle [color=" + color + ", pattern=" + pattern + "]";
    }
}
