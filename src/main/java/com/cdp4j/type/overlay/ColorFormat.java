// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ColorFormat {
    @SerializedName("rgb")
    @JsonProperty("rgb")
    Rgb("rgb"),

    @SerializedName("hsl")
    @JsonProperty("hsl")
    Hsl("hsl"),

    @SerializedName("hwb")
    @JsonProperty("hwb")
    Hwb("hwb"),

    @SerializedName("hex")
    @JsonProperty("hex")
    Hex("hex"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ColorFormat(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
