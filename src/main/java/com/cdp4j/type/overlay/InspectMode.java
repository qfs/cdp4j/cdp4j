// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum InspectMode {
    @SerializedName("searchForNode")
    @JsonProperty("searchForNode")
    SearchForNode("searchForNode"),

    @SerializedName("searchForUAShadowDOM")
    @JsonProperty("searchForUAShadowDOM")
    SearchForUAShadowDOM("searchForUAShadowDOM"),

    @SerializedName("captureAreaScreenshot")
    @JsonProperty("captureAreaScreenshot")
    CaptureAreaScreenshot("captureAreaScreenshot"),

    @SerializedName("showDistances")
    @JsonProperty("showDistances")
    ShowDistances("showDistances"),

    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    InspectMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
