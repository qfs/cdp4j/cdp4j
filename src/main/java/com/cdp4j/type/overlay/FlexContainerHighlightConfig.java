// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

/**
 * Configuration data for the highlighting of Flex container elements.
 */
public class FlexContainerHighlightConfig {
    private LineStyle containerBorder;

    private LineStyle lineSeparator;

    private LineStyle itemSeparator;

    private BoxStyle mainDistributedSpace;

    private BoxStyle crossDistributedSpace;

    private BoxStyle rowGapSpace;

    private BoxStyle columnGapSpace;

    private LineStyle crossAlignment;

    /**
     * The style of the container border
     */
    public LineStyle getContainerBorder() {
        return containerBorder;
    }

    /**
     * The style of the container border
     */
    public void setContainerBorder(LineStyle containerBorder) {
        this.containerBorder = containerBorder;
    }

    /**
     * The style of the separator between lines
     */
    public LineStyle getLineSeparator() {
        return lineSeparator;
    }

    /**
     * The style of the separator between lines
     */
    public void setLineSeparator(LineStyle lineSeparator) {
        this.lineSeparator = lineSeparator;
    }

    /**
     * The style of the separator between items
     */
    public LineStyle getItemSeparator() {
        return itemSeparator;
    }

    /**
     * The style of the separator between items
     */
    public void setItemSeparator(LineStyle itemSeparator) {
        this.itemSeparator = itemSeparator;
    }

    /**
     * Style of content-distribution space on the main axis (justify-content).
     */
    public BoxStyle getMainDistributedSpace() {
        return mainDistributedSpace;
    }

    /**
     * Style of content-distribution space on the main axis (justify-content).
     */
    public void setMainDistributedSpace(BoxStyle mainDistributedSpace) {
        this.mainDistributedSpace = mainDistributedSpace;
    }

    /**
     * Style of content-distribution space on the cross axis (align-content).
     */
    public BoxStyle getCrossDistributedSpace() {
        return crossDistributedSpace;
    }

    /**
     * Style of content-distribution space on the cross axis (align-content).
     */
    public void setCrossDistributedSpace(BoxStyle crossDistributedSpace) {
        this.crossDistributedSpace = crossDistributedSpace;
    }

    /**
     * Style of empty space caused by row gaps (gap/row-gap).
     */
    public BoxStyle getRowGapSpace() {
        return rowGapSpace;
    }

    /**
     * Style of empty space caused by row gaps (gap/row-gap).
     */
    public void setRowGapSpace(BoxStyle rowGapSpace) {
        this.rowGapSpace = rowGapSpace;
    }

    /**
     * Style of empty space caused by columns gaps (gap/column-gap).
     */
    public BoxStyle getColumnGapSpace() {
        return columnGapSpace;
    }

    /**
     * Style of empty space caused by columns gaps (gap/column-gap).
     */
    public void setColumnGapSpace(BoxStyle columnGapSpace) {
        this.columnGapSpace = columnGapSpace;
    }

    /**
     * Style of the self-alignment line (align-items).
     */
    public LineStyle getCrossAlignment() {
        return crossAlignment;
    }

    /**
     * Style of the self-alignment line (align-items).
     */
    public void setCrossAlignment(LineStyle crossAlignment) {
        this.crossAlignment = crossAlignment;
    }

    public String toString() {
        return "FlexContainerHighlightConfig [containerBorder=" + containerBorder + ", lineSeparator=" + lineSeparator
                + ", itemSeparator=" + itemSeparator + ", mainDistributedSpace=" + mainDistributedSpace
                + ", crossDistributedSpace=" + crossDistributedSpace + ", rowGapSpace=" + rowGapSpace
                + ", columnGapSpace=" + columnGapSpace + ", crossAlignment=" + crossAlignment + "]";
    }
}
