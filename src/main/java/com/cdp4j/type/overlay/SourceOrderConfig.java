// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.dom.RGBA;

/**
 * Configuration data for drawing the source order of an elements children.
 */
public class SourceOrderConfig {
    private RGBA parentOutlineColor;

    private RGBA childOutlineColor;

    /**
     * the color to outline the given element in.
     */
    public RGBA getParentOutlineColor() {
        return parentOutlineColor;
    }

    /**
     * the color to outline the given element in.
     */
    public void setParentOutlineColor(RGBA parentOutlineColor) {
        this.parentOutlineColor = parentOutlineColor;
    }

    /**
     * the color to outline the child elements in.
     */
    public RGBA getChildOutlineColor() {
        return childOutlineColor;
    }

    /**
     * the color to outline the child elements in.
     */
    public void setChildOutlineColor(RGBA childOutlineColor) {
        this.childOutlineColor = childOutlineColor;
    }

    public String toString() {
        return "SourceOrderConfig [parentOutlineColor=" + parentOutlineColor + ", childOutlineColor="
                + childOutlineColor + "]";
    }
}
