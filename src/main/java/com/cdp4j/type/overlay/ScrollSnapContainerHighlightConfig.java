// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.dom.RGBA;

public class ScrollSnapContainerHighlightConfig {
    private LineStyle snapportBorder;

    private LineStyle snapAreaBorder;

    private RGBA scrollMarginColor;

    private RGBA scrollPaddingColor;

    /**
     * The style of the snapport border (default: transparent)
     */
    public LineStyle getSnapportBorder() {
        return snapportBorder;
    }

    /**
     * The style of the snapport border (default: transparent)
     */
    public void setSnapportBorder(LineStyle snapportBorder) {
        this.snapportBorder = snapportBorder;
    }

    /**
     * The style of the snap area border (default: transparent)
     */
    public LineStyle getSnapAreaBorder() {
        return snapAreaBorder;
    }

    /**
     * The style of the snap area border (default: transparent)
     */
    public void setSnapAreaBorder(LineStyle snapAreaBorder) {
        this.snapAreaBorder = snapAreaBorder;
    }

    /**
     * The margin highlight fill color (default: transparent).
     */
    public RGBA getScrollMarginColor() {
        return scrollMarginColor;
    }

    /**
     * The margin highlight fill color (default: transparent).
     */
    public void setScrollMarginColor(RGBA scrollMarginColor) {
        this.scrollMarginColor = scrollMarginColor;
    }

    /**
     * The padding highlight fill color (default: transparent).
     */
    public RGBA getScrollPaddingColor() {
        return scrollPaddingColor;
    }

    /**
     * The padding highlight fill color (default: transparent).
     */
    public void setScrollPaddingColor(RGBA scrollPaddingColor) {
        this.scrollPaddingColor = scrollPaddingColor;
    }

    public String toString() {
        return "ScrollSnapContainerHighlightConfig [snapportBorder=" + snapportBorder + ", snapAreaBorder="
                + snapAreaBorder + ", scrollMarginColor=" + scrollMarginColor + ", scrollPaddingColor="
                + scrollPaddingColor + "]";
    }
}
