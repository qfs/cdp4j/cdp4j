// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ContrastAlgorithm {
    @SerializedName("aa")
    @JsonProperty("aa")
    Aa("aa"),

    @SerializedName("aaa")
    @JsonProperty("aaa")
    Aaa("aaa"),

    @SerializedName("apca")
    @JsonProperty("apca")
    Apca("apca"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ContrastAlgorithm(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
