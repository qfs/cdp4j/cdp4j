// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.dom.RGBA;

/**
 * Configuration data for the highlighting of page elements.
 */
public class HighlightConfig {
    private Boolean showInfo;

    private Boolean showStyles;

    private Boolean showRulers;

    private Boolean showAccessibilityInfo;

    private Boolean showExtensionLines;

    private RGBA contentColor;

    private RGBA paddingColor;

    private RGBA borderColor;

    private RGBA marginColor;

    private RGBA eventTargetColor;

    private RGBA shapeColor;

    private RGBA shapeMarginColor;

    private RGBA cssGridColor;

    private ColorFormat colorFormat;

    private GridHighlightConfig gridHighlightConfig;

    private FlexContainerHighlightConfig flexContainerHighlightConfig;

    private FlexItemHighlightConfig flexItemHighlightConfig;

    private ContrastAlgorithm contrastAlgorithm;

    private ContainerQueryContainerHighlightConfig containerQueryContainerHighlightConfig;

    /**
     * Whether the node info tooltip should be shown (default: false).
     */
    public Boolean isShowInfo() {
        return showInfo;
    }

    /**
     * Whether the node info tooltip should be shown (default: false).
     */
    public void setShowInfo(Boolean showInfo) {
        this.showInfo = showInfo;
    }

    /**
     * Whether the node styles in the tooltip (default: false).
     */
    public Boolean isShowStyles() {
        return showStyles;
    }

    /**
     * Whether the node styles in the tooltip (default: false).
     */
    public void setShowStyles(Boolean showStyles) {
        this.showStyles = showStyles;
    }

    /**
     * Whether the rulers should be shown (default: false).
     */
    public Boolean isShowRulers() {
        return showRulers;
    }

    /**
     * Whether the rulers should be shown (default: false).
     */
    public void setShowRulers(Boolean showRulers) {
        this.showRulers = showRulers;
    }

    /**
     * Whether the a11y info should be shown (default: true).
     */
    public Boolean isShowAccessibilityInfo() {
        return showAccessibilityInfo;
    }

    /**
     * Whether the a11y info should be shown (default: true).
     */
    public void setShowAccessibilityInfo(Boolean showAccessibilityInfo) {
        this.showAccessibilityInfo = showAccessibilityInfo;
    }

    /**
     * Whether the extension lines from node to the rulers should be shown (default:
     * false).
     */
    public Boolean isShowExtensionLines() {
        return showExtensionLines;
    }

    /**
     * Whether the extension lines from node to the rulers should be shown (default:
     * false).
     */
    public void setShowExtensionLines(Boolean showExtensionLines) {
        this.showExtensionLines = showExtensionLines;
    }

    /**
     * The content box highlight fill color (default: transparent).
     */
    public RGBA getContentColor() {
        return contentColor;
    }

    /**
     * The content box highlight fill color (default: transparent).
     */
    public void setContentColor(RGBA contentColor) {
        this.contentColor = contentColor;
    }

    /**
     * The padding highlight fill color (default: transparent).
     */
    public RGBA getPaddingColor() {
        return paddingColor;
    }

    /**
     * The padding highlight fill color (default: transparent).
     */
    public void setPaddingColor(RGBA paddingColor) {
        this.paddingColor = paddingColor;
    }

    /**
     * The border highlight fill color (default: transparent).
     */
    public RGBA getBorderColor() {
        return borderColor;
    }

    /**
     * The border highlight fill color (default: transparent).
     */
    public void setBorderColor(RGBA borderColor) {
        this.borderColor = borderColor;
    }

    /**
     * The margin highlight fill color (default: transparent).
     */
    public RGBA getMarginColor() {
        return marginColor;
    }

    /**
     * The margin highlight fill color (default: transparent).
     */
    public void setMarginColor(RGBA marginColor) {
        this.marginColor = marginColor;
    }

    /**
     * The event target element highlight fill color (default: transparent).
     */
    public RGBA getEventTargetColor() {
        return eventTargetColor;
    }

    /**
     * The event target element highlight fill color (default: transparent).
     */
    public void setEventTargetColor(RGBA eventTargetColor) {
        this.eventTargetColor = eventTargetColor;
    }

    /**
     * The shape outside fill color (default: transparent).
     */
    public RGBA getShapeColor() {
        return shapeColor;
    }

    /**
     * The shape outside fill color (default: transparent).
     */
    public void setShapeColor(RGBA shapeColor) {
        this.shapeColor = shapeColor;
    }

    /**
     * The shape margin fill color (default: transparent).
     */
    public RGBA getShapeMarginColor() {
        return shapeMarginColor;
    }

    /**
     * The shape margin fill color (default: transparent).
     */
    public void setShapeMarginColor(RGBA shapeMarginColor) {
        this.shapeMarginColor = shapeMarginColor;
    }

    /**
     * The grid layout color (default: transparent).
     */
    public RGBA getCssGridColor() {
        return cssGridColor;
    }

    /**
     * The grid layout color (default: transparent).
     */
    public void setCssGridColor(RGBA cssGridColor) {
        this.cssGridColor = cssGridColor;
    }

    /**
     * The color format used to format color styles (default: hex).
     */
    public ColorFormat getColorFormat() {
        return colorFormat;
    }

    /**
     * The color format used to format color styles (default: hex).
     */
    public void setColorFormat(ColorFormat colorFormat) {
        this.colorFormat = colorFormat;
    }

    /**
     * The grid layout highlight configuration (default: all transparent).
     */
    public GridHighlightConfig getGridHighlightConfig() {
        return gridHighlightConfig;
    }

    /**
     * The grid layout highlight configuration (default: all transparent).
     */
    public void setGridHighlightConfig(GridHighlightConfig gridHighlightConfig) {
        this.gridHighlightConfig = gridHighlightConfig;
    }

    /**
     * The flex container highlight configuration (default: all transparent).
     */
    public FlexContainerHighlightConfig getFlexContainerHighlightConfig() {
        return flexContainerHighlightConfig;
    }

    /**
     * The flex container highlight configuration (default: all transparent).
     */
    public void setFlexContainerHighlightConfig(FlexContainerHighlightConfig flexContainerHighlightConfig) {
        this.flexContainerHighlightConfig = flexContainerHighlightConfig;
    }

    /**
     * The flex item highlight configuration (default: all transparent).
     */
    public FlexItemHighlightConfig getFlexItemHighlightConfig() {
        return flexItemHighlightConfig;
    }

    /**
     * The flex item highlight configuration (default: all transparent).
     */
    public void setFlexItemHighlightConfig(FlexItemHighlightConfig flexItemHighlightConfig) {
        this.flexItemHighlightConfig = flexItemHighlightConfig;
    }

    /**
     * The contrast algorithm to use for the contrast ratio (default: aa).
     */
    public ContrastAlgorithm getContrastAlgorithm() {
        return contrastAlgorithm;
    }

    /**
     * The contrast algorithm to use for the contrast ratio (default: aa).
     */
    public void setContrastAlgorithm(ContrastAlgorithm contrastAlgorithm) {
        this.contrastAlgorithm = contrastAlgorithm;
    }

    /**
     * The container query container highlight configuration (default: all
     * transparent).
     */
    public ContainerQueryContainerHighlightConfig getContainerQueryContainerHighlightConfig() {
        return containerQueryContainerHighlightConfig;
    }

    /**
     * The container query container highlight configuration (default: all
     * transparent).
     */
    public void setContainerQueryContainerHighlightConfig(
            ContainerQueryContainerHighlightConfig containerQueryContainerHighlightConfig) {
        this.containerQueryContainerHighlightConfig = containerQueryContainerHighlightConfig;
    }

    public String toString() {
        return "HighlightConfig [showInfo=" + showInfo + ", showStyles=" + showStyles + ", showRulers=" + showRulers
                + ", showAccessibilityInfo=" + showAccessibilityInfo + ", showExtensionLines=" + showExtensionLines
                + ", contentColor=" + contentColor + ", paddingColor=" + paddingColor + ", borderColor=" + borderColor
                + ", marginColor=" + marginColor + ", eventTargetColor=" + eventTargetColor + ", shapeColor="
                + shapeColor + ", shapeMarginColor=" + shapeMarginColor + ", cssGridColor=" + cssGridColor
                + ", colorFormat=" + colorFormat + ", gridHighlightConfig=" + gridHighlightConfig
                + ", flexContainerHighlightConfig=" + flexContainerHighlightConfig + ", flexItemHighlightConfig="
                + flexItemHighlightConfig + ", contrastAlgorithm=" + contrastAlgorithm
                + ", containerQueryContainerHighlightConfig=" + containerQueryContainerHighlightConfig + "]";
    }
}
