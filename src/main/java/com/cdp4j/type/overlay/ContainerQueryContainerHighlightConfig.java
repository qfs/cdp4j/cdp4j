// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

public class ContainerQueryContainerHighlightConfig {
    private LineStyle containerBorder;

    private LineStyle descendantBorder;

    /**
     * The style of the container border.
     */
    public LineStyle getContainerBorder() {
        return containerBorder;
    }

    /**
     * The style of the container border.
     */
    public void setContainerBorder(LineStyle containerBorder) {
        this.containerBorder = containerBorder;
    }

    /**
     * The style of the descendants' borders.
     */
    public LineStyle getDescendantBorder() {
        return descendantBorder;
    }

    /**
     * The style of the descendants' borders.
     */
    public void setDescendantBorder(LineStyle descendantBorder) {
        this.descendantBorder = descendantBorder;
    }

    public String toString() {
        return "ContainerQueryContainerHighlightConfig [containerBorder=" + containerBorder + ", descendantBorder="
                + descendantBorder + "]";
    }
}
