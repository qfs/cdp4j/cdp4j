// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

/**
 * Configuration for Window Controls Overlay
 */
public class WindowControlsOverlayConfig {
    private Boolean showCSS;

    private String selectedPlatform;

    private String themeColor;

    /**
     * Whether the title bar CSS should be shown when emulating the Window Controls
     * Overlay.
     */
    public Boolean isShowCSS() {
        return showCSS;
    }

    /**
     * Whether the title bar CSS should be shown when emulating the Window Controls
     * Overlay.
     */
    public void setShowCSS(Boolean showCSS) {
        this.showCSS = showCSS;
    }

    /**
     * Selected platforms to show the overlay.
     */
    public String getSelectedPlatform() {
        return selectedPlatform;
    }

    /**
     * Selected platforms to show the overlay.
     */
    public void setSelectedPlatform(String selectedPlatform) {
        this.selectedPlatform = selectedPlatform;
    }

    /**
     * The theme color defined in app manifest.
     */
    public String getThemeColor() {
        return themeColor;
    }

    /**
     * The theme color defined in app manifest.
     */
    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public String toString() {
        return "WindowControlsOverlayConfig [showCSS=" + showCSS + ", selectedPlatform=" + selectedPlatform
                + ", themeColor=" + themeColor + "]";
    }
}
