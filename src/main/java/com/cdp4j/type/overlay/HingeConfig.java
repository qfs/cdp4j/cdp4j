// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.dom.RGBA;
import com.cdp4j.type.dom.Rect;

/**
 * Configuration for dual screen hinge
 */
public class HingeConfig {
    private Rect rect;

    private RGBA contentColor;

    private RGBA outlineColor;

    /**
     * A rectangle represent hinge
     */
    public Rect getRect() {
        return rect;
    }

    /**
     * A rectangle represent hinge
     */
    public void setRect(Rect rect) {
        this.rect = rect;
    }

    /**
     * The content box highlight fill color (default: a dark color).
     */
    public RGBA getContentColor() {
        return contentColor;
    }

    /**
     * The content box highlight fill color (default: a dark color).
     */
    public void setContentColor(RGBA contentColor) {
        this.contentColor = contentColor;
    }

    /**
     * The content box highlight outline color (default: transparent).
     */
    public RGBA getOutlineColor() {
        return outlineColor;
    }

    /**
     * The content box highlight outline color (default: transparent).
     */
    public void setOutlineColor(RGBA outlineColor) {
        this.outlineColor = outlineColor;
    }

    public String toString() {
        return "HingeConfig [rect=" + rect + ", contentColor=" + contentColor + ", outlineColor=" + outlineColor + "]";
    }
}
