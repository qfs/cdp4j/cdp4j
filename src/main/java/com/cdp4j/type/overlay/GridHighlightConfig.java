// SPDX-License-Identifier: MIT
package com.cdp4j.type.overlay;

import com.cdp4j.type.dom.RGBA;

/**
 * Configuration data for the highlighting of Grid elements.
 */
public class GridHighlightConfig {
    private Boolean showGridExtensionLines;

    private Boolean showPositiveLineNumbers;

    private Boolean showNegativeLineNumbers;

    private Boolean showAreaNames;

    private Boolean showLineNames;

    private Boolean showTrackSizes;

    private RGBA gridBorderColor;

    private RGBA cellBorderColor;

    private RGBA rowLineColor;

    private RGBA columnLineColor;

    private Boolean gridBorderDash;

    private Boolean cellBorderDash;

    private Boolean rowLineDash;

    private Boolean columnLineDash;

    private RGBA rowGapColor;

    private RGBA rowHatchColor;

    private RGBA columnGapColor;

    private RGBA columnHatchColor;

    private RGBA areaBorderColor;

    private RGBA gridBackgroundColor;

    /**
     * Whether the extension lines from grid cells to the rulers should be shown
     * (default: false).
     */
    public Boolean isShowGridExtensionLines() {
        return showGridExtensionLines;
    }

    /**
     * Whether the extension lines from grid cells to the rulers should be shown
     * (default: false).
     */
    public void setShowGridExtensionLines(Boolean showGridExtensionLines) {
        this.showGridExtensionLines = showGridExtensionLines;
    }

    /**
     * Show Positive line number labels (default: false).
     */
    public Boolean isShowPositiveLineNumbers() {
        return showPositiveLineNumbers;
    }

    /**
     * Show Positive line number labels (default: false).
     */
    public void setShowPositiveLineNumbers(Boolean showPositiveLineNumbers) {
        this.showPositiveLineNumbers = showPositiveLineNumbers;
    }

    /**
     * Show Negative line number labels (default: false).
     */
    public Boolean isShowNegativeLineNumbers() {
        return showNegativeLineNumbers;
    }

    /**
     * Show Negative line number labels (default: false).
     */
    public void setShowNegativeLineNumbers(Boolean showNegativeLineNumbers) {
        this.showNegativeLineNumbers = showNegativeLineNumbers;
    }

    /**
     * Show area name labels (default: false).
     */
    public Boolean isShowAreaNames() {
        return showAreaNames;
    }

    /**
     * Show area name labels (default: false).
     */
    public void setShowAreaNames(Boolean showAreaNames) {
        this.showAreaNames = showAreaNames;
    }

    /**
     * Show line name labels (default: false).
     */
    public Boolean isShowLineNames() {
        return showLineNames;
    }

    /**
     * Show line name labels (default: false).
     */
    public void setShowLineNames(Boolean showLineNames) {
        this.showLineNames = showLineNames;
    }

    /**
     * Show track size labels (default: false).
     */
    public Boolean isShowTrackSizes() {
        return showTrackSizes;
    }

    /**
     * Show track size labels (default: false).
     */
    public void setShowTrackSizes(Boolean showTrackSizes) {
        this.showTrackSizes = showTrackSizes;
    }

    /**
     * The grid container border highlight color (default: transparent).
     */
    public RGBA getGridBorderColor() {
        return gridBorderColor;
    }

    /**
     * The grid container border highlight color (default: transparent).
     */
    public void setGridBorderColor(RGBA gridBorderColor) {
        this.gridBorderColor = gridBorderColor;
    }

    /**
     * The cell border color (default: transparent). Deprecated, please use
     * rowLineColor and columnLineColor instead.
     */
    @Deprecated
    public RGBA getCellBorderColor() {
        return cellBorderColor;
    }

    /**
     * The cell border color (default: transparent). Deprecated, please use
     * rowLineColor and columnLineColor instead.
     */
    @Deprecated
    public void setCellBorderColor(RGBA cellBorderColor) {
        this.cellBorderColor = cellBorderColor;
    }

    /**
     * The row line color (default: transparent).
     */
    public RGBA getRowLineColor() {
        return rowLineColor;
    }

    /**
     * The row line color (default: transparent).
     */
    public void setRowLineColor(RGBA rowLineColor) {
        this.rowLineColor = rowLineColor;
    }

    /**
     * The column line color (default: transparent).
     */
    public RGBA getColumnLineColor() {
        return columnLineColor;
    }

    /**
     * The column line color (default: transparent).
     */
    public void setColumnLineColor(RGBA columnLineColor) {
        this.columnLineColor = columnLineColor;
    }

    /**
     * Whether the grid border is dashed (default: false).
     */
    public Boolean isGridBorderDash() {
        return gridBorderDash;
    }

    /**
     * Whether the grid border is dashed (default: false).
     */
    public void setGridBorderDash(Boolean gridBorderDash) {
        this.gridBorderDash = gridBorderDash;
    }

    /**
     * Whether the cell border is dashed (default: false). Deprecated, please us
     * rowLineDash and columnLineDash instead.
     */
    @Deprecated
    public Boolean isCellBorderDash() {
        return cellBorderDash;
    }

    /**
     * Whether the cell border is dashed (default: false). Deprecated, please us
     * rowLineDash and columnLineDash instead.
     */
    @Deprecated
    public void setCellBorderDash(Boolean cellBorderDash) {
        this.cellBorderDash = cellBorderDash;
    }

    /**
     * Whether row lines are dashed (default: false).
     */
    public Boolean isRowLineDash() {
        return rowLineDash;
    }

    /**
     * Whether row lines are dashed (default: false).
     */
    public void setRowLineDash(Boolean rowLineDash) {
        this.rowLineDash = rowLineDash;
    }

    /**
     * Whether column lines are dashed (default: false).
     */
    public Boolean isColumnLineDash() {
        return columnLineDash;
    }

    /**
     * Whether column lines are dashed (default: false).
     */
    public void setColumnLineDash(Boolean columnLineDash) {
        this.columnLineDash = columnLineDash;
    }

    /**
     * The row gap highlight fill color (default: transparent).
     */
    public RGBA getRowGapColor() {
        return rowGapColor;
    }

    /**
     * The row gap highlight fill color (default: transparent).
     */
    public void setRowGapColor(RGBA rowGapColor) {
        this.rowGapColor = rowGapColor;
    }

    /**
     * The row gap hatching fill color (default: transparent).
     */
    public RGBA getRowHatchColor() {
        return rowHatchColor;
    }

    /**
     * The row gap hatching fill color (default: transparent).
     */
    public void setRowHatchColor(RGBA rowHatchColor) {
        this.rowHatchColor = rowHatchColor;
    }

    /**
     * The column gap highlight fill color (default: transparent).
     */
    public RGBA getColumnGapColor() {
        return columnGapColor;
    }

    /**
     * The column gap highlight fill color (default: transparent).
     */
    public void setColumnGapColor(RGBA columnGapColor) {
        this.columnGapColor = columnGapColor;
    }

    /**
     * The column gap hatching fill color (default: transparent).
     */
    public RGBA getColumnHatchColor() {
        return columnHatchColor;
    }

    /**
     * The column gap hatching fill color (default: transparent).
     */
    public void setColumnHatchColor(RGBA columnHatchColor) {
        this.columnHatchColor = columnHatchColor;
    }

    /**
     * The named grid areas border color (Default: transparent).
     */
    public RGBA getAreaBorderColor() {
        return areaBorderColor;
    }

    /**
     * The named grid areas border color (Default: transparent).
     */
    public void setAreaBorderColor(RGBA areaBorderColor) {
        this.areaBorderColor = areaBorderColor;
    }

    /**
     * The grid container background color (Default: transparent).
     */
    public RGBA getGridBackgroundColor() {
        return gridBackgroundColor;
    }

    /**
     * The grid container background color (Default: transparent).
     */
    public void setGridBackgroundColor(RGBA gridBackgroundColor) {
        this.gridBackgroundColor = gridBackgroundColor;
    }

    public String toString() {
        return "GridHighlightConfig [showGridExtensionLines=" + showGridExtensionLines + ", showPositiveLineNumbers="
                + showPositiveLineNumbers + ", showNegativeLineNumbers=" + showNegativeLineNumbers + ", showAreaNames="
                + showAreaNames + ", showLineNames=" + showLineNames + ", showTrackSizes=" + showTrackSizes
                + ", gridBorderColor=" + gridBorderColor + ", cellBorderColor=" + cellBorderColor + ", rowLineColor="
                + rowLineColor + ", columnLineColor=" + columnLineColor + ", gridBorderDash=" + gridBorderDash
                + ", cellBorderDash=" + cellBorderDash + ", rowLineDash=" + rowLineDash + ", columnLineDash="
                + columnLineDash + ", rowGapColor=" + rowGapColor + ", rowHatchColor=" + rowHatchColor
                + ", columnGapColor=" + columnGapColor + ", columnHatchColor=" + columnHatchColor + ", areaBorderColor="
                + areaBorderColor + ", gridBackgroundColor=" + gridBackgroundColor + "]";
    }
}
