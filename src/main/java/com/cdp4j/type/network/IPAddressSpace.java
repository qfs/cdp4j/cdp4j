// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum IPAddressSpace {
    @SerializedName("Local")
    @JsonProperty("Local")
    Local("Local"),

    @SerializedName("Private")
    @JsonProperty("Private")
    Private("Private"),

    @SerializedName("Public")
    @JsonProperty("Public")
    Public("Public"),

    @SerializedName("Unknown")
    @JsonProperty("Unknown")
    Unknown("Unknown"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    IPAddressSpace(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
