// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

/**
 * Cookie object
 */
public class Cookie {
    private String name;

    private String value;

    private String domain;

    private String path;

    private Double expires;

    private Integer size;

    private Boolean httpOnly;

    private Boolean secure;

    private Boolean session;

    private CookieSameSite sameSite;

    private CookiePriority priority;

    private Boolean sameParty;

    private CookieSourceScheme sourceScheme;

    private Integer sourcePort;

    private CookiePartitionKey partitionKey;

    private Boolean partitionKeyOpaque;

    /**
     * Cookie name.
     */
    public String getName() {
        return name;
    }

    /**
     * Cookie name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Cookie value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Cookie value.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Cookie domain.
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Cookie domain.
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * Cookie path.
     */
    public String getPath() {
        return path;
    }

    /**
     * Cookie path.
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Cookie expiration date as the number of seconds since the UNIX epoch.
     */
    public Double getExpires() {
        return expires;
    }

    /**
     * Cookie expiration date as the number of seconds since the UNIX epoch.
     */
    public void setExpires(Double expires) {
        this.expires = expires;
    }

    /**
     * Cookie size.
     */
    public Integer getSize() {
        return size;
    }

    /**
     * Cookie size.
     */
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * True if cookie is http-only.
     */
    public Boolean isHttpOnly() {
        return httpOnly;
    }

    /**
     * True if cookie is http-only.
     */
    public void setHttpOnly(Boolean httpOnly) {
        this.httpOnly = httpOnly;
    }

    /**
     * True if cookie is secure.
     */
    public Boolean isSecure() {
        return secure;
    }

    /**
     * True if cookie is secure.
     */
    public void setSecure(Boolean secure) {
        this.secure = secure;
    }

    /**
     * True in case of session cookie.
     */
    public Boolean isSession() {
        return session;
    }

    /**
     * True in case of session cookie.
     */
    public void setSession(Boolean session) {
        this.session = session;
    }

    /**
     * Cookie SameSite type.
     */
    public CookieSameSite getSameSite() {
        return sameSite;
    }

    /**
     * Cookie SameSite type.
     */
    public void setSameSite(CookieSameSite sameSite) {
        this.sameSite = sameSite;
    }

    /**
     * Cookie Priority
     */
    public CookiePriority getPriority() {
        return priority;
    }

    /**
     * Cookie Priority
     */
    public void setPriority(CookiePriority priority) {
        this.priority = priority;
    }

    /**
     * True if cookie is SameParty.
     */
    @Deprecated
    public Boolean isSameParty() {
        return sameParty;
    }

    /**
     * True if cookie is SameParty.
     */
    @Deprecated
    public void setSameParty(Boolean sameParty) {
        this.sameParty = sameParty;
    }

    /**
     * Cookie source scheme type.
     */
    public CookieSourceScheme getSourceScheme() {
        return sourceScheme;
    }

    /**
     * Cookie source scheme type.
     */
    public void setSourceScheme(CookieSourceScheme sourceScheme) {
        this.sourceScheme = sourceScheme;
    }

    /**
     * Cookie source port. Valid values are {-1, [1, 65535]}, -1 indicates an
     * unspecified port. An unspecified port value allows protocol clients to
     * emulate legacy cookie scope for the port. This is a temporary ability and it
     * will be removed in the future.
     */
    public Integer getSourcePort() {
        return sourcePort;
    }

    /**
     * Cookie source port. Valid values are {-1, [1, 65535]}, -1 indicates an
     * unspecified port. An unspecified port value allows protocol clients to
     * emulate legacy cookie scope for the port. This is a temporary ability and it
     * will be removed in the future.
     */
    public void setSourcePort(Integer sourcePort) {
        this.sourcePort = sourcePort;
    }

    /**
     * Cookie partition key.
     */
    public CookiePartitionKey getPartitionKey() {
        return partitionKey;
    }

    /**
     * Cookie partition key.
     */
    public void setPartitionKey(CookiePartitionKey partitionKey) {
        this.partitionKey = partitionKey;
    }

    /**
     * True if cookie partition key is opaque.
     */
    public Boolean isPartitionKeyOpaque() {
        return partitionKeyOpaque;
    }

    /**
     * True if cookie partition key is opaque.
     */
    public void setPartitionKeyOpaque(Boolean partitionKeyOpaque) {
        this.partitionKeyOpaque = partitionKeyOpaque;
    }

    public String toString() {
        return "Cookie [name=" + name + ", value=" + value + ", domain=" + domain + ", path=" + path + ", expires="
                + expires + ", size=" + size + ", httpOnly=" + httpOnly + ", secure=" + secure + ", session=" + session
                + ", sameSite=" + sameSite + ", priority=" + priority + ", sameParty=" + sameParty + ", sourceScheme="
                + sourceScheme + ", sourcePort=" + sourcePort + ", partitionKey=" + partitionKey
                + ", partitionKeyOpaque=" + partitionKeyOpaque + "]";
    }
}
