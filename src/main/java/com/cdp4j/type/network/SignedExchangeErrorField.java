// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Field type for a signed exchange related error.
 */
public enum SignedExchangeErrorField {
    @SerializedName("signatureSig")
    @JsonProperty("signatureSig")
    SignatureSig("signatureSig"),

    @SerializedName("signatureIntegrity")
    @JsonProperty("signatureIntegrity")
    SignatureIntegrity("signatureIntegrity"),

    @SerializedName("signatureCertUrl")
    @JsonProperty("signatureCertUrl")
    SignatureCertUrl("signatureCertUrl"),

    @SerializedName("signatureCertSha256")
    @JsonProperty("signatureCertSha256")
    SignatureCertSha256("signatureCertSha256"),

    @SerializedName("signatureValidityUrl")
    @JsonProperty("signatureValidityUrl")
    SignatureValidityUrl("signatureValidityUrl"),

    @SerializedName("signatureTimestamps")
    @JsonProperty("signatureTimestamps")
    SignatureTimestamps("signatureTimestamps"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SignedExchangeErrorField(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
