// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Represents the source scheme of the origin that originally set the cookie.
 * A value of "Unset" allows protocol clients to emulate legacy cookie scope for the scheme.
 * This is a temporary ability and it will be removed in the future.
 */
public enum CookieSourceScheme {
    @SerializedName("Unset")
    @JsonProperty("Unset")
    Unset("Unset"),

    @SerializedName("NonSecure")
    @JsonProperty("NonSecure")
    NonSecure("NonSecure"),

    @SerializedName("Secure")
    @JsonProperty("Secure")
    Secure("Secure"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookieSourceScheme(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
