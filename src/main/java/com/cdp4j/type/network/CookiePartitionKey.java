// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

/**
 * cookiePartitionKey object The representation of the components of the key
 * that are created by the cookiePartitionKey class contained in
 * net/cookies/cookie_partition_key.h.
 */
@Experimental
public class CookiePartitionKey {
    private String topLevelSite;

    private Boolean hasCrossSiteAncestor;

    public CookiePartitionKey() {}

    public CookiePartitionKey(String topLevelSite) {
        this.topLevelSite = topLevelSite;
    }

    /**
     * The site of the top-level URL the browser was visiting at the start of the
     * request to the endpoint that set the cookie.
     */
    public String getTopLevelSite() {
        return topLevelSite;
    }

    /**
     * The site of the top-level URL the browser was visiting at the start of the
     * request to the endpoint that set the cookie.
     */
    public void setTopLevelSite(String topLevelSite) {
        this.topLevelSite = topLevelSite;
    }

    /**
     * Indicates if the cookie has any ancestors that are cross-site to the
     * topLevelSite.
     */
    public Boolean isHasCrossSiteAncestor() {
        return hasCrossSiteAncestor;
    }

    /**
     * Indicates if the cookie has any ancestors that are cross-site to the
     * topLevelSite.
     */
    public void setHasCrossSiteAncestor(Boolean hasCrossSiteAncestor) {
        this.hasCrossSiteAncestor = hasCrossSiteAncestor;
    }

    public String toString() {
        return "CookiePartitionKey [topLevelSite=" + topLevelSite + ", hasCrossSiteAncestor=" + hasCrossSiteAncestor
                + "]";
    }
}
