// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import static java.util.Collections.emptyMap;

import java.util.Map;

/**
 * WebSocket request data.
 */
public class WebSocketRequest {
    private Map<String, Object> headers = emptyMap();

    /**
     * HTTP request headers.
     */
    public Map<String, Object> getHeaders() {
        return headers;
    }

    /**
     * HTTP request headers.
     */
    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public String toString() {
        return "WebSocketRequest [headers=" + headers + "]";
    }
}
