// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.type.constant.TrustTokenRefreshPolicy;
import java.util.List;

/**
 * Determines what type of Trust Token operation is executed and depending on
 * the type, some additional parameters. The values are specified in
 * third_party/blink/renderer/core/fetch/trust_token.idl.
 */
@Experimental
public class TrustTokenParams {
    private TrustTokenOperationType operation;

    private TrustTokenRefreshPolicy refreshPolicy;

    private List<String> issuers = emptyList();

    public TrustTokenOperationType getOperation() {
        return operation;
    }

    public void setOperation(TrustTokenOperationType operation) {
        this.operation = operation;
    }

    /**
     * Only set for "token-redemption" operation and determine whether to request a
     * fresh SRR or use a still valid cached SRR.
     */
    public TrustTokenRefreshPolicy getRefreshPolicy() {
        return refreshPolicy;
    }

    /**
     * Only set for "token-redemption" operation and determine whether to request a
     * fresh SRR or use a still valid cached SRR.
     */
    public void setRefreshPolicy(TrustTokenRefreshPolicy refreshPolicy) {
        this.refreshPolicy = refreshPolicy;
    }

    /**
     * Origins of issuers from whom to request tokens or redemption records.
     */
    public List<String> getIssuers() {
        return issuers;
    }

    /**
     * Origins of issuers from whom to request tokens or redemption records.
     */
    public void setIssuers(List<String> issuers) {
        this.issuers = issuers;
    }

    public String toString() {
        return "TrustTokenParams [operation=" + operation + ", refreshPolicy=" + refreshPolicy + ", issuers=" + issuers
                + "]";
    }
}
