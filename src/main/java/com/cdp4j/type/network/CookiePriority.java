// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Represents the cookie's 'Priority' status:
 * https://tools.ietf.org/html/draft-west-cookie-priority-00
 */
public enum CookiePriority {
    @SerializedName("Low")
    @JsonProperty("Low")
    Low("Low"),

    @SerializedName("Medium")
    @JsonProperty("Medium")
    Medium("Medium"),

    @SerializedName("High")
    @JsonProperty("High")
    High("High"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookiePriority(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
