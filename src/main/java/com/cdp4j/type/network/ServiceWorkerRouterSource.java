// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Source of service worker router.
 */
public enum ServiceWorkerRouterSource {
    @SerializedName("network")
    @JsonProperty("network")
    Network("network"),

    @SerializedName("cache")
    @JsonProperty("cache")
    Cache("cache"),

    @SerializedName("fetch-event")
    @JsonProperty("fetch-event")
    FetchEvent("fetch-event"),

    @SerializedName("race-network-and-fetch-handler")
    @JsonProperty("race-network-and-fetch-handler")
    RaceNetworkAndFetchHandler("race-network-and-fetch-handler"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ServiceWorkerRouterSource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
