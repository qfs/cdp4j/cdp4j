// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Whether the request complied with Certificate Transparency policy.
 */
public enum CertificateTransparencyCompliance {
    @SerializedName("unknown")
    @JsonProperty("unknown")
    Unknown("unknown"),

    @SerializedName("not-compliant")
    @JsonProperty("not-compliant")
    NotCompliant("not-compliant"),

    @SerializedName("compliant")
    @JsonProperty("compliant")
    Compliant("compliant"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CertificateTransparencyCompliance(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
