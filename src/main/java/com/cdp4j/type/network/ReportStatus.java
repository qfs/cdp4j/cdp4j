// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The status of a Reporting API report.
 */
public enum ReportStatus {
    @SerializedName("Queued")
    @JsonProperty("Queued")
    Queued("Queued"),

    @SerializedName("Pending")
    @JsonProperty("Pending")
    Pending("Pending"),

    @SerializedName("MarkedForRemoval")
    @JsonProperty("MarkedForRemoval")
    MarkedForRemoval("MarkedForRemoval"),

    @SerializedName("Success")
    @JsonProperty("Success")
    Success("Success"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ReportStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
