// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

@Experimental
public class ContentSecurityPolicyStatus {
    private String effectiveDirectives;

    private Boolean isEnforced;

    private ContentSecurityPolicySource source;

    public String getEffectiveDirectives() {
        return effectiveDirectives;
    }

    public void setEffectiveDirectives(String effectiveDirectives) {
        this.effectiveDirectives = effectiveDirectives;
    }

    public Boolean isIsEnforced() {
        return isEnforced;
    }

    public void setIsEnforced(Boolean isEnforced) {
        this.isEnforced = isEnforced;
    }

    public ContentSecurityPolicySource getSource() {
        return source;
    }

    public void setSource(ContentSecurityPolicySource source) {
        this.source = source;
    }

    public String toString() {
        return "ContentSecurityPolicyStatus [effectiveDirectives=" + effectiveDirectives + ", isEnforced=" + isEnforced
                + ", source=" + source + "]";
    }
}
