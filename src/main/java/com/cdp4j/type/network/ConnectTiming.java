// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

@Experimental
public class ConnectTiming {
    private Double requestTime;

    /**
     * Timing's requestTime is a baseline in seconds, while the other numbers are
     * ticks in milliseconds relatively to this requestTime. Matches
     * ResourceTiming's requestTime for the same request (but not for redirected
     * requests).
     */
    public Double getRequestTime() {
        return requestTime;
    }

    /**
     * Timing's requestTime is a baseline in seconds, while the other numbers are
     * ticks in milliseconds relatively to this requestTime. Matches
     * ResourceTiming's requestTime for the same request (but not for redirected
     * requests).
     */
    public void setRequestTime(Double requestTime) {
        this.requestTime = requestTime;
    }

    public String toString() {
        return "ConnectTiming [requestTime=" + requestTime + "]";
    }
}
