// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum TrustTokenOperationType {
    @SerializedName("Issuance")
    @JsonProperty("Issuance")
    Issuance("Issuance"),

    @SerializedName("Redemption")
    @JsonProperty("Redemption")
    Redemption("Redemption"),

    @SerializedName("Signing")
    @JsonProperty("Signing")
    Signing("Signing"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    TrustTokenOperationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
