// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Loading priority of a resource request.
 */
public enum ResourcePriority {
    @SerializedName("VeryLow")
    @JsonProperty("VeryLow")
    VeryLow("VeryLow"),

    @SerializedName("Low")
    @JsonProperty("Low")
    Low("Low"),

    @SerializedName("Medium")
    @JsonProperty("Medium")
    Medium("Medium"),

    @SerializedName("High")
    @JsonProperty("High")
    High("High"),

    @SerializedName("VeryHigh")
    @JsonProperty("VeryHigh")
    VeryHigh("VeryHigh"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ResourcePriority(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
