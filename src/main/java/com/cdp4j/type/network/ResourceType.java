// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Resource type as it was perceived by the rendering engine.
 */
public enum ResourceType {
    @SerializedName("Document")
    @JsonProperty("Document")
    Document("Document"),

    @SerializedName("Stylesheet")
    @JsonProperty("Stylesheet")
    Stylesheet("Stylesheet"),

    @SerializedName("Image")
    @JsonProperty("Image")
    Image("Image"),

    @SerializedName("Media")
    @JsonProperty("Media")
    Media("Media"),

    @SerializedName("Font")
    @JsonProperty("Font")
    Font("Font"),

    @SerializedName("Script")
    @JsonProperty("Script")
    Script("Script"),

    @SerializedName("TextTrack")
    @JsonProperty("TextTrack")
    TextTrack("TextTrack"),

    @SerializedName("XHR")
    @JsonProperty("XHR")
    XHR("XHR"),

    @SerializedName("Fetch")
    @JsonProperty("Fetch")
    Fetch("Fetch"),

    @SerializedName("Prefetch")
    @JsonProperty("Prefetch")
    Prefetch("Prefetch"),

    @SerializedName("EventSource")
    @JsonProperty("EventSource")
    EventSource("EventSource"),

    @SerializedName("WebSocket")
    @JsonProperty("WebSocket")
    WebSocket("WebSocket"),

    @SerializedName("Manifest")
    @JsonProperty("Manifest")
    Manifest("Manifest"),

    @SerializedName("SignedExchange")
    @JsonProperty("SignedExchange")
    SignedExchange("SignedExchange"),

    @SerializedName("Ping")
    @JsonProperty("Ping")
    Ping("Ping"),

    @SerializedName("CSPViolationReport")
    @JsonProperty("CSPViolationReport")
    CSPViolationReport("CSPViolationReport"),

    @SerializedName("Preflight")
    @JsonProperty("Preflight")
    Preflight("Preflight"),

    @SerializedName("Other")
    @JsonProperty("Other")
    Other("Other"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ResourceType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
