// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Stages of the interception to begin intercepting. Request will intercept before the request is
 * sent. Response will intercept after the response is received.
 */
public enum InterceptionStage {
    @SerializedName("Request")
    @JsonProperty("Request")
    Request("Request"),

    @SerializedName("HeadersReceived")
    @JsonProperty("HeadersReceived")
    HeadersReceived("HeadersReceived"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    InterceptionStage(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
