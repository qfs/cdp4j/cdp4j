// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Types of reasons why a cookie may not be sent with a request.
 */
public enum CookieBlockedReason {
    @SerializedName("SecureOnly")
    @JsonProperty("SecureOnly")
    SecureOnly("SecureOnly"),

    @SerializedName("NotOnPath")
    @JsonProperty("NotOnPath")
    NotOnPath("NotOnPath"),

    @SerializedName("DomainMismatch")
    @JsonProperty("DomainMismatch")
    DomainMismatch("DomainMismatch"),

    @SerializedName("SameSiteStrict")
    @JsonProperty("SameSiteStrict")
    SameSiteStrict("SameSiteStrict"),

    @SerializedName("SameSiteLax")
    @JsonProperty("SameSiteLax")
    SameSiteLax("SameSiteLax"),

    @SerializedName("SameSiteUnspecifiedTreatedAsLax")
    @JsonProperty("SameSiteUnspecifiedTreatedAsLax")
    SameSiteUnspecifiedTreatedAsLax("SameSiteUnspecifiedTreatedAsLax"),

    @SerializedName("SameSiteNoneInsecure")
    @JsonProperty("SameSiteNoneInsecure")
    SameSiteNoneInsecure("SameSiteNoneInsecure"),

    @SerializedName("UserPreferences")
    @JsonProperty("UserPreferences")
    UserPreferences("UserPreferences"),

    @SerializedName("ThirdPartyPhaseout")
    @JsonProperty("ThirdPartyPhaseout")
    ThirdPartyPhaseout("ThirdPartyPhaseout"),

    @SerializedName("ThirdPartyBlockedInFirstPartySet")
    @JsonProperty("ThirdPartyBlockedInFirstPartySet")
    ThirdPartyBlockedInFirstPartySet("ThirdPartyBlockedInFirstPartySet"),

    @SerializedName("UnknownError")
    @JsonProperty("UnknownError")
    UnknownError("UnknownError"),

    @SerializedName("SchemefulSameSiteStrict")
    @JsonProperty("SchemefulSameSiteStrict")
    SchemefulSameSiteStrict("SchemefulSameSiteStrict"),

    @SerializedName("SchemefulSameSiteLax")
    @JsonProperty("SchemefulSameSiteLax")
    SchemefulSameSiteLax("SchemefulSameSiteLax"),

    @SerializedName("SchemefulSameSiteUnspecifiedTreatedAsLax")
    @JsonProperty("SchemefulSameSiteUnspecifiedTreatedAsLax")
    SchemefulSameSiteUnspecifiedTreatedAsLax("SchemefulSameSiteUnspecifiedTreatedAsLax"),

    @SerializedName("SamePartyFromCrossPartyContext")
    @JsonProperty("SamePartyFromCrossPartyContext")
    SamePartyFromCrossPartyContext("SamePartyFromCrossPartyContext"),

    @SerializedName("NameValuePairExceedsMaxSize")
    @JsonProperty("NameValuePairExceedsMaxSize")
    NameValuePairExceedsMaxSize("NameValuePairExceedsMaxSize"),

    @SerializedName("PortMismatch")
    @JsonProperty("PortMismatch")
    PortMismatch("PortMismatch"),

    @SerializedName("SchemeMismatch")
    @JsonProperty("SchemeMismatch")
    SchemeMismatch("SchemeMismatch"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookieBlockedReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
