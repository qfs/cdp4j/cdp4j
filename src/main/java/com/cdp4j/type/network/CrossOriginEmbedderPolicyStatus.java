// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

@Experimental
public class CrossOriginEmbedderPolicyStatus {
    private CrossOriginEmbedderPolicyValue value;

    private CrossOriginEmbedderPolicyValue reportOnlyValue;

    private String reportingEndpoint;

    private String reportOnlyReportingEndpoint;

    public CrossOriginEmbedderPolicyValue getValue() {
        return value;
    }

    public void setValue(CrossOriginEmbedderPolicyValue value) {
        this.value = value;
    }

    public CrossOriginEmbedderPolicyValue getReportOnlyValue() {
        return reportOnlyValue;
    }

    public void setReportOnlyValue(CrossOriginEmbedderPolicyValue reportOnlyValue) {
        this.reportOnlyValue = reportOnlyValue;
    }

    public String getReportingEndpoint() {
        return reportingEndpoint;
    }

    public void setReportingEndpoint(String reportingEndpoint) {
        this.reportingEndpoint = reportingEndpoint;
    }

    public String getReportOnlyReportingEndpoint() {
        return reportOnlyReportingEndpoint;
    }

    public void setReportOnlyReportingEndpoint(String reportOnlyReportingEndpoint) {
        this.reportOnlyReportingEndpoint = reportOnlyReportingEndpoint;
    }

    public String toString() {
        return "CrossOriginEmbedderPolicyStatus [value=" + value + ", reportOnlyValue=" + reportOnlyValue
                + ", reportingEndpoint=" + reportingEndpoint + ", reportOnlyReportingEndpoint="
                + reportOnlyReportingEndpoint + "]";
    }
}
