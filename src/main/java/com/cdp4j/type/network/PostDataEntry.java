// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

/**
 * Post data entry for HTTP request
 */
public class PostDataEntry {
    private String bytes;

    public String getBytes() {
        return bytes;
    }

    public void setBytes(String bytes) {
        this.bytes = bytes;
    }

    public String toString() {
        return "PostDataEntry [bytes=" + bytes + "]";
    }
}
