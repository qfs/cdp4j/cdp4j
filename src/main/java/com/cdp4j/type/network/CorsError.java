// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The reason why request was blocked.
 */
public enum CorsError {
    @SerializedName("DisallowedByMode")
    @JsonProperty("DisallowedByMode")
    DisallowedByMode("DisallowedByMode"),

    @SerializedName("InvalidResponse")
    @JsonProperty("InvalidResponse")
    InvalidResponse("InvalidResponse"),

    @SerializedName("WildcardOriginNotAllowed")
    @JsonProperty("WildcardOriginNotAllowed")
    WildcardOriginNotAllowed("WildcardOriginNotAllowed"),

    @SerializedName("MissingAllowOriginHeader")
    @JsonProperty("MissingAllowOriginHeader")
    MissingAllowOriginHeader("MissingAllowOriginHeader"),

    @SerializedName("MultipleAllowOriginValues")
    @JsonProperty("MultipleAllowOriginValues")
    MultipleAllowOriginValues("MultipleAllowOriginValues"),

    @SerializedName("InvalidAllowOriginValue")
    @JsonProperty("InvalidAllowOriginValue")
    InvalidAllowOriginValue("InvalidAllowOriginValue"),

    @SerializedName("AllowOriginMismatch")
    @JsonProperty("AllowOriginMismatch")
    AllowOriginMismatch("AllowOriginMismatch"),

    @SerializedName("InvalidAllowCredentials")
    @JsonProperty("InvalidAllowCredentials")
    InvalidAllowCredentials("InvalidAllowCredentials"),

    @SerializedName("CorsDisabledScheme")
    @JsonProperty("CorsDisabledScheme")
    CorsDisabledScheme("CorsDisabledScheme"),

    @SerializedName("PreflightInvalidStatus")
    @JsonProperty("PreflightInvalidStatus")
    PreflightInvalidStatus("PreflightInvalidStatus"),

    @SerializedName("PreflightDisallowedRedirect")
    @JsonProperty("PreflightDisallowedRedirect")
    PreflightDisallowedRedirect("PreflightDisallowedRedirect"),

    @SerializedName("PreflightWildcardOriginNotAllowed")
    @JsonProperty("PreflightWildcardOriginNotAllowed")
    PreflightWildcardOriginNotAllowed("PreflightWildcardOriginNotAllowed"),

    @SerializedName("PreflightMissingAllowOriginHeader")
    @JsonProperty("PreflightMissingAllowOriginHeader")
    PreflightMissingAllowOriginHeader("PreflightMissingAllowOriginHeader"),

    @SerializedName("PreflightMultipleAllowOriginValues")
    @JsonProperty("PreflightMultipleAllowOriginValues")
    PreflightMultipleAllowOriginValues("PreflightMultipleAllowOriginValues"),

    @SerializedName("PreflightInvalidAllowOriginValue")
    @JsonProperty("PreflightInvalidAllowOriginValue")
    PreflightInvalidAllowOriginValue("PreflightInvalidAllowOriginValue"),

    @SerializedName("PreflightAllowOriginMismatch")
    @JsonProperty("PreflightAllowOriginMismatch")
    PreflightAllowOriginMismatch("PreflightAllowOriginMismatch"),

    @SerializedName("PreflightInvalidAllowCredentials")
    @JsonProperty("PreflightInvalidAllowCredentials")
    PreflightInvalidAllowCredentials("PreflightInvalidAllowCredentials"),

    @SerializedName("PreflightMissingAllowExternal")
    @JsonProperty("PreflightMissingAllowExternal")
    PreflightMissingAllowExternal("PreflightMissingAllowExternal"),

    @SerializedName("PreflightInvalidAllowExternal")
    @JsonProperty("PreflightInvalidAllowExternal")
    PreflightInvalidAllowExternal("PreflightInvalidAllowExternal"),

    @SerializedName("PreflightMissingAllowPrivateNetwork")
    @JsonProperty("PreflightMissingAllowPrivateNetwork")
    PreflightMissingAllowPrivateNetwork("PreflightMissingAllowPrivateNetwork"),

    @SerializedName("PreflightInvalidAllowPrivateNetwork")
    @JsonProperty("PreflightInvalidAllowPrivateNetwork")
    PreflightInvalidAllowPrivateNetwork("PreflightInvalidAllowPrivateNetwork"),

    @SerializedName("InvalidAllowMethodsPreflightResponse")
    @JsonProperty("InvalidAllowMethodsPreflightResponse")
    InvalidAllowMethodsPreflightResponse("InvalidAllowMethodsPreflightResponse"),

    @SerializedName("InvalidAllowHeadersPreflightResponse")
    @JsonProperty("InvalidAllowHeadersPreflightResponse")
    InvalidAllowHeadersPreflightResponse("InvalidAllowHeadersPreflightResponse"),

    @SerializedName("MethodDisallowedByPreflightResponse")
    @JsonProperty("MethodDisallowedByPreflightResponse")
    MethodDisallowedByPreflightResponse("MethodDisallowedByPreflightResponse"),

    @SerializedName("HeaderDisallowedByPreflightResponse")
    @JsonProperty("HeaderDisallowedByPreflightResponse")
    HeaderDisallowedByPreflightResponse("HeaderDisallowedByPreflightResponse"),

    @SerializedName("RedirectContainsCredentials")
    @JsonProperty("RedirectContainsCredentials")
    RedirectContainsCredentials("RedirectContainsCredentials"),

    @SerializedName("InsecurePrivateNetwork")
    @JsonProperty("InsecurePrivateNetwork")
    InsecurePrivateNetwork("InsecurePrivateNetwork"),

    @SerializedName("InvalidPrivateNetworkAccess")
    @JsonProperty("InvalidPrivateNetworkAccess")
    InvalidPrivateNetworkAccess("InvalidPrivateNetworkAccess"),

    @SerializedName("UnexpectedPrivateNetworkAccess")
    @JsonProperty("UnexpectedPrivateNetworkAccess")
    UnexpectedPrivateNetworkAccess("UnexpectedPrivateNetworkAccess"),

    @SerializedName("NoCorsRedirectModeNotFollow")
    @JsonProperty("NoCorsRedirectModeNotFollow")
    NoCorsRedirectModeNotFollow("NoCorsRedirectModeNotFollow"),

    @SerializedName("PreflightMissingPrivateNetworkAccessId")
    @JsonProperty("PreflightMissingPrivateNetworkAccessId")
    PreflightMissingPrivateNetworkAccessId("PreflightMissingPrivateNetworkAccessId"),

    @SerializedName("PreflightMissingPrivateNetworkAccessName")
    @JsonProperty("PreflightMissingPrivateNetworkAccessName")
    PreflightMissingPrivateNetworkAccessName("PreflightMissingPrivateNetworkAccessName"),

    @SerializedName("PrivateNetworkAccessPermissionUnavailable")
    @JsonProperty("PrivateNetworkAccessPermissionUnavailable")
    PrivateNetworkAccessPermissionUnavailable("PrivateNetworkAccessPermissionUnavailable"),

    @SerializedName("PrivateNetworkAccessPermissionDenied")
    @JsonProperty("PrivateNetworkAccessPermissionDenied")
    PrivateNetworkAccessPermissionDenied("PrivateNetworkAccessPermissionDenied"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CorsError(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
