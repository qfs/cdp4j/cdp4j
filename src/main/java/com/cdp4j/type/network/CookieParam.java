// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

/**
 * Cookie parameter object
 */
public class CookieParam {
    private String name;

    private String value;

    private String url;

    private String domain;

    private String path;

    private Boolean secure;

    private Boolean httpOnly;

    private CookieSameSite sameSite;

    private Double expires;

    private CookiePriority priority;

    private Boolean sameParty;

    private CookieSourceScheme sourceScheme;

    private Integer sourcePort;

    private CookiePartitionKey partitionKey;

    /**
     * Cookie name.
     */
    public String getName() {
        return name;
    }

    /**
     * Cookie name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Cookie value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Cookie value.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * The request-URI to associate with the setting of the cookie. This value can
     * affect the default domain, path, source port, and source scheme values of the
     * created cookie.
     */
    public String getUrl() {
        return url;
    }

    /**
     * The request-URI to associate with the setting of the cookie. This value can
     * affect the default domain, path, source port, and source scheme values of the
     * created cookie.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Cookie domain.
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Cookie domain.
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * Cookie path.
     */
    public String getPath() {
        return path;
    }

    /**
     * Cookie path.
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * True if cookie is secure.
     */
    public Boolean isSecure() {
        return secure;
    }

    /**
     * True if cookie is secure.
     */
    public void setSecure(Boolean secure) {
        this.secure = secure;
    }

    /**
     * True if cookie is http-only.
     */
    public Boolean isHttpOnly() {
        return httpOnly;
    }

    /**
     * True if cookie is http-only.
     */
    public void setHttpOnly(Boolean httpOnly) {
        this.httpOnly = httpOnly;
    }

    /**
     * Cookie SameSite type.
     */
    public CookieSameSite getSameSite() {
        return sameSite;
    }

    /**
     * Cookie SameSite type.
     */
    public void setSameSite(CookieSameSite sameSite) {
        this.sameSite = sameSite;
    }

    /**
     * Cookie expiration date, session cookie if not set
     */
    public Double getExpires() {
        return expires;
    }

    /**
     * Cookie expiration date, session cookie if not set
     */
    public void setExpires(Double expires) {
        this.expires = expires;
    }

    /**
     * Cookie Priority.
     */
    public CookiePriority getPriority() {
        return priority;
    }

    /**
     * Cookie Priority.
     */
    public void setPriority(CookiePriority priority) {
        this.priority = priority;
    }

    /**
     * True if cookie is SameParty.
     */
    public Boolean isSameParty() {
        return sameParty;
    }

    /**
     * True if cookie is SameParty.
     */
    public void setSameParty(Boolean sameParty) {
        this.sameParty = sameParty;
    }

    /**
     * Cookie source scheme type.
     */
    public CookieSourceScheme getSourceScheme() {
        return sourceScheme;
    }

    /**
     * Cookie source scheme type.
     */
    public void setSourceScheme(CookieSourceScheme sourceScheme) {
        this.sourceScheme = sourceScheme;
    }

    /**
     * Cookie source port. Valid values are {-1, [1, 65535]}, -1 indicates an
     * unspecified port. An unspecified port value allows protocol clients to
     * emulate legacy cookie scope for the port. This is a temporary ability and it
     * will be removed in the future.
     */
    public Integer getSourcePort() {
        return sourcePort;
    }

    /**
     * Cookie source port. Valid values are {-1, [1, 65535]}, -1 indicates an
     * unspecified port. An unspecified port value allows protocol clients to
     * emulate legacy cookie scope for the port. This is a temporary ability and it
     * will be removed in the future.
     */
    public void setSourcePort(Integer sourcePort) {
        this.sourcePort = sourcePort;
    }

    /**
     * Cookie partition key. If not set, the cookie will be set as not partitioned.
     */
    public CookiePartitionKey getPartitionKey() {
        return partitionKey;
    }

    /**
     * Cookie partition key. If not set, the cookie will be set as not partitioned.
     */
    public void setPartitionKey(CookiePartitionKey partitionKey) {
        this.partitionKey = partitionKey;
    }

    public String toString() {
        return "CookieParam [name=" + name + ", value=" + value + ", url=" + url + ", domain=" + domain + ", path="
                + path + ", secure=" + secure + ", httpOnly=" + httpOnly + ", sameSite=" + sameSite + ", expires="
                + expires + ", priority=" + priority + ", sameParty=" + sameParty + ", sourceScheme=" + sourceScheme
                + ", sourcePort=" + sourcePort + ", partitionKey=" + partitionKey + "]";
    }
}
