// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

@Experimental
public class ReportingApiEndpoint {
    private String url;

    private String groupName;

    /**
     * The URL of the endpoint to which reports may be delivered.
     */
    public String getUrl() {
        return url;
    }

    /**
     * The URL of the endpoint to which reports may be delivered.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Name of the endpoint group.
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Name of the endpoint group.
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String toString() {
        return "ReportingApiEndpoint [url=" + url + ", groupName=" + groupName + "]";
    }
}
