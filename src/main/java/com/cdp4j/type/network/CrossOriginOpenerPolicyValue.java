// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum CrossOriginOpenerPolicyValue {
    @SerializedName("SameOrigin")
    @JsonProperty("SameOrigin")
    SameOrigin("SameOrigin"),

    @SerializedName("SameOriginAllowPopups")
    @JsonProperty("SameOriginAllowPopups")
    SameOriginAllowPopups("SameOriginAllowPopups"),

    @SerializedName("RestrictProperties")
    @JsonProperty("RestrictProperties")
    RestrictProperties("RestrictProperties"),

    @SerializedName("UnsafeNone")
    @JsonProperty("UnsafeNone")
    UnsafeNone("UnsafeNone"),

    @SerializedName("SameOriginPlusCoep")
    @JsonProperty("SameOriginPlusCoep")
    SameOriginPlusCoep("SameOriginPlusCoep"),

    @SerializedName("RestrictPropertiesPlusCoep")
    @JsonProperty("RestrictPropertiesPlusCoep")
    RestrictPropertiesPlusCoep("RestrictPropertiesPlusCoep"),

    @SerializedName("NoopenerAllowPopups")
    @JsonProperty("NoopenerAllowPopups")
    NoopenerAllowPopups("NoopenerAllowPopups"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CrossOriginOpenerPolicyValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
