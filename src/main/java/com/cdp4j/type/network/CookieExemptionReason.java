// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Types of reasons why a cookie should have been blocked by 3PCD but is exempted for the request.
 */
public enum CookieExemptionReason {
    @SerializedName("None")
    @JsonProperty("None")
    None("None"),

    @SerializedName("UserSetting")
    @JsonProperty("UserSetting")
    UserSetting("UserSetting"),

    @SerializedName("TPCDMetadata")
    @JsonProperty("TPCDMetadata")
    TPCDMetadata("TPCDMetadata"),

    @SerializedName("TPCDDeprecationTrial")
    @JsonProperty("TPCDDeprecationTrial")
    TPCDDeprecationTrial("TPCDDeprecationTrial"),

    @SerializedName("TopLevelTPCDDeprecationTrial")
    @JsonProperty("TopLevelTPCDDeprecationTrial")
    TopLevelTPCDDeprecationTrial("TopLevelTPCDDeprecationTrial"),

    @SerializedName("TPCDHeuristics")
    @JsonProperty("TPCDHeuristics")
    TPCDHeuristics("TPCDHeuristics"),

    @SerializedName("EnterprisePolicy")
    @JsonProperty("EnterprisePolicy")
    EnterprisePolicy("EnterprisePolicy"),

    @SerializedName("StorageAccess")
    @JsonProperty("StorageAccess")
    StorageAccess("StorageAccess"),

    @SerializedName("TopLevelStorageAccess")
    @JsonProperty("TopLevelStorageAccess")
    TopLevelStorageAccess("TopLevelStorageAccess"),

    @SerializedName("Scheme")
    @JsonProperty("Scheme")
    Scheme("Scheme"),

    @SerializedName("SameSiteNoneCookiesInSandbox")
    @JsonProperty("SameSiteNoneCookiesInSandbox")
    SameSiteNoneCookiesInSandbox("SameSiteNoneCookiesInSandbox"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookieExemptionReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
