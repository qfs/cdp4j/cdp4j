// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class SecurityIsolationStatus {
    private CrossOriginOpenerPolicyStatus coop;

    private CrossOriginEmbedderPolicyStatus coep;

    private List<ContentSecurityPolicyStatus> csp = emptyList();

    public CrossOriginOpenerPolicyStatus getCoop() {
        return coop;
    }

    public void setCoop(CrossOriginOpenerPolicyStatus coop) {
        this.coop = coop;
    }

    public CrossOriginEmbedderPolicyStatus getCoep() {
        return coep;
    }

    public void setCoep(CrossOriginEmbedderPolicyStatus coep) {
        this.coep = coep;
    }

    public List<ContentSecurityPolicyStatus> getCsp() {
        return csp;
    }

    public void setCsp(List<ContentSecurityPolicyStatus> csp) {
        this.csp = csp;
    }

    public String toString() {
        return "SecurityIsolationStatus [coop=" + coop + ", coep=" + coep + ", csp=" + csp + "]";
    }
}
