// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

@Experimental
public class ClientSecurityState {
    private Boolean initiatorIsSecureContext;

    private IPAddressSpace initiatorIPAddressSpace;

    private PrivateNetworkRequestPolicy privateNetworkRequestPolicy;

    public Boolean isInitiatorIsSecureContext() {
        return initiatorIsSecureContext;
    }

    public void setInitiatorIsSecureContext(Boolean initiatorIsSecureContext) {
        this.initiatorIsSecureContext = initiatorIsSecureContext;
    }

    public IPAddressSpace getInitiatorIPAddressSpace() {
        return initiatorIPAddressSpace;
    }

    public void setInitiatorIPAddressSpace(IPAddressSpace initiatorIPAddressSpace) {
        this.initiatorIPAddressSpace = initiatorIPAddressSpace;
    }

    public PrivateNetworkRequestPolicy getPrivateNetworkRequestPolicy() {
        return privateNetworkRequestPolicy;
    }

    public void setPrivateNetworkRequestPolicy(PrivateNetworkRequestPolicy privateNetworkRequestPolicy) {
        this.privateNetworkRequestPolicy = privateNetworkRequestPolicy;
    }

    public String toString() {
        return "ClientSecurityState [initiatorIsSecureContext=" + initiatorIsSecureContext
                + ", initiatorIPAddressSpace=" + initiatorIPAddressSpace + ", privateNetworkRequestPolicy="
                + privateNetworkRequestPolicy + "]";
    }
}
