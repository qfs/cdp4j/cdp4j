// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum CrossOriginEmbedderPolicyValue {
    @SerializedName("None")
    @JsonProperty("None")
    None("None"),

    @SerializedName("Credentialless")
    @JsonProperty("Credentialless")
    Credentialless("Credentialless"),

    @SerializedName("RequireCorp")
    @JsonProperty("RequireCorp")
    RequireCorp("RequireCorp"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CrossOriginEmbedderPolicyValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
