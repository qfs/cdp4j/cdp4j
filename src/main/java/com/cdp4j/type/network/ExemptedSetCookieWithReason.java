// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

/**
 * A cookie should have been blocked by 3PCD but is exempted and stored from a
 * response with the corresponding reason. A cookie could only have at most one
 * exemption reason.
 */
@Experimental
public class ExemptedSetCookieWithReason {
    private CookieExemptionReason exemptionReason;

    private String cookieLine;

    private Cookie cookie;

    /**
     * The reason the cookie was exempted.
     */
    public CookieExemptionReason getExemptionReason() {
        return exemptionReason;
    }

    /**
     * The reason the cookie was exempted.
     */
    public void setExemptionReason(CookieExemptionReason exemptionReason) {
        this.exemptionReason = exemptionReason;
    }

    /**
     * The string representing this individual cookie as it would appear in the
     * header.
     */
    public String getCookieLine() {
        return cookieLine;
    }

    /**
     * The string representing this individual cookie as it would appear in the
     * header.
     */
    public void setCookieLine(String cookieLine) {
        this.cookieLine = cookieLine;
    }

    /**
     * The cookie object representing the cookie.
     */
    public Cookie getCookie() {
        return cookie;
    }

    /**
     * The cookie object representing the cookie.
     */
    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }

    public String toString() {
        return "ExemptedSetCookieWithReason [exemptionReason=" + exemptionReason + ", cookieLine=" + cookieLine
                + ", cookie=" + cookie + "]";
    }
}
