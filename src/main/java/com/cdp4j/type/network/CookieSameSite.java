// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Represents the cookie's 'SameSite' status:
 * https://tools.ietf.org/html/draft-west-first-party-cookies
 */
public enum CookieSameSite {
    @SerializedName("Strict")
    @JsonProperty("Strict")
    Strict("Strict"),

    @SerializedName("Lax")
    @JsonProperty("Lax")
    Lax("Lax"),

    @SerializedName("None")
    @JsonProperty("None")
    None("None"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookieSameSite(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
