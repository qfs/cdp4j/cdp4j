// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Source of serviceworker response.
 */
public enum ServiceWorkerResponseSource {
    @SerializedName("cache-storage")
    @JsonProperty("cache-storage")
    CacheStorage("cache-storage"),

    @SerializedName("http-cache")
    @JsonProperty("http-cache")
    HttpCache("http-cache"),

    @SerializedName("fallback-code")
    @JsonProperty("fallback-code")
    FallbackCode("fallback-code"),

    @SerializedName("network")
    @JsonProperty("network")
    Network("network"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ServiceWorkerResponseSource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
