// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

/**
 * A cookie associated with the request which may or may not be sent with it.
 * Includes the cookies itself and reasons for blocking or exemption.
 */
@Experimental
public class AssociatedCookie {
    private Cookie cookie;

    private List<CookieBlockedReason> blockedReasons = emptyList();

    private CookieExemptionReason exemptionReason;

    /**
     * The cookie object representing the cookie which was not sent.
     */
    public Cookie getCookie() {
        return cookie;
    }

    /**
     * The cookie object representing the cookie which was not sent.
     */
    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }

    /**
     * The reason(s) the cookie was blocked. If empty means the cookie is included.
     */
    public List<CookieBlockedReason> getBlockedReasons() {
        return blockedReasons;
    }

    /**
     * The reason(s) the cookie was blocked. If empty means the cookie is included.
     */
    public void setBlockedReasons(List<CookieBlockedReason> blockedReasons) {
        this.blockedReasons = blockedReasons;
    }

    /**
     * The reason the cookie should have been blocked by 3PCD but is exempted. A
     * cookie could only have at most one exemption reason.
     */
    public CookieExemptionReason getExemptionReason() {
        return exemptionReason;
    }

    /**
     * The reason the cookie should have been blocked by 3PCD but is exempted. A
     * cookie could only have at most one exemption reason.
     */
    public void setExemptionReason(CookieExemptionReason exemptionReason) {
        this.exemptionReason = exemptionReason;
    }

    public String toString() {
        return "AssociatedCookie [cookie=" + cookie + ", blockedReasons=" + blockedReasons + ", exemptionReason="
                + exemptionReason + "]";
    }
}
