// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The reason why Chrome uses a specific transport protocol for HTTP semantics.
 */
public enum AlternateProtocolUsage {
    @SerializedName("alternativeJobWonWithoutRace")
    @JsonProperty("alternativeJobWonWithoutRace")
    AlternativeJobWonWithoutRace("alternativeJobWonWithoutRace"),

    @SerializedName("alternativeJobWonRace")
    @JsonProperty("alternativeJobWonRace")
    AlternativeJobWonRace("alternativeJobWonRace"),

    @SerializedName("mainJobWonRace")
    @JsonProperty("mainJobWonRace")
    MainJobWonRace("mainJobWonRace"),

    @SerializedName("mappingMissing")
    @JsonProperty("mappingMissing")
    MappingMissing("mappingMissing"),

    @SerializedName("broken")
    @JsonProperty("broken")
    Broken("broken"),

    @SerializedName("dnsAlpnH3JobWonWithoutRace")
    @JsonProperty("dnsAlpnH3JobWonWithoutRace")
    DnsAlpnH3JobWonWithoutRace("dnsAlpnH3JobWonWithoutRace"),

    @SerializedName("dnsAlpnH3JobWonRace")
    @JsonProperty("dnsAlpnH3JobWonRace")
    DnsAlpnH3JobWonRace("dnsAlpnH3JobWonRace"),

    @SerializedName("unspecifiedReason")
    @JsonProperty("unspecifiedReason")
    UnspecifiedReason("unspecifiedReason"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AlternateProtocolUsage(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
