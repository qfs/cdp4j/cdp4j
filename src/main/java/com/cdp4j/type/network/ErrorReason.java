// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Network level fetch failure reason.
 */
public enum ErrorReason {
    @SerializedName("Failed")
    @JsonProperty("Failed")
    Failed("Failed"),

    @SerializedName("Aborted")
    @JsonProperty("Aborted")
    Aborted("Aborted"),

    @SerializedName("TimedOut")
    @JsonProperty("TimedOut")
    TimedOut("TimedOut"),

    @SerializedName("AccessDenied")
    @JsonProperty("AccessDenied")
    AccessDenied("AccessDenied"),

    @SerializedName("ConnectionClosed")
    @JsonProperty("ConnectionClosed")
    ConnectionClosed("ConnectionClosed"),

    @SerializedName("ConnectionReset")
    @JsonProperty("ConnectionReset")
    ConnectionReset("ConnectionReset"),

    @SerializedName("ConnectionRefused")
    @JsonProperty("ConnectionRefused")
    ConnectionRefused("ConnectionRefused"),

    @SerializedName("ConnectionAborted")
    @JsonProperty("ConnectionAborted")
    ConnectionAborted("ConnectionAborted"),

    @SerializedName("ConnectionFailed")
    @JsonProperty("ConnectionFailed")
    ConnectionFailed("ConnectionFailed"),

    @SerializedName("NameNotResolved")
    @JsonProperty("NameNotResolved")
    NameNotResolved("NameNotResolved"),

    @SerializedName("InternetDisconnected")
    @JsonProperty("InternetDisconnected")
    InternetDisconnected("InternetDisconnected"),

    @SerializedName("AddressUnreachable")
    @JsonProperty("AddressUnreachable")
    AddressUnreachable("AddressUnreachable"),

    @SerializedName("BlockedByClient")
    @JsonProperty("BlockedByClient")
    BlockedByClient("BlockedByClient"),

    @SerializedName("BlockedByResponse")
    @JsonProperty("BlockedByResponse")
    BlockedByResponse("BlockedByResponse"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ErrorReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
