// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The underlying connection technology that the browser is supposedly using.
 */
public enum ConnectionType {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("cellular2g")
    @JsonProperty("cellular2g")
    Cellular2g("cellular2g"),

    @SerializedName("cellular3g")
    @JsonProperty("cellular3g")
    Cellular3g("cellular3g"),

    @SerializedName("cellular4g")
    @JsonProperty("cellular4g")
    Cellular4g("cellular4g"),

    @SerializedName("bluetooth")
    @JsonProperty("bluetooth")
    Bluetooth("bluetooth"),

    @SerializedName("ethernet")
    @JsonProperty("ethernet")
    Ethernet("ethernet"),

    @SerializedName("wifi")
    @JsonProperty("wifi")
    Wifi("wifi"),

    @SerializedName("wimax")
    @JsonProperty("wimax")
    Wimax("wimax"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ConnectionType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
