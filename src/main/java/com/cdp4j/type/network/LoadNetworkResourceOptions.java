// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

/**
 * An options object that may be extended later to better support CORS, CORB and
 * streaming.
 */
@Experimental
public class LoadNetworkResourceOptions {
    private Boolean disableCache;

    private Boolean includeCredentials;

    public Boolean isDisableCache() {
        return disableCache;
    }

    public void setDisableCache(Boolean disableCache) {
        this.disableCache = disableCache;
    }

    public Boolean isIncludeCredentials() {
        return includeCredentials;
    }

    public void setIncludeCredentials(Boolean includeCredentials) {
        this.includeCredentials = includeCredentials;
    }

    public String toString() {
        return "LoadNetworkResourceOptions [disableCache=" + disableCache + ", includeCredentials=" + includeCredentials
                + "]";
    }
}
