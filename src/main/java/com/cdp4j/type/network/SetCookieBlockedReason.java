// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Types of reasons why a cookie may not be stored from a response.
 */
public enum SetCookieBlockedReason {
    @SerializedName("SecureOnly")
    @JsonProperty("SecureOnly")
    SecureOnly("SecureOnly"),

    @SerializedName("SameSiteStrict")
    @JsonProperty("SameSiteStrict")
    SameSiteStrict("SameSiteStrict"),

    @SerializedName("SameSiteLax")
    @JsonProperty("SameSiteLax")
    SameSiteLax("SameSiteLax"),

    @SerializedName("SameSiteUnspecifiedTreatedAsLax")
    @JsonProperty("SameSiteUnspecifiedTreatedAsLax")
    SameSiteUnspecifiedTreatedAsLax("SameSiteUnspecifiedTreatedAsLax"),

    @SerializedName("SameSiteNoneInsecure")
    @JsonProperty("SameSiteNoneInsecure")
    SameSiteNoneInsecure("SameSiteNoneInsecure"),

    @SerializedName("UserPreferences")
    @JsonProperty("UserPreferences")
    UserPreferences("UserPreferences"),

    @SerializedName("ThirdPartyPhaseout")
    @JsonProperty("ThirdPartyPhaseout")
    ThirdPartyPhaseout("ThirdPartyPhaseout"),

    @SerializedName("ThirdPartyBlockedInFirstPartySet")
    @JsonProperty("ThirdPartyBlockedInFirstPartySet")
    ThirdPartyBlockedInFirstPartySet("ThirdPartyBlockedInFirstPartySet"),

    @SerializedName("SyntaxError")
    @JsonProperty("SyntaxError")
    SyntaxError("SyntaxError"),

    @SerializedName("SchemeNotSupported")
    @JsonProperty("SchemeNotSupported")
    SchemeNotSupported("SchemeNotSupported"),

    @SerializedName("OverwriteSecure")
    @JsonProperty("OverwriteSecure")
    OverwriteSecure("OverwriteSecure"),

    @SerializedName("InvalidDomain")
    @JsonProperty("InvalidDomain")
    InvalidDomain("InvalidDomain"),

    @SerializedName("InvalidPrefix")
    @JsonProperty("InvalidPrefix")
    InvalidPrefix("InvalidPrefix"),

    @SerializedName("UnknownError")
    @JsonProperty("UnknownError")
    UnknownError("UnknownError"),

    @SerializedName("SchemefulSameSiteStrict")
    @JsonProperty("SchemefulSameSiteStrict")
    SchemefulSameSiteStrict("SchemefulSameSiteStrict"),

    @SerializedName("SchemefulSameSiteLax")
    @JsonProperty("SchemefulSameSiteLax")
    SchemefulSameSiteLax("SchemefulSameSiteLax"),

    @SerializedName("SchemefulSameSiteUnspecifiedTreatedAsLax")
    @JsonProperty("SchemefulSameSiteUnspecifiedTreatedAsLax")
    SchemefulSameSiteUnspecifiedTreatedAsLax("SchemefulSameSiteUnspecifiedTreatedAsLax"),

    @SerializedName("SamePartyFromCrossPartyContext")
    @JsonProperty("SamePartyFromCrossPartyContext")
    SamePartyFromCrossPartyContext("SamePartyFromCrossPartyContext"),

    @SerializedName("SamePartyConflictsWithOtherAttributes")
    @JsonProperty("SamePartyConflictsWithOtherAttributes")
    SamePartyConflictsWithOtherAttributes("SamePartyConflictsWithOtherAttributes"),

    @SerializedName("NameValuePairExceedsMaxSize")
    @JsonProperty("NameValuePairExceedsMaxSize")
    NameValuePairExceedsMaxSize("NameValuePairExceedsMaxSize"),

    @SerializedName("DisallowedCharacter")
    @JsonProperty("DisallowedCharacter")
    DisallowedCharacter("DisallowedCharacter"),

    @SerializedName("NoCookieContent")
    @JsonProperty("NoCookieContent")
    NoCookieContent("NoCookieContent"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SetCookieBlockedReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
