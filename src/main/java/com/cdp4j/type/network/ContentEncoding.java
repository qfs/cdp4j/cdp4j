// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * List of content encodings supported by the backend.
 */
public enum ContentEncoding {
    @SerializedName("deflate")
    @JsonProperty("deflate")
    Deflate("deflate"),

    @SerializedName("gzip")
    @JsonProperty("gzip")
    Gzip("gzip"),

    @SerializedName("br")
    @JsonProperty("br")
    Br("br"),

    @SerializedName("zstd")
    @JsonProperty("zstd")
    Zstd("zstd"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ContentEncoding(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
