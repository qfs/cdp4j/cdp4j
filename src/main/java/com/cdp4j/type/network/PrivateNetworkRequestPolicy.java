// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PrivateNetworkRequestPolicy {
    @SerializedName("Allow")
    @JsonProperty("Allow")
    Allow("Allow"),

    @SerializedName("BlockFromInsecureToMorePrivate")
    @JsonProperty("BlockFromInsecureToMorePrivate")
    BlockFromInsecureToMorePrivate("BlockFromInsecureToMorePrivate"),

    @SerializedName("WarnFromInsecureToMorePrivate")
    @JsonProperty("WarnFromInsecureToMorePrivate")
    WarnFromInsecureToMorePrivate("WarnFromInsecureToMorePrivate"),

    @SerializedName("PreflightBlock")
    @JsonProperty("PreflightBlock")
    PreflightBlock("PreflightBlock"),

    @SerializedName("PreflightWarn")
    @JsonProperty("PreflightWarn")
    PreflightWarn("PreflightWarn"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PrivateNetworkRequestPolicy(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
