// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ContentSecurityPolicySource {
    @SerializedName("HTTP")
    @JsonProperty("HTTP")
    HTTP("HTTP"),

    @SerializedName("Meta")
    @JsonProperty("Meta")
    Meta("Meta"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ContentSecurityPolicySource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
