// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.cdp4j.annotation.Experimental;

@Experimental
public class CrossOriginOpenerPolicyStatus {
    private CrossOriginOpenerPolicyValue value;

    private CrossOriginOpenerPolicyValue reportOnlyValue;

    private String reportingEndpoint;

    private String reportOnlyReportingEndpoint;

    public CrossOriginOpenerPolicyValue getValue() {
        return value;
    }

    public void setValue(CrossOriginOpenerPolicyValue value) {
        this.value = value;
    }

    public CrossOriginOpenerPolicyValue getReportOnlyValue() {
        return reportOnlyValue;
    }

    public void setReportOnlyValue(CrossOriginOpenerPolicyValue reportOnlyValue) {
        this.reportOnlyValue = reportOnlyValue;
    }

    public String getReportingEndpoint() {
        return reportingEndpoint;
    }

    public void setReportingEndpoint(String reportingEndpoint) {
        this.reportingEndpoint = reportingEndpoint;
    }

    public String getReportOnlyReportingEndpoint() {
        return reportOnlyReportingEndpoint;
    }

    public void setReportOnlyReportingEndpoint(String reportOnlyReportingEndpoint) {
        this.reportOnlyReportingEndpoint = reportOnlyReportingEndpoint;
    }

    public String toString() {
        return "CrossOriginOpenerPolicyStatus [value=" + value + ", reportOnlyValue=" + reportOnlyValue
                + ", reportingEndpoint=" + reportingEndpoint + ", reportOnlyReportingEndpoint="
                + reportOnlyReportingEndpoint + "]";
    }
}
