// SPDX-License-Identifier: MIT
package com.cdp4j.type.network;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The reason why request was blocked.
 */
public enum BlockedReason {
    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    @SerializedName("csp")
    @JsonProperty("csp")
    Csp("csp"),

    @SerializedName("mixed-content")
    @JsonProperty("mixed-content")
    MixedContent("mixed-content"),

    @SerializedName("origin")
    @JsonProperty("origin")
    Origin("origin"),

    @SerializedName("inspector")
    @JsonProperty("inspector")
    Inspector("inspector"),

    @SerializedName("subresource-filter")
    @JsonProperty("subresource-filter")
    SubresourceFilter("subresource-filter"),

    @SerializedName("content-type")
    @JsonProperty("content-type")
    ContentType("content-type"),

    @SerializedName("coep-frame-resource-needs-coep-header")
    @JsonProperty("coep-frame-resource-needs-coep-header")
    CoepFrameResourceNeedsCoepHeader("coep-frame-resource-needs-coep-header"),

    @SerializedName("coop-sandboxed-iframe-cannot-navigate-to-coop-page")
    @JsonProperty("coop-sandboxed-iframe-cannot-navigate-to-coop-page")
    CoopSandboxedIframeCannotNavigateToCoopPage("coop-sandboxed-iframe-cannot-navigate-to-coop-page"),

    @SerializedName("corp-not-same-origin")
    @JsonProperty("corp-not-same-origin")
    CorpNotSameOrigin("corp-not-same-origin"),

    @SerializedName("corp-not-same-origin-after-defaulted-to-same-origin-by-coep")
    @JsonProperty("corp-not-same-origin-after-defaulted-to-same-origin-by-coep")
    CorpNotSameOriginAfterDefaultedToSameOriginByCoep("corp-not-same-origin-after-defaulted-to-same-origin-by-coep"),

    @SerializedName("corp-not-same-origin-after-defaulted-to-same-origin-by-dip")
    @JsonProperty("corp-not-same-origin-after-defaulted-to-same-origin-by-dip")
    CorpNotSameOriginAfterDefaultedToSameOriginByDip("corp-not-same-origin-after-defaulted-to-same-origin-by-dip"),

    @SerializedName("corp-not-same-origin-after-defaulted-to-same-origin-by-coep-and-dip")
    @JsonProperty("corp-not-same-origin-after-defaulted-to-same-origin-by-coep-and-dip")
    CorpNotSameOriginAfterDefaultedToSameOriginByCoepAndDip(
            "corp-not-same-origin-after-defaulted-to-same-origin-by-coep-and-dip"),

    @SerializedName("corp-not-same-site")
    @JsonProperty("corp-not-same-site")
    CorpNotSameSite("corp-not-same-site"),

    @SerializedName("sri-message-signature-mismatch")
    @JsonProperty("sri-message-signature-mismatch")
    SriMessageSignatureMismatch("sri-message-signature-mismatch"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    BlockedReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
