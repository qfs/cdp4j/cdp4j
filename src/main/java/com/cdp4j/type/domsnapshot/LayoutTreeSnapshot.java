// SPDX-License-Identifier: MIT
package com.cdp4j.type.domsnapshot;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Table of details of an element in the DOM tree with a LayoutObject.
 */
public class LayoutTreeSnapshot {
    private List<Integer> nodeIndex = emptyList();

    private List<List<Integer>> styles = emptyList();

    private List<List<Double>> bounds = emptyList();

    private List<Integer> text = emptyList();

    private RareBooleanData stackingContexts;

    private List<Integer> paintOrders = emptyList();

    private List<List<Double>> offsetRects = emptyList();

    private List<List<Double>> scrollRects = emptyList();

    private List<List<Double>> clientRects = emptyList();

    private List<Integer> blendedBackgroundColors = emptyList();

    private List<Double> textColorOpacities = emptyList();

    /**
     * Index of the corresponding node in the NodeTreeSnapshot array returned by
     * captureSnapshot.
     */
    public List<Integer> getNodeIndex() {
        return nodeIndex;
    }

    /**
     * Index of the corresponding node in the NodeTreeSnapshot array returned by
     * captureSnapshot.
     */
    public void setNodeIndex(List<Integer> nodeIndex) {
        this.nodeIndex = nodeIndex;
    }

    /**
     * Array of indexes specifying computed style strings, filtered according to the
     * computedStyles parameter passed to captureSnapshot.
     */
    public List<List<Integer>> getStyles() {
        return styles;
    }

    /**
     * Array of indexes specifying computed style strings, filtered according to the
     * computedStyles parameter passed to captureSnapshot.
     */
    public void setStyles(List<List<Integer>> styles) {
        this.styles = styles;
    }

    /**
     * The absolute position bounding box.
     */
    public List<List<Double>> getBounds() {
        return bounds;
    }

    /**
     * The absolute position bounding box.
     */
    public void setBounds(List<List<Double>> bounds) {
        this.bounds = bounds;
    }

    /**
     * Contents of the LayoutText, if any.
     */
    public List<Integer> getText() {
        return text;
    }

    /**
     * Contents of the LayoutText, if any.
     */
    public void setText(List<Integer> text) {
        this.text = text;
    }

    /**
     * Stacking context information.
     */
    public RareBooleanData getStackingContexts() {
        return stackingContexts;
    }

    /**
     * Stacking context information.
     */
    public void setStackingContexts(RareBooleanData stackingContexts) {
        this.stackingContexts = stackingContexts;
    }

    /**
     * Global paint order index, which is determined by the stacking order of the
     * nodes. Nodes that are painted together will have the same index. Only
     * provided if includePaintOrder in captureSnapshot was true.
     */
    public List<Integer> getPaintOrders() {
        return paintOrders;
    }

    /**
     * Global paint order index, which is determined by the stacking order of the
     * nodes. Nodes that are painted together will have the same index. Only
     * provided if includePaintOrder in captureSnapshot was true.
     */
    public void setPaintOrders(List<Integer> paintOrders) {
        this.paintOrders = paintOrders;
    }

    /**
     * The offset rect of nodes. Only available when includeDOMRects is set to true
     */
    public List<List<Double>> getOffsetRects() {
        return offsetRects;
    }

    /**
     * The offset rect of nodes. Only available when includeDOMRects is set to true
     */
    public void setOffsetRects(List<List<Double>> offsetRects) {
        this.offsetRects = offsetRects;
    }

    /**
     * The scroll rect of nodes. Only available when includeDOMRects is set to true
     */
    public List<List<Double>> getScrollRects() {
        return scrollRects;
    }

    /**
     * The scroll rect of nodes. Only available when includeDOMRects is set to true
     */
    public void setScrollRects(List<List<Double>> scrollRects) {
        this.scrollRects = scrollRects;
    }

    /**
     * The client rect of nodes. Only available when includeDOMRects is set to true
     */
    public List<List<Double>> getClientRects() {
        return clientRects;
    }

    /**
     * The client rect of nodes. Only available when includeDOMRects is set to true
     */
    public void setClientRects(List<List<Double>> clientRects) {
        this.clientRects = clientRects;
    }

    /**
     * The list of background colors that are blended with colors of overlapping
     * elements.
     */
    public List<Integer> getBlendedBackgroundColors() {
        return blendedBackgroundColors;
    }

    /**
     * The list of background colors that are blended with colors of overlapping
     * elements.
     */
    public void setBlendedBackgroundColors(List<Integer> blendedBackgroundColors) {
        this.blendedBackgroundColors = blendedBackgroundColors;
    }

    /**
     * The list of computed text opacities.
     */
    public List<Double> getTextColorOpacities() {
        return textColorOpacities;
    }

    /**
     * The list of computed text opacities.
     */
    public void setTextColorOpacities(List<Double> textColorOpacities) {
        this.textColorOpacities = textColorOpacities;
    }

    public String toString() {
        return "LayoutTreeSnapshot [nodeIndex=" + nodeIndex + ", styles=" + styles + ", bounds=" + bounds + ", text="
                + text + ", stackingContexts=" + stackingContexts + ", paintOrders=" + paintOrders + ", offsetRects="
                + offsetRects + ", scrollRects=" + scrollRects + ", clientRects=" + clientRects
                + ", blendedBackgroundColors=" + blendedBackgroundColors + ", textColorOpacities=" + textColorOpacities
                + "]";
    }
}
