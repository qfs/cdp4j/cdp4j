// SPDX-License-Identifier: MIT
package com.cdp4j.type.domsnapshot;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * A subset of the full ComputedStyle as defined by the request whitelist.
 */
public class ComputedStyle {
    private List<NameValue> properties = emptyList();

    /**
     * Name/value pairs of computed style properties.
     */
    public List<NameValue> getProperties() {
        return properties;
    }

    /**
     * Name/value pairs of computed style properties.
     */
    public void setProperties(List<NameValue> properties) {
        this.properties = properties;
    }

    public String toString() {
        return "ComputedStyle [properties=" + properties + "]";
    }
}
