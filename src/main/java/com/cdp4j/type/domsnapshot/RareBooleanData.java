// SPDX-License-Identifier: MIT
package com.cdp4j.type.domsnapshot;

import static java.util.Collections.emptyList;

import java.util.List;

public class RareBooleanData {
    private List<Integer> index = emptyList();

    public List<Integer> getIndex() {
        return index;
    }

    public void setIndex(List<Integer> index) {
        this.index = index;
    }

    public String toString() {
        return "RareBooleanData [index=" + index + "]";
    }
}
