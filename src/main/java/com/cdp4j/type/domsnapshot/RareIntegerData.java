// SPDX-License-Identifier: MIT
package com.cdp4j.type.domsnapshot;

import static java.util.Collections.emptyList;

import java.util.List;

public class RareIntegerData {
    private List<Integer> index = emptyList();

    private List<Integer> value = emptyList();

    public List<Integer> getIndex() {
        return index;
    }

    public void setIndex(List<Integer> index) {
        this.index = index;
    }

    public List<Integer> getValue() {
        return value;
    }

    public void setValue(List<Integer> value) {
        this.value = value;
    }

    public String toString() {
        return "RareIntegerData [index=" + index + ", value=" + value + "]";
    }
}
