// SPDX-License-Identifier: MIT
package com.cdp4j.type.webauthn;

public class Credential {
    private String credentialId;

    private Boolean isResidentCredential;

    private String rpId;

    private String privateKey;

    private String userHandle;

    private Integer signCount;

    private String largeBlob;

    private Boolean backupEligibility;

    private Boolean backupState;

    private String userName;

    private String userDisplayName;

    public String getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(String credentialId) {
        this.credentialId = credentialId;
    }

    public Boolean isIsResidentCredential() {
        return isResidentCredential;
    }

    public void setIsResidentCredential(Boolean isResidentCredential) {
        this.isResidentCredential = isResidentCredential;
    }

    /**
     * Relying Party ID the credential is scoped to. Must be set when adding a
     * credential.
     */
    public String getRpId() {
        return rpId;
    }

    /**
     * Relying Party ID the credential is scoped to. Must be set when adding a
     * credential.
     */
    public void setRpId(String rpId) {
        this.rpId = rpId;
    }

    /**
     * The ECDSA P-256 private key in PKCS#8 format. (Encoded as a base64 string
     * when passed over JSON)
     */
    public String getPrivateKey() {
        return privateKey;
    }

    /**
     * The ECDSA P-256 private key in PKCS#8 format. (Encoded as a base64 string
     * when passed over JSON)
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    /**
     * An opaque byte sequence with a maximum size of 64 bytes mapping the
     * credential to a specific user. (Encoded as a base64 string when passed over
     * JSON)
     */
    public String getUserHandle() {
        return userHandle;
    }

    /**
     * An opaque byte sequence with a maximum size of 64 bytes mapping the
     * credential to a specific user. (Encoded as a base64 string when passed over
     * JSON)
     */
    public void setUserHandle(String userHandle) {
        this.userHandle = userHandle;
    }

    /**
     * Signature counter. This is incremented by one for each successful assertion.
     * See https://w3c.github.io/webauthn/#signature-counter
     */
    public Integer getSignCount() {
        return signCount;
    }

    /**
     * Signature counter. This is incremented by one for each successful assertion.
     * See https://w3c.github.io/webauthn/#signature-counter
     */
    public void setSignCount(Integer signCount) {
        this.signCount = signCount;
    }

    /**
     * The large blob associated with the credential. See
     * https://w3c.github.io/webauthn/#sctn-large-blob-extension (Encoded as a
     * base64 string when passed over JSON)
     */
    public String getLargeBlob() {
        return largeBlob;
    }

    /**
     * The large blob associated with the credential. See
     * https://w3c.github.io/webauthn/#sctn-large-blob-extension (Encoded as a
     * base64 string when passed over JSON)
     */
    public void setLargeBlob(String largeBlob) {
        this.largeBlob = largeBlob;
    }

    /**
     * Assertions returned by this credential will have the backup eligibility (BE)
     * flag set to this value. Defaults to the authenticator's
     * defaultBackupEligibility value.
     */
    public Boolean isBackupEligibility() {
        return backupEligibility;
    }

    /**
     * Assertions returned by this credential will have the backup eligibility (BE)
     * flag set to this value. Defaults to the authenticator's
     * defaultBackupEligibility value.
     */
    public void setBackupEligibility(Boolean backupEligibility) {
        this.backupEligibility = backupEligibility;
    }

    /**
     * Assertions returned by this credential will have the backup state (BS) flag
     * set to this value. Defaults to the authenticator's defaultBackupState value.
     */
    public Boolean isBackupState() {
        return backupState;
    }

    /**
     * Assertions returned by this credential will have the backup state (BS) flag
     * set to this value. Defaults to the authenticator's defaultBackupState value.
     */
    public void setBackupState(Boolean backupState) {
        this.backupState = backupState;
    }

    /**
     * The credential's user.name property. Equivalent to empty if not set.
     * https://w3c.github.io/webauthn/#dom-publickeycredentialentity-name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * The credential's user.name property. Equivalent to empty if not set.
     * https://w3c.github.io/webauthn/#dom-publickeycredentialentity-name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * The credential's user.displayName property. Equivalent to empty if not set.
     * https://w3c.github.io/webauthn/#dom-publickeycredentialuserentity-displayname
     */
    public String getUserDisplayName() {
        return userDisplayName;
    }

    /**
     * The credential's user.displayName property. Equivalent to empty if not set.
     * https://w3c.github.io/webauthn/#dom-publickeycredentialuserentity-displayname
     */
    public void setUserDisplayName(String userDisplayName) {
        this.userDisplayName = userDisplayName;
    }

    public String toString() {
        return "Credential [credentialId=" + credentialId + ", isResidentCredential=" + isResidentCredential + ", rpId="
                + rpId + ", privateKey=" + privateKey + ", userHandle=" + userHandle + ", signCount=" + signCount
                + ", largeBlob=" + largeBlob + ", backupEligibility=" + backupEligibility + ", backupState="
                + backupState + ", userName=" + userName + ", userDisplayName=" + userDisplayName + "]";
    }
}
