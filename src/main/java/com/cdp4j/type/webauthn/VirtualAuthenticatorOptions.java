// SPDX-License-Identifier: MIT
package com.cdp4j.type.webauthn;

public class VirtualAuthenticatorOptions {
    private AuthenticatorProtocol protocol;

    private Ctap2Version ctap2Version;

    private AuthenticatorTransport transport;

    private Boolean hasResidentKey;

    private Boolean hasUserVerification;

    private Boolean hasLargeBlob;

    private Boolean hasCredBlob;

    private Boolean hasMinPinLength;

    private Boolean hasPrf;

    private Boolean automaticPresenceSimulation;

    private Boolean isUserVerified;

    private Boolean defaultBackupEligibility;

    private Boolean defaultBackupState;

    public AuthenticatorProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(AuthenticatorProtocol protocol) {
        this.protocol = protocol;
    }

    /**
     * Defaults to ctap2_0. Ignored if |protocol| == u2f.
     */
    public Ctap2Version getCtap2Version() {
        return ctap2Version;
    }

    /**
     * Defaults to ctap2_0. Ignored if |protocol| == u2f.
     */
    public void setCtap2Version(Ctap2Version ctap2Version) {
        this.ctap2Version = ctap2Version;
    }

    public AuthenticatorTransport getTransport() {
        return transport;
    }

    public void setTransport(AuthenticatorTransport transport) {
        this.transport = transport;
    }

    /**
     * Defaults to false.
     */
    public Boolean isHasResidentKey() {
        return hasResidentKey;
    }

    /**
     * Defaults to false.
     */
    public void setHasResidentKey(Boolean hasResidentKey) {
        this.hasResidentKey = hasResidentKey;
    }

    /**
     * Defaults to false.
     */
    public Boolean isHasUserVerification() {
        return hasUserVerification;
    }

    /**
     * Defaults to false.
     */
    public void setHasUserVerification(Boolean hasUserVerification) {
        this.hasUserVerification = hasUserVerification;
    }

    /**
     * If set to true, the authenticator will support the largeBlob extension.
     * https://w3c.github.io/webauthn#largeBlob Defaults to false.
     */
    public Boolean isHasLargeBlob() {
        return hasLargeBlob;
    }

    /**
     * If set to true, the authenticator will support the largeBlob extension.
     * https://w3c.github.io/webauthn#largeBlob Defaults to false.
     */
    public void setHasLargeBlob(Boolean hasLargeBlob) {
        this.hasLargeBlob = hasLargeBlob;
    }

    /**
     * If set to true, the authenticator will support the credBlob extension.
     * https://fidoalliance.org/specs/fido-v2.1-rd-20201208/fido-client-to-authenticator-protocol-v2.1-rd-20201208.html#sctn-credBlob-extension
     * Defaults to false.
     */
    public Boolean isHasCredBlob() {
        return hasCredBlob;
    }

    /**
     * If set to true, the authenticator will support the credBlob extension.
     * https://fidoalliance.org/specs/fido-v2.1-rd-20201208/fido-client-to-authenticator-protocol-v2.1-rd-20201208.html#sctn-credBlob-extension
     * Defaults to false.
     */
    public void setHasCredBlob(Boolean hasCredBlob) {
        this.hasCredBlob = hasCredBlob;
    }

    /**
     * If set to true, the authenticator will support the minPinLength extension.
     * https://fidoalliance.org/specs/fido-v2.1-ps-20210615/fido-client-to-authenticator-protocol-v2.1-ps-20210615.html#sctn-minpinlength-extension
     * Defaults to false.
     */
    public Boolean isHasMinPinLength() {
        return hasMinPinLength;
    }

    /**
     * If set to true, the authenticator will support the minPinLength extension.
     * https://fidoalliance.org/specs/fido-v2.1-ps-20210615/fido-client-to-authenticator-protocol-v2.1-ps-20210615.html#sctn-minpinlength-extension
     * Defaults to false.
     */
    public void setHasMinPinLength(Boolean hasMinPinLength) {
        this.hasMinPinLength = hasMinPinLength;
    }

    /**
     * If set to true, the authenticator will support the prf extension.
     * https://w3c.github.io/webauthn/#prf-extension Defaults to false.
     */
    public Boolean isHasPrf() {
        return hasPrf;
    }

    /**
     * If set to true, the authenticator will support the prf extension.
     * https://w3c.github.io/webauthn/#prf-extension Defaults to false.
     */
    public void setHasPrf(Boolean hasPrf) {
        this.hasPrf = hasPrf;
    }

    /**
     * If set to true, tests of user presence will succeed immediately. Otherwise,
     * they will not be resolved. Defaults to true.
     */
    public Boolean isAutomaticPresenceSimulation() {
        return automaticPresenceSimulation;
    }

    /**
     * If set to true, tests of user presence will succeed immediately. Otherwise,
     * they will not be resolved. Defaults to true.
     */
    public void setAutomaticPresenceSimulation(Boolean automaticPresenceSimulation) {
        this.automaticPresenceSimulation = automaticPresenceSimulation;
    }

    /**
     * Sets whether User Verification succeeds or fails for an authenticator.
     * Defaults to false.
     */
    public Boolean isIsUserVerified() {
        return isUserVerified;
    }

    /**
     * Sets whether User Verification succeeds or fails for an authenticator.
     * Defaults to false.
     */
    public void setIsUserVerified(Boolean isUserVerified) {
        this.isUserVerified = isUserVerified;
    }

    /**
     * Credentials created by this authenticator will have the backup eligibility
     * (BE) flag set to this value. Defaults to false.
     * https://w3c.github.io/webauthn/#sctn-credential-backup
     */
    public Boolean isDefaultBackupEligibility() {
        return defaultBackupEligibility;
    }

    /**
     * Credentials created by this authenticator will have the backup eligibility
     * (BE) flag set to this value. Defaults to false.
     * https://w3c.github.io/webauthn/#sctn-credential-backup
     */
    public void setDefaultBackupEligibility(Boolean defaultBackupEligibility) {
        this.defaultBackupEligibility = defaultBackupEligibility;
    }

    /**
     * Credentials created by this authenticator will have the backup state (BS)
     * flag set to this value. Defaults to false.
     * https://w3c.github.io/webauthn/#sctn-credential-backup
     */
    public Boolean isDefaultBackupState() {
        return defaultBackupState;
    }

    /**
     * Credentials created by this authenticator will have the backup state (BS)
     * flag set to this value. Defaults to false.
     * https://w3c.github.io/webauthn/#sctn-credential-backup
     */
    public void setDefaultBackupState(Boolean defaultBackupState) {
        this.defaultBackupState = defaultBackupState;
    }

    public String toString() {
        return "VirtualAuthenticatorOptions [protocol=" + protocol + ", ctap2Version=" + ctap2Version + ", transport="
                + transport + ", hasResidentKey=" + hasResidentKey + ", hasUserVerification=" + hasUserVerification
                + ", hasLargeBlob=" + hasLargeBlob + ", hasCredBlob=" + hasCredBlob + ", hasMinPinLength="
                + hasMinPinLength + ", hasPrf=" + hasPrf + ", automaticPresenceSimulation="
                + automaticPresenceSimulation + ", isUserVerified=" + isUserVerified + ", defaultBackupEligibility="
                + defaultBackupEligibility + ", defaultBackupState=" + defaultBackupState + "]";
    }
}
