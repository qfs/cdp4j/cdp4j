// SPDX-License-Identifier: MIT
package com.cdp4j.type.webauthn;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AuthenticatorTransport {
    @SerializedName("usb")
    @JsonProperty("usb")
    Usb("usb"),

    @SerializedName("nfc")
    @JsonProperty("nfc")
    Nfc("nfc"),

    @SerializedName("ble")
    @JsonProperty("ble")
    Ble("ble"),

    @SerializedName("cable")
    @JsonProperty("cable")
    Cable("cable"),

    @SerializedName("internal")
    @JsonProperty("internal")
    Internal("internal"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AuthenticatorTransport(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
