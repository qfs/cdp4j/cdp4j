// SPDX-License-Identifier: MIT
package com.cdp4j.type.webauthn;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AuthenticatorProtocol {
    @SerializedName("u2f")
    @JsonProperty("u2f")
    U2f("u2f"),

    @SerializedName("ctap2")
    @JsonProperty("ctap2")
    Ctap2("ctap2"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AuthenticatorProtocol(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
