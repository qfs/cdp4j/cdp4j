// SPDX-License-Identifier: MIT
package com.cdp4j.type.webauthn;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum Ctap2Version {
    @SerializedName("ctap2_0")
    @JsonProperty("ctap2_0")
    Ctap20("ctap2_0"),

    @SerializedName("ctap2_1")
    @JsonProperty("ctap2_1")
    Ctap21("ctap2_1"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    Ctap2Version(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
