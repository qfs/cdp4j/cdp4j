// SPDX-License-Identifier: MIT
package com.cdp4j.type.pwa;

import static java.util.Collections.emptyList;

import java.util.List;

public class FileHandler {
    private String action;

    private List<FileHandlerAccept> accepts = emptyList();

    private String displayName;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<FileHandlerAccept> getAccepts() {
        return accepts;
    }

    public void setAccepts(List<FileHandlerAccept> accepts) {
        this.accepts = accepts;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String toString() {
        return "FileHandler [action=" + action + ", accepts=" + accepts + ", displayName=" + displayName + "]";
    }
}
