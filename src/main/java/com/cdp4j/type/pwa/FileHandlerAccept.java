// SPDX-License-Identifier: MIT
package com.cdp4j.type.pwa;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * The following types are the replica of
 * https://crsrc.org/c/chrome/browser/web_applications/proto/web_app_os_integration_state.proto;drc=9910d3be894c8f142c977ba1023f30a656bc13fc;l=67
 */
public class FileHandlerAccept {
    private String mediaType;

    private List<String> fileExtensions = emptyList();

    /**
     * New name of the mimetype according to
     * https://www.iana.org/assignments/media-types/media-types.xhtml
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * New name of the mimetype according to
     * https://www.iana.org/assignments/media-types/media-types.xhtml
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public List<String> getFileExtensions() {
        return fileExtensions;
    }

    public void setFileExtensions(List<String> fileExtensions) {
        this.fileExtensions = fileExtensions;
    }

    public String toString() {
        return "FileHandlerAccept [mediaType=" + mediaType + ", fileExtensions=" + fileExtensions + "]";
    }
}
