// SPDX-License-Identifier: MIT
package com.cdp4j.type.pwa;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * If user prefers opening the app in browser or an app window.
 */
public enum DisplayMode {
    @SerializedName("standalone")
    @JsonProperty("standalone")
    Standalone("standalone"),

    @SerializedName("browser")
    @JsonProperty("browser")
    Browser("browser"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    DisplayMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
