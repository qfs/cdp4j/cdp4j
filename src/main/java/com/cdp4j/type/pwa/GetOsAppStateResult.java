// SPDX-License-Identifier: MIT
package com.cdp4j.type.pwa;

import java.util.List;

public class GetOsAppStateResult {
    private Integer badgeCount;

    private List<FileHandler> fileHandlers;

    public Integer getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(Integer badgeCount) {
        this.badgeCount = badgeCount;
    }

    public List<FileHandler> getFileHandlers() {
        return fileHandlers;
    }

    public void setFileHandlers(List<FileHandler> fileHandlers) {
        this.fileHandlers = fileHandlers;
    }

    public String toString() {
        return "GetOsAppStateResult [badgeCount=" + badgeCount + ", fileHandlers=" + fileHandlers + "]";
    }
}
