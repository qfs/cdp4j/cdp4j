// SPDX-License-Identifier: MIT
package com.cdp4j.type.memory;

public class GetDOMCountersResult {
    private Integer documents;

    private Integer nodes;

    private Integer jsEventListeners;

    public Integer getDocuments() {
        return documents;
    }

    public void setDocuments(Integer documents) {
        this.documents = documents;
    }

    public Integer getNodes() {
        return nodes;
    }

    public void setNodes(Integer nodes) {
        this.nodes = nodes;
    }

    public Integer getJsEventListeners() {
        return jsEventListeners;
    }

    public void setJsEventListeners(Integer jsEventListeners) {
        this.jsEventListeners = jsEventListeners;
    }

    public String toString() {
        return "GetDOMCountersResult [documents=" + documents + ", nodes=" + nodes + ", jsEventListeners="
                + jsEventListeners + "]";
    }
}
