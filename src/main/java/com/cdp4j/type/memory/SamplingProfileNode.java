// SPDX-License-Identifier: MIT
package com.cdp4j.type.memory;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Heap profile sample.
 */
public class SamplingProfileNode {
    private Double size;

    private Double total;

    private List<String> stack = emptyList();

    /**
     * Size of the sampled allocation.
     */
    public Double getSize() {
        return size;
    }

    /**
     * Size of the sampled allocation.
     */
    public void setSize(Double size) {
        this.size = size;
    }

    /**
     * Total bytes attributed to this sample.
     */
    public Double getTotal() {
        return total;
    }

    /**
     * Total bytes attributed to this sample.
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * Execution stack at the point of allocation.
     */
    public List<String> getStack() {
        return stack;
    }

    /**
     * Execution stack at the point of allocation.
     */
    public void setStack(List<String> stack) {
        this.stack = stack;
    }

    public String toString() {
        return "SamplingProfileNode [size=" + size + ", total=" + total + ", stack=" + stack + "]";
    }
}
