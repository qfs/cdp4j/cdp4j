// SPDX-License-Identifier: MIT
package com.cdp4j.type.memory;

/**
 * DOM object counter data.
 */
public class DOMCounter {
    private String name;

    private Integer count;

    /**
     * Object name. Note: object names should be presumed volatile and clients
     * should not expect the returned names to be consistent across runs.
     */
    public String getName() {
        return name;
    }

    /**
     * Object name. Note: object names should be presumed volatile and clients
     * should not expect the returned names to be consistent across runs.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Object count.
     */
    public Integer getCount() {
        return count;
    }

    /**
     * Object count.
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    public String toString() {
        return "DOMCounter [name=" + name + ", count=" + count + "]";
    }
}
