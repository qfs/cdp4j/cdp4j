// SPDX-License-Identifier: MIT
package com.cdp4j.type.memory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Memory pressure level.
 */
public enum PressureLevel {
    @SerializedName("moderate")
    @JsonProperty("moderate")
    Moderate("moderate"),

    @SerializedName("critical")
    @JsonProperty("critical")
    Critical("critical"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PressureLevel(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
