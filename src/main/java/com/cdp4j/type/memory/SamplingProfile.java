// SPDX-License-Identifier: MIT
package com.cdp4j.type.memory;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Array of heap profile samples.
 */
public class SamplingProfile {
    private List<SamplingProfileNode> samples = emptyList();

    private List<Module> modules = emptyList();

    public List<SamplingProfileNode> getSamples() {
        return samples;
    }

    public void setSamples(List<SamplingProfileNode> samples) {
        this.samples = samples;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public String toString() {
        return "SamplingProfile [samples=" + samples + ", modules=" + modules + "]";
    }
}
