// SPDX-License-Identifier: MIT
package com.cdp4j.type.browser;

public class GetWindowForTargetResult {
    private Integer windowId;

    private Bounds bounds;

    /**
     * Browser window id.
     */
    public Integer getWindowId() {
        return windowId;
    }

    /**
     * Browser window id.
     */
    public void setWindowId(Integer windowId) {
        this.windowId = windowId;
    }

    /**
     * Bounds information of the window. When window state is 'minimized', the
     * restored window position and size are returned.
     */
    public Bounds getBounds() {
        return bounds;
    }

    /**
     * Bounds information of the window. When window state is 'minimized', the
     * restored window position and size are returned.
     */
    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public String toString() {
        return "GetWindowForTargetResult [windowId=" + windowId + ", bounds=" + bounds + "]";
    }
}
