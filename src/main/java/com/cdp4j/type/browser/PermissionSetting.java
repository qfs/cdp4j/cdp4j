// SPDX-License-Identifier: MIT
package com.cdp4j.type.browser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PermissionSetting {
    @SerializedName("granted")
    @JsonProperty("granted")
    Granted("granted"),

    @SerializedName("denied")
    @JsonProperty("denied")
    Denied("denied"),

    @SerializedName("prompt")
    @JsonProperty("prompt")
    Prompt("prompt"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PermissionSetting(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
