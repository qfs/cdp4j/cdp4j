// SPDX-License-Identifier: MIT
package com.cdp4j.type.browser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The state of the browser window.
 */
public enum WindowState {
    @SerializedName("normal")
    @JsonProperty("normal")
    Normal("normal"),

    @SerializedName("minimized")
    @JsonProperty("minimized")
    Minimized("minimized"),

    @SerializedName("maximized")
    @JsonProperty("maximized")
    Maximized("maximized"),

    @SerializedName("fullscreen")
    @JsonProperty("fullscreen")
    Fullscreen("fullscreen"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    WindowState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
