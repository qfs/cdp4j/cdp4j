// SPDX-License-Identifier: MIT
package com.cdp4j.type.browser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Browser command ids used by executeBrowserCommand.
 */
public enum BrowserCommandId {
    @SerializedName("openTabSearch")
    @JsonProperty("openTabSearch")
    OpenTabSearch("openTabSearch"),

    @SerializedName("closeTabSearch")
    @JsonProperty("closeTabSearch")
    CloseTabSearch("closeTabSearch"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    BrowserCommandId(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
