// SPDX-License-Identifier: MIT
package com.cdp4j.type.browser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PermissionType {
    @SerializedName("ar")
    @JsonProperty("ar")
    Ar("ar"),

    @SerializedName("audioCapture")
    @JsonProperty("audioCapture")
    AudioCapture("audioCapture"),

    @SerializedName("automaticFullscreen")
    @JsonProperty("automaticFullscreen")
    AutomaticFullscreen("automaticFullscreen"),

    @SerializedName("backgroundFetch")
    @JsonProperty("backgroundFetch")
    BackgroundFetch("backgroundFetch"),

    @SerializedName("backgroundSync")
    @JsonProperty("backgroundSync")
    BackgroundSync("backgroundSync"),

    @SerializedName("cameraPanTiltZoom")
    @JsonProperty("cameraPanTiltZoom")
    CameraPanTiltZoom("cameraPanTiltZoom"),

    @SerializedName("capturedSurfaceControl")
    @JsonProperty("capturedSurfaceControl")
    CapturedSurfaceControl("capturedSurfaceControl"),

    @SerializedName("clipboardReadWrite")
    @JsonProperty("clipboardReadWrite")
    ClipboardReadWrite("clipboardReadWrite"),

    @SerializedName("clipboardSanitizedWrite")
    @JsonProperty("clipboardSanitizedWrite")
    ClipboardSanitizedWrite("clipboardSanitizedWrite"),

    @SerializedName("displayCapture")
    @JsonProperty("displayCapture")
    DisplayCapture("displayCapture"),

    @SerializedName("durableStorage")
    @JsonProperty("durableStorage")
    DurableStorage("durableStorage"),

    @SerializedName("geolocation")
    @JsonProperty("geolocation")
    Geolocation("geolocation"),

    @SerializedName("handTracking")
    @JsonProperty("handTracking")
    HandTracking("handTracking"),

    @SerializedName("idleDetection")
    @JsonProperty("idleDetection")
    IdleDetection("idleDetection"),

    @SerializedName("keyboardLock")
    @JsonProperty("keyboardLock")
    KeyboardLock("keyboardLock"),

    @SerializedName("localFonts")
    @JsonProperty("localFonts")
    LocalFonts("localFonts"),

    @SerializedName("midi")
    @JsonProperty("midi")
    Midi("midi"),

    @SerializedName("midiSysex")
    @JsonProperty("midiSysex")
    MidiSysex("midiSysex"),

    @SerializedName("nfc")
    @JsonProperty("nfc")
    Nfc("nfc"),

    @SerializedName("notifications")
    @JsonProperty("notifications")
    Notifications("notifications"),

    @SerializedName("paymentHandler")
    @JsonProperty("paymentHandler")
    PaymentHandler("paymentHandler"),

    @SerializedName("periodicBackgroundSync")
    @JsonProperty("periodicBackgroundSync")
    PeriodicBackgroundSync("periodicBackgroundSync"),

    @SerializedName("pointerLock")
    @JsonProperty("pointerLock")
    PointerLock("pointerLock"),

    @SerializedName("protectedMediaIdentifier")
    @JsonProperty("protectedMediaIdentifier")
    ProtectedMediaIdentifier("protectedMediaIdentifier"),

    @SerializedName("sensors")
    @JsonProperty("sensors")
    Sensors("sensors"),

    @SerializedName("smartCard")
    @JsonProperty("smartCard")
    SmartCard("smartCard"),

    @SerializedName("speakerSelection")
    @JsonProperty("speakerSelection")
    SpeakerSelection("speakerSelection"),

    @SerializedName("storageAccess")
    @JsonProperty("storageAccess")
    StorageAccess("storageAccess"),

    @SerializedName("topLevelStorageAccess")
    @JsonProperty("topLevelStorageAccess")
    TopLevelStorageAccess("topLevelStorageAccess"),

    @SerializedName("videoCapture")
    @JsonProperty("videoCapture")
    VideoCapture("videoCapture"),

    @SerializedName("vr")
    @JsonProperty("vr")
    Vr("vr"),

    @SerializedName("wakeLockScreen")
    @JsonProperty("wakeLockScreen")
    WakeLockScreen("wakeLockScreen"),

    @SerializedName("wakeLockSystem")
    @JsonProperty("wakeLockSystem")
    WakeLockSystem("wakeLockSystem"),

    @SerializedName("webAppInstallation")
    @JsonProperty("webAppInstallation")
    WebAppInstallation("webAppInstallation"),

    @SerializedName("webPrinting")
    @JsonProperty("webPrinting")
    WebPrinting("webPrinting"),

    @SerializedName("windowManagement")
    @JsonProperty("windowManagement")
    WindowManagement("windowManagement"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PermissionType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
