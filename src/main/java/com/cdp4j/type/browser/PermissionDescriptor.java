// SPDX-License-Identifier: MIT
package com.cdp4j.type.browser;

import com.cdp4j.annotation.Experimental;

/**
 * Definition of PermissionDescriptor defined in the Permissions API:
 * https://w3c.github.io/permissions/#dom-permissiondescriptor.
 */
@Experimental
public class PermissionDescriptor {
    private String name;

    private Boolean sysex;

    private Boolean userVisibleOnly;

    private Boolean allowWithoutSanitization;

    private Boolean allowWithoutGesture;

    private Boolean panTiltZoom;

    /**
     * Name of permission. See
     * https://cs.chromium.org/chromium/src/third_party/blink/renderer/modules/permissions/permission_descriptor.idl
     * for valid permission names.
     */
    public String getName() {
        return name;
    }

    /**
     * Name of permission. See
     * https://cs.chromium.org/chromium/src/third_party/blink/renderer/modules/permissions/permission_descriptor.idl
     * for valid permission names.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * For "midi" permission, may also specify sysex control.
     */
    public Boolean isSysex() {
        return sysex;
    }

    /**
     * For "midi" permission, may also specify sysex control.
     */
    public void setSysex(Boolean sysex) {
        this.sysex = sysex;
    }

    /**
     * For "push" permission, may specify userVisibleOnly. Note that userVisibleOnly
     * = true is the only currently supported type.
     */
    public Boolean isUserVisibleOnly() {
        return userVisibleOnly;
    }

    /**
     * For "push" permission, may specify userVisibleOnly. Note that userVisibleOnly
     * = true is the only currently supported type.
     */
    public void setUserVisibleOnly(Boolean userVisibleOnly) {
        this.userVisibleOnly = userVisibleOnly;
    }

    /**
     * For "clipboard" permission, may specify allowWithoutSanitization.
     */
    public Boolean isAllowWithoutSanitization() {
        return allowWithoutSanitization;
    }

    /**
     * For "clipboard" permission, may specify allowWithoutSanitization.
     */
    public void setAllowWithoutSanitization(Boolean allowWithoutSanitization) {
        this.allowWithoutSanitization = allowWithoutSanitization;
    }

    /**
     * For "fullscreen" permission, must specify allowWithoutGesture:true.
     */
    public Boolean isAllowWithoutGesture() {
        return allowWithoutGesture;
    }

    /**
     * For "fullscreen" permission, must specify allowWithoutGesture:true.
     */
    public void setAllowWithoutGesture(Boolean allowWithoutGesture) {
        this.allowWithoutGesture = allowWithoutGesture;
    }

    /**
     * For "camera" permission, may specify panTiltZoom.
     */
    public Boolean isPanTiltZoom() {
        return panTiltZoom;
    }

    /**
     * For "camera" permission, may specify panTiltZoom.
     */
    public void setPanTiltZoom(Boolean panTiltZoom) {
        this.panTiltZoom = panTiltZoom;
    }

    public String toString() {
        return "PermissionDescriptor [name=" + name + ", sysex=" + sysex + ", userVisibleOnly=" + userVisibleOnly
                + ", allowWithoutSanitization=" + allowWithoutSanitization + ", allowWithoutGesture="
                + allowWithoutGesture + ", panTiltZoom=" + panTiltZoom + "]";
    }
}
