// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum OriginTrialUsageRestriction {
    @SerializedName("None")
    @JsonProperty("None")
    None("None"),

    @SerializedName("Subset")
    @JsonProperty("Subset")
    Subset("Subset"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    OriginTrialUsageRestriction(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
