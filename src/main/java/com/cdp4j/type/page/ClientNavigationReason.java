// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ClientNavigationReason {
    @SerializedName("anchorClick")
    @JsonProperty("anchorClick")
    AnchorClick("anchorClick"),

    @SerializedName("formSubmissionGet")
    @JsonProperty("formSubmissionGet")
    FormSubmissionGet("formSubmissionGet"),

    @SerializedName("formSubmissionPost")
    @JsonProperty("formSubmissionPost")
    FormSubmissionPost("formSubmissionPost"),

    @SerializedName("httpHeaderRefresh")
    @JsonProperty("httpHeaderRefresh")
    HttpHeaderRefresh("httpHeaderRefresh"),

    @SerializedName("initialFrameNavigation")
    @JsonProperty("initialFrameNavigation")
    InitialFrameNavigation("initialFrameNavigation"),

    @SerializedName("metaTagRefresh")
    @JsonProperty("metaTagRefresh")
    MetaTagRefresh("metaTagRefresh"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    @SerializedName("pageBlockInterstitial")
    @JsonProperty("pageBlockInterstitial")
    PageBlockInterstitial("pageBlockInterstitial"),

    @SerializedName("reload")
    @JsonProperty("reload")
    Reload("reload"),

    @SerializedName("scriptInitiated")
    @JsonProperty("scriptInitiated")
    ScriptInitiated("scriptInitiated"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ClientNavigationReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
