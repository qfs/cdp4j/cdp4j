// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Reason for a permissions policy feature to be disabled.
 */
public enum PermissionsPolicyBlockReason {
    @SerializedName("Header")
    @JsonProperty("Header")
    Header("Header"),

    @SerializedName("IframeAttribute")
    @JsonProperty("IframeAttribute")
    IframeAttribute("IframeAttribute"),

    @SerializedName("InFencedFrameTree")
    @JsonProperty("InFencedFrameTree")
    InFencedFrameTree("InFencedFrameTree"),

    @SerializedName("InIsolatedApp")
    @JsonProperty("InIsolatedApp")
    InIsolatedApp("InIsolatedApp"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PermissionsPolicyBlockReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
