// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Javascript dialog type.
 */
public enum DialogType {
    @SerializedName("alert")
    @JsonProperty("alert")
    Alert("alert"),

    @SerializedName("confirm")
    @JsonProperty("confirm")
    Confirm("confirm"),

    @SerializedName("prompt")
    @JsonProperty("prompt")
    Prompt("prompt"),

    @SerializedName("beforeunload")
    @JsonProperty("beforeunload")
    Beforeunload("beforeunload"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    DialogType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
