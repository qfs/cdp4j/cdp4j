// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

@Experimental
public class OriginTrialTokenWithStatus {
    private String rawTokenText;

    private OriginTrialToken parsedToken;

    private OriginTrialTokenStatus status;

    public String getRawTokenText() {
        return rawTokenText;
    }

    public void setRawTokenText(String rawTokenText) {
        this.rawTokenText = rawTokenText;
    }

    /**
     * parsedToken is present only when the token is extractable and parsable.
     */
    public OriginTrialToken getParsedToken() {
        return parsedToken;
    }

    /**
     * parsedToken is present only when the token is extractable and parsable.
     */
    public void setParsedToken(OriginTrialToken parsedToken) {
        this.parsedToken = parsedToken;
    }

    public OriginTrialTokenStatus getStatus() {
        return status;
    }

    public void setStatus(OriginTrialTokenStatus status) {
        this.status = status;
    }

    public String toString() {
        return "OriginTrialTokenWithStatus [rawTokenText=" + rawTokenText + ", parsedToken=" + parsedToken + ", status="
                + status + "]";
    }
}
