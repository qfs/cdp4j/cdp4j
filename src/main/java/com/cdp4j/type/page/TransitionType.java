// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Transition type.
 */
public enum TransitionType {
    @SerializedName("link")
    @JsonProperty("link")
    Link("link"),

    @SerializedName("typed")
    @JsonProperty("typed")
    Typed("typed"),

    @SerializedName("address_bar")
    @JsonProperty("address_bar")
    AddressBar("address_bar"),

    @SerializedName("auto_bookmark")
    @JsonProperty("auto_bookmark")
    AutoBookmark("auto_bookmark"),

    @SerializedName("auto_subframe")
    @JsonProperty("auto_subframe")
    AutoSubframe("auto_subframe"),

    @SerializedName("manual_subframe")
    @JsonProperty("manual_subframe")
    ManualSubframe("manual_subframe"),

    @SerializedName("generated")
    @JsonProperty("generated")
    Generated("generated"),

    @SerializedName("auto_toplevel")
    @JsonProperty("auto_toplevel")
    AutoToplevel("auto_toplevel"),

    @SerializedName("form_submit")
    @JsonProperty("form_submit")
    FormSubmit("form_submit"),

    @SerializedName("reload")
    @JsonProperty("reload")
    Reload("reload"),

    @SerializedName("keyword")
    @JsonProperty("keyword")
    Keyword("keyword"),

    @SerializedName("keyword_generated")
    @JsonProperty("keyword_generated")
    KeywordGenerated("keyword_generated"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    TransitionType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
