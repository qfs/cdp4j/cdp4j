// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

@Experimental
public class PermissionsPolicyFeatureState {
    private PermissionsPolicyFeature feature;

    private Boolean allowed;

    private PermissionsPolicyBlockLocator locator;

    public PermissionsPolicyFeature getFeature() {
        return feature;
    }

    public void setFeature(PermissionsPolicyFeature feature) {
        this.feature = feature;
    }

    public Boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(Boolean allowed) {
        this.allowed = allowed;
    }

    public PermissionsPolicyBlockLocator getLocator() {
        return locator;
    }

    public void setLocator(PermissionsPolicyBlockLocator locator) {
        this.locator = locator;
    }

    public String toString() {
        return "PermissionsPolicyFeatureState [feature=" + feature + ", allowed=" + allowed + ", locator=" + locator
                + "]";
    }
}
