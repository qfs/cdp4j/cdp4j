// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Indicates whether the frame is a secure context and why it is the case.
 */
public enum SecureContextType {
    @SerializedName("Secure")
    @JsonProperty("Secure")
    Secure("Secure"),

    @SerializedName("SecureLocalhost")
    @JsonProperty("SecureLocalhost")
    SecureLocalhost("SecureLocalhost"),

    @SerializedName("InsecureScheme")
    @JsonProperty("InsecureScheme")
    InsecureScheme("InsecureScheme"),

    @SerializedName("InsecureAncestor")
    @JsonProperty("InsecureAncestor")
    InsecureAncestor("InsecureAncestor"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SecureContextType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
