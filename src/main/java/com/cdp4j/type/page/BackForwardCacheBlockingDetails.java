// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

@Experimental
public class BackForwardCacheBlockingDetails {
    private String url;

    private String function;

    private Integer lineNumber;

    private Integer columnNumber;

    /**
     * Url of the file where blockage happened. Optional because of tests.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Url of the file where blockage happened. Optional because of tests.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Function name where blockage happened. Optional because of anonymous
     * functions and tests.
     */
    public String getFunction() {
        return function;
    }

    /**
     * Function name where blockage happened. Optional because of anonymous
     * functions and tests.
     */
    public void setFunction(String function) {
        this.function = function;
    }

    /**
     * Line number in the script (0-based).
     */
    public Integer getLineNumber() {
        return lineNumber;
    }

    /**
     * Line number in the script (0-based).
     */
    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     * Column number in the script (0-based).
     */
    public Integer getColumnNumber() {
        return columnNumber;
    }

    /**
     * Column number in the script (0-based).
     */
    public void setColumnNumber(Integer columnNumber) {
        this.columnNumber = columnNumber;
    }

    public String toString() {
        return "BackForwardCacheBlockingDetails [url=" + url + ", function=" + function + ", lineNumber=" + lineNumber
                + ", columnNumber=" + columnNumber + "]";
    }
}
