// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Origin Trial(https://www.chromium.org/blink/origin-trials) support.
 * Status for an Origin Trial token.
 */
public enum OriginTrialTokenStatus {
    @SerializedName("Success")
    @JsonProperty("Success")
    Success("Success"),

    @SerializedName("NotSupported")
    @JsonProperty("NotSupported")
    NotSupported("NotSupported"),

    @SerializedName("Insecure")
    @JsonProperty("Insecure")
    Insecure("Insecure"),

    @SerializedName("Expired")
    @JsonProperty("Expired")
    Expired("Expired"),

    @SerializedName("WrongOrigin")
    @JsonProperty("WrongOrigin")
    WrongOrigin("WrongOrigin"),

    @SerializedName("InvalidSignature")
    @JsonProperty("InvalidSignature")
    InvalidSignature("InvalidSignature"),

    @SerializedName("Malformed")
    @JsonProperty("Malformed")
    Malformed("Malformed"),

    @SerializedName("WrongVersion")
    @JsonProperty("WrongVersion")
    WrongVersion("WrongVersion"),

    @SerializedName("FeatureDisabled")
    @JsonProperty("FeatureDisabled")
    FeatureDisabled("FeatureDisabled"),

    @SerializedName("TokenDisabled")
    @JsonProperty("TokenDisabled")
    TokenDisabled("TokenDisabled"),

    @SerializedName("FeatureDisabledForUser")
    @JsonProperty("FeatureDisabledForUser")
    FeatureDisabledForUser("FeatureDisabledForUser"),

    @SerializedName("UnknownTrial")
    @JsonProperty("UnknownTrial")
    UnknownTrial("UnknownTrial"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    OriginTrialTokenStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
