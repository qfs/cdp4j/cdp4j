// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Types of not restored reasons for back-forward cache.
 */
public enum BackForwardCacheNotRestoredReasonType {
    @SerializedName("SupportPending")
    @JsonProperty("SupportPending")
    SupportPending("SupportPending"),

    @SerializedName("PageSupportNeeded")
    @JsonProperty("PageSupportNeeded")
    PageSupportNeeded("PageSupportNeeded"),

    @SerializedName("Circumstantial")
    @JsonProperty("Circumstantial")
    Circumstantial("Circumstantial"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    BackForwardCacheNotRestoredReasonType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
