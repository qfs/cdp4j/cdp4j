// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AdFrameExplanation {
    @SerializedName("ParentIsAd")
    @JsonProperty("ParentIsAd")
    ParentIsAd("ParentIsAd"),

    @SerializedName("CreatedByAdScript")
    @JsonProperty("CreatedByAdScript")
    CreatedByAdScript("CreatedByAdScript"),

    @SerializedName("MatchedBlockingRule")
    @JsonProperty("MatchedBlockingRule")
    MatchedBlockingRule("MatchedBlockingRule"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AdFrameExplanation(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
