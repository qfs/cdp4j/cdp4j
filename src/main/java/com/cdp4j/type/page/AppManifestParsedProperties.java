// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

/**
 * Parsed app manifest properties.
 */
@Experimental
public class AppManifestParsedProperties {
    private String scope;

    /**
     * Computed scope value
     */
    public String getScope() {
        return scope;
    }

    /**
     * Computed scope value
     */
    public void setScope(String scope) {
        this.scope = scope;
    }

    public String toString() {
        return "AppManifestParsedProperties [scope=" + scope + "]";
    }
}
