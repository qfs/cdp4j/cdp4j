// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

@Experimental
public class PermissionsPolicyBlockLocator {
    private String frameId;

    private PermissionsPolicyBlockReason blockReason;

    public String getFrameId() {
        return frameId;
    }

    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public PermissionsPolicyBlockReason getBlockReason() {
        return blockReason;
    }

    public void setBlockReason(PermissionsPolicyBlockReason blockReason) {
        this.blockReason = blockReason;
    }

    public String toString() {
        return "PermissionsPolicyBlockLocator [frameId=" + frameId + ", blockReason=" + blockReason + "]";
    }
}
