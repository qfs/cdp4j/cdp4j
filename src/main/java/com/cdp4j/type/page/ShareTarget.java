// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class ShareTarget {
    private String action;

    private String method;

    private String enctype;

    private String title;

    private String text;

    private String url;

    private List<FileFilter> files = emptyList();

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getEnctype() {
        return enctype;
    }

    public void setEnctype(String enctype) {
        this.enctype = enctype;
    }

    /**
     * Embed the ShareTargetParams
     */
    public String getTitle() {
        return title;
    }

    /**
     * Embed the ShareTargetParams
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<FileFilter> getFiles() {
        return files;
    }

    public void setFiles(List<FileFilter> files) {
        this.files = files;
    }

    public String toString() {
        return "ShareTarget [action=" + action + ", method=" + method + ", enctype=" + enctype + ", title=" + title
                + ", text=" + text + ", url=" + url + ", files=" + files + "]";
    }
}
