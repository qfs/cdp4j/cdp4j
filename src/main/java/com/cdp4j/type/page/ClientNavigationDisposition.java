// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ClientNavigationDisposition {
    @SerializedName("currentTab")
    @JsonProperty("currentTab")
    CurrentTab("currentTab"),

    @SerializedName("newTab")
    @JsonProperty("newTab")
    NewTab("newTab"),

    @SerializedName("newWindow")
    @JsonProperty("newWindow")
    NewWindow("newWindow"),

    @SerializedName("download")
    @JsonProperty("download")
    Download("download"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ClientNavigationDisposition(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
