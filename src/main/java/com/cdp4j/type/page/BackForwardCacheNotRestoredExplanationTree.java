// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class BackForwardCacheNotRestoredExplanationTree {
    private String url;

    private List<BackForwardCacheNotRestoredExplanation> explanations = emptyList();

    private List<BackForwardCacheNotRestoredExplanationTree> children = emptyList();

    /**
     * URL of each frame
     */
    public String getUrl() {
        return url;
    }

    /**
     * URL of each frame
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Not restored reasons of each frame
     */
    public List<BackForwardCacheNotRestoredExplanation> getExplanations() {
        return explanations;
    }

    /**
     * Not restored reasons of each frame
     */
    public void setExplanations(List<BackForwardCacheNotRestoredExplanation> explanations) {
        this.explanations = explanations;
    }

    /**
     * Array of children frame
     */
    public List<BackForwardCacheNotRestoredExplanationTree> getChildren() {
        return children;
    }

    /**
     * Array of children frame
     */
    public void setChildren(List<BackForwardCacheNotRestoredExplanationTree> children) {
        this.children = children;
    }

    public String toString() {
        return "BackForwardCacheNotRestoredExplanationTree [url=" + url + ", explanations=" + explanations
                + ", children=" + children + "]";
    }
}
