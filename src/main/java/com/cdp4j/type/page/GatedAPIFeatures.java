// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum GatedAPIFeatures {
    @SerializedName("SharedArrayBuffers")
    @JsonProperty("SharedArrayBuffers")
    SharedArrayBuffers("SharedArrayBuffers"),

    @SerializedName("SharedArrayBuffersTransferAllowed")
    @JsonProperty("SharedArrayBuffersTransferAllowed")
    SharedArrayBuffersTransferAllowed("SharedArrayBuffersTransferAllowed"),

    @SerializedName("PerformanceMeasureMemory")
    @JsonProperty("PerformanceMeasureMemory")
    PerformanceMeasureMemory("PerformanceMeasureMemory"),

    @SerializedName("PerformanceProfile")
    @JsonProperty("PerformanceProfile")
    PerformanceProfile("PerformanceProfile"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    GatedAPIFeatures(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
