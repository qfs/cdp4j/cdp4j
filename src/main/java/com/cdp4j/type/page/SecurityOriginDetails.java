// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

/**
 * Additional information about the frame document's security origin.
 */
@Experimental
public class SecurityOriginDetails {
    private Boolean isLocalhost;

    /**
     * Indicates whether the frame document's security origin is one of the local
     * hostnames (e.g. "localhost") or IP addresses (IPv4 127.0.0.0/8 or IPv6 ::1).
     */
    public Boolean isIsLocalhost() {
        return isLocalhost;
    }

    /**
     * Indicates whether the frame document's security origin is one of the local
     * hostnames (e.g. "localhost") or IP addresses (IPv4 127.0.0.0/8 or IPv6 ::1).
     */
    public void setIsLocalhost(Boolean isLocalhost) {
        this.isLocalhost = isLocalhost;
    }

    public String toString() {
        return "SecurityOriginDetails [isLocalhost=" + isLocalhost + "]";
    }
}
