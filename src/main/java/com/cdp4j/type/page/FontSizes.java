// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

/**
 * Default font sizes.
 */
@Experimental
public class FontSizes {
    private Integer standard;

    private Integer fixed;

    /**
     * Default standard font size.
     */
    public Integer getStandard() {
        return standard;
    }

    /**
     * Default standard font size.
     */
    public void setStandard(Integer standard) {
        this.standard = standard;
    }

    /**
     * Default fixed font size.
     */
    public Integer getFixed() {
        return fixed;
    }

    /**
     * Default fixed font size.
     */
    public void setFixed(Integer fixed) {
        this.fixed = fixed;
    }

    public String toString() {
        return "FontSizes [standard=" + standard + ", fixed=" + fixed + "]";
    }
}
