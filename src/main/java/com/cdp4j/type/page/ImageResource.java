// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

/**
 * The image definition used in both icon and screenshot.
 */
@Experimental
public class ImageResource {
    private String url;

    private String sizes;

    private String type;

    /**
     * The src field in the definition, but changing to url in favor of consistency.
     */
    public String getUrl() {
        return url;
    }

    /**
     * The src field in the definition, but changing to url in favor of consistency.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public String getSizes() {
        return sizes;
    }

    public void setSizes(String sizes) {
        this.sizes = sizes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return "ImageResource [url=" + url + ", sizes=" + sizes + ", type=" + type + "]";
    }
}
