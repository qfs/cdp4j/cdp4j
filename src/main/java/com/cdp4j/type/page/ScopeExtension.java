// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

@Experimental
public class ScopeExtension {
    private String origin;

    private Boolean hasOriginWildcard;

    /**
     * Instead of using tuple, this field always returns the serialized string for
     * easy understanding and comparison.
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * Instead of using tuple, this field always returns the serialized string for
     * easy understanding and comparison.
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Boolean isHasOriginWildcard() {
        return hasOriginWildcard;
    }

    public void setHasOriginWildcard(Boolean hasOriginWildcard) {
        this.hasOriginWildcard = hasOriginWildcard;
    }

    public String toString() {
        return "ScopeExtension [origin=" + origin + ", hasOriginWildcard=" + hasOriginWildcard + "]";
    }
}
