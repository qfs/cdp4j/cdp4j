// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

/**
 * Information about the Frame hierarchy along with their cached resources.
 */
@Experimental
public class FrameResourceTree {
    private Frame frame;

    private List<FrameResourceTree> childFrames = emptyList();

    private List<FrameResource> resources = emptyList();

    /**
     * Frame information for this tree item.
     */
    public Frame getFrame() {
        return frame;
    }

    /**
     * Frame information for this tree item.
     */
    public void setFrame(Frame frame) {
        this.frame = frame;
    }

    /**
     * Child frames.
     */
    public List<FrameResourceTree> getChildFrames() {
        return childFrames;
    }

    /**
     * Child frames.
     */
    public void setChildFrames(List<FrameResourceTree> childFrames) {
        this.childFrames = childFrames;
    }

    /**
     * Information about frame resources.
     */
    public List<FrameResource> getResources() {
        return resources;
    }

    /**
     * Information about frame resources.
     */
    public void setResources(List<FrameResource> resources) {
        this.resources = resources;
    }

    public String toString() {
        return "FrameResourceTree [frame=" + frame + ", childFrames=" + childFrames + ", resources=" + resources + "]";
    }
}
