// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The referring-policy used for the navigation.
 */
public enum ReferrerPolicy {
    @SerializedName("noReferrer")
    @JsonProperty("noReferrer")
    NoReferrer("noReferrer"),

    @SerializedName("noReferrerWhenDowngrade")
    @JsonProperty("noReferrerWhenDowngrade")
    NoReferrerWhenDowngrade("noReferrerWhenDowngrade"),

    @SerializedName("origin")
    @JsonProperty("origin")
    Origin("origin"),

    @SerializedName("originWhenCrossOrigin")
    @JsonProperty("originWhenCrossOrigin")
    OriginWhenCrossOrigin("originWhenCrossOrigin"),

    @SerializedName("sameOrigin")
    @JsonProperty("sameOrigin")
    SameOrigin("sameOrigin"),

    @SerializedName("strictOrigin")
    @JsonProperty("strictOrigin")
    StrictOrigin("strictOrigin"),

    @SerializedName("strictOriginWhenCrossOrigin")
    @JsonProperty("strictOriginWhenCrossOrigin")
    StrictOriginWhenCrossOrigin("strictOriginWhenCrossOrigin"),

    @SerializedName("unsafeUrl")
    @JsonProperty("unsafeUrl")
    UnsafeUrl("unsafeUrl"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ReferrerPolicy(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
