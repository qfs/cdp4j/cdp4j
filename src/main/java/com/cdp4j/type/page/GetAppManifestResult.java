// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import java.util.List;

public class GetAppManifestResult {
    private String url;

    private List<AppManifestError> errors;

    private String data;

    private AppManifestParsedProperties parsed;

    private WebAppManifest manifest;

    /**
     * Manifest location.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Manifest location.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public List<AppManifestError> getErrors() {
        return errors;
    }

    public void setErrors(List<AppManifestError> errors) {
        this.errors = errors;
    }

    /**
     * Manifest content.
     */
    public String getData() {
        return data;
    }

    /**
     * Manifest content.
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Parsed manifest properties. Deprecated, use manifest instead.
     */
    public AppManifestParsedProperties getParsed() {
        return parsed;
    }

    /**
     * Parsed manifest properties. Deprecated, use manifest instead.
     */
    public void setParsed(AppManifestParsedProperties parsed) {
        this.parsed = parsed;
    }

    public WebAppManifest getManifest() {
        return manifest;
    }

    public void setManifest(WebAppManifest manifest) {
        this.manifest = manifest;
    }

    public String toString() {
        return "GetAppManifestResult [url=" + url + ", errors=" + errors + ", data=" + data + ", parsed=" + parsed
                + ", manifest=" + manifest + "]";
    }
}
