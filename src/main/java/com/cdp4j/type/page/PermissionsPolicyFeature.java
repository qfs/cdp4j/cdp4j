// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * All Permissions Policy features. This enum should match the one defined
 * in third_party/blink/renderer/core/permissions_policy/permissions_policy_features.json5.
 */
public enum PermissionsPolicyFeature {
    @SerializedName("accelerometer")
    @JsonProperty("accelerometer")
    Accelerometer("accelerometer"),

    @SerializedName("all-screens-capture")
    @JsonProperty("all-screens-capture")
    AllScreensCapture("all-screens-capture"),

    @SerializedName("ambient-light-sensor")
    @JsonProperty("ambient-light-sensor")
    AmbientLightSensor("ambient-light-sensor"),

    @SerializedName("attribution-reporting")
    @JsonProperty("attribution-reporting")
    AttributionReporting("attribution-reporting"),

    @SerializedName("autoplay")
    @JsonProperty("autoplay")
    Autoplay("autoplay"),

    @SerializedName("bluetooth")
    @JsonProperty("bluetooth")
    Bluetooth("bluetooth"),

    @SerializedName("browsing-topics")
    @JsonProperty("browsing-topics")
    BrowsingTopics("browsing-topics"),

    @SerializedName("camera")
    @JsonProperty("camera")
    Camera("camera"),

    @SerializedName("captured-surface-control")
    @JsonProperty("captured-surface-control")
    CapturedSurfaceControl("captured-surface-control"),

    @SerializedName("ch-dpr")
    @JsonProperty("ch-dpr")
    ChDpr("ch-dpr"),

    @SerializedName("ch-device-memory")
    @JsonProperty("ch-device-memory")
    ChDeviceMemory("ch-device-memory"),

    @SerializedName("ch-downlink")
    @JsonProperty("ch-downlink")
    ChDownlink("ch-downlink"),

    @SerializedName("ch-ect")
    @JsonProperty("ch-ect")
    ChEct("ch-ect"),

    @SerializedName("ch-prefers-color-scheme")
    @JsonProperty("ch-prefers-color-scheme")
    ChPrefersColorScheme("ch-prefers-color-scheme"),

    @SerializedName("ch-prefers-reduced-motion")
    @JsonProperty("ch-prefers-reduced-motion")
    ChPrefersReducedMotion("ch-prefers-reduced-motion"),

    @SerializedName("ch-prefers-reduced-transparency")
    @JsonProperty("ch-prefers-reduced-transparency")
    ChPrefersReducedTransparency("ch-prefers-reduced-transparency"),

    @SerializedName("ch-rtt")
    @JsonProperty("ch-rtt")
    ChRtt("ch-rtt"),

    @SerializedName("ch-save-data")
    @JsonProperty("ch-save-data")
    ChSaveData("ch-save-data"),

    @SerializedName("ch-ua")
    @JsonProperty("ch-ua")
    ChUa("ch-ua"),

    @SerializedName("ch-ua-arch")
    @JsonProperty("ch-ua-arch")
    ChUaArch("ch-ua-arch"),

    @SerializedName("ch-ua-bitness")
    @JsonProperty("ch-ua-bitness")
    ChUaBitness("ch-ua-bitness"),

    @SerializedName("ch-ua-high-entropy-values")
    @JsonProperty("ch-ua-high-entropy-values")
    ChUaHighEntropyValues("ch-ua-high-entropy-values"),

    @SerializedName("ch-ua-platform")
    @JsonProperty("ch-ua-platform")
    ChUaPlatform("ch-ua-platform"),

    @SerializedName("ch-ua-model")
    @JsonProperty("ch-ua-model")
    ChUaModel("ch-ua-model"),

    @SerializedName("ch-ua-mobile")
    @JsonProperty("ch-ua-mobile")
    ChUaMobile("ch-ua-mobile"),

    @SerializedName("ch-ua-form-factors")
    @JsonProperty("ch-ua-form-factors")
    ChUaFormFactors("ch-ua-form-factors"),

    @SerializedName("ch-ua-full-version")
    @JsonProperty("ch-ua-full-version")
    ChUaFullVersion("ch-ua-full-version"),

    @SerializedName("ch-ua-full-version-list")
    @JsonProperty("ch-ua-full-version-list")
    ChUaFullVersionList("ch-ua-full-version-list"),

    @SerializedName("ch-ua-platform-version")
    @JsonProperty("ch-ua-platform-version")
    ChUaPlatformVersion("ch-ua-platform-version"),

    @SerializedName("ch-ua-wow64")
    @JsonProperty("ch-ua-wow64")
    ChUaWow64("ch-ua-wow64"),

    @SerializedName("ch-viewport-height")
    @JsonProperty("ch-viewport-height")
    ChViewportHeight("ch-viewport-height"),

    @SerializedName("ch-viewport-width")
    @JsonProperty("ch-viewport-width")
    ChViewportWidth("ch-viewport-width"),

    @SerializedName("ch-width")
    @JsonProperty("ch-width")
    ChWidth("ch-width"),

    @SerializedName("clipboard-read")
    @JsonProperty("clipboard-read")
    ClipboardRead("clipboard-read"),

    @SerializedName("clipboard-write")
    @JsonProperty("clipboard-write")
    ClipboardWrite("clipboard-write"),

    @SerializedName("compute-pressure")
    @JsonProperty("compute-pressure")
    ComputePressure("compute-pressure"),

    @SerializedName("controlled-frame")
    @JsonProperty("controlled-frame")
    ControlledFrame("controlled-frame"),

    @SerializedName("cross-origin-isolated")
    @JsonProperty("cross-origin-isolated")
    CrossOriginIsolated("cross-origin-isolated"),

    @SerializedName("deferred-fetch")
    @JsonProperty("deferred-fetch")
    DeferredFetch("deferred-fetch"),

    @SerializedName("deferred-fetch-minimal")
    @JsonProperty("deferred-fetch-minimal")
    DeferredFetchMinimal("deferred-fetch-minimal"),

    @SerializedName("digital-credentials-get")
    @JsonProperty("digital-credentials-get")
    DigitalCredentialsGet("digital-credentials-get"),

    @SerializedName("direct-sockets")
    @JsonProperty("direct-sockets")
    DirectSockets("direct-sockets"),

    @SerializedName("direct-sockets-private")
    @JsonProperty("direct-sockets-private")
    DirectSocketsPrivate("direct-sockets-private"),

    @SerializedName("display-capture")
    @JsonProperty("display-capture")
    DisplayCapture("display-capture"),

    @SerializedName("document-domain")
    @JsonProperty("document-domain")
    DocumentDomain("document-domain"),

    @SerializedName("encrypted-media")
    @JsonProperty("encrypted-media")
    EncryptedMedia("encrypted-media"),

    @SerializedName("execution-while-out-of-viewport")
    @JsonProperty("execution-while-out-of-viewport")
    ExecutionWhileOutOfViewport("execution-while-out-of-viewport"),

    @SerializedName("execution-while-not-rendered")
    @JsonProperty("execution-while-not-rendered")
    ExecutionWhileNotRendered("execution-while-not-rendered"),

    @SerializedName("fenced-unpartitioned-storage-read")
    @JsonProperty("fenced-unpartitioned-storage-read")
    FencedUnpartitionedStorageRead("fenced-unpartitioned-storage-read"),

    @SerializedName("focus-without-user-activation")
    @JsonProperty("focus-without-user-activation")
    FocusWithoutUserActivation("focus-without-user-activation"),

    @SerializedName("fullscreen")
    @JsonProperty("fullscreen")
    Fullscreen("fullscreen"),

    @SerializedName("frobulate")
    @JsonProperty("frobulate")
    Frobulate("frobulate"),

    @SerializedName("gamepad")
    @JsonProperty("gamepad")
    Gamepad("gamepad"),

    @SerializedName("geolocation")
    @JsonProperty("geolocation")
    Geolocation("geolocation"),

    @SerializedName("gyroscope")
    @JsonProperty("gyroscope")
    Gyroscope("gyroscope"),

    @SerializedName("hid")
    @JsonProperty("hid")
    Hid("hid"),

    @SerializedName("identity-credentials-get")
    @JsonProperty("identity-credentials-get")
    IdentityCredentialsGet("identity-credentials-get"),

    @SerializedName("idle-detection")
    @JsonProperty("idle-detection")
    IdleDetection("idle-detection"),

    @SerializedName("interest-cohort")
    @JsonProperty("interest-cohort")
    InterestCohort("interest-cohort"),

    @SerializedName("join-ad-interest-group")
    @JsonProperty("join-ad-interest-group")
    JoinAdInterestGroup("join-ad-interest-group"),

    @SerializedName("keyboard-map")
    @JsonProperty("keyboard-map")
    KeyboardMap("keyboard-map"),

    @SerializedName("local-fonts")
    @JsonProperty("local-fonts")
    LocalFonts("local-fonts"),

    @SerializedName("magnetometer")
    @JsonProperty("magnetometer")
    Magnetometer("magnetometer"),

    @SerializedName("media-playback-while-not-visible")
    @JsonProperty("media-playback-while-not-visible")
    MediaPlaybackWhileNotVisible("media-playback-while-not-visible"),

    @SerializedName("microphone")
    @JsonProperty("microphone")
    Microphone("microphone"),

    @SerializedName("midi")
    @JsonProperty("midi")
    Midi("midi"),

    @SerializedName("otp-credentials")
    @JsonProperty("otp-credentials")
    OtpCredentials("otp-credentials"),

    @SerializedName("payment")
    @JsonProperty("payment")
    Payment("payment"),

    @SerializedName("picture-in-picture")
    @JsonProperty("picture-in-picture")
    PictureInPicture("picture-in-picture"),

    @SerializedName("popins")
    @JsonProperty("popins")
    Popins("popins"),

    @SerializedName("private-aggregation")
    @JsonProperty("private-aggregation")
    PrivateAggregation("private-aggregation"),

    @SerializedName("private-state-token-issuance")
    @JsonProperty("private-state-token-issuance")
    PrivateStateTokenIssuance("private-state-token-issuance"),

    @SerializedName("private-state-token-redemption")
    @JsonProperty("private-state-token-redemption")
    PrivateStateTokenRedemption("private-state-token-redemption"),

    @SerializedName("publickey-credentials-create")
    @JsonProperty("publickey-credentials-create")
    PublickeyCredentialsCreate("publickey-credentials-create"),

    @SerializedName("publickey-credentials-get")
    @JsonProperty("publickey-credentials-get")
    PublickeyCredentialsGet("publickey-credentials-get"),

    @SerializedName("run-ad-auction")
    @JsonProperty("run-ad-auction")
    RunAdAuction("run-ad-auction"),

    @SerializedName("screen-wake-lock")
    @JsonProperty("screen-wake-lock")
    ScreenWakeLock("screen-wake-lock"),

    @SerializedName("serial")
    @JsonProperty("serial")
    Serial("serial"),

    @SerializedName("shared-autofill")
    @JsonProperty("shared-autofill")
    SharedAutofill("shared-autofill"),

    @SerializedName("shared-storage")
    @JsonProperty("shared-storage")
    SharedStorage("shared-storage"),

    @SerializedName("shared-storage-select-url")
    @JsonProperty("shared-storage-select-url")
    SharedStorageSelectUrl("shared-storage-select-url"),

    @SerializedName("smart-card")
    @JsonProperty("smart-card")
    SmartCard("smart-card"),

    @SerializedName("speaker-selection")
    @JsonProperty("speaker-selection")
    SpeakerSelection("speaker-selection"),

    @SerializedName("storage-access")
    @JsonProperty("storage-access")
    StorageAccess("storage-access"),

    @SerializedName("sub-apps")
    @JsonProperty("sub-apps")
    SubApps("sub-apps"),

    @SerializedName("sync-xhr")
    @JsonProperty("sync-xhr")
    SyncXhr("sync-xhr"),

    @SerializedName("unload")
    @JsonProperty("unload")
    Unload("unload"),

    @SerializedName("usb")
    @JsonProperty("usb")
    Usb("usb"),

    @SerializedName("usb-unrestricted")
    @JsonProperty("usb-unrestricted")
    UsbUnrestricted("usb-unrestricted"),

    @SerializedName("vertical-scroll")
    @JsonProperty("vertical-scroll")
    VerticalScroll("vertical-scroll"),

    @SerializedName("web-app-installation")
    @JsonProperty("web-app-installation")
    WebAppInstallation("web-app-installation"),

    @SerializedName("web-printing")
    @JsonProperty("web-printing")
    WebPrinting("web-printing"),

    @SerializedName("web-share")
    @JsonProperty("web-share")
    WebShare("web-share"),

    @SerializedName("window-management")
    @JsonProperty("window-management")
    WindowManagement("window-management"),

    @SerializedName("xr-spatial-tracking")
    @JsonProperty("xr-spatial-tracking")
    XrSpatialTracking("xr-spatial-tracking"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PermissionsPolicyFeature(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
