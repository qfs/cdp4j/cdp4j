// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Status for an Origin Trial.
 */
public enum OriginTrialStatus {
    @SerializedName("Enabled")
    @JsonProperty("Enabled")
    Enabled("Enabled"),

    @SerializedName("ValidTokenNotProvided")
    @JsonProperty("ValidTokenNotProvided")
    ValidTokenNotProvided("ValidTokenNotProvided"),

    @SerializedName("OSNotSupported")
    @JsonProperty("OSNotSupported")
    OSNotSupported("OSNotSupported"),

    @SerializedName("TrialNotAllowed")
    @JsonProperty("TrialNotAllowed")
    TrialNotAllowed("TrialNotAllowed"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    OriginTrialStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
