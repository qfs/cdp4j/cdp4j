// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of possible auto-response for permission / prompt dialogs.
 */
public enum AutoResponseMode {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("autoAccept")
    @JsonProperty("autoAccept")
    AutoAccept("autoAccept"),

    @SerializedName("autoReject")
    @JsonProperty("autoReject")
    AutoReject("autoReject"),

    @SerializedName("autoOptOut")
    @JsonProperty("autoOptOut")
    AutoOptOut("autoOptOut"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AutoResponseMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
