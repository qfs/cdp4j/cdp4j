// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The type of a frameNavigated event.
 */
public enum NavigationType {
    @SerializedName("Navigation")
    @JsonProperty("Navigation")
    Navigation("Navigation"),

    @SerializedName("BackForwardCacheRestore")
    @JsonProperty("BackForwardCacheRestore")
    BackForwardCacheRestore("BackForwardCacheRestore"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    NavigationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
