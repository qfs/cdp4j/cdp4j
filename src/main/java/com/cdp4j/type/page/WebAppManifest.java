// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class WebAppManifest {
    private String backgroundColor;

    private String description;

    private String dir;

    private String display;

    private List<String> displayOverrides = emptyList();

    private List<FileHandler> fileHandlers = emptyList();

    private List<ImageResource> icons = emptyList();

    private String id;

    private String lang;

    private LaunchHandler launchHandler;

    private String name;

    private String orientation;

    private Boolean preferRelatedApplications;

    private List<ProtocolHandler> protocolHandlers = emptyList();

    private List<RelatedApplication> relatedApplications = emptyList();

    private String scope;

    private List<ScopeExtension> scopeExtensions = emptyList();

    private List<Screenshot> screenshots = emptyList();

    private ShareTarget shareTarget;

    private String shortName;

    private List<Shortcut> shortcuts = emptyList();

    private String startUrl;

    private String themeColor;

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * The extra description provided by the manifest.
     */
    public String getDescription() {
        return description;
    }

    /**
     * The extra description provided by the manifest.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    /**
     * The overrided display mode controlled by the user.
     */
    public List<String> getDisplayOverrides() {
        return displayOverrides;
    }

    /**
     * The overrided display mode controlled by the user.
     */
    public void setDisplayOverrides(List<String> displayOverrides) {
        this.displayOverrides = displayOverrides;
    }

    /**
     * The handlers to open files.
     */
    public List<FileHandler> getFileHandlers() {
        return fileHandlers;
    }

    /**
     * The handlers to open files.
     */
    public void setFileHandlers(List<FileHandler> fileHandlers) {
        this.fileHandlers = fileHandlers;
    }

    public List<ImageResource> getIcons() {
        return icons;
    }

    public void setIcons(List<ImageResource> icons) {
        this.icons = icons;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * TODO(crbug.com/1231886): This field is non-standard and part of a Chrome
     * experiment. See:
     * https://github.com/WICG/web-app-launch/blob/main/launch_handler.md
     */
    public LaunchHandler getLaunchHandler() {
        return launchHandler;
    }

    /**
     * TODO(crbug.com/1231886): This field is non-standard and part of a Chrome
     * experiment. See:
     * https://github.com/WICG/web-app-launch/blob/main/launch_handler.md
     */
    public void setLaunchHandler(LaunchHandler launchHandler) {
        this.launchHandler = launchHandler;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrientation() {
        return orientation;
    }

    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

    public Boolean isPreferRelatedApplications() {
        return preferRelatedApplications;
    }

    public void setPreferRelatedApplications(Boolean preferRelatedApplications) {
        this.preferRelatedApplications = preferRelatedApplications;
    }

    /**
     * The handlers to open protocols.
     */
    public List<ProtocolHandler> getProtocolHandlers() {
        return protocolHandlers;
    }

    /**
     * The handlers to open protocols.
     */
    public void setProtocolHandlers(List<ProtocolHandler> protocolHandlers) {
        this.protocolHandlers = protocolHandlers;
    }

    public List<RelatedApplication> getRelatedApplications() {
        return relatedApplications;
    }

    public void setRelatedApplications(List<RelatedApplication> relatedApplications) {
        this.relatedApplications = relatedApplications;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    /**
     * Non-standard, see
     * https://github.com/WICG/manifest-incubations/blob/gh-pages/scope_extensions-explainer.md
     */
    public List<ScopeExtension> getScopeExtensions() {
        return scopeExtensions;
    }

    /**
     * Non-standard, see
     * https://github.com/WICG/manifest-incubations/blob/gh-pages/scope_extensions-explainer.md
     */
    public void setScopeExtensions(List<ScopeExtension> scopeExtensions) {
        this.scopeExtensions = scopeExtensions;
    }

    /**
     * The screenshots used by chromium.
     */
    public List<Screenshot> getScreenshots() {
        return screenshots;
    }

    /**
     * The screenshots used by chromium.
     */
    public void setScreenshots(List<Screenshot> screenshots) {
        this.screenshots = screenshots;
    }

    public ShareTarget getShareTarget() {
        return shareTarget;
    }

    public void setShareTarget(ShareTarget shareTarget) {
        this.shareTarget = shareTarget;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<Shortcut> getShortcuts() {
        return shortcuts;
    }

    public void setShortcuts(List<Shortcut> shortcuts) {
        this.shortcuts = shortcuts;
    }

    public String getStartUrl() {
        return startUrl;
    }

    public void setStartUrl(String startUrl) {
        this.startUrl = startUrl;
    }

    public String getThemeColor() {
        return themeColor;
    }

    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    public String toString() {
        return "WebAppManifest [backgroundColor=" + backgroundColor + ", description=" + description + ", dir=" + dir
                + ", display=" + display + ", displayOverrides=" + displayOverrides + ", fileHandlers=" + fileHandlers
                + ", icons=" + icons + ", id=" + id + ", lang=" + lang + ", launchHandler=" + launchHandler + ", name="
                + name + ", orientation=" + orientation + ", preferRelatedApplications=" + preferRelatedApplications
                + ", protocolHandlers=" + protocolHandlers + ", relatedApplications=" + relatedApplications + ", scope="
                + scope + ", scopeExtensions=" + scopeExtensions + ", screenshots=" + screenshots + ", shareTarget="
                + shareTarget + ", shortName=" + shortName + ", shortcuts=" + shortcuts + ", startUrl=" + startUrl
                + ", themeColor=" + themeColor + "]";
    }
}
