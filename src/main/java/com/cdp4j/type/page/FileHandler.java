// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class FileHandler {
    private String action;

    private String name;

    private List<ImageResource> icons = emptyList();

    private List<FileFilter> accepts = emptyList();

    private String launchType;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ImageResource> getIcons() {
        return icons;
    }

    public void setIcons(List<ImageResource> icons) {
        this.icons = icons;
    }

    /**
     * Mimic a map, name is the key, accepts is the value.
     */
    public List<FileFilter> getAccepts() {
        return accepts;
    }

    /**
     * Mimic a map, name is the key, accepts is the value.
     */
    public void setAccepts(List<FileFilter> accepts) {
        this.accepts = accepts;
    }

    /**
     * Won't repeat the enums, using string for easy comparison. Same as the other
     * enums below.
     */
    public String getLaunchType() {
        return launchType;
    }

    /**
     * Won't repeat the enums, using string for easy comparison. Same as the other
     * enums below.
     */
    public void setLaunchType(String launchType) {
        this.launchType = launchType;
    }

    public String toString() {
        return "FileHandler [action=" + action + ", name=" + name + ", icons=" + icons + ", accepts=" + accepts
                + ", launchType=" + launchType + "]";
    }
}
