// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Indicates whether the frame is cross-origin isolated and why it is the case.
 */
public enum CrossOriginIsolatedContextType {
    @SerializedName("Isolated")
    @JsonProperty("Isolated")
    Isolated("Isolated"),

    @SerializedName("NotIsolated")
    @JsonProperty("NotIsolated")
    NotIsolated("NotIsolated"),

    @SerializedName("NotIsolatedFeatureDisabled")
    @JsonProperty("NotIsolatedFeatureDisabled")
    NotIsolatedFeatureDisabled("NotIsolatedFeatureDisabled"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CrossOriginIsolatedContextType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
