// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

/**
 * Indicates whether a frame has been identified as an ad and why.
 */
@Experimental
public class AdFrameStatus {
    private AdFrameType adFrameType;

    private List<AdFrameExplanation> explanations = emptyList();

    public AdFrameType getAdFrameType() {
        return adFrameType;
    }

    public void setAdFrameType(AdFrameType adFrameType) {
        this.adFrameType = adFrameType;
    }

    public List<AdFrameExplanation> getExplanations() {
        return explanations;
    }

    public void setExplanations(List<AdFrameExplanation> explanations) {
        this.explanations = explanations;
    }

    public String toString() {
        return "AdFrameStatus [adFrameType=" + adFrameType + ", explanations=" + explanations + "]";
    }
}
