// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

public class PrintToPDFResult {
    private byte[] data;

    private String stream;

    /**
     * Base64-encoded pdf data. Empty if |returnAsStream| is specified. (Encoded as
     * a base64 string when passed over JSON)
     */
    public byte[] getData() {
        return data;
    }

    /**
     * Base64-encoded pdf data. Empty if |returnAsStream| is specified. (Encoded as
     * a base64 string when passed over JSON)
     */
    public void setData(byte[] data) {
        this.data = data;
    }

    /**
     * A handle of the stream that holds resulting PDF data.
     */
    public String getStream() {
        return stream;
    }

    /**
     * A handle of the stream that holds resulting PDF data.
     */
    public void setStream(String stream) {
        this.stream = stream;
    }

    public String toString() {
        return "PrintToPDFResult [data=" + data + ", stream=" + stream + "]";
    }
}
