// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * List of not restored reasons for back-forward cache.
 */
public enum BackForwardCacheNotRestoredReason {
    @SerializedName("NotPrimaryMainFrame")
    @JsonProperty("NotPrimaryMainFrame")
    NotPrimaryMainFrame("NotPrimaryMainFrame"),

    @SerializedName("BackForwardCacheDisabled")
    @JsonProperty("BackForwardCacheDisabled")
    BackForwardCacheDisabled("BackForwardCacheDisabled"),

    @SerializedName("RelatedActiveContentsExist")
    @JsonProperty("RelatedActiveContentsExist")
    RelatedActiveContentsExist("RelatedActiveContentsExist"),

    @SerializedName("HTTPStatusNotOK")
    @JsonProperty("HTTPStatusNotOK")
    HTTPStatusNotOK("HTTPStatusNotOK"),

    @SerializedName("SchemeNotHTTPOrHTTPS")
    @JsonProperty("SchemeNotHTTPOrHTTPS")
    SchemeNotHTTPOrHTTPS("SchemeNotHTTPOrHTTPS"),

    @SerializedName("Loading")
    @JsonProperty("Loading")
    Loading("Loading"),

    @SerializedName("WasGrantedMediaAccess")
    @JsonProperty("WasGrantedMediaAccess")
    WasGrantedMediaAccess("WasGrantedMediaAccess"),

    @SerializedName("DisableForRenderFrameHostCalled")
    @JsonProperty("DisableForRenderFrameHostCalled")
    DisableForRenderFrameHostCalled("DisableForRenderFrameHostCalled"),

    @SerializedName("DomainNotAllowed")
    @JsonProperty("DomainNotAllowed")
    DomainNotAllowed("DomainNotAllowed"),

    @SerializedName("HTTPMethodNotGET")
    @JsonProperty("HTTPMethodNotGET")
    HTTPMethodNotGET("HTTPMethodNotGET"),

    @SerializedName("SubframeIsNavigating")
    @JsonProperty("SubframeIsNavigating")
    SubframeIsNavigating("SubframeIsNavigating"),

    @SerializedName("Timeout")
    @JsonProperty("Timeout")
    Timeout("Timeout"),

    @SerializedName("CacheLimit")
    @JsonProperty("CacheLimit")
    CacheLimit("CacheLimit"),

    @SerializedName("JavaScriptExecution")
    @JsonProperty("JavaScriptExecution")
    JavaScriptExecution("JavaScriptExecution"),

    @SerializedName("RendererProcessKilled")
    @JsonProperty("RendererProcessKilled")
    RendererProcessKilled("RendererProcessKilled"),

    @SerializedName("RendererProcessCrashed")
    @JsonProperty("RendererProcessCrashed")
    RendererProcessCrashed("RendererProcessCrashed"),

    @SerializedName("SchedulerTrackedFeatureUsed")
    @JsonProperty("SchedulerTrackedFeatureUsed")
    SchedulerTrackedFeatureUsed("SchedulerTrackedFeatureUsed"),

    @SerializedName("ConflictingBrowsingInstance")
    @JsonProperty("ConflictingBrowsingInstance")
    ConflictingBrowsingInstance("ConflictingBrowsingInstance"),

    @SerializedName("CacheFlushed")
    @JsonProperty("CacheFlushed")
    CacheFlushed("CacheFlushed"),

    @SerializedName("ServiceWorkerVersionActivation")
    @JsonProperty("ServiceWorkerVersionActivation")
    ServiceWorkerVersionActivation("ServiceWorkerVersionActivation"),

    @SerializedName("SessionRestored")
    @JsonProperty("SessionRestored")
    SessionRestored("SessionRestored"),

    @SerializedName("ServiceWorkerPostMessage")
    @JsonProperty("ServiceWorkerPostMessage")
    ServiceWorkerPostMessage("ServiceWorkerPostMessage"),

    @SerializedName("EnteredBackForwardCacheBeforeServiceWorkerHostAdded")
    @JsonProperty("EnteredBackForwardCacheBeforeServiceWorkerHostAdded")
    EnteredBackForwardCacheBeforeServiceWorkerHostAdded("EnteredBackForwardCacheBeforeServiceWorkerHostAdded"),

    @SerializedName("RenderFrameHostReused_SameSite")
    @JsonProperty("RenderFrameHostReused_SameSite")
    RenderFrameHostReusedSameSite("RenderFrameHostReused_SameSite"),

    @SerializedName("RenderFrameHostReused_CrossSite")
    @JsonProperty("RenderFrameHostReused_CrossSite")
    RenderFrameHostReusedCrossSite("RenderFrameHostReused_CrossSite"),

    @SerializedName("ServiceWorkerClaim")
    @JsonProperty("ServiceWorkerClaim")
    ServiceWorkerClaim("ServiceWorkerClaim"),

    @SerializedName("IgnoreEventAndEvict")
    @JsonProperty("IgnoreEventAndEvict")
    IgnoreEventAndEvict("IgnoreEventAndEvict"),

    @SerializedName("HaveInnerContents")
    @JsonProperty("HaveInnerContents")
    HaveInnerContents("HaveInnerContents"),

    @SerializedName("TimeoutPuttingInCache")
    @JsonProperty("TimeoutPuttingInCache")
    TimeoutPuttingInCache("TimeoutPuttingInCache"),

    @SerializedName("BackForwardCacheDisabledByLowMemory")
    @JsonProperty("BackForwardCacheDisabledByLowMemory")
    BackForwardCacheDisabledByLowMemory("BackForwardCacheDisabledByLowMemory"),

    @SerializedName("BackForwardCacheDisabledByCommandLine")
    @JsonProperty("BackForwardCacheDisabledByCommandLine")
    BackForwardCacheDisabledByCommandLine("BackForwardCacheDisabledByCommandLine"),

    @SerializedName("NetworkRequestDatapipeDrainedAsBytesConsumer")
    @JsonProperty("NetworkRequestDatapipeDrainedAsBytesConsumer")
    NetworkRequestDatapipeDrainedAsBytesConsumer("NetworkRequestDatapipeDrainedAsBytesConsumer"),

    @SerializedName("NetworkRequestRedirected")
    @JsonProperty("NetworkRequestRedirected")
    NetworkRequestRedirected("NetworkRequestRedirected"),

    @SerializedName("NetworkRequestTimeout")
    @JsonProperty("NetworkRequestTimeout")
    NetworkRequestTimeout("NetworkRequestTimeout"),

    @SerializedName("NetworkExceedsBufferLimit")
    @JsonProperty("NetworkExceedsBufferLimit")
    NetworkExceedsBufferLimit("NetworkExceedsBufferLimit"),

    @SerializedName("NavigationCancelledWhileRestoring")
    @JsonProperty("NavigationCancelledWhileRestoring")
    NavigationCancelledWhileRestoring("NavigationCancelledWhileRestoring"),

    @SerializedName("NotMostRecentNavigationEntry")
    @JsonProperty("NotMostRecentNavigationEntry")
    NotMostRecentNavigationEntry("NotMostRecentNavigationEntry"),

    @SerializedName("BackForwardCacheDisabledForPrerender")
    @JsonProperty("BackForwardCacheDisabledForPrerender")
    BackForwardCacheDisabledForPrerender("BackForwardCacheDisabledForPrerender"),

    @SerializedName("UserAgentOverrideDiffers")
    @JsonProperty("UserAgentOverrideDiffers")
    UserAgentOverrideDiffers("UserAgentOverrideDiffers"),

    @SerializedName("ForegroundCacheLimit")
    @JsonProperty("ForegroundCacheLimit")
    ForegroundCacheLimit("ForegroundCacheLimit"),

    @SerializedName("BrowsingInstanceNotSwapped")
    @JsonProperty("BrowsingInstanceNotSwapped")
    BrowsingInstanceNotSwapped("BrowsingInstanceNotSwapped"),

    @SerializedName("BackForwardCacheDisabledForDelegate")
    @JsonProperty("BackForwardCacheDisabledForDelegate")
    BackForwardCacheDisabledForDelegate("BackForwardCacheDisabledForDelegate"),

    @SerializedName("UnloadHandlerExistsInMainFrame")
    @JsonProperty("UnloadHandlerExistsInMainFrame")
    UnloadHandlerExistsInMainFrame("UnloadHandlerExistsInMainFrame"),

    @SerializedName("UnloadHandlerExistsInSubFrame")
    @JsonProperty("UnloadHandlerExistsInSubFrame")
    UnloadHandlerExistsInSubFrame("UnloadHandlerExistsInSubFrame"),

    @SerializedName("ServiceWorkerUnregistration")
    @JsonProperty("ServiceWorkerUnregistration")
    ServiceWorkerUnregistration("ServiceWorkerUnregistration"),

    @SerializedName("CacheControlNoStore")
    @JsonProperty("CacheControlNoStore")
    CacheControlNoStore("CacheControlNoStore"),

    @SerializedName("CacheControlNoStoreCookieModified")
    @JsonProperty("CacheControlNoStoreCookieModified")
    CacheControlNoStoreCookieModified("CacheControlNoStoreCookieModified"),

    @SerializedName("CacheControlNoStoreHTTPOnlyCookieModified")
    @JsonProperty("CacheControlNoStoreHTTPOnlyCookieModified")
    CacheControlNoStoreHTTPOnlyCookieModified("CacheControlNoStoreHTTPOnlyCookieModified"),

    @SerializedName("NoResponseHead")
    @JsonProperty("NoResponseHead")
    NoResponseHead("NoResponseHead"),

    @SerializedName("Unknown")
    @JsonProperty("Unknown")
    Unknown("Unknown"),

    @SerializedName("ActivationNavigationsDisallowedForBug1234857")
    @JsonProperty("ActivationNavigationsDisallowedForBug1234857")
    ActivationNavigationsDisallowedForBug1234857("ActivationNavigationsDisallowedForBug1234857"),

    @SerializedName("ErrorDocument")
    @JsonProperty("ErrorDocument")
    ErrorDocument("ErrorDocument"),

    @SerializedName("FencedFramesEmbedder")
    @JsonProperty("FencedFramesEmbedder")
    FencedFramesEmbedder("FencedFramesEmbedder"),

    @SerializedName("CookieDisabled")
    @JsonProperty("CookieDisabled")
    CookieDisabled("CookieDisabled"),

    @SerializedName("HTTPAuthRequired")
    @JsonProperty("HTTPAuthRequired")
    HTTPAuthRequired("HTTPAuthRequired"),

    @SerializedName("CookieFlushed")
    @JsonProperty("CookieFlushed")
    CookieFlushed("CookieFlushed"),

    @SerializedName("BroadcastChannelOnMessage")
    @JsonProperty("BroadcastChannelOnMessage")
    BroadcastChannelOnMessage("BroadcastChannelOnMessage"),

    @SerializedName("WebViewSettingsChanged")
    @JsonProperty("WebViewSettingsChanged")
    WebViewSettingsChanged("WebViewSettingsChanged"),

    @SerializedName("WebViewJavaScriptObjectChanged")
    @JsonProperty("WebViewJavaScriptObjectChanged")
    WebViewJavaScriptObjectChanged("WebViewJavaScriptObjectChanged"),

    @SerializedName("WebViewMessageListenerInjected")
    @JsonProperty("WebViewMessageListenerInjected")
    WebViewMessageListenerInjected("WebViewMessageListenerInjected"),

    @SerializedName("WebViewSafeBrowsingAllowlistChanged")
    @JsonProperty("WebViewSafeBrowsingAllowlistChanged")
    WebViewSafeBrowsingAllowlistChanged("WebViewSafeBrowsingAllowlistChanged"),

    @SerializedName("WebViewDocumentStartJavascriptChanged")
    @JsonProperty("WebViewDocumentStartJavascriptChanged")
    WebViewDocumentStartJavascriptChanged("WebViewDocumentStartJavascriptChanged"),

    @SerializedName("WebSocket")
    @JsonProperty("WebSocket")
    WebSocket("WebSocket"),

    @SerializedName("WebTransport")
    @JsonProperty("WebTransport")
    WebTransport("WebTransport"),

    @SerializedName("WebRTC")
    @JsonProperty("WebRTC")
    WebRTC("WebRTC"),

    @SerializedName("MainResourceHasCacheControlNoStore")
    @JsonProperty("MainResourceHasCacheControlNoStore")
    MainResourceHasCacheControlNoStore("MainResourceHasCacheControlNoStore"),

    @SerializedName("MainResourceHasCacheControlNoCache")
    @JsonProperty("MainResourceHasCacheControlNoCache")
    MainResourceHasCacheControlNoCache("MainResourceHasCacheControlNoCache"),

    @SerializedName("SubresourceHasCacheControlNoStore")
    @JsonProperty("SubresourceHasCacheControlNoStore")
    SubresourceHasCacheControlNoStore("SubresourceHasCacheControlNoStore"),

    @SerializedName("SubresourceHasCacheControlNoCache")
    @JsonProperty("SubresourceHasCacheControlNoCache")
    SubresourceHasCacheControlNoCache("SubresourceHasCacheControlNoCache"),

    @SerializedName("ContainsPlugins")
    @JsonProperty("ContainsPlugins")
    ContainsPlugins("ContainsPlugins"),

    @SerializedName("DocumentLoaded")
    @JsonProperty("DocumentLoaded")
    DocumentLoaded("DocumentLoaded"),

    @SerializedName("OutstandingNetworkRequestOthers")
    @JsonProperty("OutstandingNetworkRequestOthers")
    OutstandingNetworkRequestOthers("OutstandingNetworkRequestOthers"),

    @SerializedName("RequestedMIDIPermission")
    @JsonProperty("RequestedMIDIPermission")
    RequestedMIDIPermission("RequestedMIDIPermission"),

    @SerializedName("RequestedAudioCapturePermission")
    @JsonProperty("RequestedAudioCapturePermission")
    RequestedAudioCapturePermission("RequestedAudioCapturePermission"),

    @SerializedName("RequestedVideoCapturePermission")
    @JsonProperty("RequestedVideoCapturePermission")
    RequestedVideoCapturePermission("RequestedVideoCapturePermission"),

    @SerializedName("RequestedBackForwardCacheBlockedSensors")
    @JsonProperty("RequestedBackForwardCacheBlockedSensors")
    RequestedBackForwardCacheBlockedSensors("RequestedBackForwardCacheBlockedSensors"),

    @SerializedName("RequestedBackgroundWorkPermission")
    @JsonProperty("RequestedBackgroundWorkPermission")
    RequestedBackgroundWorkPermission("RequestedBackgroundWorkPermission"),

    @SerializedName("BroadcastChannel")
    @JsonProperty("BroadcastChannel")
    BroadcastChannel("BroadcastChannel"),

    @SerializedName("WebXR")
    @JsonProperty("WebXR")
    WebXR("WebXR"),

    @SerializedName("SharedWorker")
    @JsonProperty("SharedWorker")
    SharedWorker("SharedWorker"),

    @SerializedName("WebLocks")
    @JsonProperty("WebLocks")
    WebLocks("WebLocks"),

    @SerializedName("WebHID")
    @JsonProperty("WebHID")
    WebHID("WebHID"),

    @SerializedName("WebShare")
    @JsonProperty("WebShare")
    WebShare("WebShare"),

    @SerializedName("RequestedStorageAccessGrant")
    @JsonProperty("RequestedStorageAccessGrant")
    RequestedStorageAccessGrant("RequestedStorageAccessGrant"),

    @SerializedName("WebNfc")
    @JsonProperty("WebNfc")
    WebNfc("WebNfc"),

    @SerializedName("OutstandingNetworkRequestFetch")
    @JsonProperty("OutstandingNetworkRequestFetch")
    OutstandingNetworkRequestFetch("OutstandingNetworkRequestFetch"),

    @SerializedName("OutstandingNetworkRequestXHR")
    @JsonProperty("OutstandingNetworkRequestXHR")
    OutstandingNetworkRequestXHR("OutstandingNetworkRequestXHR"),

    @SerializedName("AppBanner")
    @JsonProperty("AppBanner")
    AppBanner("AppBanner"),

    @SerializedName("Printing")
    @JsonProperty("Printing")
    Printing("Printing"),

    @SerializedName("WebDatabase")
    @JsonProperty("WebDatabase")
    WebDatabase("WebDatabase"),

    @SerializedName("PictureInPicture")
    @JsonProperty("PictureInPicture")
    PictureInPicture("PictureInPicture"),

    @SerializedName("SpeechRecognizer")
    @JsonProperty("SpeechRecognizer")
    SpeechRecognizer("SpeechRecognizer"),

    @SerializedName("IdleManager")
    @JsonProperty("IdleManager")
    IdleManager("IdleManager"),

    @SerializedName("PaymentManager")
    @JsonProperty("PaymentManager")
    PaymentManager("PaymentManager"),

    @SerializedName("SpeechSynthesis")
    @JsonProperty("SpeechSynthesis")
    SpeechSynthesis("SpeechSynthesis"),

    @SerializedName("KeyboardLock")
    @JsonProperty("KeyboardLock")
    KeyboardLock("KeyboardLock"),

    @SerializedName("WebOTPService")
    @JsonProperty("WebOTPService")
    WebOTPService("WebOTPService"),

    @SerializedName("OutstandingNetworkRequestDirectSocket")
    @JsonProperty("OutstandingNetworkRequestDirectSocket")
    OutstandingNetworkRequestDirectSocket("OutstandingNetworkRequestDirectSocket"),

    @SerializedName("InjectedJavascript")
    @JsonProperty("InjectedJavascript")
    InjectedJavascript("InjectedJavascript"),

    @SerializedName("InjectedStyleSheet")
    @JsonProperty("InjectedStyleSheet")
    InjectedStyleSheet("InjectedStyleSheet"),

    @SerializedName("KeepaliveRequest")
    @JsonProperty("KeepaliveRequest")
    KeepaliveRequest("KeepaliveRequest"),

    @SerializedName("IndexedDBEvent")
    @JsonProperty("IndexedDBEvent")
    IndexedDBEvent("IndexedDBEvent"),

    @SerializedName("Dummy")
    @JsonProperty("Dummy")
    Dummy("Dummy"),

    @SerializedName("JsNetworkRequestReceivedCacheControlNoStoreResource")
    @JsonProperty("JsNetworkRequestReceivedCacheControlNoStoreResource")
    JsNetworkRequestReceivedCacheControlNoStoreResource("JsNetworkRequestReceivedCacheControlNoStoreResource"),

    @SerializedName("WebRTCSticky")
    @JsonProperty("WebRTCSticky")
    WebRTCSticky("WebRTCSticky"),

    @SerializedName("WebTransportSticky")
    @JsonProperty("WebTransportSticky")
    WebTransportSticky("WebTransportSticky"),

    @SerializedName("WebSocketSticky")
    @JsonProperty("WebSocketSticky")
    WebSocketSticky("WebSocketSticky"),

    @SerializedName("SmartCard")
    @JsonProperty("SmartCard")
    SmartCard("SmartCard"),

    @SerializedName("LiveMediaStreamTrack")
    @JsonProperty("LiveMediaStreamTrack")
    LiveMediaStreamTrack("LiveMediaStreamTrack"),

    @SerializedName("UnloadHandler")
    @JsonProperty("UnloadHandler")
    UnloadHandler("UnloadHandler"),

    @SerializedName("ParserAborted")
    @JsonProperty("ParserAborted")
    ParserAborted("ParserAborted"),

    @SerializedName("ContentSecurityHandler")
    @JsonProperty("ContentSecurityHandler")
    ContentSecurityHandler("ContentSecurityHandler"),

    @SerializedName("ContentWebAuthenticationAPI")
    @JsonProperty("ContentWebAuthenticationAPI")
    ContentWebAuthenticationAPI("ContentWebAuthenticationAPI"),

    @SerializedName("ContentFileChooser")
    @JsonProperty("ContentFileChooser")
    ContentFileChooser("ContentFileChooser"),

    @SerializedName("ContentSerial")
    @JsonProperty("ContentSerial")
    ContentSerial("ContentSerial"),

    @SerializedName("ContentFileSystemAccess")
    @JsonProperty("ContentFileSystemAccess")
    ContentFileSystemAccess("ContentFileSystemAccess"),

    @SerializedName("ContentMediaDevicesDispatcherHost")
    @JsonProperty("ContentMediaDevicesDispatcherHost")
    ContentMediaDevicesDispatcherHost("ContentMediaDevicesDispatcherHost"),

    @SerializedName("ContentWebBluetooth")
    @JsonProperty("ContentWebBluetooth")
    ContentWebBluetooth("ContentWebBluetooth"),

    @SerializedName("ContentWebUSB")
    @JsonProperty("ContentWebUSB")
    ContentWebUSB("ContentWebUSB"),

    @SerializedName("ContentMediaSessionService")
    @JsonProperty("ContentMediaSessionService")
    ContentMediaSessionService("ContentMediaSessionService"),

    @SerializedName("ContentScreenReader")
    @JsonProperty("ContentScreenReader")
    ContentScreenReader("ContentScreenReader"),

    @SerializedName("ContentDiscarded")
    @JsonProperty("ContentDiscarded")
    ContentDiscarded("ContentDiscarded"),

    @SerializedName("EmbedderPopupBlockerTabHelper")
    @JsonProperty("EmbedderPopupBlockerTabHelper")
    EmbedderPopupBlockerTabHelper("EmbedderPopupBlockerTabHelper"),

    @SerializedName("EmbedderSafeBrowsingTriggeredPopupBlocker")
    @JsonProperty("EmbedderSafeBrowsingTriggeredPopupBlocker")
    EmbedderSafeBrowsingTriggeredPopupBlocker("EmbedderSafeBrowsingTriggeredPopupBlocker"),

    @SerializedName("EmbedderSafeBrowsingThreatDetails")
    @JsonProperty("EmbedderSafeBrowsingThreatDetails")
    EmbedderSafeBrowsingThreatDetails("EmbedderSafeBrowsingThreatDetails"),

    @SerializedName("EmbedderAppBannerManager")
    @JsonProperty("EmbedderAppBannerManager")
    EmbedderAppBannerManager("EmbedderAppBannerManager"),

    @SerializedName("EmbedderDomDistillerViewerSource")
    @JsonProperty("EmbedderDomDistillerViewerSource")
    EmbedderDomDistillerViewerSource("EmbedderDomDistillerViewerSource"),

    @SerializedName("EmbedderDomDistillerSelfDeletingRequestDelegate")
    @JsonProperty("EmbedderDomDistillerSelfDeletingRequestDelegate")
    EmbedderDomDistillerSelfDeletingRequestDelegate("EmbedderDomDistillerSelfDeletingRequestDelegate"),

    @SerializedName("EmbedderOomInterventionTabHelper")
    @JsonProperty("EmbedderOomInterventionTabHelper")
    EmbedderOomInterventionTabHelper("EmbedderOomInterventionTabHelper"),

    @SerializedName("EmbedderOfflinePage")
    @JsonProperty("EmbedderOfflinePage")
    EmbedderOfflinePage("EmbedderOfflinePage"),

    @SerializedName("EmbedderChromePasswordManagerClientBindCredentialManager")
    @JsonProperty("EmbedderChromePasswordManagerClientBindCredentialManager")
    EmbedderChromePasswordManagerClientBindCredentialManager(
            "EmbedderChromePasswordManagerClientBindCredentialManager"),

    @SerializedName("EmbedderPermissionRequestManager")
    @JsonProperty("EmbedderPermissionRequestManager")
    EmbedderPermissionRequestManager("EmbedderPermissionRequestManager"),

    @SerializedName("EmbedderModalDialog")
    @JsonProperty("EmbedderModalDialog")
    EmbedderModalDialog("EmbedderModalDialog"),

    @SerializedName("EmbedderExtensions")
    @JsonProperty("EmbedderExtensions")
    EmbedderExtensions("EmbedderExtensions"),

    @SerializedName("EmbedderExtensionMessaging")
    @JsonProperty("EmbedderExtensionMessaging")
    EmbedderExtensionMessaging("EmbedderExtensionMessaging"),

    @SerializedName("EmbedderExtensionMessagingForOpenPort")
    @JsonProperty("EmbedderExtensionMessagingForOpenPort")
    EmbedderExtensionMessagingForOpenPort("EmbedderExtensionMessagingForOpenPort"),

    @SerializedName("EmbedderExtensionSentMessageToCachedFrame")
    @JsonProperty("EmbedderExtensionSentMessageToCachedFrame")
    EmbedderExtensionSentMessageToCachedFrame("EmbedderExtensionSentMessageToCachedFrame"),

    @SerializedName("RequestedByWebViewClient")
    @JsonProperty("RequestedByWebViewClient")
    RequestedByWebViewClient("RequestedByWebViewClient"),

    @SerializedName("PostMessageByWebViewClient")
    @JsonProperty("PostMessageByWebViewClient")
    PostMessageByWebViewClient("PostMessageByWebViewClient"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    BackForwardCacheNotRestoredReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
