// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.cdp4j.annotation.Experimental;

@Experimental
public class LaunchHandler {
    private String clientMode;

    public String getClientMode() {
        return clientMode;
    }

    public void setClientMode(String clientMode) {
        this.clientMode = clientMode;
    }

    public String toString() {
        return "LaunchHandler [clientMode=" + clientMode + "]";
    }
}
