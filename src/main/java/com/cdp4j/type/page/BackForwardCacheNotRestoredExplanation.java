// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class BackForwardCacheNotRestoredExplanation {
    private BackForwardCacheNotRestoredReasonType type;

    private BackForwardCacheNotRestoredReason reason;

    private String context;

    private List<BackForwardCacheBlockingDetails> details = emptyList();

    /**
     * Type of the reason
     */
    public BackForwardCacheNotRestoredReasonType getType() {
        return type;
    }

    /**
     * Type of the reason
     */
    public void setType(BackForwardCacheNotRestoredReasonType type) {
        this.type = type;
    }

    /**
     * Not restored reason
     */
    public BackForwardCacheNotRestoredReason getReason() {
        return reason;
    }

    /**
     * Not restored reason
     */
    public void setReason(BackForwardCacheNotRestoredReason reason) {
        this.reason = reason;
    }

    /**
     * Context associated with the reason. The meaning of this context is dependent
     * on the reason: - EmbedderExtensionSentMessageToCachedFrame: the extension ID.
     */
    public String getContext() {
        return context;
    }

    /**
     * Context associated with the reason. The meaning of this context is dependent
     * on the reason: - EmbedderExtensionSentMessageToCachedFrame: the extension ID.
     */
    public void setContext(String context) {
        this.context = context;
    }

    public List<BackForwardCacheBlockingDetails> getDetails() {
        return details;
    }

    public void setDetails(List<BackForwardCacheBlockingDetails> details) {
        this.details = details;
    }

    public String toString() {
        return "BackForwardCacheNotRestoredExplanation [type=" + type + ", reason=" + reason + ", context=" + context
                + ", details=" + details + "]";
    }
}
