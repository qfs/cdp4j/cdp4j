// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class OriginTrial {
    private String trialName;

    private OriginTrialStatus status;

    private List<OriginTrialTokenWithStatus> tokensWithStatus = emptyList();

    public String getTrialName() {
        return trialName;
    }

    public void setTrialName(String trialName) {
        this.trialName = trialName;
    }

    public OriginTrialStatus getStatus() {
        return status;
    }

    public void setStatus(OriginTrialStatus status) {
        this.status = status;
    }

    public List<OriginTrialTokenWithStatus> getTokensWithStatus() {
        return tokensWithStatus;
    }

    public void setTokensWithStatus(List<OriginTrialTokenWithStatus> tokensWithStatus) {
        this.tokensWithStatus = tokensWithStatus;
    }

    public String toString() {
        return "OriginTrial [trialName=" + trialName + ", status=" + status + ", tokensWithStatus=" + tokensWithStatus
                + "]";
    }
}
