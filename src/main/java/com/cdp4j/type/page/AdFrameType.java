// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Indicates whether a frame has been identified as an ad.
 */
public enum AdFrameType {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("child")
    @JsonProperty("child")
    Child("child"),

    @SerializedName("root")
    @JsonProperty("root")
    Root("root"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AdFrameType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
