// SPDX-License-Identifier: MIT
package com.cdp4j.type.page;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class FileFilter {
    private String name;

    private List<String> accepts = emptyList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAccepts() {
        return accepts;
    }

    public void setAccepts(List<String> accepts) {
        this.accepts = accepts;
    }

    public String toString() {
        return "FileFilter [name=" + name + ", accepts=" + accepts + "]";
    }
}
