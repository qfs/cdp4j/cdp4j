// SPDX-License-Identifier: MIT
package com.cdp4j.type.media;

/**
 * Corresponds to kMediaEventTriggered
 */
public class PlayerEvent {
    private Double timestamp;

    private String value;

    public Double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return "PlayerEvent [timestamp=" + timestamp + ", value=" + value + "]";
    }
}
