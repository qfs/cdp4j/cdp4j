// SPDX-License-Identifier: MIT
package com.cdp4j.type.media;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Corresponds to kMediaError
 */
public class PlayerError {
    private String errorType;

    private Integer code;

    private List<PlayerErrorSourceLocation> stack = emptyList();

    private List<PlayerError> cause = emptyList();

    private Object data;

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    /**
     * Code is the numeric enum entry for a specific set of error codes, such as
     * PipelineStatusCodes in media/base/pipeline_status.h
     */
    public Integer getCode() {
        return code;
    }

    /**
     * Code is the numeric enum entry for a specific set of error codes, such as
     * PipelineStatusCodes in media/base/pipeline_status.h
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * A trace of where this error was caused / where it passed through.
     */
    public List<PlayerErrorSourceLocation> getStack() {
        return stack;
    }

    /**
     * A trace of where this error was caused / where it passed through.
     */
    public void setStack(List<PlayerErrorSourceLocation> stack) {
        this.stack = stack;
    }

    /**
     * Errors potentially have a root cause error, ie, a DecoderError might be
     * caused by an WindowsError
     */
    public List<PlayerError> getCause() {
        return cause;
    }

    /**
     * Errors potentially have a root cause error, ie, a DecoderError might be
     * caused by an WindowsError
     */
    public void setCause(List<PlayerError> cause) {
        this.cause = cause;
    }

    /**
     * Extra data attached to an error, such as an HRESULT, Video Codec, etc.
     */
    public Object getData() {
        return data;
    }

    /**
     * Extra data attached to an error, such as an HRESULT, Video Codec, etc.
     */
    public void setData(Object data) {
        this.data = data;
    }

    public String toString() {
        return "PlayerError [errorType=" + errorType + ", code=" + code + ", stack=" + stack + ", cause=" + cause
                + ", data=" + data + "]";
    }
}
