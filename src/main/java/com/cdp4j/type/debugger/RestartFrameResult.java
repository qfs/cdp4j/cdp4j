// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

import com.cdp4j.type.runtime.StackTrace;
import com.cdp4j.type.runtime.StackTraceId;
import java.util.List;

public class RestartFrameResult {
    private List<CallFrame> callFrames;

    private StackTrace asyncStackTrace;

    private StackTraceId asyncStackTraceId;

    /**
     * New stack trace.
     */
    public List<CallFrame> getCallFrames() {
        return callFrames;
    }

    /**
     * New stack trace.
     */
    public void setCallFrames(List<CallFrame> callFrames) {
        this.callFrames = callFrames;
    }

    /**
     * Async stack trace, if any.
     */
    public StackTrace getAsyncStackTrace() {
        return asyncStackTrace;
    }

    /**
     * Async stack trace, if any.
     */
    public void setAsyncStackTrace(StackTrace asyncStackTrace) {
        this.asyncStackTrace = asyncStackTrace;
    }

    /**
     * Async stack trace, if any.
     */
    public StackTraceId getAsyncStackTraceId() {
        return asyncStackTraceId;
    }

    /**
     * Async stack trace, if any.
     */
    public void setAsyncStackTraceId(StackTraceId asyncStackTraceId) {
        this.asyncStackTraceId = asyncStackTraceId;
    }

    public String toString() {
        return "RestartFrameResult [callFrames=" + callFrames + ", asyncStackTrace=" + asyncStackTrace
                + ", asyncStackTraceId=" + asyncStackTraceId + "]";
    }
}
