// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of possible script languages.
 */
public enum ScriptLanguage {
    @SerializedName("JavaScript")
    @JsonProperty("JavaScript")
    JavaScript("JavaScript"),

    @SerializedName("WebAssembly")
    @JsonProperty("WebAssembly")
    WebAssembly("WebAssembly"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ScriptLanguage(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
