// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

import com.cdp4j.type.runtime.ExceptionDetails;
import com.cdp4j.type.runtime.RemoteObject;

public class EvaluateOnCallFrameResult {
    private RemoteObject result;

    private ExceptionDetails exceptionDetails;

    /**
     * Object wrapper for the evaluation result.
     */
    public RemoteObject getResult() {
        return result;
    }

    /**
     * Object wrapper for the evaluation result.
     */
    public void setResult(RemoteObject result) {
        this.result = result;
    }

    /**
     * Exception details.
     */
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }

    /**
     * Exception details.
     */
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String toString() {
        return "EvaluateOnCallFrameResult [result=" + result + ", exceptionDetails=" + exceptionDetails + "]";
    }
}
