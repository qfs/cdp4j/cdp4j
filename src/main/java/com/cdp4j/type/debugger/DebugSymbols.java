// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

import com.cdp4j.type.constant.DebugSymbolType;

/**
 * Debug symbols available for a wasm script.
 */
public class DebugSymbols {
    private DebugSymbolType type;

    private String externalURL;

    /**
     * Type of the debug symbols.
     */
    public DebugSymbolType getType() {
        return type;
    }

    /**
     * Type of the debug symbols.
     */
    public void setType(DebugSymbolType type) {
        this.type = type;
    }

    /**
     * URL of the external symbol source.
     */
    public String getExternalURL() {
        return externalURL;
    }

    /**
     * URL of the external symbol source.
     */
    public void setExternalURL(String externalURL) {
        this.externalURL = externalURL;
    }

    public String toString() {
        return "DebugSymbols [type=" + type + ", externalURL=" + externalURL + "]";
    }
}
