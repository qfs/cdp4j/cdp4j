// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

import com.cdp4j.type.runtime.ExceptionDetails;
import com.cdp4j.type.runtime.StackTrace;
import com.cdp4j.type.runtime.StackTraceId;
import java.util.List;

public class SetScriptSourceResult {
    private List<CallFrame> callFrames;

    private Boolean stackChanged;

    private StackTrace asyncStackTrace;

    private StackTraceId asyncStackTraceId;

    private String status;

    private ExceptionDetails exceptionDetails;

    /**
     * New stack trace in case editing has happened while VM was stopped.
     */
    public List<CallFrame> getCallFrames() {
        return callFrames;
    }

    /**
     * New stack trace in case editing has happened while VM was stopped.
     */
    public void setCallFrames(List<CallFrame> callFrames) {
        this.callFrames = callFrames;
    }

    /**
     * Whether current call stack was modified after applying the changes.
     */
    public Boolean getStackChanged() {
        return stackChanged;
    }

    /**
     * Whether current call stack was modified after applying the changes.
     */
    public void setStackChanged(Boolean stackChanged) {
        this.stackChanged = stackChanged;
    }

    /**
     * Async stack trace, if any.
     */
    public StackTrace getAsyncStackTrace() {
        return asyncStackTrace;
    }

    /**
     * Async stack trace, if any.
     */
    public void setAsyncStackTrace(StackTrace asyncStackTrace) {
        this.asyncStackTrace = asyncStackTrace;
    }

    /**
     * Async stack trace, if any.
     */
    public StackTraceId getAsyncStackTraceId() {
        return asyncStackTraceId;
    }

    /**
     * Async stack trace, if any.
     */
    public void setAsyncStackTraceId(StackTraceId asyncStackTraceId) {
        this.asyncStackTraceId = asyncStackTraceId;
    }

    /**
     * Whether the operation was successful or not. Only Ok denotes a successful
     * live edit while the other enum variants denote why the live edit failed.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Whether the operation was successful or not. Only Ok denotes a successful
     * live edit while the other enum variants denote why the live edit failed.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Exception details if any. Only present when status is CompileError.
     */
    public ExceptionDetails getExceptionDetails() {
        return exceptionDetails;
    }

    /**
     * Exception details if any. Only present when status is CompileError.
     */
    public void setExceptionDetails(ExceptionDetails exceptionDetails) {
        this.exceptionDetails = exceptionDetails;
    }

    public String toString() {
        return "SetScriptSourceResult [callFrames=" + callFrames + ", stackChanged=" + stackChanged
                + ", asyncStackTrace=" + asyncStackTrace + ", asyncStackTraceId=" + asyncStackTraceId + ", status="
                + status + ", exceptionDetails=" + exceptionDetails + "]";
    }
}
