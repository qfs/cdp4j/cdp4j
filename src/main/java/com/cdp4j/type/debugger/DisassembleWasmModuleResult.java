// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

public class DisassembleWasmModuleResult {
    private String streamId;

    private Integer totalNumberOfLines;

    private WasmDisassemblyChunk chunk;

    /**
     * For large modules, return a stream from which additional chunks of
     * disassembly can be read successively.
     */
    public String getStreamId() {
        return streamId;
    }

    /**
     * For large modules, return a stream from which additional chunks of
     * disassembly can be read successively.
     */
    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    /**
     * The total number of lines in the disassembly text.
     */
    public Integer getTotalNumberOfLines() {
        return totalNumberOfLines;
    }

    /**
     * The total number of lines in the disassembly text.
     */
    public void setTotalNumberOfLines(Integer totalNumberOfLines) {
        this.totalNumberOfLines = totalNumberOfLines;
    }

    /**
     * The first chunk of disassembly.
     */
    public WasmDisassemblyChunk getChunk() {
        return chunk;
    }

    /**
     * The first chunk of disassembly.
     */
    public void setChunk(WasmDisassemblyChunk chunk) {
        this.chunk = chunk;
    }

    public String toString() {
        return "DisassembleWasmModuleResult [streamId=" + streamId + ", totalNumberOfLines=" + totalNumberOfLines
                + ", chunk=" + chunk + "]";
    }
}
