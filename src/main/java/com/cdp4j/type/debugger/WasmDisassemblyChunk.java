// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class WasmDisassemblyChunk {
    private List<String> lines = emptyList();

    private List<Integer> bytecodeOffsets = emptyList();

    /**
     * The next chunk of disassembled lines.
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * The next chunk of disassembled lines.
     */
    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    /**
     * The bytecode offsets describing the start of each line.
     */
    public List<Integer> getBytecodeOffsets() {
        return bytecodeOffsets;
    }

    /**
     * The bytecode offsets describing the start of each line.
     */
    public void setBytecodeOffsets(List<Integer> bytecodeOffsets) {
        this.bytecodeOffsets = bytecodeOffsets;
    }

    public String toString() {
        return "WasmDisassemblyChunk [lines=" + lines + ", bytecodeOffsets=" + bytecodeOffsets + "]";
    }
}
