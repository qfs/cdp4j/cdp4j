// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

import static java.util.Collections.emptyList;

import com.cdp4j.type.runtime.RemoteObject;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * JavaScript call frame. Array of call frames form the call stack.
 */
public class CallFrame {
    private String callFrameId;

    private String functionName;

    private Location functionLocation;

    private Location location;

    private String url;

    private List<Scope> scopeChain = emptyList();

    @SerializedName("this")
    @JsonProperty("this")
    private RemoteObject that;

    private RemoteObject returnValue;

    private Boolean canBeRestarted;

    /**
     * Call frame identifier. This identifier is only valid while the virtual
     * machine is paused.
     */
    public String getCallFrameId() {
        return callFrameId;
    }

    /**
     * Call frame identifier. This identifier is only valid while the virtual
     * machine is paused.
     */
    public void setCallFrameId(String callFrameId) {
        this.callFrameId = callFrameId;
    }

    /**
     * Name of the JavaScript function called on this call frame.
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * Name of the JavaScript function called on this call frame.
     */
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    /**
     * Location in the source code.
     */
    public Location getFunctionLocation() {
        return functionLocation;
    }

    /**
     * Location in the source code.
     */
    public void setFunctionLocation(Location functionLocation) {
        this.functionLocation = functionLocation;
    }

    /**
     * Location in the source code.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Location in the source code.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * JavaScript script name or url. Deprecated in favor of using the
     * location.scriptId to resolve the URL via a previously sent
     * Debugger.scriptParsed event.
     */
    @Deprecated
    public String getUrl() {
        return url;
    }

    /**
     * JavaScript script name or url. Deprecated in favor of using the
     * location.scriptId to resolve the URL via a previously sent
     * Debugger.scriptParsed event.
     */
    @Deprecated
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Scope chain for this call frame.
     */
    public List<Scope> getScopeChain() {
        return scopeChain;
    }

    /**
     * Scope chain for this call frame.
     */
    public void setScopeChain(List<Scope> scopeChain) {
        this.scopeChain = scopeChain;
    }

    /**
     * this object for this call frame.
     */
    public RemoteObject getThat() {
        return that;
    }

    /**
     * this object for this call frame.
     */
    public void setThat(RemoteObject that) {
        this.that = that;
    }

    /**
     * The value being returned, if the function is at return point.
     */
    public RemoteObject getReturnValue() {
        return returnValue;
    }

    /**
     * The value being returned, if the function is at return point.
     */
    public void setReturnValue(RemoteObject returnValue) {
        this.returnValue = returnValue;
    }

    /**
     * Valid only while the VM is paused and indicates whether this frame can be
     * restarted or not. Note that a true value here does not guarantee that
     * Debugger#restartFrame with this CallFrameId will be successful, but it is
     * very likely.
     */
    public Boolean isCanBeRestarted() {
        return canBeRestarted;
    }

    /**
     * Valid only while the VM is paused and indicates whether this frame can be
     * restarted or not. Note that a true value here does not guarantee that
     * Debugger#restartFrame with this CallFrameId will be successful, but it is
     * very likely.
     */
    public void setCanBeRestarted(Boolean canBeRestarted) {
        this.canBeRestarted = canBeRestarted;
    }

    public String toString() {
        return "CallFrame [callFrameId=" + callFrameId + ", functionName=" + functionName + ", functionLocation="
                + functionLocation + ", location=" + location + ", url=" + url + ", scopeChain=" + scopeChain
                + ", that=" + that + ", returnValue=" + returnValue + ", canBeRestarted=" + canBeRestarted + "]";
    }
}
