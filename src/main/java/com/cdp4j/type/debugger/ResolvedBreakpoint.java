// SPDX-License-Identifier: MIT
package com.cdp4j.type.debugger;

public class ResolvedBreakpoint {
    private String breakpointId;

    private Location location;

    /**
     * Breakpoint unique identifier.
     */
    public String getBreakpointId() {
        return breakpointId;
    }

    /**
     * Breakpoint unique identifier.
     */
    public void setBreakpointId(String breakpointId) {
        this.breakpointId = breakpointId;
    }

    /**
     * Actual breakpoint location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Actual breakpoint location.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    public String toString() {
        return "ResolvedBreakpoint [breakpointId=" + breakpointId + ", location=" + location + "]";
    }
}
