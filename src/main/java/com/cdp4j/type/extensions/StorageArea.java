// SPDX-License-Identifier: MIT
package com.cdp4j.type.extensions;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Storage areas.
 */
public enum StorageArea {
    @SerializedName("session")
    @JsonProperty("session")
    Session("session"),

    @SerializedName("local")
    @JsonProperty("local")
    Local("local"),

    @SerializedName("sync")
    @JsonProperty("sync")
    Sync("sync"),

    @SerializedName("managed")
    @JsonProperty("managed")
    Managed("managed"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    StorageArea(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
