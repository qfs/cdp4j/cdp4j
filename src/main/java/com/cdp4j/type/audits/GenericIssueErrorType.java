// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum GenericIssueErrorType {
    @SerializedName("FormLabelForNameError")
    @JsonProperty("FormLabelForNameError")
    FormLabelForNameError("FormLabelForNameError"),

    @SerializedName("FormDuplicateIdForInputError")
    @JsonProperty("FormDuplicateIdForInputError")
    FormDuplicateIdForInputError("FormDuplicateIdForInputError"),

    @SerializedName("FormInputWithNoLabelError")
    @JsonProperty("FormInputWithNoLabelError")
    FormInputWithNoLabelError("FormInputWithNoLabelError"),

    @SerializedName("FormAutocompleteAttributeEmptyError")
    @JsonProperty("FormAutocompleteAttributeEmptyError")
    FormAutocompleteAttributeEmptyError("FormAutocompleteAttributeEmptyError"),

    @SerializedName("FormEmptyIdAndNameAttributesForInputError")
    @JsonProperty("FormEmptyIdAndNameAttributesForInputError")
    FormEmptyIdAndNameAttributesForInputError("FormEmptyIdAndNameAttributesForInputError"),

    @SerializedName("FormAriaLabelledByToNonExistingId")
    @JsonProperty("FormAriaLabelledByToNonExistingId")
    FormAriaLabelledByToNonExistingId("FormAriaLabelledByToNonExistingId"),

    @SerializedName("FormInputAssignedAutocompleteValueToIdOrNameAttributeError")
    @JsonProperty("FormInputAssignedAutocompleteValueToIdOrNameAttributeError")
    FormInputAssignedAutocompleteValueToIdOrNameAttributeError(
            "FormInputAssignedAutocompleteValueToIdOrNameAttributeError"),

    @SerializedName("FormLabelHasNeitherForNorNestedInput")
    @JsonProperty("FormLabelHasNeitherForNorNestedInput")
    FormLabelHasNeitherForNorNestedInput("FormLabelHasNeitherForNorNestedInput"),

    @SerializedName("FormLabelForMatchesNonExistingIdError")
    @JsonProperty("FormLabelForMatchesNonExistingIdError")
    FormLabelForMatchesNonExistingIdError("FormLabelForMatchesNonExistingIdError"),

    @SerializedName("FormInputHasWrongButWellIntendedAutocompleteValueError")
    @JsonProperty("FormInputHasWrongButWellIntendedAutocompleteValueError")
    FormInputHasWrongButWellIntendedAutocompleteValueError("FormInputHasWrongButWellIntendedAutocompleteValueError"),

    @SerializedName("ResponseWasBlockedByORB")
    @JsonProperty("ResponseWasBlockedByORB")
    ResponseWasBlockedByORB("ResponseWasBlockedByORB"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    GenericIssueErrorType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
