// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum indicating the reason a response has been blocked. These reasons are
 * refinements of the net error BLOCKED_BY_RESPONSE.
 */
public enum BlockedByResponseReason {
    @SerializedName("CoepFrameResourceNeedsCoepHeader")
    @JsonProperty("CoepFrameResourceNeedsCoepHeader")
    CoepFrameResourceNeedsCoepHeader("CoepFrameResourceNeedsCoepHeader"),

    @SerializedName("CoopSandboxedIFrameCannotNavigateToCoopPage")
    @JsonProperty("CoopSandboxedIFrameCannotNavigateToCoopPage")
    CoopSandboxedIFrameCannotNavigateToCoopPage("CoopSandboxedIFrameCannotNavigateToCoopPage"),

    @SerializedName("CorpNotSameOrigin")
    @JsonProperty("CorpNotSameOrigin")
    CorpNotSameOrigin("CorpNotSameOrigin"),

    @SerializedName("CorpNotSameOriginAfterDefaultedToSameOriginByCoep")
    @JsonProperty("CorpNotSameOriginAfterDefaultedToSameOriginByCoep")
    CorpNotSameOriginAfterDefaultedToSameOriginByCoep("CorpNotSameOriginAfterDefaultedToSameOriginByCoep"),

    @SerializedName("CorpNotSameOriginAfterDefaultedToSameOriginByDip")
    @JsonProperty("CorpNotSameOriginAfterDefaultedToSameOriginByDip")
    CorpNotSameOriginAfterDefaultedToSameOriginByDip("CorpNotSameOriginAfterDefaultedToSameOriginByDip"),

    @SerializedName("CorpNotSameOriginAfterDefaultedToSameOriginByCoepAndDip")
    @JsonProperty("CorpNotSameOriginAfterDefaultedToSameOriginByCoepAndDip")
    CorpNotSameOriginAfterDefaultedToSameOriginByCoepAndDip("CorpNotSameOriginAfterDefaultedToSameOriginByCoepAndDip"),

    @SerializedName("CorpNotSameSite")
    @JsonProperty("CorpNotSameSite")
    CorpNotSameSite("CorpNotSameSite"),

    @SerializedName("SRIMessageSignatureMismatch")
    @JsonProperty("SRIMessageSignatureMismatch")
    SRIMessageSignatureMismatch("SRIMessageSignatureMismatch"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    BlockedByResponseReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
