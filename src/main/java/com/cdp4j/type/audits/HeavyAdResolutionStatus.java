// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum HeavyAdResolutionStatus {
    @SerializedName("HeavyAdBlocked")
    @JsonProperty("HeavyAdBlocked")
    HeavyAdBlocked("HeavyAdBlocked"),

    @SerializedName("HeavyAdWarning")
    @JsonProperty("HeavyAdWarning")
    HeavyAdWarning("HeavyAdWarning"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    HeavyAdResolutionStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
