// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum SharedDictionaryError {
    @SerializedName("UseErrorCrossOriginNoCorsRequest")
    @JsonProperty("UseErrorCrossOriginNoCorsRequest")
    UseErrorCrossOriginNoCorsRequest("UseErrorCrossOriginNoCorsRequest"),

    @SerializedName("UseErrorDictionaryLoadFailure")
    @JsonProperty("UseErrorDictionaryLoadFailure")
    UseErrorDictionaryLoadFailure("UseErrorDictionaryLoadFailure"),

    @SerializedName("UseErrorMatchingDictionaryNotUsed")
    @JsonProperty("UseErrorMatchingDictionaryNotUsed")
    UseErrorMatchingDictionaryNotUsed("UseErrorMatchingDictionaryNotUsed"),

    @SerializedName("UseErrorUnexpectedContentDictionaryHeader")
    @JsonProperty("UseErrorUnexpectedContentDictionaryHeader")
    UseErrorUnexpectedContentDictionaryHeader("UseErrorUnexpectedContentDictionaryHeader"),

    @SerializedName("WriteErrorCossOriginNoCorsRequest")
    @JsonProperty("WriteErrorCossOriginNoCorsRequest")
    WriteErrorCossOriginNoCorsRequest("WriteErrorCossOriginNoCorsRequest"),

    @SerializedName("WriteErrorDisallowedBySettings")
    @JsonProperty("WriteErrorDisallowedBySettings")
    WriteErrorDisallowedBySettings("WriteErrorDisallowedBySettings"),

    @SerializedName("WriteErrorExpiredResponse")
    @JsonProperty("WriteErrorExpiredResponse")
    WriteErrorExpiredResponse("WriteErrorExpiredResponse"),

    @SerializedName("WriteErrorFeatureDisabled")
    @JsonProperty("WriteErrorFeatureDisabled")
    WriteErrorFeatureDisabled("WriteErrorFeatureDisabled"),

    @SerializedName("WriteErrorInsufficientResources")
    @JsonProperty("WriteErrorInsufficientResources")
    WriteErrorInsufficientResources("WriteErrorInsufficientResources"),

    @SerializedName("WriteErrorInvalidMatchField")
    @JsonProperty("WriteErrorInvalidMatchField")
    WriteErrorInvalidMatchField("WriteErrorInvalidMatchField"),

    @SerializedName("WriteErrorInvalidStructuredHeader")
    @JsonProperty("WriteErrorInvalidStructuredHeader")
    WriteErrorInvalidStructuredHeader("WriteErrorInvalidStructuredHeader"),

    @SerializedName("WriteErrorNavigationRequest")
    @JsonProperty("WriteErrorNavigationRequest")
    WriteErrorNavigationRequest("WriteErrorNavigationRequest"),

    @SerializedName("WriteErrorNoMatchField")
    @JsonProperty("WriteErrorNoMatchField")
    WriteErrorNoMatchField("WriteErrorNoMatchField"),

    @SerializedName("WriteErrorNonListMatchDestField")
    @JsonProperty("WriteErrorNonListMatchDestField")
    WriteErrorNonListMatchDestField("WriteErrorNonListMatchDestField"),

    @SerializedName("WriteErrorNonSecureContext")
    @JsonProperty("WriteErrorNonSecureContext")
    WriteErrorNonSecureContext("WriteErrorNonSecureContext"),

    @SerializedName("WriteErrorNonStringIdField")
    @JsonProperty("WriteErrorNonStringIdField")
    WriteErrorNonStringIdField("WriteErrorNonStringIdField"),

    @SerializedName("WriteErrorNonStringInMatchDestList")
    @JsonProperty("WriteErrorNonStringInMatchDestList")
    WriteErrorNonStringInMatchDestList("WriteErrorNonStringInMatchDestList"),

    @SerializedName("WriteErrorNonStringMatchField")
    @JsonProperty("WriteErrorNonStringMatchField")
    WriteErrorNonStringMatchField("WriteErrorNonStringMatchField"),

    @SerializedName("WriteErrorNonTokenTypeField")
    @JsonProperty("WriteErrorNonTokenTypeField")
    WriteErrorNonTokenTypeField("WriteErrorNonTokenTypeField"),

    @SerializedName("WriteErrorRequestAborted")
    @JsonProperty("WriteErrorRequestAborted")
    WriteErrorRequestAborted("WriteErrorRequestAborted"),

    @SerializedName("WriteErrorShuttingDown")
    @JsonProperty("WriteErrorShuttingDown")
    WriteErrorShuttingDown("WriteErrorShuttingDown"),

    @SerializedName("WriteErrorTooLongIdField")
    @JsonProperty("WriteErrorTooLongIdField")
    WriteErrorTooLongIdField("WriteErrorTooLongIdField"),

    @SerializedName("WriteErrorUnsupportedType")
    @JsonProperty("WriteErrorUnsupportedType")
    WriteErrorUnsupportedType("WriteErrorUnsupportedType"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SharedDictionaryError(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
