// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ClientHintIssueReason {
    @SerializedName("MetaTagAllowListInvalidOrigin")
    @JsonProperty("MetaTagAllowListInvalidOrigin")
    MetaTagAllowListInvalidOrigin("MetaTagAllowListInvalidOrigin"),

    @SerializedName("MetaTagModifiedHTML")
    @JsonProperty("MetaTagModifiedHTML")
    MetaTagModifiedHTML("MetaTagModifiedHTML"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ClientHintIssueReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
