// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum SharedArrayBufferIssueType {
    @SerializedName("TransferIssue")
    @JsonProperty("TransferIssue")
    TransferIssue("TransferIssue"),

    @SerializedName("CreationIssue")
    @JsonProperty("CreationIssue")
    CreationIssue("CreationIssue"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SharedArrayBufferIssueType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
