// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PropertyRuleIssueReason {
    @SerializedName("InvalidSyntax")
    @JsonProperty("InvalidSyntax")
    InvalidSyntax("InvalidSyntax"),

    @SerializedName("InvalidInitialValue")
    @JsonProperty("InvalidInitialValue")
    InvalidInitialValue("InvalidInitialValue"),

    @SerializedName("InvalidInherits")
    @JsonProperty("InvalidInherits")
    InvalidInherits("InvalidInherits"),

    @SerializedName("InvalidName")
    @JsonProperty("InvalidName")
    InvalidName("InvalidName"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PropertyRuleIssueReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
