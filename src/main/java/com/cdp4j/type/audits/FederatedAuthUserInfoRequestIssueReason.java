// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Represents the failure reason when a getUserInfo() call fails.
 * Should be updated alongside FederatedAuthUserInfoRequestResult in
 * third_party/blink/public/mojom/devtools/inspector_issue.mojom.
 */
public enum FederatedAuthUserInfoRequestIssueReason {
    @SerializedName("NotSameOrigin")
    @JsonProperty("NotSameOrigin")
    NotSameOrigin("NotSameOrigin"),

    @SerializedName("NotIframe")
    @JsonProperty("NotIframe")
    NotIframe("NotIframe"),

    @SerializedName("NotPotentiallyTrustworthy")
    @JsonProperty("NotPotentiallyTrustworthy")
    NotPotentiallyTrustworthy("NotPotentiallyTrustworthy"),

    @SerializedName("NoApiPermission")
    @JsonProperty("NoApiPermission")
    NoApiPermission("NoApiPermission"),

    @SerializedName("NotSignedInWithIdp")
    @JsonProperty("NotSignedInWithIdp")
    NotSignedInWithIdp("NotSignedInWithIdp"),

    @SerializedName("NoAccountSharingPermission")
    @JsonProperty("NoAccountSharingPermission")
    NoAccountSharingPermission("NoAccountSharingPermission"),

    @SerializedName("InvalidConfigOrWellKnown")
    @JsonProperty("InvalidConfigOrWellKnown")
    InvalidConfigOrWellKnown("InvalidConfigOrWellKnown"),

    @SerializedName("InvalidAccountsResponse")
    @JsonProperty("InvalidAccountsResponse")
    InvalidAccountsResponse("InvalidAccountsResponse"),

    @SerializedName("NoReturningUserFromFetchedAccounts")
    @JsonProperty("NoReturningUserFromFetchedAccounts")
    NoReturningUserFromFetchedAccounts("NoReturningUserFromFetchedAccounts"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    FederatedAuthUserInfoRequestIssueReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
