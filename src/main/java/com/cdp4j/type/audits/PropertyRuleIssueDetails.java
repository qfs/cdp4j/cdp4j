// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * This issue warns about errors in property rules that lead to property
 * registrations being ignored.
 */
public class PropertyRuleIssueDetails {
    private SourceCodeLocation sourceCodeLocation;

    private PropertyRuleIssueReason propertyRuleIssueReason;

    private String propertyValue;

    /**
     * Source code position of the property rule.
     */
    public SourceCodeLocation getSourceCodeLocation() {
        return sourceCodeLocation;
    }

    /**
     * Source code position of the property rule.
     */
    public void setSourceCodeLocation(SourceCodeLocation sourceCodeLocation) {
        this.sourceCodeLocation = sourceCodeLocation;
    }

    /**
     * Reason why the property rule was discarded.
     */
    public PropertyRuleIssueReason getPropertyRuleIssueReason() {
        return propertyRuleIssueReason;
    }

    /**
     * Reason why the property rule was discarded.
     */
    public void setPropertyRuleIssueReason(PropertyRuleIssueReason propertyRuleIssueReason) {
        this.propertyRuleIssueReason = propertyRuleIssueReason;
    }

    /**
     * The value of the property rule property that failed to parse
     */
    public String getPropertyValue() {
        return propertyValue;
    }

    /**
     * The value of the property rule property that failed to parse
     */
    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public String toString() {
        return "PropertyRuleIssueDetails [sourceCodeLocation=" + sourceCodeLocation + ", propertyRuleIssueReason="
                + propertyRuleIssueReason + ", propertyValue=" + propertyValue + "]";
    }
}
