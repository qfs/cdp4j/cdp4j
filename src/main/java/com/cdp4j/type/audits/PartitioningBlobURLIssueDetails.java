// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

public class PartitioningBlobURLIssueDetails {
    private String url;

    private PartitioningBlobURLInfo partitioningBlobURLInfo;

    /**
     * The BlobURL that failed to load.
     */
    public String getUrl() {
        return url;
    }

    /**
     * The BlobURL that failed to load.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Additional information about the Partitioning Blob URL issue.
     */
    public PartitioningBlobURLInfo getPartitioningBlobURLInfo() {
        return partitioningBlobURLInfo;
    }

    /**
     * Additional information about the Partitioning Blob URL issue.
     */
    public void setPartitioningBlobURLInfo(PartitioningBlobURLInfo partitioningBlobURLInfo) {
        this.partitioningBlobURLInfo = partitioningBlobURLInfo;
    }

    public String toString() {
        return "PartitioningBlobURLIssueDetails [url=" + url + ", partitioningBlobURLInfo=" + partitioningBlobURLInfo
                + "]";
    }
}
