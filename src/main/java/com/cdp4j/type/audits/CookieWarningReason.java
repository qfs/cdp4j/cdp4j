// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum CookieWarningReason {
    @SerializedName("WarnSameSiteUnspecifiedCrossSiteContext")
    @JsonProperty("WarnSameSiteUnspecifiedCrossSiteContext")
    WarnSameSiteUnspecifiedCrossSiteContext("WarnSameSiteUnspecifiedCrossSiteContext"),

    @SerializedName("WarnSameSiteNoneInsecure")
    @JsonProperty("WarnSameSiteNoneInsecure")
    WarnSameSiteNoneInsecure("WarnSameSiteNoneInsecure"),

    @SerializedName("WarnSameSiteUnspecifiedLaxAllowUnsafe")
    @JsonProperty("WarnSameSiteUnspecifiedLaxAllowUnsafe")
    WarnSameSiteUnspecifiedLaxAllowUnsafe("WarnSameSiteUnspecifiedLaxAllowUnsafe"),

    @SerializedName("WarnSameSiteStrictLaxDowngradeStrict")
    @JsonProperty("WarnSameSiteStrictLaxDowngradeStrict")
    WarnSameSiteStrictLaxDowngradeStrict("WarnSameSiteStrictLaxDowngradeStrict"),

    @SerializedName("WarnSameSiteStrictCrossDowngradeStrict")
    @JsonProperty("WarnSameSiteStrictCrossDowngradeStrict")
    WarnSameSiteStrictCrossDowngradeStrict("WarnSameSiteStrictCrossDowngradeStrict"),

    @SerializedName("WarnSameSiteStrictCrossDowngradeLax")
    @JsonProperty("WarnSameSiteStrictCrossDowngradeLax")
    WarnSameSiteStrictCrossDowngradeLax("WarnSameSiteStrictCrossDowngradeLax"),

    @SerializedName("WarnSameSiteLaxCrossDowngradeStrict")
    @JsonProperty("WarnSameSiteLaxCrossDowngradeStrict")
    WarnSameSiteLaxCrossDowngradeStrict("WarnSameSiteLaxCrossDowngradeStrict"),

    @SerializedName("WarnSameSiteLaxCrossDowngradeLax")
    @JsonProperty("WarnSameSiteLaxCrossDowngradeLax")
    WarnSameSiteLaxCrossDowngradeLax("WarnSameSiteLaxCrossDowngradeLax"),

    @SerializedName("WarnAttributeValueExceedsMaxSize")
    @JsonProperty("WarnAttributeValueExceedsMaxSize")
    WarnAttributeValueExceedsMaxSize("WarnAttributeValueExceedsMaxSize"),

    @SerializedName("WarnDomainNonASCII")
    @JsonProperty("WarnDomainNonASCII")
    WarnDomainNonASCII("WarnDomainNonASCII"),

    @SerializedName("WarnThirdPartyPhaseout")
    @JsonProperty("WarnThirdPartyPhaseout")
    WarnThirdPartyPhaseout("WarnThirdPartyPhaseout"),

    @SerializedName("WarnCrossSiteRedirectDowngradeChangesInclusion")
    @JsonProperty("WarnCrossSiteRedirectDowngradeChangesInclusion")
    WarnCrossSiteRedirectDowngradeChangesInclusion("WarnCrossSiteRedirectDowngradeChangesInclusion"),

    @SerializedName("WarnDeprecationTrialMetadata")
    @JsonProperty("WarnDeprecationTrialMetadata")
    WarnDeprecationTrialMetadata("WarnDeprecationTrialMetadata"),

    @SerializedName("WarnThirdPartyCookieHeuristic")
    @JsonProperty("WarnThirdPartyCookieHeuristic")
    WarnThirdPartyCookieHeuristic("WarnThirdPartyCookieHeuristic"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookieWarningReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
