// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum SelectElementAccessibilityIssueReason {
    @SerializedName("DisallowedSelectChild")
    @JsonProperty("DisallowedSelectChild")
    DisallowedSelectChild("DisallowedSelectChild"),

    @SerializedName("DisallowedOptGroupChild")
    @JsonProperty("DisallowedOptGroupChild")
    DisallowedOptGroupChild("DisallowedOptGroupChild"),

    @SerializedName("NonPhrasingContentOptionChild")
    @JsonProperty("NonPhrasingContentOptionChild")
    NonPhrasingContentOptionChild("NonPhrasingContentOptionChild"),

    @SerializedName("InteractiveContentOptionChild")
    @JsonProperty("InteractiveContentOptionChild")
    InteractiveContentOptionChild("InteractiveContentOptionChild"),

    @SerializedName("InteractiveContentLegendChild")
    @JsonProperty("InteractiveContentLegendChild")
    InteractiveContentLegendChild("InteractiveContentLegendChild"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SelectElementAccessibilityIssueReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
