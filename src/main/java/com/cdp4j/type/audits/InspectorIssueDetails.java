// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * This struct holds a list of optional fields with additional information
 * specific to the kind of issue. When adding a new issue code, please also add
 * a new optional field to this type.
 */
public class InspectorIssueDetails {
    private CookieIssueDetails cookieIssueDetails;

    private MixedContentIssueDetails mixedContentIssueDetails;

    private BlockedByResponseIssueDetails blockedByResponseIssueDetails;

    private HeavyAdIssueDetails heavyAdIssueDetails;

    private ContentSecurityPolicyIssueDetails contentSecurityPolicyIssueDetails;

    private SharedArrayBufferIssueDetails sharedArrayBufferIssueDetails;

    private LowTextContrastIssueDetails lowTextContrastIssueDetails;

    private CorsIssueDetails corsIssueDetails;

    private AttributionReportingIssueDetails attributionReportingIssueDetails;

    private QuirksModeIssueDetails quirksModeIssueDetails;

    private PartitioningBlobURLIssueDetails partitioningBlobURLIssueDetails;

    private NavigatorUserAgentIssueDetails navigatorUserAgentIssueDetails;

    private GenericIssueDetails genericIssueDetails;

    private DeprecationIssueDetails deprecationIssueDetails;

    private ClientHintIssueDetails clientHintIssueDetails;

    private FederatedAuthRequestIssueDetails federatedAuthRequestIssueDetails;

    private BounceTrackingIssueDetails bounceTrackingIssueDetails;

    private CookieDeprecationMetadataIssueDetails cookieDeprecationMetadataIssueDetails;

    private StylesheetLoadingIssueDetails stylesheetLoadingIssueDetails;

    private PropertyRuleIssueDetails propertyRuleIssueDetails;

    private FederatedAuthUserInfoRequestIssueDetails federatedAuthUserInfoRequestIssueDetails;

    private SharedDictionaryIssueDetails sharedDictionaryIssueDetails;

    private SelectElementAccessibilityIssueDetails selectElementAccessibilityIssueDetails;

    public CookieIssueDetails getCookieIssueDetails() {
        return cookieIssueDetails;
    }

    public void setCookieIssueDetails(CookieIssueDetails cookieIssueDetails) {
        this.cookieIssueDetails = cookieIssueDetails;
    }

    public MixedContentIssueDetails getMixedContentIssueDetails() {
        return mixedContentIssueDetails;
    }

    public void setMixedContentIssueDetails(MixedContentIssueDetails mixedContentIssueDetails) {
        this.mixedContentIssueDetails = mixedContentIssueDetails;
    }

    public BlockedByResponseIssueDetails getBlockedByResponseIssueDetails() {
        return blockedByResponseIssueDetails;
    }

    public void setBlockedByResponseIssueDetails(BlockedByResponseIssueDetails blockedByResponseIssueDetails) {
        this.blockedByResponseIssueDetails = blockedByResponseIssueDetails;
    }

    public HeavyAdIssueDetails getHeavyAdIssueDetails() {
        return heavyAdIssueDetails;
    }

    public void setHeavyAdIssueDetails(HeavyAdIssueDetails heavyAdIssueDetails) {
        this.heavyAdIssueDetails = heavyAdIssueDetails;
    }

    public ContentSecurityPolicyIssueDetails getContentSecurityPolicyIssueDetails() {
        return contentSecurityPolicyIssueDetails;
    }

    public void setContentSecurityPolicyIssueDetails(
            ContentSecurityPolicyIssueDetails contentSecurityPolicyIssueDetails) {
        this.contentSecurityPolicyIssueDetails = contentSecurityPolicyIssueDetails;
    }

    public SharedArrayBufferIssueDetails getSharedArrayBufferIssueDetails() {
        return sharedArrayBufferIssueDetails;
    }

    public void setSharedArrayBufferIssueDetails(SharedArrayBufferIssueDetails sharedArrayBufferIssueDetails) {
        this.sharedArrayBufferIssueDetails = sharedArrayBufferIssueDetails;
    }

    public LowTextContrastIssueDetails getLowTextContrastIssueDetails() {
        return lowTextContrastIssueDetails;
    }

    public void setLowTextContrastIssueDetails(LowTextContrastIssueDetails lowTextContrastIssueDetails) {
        this.lowTextContrastIssueDetails = lowTextContrastIssueDetails;
    }

    public CorsIssueDetails getCorsIssueDetails() {
        return corsIssueDetails;
    }

    public void setCorsIssueDetails(CorsIssueDetails corsIssueDetails) {
        this.corsIssueDetails = corsIssueDetails;
    }

    public AttributionReportingIssueDetails getAttributionReportingIssueDetails() {
        return attributionReportingIssueDetails;
    }

    public void setAttributionReportingIssueDetails(AttributionReportingIssueDetails attributionReportingIssueDetails) {
        this.attributionReportingIssueDetails = attributionReportingIssueDetails;
    }

    public QuirksModeIssueDetails getQuirksModeIssueDetails() {
        return quirksModeIssueDetails;
    }

    public void setQuirksModeIssueDetails(QuirksModeIssueDetails quirksModeIssueDetails) {
        this.quirksModeIssueDetails = quirksModeIssueDetails;
    }

    public PartitioningBlobURLIssueDetails getPartitioningBlobURLIssueDetails() {
        return partitioningBlobURLIssueDetails;
    }

    public void setPartitioningBlobURLIssueDetails(PartitioningBlobURLIssueDetails partitioningBlobURLIssueDetails) {
        this.partitioningBlobURLIssueDetails = partitioningBlobURLIssueDetails;
    }

    @Deprecated
    public NavigatorUserAgentIssueDetails getNavigatorUserAgentIssueDetails() {
        return navigatorUserAgentIssueDetails;
    }

    @Deprecated
    public void setNavigatorUserAgentIssueDetails(NavigatorUserAgentIssueDetails navigatorUserAgentIssueDetails) {
        this.navigatorUserAgentIssueDetails = navigatorUserAgentIssueDetails;
    }

    public GenericIssueDetails getGenericIssueDetails() {
        return genericIssueDetails;
    }

    public void setGenericIssueDetails(GenericIssueDetails genericIssueDetails) {
        this.genericIssueDetails = genericIssueDetails;
    }

    public DeprecationIssueDetails getDeprecationIssueDetails() {
        return deprecationIssueDetails;
    }

    public void setDeprecationIssueDetails(DeprecationIssueDetails deprecationIssueDetails) {
        this.deprecationIssueDetails = deprecationIssueDetails;
    }

    public ClientHintIssueDetails getClientHintIssueDetails() {
        return clientHintIssueDetails;
    }

    public void setClientHintIssueDetails(ClientHintIssueDetails clientHintIssueDetails) {
        this.clientHintIssueDetails = clientHintIssueDetails;
    }

    public FederatedAuthRequestIssueDetails getFederatedAuthRequestIssueDetails() {
        return federatedAuthRequestIssueDetails;
    }

    public void setFederatedAuthRequestIssueDetails(FederatedAuthRequestIssueDetails federatedAuthRequestIssueDetails) {
        this.federatedAuthRequestIssueDetails = federatedAuthRequestIssueDetails;
    }

    public BounceTrackingIssueDetails getBounceTrackingIssueDetails() {
        return bounceTrackingIssueDetails;
    }

    public void setBounceTrackingIssueDetails(BounceTrackingIssueDetails bounceTrackingIssueDetails) {
        this.bounceTrackingIssueDetails = bounceTrackingIssueDetails;
    }

    public CookieDeprecationMetadataIssueDetails getCookieDeprecationMetadataIssueDetails() {
        return cookieDeprecationMetadataIssueDetails;
    }

    public void setCookieDeprecationMetadataIssueDetails(
            CookieDeprecationMetadataIssueDetails cookieDeprecationMetadataIssueDetails) {
        this.cookieDeprecationMetadataIssueDetails = cookieDeprecationMetadataIssueDetails;
    }

    public StylesheetLoadingIssueDetails getStylesheetLoadingIssueDetails() {
        return stylesheetLoadingIssueDetails;
    }

    public void setStylesheetLoadingIssueDetails(StylesheetLoadingIssueDetails stylesheetLoadingIssueDetails) {
        this.stylesheetLoadingIssueDetails = stylesheetLoadingIssueDetails;
    }

    public PropertyRuleIssueDetails getPropertyRuleIssueDetails() {
        return propertyRuleIssueDetails;
    }

    public void setPropertyRuleIssueDetails(PropertyRuleIssueDetails propertyRuleIssueDetails) {
        this.propertyRuleIssueDetails = propertyRuleIssueDetails;
    }

    public FederatedAuthUserInfoRequestIssueDetails getFederatedAuthUserInfoRequestIssueDetails() {
        return federatedAuthUserInfoRequestIssueDetails;
    }

    public void setFederatedAuthUserInfoRequestIssueDetails(
            FederatedAuthUserInfoRequestIssueDetails federatedAuthUserInfoRequestIssueDetails) {
        this.federatedAuthUserInfoRequestIssueDetails = federatedAuthUserInfoRequestIssueDetails;
    }

    public SharedDictionaryIssueDetails getSharedDictionaryIssueDetails() {
        return sharedDictionaryIssueDetails;
    }

    public void setSharedDictionaryIssueDetails(SharedDictionaryIssueDetails sharedDictionaryIssueDetails) {
        this.sharedDictionaryIssueDetails = sharedDictionaryIssueDetails;
    }

    public SelectElementAccessibilityIssueDetails getSelectElementAccessibilityIssueDetails() {
        return selectElementAccessibilityIssueDetails;
    }

    public void setSelectElementAccessibilityIssueDetails(
            SelectElementAccessibilityIssueDetails selectElementAccessibilityIssueDetails) {
        this.selectElementAccessibilityIssueDetails = selectElementAccessibilityIssueDetails;
    }

    public String toString() {
        return "InspectorIssueDetails [cookieIssueDetails=" + cookieIssueDetails + ", mixedContentIssueDetails="
                + mixedContentIssueDetails + ", blockedByResponseIssueDetails=" + blockedByResponseIssueDetails
                + ", heavyAdIssueDetails=" + heavyAdIssueDetails + ", contentSecurityPolicyIssueDetails="
                + contentSecurityPolicyIssueDetails + ", sharedArrayBufferIssueDetails=" + sharedArrayBufferIssueDetails
                + ", lowTextContrastIssueDetails=" + lowTextContrastIssueDetails + ", corsIssueDetails="
                + corsIssueDetails + ", attributionReportingIssueDetails=" + attributionReportingIssueDetails
                + ", quirksModeIssueDetails=" + quirksModeIssueDetails + ", partitioningBlobURLIssueDetails="
                + partitioningBlobURLIssueDetails + ", navigatorUserAgentIssueDetails=" + navigatorUserAgentIssueDetails
                + ", genericIssueDetails=" + genericIssueDetails + ", deprecationIssueDetails="
                + deprecationIssueDetails + ", clientHintIssueDetails=" + clientHintIssueDetails
                + ", federatedAuthRequestIssueDetails=" + federatedAuthRequestIssueDetails
                + ", bounceTrackingIssueDetails=" + bounceTrackingIssueDetails
                + ", cookieDeprecationMetadataIssueDetails=" + cookieDeprecationMetadataIssueDetails
                + ", stylesheetLoadingIssueDetails=" + stylesheetLoadingIssueDetails + ", propertyRuleIssueDetails="
                + propertyRuleIssueDetails + ", federatedAuthUserInfoRequestIssueDetails="
                + federatedAuthUserInfoRequestIssueDetails + ", sharedDictionaryIssueDetails="
                + sharedDictionaryIssueDetails + ", selectElementAccessibilityIssueDetails="
                + selectElementAccessibilityIssueDetails + "]";
    }
}
