// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum CookieExclusionReason {
    @SerializedName("ExcludeSameSiteUnspecifiedTreatedAsLax")
    @JsonProperty("ExcludeSameSiteUnspecifiedTreatedAsLax")
    ExcludeSameSiteUnspecifiedTreatedAsLax("ExcludeSameSiteUnspecifiedTreatedAsLax"),

    @SerializedName("ExcludeSameSiteNoneInsecure")
    @JsonProperty("ExcludeSameSiteNoneInsecure")
    ExcludeSameSiteNoneInsecure("ExcludeSameSiteNoneInsecure"),

    @SerializedName("ExcludeSameSiteLax")
    @JsonProperty("ExcludeSameSiteLax")
    ExcludeSameSiteLax("ExcludeSameSiteLax"),

    @SerializedName("ExcludeSameSiteStrict")
    @JsonProperty("ExcludeSameSiteStrict")
    ExcludeSameSiteStrict("ExcludeSameSiteStrict"),

    @SerializedName("ExcludeInvalidSameParty")
    @JsonProperty("ExcludeInvalidSameParty")
    ExcludeInvalidSameParty("ExcludeInvalidSameParty"),

    @SerializedName("ExcludeSamePartyCrossPartyContext")
    @JsonProperty("ExcludeSamePartyCrossPartyContext")
    ExcludeSamePartyCrossPartyContext("ExcludeSamePartyCrossPartyContext"),

    @SerializedName("ExcludeDomainNonASCII")
    @JsonProperty("ExcludeDomainNonASCII")
    ExcludeDomainNonASCII("ExcludeDomainNonASCII"),

    @SerializedName("ExcludeThirdPartyCookieBlockedInFirstPartySet")
    @JsonProperty("ExcludeThirdPartyCookieBlockedInFirstPartySet")
    ExcludeThirdPartyCookieBlockedInFirstPartySet("ExcludeThirdPartyCookieBlockedInFirstPartySet"),

    @SerializedName("ExcludeThirdPartyPhaseout")
    @JsonProperty("ExcludeThirdPartyPhaseout")
    ExcludeThirdPartyPhaseout("ExcludeThirdPartyPhaseout"),

    @SerializedName("ExcludePortMismatch")
    @JsonProperty("ExcludePortMismatch")
    ExcludePortMismatch("ExcludePortMismatch"),

    @SerializedName("ExcludeSchemeMismatch")
    @JsonProperty("ExcludeSchemeMismatch")
    ExcludeSchemeMismatch("ExcludeSchemeMismatch"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookieExclusionReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
