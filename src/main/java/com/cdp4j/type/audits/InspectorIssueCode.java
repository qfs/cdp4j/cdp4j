// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * A unique identifier for the type of issue. Each type may use one of the
 * optional fields in InspectorIssueDetails to convey more specific
 * information about the kind of issue.
 */
public enum InspectorIssueCode {
    @SerializedName("CookieIssue")
    @JsonProperty("CookieIssue")
    CookieIssue("CookieIssue"),

    @SerializedName("MixedContentIssue")
    @JsonProperty("MixedContentIssue")
    MixedContentIssue("MixedContentIssue"),

    @SerializedName("BlockedByResponseIssue")
    @JsonProperty("BlockedByResponseIssue")
    BlockedByResponseIssue("BlockedByResponseIssue"),

    @SerializedName("HeavyAdIssue")
    @JsonProperty("HeavyAdIssue")
    HeavyAdIssue("HeavyAdIssue"),

    @SerializedName("ContentSecurityPolicyIssue")
    @JsonProperty("ContentSecurityPolicyIssue")
    ContentSecurityPolicyIssue("ContentSecurityPolicyIssue"),

    @SerializedName("SharedArrayBufferIssue")
    @JsonProperty("SharedArrayBufferIssue")
    SharedArrayBufferIssue("SharedArrayBufferIssue"),

    @SerializedName("LowTextContrastIssue")
    @JsonProperty("LowTextContrastIssue")
    LowTextContrastIssue("LowTextContrastIssue"),

    @SerializedName("CorsIssue")
    @JsonProperty("CorsIssue")
    CorsIssue("CorsIssue"),

    @SerializedName("AttributionReportingIssue")
    @JsonProperty("AttributionReportingIssue")
    AttributionReportingIssue("AttributionReportingIssue"),

    @SerializedName("QuirksModeIssue")
    @JsonProperty("QuirksModeIssue")
    QuirksModeIssue("QuirksModeIssue"),

    @SerializedName("PartitioningBlobURLIssue")
    @JsonProperty("PartitioningBlobURLIssue")
    PartitioningBlobURLIssue("PartitioningBlobURLIssue"),

    @SerializedName("NavigatorUserAgentIssue")
    @JsonProperty("NavigatorUserAgentIssue")
    NavigatorUserAgentIssue("NavigatorUserAgentIssue"),

    @SerializedName("GenericIssue")
    @JsonProperty("GenericIssue")
    GenericIssue("GenericIssue"),

    @SerializedName("DeprecationIssue")
    @JsonProperty("DeprecationIssue")
    DeprecationIssue("DeprecationIssue"),

    @SerializedName("ClientHintIssue")
    @JsonProperty("ClientHintIssue")
    ClientHintIssue("ClientHintIssue"),

    @SerializedName("FederatedAuthRequestIssue")
    @JsonProperty("FederatedAuthRequestIssue")
    FederatedAuthRequestIssue("FederatedAuthRequestIssue"),

    @SerializedName("BounceTrackingIssue")
    @JsonProperty("BounceTrackingIssue")
    BounceTrackingIssue("BounceTrackingIssue"),

    @SerializedName("CookieDeprecationMetadataIssue")
    @JsonProperty("CookieDeprecationMetadataIssue")
    CookieDeprecationMetadataIssue("CookieDeprecationMetadataIssue"),

    @SerializedName("StylesheetLoadingIssue")
    @JsonProperty("StylesheetLoadingIssue")
    StylesheetLoadingIssue("StylesheetLoadingIssue"),

    @SerializedName("FederatedAuthUserInfoRequestIssue")
    @JsonProperty("FederatedAuthUserInfoRequestIssue")
    FederatedAuthUserInfoRequestIssue("FederatedAuthUserInfoRequestIssue"),

    @SerializedName("PropertyRuleIssue")
    @JsonProperty("PropertyRuleIssue")
    PropertyRuleIssue("PropertyRuleIssue"),

    @SerializedName("SharedDictionaryIssue")
    @JsonProperty("SharedDictionaryIssue")
    SharedDictionaryIssue("SharedDictionaryIssue"),

    @SerializedName("SelectElementAccessibilityIssue")
    @JsonProperty("SelectElementAccessibilityIssue")
    SelectElementAccessibilityIssue("SelectElementAccessibilityIssue"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    InspectorIssueCode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
