// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

public class FederatedAuthUserInfoRequestIssueDetails {
    private FederatedAuthUserInfoRequestIssueReason federatedAuthUserInfoRequestIssueReason;

    public FederatedAuthUserInfoRequestIssueReason getFederatedAuthUserInfoRequestIssueReason() {
        return federatedAuthUserInfoRequestIssueReason;
    }

    public void setFederatedAuthUserInfoRequestIssueReason(
            FederatedAuthUserInfoRequestIssueReason federatedAuthUserInfoRequestIssueReason) {
        this.federatedAuthUserInfoRequestIssueReason = federatedAuthUserInfoRequestIssueReason;
    }

    public String toString() {
        return "FederatedAuthUserInfoRequestIssueDetails [federatedAuthUserInfoRequestIssueReason="
                + federatedAuthUserInfoRequestIssueReason + "]";
    }
}
