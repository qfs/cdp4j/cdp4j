// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AttributionReportingIssueType {
    @SerializedName("PermissionPolicyDisabled")
    @JsonProperty("PermissionPolicyDisabled")
    PermissionPolicyDisabled("PermissionPolicyDisabled"),

    @SerializedName("UntrustworthyReportingOrigin")
    @JsonProperty("UntrustworthyReportingOrigin")
    UntrustworthyReportingOrigin("UntrustworthyReportingOrigin"),

    @SerializedName("InsecureContext")
    @JsonProperty("InsecureContext")
    InsecureContext("InsecureContext"),

    @SerializedName("InvalidHeader")
    @JsonProperty("InvalidHeader")
    InvalidHeader("InvalidHeader"),

    @SerializedName("InvalidRegisterTriggerHeader")
    @JsonProperty("InvalidRegisterTriggerHeader")
    InvalidRegisterTriggerHeader("InvalidRegisterTriggerHeader"),

    @SerializedName("SourceAndTriggerHeaders")
    @JsonProperty("SourceAndTriggerHeaders")
    SourceAndTriggerHeaders("SourceAndTriggerHeaders"),

    @SerializedName("SourceIgnored")
    @JsonProperty("SourceIgnored")
    SourceIgnored("SourceIgnored"),

    @SerializedName("TriggerIgnored")
    @JsonProperty("TriggerIgnored")
    TriggerIgnored("TriggerIgnored"),

    @SerializedName("OsSourceIgnored")
    @JsonProperty("OsSourceIgnored")
    OsSourceIgnored("OsSourceIgnored"),

    @SerializedName("OsTriggerIgnored")
    @JsonProperty("OsTriggerIgnored")
    OsTriggerIgnored("OsTriggerIgnored"),

    @SerializedName("InvalidRegisterOsSourceHeader")
    @JsonProperty("InvalidRegisterOsSourceHeader")
    InvalidRegisterOsSourceHeader("InvalidRegisterOsSourceHeader"),

    @SerializedName("InvalidRegisterOsTriggerHeader")
    @JsonProperty("InvalidRegisterOsTriggerHeader")
    InvalidRegisterOsTriggerHeader("InvalidRegisterOsTriggerHeader"),

    @SerializedName("WebAndOsHeaders")
    @JsonProperty("WebAndOsHeaders")
    WebAndOsHeaders("WebAndOsHeaders"),

    @SerializedName("NoWebOrOsSupport")
    @JsonProperty("NoWebOrOsSupport")
    NoWebOrOsSupport("NoWebOrOsSupport"),

    @SerializedName("NavigationRegistrationWithoutTransientUserActivation")
    @JsonProperty("NavigationRegistrationWithoutTransientUserActivation")
    NavigationRegistrationWithoutTransientUserActivation("NavigationRegistrationWithoutTransientUserActivation"),

    @SerializedName("InvalidInfoHeader")
    @JsonProperty("InvalidInfoHeader")
    InvalidInfoHeader("InvalidInfoHeader"),

    @SerializedName("NoRegisterSourceHeader")
    @JsonProperty("NoRegisterSourceHeader")
    NoRegisterSourceHeader("NoRegisterSourceHeader"),

    @SerializedName("NoRegisterTriggerHeader")
    @JsonProperty("NoRegisterTriggerHeader")
    NoRegisterTriggerHeader("NoRegisterTriggerHeader"),

    @SerializedName("NoRegisterOsSourceHeader")
    @JsonProperty("NoRegisterOsSourceHeader")
    NoRegisterOsSourceHeader("NoRegisterOsSourceHeader"),

    @SerializedName("NoRegisterOsTriggerHeader")
    @JsonProperty("NoRegisterOsTriggerHeader")
    NoRegisterOsTriggerHeader("NoRegisterOsTriggerHeader"),

    @SerializedName("NavigationRegistrationUniqueScopeAlreadySet")
    @JsonProperty("NavigationRegistrationUniqueScopeAlreadySet")
    NavigationRegistrationUniqueScopeAlreadySet("NavigationRegistrationUniqueScopeAlreadySet"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AttributionReportingIssueType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
