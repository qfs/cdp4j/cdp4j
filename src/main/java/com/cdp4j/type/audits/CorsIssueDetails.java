// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.cdp4j.type.network.ClientSecurityState;
import com.cdp4j.type.network.CorsErrorStatus;
import com.cdp4j.type.network.IPAddressSpace;

/**
 * Details for a CORS related issue, e.g. a warning or error related to CORS
 * RFC1918 enforcement.
 */
public class CorsIssueDetails {
    private CorsErrorStatus corsErrorStatus;

    private Boolean isWarning;

    private AffectedRequest request;

    private SourceCodeLocation location;

    private String initiatorOrigin;

    private IPAddressSpace resourceIPAddressSpace;

    private ClientSecurityState clientSecurityState;

    public CorsErrorStatus getCorsErrorStatus() {
        return corsErrorStatus;
    }

    public void setCorsErrorStatus(CorsErrorStatus corsErrorStatus) {
        this.corsErrorStatus = corsErrorStatus;
    }

    public Boolean isIsWarning() {
        return isWarning;
    }

    public void setIsWarning(Boolean isWarning) {
        this.isWarning = isWarning;
    }

    public AffectedRequest getRequest() {
        return request;
    }

    public void setRequest(AffectedRequest request) {
        this.request = request;
    }

    public SourceCodeLocation getLocation() {
        return location;
    }

    public void setLocation(SourceCodeLocation location) {
        this.location = location;
    }

    public String getInitiatorOrigin() {
        return initiatorOrigin;
    }

    public void setInitiatorOrigin(String initiatorOrigin) {
        this.initiatorOrigin = initiatorOrigin;
    }

    public IPAddressSpace getResourceIPAddressSpace() {
        return resourceIPAddressSpace;
    }

    public void setResourceIPAddressSpace(IPAddressSpace resourceIPAddressSpace) {
        this.resourceIPAddressSpace = resourceIPAddressSpace;
    }

    public ClientSecurityState getClientSecurityState() {
        return clientSecurityState;
    }

    public void setClientSecurityState(ClientSecurityState clientSecurityState) {
        this.clientSecurityState = clientSecurityState;
    }

    public String toString() {
        return "CorsIssueDetails [corsErrorStatus=" + corsErrorStatus + ", isWarning=" + isWarning + ", request="
                + request + ", location=" + location + ", initiatorOrigin=" + initiatorOrigin
                + ", resourceIPAddressSpace=" + resourceIPAddressSpace + ", clientSecurityState=" + clientSecurityState
                + "]";
    }
}
