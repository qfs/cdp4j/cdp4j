// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

public class NavigatorUserAgentIssueDetails {
    private String url;

    private SourceCodeLocation location;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public SourceCodeLocation getLocation() {
        return location;
    }

    public void setLocation(SourceCodeLocation location) {
        this.location = location;
    }

    public String toString() {
        return "NavigatorUserAgentIssueDetails [url=" + url + ", location=" + location + "]";
    }
}
