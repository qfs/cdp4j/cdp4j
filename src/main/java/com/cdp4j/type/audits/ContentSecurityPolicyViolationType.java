// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ContentSecurityPolicyViolationType {
    @SerializedName("kInlineViolation")
    @JsonProperty("kInlineViolation")
    KInlineViolation("kInlineViolation"),

    @SerializedName("kEvalViolation")
    @JsonProperty("kEvalViolation")
    KEvalViolation("kEvalViolation"),

    @SerializedName("kURLViolation")
    @JsonProperty("kURLViolation")
    KURLViolation("kURLViolation"),

    @SerializedName("kTrustedTypesSinkViolation")
    @JsonProperty("kTrustedTypesSinkViolation")
    KTrustedTypesSinkViolation("kTrustedTypesSinkViolation"),

    @SerializedName("kTrustedTypesPolicyViolation")
    @JsonProperty("kTrustedTypesPolicyViolation")
    KTrustedTypesPolicyViolation("kTrustedTypesPolicyViolation"),

    @SerializedName("kWasmEvalViolation")
    @JsonProperty("kWasmEvalViolation")
    KWasmEvalViolation("kWasmEvalViolation"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ContentSecurityPolicyViolationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
