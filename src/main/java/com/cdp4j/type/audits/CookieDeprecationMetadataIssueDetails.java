// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * This issue warns about third-party sites that are accessing cookies on the
 * current page, and have been permitted due to having a global metadata grant.
 * Note that in this context 'site' means eTLD+1. For example, if the URL
 * https://example.test:80/web_page was accessing cookies, the site reported
 * would be example.test.
 */
public class CookieDeprecationMetadataIssueDetails {
    private List<String> allowedSites = emptyList();

    private Double optOutPercentage;

    private Boolean isOptOutTopLevel;

    private CookieOperation operation;

    public List<String> getAllowedSites() {
        return allowedSites;
    }

    public void setAllowedSites(List<String> allowedSites) {
        this.allowedSites = allowedSites;
    }

    public Double getOptOutPercentage() {
        return optOutPercentage;
    }

    public void setOptOutPercentage(Double optOutPercentage) {
        this.optOutPercentage = optOutPercentage;
    }

    public Boolean isIsOptOutTopLevel() {
        return isOptOutTopLevel;
    }

    public void setIsOptOutTopLevel(Boolean isOptOutTopLevel) {
        this.isOptOutTopLevel = isOptOutTopLevel;
    }

    public CookieOperation getOperation() {
        return operation;
    }

    public void setOperation(CookieOperation operation) {
        this.operation = operation;
    }

    public String toString() {
        return "CookieDeprecationMetadataIssueDetails [allowedSites=" + allowedSites + ", optOutPercentage="
                + optOutPercentage + ", isOptOutTopLevel=" + isOptOutTopLevel + ", operation=" + operation + "]";
    }
}
