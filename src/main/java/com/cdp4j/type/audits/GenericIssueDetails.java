// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * Depending on the concrete errorType, different properties are set.
 */
public class GenericIssueDetails {
    private GenericIssueErrorType errorType;

    private String frameId;

    private Integer violatingNodeId;

    private String violatingNodeAttribute;

    private AffectedRequest request;

    /**
     * Issues with the same errorType are aggregated in the frontend.
     */
    public GenericIssueErrorType getErrorType() {
        return errorType;
    }

    /**
     * Issues with the same errorType are aggregated in the frontend.
     */
    public void setErrorType(GenericIssueErrorType errorType) {
        this.errorType = errorType;
    }

    public String getFrameId() {
        return frameId;
    }

    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public Integer getViolatingNodeId() {
        return violatingNodeId;
    }

    public void setViolatingNodeId(Integer violatingNodeId) {
        this.violatingNodeId = violatingNodeId;
    }

    public String getViolatingNodeAttribute() {
        return violatingNodeAttribute;
    }

    public void setViolatingNodeAttribute(String violatingNodeAttribute) {
        this.violatingNodeAttribute = violatingNodeAttribute;
    }

    public AffectedRequest getRequest() {
        return request;
    }

    public void setRequest(AffectedRequest request) {
        this.request = request;
    }

    public String toString() {
        return "GenericIssueDetails [errorType=" + errorType + ", frameId=" + frameId + ", violatingNodeId="
                + violatingNodeId + ", violatingNodeAttribute=" + violatingNodeAttribute + ", request=" + request + "]";
    }
}
