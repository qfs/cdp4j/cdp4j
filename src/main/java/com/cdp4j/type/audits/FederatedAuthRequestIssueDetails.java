// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

public class FederatedAuthRequestIssueDetails {
    private FederatedAuthRequestIssueReason federatedAuthRequestIssueReason;

    public FederatedAuthRequestIssueReason getFederatedAuthRequestIssueReason() {
        return federatedAuthRequestIssueReason;
    }

    public void setFederatedAuthRequestIssueReason(FederatedAuthRequestIssueReason federatedAuthRequestIssueReason) {
        this.federatedAuthRequestIssueReason = federatedAuthRequestIssueReason;
    }

    public String toString() {
        return "FederatedAuthRequestIssueDetails [federatedAuthRequestIssueReason=" + federatedAuthRequestIssueReason
                + "]";
    }
}
