// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * This issue warns about errors in the select element content model.
 */
public class SelectElementAccessibilityIssueDetails {
    private Integer nodeId;

    private SelectElementAccessibilityIssueReason selectElementAccessibilityIssueReason;

    private Boolean hasDisallowedAttributes;

    public Integer getNodeId() {
        return nodeId;
    }

    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public SelectElementAccessibilityIssueReason getSelectElementAccessibilityIssueReason() {
        return selectElementAccessibilityIssueReason;
    }

    public void setSelectElementAccessibilityIssueReason(
            SelectElementAccessibilityIssueReason selectElementAccessibilityIssueReason) {
        this.selectElementAccessibilityIssueReason = selectElementAccessibilityIssueReason;
    }

    public Boolean isHasDisallowedAttributes() {
        return hasDisallowedAttributes;
    }

    public void setHasDisallowedAttributes(Boolean hasDisallowedAttributes) {
        this.hasDisallowedAttributes = hasDisallowedAttributes;
    }

    public String toString() {
        return "SelectElementAccessibilityIssueDetails [nodeId=" + nodeId + ", selectElementAccessibilityIssueReason="
                + selectElementAccessibilityIssueReason + ", hasDisallowedAttributes=" + hasDisallowedAttributes + "]";
    }
}
