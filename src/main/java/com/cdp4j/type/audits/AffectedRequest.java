// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * Information about a request that is affected by an inspector issue.
 */
public class AffectedRequest {
    private String requestId;

    private String url;

    /**
     * The unique request id.
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * The unique request id.
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String toString() {
        return "AffectedRequest [requestId=" + requestId + ", url=" + url + "]";
    }
}
