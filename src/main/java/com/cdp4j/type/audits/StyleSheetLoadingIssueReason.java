// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum StyleSheetLoadingIssueReason {
    @SerializedName("LateImportRule")
    @JsonProperty("LateImportRule")
    LateImportRule("LateImportRule"),

    @SerializedName("RequestFailed")
    @JsonProperty("RequestFailed")
    RequestFailed("RequestFailed"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    StyleSheetLoadingIssueReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
