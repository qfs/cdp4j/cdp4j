// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum MixedContentResolutionStatus {
    @SerializedName("MixedContentBlocked")
    @JsonProperty("MixedContentBlocked")
    MixedContentBlocked("MixedContentBlocked"),

    @SerializedName("MixedContentAutomaticallyUpgraded")
    @JsonProperty("MixedContentAutomaticallyUpgraded")
    MixedContentAutomaticallyUpgraded("MixedContentAutomaticallyUpgraded"),

    @SerializedName("MixedContentWarning")
    @JsonProperty("MixedContentWarning")
    MixedContentWarning("MixedContentWarning"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    MixedContentResolutionStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
