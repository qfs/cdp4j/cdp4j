// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

public class FailedRequestInfo {
    private String url;

    private String failureMessage;

    private String requestId;

    /**
     * The URL that failed to load.
     */
    public String getUrl() {
        return url;
    }

    /**
     * The URL that failed to load.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * The failure message for the failed request.
     */
    public String getFailureMessage() {
        return failureMessage;
    }

    /**
     * The failure message for the failed request.
     */
    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String toString() {
        return "FailedRequestInfo [url=" + url + ", failureMessage=" + failureMessage + ", requestId=" + requestId
                + "]";
    }
}
