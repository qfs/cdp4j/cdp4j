// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * Details for a request that has been blocked with the BLOCKED_BY_RESPONSE
 * code. Currently only used for COEP/COOP, but may be extended to include some
 * CSP errors in the future.
 */
public class BlockedByResponseIssueDetails {
    private AffectedRequest request;

    private AffectedFrame parentFrame;

    private AffectedFrame blockedFrame;

    private BlockedByResponseReason reason;

    public AffectedRequest getRequest() {
        return request;
    }

    public void setRequest(AffectedRequest request) {
        this.request = request;
    }

    public AffectedFrame getParentFrame() {
        return parentFrame;
    }

    public void setParentFrame(AffectedFrame parentFrame) {
        this.parentFrame = parentFrame;
    }

    public AffectedFrame getBlockedFrame() {
        return blockedFrame;
    }

    public void setBlockedFrame(AffectedFrame blockedFrame) {
        this.blockedFrame = blockedFrame;
    }

    public BlockedByResponseReason getReason() {
        return reason;
    }

    public void setReason(BlockedByResponseReason reason) {
        this.reason = reason;
    }

    public String toString() {
        return "BlockedByResponseIssueDetails [request=" + request + ", parentFrame=" + parentFrame + ", blockedFrame="
                + blockedFrame + ", reason=" + reason + "]";
    }
}
