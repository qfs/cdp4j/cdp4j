// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * This issue warns when a referenced stylesheet couldn't be loaded.
 */
public class StylesheetLoadingIssueDetails {
    private SourceCodeLocation sourceCodeLocation;

    private StyleSheetLoadingIssueReason styleSheetLoadingIssueReason;

    private FailedRequestInfo failedRequestInfo;

    /**
     * Source code position that referenced the failing stylesheet.
     */
    public SourceCodeLocation getSourceCodeLocation() {
        return sourceCodeLocation;
    }

    /**
     * Source code position that referenced the failing stylesheet.
     */
    public void setSourceCodeLocation(SourceCodeLocation sourceCodeLocation) {
        this.sourceCodeLocation = sourceCodeLocation;
    }

    /**
     * Reason why the stylesheet couldn't be loaded.
     */
    public StyleSheetLoadingIssueReason getStyleSheetLoadingIssueReason() {
        return styleSheetLoadingIssueReason;
    }

    /**
     * Reason why the stylesheet couldn't be loaded.
     */
    public void setStyleSheetLoadingIssueReason(StyleSheetLoadingIssueReason styleSheetLoadingIssueReason) {
        this.styleSheetLoadingIssueReason = styleSheetLoadingIssueReason;
    }

    /**
     * Contains additional info when the failure was due to a request.
     */
    public FailedRequestInfo getFailedRequestInfo() {
        return failedRequestInfo;
    }

    /**
     * Contains additional info when the failure was due to a request.
     */
    public void setFailedRequestInfo(FailedRequestInfo failedRequestInfo) {
        this.failedRequestInfo = failedRequestInfo;
    }

    public String toString() {
        return "StylesheetLoadingIssueDetails [sourceCodeLocation=" + sourceCodeLocation
                + ", styleSheetLoadingIssueReason=" + styleSheetLoadingIssueReason + ", failedRequestInfo="
                + failedRequestInfo + "]";
    }
}
