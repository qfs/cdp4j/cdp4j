// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * This issue warns about sites in the redirect chain of a finished navigation
 * that may be flagged as trackers and have their state cleared if they don't
 * receive a user interaction. Note that in this context 'site' means eTLD+1.
 * For example, if the URL https://example.test:80/bounce was in the redirect
 * chain, the site reported would be example.test.
 */
public class BounceTrackingIssueDetails {
    private List<String> trackingSites = emptyList();

    public List<String> getTrackingSites() {
        return trackingSites;
    }

    public void setTrackingSites(List<String> trackingSites) {
        this.trackingSites = trackingSites;
    }

    public String toString() {
        return "BounceTrackingIssueDetails [trackingSites=" + trackingSites + "]";
    }
}
