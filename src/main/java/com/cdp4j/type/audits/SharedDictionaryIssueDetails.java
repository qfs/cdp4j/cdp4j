// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

public class SharedDictionaryIssueDetails {
    private SharedDictionaryError sharedDictionaryError;

    private AffectedRequest request;

    public SharedDictionaryError getSharedDictionaryError() {
        return sharedDictionaryError;
    }

    public void setSharedDictionaryError(SharedDictionaryError sharedDictionaryError) {
        this.sharedDictionaryError = sharedDictionaryError;
    }

    public AffectedRequest getRequest() {
        return request;
    }

    public void setRequest(AffectedRequest request) {
        this.request = request;
    }

    public String toString() {
        return "SharedDictionaryIssueDetails [sharedDictionaryError=" + sharedDictionaryError + ", request=" + request
                + "]";
    }
}
