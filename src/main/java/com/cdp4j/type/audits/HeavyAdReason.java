// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum HeavyAdReason {
    @SerializedName("NetworkTotalLimit")
    @JsonProperty("NetworkTotalLimit")
    NetworkTotalLimit("NetworkTotalLimit"),

    @SerializedName("CpuTotalLimit")
    @JsonProperty("CpuTotalLimit")
    CpuTotalLimit("CpuTotalLimit"),

    @SerializedName("CpuPeakLimit")
    @JsonProperty("CpuPeakLimit")
    CpuPeakLimit("CpuPeakLimit"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    HeavyAdReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
