// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Represents the failure reason when a federated authentication reason fails.
 * Should be updated alongside RequestIdTokenStatus in
 * third_party/blink/public/mojom/devtools/inspector_issue.mojom to include
 * all cases except for success.
 */
public enum FederatedAuthRequestIssueReason {
    @SerializedName("ShouldEmbargo")
    @JsonProperty("ShouldEmbargo")
    ShouldEmbargo("ShouldEmbargo"),

    @SerializedName("TooManyRequests")
    @JsonProperty("TooManyRequests")
    TooManyRequests("TooManyRequests"),

    @SerializedName("WellKnownHttpNotFound")
    @JsonProperty("WellKnownHttpNotFound")
    WellKnownHttpNotFound("WellKnownHttpNotFound"),

    @SerializedName("WellKnownNoResponse")
    @JsonProperty("WellKnownNoResponse")
    WellKnownNoResponse("WellKnownNoResponse"),

    @SerializedName("WellKnownInvalidResponse")
    @JsonProperty("WellKnownInvalidResponse")
    WellKnownInvalidResponse("WellKnownInvalidResponse"),

    @SerializedName("WellKnownListEmpty")
    @JsonProperty("WellKnownListEmpty")
    WellKnownListEmpty("WellKnownListEmpty"),

    @SerializedName("WellKnownInvalidContentType")
    @JsonProperty("WellKnownInvalidContentType")
    WellKnownInvalidContentType("WellKnownInvalidContentType"),

    @SerializedName("ConfigNotInWellKnown")
    @JsonProperty("ConfigNotInWellKnown")
    ConfigNotInWellKnown("ConfigNotInWellKnown"),

    @SerializedName("WellKnownTooBig")
    @JsonProperty("WellKnownTooBig")
    WellKnownTooBig("WellKnownTooBig"),

    @SerializedName("ConfigHttpNotFound")
    @JsonProperty("ConfigHttpNotFound")
    ConfigHttpNotFound("ConfigHttpNotFound"),

    @SerializedName("ConfigNoResponse")
    @JsonProperty("ConfigNoResponse")
    ConfigNoResponse("ConfigNoResponse"),

    @SerializedName("ConfigInvalidResponse")
    @JsonProperty("ConfigInvalidResponse")
    ConfigInvalidResponse("ConfigInvalidResponse"),

    @SerializedName("ConfigInvalidContentType")
    @JsonProperty("ConfigInvalidContentType")
    ConfigInvalidContentType("ConfigInvalidContentType"),

    @SerializedName("ClientMetadataHttpNotFound")
    @JsonProperty("ClientMetadataHttpNotFound")
    ClientMetadataHttpNotFound("ClientMetadataHttpNotFound"),

    @SerializedName("ClientMetadataNoResponse")
    @JsonProperty("ClientMetadataNoResponse")
    ClientMetadataNoResponse("ClientMetadataNoResponse"),

    @SerializedName("ClientMetadataInvalidResponse")
    @JsonProperty("ClientMetadataInvalidResponse")
    ClientMetadataInvalidResponse("ClientMetadataInvalidResponse"),

    @SerializedName("ClientMetadataInvalidContentType")
    @JsonProperty("ClientMetadataInvalidContentType")
    ClientMetadataInvalidContentType("ClientMetadataInvalidContentType"),

    @SerializedName("IdpNotPotentiallyTrustworthy")
    @JsonProperty("IdpNotPotentiallyTrustworthy")
    IdpNotPotentiallyTrustworthy("IdpNotPotentiallyTrustworthy"),

    @SerializedName("DisabledInSettings")
    @JsonProperty("DisabledInSettings")
    DisabledInSettings("DisabledInSettings"),

    @SerializedName("DisabledInFlags")
    @JsonProperty("DisabledInFlags")
    DisabledInFlags("DisabledInFlags"),

    @SerializedName("ErrorFetchingSignin")
    @JsonProperty("ErrorFetchingSignin")
    ErrorFetchingSignin("ErrorFetchingSignin"),

    @SerializedName("InvalidSigninResponse")
    @JsonProperty("InvalidSigninResponse")
    InvalidSigninResponse("InvalidSigninResponse"),

    @SerializedName("AccountsHttpNotFound")
    @JsonProperty("AccountsHttpNotFound")
    AccountsHttpNotFound("AccountsHttpNotFound"),

    @SerializedName("AccountsNoResponse")
    @JsonProperty("AccountsNoResponse")
    AccountsNoResponse("AccountsNoResponse"),

    @SerializedName("AccountsInvalidResponse")
    @JsonProperty("AccountsInvalidResponse")
    AccountsInvalidResponse("AccountsInvalidResponse"),

    @SerializedName("AccountsListEmpty")
    @JsonProperty("AccountsListEmpty")
    AccountsListEmpty("AccountsListEmpty"),

    @SerializedName("AccountsInvalidContentType")
    @JsonProperty("AccountsInvalidContentType")
    AccountsInvalidContentType("AccountsInvalidContentType"),

    @SerializedName("IdTokenHttpNotFound")
    @JsonProperty("IdTokenHttpNotFound")
    IdTokenHttpNotFound("IdTokenHttpNotFound"),

    @SerializedName("IdTokenNoResponse")
    @JsonProperty("IdTokenNoResponse")
    IdTokenNoResponse("IdTokenNoResponse"),

    @SerializedName("IdTokenInvalidResponse")
    @JsonProperty("IdTokenInvalidResponse")
    IdTokenInvalidResponse("IdTokenInvalidResponse"),

    @SerializedName("IdTokenIdpErrorResponse")
    @JsonProperty("IdTokenIdpErrorResponse")
    IdTokenIdpErrorResponse("IdTokenIdpErrorResponse"),

    @SerializedName("IdTokenCrossSiteIdpErrorResponse")
    @JsonProperty("IdTokenCrossSiteIdpErrorResponse")
    IdTokenCrossSiteIdpErrorResponse("IdTokenCrossSiteIdpErrorResponse"),

    @SerializedName("IdTokenInvalidRequest")
    @JsonProperty("IdTokenInvalidRequest")
    IdTokenInvalidRequest("IdTokenInvalidRequest"),

    @SerializedName("IdTokenInvalidContentType")
    @JsonProperty("IdTokenInvalidContentType")
    IdTokenInvalidContentType("IdTokenInvalidContentType"),

    @SerializedName("ErrorIdToken")
    @JsonProperty("ErrorIdToken")
    ErrorIdToken("ErrorIdToken"),

    @SerializedName("Canceled")
    @JsonProperty("Canceled")
    Canceled("Canceled"),

    @SerializedName("RpPageNotVisible")
    @JsonProperty("RpPageNotVisible")
    RpPageNotVisible("RpPageNotVisible"),

    @SerializedName("SilentMediationFailure")
    @JsonProperty("SilentMediationFailure")
    SilentMediationFailure("SilentMediationFailure"),

    @SerializedName("ThirdPartyCookiesBlocked")
    @JsonProperty("ThirdPartyCookiesBlocked")
    ThirdPartyCookiesBlocked("ThirdPartyCookiesBlocked"),

    @SerializedName("NotSignedInWithIdp")
    @JsonProperty("NotSignedInWithIdp")
    NotSignedInWithIdp("NotSignedInWithIdp"),

    @SerializedName("MissingTransientUserActivation")
    @JsonProperty("MissingTransientUserActivation")
    MissingTransientUserActivation("MissingTransientUserActivation"),

    @SerializedName("ReplacedByActiveMode")
    @JsonProperty("ReplacedByActiveMode")
    ReplacedByActiveMode("ReplacedByActiveMode"),

    @SerializedName("InvalidFieldsSpecified")
    @JsonProperty("InvalidFieldsSpecified")
    InvalidFieldsSpecified("InvalidFieldsSpecified"),

    @SerializedName("RelyingPartyOriginIsOpaque")
    @JsonProperty("RelyingPartyOriginIsOpaque")
    RelyingPartyOriginIsOpaque("RelyingPartyOriginIsOpaque"),

    @SerializedName("TypeNotMatching")
    @JsonProperty("TypeNotMatching")
    TypeNotMatching("TypeNotMatching"),

    @SerializedName("UiDismissedNoEmbargo")
    @JsonProperty("UiDismissedNoEmbargo")
    UiDismissedNoEmbargo("UiDismissedNoEmbargo"),

    @SerializedName("CorsError")
    @JsonProperty("CorsError")
    CorsError("CorsError"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    FederatedAuthRequestIssueReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
