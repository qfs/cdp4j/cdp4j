// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum MixedContentResourceType {
    @SerializedName("AttributionSrc")
    @JsonProperty("AttributionSrc")
    AttributionSrc("AttributionSrc"),

    @SerializedName("Audio")
    @JsonProperty("Audio")
    Audio("Audio"),

    @SerializedName("Beacon")
    @JsonProperty("Beacon")
    Beacon("Beacon"),

    @SerializedName("CSPReport")
    @JsonProperty("CSPReport")
    CSPReport("CSPReport"),

    @SerializedName("Download")
    @JsonProperty("Download")
    Download("Download"),

    @SerializedName("EventSource")
    @JsonProperty("EventSource")
    EventSource("EventSource"),

    @SerializedName("Favicon")
    @JsonProperty("Favicon")
    Favicon("Favicon"),

    @SerializedName("Font")
    @JsonProperty("Font")
    Font("Font"),

    @SerializedName("Form")
    @JsonProperty("Form")
    Form("Form"),

    @SerializedName("Frame")
    @JsonProperty("Frame")
    Frame("Frame"),

    @SerializedName("Image")
    @JsonProperty("Image")
    Image("Image"),

    @SerializedName("Import")
    @JsonProperty("Import")
    Import("Import"),

    @SerializedName("JSON")
    @JsonProperty("JSON")
    JSON("JSON"),

    @SerializedName("Manifest")
    @JsonProperty("Manifest")
    Manifest("Manifest"),

    @SerializedName("Ping")
    @JsonProperty("Ping")
    Ping("Ping"),

    @SerializedName("PluginData")
    @JsonProperty("PluginData")
    PluginData("PluginData"),

    @SerializedName("PluginResource")
    @JsonProperty("PluginResource")
    PluginResource("PluginResource"),

    @SerializedName("Prefetch")
    @JsonProperty("Prefetch")
    Prefetch("Prefetch"),

    @SerializedName("Resource")
    @JsonProperty("Resource")
    Resource("Resource"),

    @SerializedName("Script")
    @JsonProperty("Script")
    Script("Script"),

    @SerializedName("ServiceWorker")
    @JsonProperty("ServiceWorker")
    ServiceWorker("ServiceWorker"),

    @SerializedName("SharedWorker")
    @JsonProperty("SharedWorker")
    SharedWorker("SharedWorker"),

    @SerializedName("SpeculationRules")
    @JsonProperty("SpeculationRules")
    SpeculationRules("SpeculationRules"),

    @SerializedName("Stylesheet")
    @JsonProperty("Stylesheet")
    Stylesheet("Stylesheet"),

    @SerializedName("Track")
    @JsonProperty("Track")
    Track("Track"),

    @SerializedName("Video")
    @JsonProperty("Video")
    Video("Video"),

    @SerializedName("Worker")
    @JsonProperty("Worker")
    Worker("Worker"),

    @SerializedName("XMLHttpRequest")
    @JsonProperty("XMLHttpRequest")
    XMLHttpRequest("XMLHttpRequest"),

    @SerializedName("XSLT")
    @JsonProperty("XSLT")
    XSLT("XSLT"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    MixedContentResourceType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
