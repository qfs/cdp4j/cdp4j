// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PartitioningBlobURLInfo {
    @SerializedName("BlockedCrossPartitionFetching")
    @JsonProperty("BlockedCrossPartitionFetching")
    BlockedCrossPartitionFetching("BlockedCrossPartitionFetching"),

    @SerializedName("EnforceNoopenerForNavigation")
    @JsonProperty("EnforceNoopenerForNavigation")
    EnforceNoopenerForNavigation("EnforceNoopenerForNavigation"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PartitioningBlobURLInfo(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
