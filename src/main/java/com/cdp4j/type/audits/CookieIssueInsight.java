// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

/**
 * Information about the suggested solution to a cookie issue.
 */
public class CookieIssueInsight {
    private InsightType type;

    private String tableEntryUrl;

    public InsightType getType() {
        return type;
    }

    public void setType(InsightType type) {
        this.type = type;
    }

    /**
     * Link to table entry in third-party cookie migration readiness list.
     */
    public String getTableEntryUrl() {
        return tableEntryUrl;
    }

    /**
     * Link to table entry in third-party cookie migration readiness list.
     */
    public void setTableEntryUrl(String tableEntryUrl) {
        this.tableEntryUrl = tableEntryUrl;
    }

    public String toString() {
        return "CookieIssueInsight [type=" + type + ", tableEntryUrl=" + tableEntryUrl + "]";
    }
}
