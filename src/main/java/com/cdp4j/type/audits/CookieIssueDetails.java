// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * This information is currently necessary, as the front-end has a difficult
 * time finding a specific cookie. With this, we can convey specific error
 * information without the cookie.
 */
public class CookieIssueDetails {
    private AffectedCookie cookie;

    private String rawCookieLine;

    private List<CookieWarningReason> cookieWarningReasons = emptyList();

    private List<CookieExclusionReason> cookieExclusionReasons = emptyList();

    private CookieOperation operation;

    private String siteForCookies;

    private String cookieUrl;

    private AffectedRequest request;

    private CookieIssueInsight insight;

    /**
     * If AffectedCookie is not set then rawCookieLine contains the raw Set-Cookie
     * header string. This hints at a problem where the cookie line is syntactically
     * or semantically malformed in a way that no valid cookie could be created.
     */
    public AffectedCookie getCookie() {
        return cookie;
    }

    /**
     * If AffectedCookie is not set then rawCookieLine contains the raw Set-Cookie
     * header string. This hints at a problem where the cookie line is syntactically
     * or semantically malformed in a way that no valid cookie could be created.
     */
    public void setCookie(AffectedCookie cookie) {
        this.cookie = cookie;
    }

    public String getRawCookieLine() {
        return rawCookieLine;
    }

    public void setRawCookieLine(String rawCookieLine) {
        this.rawCookieLine = rawCookieLine;
    }

    public List<CookieWarningReason> getCookieWarningReasons() {
        return cookieWarningReasons;
    }

    public void setCookieWarningReasons(List<CookieWarningReason> cookieWarningReasons) {
        this.cookieWarningReasons = cookieWarningReasons;
    }

    public List<CookieExclusionReason> getCookieExclusionReasons() {
        return cookieExclusionReasons;
    }

    public void setCookieExclusionReasons(List<CookieExclusionReason> cookieExclusionReasons) {
        this.cookieExclusionReasons = cookieExclusionReasons;
    }

    /**
     * Optionally identifies the site-for-cookies and the cookie url, which may be
     * used by the front-end as additional context.
     */
    public CookieOperation getOperation() {
        return operation;
    }

    /**
     * Optionally identifies the site-for-cookies and the cookie url, which may be
     * used by the front-end as additional context.
     */
    public void setOperation(CookieOperation operation) {
        this.operation = operation;
    }

    public String getSiteForCookies() {
        return siteForCookies;
    }

    public void setSiteForCookies(String siteForCookies) {
        this.siteForCookies = siteForCookies;
    }

    public String getCookieUrl() {
        return cookieUrl;
    }

    public void setCookieUrl(String cookieUrl) {
        this.cookieUrl = cookieUrl;
    }

    public AffectedRequest getRequest() {
        return request;
    }

    public void setRequest(AffectedRequest request) {
        this.request = request;
    }

    /**
     * The recommended solution to the issue.
     */
    public CookieIssueInsight getInsight() {
        return insight;
    }

    /**
     * The recommended solution to the issue.
     */
    public void setInsight(CookieIssueInsight insight) {
        this.insight = insight;
    }

    public String toString() {
        return "CookieIssueDetails [cookie=" + cookie + ", rawCookieLine=" + rawCookieLine + ", cookieWarningReasons="
                + cookieWarningReasons + ", cookieExclusionReasons=" + cookieExclusionReasons + ", operation="
                + operation + ", siteForCookies=" + siteForCookies + ", cookieUrl=" + cookieUrl + ", request=" + request
                + ", insight=" + insight + "]";
    }
}
