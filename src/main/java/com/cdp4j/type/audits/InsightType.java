// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Represents the category of insight that a cookie issue falls under.
 */
public enum InsightType {
    @SerializedName("GitHubResource")
    @JsonProperty("GitHubResource")
    GitHubResource("GitHubResource"),

    @SerializedName("GracePeriod")
    @JsonProperty("GracePeriod")
    GracePeriod("GracePeriod"),

    @SerializedName("Heuristics")
    @JsonProperty("Heuristics")
    Heuristics("Heuristics"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    InsightType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
