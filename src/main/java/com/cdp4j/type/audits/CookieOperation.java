// SPDX-License-Identifier: MIT
package com.cdp4j.type.audits;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum CookieOperation {
    @SerializedName("SetCookie")
    @JsonProperty("SetCookie")
    SetCookie("SetCookie"),

    @SerializedName("ReadCookie")
    @JsonProperty("ReadCookie")
    ReadCookie("ReadCookie"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CookieOperation(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
