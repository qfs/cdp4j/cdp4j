// SPDX-License-Identifier: MIT
package com.cdp4j.type.filesystem;

import static java.util.Collections.emptyList;

import java.util.List;

public class Directory {
    private String name;

    private List<String> nestedDirectories = emptyList();

    private List<File> nestedFiles = emptyList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getNestedDirectories() {
        return nestedDirectories;
    }

    public void setNestedDirectories(List<String> nestedDirectories) {
        this.nestedDirectories = nestedDirectories;
    }

    /**
     * Files that are directly nested under this directory.
     */
    public List<File> getNestedFiles() {
        return nestedFiles;
    }

    /**
     * Files that are directly nested under this directory.
     */
    public void setNestedFiles(List<File> nestedFiles) {
        this.nestedFiles = nestedFiles;
    }

    public String toString() {
        return "Directory [name=" + name + ", nestedDirectories=" + nestedDirectories + ", nestedFiles=" + nestedFiles
                + "]";
    }
}
