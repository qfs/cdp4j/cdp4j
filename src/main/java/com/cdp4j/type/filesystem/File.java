// SPDX-License-Identifier: MIT
package com.cdp4j.type.filesystem;

public class File {
    private String name;

    private Double lastModified;

    private Double size;

    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Timestamp
     */
    public Double getLastModified() {
        return lastModified;
    }

    /**
     * Timestamp
     */
    public void setLastModified(Double lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * Size in bytes
     */
    public Double getSize() {
        return size;
    }

    /**
     * Size in bytes
     */
    public void setSize(Double size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return "File [name=" + name + ", lastModified=" + lastModified + ", size=" + size + ", type=" + type + "]";
    }
}
