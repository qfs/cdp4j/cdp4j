// SPDX-License-Identifier: MIT
package com.cdp4j.type.filesystem;

import static java.util.Collections.emptyList;

import java.util.List;

public class BucketFileSystemLocator {
    private String storageKey;

    private String bucketName;

    private List<String> pathComponents = emptyList();

    /**
     * Storage key
     */
    public String getStorageKey() {
        return storageKey;
    }

    /**
     * Storage key
     */
    public void setStorageKey(String storageKey) {
        this.storageKey = storageKey;
    }

    /**
     * Bucket name. Not passing a bucketName will retrieve the default Bucket.
     * (https://developer.mozilla.org/en-US/docs/Web/API/Storage_API#storage_buckets)
     */
    public String getBucketName() {
        return bucketName;
    }

    /**
     * Bucket name. Not passing a bucketName will retrieve the default Bucket.
     * (https://developer.mozilla.org/en-US/docs/Web/API/Storage_API#storage_buckets)
     */
    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    /**
     * Path to the directory using each path component as an array item.
     */
    public List<String> getPathComponents() {
        return pathComponents;
    }

    /**
     * Path to the directory using each path component as an array item.
     */
    public void setPathComponents(List<String> pathComponents) {
        this.pathComponents = pathComponents;
    }

    public String toString() {
        return "BucketFileSystemLocator [storageKey=" + storageKey + ", bucketName=" + bucketName + ", pathComponents="
                + pathComponents + "]";
    }
}
