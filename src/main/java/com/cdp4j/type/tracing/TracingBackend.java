// SPDX-License-Identifier: MIT
package com.cdp4j.type.tracing;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Backend type to use for tracing. chrome uses the Chrome-integrated
 * tracing service and is supported on all platforms. system is only
 * supported on Chrome OS and uses the Perfetto system tracing service.
 * auto chooses system when the perfettoConfig provided to Tracing.start
 * specifies at least one non-Chrome data source; otherwise uses chrome.
 */
public enum TracingBackend {
    @SerializedName("auto")
    @JsonProperty("auto")
    Auto("auto"),

    @SerializedName("chrome")
    @JsonProperty("chrome")
    Chrome("chrome"),

    @SerializedName("system")
    @JsonProperty("system")
    System("system"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    TracingBackend(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
