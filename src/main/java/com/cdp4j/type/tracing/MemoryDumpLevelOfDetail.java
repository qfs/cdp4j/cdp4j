// SPDX-License-Identifier: MIT
package com.cdp4j.type.tracing;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Details exposed when memory request explicitly declared.
 * Keep consistent with memory_dump_request_args.h and
 * memory_instrumentation.mojom
 */
public enum MemoryDumpLevelOfDetail {
    @SerializedName("background")
    @JsonProperty("background")
    Background("background"),

    @SerializedName("light")
    @JsonProperty("light")
    Light("light"),

    @SerializedName("detailed")
    @JsonProperty("detailed")
    Detailed("detailed"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    MemoryDumpLevelOfDetail(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
