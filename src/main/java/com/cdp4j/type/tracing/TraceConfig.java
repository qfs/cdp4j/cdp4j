// SPDX-License-Identifier: MIT
package com.cdp4j.type.tracing;

import static java.util.Collections.emptyList;

import com.cdp4j.type.constant.TraceRecordMode;
import java.util.List;

public class TraceConfig {
    private TraceRecordMode recordMode;

    private Double traceBufferSizeInKb;

    private Boolean enableSampling;

    private Boolean enableSystrace;

    private Boolean enableArgumentFilter;

    private List<String> includedCategories = emptyList();

    private List<String> excludedCategories = emptyList();

    private List<String> syntheticDelays = emptyList();

    private Object memoryDumpConfig;

    /**
     * Controls how the trace buffer stores data.
     */
    public TraceRecordMode getRecordMode() {
        return recordMode;
    }

    /**
     * Controls how the trace buffer stores data.
     */
    public void setRecordMode(TraceRecordMode recordMode) {
        this.recordMode = recordMode;
    }

    /**
     * Size of the trace buffer in kilobytes. If not specified or zero is passed, a
     * default value of 200 MB would be used.
     */
    public Double getTraceBufferSizeInKb() {
        return traceBufferSizeInKb;
    }

    /**
     * Size of the trace buffer in kilobytes. If not specified or zero is passed, a
     * default value of 200 MB would be used.
     */
    public void setTraceBufferSizeInKb(Double traceBufferSizeInKb) {
        this.traceBufferSizeInKb = traceBufferSizeInKb;
    }

    /**
     * Turns on JavaScript stack sampling.
     */
    public Boolean isEnableSampling() {
        return enableSampling;
    }

    /**
     * Turns on JavaScript stack sampling.
     */
    public void setEnableSampling(Boolean enableSampling) {
        this.enableSampling = enableSampling;
    }

    /**
     * Turns on system tracing.
     */
    public Boolean isEnableSystrace() {
        return enableSystrace;
    }

    /**
     * Turns on system tracing.
     */
    public void setEnableSystrace(Boolean enableSystrace) {
        this.enableSystrace = enableSystrace;
    }

    /**
     * Turns on argument filter.
     */
    public Boolean isEnableArgumentFilter() {
        return enableArgumentFilter;
    }

    /**
     * Turns on argument filter.
     */
    public void setEnableArgumentFilter(Boolean enableArgumentFilter) {
        this.enableArgumentFilter = enableArgumentFilter;
    }

    /**
     * Included category filters.
     */
    public List<String> getIncludedCategories() {
        return includedCategories;
    }

    /**
     * Included category filters.
     */
    public void setIncludedCategories(List<String> includedCategories) {
        this.includedCategories = includedCategories;
    }

    /**
     * Excluded category filters.
     */
    public List<String> getExcludedCategories() {
        return excludedCategories;
    }

    /**
     * Excluded category filters.
     */
    public void setExcludedCategories(List<String> excludedCategories) {
        this.excludedCategories = excludedCategories;
    }

    /**
     * Configuration to synthesize the delays in tracing.
     */
    public List<String> getSyntheticDelays() {
        return syntheticDelays;
    }

    /**
     * Configuration to synthesize the delays in tracing.
     */
    public void setSyntheticDelays(List<String> syntheticDelays) {
        this.syntheticDelays = syntheticDelays;
    }

    /**
     * Configuration for memory dump triggers. Used only when "memory-infra"
     * category is enabled.
     */
    public Object getMemoryDumpConfig() {
        return memoryDumpConfig;
    }

    /**
     * Configuration for memory dump triggers. Used only when "memory-infra"
     * category is enabled.
     */
    public void setMemoryDumpConfig(Object memoryDumpConfig) {
        this.memoryDumpConfig = memoryDumpConfig;
    }

    public String toString() {
        return "TraceConfig [recordMode=" + recordMode + ", traceBufferSizeInKb=" + traceBufferSizeInKb
                + ", enableSampling=" + enableSampling + ", enableSystrace=" + enableSystrace
                + ", enableArgumentFilter=" + enableArgumentFilter + ", includedCategories=" + includedCategories
                + ", excludedCategories=" + excludedCategories + ", syntheticDelays=" + syntheticDelays
                + ", memoryDumpConfig=" + memoryDumpConfig + "]";
    }
}
