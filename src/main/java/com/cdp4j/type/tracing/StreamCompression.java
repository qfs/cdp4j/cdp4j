// SPDX-License-Identifier: MIT
package com.cdp4j.type.tracing;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Compression type to use for traces returned via streams.
 */
public enum StreamCompression {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("gzip")
    @JsonProperty("gzip")
    Gzip("gzip"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    StreamCompression(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
