// SPDX-License-Identifier: MIT
package com.cdp4j.type.tracing;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Data format of a trace. Can be either the legacy JSON format or the
 * protocol buffer format. Note that the JSON format will be deprecated soon.
 */
public enum StreamFormat {
    @SerializedName("json")
    @JsonProperty("json")
    Json("json"),

    @SerializedName("proto")
    @JsonProperty("proto")
    Proto("proto"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    StreamFormat(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
