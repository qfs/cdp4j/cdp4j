// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of network fetches auctions can do.
 */
public enum InterestGroupAuctionFetchType {
    @SerializedName("bidderJs")
    @JsonProperty("bidderJs")
    BidderJs("bidderJs"),

    @SerializedName("bidderWasm")
    @JsonProperty("bidderWasm")
    BidderWasm("bidderWasm"),

    @SerializedName("sellerJs")
    @JsonProperty("sellerJs")
    SellerJs("sellerJs"),

    @SerializedName("bidderTrustedSignals")
    @JsonProperty("bidderTrustedSignals")
    BidderTrustedSignals("bidderTrustedSignals"),

    @SerializedName("sellerTrustedSignals")
    @JsonProperty("sellerTrustedSignals")
    SellerTrustedSignals("sellerTrustedSignals"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    InterestGroupAuctionFetchType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
