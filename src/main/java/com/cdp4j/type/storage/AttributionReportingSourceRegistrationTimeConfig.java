// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AttributionReportingSourceRegistrationTimeConfig {
    @SerializedName("include")
    @JsonProperty("include")
    Include("include"),

    @SerializedName("exclude")
    @JsonProperty("exclude")
    Exclude("exclude"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AttributionReportingSourceRegistrationTimeConfig(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
