// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingSourceRegistration {
    private Double time;

    private Integer expiry;

    private List<AttributionReportingTriggerSpec> triggerSpecs = emptyList();

    private Integer aggregatableReportWindow;

    private AttributionReportingSourceType type;

    private String sourceOrigin;

    private String reportingOrigin;

    private List<String> destinationSites = emptyList();

    private String eventId;

    private String priority;

    private List<AttributionReportingFilterDataEntry> filterData = emptyList();

    private List<AttributionReportingAggregationKeysEntry> aggregationKeys = emptyList();

    private String debugKey;

    private AttributionReportingTriggerDataMatching triggerDataMatching;

    private String destinationLimitPriority;

    private AttributionReportingAggregatableDebugReportingConfig aggregatableDebugReportingConfig;

    private AttributionScopesData scopesData;

    private Integer maxEventLevelReports;

    public Double getTime() {
        return time;
    }

    public void setTime(Double time) {
        this.time = time;
    }

    /**
     * duration in seconds
     */
    public Integer getExpiry() {
        return expiry;
    }

    /**
     * duration in seconds
     */
    public void setExpiry(Integer expiry) {
        this.expiry = expiry;
    }

    public List<AttributionReportingTriggerSpec> getTriggerSpecs() {
        return triggerSpecs;
    }

    public void setTriggerSpecs(List<AttributionReportingTriggerSpec> triggerSpecs) {
        this.triggerSpecs = triggerSpecs;
    }

    /**
     * duration in seconds
     */
    public Integer getAggregatableReportWindow() {
        return aggregatableReportWindow;
    }

    /**
     * duration in seconds
     */
    public void setAggregatableReportWindow(Integer aggregatableReportWindow) {
        this.aggregatableReportWindow = aggregatableReportWindow;
    }

    public AttributionReportingSourceType getType() {
        return type;
    }

    public void setType(AttributionReportingSourceType type) {
        this.type = type;
    }

    public String getSourceOrigin() {
        return sourceOrigin;
    }

    public void setSourceOrigin(String sourceOrigin) {
        this.sourceOrigin = sourceOrigin;
    }

    public String getReportingOrigin() {
        return reportingOrigin;
    }

    public void setReportingOrigin(String reportingOrigin) {
        this.reportingOrigin = reportingOrigin;
    }

    public List<String> getDestinationSites() {
        return destinationSites;
    }

    public void setDestinationSites(List<String> destinationSites) {
        this.destinationSites = destinationSites;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public List<AttributionReportingFilterDataEntry> getFilterData() {
        return filterData;
    }

    public void setFilterData(List<AttributionReportingFilterDataEntry> filterData) {
        this.filterData = filterData;
    }

    public List<AttributionReportingAggregationKeysEntry> getAggregationKeys() {
        return aggregationKeys;
    }

    public void setAggregationKeys(List<AttributionReportingAggregationKeysEntry> aggregationKeys) {
        this.aggregationKeys = aggregationKeys;
    }

    public String getDebugKey() {
        return debugKey;
    }

    public void setDebugKey(String debugKey) {
        this.debugKey = debugKey;
    }

    public AttributionReportingTriggerDataMatching getTriggerDataMatching() {
        return triggerDataMatching;
    }

    public void setTriggerDataMatching(AttributionReportingTriggerDataMatching triggerDataMatching) {
        this.triggerDataMatching = triggerDataMatching;
    }

    public String getDestinationLimitPriority() {
        return destinationLimitPriority;
    }

    public void setDestinationLimitPriority(String destinationLimitPriority) {
        this.destinationLimitPriority = destinationLimitPriority;
    }

    public AttributionReportingAggregatableDebugReportingConfig getAggregatableDebugReportingConfig() {
        return aggregatableDebugReportingConfig;
    }

    public void setAggregatableDebugReportingConfig(
            AttributionReportingAggregatableDebugReportingConfig aggregatableDebugReportingConfig) {
        this.aggregatableDebugReportingConfig = aggregatableDebugReportingConfig;
    }

    public AttributionScopesData getScopesData() {
        return scopesData;
    }

    public void setScopesData(AttributionScopesData scopesData) {
        this.scopesData = scopesData;
    }

    public Integer getMaxEventLevelReports() {
        return maxEventLevelReports;
    }

    public void setMaxEventLevelReports(Integer maxEventLevelReports) {
        this.maxEventLevelReports = maxEventLevelReports;
    }

    public String toString() {
        return "AttributionReportingSourceRegistration [time=" + time + ", expiry=" + expiry + ", triggerSpecs="
                + triggerSpecs + ", aggregatableReportWindow=" + aggregatableReportWindow + ", type=" + type
                + ", sourceOrigin=" + sourceOrigin + ", reportingOrigin=" + reportingOrigin + ", destinationSites="
                + destinationSites + ", eventId=" + eventId + ", priority=" + priority + ", filterData=" + filterData
                + ", aggregationKeys=" + aggregationKeys + ", debugKey=" + debugKey + ", triggerDataMatching="
                + triggerDataMatching + ", destinationLimitPriority=" + destinationLimitPriority
                + ", aggregatableDebugReportingConfig=" + aggregatableDebugReportingConfig + ", scopesData="
                + scopesData + ", maxEventLevelReports=" + maxEventLevelReports + "]";
    }
}
