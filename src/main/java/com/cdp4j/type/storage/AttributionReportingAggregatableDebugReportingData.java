// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingAggregatableDebugReportingData {
    private String keyPiece;

    private Double value;

    private List<String> types = emptyList();

    public String getKeyPiece() {
        return keyPiece;
    }

    public void setKeyPiece(String keyPiece) {
        this.keyPiece = keyPiece;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public Double getValue() {
        return value;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public void setValue(Double value) {
        this.value = value;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String toString() {
        return "AttributionReportingAggregatableDebugReportingData [keyPiece=" + keyPiece + ", value=" + value
                + ", types=" + types + "]";
    }
}
