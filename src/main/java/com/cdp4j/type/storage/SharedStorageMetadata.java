// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

/**
 * Details for an origin's shared storage.
 */
public class SharedStorageMetadata {
    private Double creationTime;

    private Integer length;

    private Double remainingBudget;

    private Integer bytesUsed;

    /**
     * Time when the origin's shared storage was last created.
     */
    public Double getCreationTime() {
        return creationTime;
    }

    /**
     * Time when the origin's shared storage was last created.
     */
    public void setCreationTime(Double creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * Number of key-value pairs stored in origin's shared storage.
     */
    public Integer getLength() {
        return length;
    }

    /**
     * Number of key-value pairs stored in origin's shared storage.
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * Current amount of bits of entropy remaining in the navigation budget.
     */
    public Double getRemainingBudget() {
        return remainingBudget;
    }

    /**
     * Current amount of bits of entropy remaining in the navigation budget.
     */
    public void setRemainingBudget(Double remainingBudget) {
        this.remainingBudget = remainingBudget;
    }

    /**
     * Total number of bytes stored as key-value pairs in origin's shared storage.
     */
    public Integer getBytesUsed() {
        return bytesUsed;
    }

    /**
     * Total number of bytes stored as key-value pairs in origin's shared storage.
     */
    public void setBytesUsed(Integer bytesUsed) {
        this.bytesUsed = bytesUsed;
    }

    public String toString() {
        return "SharedStorageMetadata [creationTime=" + creationTime + ", length=" + length + ", remainingBudget="
                + remainingBudget + ", bytesUsed=" + bytesUsed + "]";
    }
}
