// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AttributionReportingAggregatableResult {
    @SerializedName("success")
    @JsonProperty("success")
    Success("success"),

    @SerializedName("internalError")
    @JsonProperty("internalError")
    InternalError("internalError"),

    @SerializedName("noCapacityForAttributionDestination")
    @JsonProperty("noCapacityForAttributionDestination")
    NoCapacityForAttributionDestination("noCapacityForAttributionDestination"),

    @SerializedName("noMatchingSources")
    @JsonProperty("noMatchingSources")
    NoMatchingSources("noMatchingSources"),

    @SerializedName("excessiveAttributions")
    @JsonProperty("excessiveAttributions")
    ExcessiveAttributions("excessiveAttributions"),

    @SerializedName("excessiveReportingOrigins")
    @JsonProperty("excessiveReportingOrigins")
    ExcessiveReportingOrigins("excessiveReportingOrigins"),

    @SerializedName("noHistograms")
    @JsonProperty("noHistograms")
    NoHistograms("noHistograms"),

    @SerializedName("insufficientBudget")
    @JsonProperty("insufficientBudget")
    InsufficientBudget("insufficientBudget"),

    @SerializedName("insufficientNamedBudget")
    @JsonProperty("insufficientNamedBudget")
    InsufficientNamedBudget("insufficientNamedBudget"),

    @SerializedName("noMatchingSourceFilterData")
    @JsonProperty("noMatchingSourceFilterData")
    NoMatchingSourceFilterData("noMatchingSourceFilterData"),

    @SerializedName("notRegistered")
    @JsonProperty("notRegistered")
    NotRegistered("notRegistered"),

    @SerializedName("prohibitedByBrowserPolicy")
    @JsonProperty("prohibitedByBrowserPolicy")
    ProhibitedByBrowserPolicy("prohibitedByBrowserPolicy"),

    @SerializedName("deduplicated")
    @JsonProperty("deduplicated")
    Deduplicated("deduplicated"),

    @SerializedName("reportWindowPassed")
    @JsonProperty("reportWindowPassed")
    ReportWindowPassed("reportWindowPassed"),

    @SerializedName("excessiveReports")
    @JsonProperty("excessiveReports")
    ExcessiveReports("excessiveReports"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AttributionReportingAggregatableResult(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
