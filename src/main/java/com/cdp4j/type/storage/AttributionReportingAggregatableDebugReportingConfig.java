// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingAggregatableDebugReportingConfig {
    private Double budget;

    private String keyPiece;

    private List<AttributionReportingAggregatableDebugReportingData> debugData = emptyList();

    private String aggregationCoordinatorOrigin;

    /**
     * number instead of integer because not all uint32 can be represented by int,
     * only present for source registrations
     */
    public Double getBudget() {
        return budget;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int,
     * only present for source registrations
     */
    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public String getKeyPiece() {
        return keyPiece;
    }

    public void setKeyPiece(String keyPiece) {
        this.keyPiece = keyPiece;
    }

    public List<AttributionReportingAggregatableDebugReportingData> getDebugData() {
        return debugData;
    }

    public void setDebugData(List<AttributionReportingAggregatableDebugReportingData> debugData) {
        this.debugData = debugData;
    }

    public String getAggregationCoordinatorOrigin() {
        return aggregationCoordinatorOrigin;
    }

    public void setAggregationCoordinatorOrigin(String aggregationCoordinatorOrigin) {
        this.aggregationCoordinatorOrigin = aggregationCoordinatorOrigin;
    }

    public String toString() {
        return "AttributionReportingAggregatableDebugReportingConfig [budget=" + budget + ", keyPiece=" + keyPiece
                + ", debugData=" + debugData + ", aggregationCoordinatorOrigin=" + aggregationCoordinatorOrigin + "]";
    }
}
