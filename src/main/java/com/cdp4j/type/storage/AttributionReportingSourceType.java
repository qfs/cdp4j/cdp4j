// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AttributionReportingSourceType {
    @SerializedName("navigation")
    @JsonProperty("navigation")
    Navigation("navigation"),

    @SerializedName("event")
    @JsonProperty("event")
    Event("event"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AttributionReportingSourceType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
