// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of auction events.
 */
public enum InterestGroupAuctionEventType {
    @SerializedName("started")
    @JsonProperty("started")
    Started("started"),

    @SerializedName("configResolved")
    @JsonProperty("configResolved")
    ConfigResolved("configResolved"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    InterestGroupAuctionEventType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
