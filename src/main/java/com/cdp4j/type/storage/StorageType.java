// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of possible storage types.
 */
public enum StorageType {
    @SerializedName("cookies")
    @JsonProperty("cookies")
    Cookies("cookies"),

    @SerializedName("file_systems")
    @JsonProperty("file_systems")
    FileSystems("file_systems"),

    @SerializedName("indexeddb")
    @JsonProperty("indexeddb")
    Indexeddb("indexeddb"),

    @SerializedName("local_storage")
    @JsonProperty("local_storage")
    LocalStorage("local_storage"),

    @SerializedName("shader_cache")
    @JsonProperty("shader_cache")
    ShaderCache("shader_cache"),

    @SerializedName("websql")
    @JsonProperty("websql")
    Websql("websql"),

    @SerializedName("service_workers")
    @JsonProperty("service_workers")
    ServiceWorkers("service_workers"),

    @SerializedName("cache_storage")
    @JsonProperty("cache_storage")
    CacheStorage("cache_storage"),

    @SerializedName("interest_groups")
    @JsonProperty("interest_groups")
    InterestGroups("interest_groups"),

    @SerializedName("shared_storage")
    @JsonProperty("shared_storage")
    SharedStorage("shared_storage"),

    @SerializedName("storage_buckets")
    @JsonProperty("storage_buckets")
    StorageBuckets("storage_buckets"),

    @SerializedName("all")
    @JsonProperty("all")
    All("all"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    StorageType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
