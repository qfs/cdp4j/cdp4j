// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of interest group access types.
 */
public enum InterestGroupAccessType {
    @SerializedName("join")
    @JsonProperty("join")
    Join("join"),

    @SerializedName("leave")
    @JsonProperty("leave")
    Leave("leave"),

    @SerializedName("update")
    @JsonProperty("update")
    Update("update"),

    @SerializedName("loaded")
    @JsonProperty("loaded")
    Loaded("loaded"),

    @SerializedName("bid")
    @JsonProperty("bid")
    Bid("bid"),

    @SerializedName("win")
    @JsonProperty("win")
    Win("win"),

    @SerializedName("additionalBid")
    @JsonProperty("additionalBid")
    AdditionalBid("additionalBid"),

    @SerializedName("additionalBidWin")
    @JsonProperty("additionalBidWin")
    AdditionalBidWin("additionalBidWin"),

    @SerializedName("topLevelBid")
    @JsonProperty("topLevelBid")
    TopLevelBid("topLevelBid"),

    @SerializedName("topLevelAdditionalBid")
    @JsonProperty("topLevelAdditionalBid")
    TopLevelAdditionalBid("topLevelAdditionalBid"),

    @SerializedName("clear")
    @JsonProperty("clear")
    Clear("clear"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    InterestGroupAccessType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
