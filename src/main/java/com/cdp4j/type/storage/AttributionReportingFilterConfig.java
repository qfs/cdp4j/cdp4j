// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingFilterConfig {
    private List<AttributionReportingFilterDataEntry> filterValues = emptyList();

    private Integer lookbackWindow;

    public List<AttributionReportingFilterDataEntry> getFilterValues() {
        return filterValues;
    }

    public void setFilterValues(List<AttributionReportingFilterDataEntry> filterValues) {
        this.filterValues = filterValues;
    }

    /**
     * duration in seconds
     */
    public Integer getLookbackWindow() {
        return lookbackWindow;
    }

    /**
     * duration in seconds
     */
    public void setLookbackWindow(Integer lookbackWindow) {
        this.lookbackWindow = lookbackWindow;
    }

    public String toString() {
        return "AttributionReportingFilterConfig [filterValues=" + filterValues + ", lookbackWindow=" + lookbackWindow
                + "]";
    }
}
