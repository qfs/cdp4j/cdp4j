// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.cdp4j.annotation.Experimental;

@Experimental
public class AttributionReportingAggregatableValueDictEntry {
    private String key;

    private Double value;

    private String filteringId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public Double getValue() {
        return value;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public void setValue(Double value) {
        this.value = value;
    }

    public String getFilteringId() {
        return filteringId;
    }

    public void setFilteringId(String filteringId) {
        this.filteringId = filteringId;
    }

    public String toString() {
        return "AttributionReportingAggregatableValueDictEntry [key=" + key + ", value=" + value + ", filteringId="
                + filteringId + "]";
    }
}
