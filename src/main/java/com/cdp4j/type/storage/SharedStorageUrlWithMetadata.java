// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Bundles a candidate URL with its reporting metadata.
 */
public class SharedStorageUrlWithMetadata {
    private String url;

    private List<SharedStorageReportingMetadata> reportingMetadata = emptyList();

    /**
     * Spec of candidate URL.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Spec of candidate URL.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Any associated reporting metadata.
     */
    public List<SharedStorageReportingMetadata> getReportingMetadata() {
        return reportingMetadata;
    }

    /**
     * Any associated reporting metadata.
     */
    public void setReportingMetadata(List<SharedStorageReportingMetadata> reportingMetadata) {
        this.reportingMetadata = reportingMetadata;
    }

    public String toString() {
        return "SharedStorageUrlWithMetadata [url=" + url + ", reportingMetadata=" + reportingMetadata + "]";
    }
}
