// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingTriggerSpec {
    private List<Double> triggerData = emptyList();

    private AttributionReportingEventReportWindows eventReportWindows;

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public List<Double> getTriggerData() {
        return triggerData;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public void setTriggerData(List<Double> triggerData) {
        this.triggerData = triggerData;
    }

    public AttributionReportingEventReportWindows getEventReportWindows() {
        return eventReportWindows;
    }

    public void setEventReportWindows(AttributionReportingEventReportWindows eventReportWindows) {
        this.eventReportWindows = eventReportWindows;
    }

    public String toString() {
        return "AttributionReportingTriggerSpec [triggerData=" + triggerData + ", eventReportWindows="
                + eventReportWindows + "]";
    }
}
