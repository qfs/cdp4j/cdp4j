// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.cdp4j.annotation.Experimental;

@Experimental
public class AttributionReportingAggregationKeysEntry {
    private String key;

    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        return "AttributionReportingAggregationKeysEntry [key=" + key + ", value=" + value + "]";
    }
}
