// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

public class StorageBucketInfo {
    private StorageBucket bucket;

    private String id;

    private Double expiration;

    private Double quota;

    private Boolean persistent;

    private StorageBucketsDurability durability;

    public StorageBucket getBucket() {
        return bucket;
    }

    public void setBucket(StorageBucket bucket) {
        this.bucket = bucket;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getExpiration() {
        return expiration;
    }

    public void setExpiration(Double expiration) {
        this.expiration = expiration;
    }

    /**
     * Storage quota (bytes).
     */
    public Double getQuota() {
        return quota;
    }

    /**
     * Storage quota (bytes).
     */
    public void setQuota(Double quota) {
        this.quota = quota;
    }

    public Boolean isPersistent() {
        return persistent;
    }

    public void setPersistent(Boolean persistent) {
        this.persistent = persistent;
    }

    public StorageBucketsDurability getDurability() {
        return durability;
    }

    public void setDurability(StorageBucketsDurability durability) {
        this.durability = durability;
    }

    public String toString() {
        return "StorageBucketInfo [bucket=" + bucket + ", id=" + id + ", expiration=" + expiration + ", quota=" + quota
                + ", persistent=" + persistent + ", durability=" + durability + "]";
    }
}
