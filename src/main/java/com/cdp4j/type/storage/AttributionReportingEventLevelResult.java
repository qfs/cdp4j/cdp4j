// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AttributionReportingEventLevelResult {
    @SerializedName("success")
    @JsonProperty("success")
    Success("success"),

    @SerializedName("successDroppedLowerPriority")
    @JsonProperty("successDroppedLowerPriority")
    SuccessDroppedLowerPriority("successDroppedLowerPriority"),

    @SerializedName("internalError")
    @JsonProperty("internalError")
    InternalError("internalError"),

    @SerializedName("noCapacityForAttributionDestination")
    @JsonProperty("noCapacityForAttributionDestination")
    NoCapacityForAttributionDestination("noCapacityForAttributionDestination"),

    @SerializedName("noMatchingSources")
    @JsonProperty("noMatchingSources")
    NoMatchingSources("noMatchingSources"),

    @SerializedName("deduplicated")
    @JsonProperty("deduplicated")
    Deduplicated("deduplicated"),

    @SerializedName("excessiveAttributions")
    @JsonProperty("excessiveAttributions")
    ExcessiveAttributions("excessiveAttributions"),

    @SerializedName("priorityTooLow")
    @JsonProperty("priorityTooLow")
    PriorityTooLow("priorityTooLow"),

    @SerializedName("neverAttributedSource")
    @JsonProperty("neverAttributedSource")
    NeverAttributedSource("neverAttributedSource"),

    @SerializedName("excessiveReportingOrigins")
    @JsonProperty("excessiveReportingOrigins")
    ExcessiveReportingOrigins("excessiveReportingOrigins"),

    @SerializedName("noMatchingSourceFilterData")
    @JsonProperty("noMatchingSourceFilterData")
    NoMatchingSourceFilterData("noMatchingSourceFilterData"),

    @SerializedName("prohibitedByBrowserPolicy")
    @JsonProperty("prohibitedByBrowserPolicy")
    ProhibitedByBrowserPolicy("prohibitedByBrowserPolicy"),

    @SerializedName("noMatchingConfigurations")
    @JsonProperty("noMatchingConfigurations")
    NoMatchingConfigurations("noMatchingConfigurations"),

    @SerializedName("excessiveReports")
    @JsonProperty("excessiveReports")
    ExcessiveReports("excessiveReports"),

    @SerializedName("falselyAttributedSource")
    @JsonProperty("falselyAttributedSource")
    FalselyAttributedSource("falselyAttributedSource"),

    @SerializedName("reportWindowPassed")
    @JsonProperty("reportWindowPassed")
    ReportWindowPassed("reportWindowPassed"),

    @SerializedName("notRegistered")
    @JsonProperty("notRegistered")
    NotRegistered("notRegistered"),

    @SerializedName("reportWindowNotStarted")
    @JsonProperty("reportWindowNotStarted")
    ReportWindowNotStarted("reportWindowNotStarted"),

    @SerializedName("noMatchingTriggerData")
    @JsonProperty("noMatchingTriggerData")
    NoMatchingTriggerData("noMatchingTriggerData"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AttributionReportingEventLevelResult(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
