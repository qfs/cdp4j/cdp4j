// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum StorageBucketsDurability {
    @SerializedName("relaxed")
    @JsonProperty("relaxed")
    Relaxed("relaxed"),

    @SerializedName("strict")
    @JsonProperty("strict")
    Strict("strict"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    StorageBucketsDurability(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
