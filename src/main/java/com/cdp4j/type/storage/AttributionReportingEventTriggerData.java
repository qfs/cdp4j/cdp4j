// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.cdp4j.annotation.Experimental;

@Experimental
public class AttributionReportingEventTriggerData {
    private String data;

    private String priority;

    private String dedupKey;

    private AttributionReportingFilterPair filters;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDedupKey() {
        return dedupKey;
    }

    public void setDedupKey(String dedupKey) {
        this.dedupKey = dedupKey;
    }

    public AttributionReportingFilterPair getFilters() {
        return filters;
    }

    public void setFilters(AttributionReportingFilterPair filters) {
        this.filters = filters;
    }

    public String toString() {
        return "AttributionReportingEventTriggerData [data=" + data + ", priority=" + priority + ", dedupKey="
                + dedupKey + ", filters=" + filters + "]";
    }
}
