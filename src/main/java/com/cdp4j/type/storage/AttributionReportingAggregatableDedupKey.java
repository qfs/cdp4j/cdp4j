// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.cdp4j.annotation.Experimental;

@Experimental
public class AttributionReportingAggregatableDedupKey {
    private String dedupKey;

    private AttributionReportingFilterPair filters;

    public String getDedupKey() {
        return dedupKey;
    }

    public void setDedupKey(String dedupKey) {
        this.dedupKey = dedupKey;
    }

    public AttributionReportingFilterPair getFilters() {
        return filters;
    }

    public void setFilters(AttributionReportingFilterPair filters) {
        this.filters = filters;
    }

    public String toString() {
        return "AttributionReportingAggregatableDedupKey [dedupKey=" + dedupKey + ", filters=" + filters + "]";
    }
}
