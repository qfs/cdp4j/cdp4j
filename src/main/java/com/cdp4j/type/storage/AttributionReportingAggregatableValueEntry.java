// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingAggregatableValueEntry {
    private List<AttributionReportingAggregatableValueDictEntry> values = emptyList();

    private AttributionReportingFilterPair filters;

    public List<AttributionReportingAggregatableValueDictEntry> getValues() {
        return values;
    }

    public void setValues(List<AttributionReportingAggregatableValueDictEntry> values) {
        this.values = values;
    }

    public AttributionReportingFilterPair getFilters() {
        return filters;
    }

    public void setFilters(AttributionReportingFilterPair filters) {
        this.filters = filters;
    }

    public String toString() {
        return "AttributionReportingAggregatableValueEntry [values=" + values + ", filters=" + filters + "]";
    }
}
