// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of shared storage access types.
 */
public enum SharedStorageAccessType {
    @SerializedName("documentAddModule")
    @JsonProperty("documentAddModule")
    DocumentAddModule("documentAddModule"),

    @SerializedName("documentSelectURL")
    @JsonProperty("documentSelectURL")
    DocumentSelectURL("documentSelectURL"),

    @SerializedName("documentRun")
    @JsonProperty("documentRun")
    DocumentRun("documentRun"),

    @SerializedName("documentSet")
    @JsonProperty("documentSet")
    DocumentSet("documentSet"),

    @SerializedName("documentAppend")
    @JsonProperty("documentAppend")
    DocumentAppend("documentAppend"),

    @SerializedName("documentDelete")
    @JsonProperty("documentDelete")
    DocumentDelete("documentDelete"),

    @SerializedName("documentClear")
    @JsonProperty("documentClear")
    DocumentClear("documentClear"),

    @SerializedName("documentGet")
    @JsonProperty("documentGet")
    DocumentGet("documentGet"),

    @SerializedName("workletSet")
    @JsonProperty("workletSet")
    WorkletSet("workletSet"),

    @SerializedName("workletAppend")
    @JsonProperty("workletAppend")
    WorkletAppend("workletAppend"),

    @SerializedName("workletDelete")
    @JsonProperty("workletDelete")
    WorkletDelete("workletDelete"),

    @SerializedName("workletClear")
    @JsonProperty("workletClear")
    WorkletClear("workletClear"),

    @SerializedName("workletGet")
    @JsonProperty("workletGet")
    WorkletGet("workletGet"),

    @SerializedName("workletKeys")
    @JsonProperty("workletKeys")
    WorkletKeys("workletKeys"),

    @SerializedName("workletEntries")
    @JsonProperty("workletEntries")
    WorkletEntries("workletEntries"),

    @SerializedName("workletLength")
    @JsonProperty("workletLength")
    WorkletLength("workletLength"),

    @SerializedName("workletRemainingBudget")
    @JsonProperty("workletRemainingBudget")
    WorkletRemainingBudget("workletRemainingBudget"),

    @SerializedName("headerSet")
    @JsonProperty("headerSet")
    HeaderSet("headerSet"),

    @SerializedName("headerAppend")
    @JsonProperty("headerAppend")
    HeaderAppend("headerAppend"),

    @SerializedName("headerDelete")
    @JsonProperty("headerDelete")
    HeaderDelete("headerDelete"),

    @SerializedName("headerClear")
    @JsonProperty("headerClear")
    HeaderClear("headerClear"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SharedStorageAccessType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
