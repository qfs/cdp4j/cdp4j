// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

public class StorageBucket {
    private String storageKey;

    private String name;

    public String getStorageKey() {
        return storageKey;
    }

    public void setStorageKey(String storageKey) {
        this.storageKey = storageKey;
    }

    /**
     * If not specified, it is the default bucket of the storageKey.
     */
    public String getName() {
        return name;
    }

    /**
     * If not specified, it is the default bucket of the storageKey.
     */
    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "StorageBucket [storageKey=" + storageKey + ", name=" + name + "]";
    }
}
