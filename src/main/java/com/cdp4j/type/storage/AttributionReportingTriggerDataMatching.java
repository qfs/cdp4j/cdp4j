// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AttributionReportingTriggerDataMatching {
    @SerializedName("exact")
    @JsonProperty("exact")
    Exact("exact"),

    @SerializedName("modulus")
    @JsonProperty("modulus")
    Modulus("modulus"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AttributionReportingTriggerDataMatching(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
