// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionScopesData {
    private List<String> values = emptyList();

    private Double limit;

    private Double maxEventStates;

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public Double getLimit() {
        return limit;
    }

    /**
     * number instead of integer because not all uint32 can be represented by int
     */
    public void setLimit(Double limit) {
        this.limit = limit;
    }

    public Double getMaxEventStates() {
        return maxEventStates;
    }

    public void setMaxEventStates(Double maxEventStates) {
        this.maxEventStates = maxEventStates;
    }

    public String toString() {
        return "AttributionScopesData [values=" + values + ", limit=" + limit + ", maxEventStates=" + maxEventStates
                + "]";
    }
}
