// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingFilterPair {
    private List<AttributionReportingFilterConfig> filters = emptyList();

    private List<AttributionReportingFilterConfig> notFilters = emptyList();

    public List<AttributionReportingFilterConfig> getFilters() {
        return filters;
    }

    public void setFilters(List<AttributionReportingFilterConfig> filters) {
        this.filters = filters;
    }

    public List<AttributionReportingFilterConfig> getNotFilters() {
        return notFilters;
    }

    public void setNotFilters(List<AttributionReportingFilterConfig> notFilters) {
        this.notFilters = notFilters;
    }

    public String toString() {
        return "AttributionReportingFilterPair [filters=" + filters + ", notFilters=" + notFilters + "]";
    }
}
