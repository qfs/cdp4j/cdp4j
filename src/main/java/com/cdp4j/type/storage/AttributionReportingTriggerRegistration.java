// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingTriggerRegistration {
    private AttributionReportingFilterPair filters;

    private String debugKey;

    private List<AttributionReportingAggregatableDedupKey> aggregatableDedupKeys = emptyList();

    private List<AttributionReportingEventTriggerData> eventTriggerData = emptyList();

    private List<AttributionReportingAggregatableTriggerData> aggregatableTriggerData = emptyList();

    private List<AttributionReportingAggregatableValueEntry> aggregatableValues = emptyList();

    private Integer aggregatableFilteringIdMaxBytes;

    private Boolean debugReporting;

    private String aggregationCoordinatorOrigin;

    private AttributionReportingSourceRegistrationTimeConfig sourceRegistrationTimeConfig;

    private String triggerContextId;

    private AttributionReportingAggregatableDebugReportingConfig aggregatableDebugReportingConfig;

    private List<String> scopes = emptyList();

    public AttributionReportingFilterPair getFilters() {
        return filters;
    }

    public void setFilters(AttributionReportingFilterPair filters) {
        this.filters = filters;
    }

    public String getDebugKey() {
        return debugKey;
    }

    public void setDebugKey(String debugKey) {
        this.debugKey = debugKey;
    }

    public List<AttributionReportingAggregatableDedupKey> getAggregatableDedupKeys() {
        return aggregatableDedupKeys;
    }

    public void setAggregatableDedupKeys(List<AttributionReportingAggregatableDedupKey> aggregatableDedupKeys) {
        this.aggregatableDedupKeys = aggregatableDedupKeys;
    }

    public List<AttributionReportingEventTriggerData> getEventTriggerData() {
        return eventTriggerData;
    }

    public void setEventTriggerData(List<AttributionReportingEventTriggerData> eventTriggerData) {
        this.eventTriggerData = eventTriggerData;
    }

    public List<AttributionReportingAggregatableTriggerData> getAggregatableTriggerData() {
        return aggregatableTriggerData;
    }

    public void setAggregatableTriggerData(List<AttributionReportingAggregatableTriggerData> aggregatableTriggerData) {
        this.aggregatableTriggerData = aggregatableTriggerData;
    }

    public List<AttributionReportingAggregatableValueEntry> getAggregatableValues() {
        return aggregatableValues;
    }

    public void setAggregatableValues(List<AttributionReportingAggregatableValueEntry> aggregatableValues) {
        this.aggregatableValues = aggregatableValues;
    }

    public Integer getAggregatableFilteringIdMaxBytes() {
        return aggregatableFilteringIdMaxBytes;
    }

    public void setAggregatableFilteringIdMaxBytes(Integer aggregatableFilteringIdMaxBytes) {
        this.aggregatableFilteringIdMaxBytes = aggregatableFilteringIdMaxBytes;
    }

    public Boolean isDebugReporting() {
        return debugReporting;
    }

    public void setDebugReporting(Boolean debugReporting) {
        this.debugReporting = debugReporting;
    }

    public String getAggregationCoordinatorOrigin() {
        return aggregationCoordinatorOrigin;
    }

    public void setAggregationCoordinatorOrigin(String aggregationCoordinatorOrigin) {
        this.aggregationCoordinatorOrigin = aggregationCoordinatorOrigin;
    }

    public AttributionReportingSourceRegistrationTimeConfig getSourceRegistrationTimeConfig() {
        return sourceRegistrationTimeConfig;
    }

    public void setSourceRegistrationTimeConfig(
            AttributionReportingSourceRegistrationTimeConfig sourceRegistrationTimeConfig) {
        this.sourceRegistrationTimeConfig = sourceRegistrationTimeConfig;
    }

    public String getTriggerContextId() {
        return triggerContextId;
    }

    public void setTriggerContextId(String triggerContextId) {
        this.triggerContextId = triggerContextId;
    }

    public AttributionReportingAggregatableDebugReportingConfig getAggregatableDebugReportingConfig() {
        return aggregatableDebugReportingConfig;
    }

    public void setAggregatableDebugReportingConfig(
            AttributionReportingAggregatableDebugReportingConfig aggregatableDebugReportingConfig) {
        this.aggregatableDebugReportingConfig = aggregatableDebugReportingConfig;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }

    public String toString() {
        return "AttributionReportingTriggerRegistration [filters=" + filters + ", debugKey=" + debugKey
                + ", aggregatableDedupKeys=" + aggregatableDedupKeys + ", eventTriggerData=" + eventTriggerData
                + ", aggregatableTriggerData=" + aggregatableTriggerData + ", aggregatableValues=" + aggregatableValues
                + ", aggregatableFilteringIdMaxBytes=" + aggregatableFilteringIdMaxBytes + ", debugReporting="
                + debugReporting + ", aggregationCoordinatorOrigin=" + aggregationCoordinatorOrigin
                + ", sourceRegistrationTimeConfig=" + sourceRegistrationTimeConfig + ", triggerContextId="
                + triggerContextId + ", aggregatableDebugReportingConfig=" + aggregatableDebugReportingConfig
                + ", scopes=" + scopes + "]";
    }
}
