// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingAggregatableTriggerData {
    private String keyPiece;

    private List<String> sourceKeys = emptyList();

    private AttributionReportingFilterPair filters;

    public String getKeyPiece() {
        return keyPiece;
    }

    public void setKeyPiece(String keyPiece) {
        this.keyPiece = keyPiece;
    }

    public List<String> getSourceKeys() {
        return sourceKeys;
    }

    public void setSourceKeys(List<String> sourceKeys) {
        this.sourceKeys = sourceKeys;
    }

    public AttributionReportingFilterPair getFilters() {
        return filters;
    }

    public void setFilters(AttributionReportingFilterPair filters) {
        this.filters = filters;
    }

    public String toString() {
        return "AttributionReportingAggregatableTriggerData [keyPiece=" + keyPiece + ", sourceKeys=" + sourceKeys
                + ", filters=" + filters + "]";
    }
}
