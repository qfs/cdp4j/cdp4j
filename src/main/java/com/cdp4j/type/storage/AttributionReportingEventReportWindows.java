// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

@Experimental
public class AttributionReportingEventReportWindows {
    private Integer start;

    private List<Integer> ends = emptyList();

    /**
     * duration in seconds
     */
    public Integer getStart() {
        return start;
    }

    /**
     * duration in seconds
     */
    public void setStart(Integer start) {
        this.start = start;
    }

    /**
     * duration in seconds
     */
    public List<Integer> getEnds() {
        return ends;
    }

    /**
     * duration in seconds
     */
    public void setEnds(List<Integer> ends) {
        this.ends = ends;
    }

    public String toString() {
        return "AttributionReportingEventReportWindows [start=" + start + ", ends=" + ends + "]";
    }
}
