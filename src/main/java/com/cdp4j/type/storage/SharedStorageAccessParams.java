// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Bundles the parameters for shared storage access events whose
 * presence/absence can vary according to SharedStorageAccessType.
 */
public class SharedStorageAccessParams {
    private String scriptSourceUrl;

    private String operationName;

    private String serializedData;

    private List<SharedStorageUrlWithMetadata> urlsWithMetadata = emptyList();

    private String key;

    private String value;

    private Boolean ignoreIfPresent;

    /**
     * Spec of the module script URL. Present only for
     * SharedStorageAccessType.documentAddModule.
     */
    public String getScriptSourceUrl() {
        return scriptSourceUrl;
    }

    /**
     * Spec of the module script URL. Present only for
     * SharedStorageAccessType.documentAddModule.
     */
    public void setScriptSourceUrl(String scriptSourceUrl) {
        this.scriptSourceUrl = scriptSourceUrl;
    }

    /**
     * Name of the registered operation to be run. Present only for
     * SharedStorageAccessType.documentRun and
     * SharedStorageAccessType.documentSelectURL.
     */
    public String getOperationName() {
        return operationName;
    }

    /**
     * Name of the registered operation to be run. Present only for
     * SharedStorageAccessType.documentRun and
     * SharedStorageAccessType.documentSelectURL.
     */
    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    /**
     * The operation's serialized data in bytes (converted to a string). Present
     * only for SharedStorageAccessType.documentRun and
     * SharedStorageAccessType.documentSelectURL.
     */
    public String getSerializedData() {
        return serializedData;
    }

    /**
     * The operation's serialized data in bytes (converted to a string). Present
     * only for SharedStorageAccessType.documentRun and
     * SharedStorageAccessType.documentSelectURL.
     */
    public void setSerializedData(String serializedData) {
        this.serializedData = serializedData;
    }

    /**
     * Array of candidate URLs' specs, along with any associated metadata. Present
     * only for SharedStorageAccessType.documentSelectURL.
     */
    public List<SharedStorageUrlWithMetadata> getUrlsWithMetadata() {
        return urlsWithMetadata;
    }

    /**
     * Array of candidate URLs' specs, along with any associated metadata. Present
     * only for SharedStorageAccessType.documentSelectURL.
     */
    public void setUrlsWithMetadata(List<SharedStorageUrlWithMetadata> urlsWithMetadata) {
        this.urlsWithMetadata = urlsWithMetadata;
    }

    /**
     * Key for a specific entry in an origin's shared storage. Present only for
     * SharedStorageAccessType.documentSet, SharedStorageAccessType.documentAppend,
     * SharedStorageAccessType.documentDelete, SharedStorageAccessType.workletSet,
     * SharedStorageAccessType.workletAppend, SharedStorageAccessType.workletDelete,
     * SharedStorageAccessType.workletGet, SharedStorageAccessType.headerSet,
     * SharedStorageAccessType.headerAppend, and
     * SharedStorageAccessType.headerDelete.
     */
    public String getKey() {
        return key;
    }

    /**
     * Key for a specific entry in an origin's shared storage. Present only for
     * SharedStorageAccessType.documentSet, SharedStorageAccessType.documentAppend,
     * SharedStorageAccessType.documentDelete, SharedStorageAccessType.workletSet,
     * SharedStorageAccessType.workletAppend, SharedStorageAccessType.workletDelete,
     * SharedStorageAccessType.workletGet, SharedStorageAccessType.headerSet,
     * SharedStorageAccessType.headerAppend, and
     * SharedStorageAccessType.headerDelete.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Value for a specific entry in an origin's shared storage. Present only for
     * SharedStorageAccessType.documentSet, SharedStorageAccessType.documentAppend,
     * SharedStorageAccessType.workletSet, SharedStorageAccessType.workletAppend,
     * SharedStorageAccessType.headerSet, and SharedStorageAccessType.headerAppend.
     */
    public String getValue() {
        return value;
    }

    /**
     * Value for a specific entry in an origin's shared storage. Present only for
     * SharedStorageAccessType.documentSet, SharedStorageAccessType.documentAppend,
     * SharedStorageAccessType.workletSet, SharedStorageAccessType.workletAppend,
     * SharedStorageAccessType.headerSet, and SharedStorageAccessType.headerAppend.
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Whether or not to set an entry for a key if that key is already present.
     * Present only for SharedStorageAccessType.documentSet,
     * SharedStorageAccessType.workletSet, and SharedStorageAccessType.headerSet.
     */
    public Boolean isIgnoreIfPresent() {
        return ignoreIfPresent;
    }

    /**
     * Whether or not to set an entry for a key if that key is already present.
     * Present only for SharedStorageAccessType.documentSet,
     * SharedStorageAccessType.workletSet, and SharedStorageAccessType.headerSet.
     */
    public void setIgnoreIfPresent(Boolean ignoreIfPresent) {
        this.ignoreIfPresent = ignoreIfPresent;
    }

    public String toString() {
        return "SharedStorageAccessParams [scriptSourceUrl=" + scriptSourceUrl + ", operationName=" + operationName
                + ", serializedData=" + serializedData + ", urlsWithMetadata=" + urlsWithMetadata + ", key=" + key
                + ", value=" + value + ", ignoreIfPresent=" + ignoreIfPresent + "]";
    }
}
