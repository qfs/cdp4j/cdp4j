// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

/**
 * A single Related Website Set object.
 */
@Experimental
public class RelatedWebsiteSet {
    private List<String> primarySites = emptyList();

    private List<String> associatedSites = emptyList();

    private List<String> serviceSites = emptyList();

    /**
     * The primary site of this set, along with the ccTLDs if there is any.
     */
    public List<String> getPrimarySites() {
        return primarySites;
    }

    /**
     * The primary site of this set, along with the ccTLDs if there is any.
     */
    public void setPrimarySites(List<String> primarySites) {
        this.primarySites = primarySites;
    }

    /**
     * The associated sites of this set, along with the ccTLDs if there is any.
     */
    public List<String> getAssociatedSites() {
        return associatedSites;
    }

    /**
     * The associated sites of this set, along with the ccTLDs if there is any.
     */
    public void setAssociatedSites(List<String> associatedSites) {
        this.associatedSites = associatedSites;
    }

    /**
     * The service sites of this set, along with the ccTLDs if there is any.
     */
    public List<String> getServiceSites() {
        return serviceSites;
    }

    /**
     * The service sites of this set, along with the ccTLDs if there is any.
     */
    public void setServiceSites(List<String> serviceSites) {
        this.serviceSites = serviceSites;
    }

    public String toString() {
        return "RelatedWebsiteSet [primarySites=" + primarySites + ", associatedSites=" + associatedSites
                + ", serviceSites=" + serviceSites + "]";
    }
}
