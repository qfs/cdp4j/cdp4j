// SPDX-License-Identifier: MIT
package com.cdp4j.type.storage;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum AttributionReportingSourceRegistrationResult {
    @SerializedName("success")
    @JsonProperty("success")
    Success("success"),

    @SerializedName("internalError")
    @JsonProperty("internalError")
    InternalError("internalError"),

    @SerializedName("insufficientSourceCapacity")
    @JsonProperty("insufficientSourceCapacity")
    InsufficientSourceCapacity("insufficientSourceCapacity"),

    @SerializedName("insufficientUniqueDestinationCapacity")
    @JsonProperty("insufficientUniqueDestinationCapacity")
    InsufficientUniqueDestinationCapacity("insufficientUniqueDestinationCapacity"),

    @SerializedName("excessiveReportingOrigins")
    @JsonProperty("excessiveReportingOrigins")
    ExcessiveReportingOrigins("excessiveReportingOrigins"),

    @SerializedName("prohibitedByBrowserPolicy")
    @JsonProperty("prohibitedByBrowserPolicy")
    ProhibitedByBrowserPolicy("prohibitedByBrowserPolicy"),

    @SerializedName("successNoised")
    @JsonProperty("successNoised")
    SuccessNoised("successNoised"),

    @SerializedName("destinationReportingLimitReached")
    @JsonProperty("destinationReportingLimitReached")
    DestinationReportingLimitReached("destinationReportingLimitReached"),

    @SerializedName("destinationGlobalLimitReached")
    @JsonProperty("destinationGlobalLimitReached")
    DestinationGlobalLimitReached("destinationGlobalLimitReached"),

    @SerializedName("destinationBothLimitsReached")
    @JsonProperty("destinationBothLimitsReached")
    DestinationBothLimitsReached("destinationBothLimitsReached"),

    @SerializedName("reportingOriginsPerSiteLimitReached")
    @JsonProperty("reportingOriginsPerSiteLimitReached")
    ReportingOriginsPerSiteLimitReached("reportingOriginsPerSiteLimitReached"),

    @SerializedName("exceedsMaxChannelCapacity")
    @JsonProperty("exceedsMaxChannelCapacity")
    ExceedsMaxChannelCapacity("exceedsMaxChannelCapacity"),

    @SerializedName("exceedsMaxScopesChannelCapacity")
    @JsonProperty("exceedsMaxScopesChannelCapacity")
    ExceedsMaxScopesChannelCapacity("exceedsMaxScopesChannelCapacity"),

    @SerializedName("exceedsMaxTriggerStateCardinality")
    @JsonProperty("exceedsMaxTriggerStateCardinality")
    ExceedsMaxTriggerStateCardinality("exceedsMaxTriggerStateCardinality"),

    @SerializedName("exceedsMaxEventStatesLimit")
    @JsonProperty("exceedsMaxEventStatesLimit")
    ExceedsMaxEventStatesLimit("exceedsMaxEventStatesLimit"),

    @SerializedName("destinationPerDayReportingLimitReached")
    @JsonProperty("destinationPerDayReportingLimitReached")
    DestinationPerDayReportingLimitReached("destinationPerDayReportingLimitReached"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AttributionReportingSourceRegistrationResult(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
