// SPDX-License-Identifier: MIT
package com.cdp4j.type.indexeddb;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Database with an array of object stores.
 */
public class DatabaseWithObjectStores {
    private String name;

    private Double version;

    private List<ObjectStore> objectStores = emptyList();

    /**
     * Database name.
     */
    public String getName() {
        return name;
    }

    /**
     * Database name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Database version (type is not 'integer', as the standard requires the version
     * number to be 'unsigned long long')
     */
    public Double getVersion() {
        return version;
    }

    /**
     * Database version (type is not 'integer', as the standard requires the version
     * number to be 'unsigned long long')
     */
    public void setVersion(Double version) {
        this.version = version;
    }

    /**
     * Object stores in this database.
     */
    public List<ObjectStore> getObjectStores() {
        return objectStores;
    }

    /**
     * Object stores in this database.
     */
    public void setObjectStores(List<ObjectStore> objectStores) {
        this.objectStores = objectStores;
    }

    public String toString() {
        return "DatabaseWithObjectStores [name=" + name + ", version=" + version + ", objectStores=" + objectStores
                + "]";
    }
}
