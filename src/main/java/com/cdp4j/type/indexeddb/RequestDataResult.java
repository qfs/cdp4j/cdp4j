// SPDX-License-Identifier: MIT
package com.cdp4j.type.indexeddb;

import java.util.List;

public class RequestDataResult {
    private List<DataEntry> objectStoreDataEntries;

    private Boolean hasMore;

    /**
     * Array of object store data entries.
     */
    public List<DataEntry> getObjectStoreDataEntries() {
        return objectStoreDataEntries;
    }

    /**
     * Array of object store data entries.
     */
    public void setObjectStoreDataEntries(List<DataEntry> objectStoreDataEntries) {
        this.objectStoreDataEntries = objectStoreDataEntries;
    }

    /**
     * If true, there are more entries to fetch in the given range.
     */
    public Boolean getHasMore() {
        return hasMore;
    }

    /**
     * If true, there are more entries to fetch in the given range.
     */
    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    public String toString() {
        return "RequestDataResult [objectStoreDataEntries=" + objectStoreDataEntries + ", hasMore=" + hasMore + "]";
    }
}
