// SPDX-License-Identifier: MIT
package com.cdp4j.type.indexeddb;

import static java.util.Collections.emptyList;

import com.cdp4j.type.constant.KeyPathType;
import java.util.List;

/**
 * Key path.
 */
public class KeyPath {
    private KeyPathType type;

    private String string;

    private List<String> array = emptyList();

    /**
     * Key path type.
     */
    public KeyPathType getType() {
        return type;
    }

    /**
     * Key path type.
     */
    public void setType(KeyPathType type) {
        this.type = type;
    }

    /**
     * String value.
     */
    public String getString() {
        return string;
    }

    /**
     * String value.
     */
    public void setString(String string) {
        this.string = string;
    }

    /**
     * Array value.
     */
    public List<String> getArray() {
        return array;
    }

    /**
     * Array value.
     */
    public void setArray(List<String> array) {
        this.array = array;
    }

    public String toString() {
        return "KeyPath [type=" + type + ", string=" + string + ", array=" + array + "]";
    }
}
