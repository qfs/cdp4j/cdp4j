// SPDX-License-Identifier: MIT
package com.cdp4j.type.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum MouseButton {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("left")
    @JsonProperty("left")
    Left("left"),

    @SerializedName("middle")
    @JsonProperty("middle")
    Middle("middle"),

    @SerializedName("right")
    @JsonProperty("right")
    Right("right"),

    @SerializedName("back")
    @JsonProperty("back")
    Back("back"),

    @SerializedName("forward")
    @JsonProperty("forward")
    Forward("forward"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    MouseButton(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
