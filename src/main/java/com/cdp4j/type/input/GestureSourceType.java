// SPDX-License-Identifier: MIT
package com.cdp4j.type.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum GestureSourceType {
    @SerializedName("default")
    @JsonProperty("default")
    Default("default"),

    @SerializedName("touch")
    @JsonProperty("touch")
    Touch("touch"),

    @SerializedName("mouse")
    @JsonProperty("mouse")
    Mouse("mouse"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    GestureSourceType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
