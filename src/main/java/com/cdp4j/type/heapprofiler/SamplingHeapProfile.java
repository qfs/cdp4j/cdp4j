// SPDX-License-Identifier: MIT
package com.cdp4j.type.heapprofiler;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Sampling profile.
 */
public class SamplingHeapProfile {
    private SamplingHeapProfileNode head;

    private List<SamplingHeapProfileSample> samples = emptyList();

    public SamplingHeapProfileNode getHead() {
        return head;
    }

    public void setHead(SamplingHeapProfileNode head) {
        this.head = head;
    }

    public List<SamplingHeapProfileSample> getSamples() {
        return samples;
    }

    public void setSamples(List<SamplingHeapProfileSample> samples) {
        this.samples = samples;
    }

    public String toString() {
        return "SamplingHeapProfile [head=" + head + ", samples=" + samples + "]";
    }
}
