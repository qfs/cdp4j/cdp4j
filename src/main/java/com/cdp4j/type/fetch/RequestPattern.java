// SPDX-License-Identifier: MIT
package com.cdp4j.type.fetch;

import com.cdp4j.type.network.ResourceType;

public class RequestPattern {
    private String urlPattern;

    private ResourceType resourceType;

    private RequestStage requestStage;

    /**
     * Wildcards ('*' -> zero or more, '?' -> exactly one) are allowed. Escape
     * character is backslash. Omitting is equivalent to "*".
     */
    public String getUrlPattern() {
        return urlPattern;
    }

    /**
     * Wildcards ('*' -> zero or more, '?' -> exactly one) are allowed. Escape
     * character is backslash. Omitting is equivalent to "*".
     */
    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }

    /**
     * If set, only requests for matching resource types will be intercepted.
     */
    public ResourceType getResourceType() {
        return resourceType;
    }

    /**
     * If set, only requests for matching resource types will be intercepted.
     */
    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * Stage at which to begin intercepting requests. Default is Request.
     */
    public RequestStage getRequestStage() {
        return requestStage;
    }

    /**
     * Stage at which to begin intercepting requests. Default is Request.
     */
    public void setRequestStage(RequestStage requestStage) {
        this.requestStage = requestStage;
    }

    public String toString() {
        return "RequestPattern [urlPattern=" + urlPattern + ", resourceType=" + resourceType + ", requestStage="
                + requestStage + "]";
    }
}
