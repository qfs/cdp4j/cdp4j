// SPDX-License-Identifier: MIT
package com.cdp4j.type.fetch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Stages of the request to handle. Request will intercept before the request is
 * sent. Response will intercept after the response is received (but before response
 * body is received).
 */
public enum RequestStage {
    @SerializedName("Request")
    @JsonProperty("Request")
    Request("Request"),

    @SerializedName("Response")
    @JsonProperty("Response")
    Response("Response"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    RequestStage(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
