// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

/**
 * Information of headers to be displayed when the header mismatch occurred.
 */
public class PrerenderMismatchedHeaders {
    private String headerName;

    private String initialValue;

    private String activationValue;

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(String initialValue) {
        this.initialValue = initialValue;
    }

    public String getActivationValue() {
        return activationValue;
    }

    public void setActivationValue(String activationValue) {
        this.activationValue = activationValue;
    }

    public String toString() {
        return "PrerenderMismatchedHeaders [headerName=" + headerName + ", initialValue=" + initialValue
                + ", activationValue=" + activationValue + "]";
    }
}
