// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

/**
 * Corresponds to SpeculationRuleSet
 */
public class RuleSet {
    private String id;

    private String loaderId;

    private String sourceText;

    private Integer backendNodeId;

    private String url;

    private String requestId;

    private RuleSetErrorType errorType;

    private String errorMessage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * Identifies a document which the rule set is associated with.
     */
    public String getLoaderId() {
        return loaderId;
    }

    /**
     * Identifies a document which the rule set is associated with.
     */
    public void setLoaderId(String loaderId) {
        this.loaderId = loaderId;
    }

    /**
     * Source text of JSON representing the rule set. If it comes from <script> tag,
     * it is the textContent of the node. Note that it is a JSON for valid case.
     *
     * See also: - https://wicg.github.io/nav-speculation/speculation-rules.html -
     * https://github.com/WICG/nav-speculation/blob/main/triggers.md
     */
    public String getSourceText() {
        return sourceText;
    }

    /**
     * Source text of JSON representing the rule set. If it comes from <script> tag,
     * it is the textContent of the node. Note that it is a JSON for valid case.
     *
     * See also: - https://wicg.github.io/nav-speculation/speculation-rules.html -
     * https://github.com/WICG/nav-speculation/blob/main/triggers.md
     */
    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    /**
     * A speculation rule set is either added through an inline <script> tag or
     * through an external resource via the 'Speculation-Rules' HTTP header. For the
     * first case, we include the BackendNodeId of the relevant <script> tag. For
     * the second case, we include the external URL where the rule set was loaded
     * from, and also RequestId if Network domain is enabled.
     *
     * See also: -
     * https://wicg.github.io/nav-speculation/speculation-rules.html#speculation-rules-script
     * -
     * https://wicg.github.io/nav-speculation/speculation-rules.html#speculation-rules-header
     */
    public Integer getBackendNodeId() {
        return backendNodeId;
    }

    /**
     * A speculation rule set is either added through an inline <script> tag or
     * through an external resource via the 'Speculation-Rules' HTTP header. For the
     * first case, we include the BackendNodeId of the relevant <script> tag. For
     * the second case, we include the external URL where the rule set was loaded
     * from, and also RequestId if Network domain is enabled.
     *
     * See also: -
     * https://wicg.github.io/nav-speculation/speculation-rules.html#speculation-rules-script
     * -
     * https://wicg.github.io/nav-speculation/speculation-rules.html#speculation-rules-header
     */
    public void setBackendNodeId(Integer backendNodeId) {
        this.backendNodeId = backendNodeId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     * Error information errorMessage is null iff errorType is null.
     */
    public RuleSetErrorType getErrorType() {
        return errorType;
    }

    /**
     * Error information errorMessage is null iff errorType is null.
     */
    public void setErrorType(RuleSetErrorType errorType) {
        this.errorType = errorType;
    }

    /**
     * TODO(https://crbug.com/1425354): Replace this property with structured error.
     */
    @Deprecated
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * TODO(https://crbug.com/1425354): Replace this property with structured error.
     */
    @Deprecated
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String toString() {
        return "RuleSet [id=" + id + ", loaderId=" + loaderId + ", sourceText=" + sourceText + ", backendNodeId="
                + backendNodeId + ", url=" + url + ", requestId=" + requestId + ", errorType=" + errorType
                + ", errorMessage=" + errorMessage + "]";
    }
}
