// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum RuleSetErrorType {
    @SerializedName("SourceIsNotJsonObject")
    @JsonProperty("SourceIsNotJsonObject")
    SourceIsNotJsonObject("SourceIsNotJsonObject"),

    @SerializedName("InvalidRulesSkipped")
    @JsonProperty("InvalidRulesSkipped")
    InvalidRulesSkipped("InvalidRulesSkipped"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    RuleSetErrorType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
