// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * List of FinalStatus reasons for Prerender2.
 */
public enum PrerenderFinalStatus {
    @SerializedName("Activated")
    @JsonProperty("Activated")
    Activated("Activated"),

    @SerializedName("Destroyed")
    @JsonProperty("Destroyed")
    Destroyed("Destroyed"),

    @SerializedName("LowEndDevice")
    @JsonProperty("LowEndDevice")
    LowEndDevice("LowEndDevice"),

    @SerializedName("InvalidSchemeRedirect")
    @JsonProperty("InvalidSchemeRedirect")
    InvalidSchemeRedirect("InvalidSchemeRedirect"),

    @SerializedName("InvalidSchemeNavigation")
    @JsonProperty("InvalidSchemeNavigation")
    InvalidSchemeNavigation("InvalidSchemeNavigation"),

    @SerializedName("NavigationRequestBlockedByCsp")
    @JsonProperty("NavigationRequestBlockedByCsp")
    NavigationRequestBlockedByCsp("NavigationRequestBlockedByCsp"),

    @SerializedName("MainFrameNavigation")
    @JsonProperty("MainFrameNavigation")
    MainFrameNavigation("MainFrameNavigation"),

    @SerializedName("MojoBinderPolicy")
    @JsonProperty("MojoBinderPolicy")
    MojoBinderPolicy("MojoBinderPolicy"),

    @SerializedName("RendererProcessCrashed")
    @JsonProperty("RendererProcessCrashed")
    RendererProcessCrashed("RendererProcessCrashed"),

    @SerializedName("RendererProcessKilled")
    @JsonProperty("RendererProcessKilled")
    RendererProcessKilled("RendererProcessKilled"),

    @SerializedName("Download")
    @JsonProperty("Download")
    Download("Download"),

    @SerializedName("TriggerDestroyed")
    @JsonProperty("TriggerDestroyed")
    TriggerDestroyed("TriggerDestroyed"),

    @SerializedName("NavigationNotCommitted")
    @JsonProperty("NavigationNotCommitted")
    NavigationNotCommitted("NavigationNotCommitted"),

    @SerializedName("NavigationBadHttpStatus")
    @JsonProperty("NavigationBadHttpStatus")
    NavigationBadHttpStatus("NavigationBadHttpStatus"),

    @SerializedName("ClientCertRequested")
    @JsonProperty("ClientCertRequested")
    ClientCertRequested("ClientCertRequested"),

    @SerializedName("NavigationRequestNetworkError")
    @JsonProperty("NavigationRequestNetworkError")
    NavigationRequestNetworkError("NavigationRequestNetworkError"),

    @SerializedName("CancelAllHostsForTesting")
    @JsonProperty("CancelAllHostsForTesting")
    CancelAllHostsForTesting("CancelAllHostsForTesting"),

    @SerializedName("DidFailLoad")
    @JsonProperty("DidFailLoad")
    DidFailLoad("DidFailLoad"),

    @SerializedName("Stop")
    @JsonProperty("Stop")
    Stop("Stop"),

    @SerializedName("SslCertificateError")
    @JsonProperty("SslCertificateError")
    SslCertificateError("SslCertificateError"),

    @SerializedName("LoginAuthRequested")
    @JsonProperty("LoginAuthRequested")
    LoginAuthRequested("LoginAuthRequested"),

    @SerializedName("UaChangeRequiresReload")
    @JsonProperty("UaChangeRequiresReload")
    UaChangeRequiresReload("UaChangeRequiresReload"),

    @SerializedName("BlockedByClient")
    @JsonProperty("BlockedByClient")
    BlockedByClient("BlockedByClient"),

    @SerializedName("AudioOutputDeviceRequested")
    @JsonProperty("AudioOutputDeviceRequested")
    AudioOutputDeviceRequested("AudioOutputDeviceRequested"),

    @SerializedName("MixedContent")
    @JsonProperty("MixedContent")
    MixedContent("MixedContent"),

    @SerializedName("TriggerBackgrounded")
    @JsonProperty("TriggerBackgrounded")
    TriggerBackgrounded("TriggerBackgrounded"),

    @SerializedName("MemoryLimitExceeded")
    @JsonProperty("MemoryLimitExceeded")
    MemoryLimitExceeded("MemoryLimitExceeded"),

    @SerializedName("DataSaverEnabled")
    @JsonProperty("DataSaverEnabled")
    DataSaverEnabled("DataSaverEnabled"),

    @SerializedName("TriggerUrlHasEffectiveUrl")
    @JsonProperty("TriggerUrlHasEffectiveUrl")
    TriggerUrlHasEffectiveUrl("TriggerUrlHasEffectiveUrl"),

    @SerializedName("ActivatedBeforeStarted")
    @JsonProperty("ActivatedBeforeStarted")
    ActivatedBeforeStarted("ActivatedBeforeStarted"),

    @SerializedName("InactivePageRestriction")
    @JsonProperty("InactivePageRestriction")
    InactivePageRestriction("InactivePageRestriction"),

    @SerializedName("StartFailed")
    @JsonProperty("StartFailed")
    StartFailed("StartFailed"),

    @SerializedName("TimeoutBackgrounded")
    @JsonProperty("TimeoutBackgrounded")
    TimeoutBackgrounded("TimeoutBackgrounded"),

    @SerializedName("CrossSiteRedirectInInitialNavigation")
    @JsonProperty("CrossSiteRedirectInInitialNavigation")
    CrossSiteRedirectInInitialNavigation("CrossSiteRedirectInInitialNavigation"),

    @SerializedName("CrossSiteNavigationInInitialNavigation")
    @JsonProperty("CrossSiteNavigationInInitialNavigation")
    CrossSiteNavigationInInitialNavigation("CrossSiteNavigationInInitialNavigation"),

    @SerializedName("SameSiteCrossOriginRedirectNotOptInInInitialNavigation")
    @JsonProperty("SameSiteCrossOriginRedirectNotOptInInInitialNavigation")
    SameSiteCrossOriginRedirectNotOptInInInitialNavigation("SameSiteCrossOriginRedirectNotOptInInInitialNavigation"),

    @SerializedName("SameSiteCrossOriginNavigationNotOptInInInitialNavigation")
    @JsonProperty("SameSiteCrossOriginNavigationNotOptInInInitialNavigation")
    SameSiteCrossOriginNavigationNotOptInInInitialNavigation(
            "SameSiteCrossOriginNavigationNotOptInInInitialNavigation"),

    @SerializedName("ActivationNavigationParameterMismatch")
    @JsonProperty("ActivationNavigationParameterMismatch")
    ActivationNavigationParameterMismatch("ActivationNavigationParameterMismatch"),

    @SerializedName("ActivatedInBackground")
    @JsonProperty("ActivatedInBackground")
    ActivatedInBackground("ActivatedInBackground"),

    @SerializedName("EmbedderHostDisallowed")
    @JsonProperty("EmbedderHostDisallowed")
    EmbedderHostDisallowed("EmbedderHostDisallowed"),

    @SerializedName("ActivationNavigationDestroyedBeforeSuccess")
    @JsonProperty("ActivationNavigationDestroyedBeforeSuccess")
    ActivationNavigationDestroyedBeforeSuccess("ActivationNavigationDestroyedBeforeSuccess"),

    @SerializedName("TabClosedByUserGesture")
    @JsonProperty("TabClosedByUserGesture")
    TabClosedByUserGesture("TabClosedByUserGesture"),

    @SerializedName("TabClosedWithoutUserGesture")
    @JsonProperty("TabClosedWithoutUserGesture")
    TabClosedWithoutUserGesture("TabClosedWithoutUserGesture"),

    @SerializedName("PrimaryMainFrameRendererProcessCrashed")
    @JsonProperty("PrimaryMainFrameRendererProcessCrashed")
    PrimaryMainFrameRendererProcessCrashed("PrimaryMainFrameRendererProcessCrashed"),

    @SerializedName("PrimaryMainFrameRendererProcessKilled")
    @JsonProperty("PrimaryMainFrameRendererProcessKilled")
    PrimaryMainFrameRendererProcessKilled("PrimaryMainFrameRendererProcessKilled"),

    @SerializedName("ActivationFramePolicyNotCompatible")
    @JsonProperty("ActivationFramePolicyNotCompatible")
    ActivationFramePolicyNotCompatible("ActivationFramePolicyNotCompatible"),

    @SerializedName("PreloadingDisabled")
    @JsonProperty("PreloadingDisabled")
    PreloadingDisabled("PreloadingDisabled"),

    @SerializedName("BatterySaverEnabled")
    @JsonProperty("BatterySaverEnabled")
    BatterySaverEnabled("BatterySaverEnabled"),

    @SerializedName("ActivatedDuringMainFrameNavigation")
    @JsonProperty("ActivatedDuringMainFrameNavigation")
    ActivatedDuringMainFrameNavigation("ActivatedDuringMainFrameNavigation"),

    @SerializedName("PreloadingUnsupportedByWebContents")
    @JsonProperty("PreloadingUnsupportedByWebContents")
    PreloadingUnsupportedByWebContents("PreloadingUnsupportedByWebContents"),

    @SerializedName("CrossSiteRedirectInMainFrameNavigation")
    @JsonProperty("CrossSiteRedirectInMainFrameNavigation")
    CrossSiteRedirectInMainFrameNavigation("CrossSiteRedirectInMainFrameNavigation"),

    @SerializedName("CrossSiteNavigationInMainFrameNavigation")
    @JsonProperty("CrossSiteNavigationInMainFrameNavigation")
    CrossSiteNavigationInMainFrameNavigation("CrossSiteNavigationInMainFrameNavigation"),

    @SerializedName("SameSiteCrossOriginRedirectNotOptInInMainFrameNavigation")
    @JsonProperty("SameSiteCrossOriginRedirectNotOptInInMainFrameNavigation")
    SameSiteCrossOriginRedirectNotOptInInMainFrameNavigation(
            "SameSiteCrossOriginRedirectNotOptInInMainFrameNavigation"),

    @SerializedName("SameSiteCrossOriginNavigationNotOptInInMainFrameNavigation")
    @JsonProperty("SameSiteCrossOriginNavigationNotOptInInMainFrameNavigation")
    SameSiteCrossOriginNavigationNotOptInInMainFrameNavigation(
            "SameSiteCrossOriginNavigationNotOptInInMainFrameNavigation"),

    @SerializedName("MemoryPressureOnTrigger")
    @JsonProperty("MemoryPressureOnTrigger")
    MemoryPressureOnTrigger("MemoryPressureOnTrigger"),

    @SerializedName("MemoryPressureAfterTriggered")
    @JsonProperty("MemoryPressureAfterTriggered")
    MemoryPressureAfterTriggered("MemoryPressureAfterTriggered"),

    @SerializedName("PrerenderingDisabledByDevTools")
    @JsonProperty("PrerenderingDisabledByDevTools")
    PrerenderingDisabledByDevTools("PrerenderingDisabledByDevTools"),

    @SerializedName("SpeculationRuleRemoved")
    @JsonProperty("SpeculationRuleRemoved")
    SpeculationRuleRemoved("SpeculationRuleRemoved"),

    @SerializedName("ActivatedWithAuxiliaryBrowsingContexts")
    @JsonProperty("ActivatedWithAuxiliaryBrowsingContexts")
    ActivatedWithAuxiliaryBrowsingContexts("ActivatedWithAuxiliaryBrowsingContexts"),

    @SerializedName("MaxNumOfRunningEagerPrerendersExceeded")
    @JsonProperty("MaxNumOfRunningEagerPrerendersExceeded")
    MaxNumOfRunningEagerPrerendersExceeded("MaxNumOfRunningEagerPrerendersExceeded"),

    @SerializedName("MaxNumOfRunningNonEagerPrerendersExceeded")
    @JsonProperty("MaxNumOfRunningNonEagerPrerendersExceeded")
    MaxNumOfRunningNonEagerPrerendersExceeded("MaxNumOfRunningNonEagerPrerendersExceeded"),

    @SerializedName("MaxNumOfRunningEmbedderPrerendersExceeded")
    @JsonProperty("MaxNumOfRunningEmbedderPrerendersExceeded")
    MaxNumOfRunningEmbedderPrerendersExceeded("MaxNumOfRunningEmbedderPrerendersExceeded"),

    @SerializedName("PrerenderingUrlHasEffectiveUrl")
    @JsonProperty("PrerenderingUrlHasEffectiveUrl")
    PrerenderingUrlHasEffectiveUrl("PrerenderingUrlHasEffectiveUrl"),

    @SerializedName("RedirectedPrerenderingUrlHasEffectiveUrl")
    @JsonProperty("RedirectedPrerenderingUrlHasEffectiveUrl")
    RedirectedPrerenderingUrlHasEffectiveUrl("RedirectedPrerenderingUrlHasEffectiveUrl"),

    @SerializedName("ActivationUrlHasEffectiveUrl")
    @JsonProperty("ActivationUrlHasEffectiveUrl")
    ActivationUrlHasEffectiveUrl("ActivationUrlHasEffectiveUrl"),

    @SerializedName("JavaScriptInterfaceAdded")
    @JsonProperty("JavaScriptInterfaceAdded")
    JavaScriptInterfaceAdded("JavaScriptInterfaceAdded"),

    @SerializedName("JavaScriptInterfaceRemoved")
    @JsonProperty("JavaScriptInterfaceRemoved")
    JavaScriptInterfaceRemoved("JavaScriptInterfaceRemoved"),

    @SerializedName("AllPrerenderingCanceled")
    @JsonProperty("AllPrerenderingCanceled")
    AllPrerenderingCanceled("AllPrerenderingCanceled"),

    @SerializedName("WindowClosed")
    @JsonProperty("WindowClosed")
    WindowClosed("WindowClosed"),

    @SerializedName("SlowNetwork")
    @JsonProperty("SlowNetwork")
    SlowNetwork("SlowNetwork"),

    @SerializedName("OtherPrerenderedPageActivated")
    @JsonProperty("OtherPrerenderedPageActivated")
    OtherPrerenderedPageActivated("OtherPrerenderedPageActivated"),

    @SerializedName("V8OptimizerDisabled")
    @JsonProperty("V8OptimizerDisabled")
    V8OptimizerDisabled("V8OptimizerDisabled"),

    @SerializedName("PrerenderFailedDuringPrefetch")
    @JsonProperty("PrerenderFailedDuringPrefetch")
    PrerenderFailedDuringPrefetch("PrerenderFailedDuringPrefetch"),

    @SerializedName("BrowsingDataRemoved")
    @JsonProperty("BrowsingDataRemoved")
    BrowsingDataRemoved("BrowsingDataRemoved"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PrerenderFinalStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
