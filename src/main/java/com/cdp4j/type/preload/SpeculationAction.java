// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The type of preloading attempted. It corresponds to
 * mojom::SpeculationAction (although PrefetchWithSubresources is omitted as it
 * isn't being used by clients).
 */
public enum SpeculationAction {
    @SerializedName("Prefetch")
    @JsonProperty("Prefetch")
    Prefetch("Prefetch"),

    @SerializedName("Prerender")
    @JsonProperty("Prerender")
    Prerender("Prerender"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SpeculationAction(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
