// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Preloading status values, see also PreloadingTriggeringOutcome. This
 * status is shared by prefetchStatusUpdated and prerenderStatusUpdated.
 */
public enum PreloadingStatus {
    @SerializedName("Pending")
    @JsonProperty("Pending")
    Pending("Pending"),

    @SerializedName("Running")
    @JsonProperty("Running")
    Running("Running"),

    @SerializedName("Ready")
    @JsonProperty("Ready")
    Ready("Ready"),

    @SerializedName("Success")
    @JsonProperty("Success")
    Success("Success"),

    @SerializedName("Failure")
    @JsonProperty("Failure")
    Failure("Failure"),

    @SerializedName("NotSupported")
    @JsonProperty("NotSupported")
    NotSupported("NotSupported"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PreloadingStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
