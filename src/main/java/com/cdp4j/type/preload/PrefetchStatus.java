// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * TODO(https://crbug.com/1384419): revisit the list of PrefetchStatus and
 * filter out the ones that aren't necessary to the developers.
 */
public enum PrefetchStatus {
    @SerializedName("PrefetchAllowed")
    @JsonProperty("PrefetchAllowed")
    PrefetchAllowed("PrefetchAllowed"),

    @SerializedName("PrefetchFailedIneligibleRedirect")
    @JsonProperty("PrefetchFailedIneligibleRedirect")
    PrefetchFailedIneligibleRedirect("PrefetchFailedIneligibleRedirect"),

    @SerializedName("PrefetchFailedInvalidRedirect")
    @JsonProperty("PrefetchFailedInvalidRedirect")
    PrefetchFailedInvalidRedirect("PrefetchFailedInvalidRedirect"),

    @SerializedName("PrefetchFailedMIMENotSupported")
    @JsonProperty("PrefetchFailedMIMENotSupported")
    PrefetchFailedMIMENotSupported("PrefetchFailedMIMENotSupported"),

    @SerializedName("PrefetchFailedNetError")
    @JsonProperty("PrefetchFailedNetError")
    PrefetchFailedNetError("PrefetchFailedNetError"),

    @SerializedName("PrefetchFailedNon2XX")
    @JsonProperty("PrefetchFailedNon2XX")
    PrefetchFailedNon2XX("PrefetchFailedNon2XX"),

    @SerializedName("PrefetchEvictedAfterCandidateRemoved")
    @JsonProperty("PrefetchEvictedAfterCandidateRemoved")
    PrefetchEvictedAfterCandidateRemoved("PrefetchEvictedAfterCandidateRemoved"),

    @SerializedName("PrefetchEvictedForNewerPrefetch")
    @JsonProperty("PrefetchEvictedForNewerPrefetch")
    PrefetchEvictedForNewerPrefetch("PrefetchEvictedForNewerPrefetch"),

    @SerializedName("PrefetchHeldback")
    @JsonProperty("PrefetchHeldback")
    PrefetchHeldback("PrefetchHeldback"),

    @SerializedName("PrefetchIneligibleRetryAfter")
    @JsonProperty("PrefetchIneligibleRetryAfter")
    PrefetchIneligibleRetryAfter("PrefetchIneligibleRetryAfter"),

    @SerializedName("PrefetchIsPrivacyDecoy")
    @JsonProperty("PrefetchIsPrivacyDecoy")
    PrefetchIsPrivacyDecoy("PrefetchIsPrivacyDecoy"),

    @SerializedName("PrefetchIsStale")
    @JsonProperty("PrefetchIsStale")
    PrefetchIsStale("PrefetchIsStale"),

    @SerializedName("PrefetchNotEligibleBrowserContextOffTheRecord")
    @JsonProperty("PrefetchNotEligibleBrowserContextOffTheRecord")
    PrefetchNotEligibleBrowserContextOffTheRecord("PrefetchNotEligibleBrowserContextOffTheRecord"),

    @SerializedName("PrefetchNotEligibleDataSaverEnabled")
    @JsonProperty("PrefetchNotEligibleDataSaverEnabled")
    PrefetchNotEligibleDataSaverEnabled("PrefetchNotEligibleDataSaverEnabled"),

    @SerializedName("PrefetchNotEligibleExistingProxy")
    @JsonProperty("PrefetchNotEligibleExistingProxy")
    PrefetchNotEligibleExistingProxy("PrefetchNotEligibleExistingProxy"),

    @SerializedName("PrefetchNotEligibleHostIsNonUnique")
    @JsonProperty("PrefetchNotEligibleHostIsNonUnique")
    PrefetchNotEligibleHostIsNonUnique("PrefetchNotEligibleHostIsNonUnique"),

    @SerializedName("PrefetchNotEligibleNonDefaultStoragePartition")
    @JsonProperty("PrefetchNotEligibleNonDefaultStoragePartition")
    PrefetchNotEligibleNonDefaultStoragePartition("PrefetchNotEligibleNonDefaultStoragePartition"),

    @SerializedName("PrefetchNotEligibleSameSiteCrossOriginPrefetchRequiredProxy")
    @JsonProperty("PrefetchNotEligibleSameSiteCrossOriginPrefetchRequiredProxy")
    PrefetchNotEligibleSameSiteCrossOriginPrefetchRequiredProxy(
            "PrefetchNotEligibleSameSiteCrossOriginPrefetchRequiredProxy"),

    @SerializedName("PrefetchNotEligibleSchemeIsNotHttps")
    @JsonProperty("PrefetchNotEligibleSchemeIsNotHttps")
    PrefetchNotEligibleSchemeIsNotHttps("PrefetchNotEligibleSchemeIsNotHttps"),

    @SerializedName("PrefetchNotEligibleUserHasCookies")
    @JsonProperty("PrefetchNotEligibleUserHasCookies")
    PrefetchNotEligibleUserHasCookies("PrefetchNotEligibleUserHasCookies"),

    @SerializedName("PrefetchNotEligibleUserHasServiceWorker")
    @JsonProperty("PrefetchNotEligibleUserHasServiceWorker")
    PrefetchNotEligibleUserHasServiceWorker("PrefetchNotEligibleUserHasServiceWorker"),

    @SerializedName("PrefetchNotEligibleBatterySaverEnabled")
    @JsonProperty("PrefetchNotEligibleBatterySaverEnabled")
    PrefetchNotEligibleBatterySaverEnabled("PrefetchNotEligibleBatterySaverEnabled"),

    @SerializedName("PrefetchNotEligiblePreloadingDisabled")
    @JsonProperty("PrefetchNotEligiblePreloadingDisabled")
    PrefetchNotEligiblePreloadingDisabled("PrefetchNotEligiblePreloadingDisabled"),

    @SerializedName("PrefetchNotFinishedInTime")
    @JsonProperty("PrefetchNotFinishedInTime")
    PrefetchNotFinishedInTime("PrefetchNotFinishedInTime"),

    @SerializedName("PrefetchNotStarted")
    @JsonProperty("PrefetchNotStarted")
    PrefetchNotStarted("PrefetchNotStarted"),

    @SerializedName("PrefetchNotUsedCookiesChanged")
    @JsonProperty("PrefetchNotUsedCookiesChanged")
    PrefetchNotUsedCookiesChanged("PrefetchNotUsedCookiesChanged"),

    @SerializedName("PrefetchProxyNotAvailable")
    @JsonProperty("PrefetchProxyNotAvailable")
    PrefetchProxyNotAvailable("PrefetchProxyNotAvailable"),

    @SerializedName("PrefetchResponseUsed")
    @JsonProperty("PrefetchResponseUsed")
    PrefetchResponseUsed("PrefetchResponseUsed"),

    @SerializedName("PrefetchSuccessfulButNotUsed")
    @JsonProperty("PrefetchSuccessfulButNotUsed")
    PrefetchSuccessfulButNotUsed("PrefetchSuccessfulButNotUsed"),

    @SerializedName("PrefetchNotUsedProbeFailed")
    @JsonProperty("PrefetchNotUsedProbeFailed")
    PrefetchNotUsedProbeFailed("PrefetchNotUsedProbeFailed"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PrefetchStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
