// SPDX-License-Identifier: MIT
package com.cdp4j.type.preload;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Corresponds to mojom::SpeculationTargetHint.
 * See https://github.com/WICG/nav-speculation/blob/main/triggers.md#window-name-targeting-hints
 */
public enum SpeculationTargetHint {
    @SerializedName("Blank")
    @JsonProperty("Blank")
    Blank("Blank"),

    @SerializedName("Self")
    @JsonProperty("Self")
    Self("Self"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SpeculationTargetHint(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
