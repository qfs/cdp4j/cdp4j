// SPDX-License-Identifier: MIT
package com.cdp4j.type.security;

import static java.util.Collections.emptyList;

import com.cdp4j.annotation.Experimental;
import java.util.List;

/**
 * Security state information about the page.
 */
@Experimental
public class VisibleSecurityState {
    private SecurityState securityState;

    private CertificateSecurityState certificateSecurityState;

    private SafetyTipInfo safetyTipInfo;

    private List<String> securityStateIssueIds = emptyList();

    /**
     * The security level of the page.
     */
    public SecurityState getSecurityState() {
        return securityState;
    }

    /**
     * The security level of the page.
     */
    public void setSecurityState(SecurityState securityState) {
        this.securityState = securityState;
    }

    /**
     * Security state details about the page certificate.
     */
    public CertificateSecurityState getCertificateSecurityState() {
        return certificateSecurityState;
    }

    /**
     * Security state details about the page certificate.
     */
    public void setCertificateSecurityState(CertificateSecurityState certificateSecurityState) {
        this.certificateSecurityState = certificateSecurityState;
    }

    /**
     * The type of Safety Tip triggered on the page. Note that this field will be
     * set even if the Safety Tip UI was not actually shown.
     */
    public SafetyTipInfo getSafetyTipInfo() {
        return safetyTipInfo;
    }

    /**
     * The type of Safety Tip triggered on the page. Note that this field will be
     * set even if the Safety Tip UI was not actually shown.
     */
    public void setSafetyTipInfo(SafetyTipInfo safetyTipInfo) {
        this.safetyTipInfo = safetyTipInfo;
    }

    /**
     * Array of security state issues ids.
     */
    public List<String> getSecurityStateIssueIds() {
        return securityStateIssueIds;
    }

    /**
     * Array of security state issues ids.
     */
    public void setSecurityStateIssueIds(List<String> securityStateIssueIds) {
        this.securityStateIssueIds = securityStateIssueIds;
    }

    public String toString() {
        return "VisibleSecurityState [securityState=" + securityState + ", certificateSecurityState="
                + certificateSecurityState + ", safetyTipInfo=" + safetyTipInfo + ", securityStateIssueIds="
                + securityStateIssueIds + "]";
    }
}
