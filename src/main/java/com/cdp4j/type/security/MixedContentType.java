// SPDX-License-Identifier: MIT
package com.cdp4j.type.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * A description of mixed content (HTTP resources on HTTPS pages), as defined by
 * https://www.w3.org/TR/mixed-content/#categories
 */
public enum MixedContentType {
    @SerializedName("blockable")
    @JsonProperty("blockable")
    Blockable("blockable"),

    @SerializedName("optionally-blockable")
    @JsonProperty("optionally-blockable")
    OptionallyBlockable("optionally-blockable"),

    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    MixedContentType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
