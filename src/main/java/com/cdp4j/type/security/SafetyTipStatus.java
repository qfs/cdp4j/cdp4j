// SPDX-License-Identifier: MIT
package com.cdp4j.type.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum SafetyTipStatus {
    @SerializedName("badReputation")
    @JsonProperty("badReputation")
    BadReputation("badReputation"),

    @SerializedName("lookalike")
    @JsonProperty("lookalike")
    Lookalike("lookalike"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SafetyTipStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
