// SPDX-License-Identifier: MIT
package com.cdp4j.type.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The action to take when a certificate error occurs. continue will continue processing the
 * request and cancel will cancel the request.
 */
public enum CertificateErrorAction {
    @SerializedName("continue")
    @JsonProperty("continue")
    Continue("continue"),

    @SerializedName("cancel")
    @JsonProperty("cancel")
    Cancel("cancel"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CertificateErrorAction(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
