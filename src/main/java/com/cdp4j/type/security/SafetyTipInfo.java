// SPDX-License-Identifier: MIT
package com.cdp4j.type.security;

import com.cdp4j.annotation.Experimental;

@Experimental
public class SafetyTipInfo {
    private SafetyTipStatus safetyTipStatus;

    private String safeUrl;

    /**
     * Describes whether the page triggers any safety tips or reputation warnings.
     * Default is unknown.
     */
    public SafetyTipStatus getSafetyTipStatus() {
        return safetyTipStatus;
    }

    /**
     * Describes whether the page triggers any safety tips or reputation warnings.
     * Default is unknown.
     */
    public void setSafetyTipStatus(SafetyTipStatus safetyTipStatus) {
        this.safetyTipStatus = safetyTipStatus;
    }

    /**
     * The URL the safety tip suggested ("Did you mean?"). Only filled in for
     * lookalike matches.
     */
    public String getSafeUrl() {
        return safeUrl;
    }

    /**
     * The URL the safety tip suggested ("Did you mean?"). Only filled in for
     * lookalike matches.
     */
    public void setSafeUrl(String safeUrl) {
        this.safeUrl = safeUrl;
    }

    public String toString() {
        return "SafetyTipInfo [safetyTipStatus=" + safetyTipStatus + ", safeUrl=" + safeUrl + "]";
    }
}
