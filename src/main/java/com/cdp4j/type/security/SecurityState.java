// SPDX-License-Identifier: MIT
package com.cdp4j.type.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The security level of a page or resource.
 */
public enum SecurityState {
    @SerializedName("unknown")
    @JsonProperty("unknown")
    Unknown("unknown"),

    @SerializedName("neutral")
    @JsonProperty("neutral")
    Neutral("neutral"),

    @SerializedName("insecure")
    @JsonProperty("insecure")
    Insecure("insecure"),

    @SerializedName("secure")
    @JsonProperty("secure")
    Secure("secure"),

    @SerializedName("info")
    @JsonProperty("info")
    Info("info"),

    @SerializedName("insecure-broken")
    @JsonProperty("insecure-broken")
    InsecureBroken("insecure-broken"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SecurityState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
