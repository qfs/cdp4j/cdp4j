// SPDX-License-Identifier: MIT
package com.cdp4j.type.webaudio;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of AudioContextState from the spec
 */
public enum ContextState {
    @SerializedName("suspended")
    @JsonProperty("suspended")
    Suspended("suspended"),

    @SerializedName("running")
    @JsonProperty("running")
    Running("running"),

    @SerializedName("closed")
    @JsonProperty("closed")
    Closed("closed"),

    @SerializedName("interrupted")
    @JsonProperty("interrupted")
    Interrupted("interrupted"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ContextState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
