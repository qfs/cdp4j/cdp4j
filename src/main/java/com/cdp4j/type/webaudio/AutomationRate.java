// SPDX-License-Identifier: MIT
package com.cdp4j.type.webaudio;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of AudioParam::AutomationRate from the spec
 */
public enum AutomationRate {
    @SerializedName("a-rate")
    @JsonProperty("a-rate")
    ARate("a-rate"),

    @SerializedName("k-rate")
    @JsonProperty("k-rate")
    KRate("k-rate"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AutomationRate(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
