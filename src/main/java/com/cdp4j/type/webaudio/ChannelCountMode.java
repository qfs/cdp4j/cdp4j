// SPDX-License-Identifier: MIT
package com.cdp4j.type.webaudio;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of AudioNode::ChannelCountMode from the spec
 */
public enum ChannelCountMode {
    @SerializedName("clamped-max")
    @JsonProperty("clamped-max")
    ClampedMax("clamped-max"),

    @SerializedName("explicit")
    @JsonProperty("explicit")
    Explicit("explicit"),

    @SerializedName("max")
    @JsonProperty("max")
    Max("max"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ChannelCountMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
