// SPDX-License-Identifier: MIT
package com.cdp4j.type.webaudio;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of AudioNode::ChannelInterpretation from the spec
 */
public enum ChannelInterpretation {
    @SerializedName("discrete")
    @JsonProperty("discrete")
    Discrete("discrete"),

    @SerializedName("speakers")
    @JsonProperty("speakers")
    Speakers("speakers"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ChannelInterpretation(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
