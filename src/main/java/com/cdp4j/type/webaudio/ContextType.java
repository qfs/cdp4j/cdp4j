// SPDX-License-Identifier: MIT
package com.cdp4j.type.webaudio;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of BaseAudioContext types
 */
public enum ContextType {
    @SerializedName("realtime")
    @JsonProperty("realtime")
    Realtime("realtime"),

    @SerializedName("offline")
    @JsonProperty("offline")
    Offline("offline"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ContextType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
