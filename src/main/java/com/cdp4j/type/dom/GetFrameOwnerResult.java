// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

public class GetFrameOwnerResult {
    private Integer backendNodeId;

    private Integer nodeId;

    /**
     * Resulting node.
     */
    public Integer getBackendNodeId() {
        return backendNodeId;
    }

    /**
     * Resulting node.
     */
    public void setBackendNodeId(Integer backendNodeId) {
        this.backendNodeId = backendNodeId;
    }

    /**
     * Id of the node at given coordinates, only when enabled and requested
     * document.
     */
    public Integer getNodeId() {
        return nodeId;
    }

    /**
     * Id of the node at given coordinates, only when enabled and requested
     * document.
     */
    public void setNodeId(Integer nodeId) {
        this.nodeId = nodeId;
    }

    public String toString() {
        return "GetFrameOwnerResult [backendNodeId=" + backendNodeId + ", nodeId=" + nodeId + "]";
    }
}
