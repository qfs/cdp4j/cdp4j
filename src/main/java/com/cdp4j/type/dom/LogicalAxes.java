// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * ContainerSelector logical axes
 */
public enum LogicalAxes {
    @SerializedName("Inline")
    @JsonProperty("Inline")
    Inline("Inline"),

    @SerializedName("Block")
    @JsonProperty("Block")
    Block("Block"),

    @SerializedName("Both")
    @JsonProperty("Both")
    Both("Both"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    LogicalAxes(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
