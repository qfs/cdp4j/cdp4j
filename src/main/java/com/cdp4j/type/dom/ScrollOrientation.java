// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Physical scroll orientation
 */
public enum ScrollOrientation {
    @SerializedName("horizontal")
    @JsonProperty("horizontal")
    Horizontal("horizontal"),

    @SerializedName("vertical")
    @JsonProperty("vertical")
    Vertical("vertical"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ScrollOrientation(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
