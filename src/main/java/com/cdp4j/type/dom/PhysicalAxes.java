// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * ContainerSelector physical axes
 */
public enum PhysicalAxes {
    @SerializedName("Horizontal")
    @JsonProperty("Horizontal")
    Horizontal("Horizontal"),

    @SerializedName("Vertical")
    @JsonProperty("Vertical")
    Vertical("Vertical"),

    @SerializedName("Both")
    @JsonProperty("Both")
    Both("Both"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PhysicalAxes(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
