// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Document compatibility mode.
 */
public enum CompatibilityMode {
    @SerializedName("QuirksMode")
    @JsonProperty("QuirksMode")
    QuirksMode("QuirksMode"),

    @SerializedName("LimitedQuirksMode")
    @JsonProperty("LimitedQuirksMode")
    LimitedQuirksMode("LimitedQuirksMode"),

    @SerializedName("NoQuirksMode")
    @JsonProperty("NoQuirksMode")
    NoQuirksMode("NoQuirksMode"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CompatibilityMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
