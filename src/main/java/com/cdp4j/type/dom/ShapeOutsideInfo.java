// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * CSS Shape Outside details.
 */
public class ShapeOutsideInfo {
    private List<Double> bounds = emptyList();

    private List<Object> shape = emptyList();

    private List<Object> marginShape = emptyList();

    /**
     * Shape bounds
     */
    public List<Double> getBounds() {
        return bounds;
    }

    /**
     * Shape bounds
     */
    public void setBounds(List<Double> bounds) {
        this.bounds = bounds;
    }

    /**
     * Shape coordinate details
     */
    public List<Object> getShape() {
        return shape;
    }

    /**
     * Shape coordinate details
     */
    public void setShape(List<Object> shape) {
        this.shape = shape;
    }

    /**
     * Margin shape bounds
     */
    public List<Object> getMarginShape() {
        return marginShape;
    }

    /**
     * Margin shape bounds
     */
    public void setMarginShape(List<Object> marginShape) {
        this.marginShape = marginShape;
    }

    public String toString() {
        return "ShapeOutsideInfo [bounds=" + bounds + ", shape=" + shape + ", marginShape=" + marginShape + "]";
    }
}
