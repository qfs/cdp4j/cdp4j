// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Shadow root type.
 */
public enum ShadowRootType {
    @SerializedName("user-agent")
    @JsonProperty("user-agent")
    UserAgent("user-agent"),

    @SerializedName("open")
    @JsonProperty("open")
    Open("open"),

    @SerializedName("closed")
    @JsonProperty("closed")
    Closed("closed"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ShadowRootType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
