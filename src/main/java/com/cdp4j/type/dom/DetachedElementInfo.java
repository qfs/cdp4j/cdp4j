// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * A structure to hold the top-level node of a detached tree and an array of its
 * retained descendants.
 */
public class DetachedElementInfo {
    private Node treeNode;

    private List<Integer> retainedNodeIds = emptyList();

    public Node getTreeNode() {
        return treeNode;
    }

    public void setTreeNode(Node treeNode) {
        this.treeNode = treeNode;
    }

    public List<Integer> getRetainedNodeIds() {
        return retainedNodeIds;
    }

    public void setRetainedNodeIds(List<Integer> retainedNodeIds) {
        this.retainedNodeIds = retainedNodeIds;
    }

    public String toString() {
        return "DetachedElementInfo [treeNode=" + treeNode + ", retainedNodeIds=" + retainedNodeIds + "]";
    }
}
