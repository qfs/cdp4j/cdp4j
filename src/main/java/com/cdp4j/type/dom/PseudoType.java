// SPDX-License-Identifier: MIT
package com.cdp4j.type.dom;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Pseudo element type.
 */
public enum PseudoType {
    @SerializedName("first-line")
    @JsonProperty("first-line")
    FirstLine("first-line"),

    @SerializedName("first-letter")
    @JsonProperty("first-letter")
    FirstLetter("first-letter"),

    @SerializedName("checkmark")
    @JsonProperty("checkmark")
    Checkmark("checkmark"),

    @SerializedName("before")
    @JsonProperty("before")
    Before("before"),

    @SerializedName("after")
    @JsonProperty("after")
    After("after"),

    @SerializedName("picker-icon")
    @JsonProperty("picker-icon")
    PickerIcon("picker-icon"),

    @SerializedName("marker")
    @JsonProperty("marker")
    Marker("marker"),

    @SerializedName("backdrop")
    @JsonProperty("backdrop")
    Backdrop("backdrop"),

    @SerializedName("column")
    @JsonProperty("column")
    Column("column"),

    @SerializedName("selection")
    @JsonProperty("selection")
    Selection("selection"),

    @SerializedName("search-text")
    @JsonProperty("search-text")
    SearchText("search-text"),

    @SerializedName("target-text")
    @JsonProperty("target-text")
    TargetText("target-text"),

    @SerializedName("spelling-error")
    @JsonProperty("spelling-error")
    SpellingError("spelling-error"),

    @SerializedName("grammar-error")
    @JsonProperty("grammar-error")
    GrammarError("grammar-error"),

    @SerializedName("highlight")
    @JsonProperty("highlight")
    Highlight("highlight"),

    @SerializedName("first-line-inherited")
    @JsonProperty("first-line-inherited")
    FirstLineInherited("first-line-inherited"),

    @SerializedName("scroll-marker")
    @JsonProperty("scroll-marker")
    ScrollMarker("scroll-marker"),

    @SerializedName("scroll-marker-group")
    @JsonProperty("scroll-marker-group")
    ScrollMarkerGroup("scroll-marker-group"),

    @SerializedName("scroll-button")
    @JsonProperty("scroll-button")
    ScrollButton("scroll-button"),

    @SerializedName("scrollbar")
    @JsonProperty("scrollbar")
    Scrollbar("scrollbar"),

    @SerializedName("scrollbar-thumb")
    @JsonProperty("scrollbar-thumb")
    ScrollbarThumb("scrollbar-thumb"),

    @SerializedName("scrollbar-button")
    @JsonProperty("scrollbar-button")
    ScrollbarButton("scrollbar-button"),

    @SerializedName("scrollbar-track")
    @JsonProperty("scrollbar-track")
    ScrollbarTrack("scrollbar-track"),

    @SerializedName("scrollbar-track-piece")
    @JsonProperty("scrollbar-track-piece")
    ScrollbarTrackPiece("scrollbar-track-piece"),

    @SerializedName("scrollbar-corner")
    @JsonProperty("scrollbar-corner")
    ScrollbarCorner("scrollbar-corner"),

    @SerializedName("resizer")
    @JsonProperty("resizer")
    Resizer("resizer"),

    @SerializedName("input-list-button")
    @JsonProperty("input-list-button")
    InputListButton("input-list-button"),

    @SerializedName("view-transition")
    @JsonProperty("view-transition")
    ViewTransition("view-transition"),

    @SerializedName("view-transition-group")
    @JsonProperty("view-transition-group")
    ViewTransitionGroup("view-transition-group"),

    @SerializedName("view-transition-image-pair")
    @JsonProperty("view-transition-image-pair")
    ViewTransitionImagePair("view-transition-image-pair"),

    @SerializedName("view-transition-old")
    @JsonProperty("view-transition-old")
    ViewTransitionOld("view-transition-old"),

    @SerializedName("view-transition-new")
    @JsonProperty("view-transition-new")
    ViewTransitionNew("view-transition-new"),

    @SerializedName("placeholder")
    @JsonProperty("placeholder")
    Placeholder("placeholder"),

    @SerializedName("file-selector-button")
    @JsonProperty("file-selector-button")
    FileSelectorButton("file-selector-button"),

    @SerializedName("details-content")
    @JsonProperty("details-content")
    DetailsContent("details-content"),

    @SerializedName("picker")
    @JsonProperty("picker")
    Picker("picker"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    PseudoType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
