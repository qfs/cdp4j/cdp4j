// SPDX-License-Identifier: MIT
package com.cdp4j.type.target;

import com.cdp4j.annotation.Experimental;

/**
 * A filter used by target query/discovery/auto-attach operations.
 */
@Experimental
public class FilterEntry {
    private Boolean exclude;

    private String type;

    /**
     * If set, causes exclusion of matching targets from the list.
     */
    public Boolean isExclude() {
        return exclude;
    }

    /**
     * If set, causes exclusion of matching targets from the list.
     */
    public void setExclude(Boolean exclude) {
        this.exclude = exclude;
    }

    /**
     * If not present, matches any type.
     */
    public String getType() {
        return type;
    }

    /**
     * If not present, matches any type.
     */
    public void setType(String type) {
        this.type = type;
    }

    public String toString() {
        return "FilterEntry [exclude=" + exclude + ", type=" + type + "]";
    }
}
