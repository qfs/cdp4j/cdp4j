// SPDX-License-Identifier: MIT
package com.cdp4j.type.target;

import com.cdp4j.annotation.Experimental;

@Experimental
public class RemoteLocation {
    private String host;

    private Integer port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String toString() {
        return "RemoteLocation [host=" + host + ", port=" + port + "]";
    }
}
