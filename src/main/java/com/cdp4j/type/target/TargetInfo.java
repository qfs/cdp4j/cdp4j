// SPDX-License-Identifier: MIT
package com.cdp4j.type.target;

public class TargetInfo {
    private String targetId;

    private String type;

    private String title;

    private String url;

    private Boolean attached;

    private String openerId;

    private Boolean canAccessOpener;

    private String openerFrameId;

    private String browserContextId;

    private String subtype;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    /**
     * List of types:
     * https://source.chromium.org/chromium/chromium/src/+/main:content/browser/devtools/devtools_agent_host_impl.cc?ss=chromium&q=f:devtools%20-f:out%20%22::kTypeTab%5B%5D%22
     */
    public String getType() {
        return type;
    }

    /**
     * List of types:
     * https://source.chromium.org/chromium/chromium/src/+/main:content/browser/devtools/devtools_agent_host_impl.cc?ss=chromium&q=f:devtools%20-f:out%20%22::kTypeTab%5B%5D%22
     */
    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Whether the target has an attached client.
     */
    public Boolean isAttached() {
        return attached;
    }

    /**
     * Whether the target has an attached client.
     */
    public void setAttached(Boolean attached) {
        this.attached = attached;
    }

    /**
     * Opener target Id
     */
    public String getOpenerId() {
        return openerId;
    }

    /**
     * Opener target Id
     */
    public void setOpenerId(String openerId) {
        this.openerId = openerId;
    }

    /**
     * Whether the target has access to the originating window.
     */
    public Boolean isCanAccessOpener() {
        return canAccessOpener;
    }

    /**
     * Whether the target has access to the originating window.
     */
    public void setCanAccessOpener(Boolean canAccessOpener) {
        this.canAccessOpener = canAccessOpener;
    }

    /**
     * Frame id of originating window (is only set if target has an opener).
     */
    public String getOpenerFrameId() {
        return openerFrameId;
    }

    /**
     * Frame id of originating window (is only set if target has an opener).
     */
    public void setOpenerFrameId(String openerFrameId) {
        this.openerFrameId = openerFrameId;
    }

    public String getBrowserContextId() {
        return browserContextId;
    }

    public void setBrowserContextId(String browserContextId) {
        this.browserContextId = browserContextId;
    }

    /**
     * Provides additional details for specific target types. For example, for the
     * type of "page", this may be set to "prerender".
     */
    public String getSubtype() {
        return subtype;
    }

    /**
     * Provides additional details for specific target types. For example, for the
     * type of "page", this may be set to "prerender".
     */
    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String toString() {
        return "TargetInfo [targetId=" + targetId + ", type=" + type + ", title=" + title + ", url=" + url
                + ", attached=" + attached + ", openerId=" + openerId + ", canAccessOpener=" + canAccessOpener
                + ", openerFrameId=" + openerFrameId + ", browserContextId=" + browserContextId + ", subtype=" + subtype
                + "]";
    }
}
