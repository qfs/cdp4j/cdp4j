// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Orientation of a display feature in relation to screen
 */
public enum DisplayOrientation {
    @SerializedName("vertical")
    @JsonProperty("vertical")
    Vertical("vertical"),

    @SerializedName("horizontal")
    @JsonProperty("horizontal")
    Horizontal("horizontal");

    public final String value;

    DisplayOrientation(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
