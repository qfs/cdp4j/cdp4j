// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum Encoding {
    @SerializedName("webp")
    @JsonProperty("webp")
    Webp("webp"),

    @SerializedName("jpeg")
    @JsonProperty("jpeg")
    Jpeg("jpeg"),

    @SerializedName("png")
    @JsonProperty("png")
    Png("png");

    public final String value;

    Encoding(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
