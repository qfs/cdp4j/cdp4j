// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum RestartFrameMode {
    @SerializedName("StepInto")
    @JsonProperty("StepInto")
    StepInto("StepInto");

    public final String value;

    RestartFrameMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
