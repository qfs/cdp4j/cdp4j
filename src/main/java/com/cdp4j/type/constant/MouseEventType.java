// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum MouseEventType {
    @SerializedName("mousePressed")
    @JsonProperty("mousePressed")
    MousePressed("mousePressed"),

    @SerializedName("mouseReleased")
    @JsonProperty("mouseReleased")
    MouseReleased("mouseReleased"),

    @SerializedName("mouseMoved")
    @JsonProperty("mouseMoved")
    MouseMoved("mouseMoved"),

    @SerializedName("mouseWheel")
    @JsonProperty("mouseWheel")
    MouseWheel("mouseWheel");

    public final String value;

    MouseEventType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
