// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum TimeDomain {
    @SerializedName("timeTicks")
    @JsonProperty("timeTicks")
    TimeTicks("timeTicks"),

    @SerializedName("threadTicks")
    @JsonProperty("threadTicks")
    ThreadTicks("threadTicks");

    public final String value;

    TimeDomain(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
