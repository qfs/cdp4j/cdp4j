// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ConsoleApiCallType {
    @SerializedName("log")
    @JsonProperty("log")
    Log("log"),

    @SerializedName("debug")
    @JsonProperty("debug")
    Debug("debug"),

    @SerializedName("info")
    @JsonProperty("info")
    Info("info"),

    @SerializedName("error")
    @JsonProperty("error")
    Error("error"),

    @SerializedName("warning")
    @JsonProperty("warning")
    Warning("warning"),

    @SerializedName("dir")
    @JsonProperty("dir")
    Dir("dir"),

    @SerializedName("dirxml")
    @JsonProperty("dirxml")
    Dirxml("dirxml"),

    @SerializedName("table")
    @JsonProperty("table")
    Table("table"),

    @SerializedName("trace")
    @JsonProperty("trace")
    Trace("trace"),

    @SerializedName("clear")
    @JsonProperty("clear")
    Clear("clear"),

    @SerializedName("startGroup")
    @JsonProperty("startGroup")
    StartGroup("startGroup"),

    @SerializedName("startGroupCollapsed")
    @JsonProperty("startGroupCollapsed")
    StartGroupCollapsed("startGroupCollapsed"),

    @SerializedName("endGroup")
    @JsonProperty("endGroup")
    EndGroup("endGroup"),

    @SerializedName("assert")
    @JsonProperty("assert")
    Assert("assert"),

    @SerializedName("profile")
    @JsonProperty("profile")
    Profile("profile"),

    @SerializedName("profileEnd")
    @JsonProperty("profileEnd")
    ProfileEnd("profileEnd"),

    @SerializedName("count")
    @JsonProperty("count")
    Count("count"),

    @SerializedName("timeEnd")
    @JsonProperty("timeEnd")
    TimeEnd("timeEnd");

    public final String value;

    ConsoleApiCallType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
