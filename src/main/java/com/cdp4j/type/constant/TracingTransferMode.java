// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum TracingTransferMode {
    @SerializedName("ReportEvents")
    @JsonProperty("ReportEvents")
    ReportEvents("ReportEvents"),

    @SerializedName("ReturnAsStream")
    @JsonProperty("ReturnAsStream")
    ReturnAsStream("ReturnAsStream");

    public final String value;

    TracingTransferMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
