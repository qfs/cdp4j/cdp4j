// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Key path type.
 */
public enum KeyPathType {
    @SerializedName("null")
    @JsonProperty("null")
    Null("null"),

    @SerializedName("string")
    @JsonProperty("string")
    String("string"),

    @SerializedName("array")
    @JsonProperty("array")
    Array("array");

    public final String value;

    KeyPathType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
