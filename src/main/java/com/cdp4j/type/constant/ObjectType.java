// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Object type.
 */
public enum ObjectType {
    @SerializedName("object")
    @JsonProperty("object")
    Object("object"),

    @SerializedName("function")
    @JsonProperty("function")
    Function("function"),

    @SerializedName("undefined")
    @JsonProperty("undefined")
    Undefined("undefined"),

    @SerializedName("string")
    @JsonProperty("string")
    String("string"),

    @SerializedName("number")
    @JsonProperty("number")
    Number("number"),

    @SerializedName("boolean")
    @JsonProperty("boolean")
    Boolean("boolean"),

    @SerializedName("symbol")
    @JsonProperty("symbol")
    Symbol("symbol"),

    @SerializedName("bigint")
    @JsonProperty("bigint")
    Bigint("bigint");

    public final String value;

    ObjectType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
