// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum Platform {
    @SerializedName("mobile")
    @JsonProperty("mobile")
    Mobile("mobile"),

    @SerializedName("desktop")
    @JsonProperty("desktop")
    Desktop("desktop");

    public final String value;

    Platform(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
