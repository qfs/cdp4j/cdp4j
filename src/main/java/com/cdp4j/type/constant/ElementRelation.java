// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum ElementRelation {
    @SerializedName("PopoverTarget")
    @JsonProperty("PopoverTarget")
    PopoverTarget("PopoverTarget");

    public final String value;

    ElementRelation(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
