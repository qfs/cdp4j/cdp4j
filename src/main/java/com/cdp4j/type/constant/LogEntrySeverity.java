// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Log entry severity.
 */
public enum LogEntrySeverity {
    @SerializedName("verbose")
    @JsonProperty("verbose")
    Verbose("verbose"),

    @SerializedName("info")
    @JsonProperty("info")
    Info("info"),

    @SerializedName("warning")
    @JsonProperty("warning")
    Warning("warning"),

    @SerializedName("error")
    @JsonProperty("error")
    Error("error");

    public final String value;

    LogEntrySeverity(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
