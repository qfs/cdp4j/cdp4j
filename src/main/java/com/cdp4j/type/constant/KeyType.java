// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Key type.
 */
public enum KeyType {
    @SerializedName("number")
    @JsonProperty("number")
    Number("number"),

    @SerializedName("string")
    @JsonProperty("string")
    String("string"),

    @SerializedName("date")
    @JsonProperty("date")
    Date("date"),

    @SerializedName("array")
    @JsonProperty("array")
    Array("array");

    public final String value;

    KeyType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
