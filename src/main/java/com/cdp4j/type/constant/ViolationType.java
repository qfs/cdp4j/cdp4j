// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Violation type.
 */
public enum ViolationType {
    @SerializedName("longTask")
    @JsonProperty("longTask")
    LongTask("longTask"),

    @SerializedName("longLayout")
    @JsonProperty("longLayout")
    LongLayout("longLayout"),

    @SerializedName("blockedEvent")
    @JsonProperty("blockedEvent")
    BlockedEvent("blockedEvent"),

    @SerializedName("blockedParser")
    @JsonProperty("blockedParser")
    BlockedParser("blockedParser"),

    @SerializedName("discouragedAPIUse")
    @JsonProperty("discouragedAPIUse")
    DiscouragedAPIUse("discouragedAPIUse"),

    @SerializedName("handler")
    @JsonProperty("handler")
    Handler("handler"),

    @SerializedName("recurringHandler")
    @JsonProperty("recurringHandler")
    RecurringHandler("recurringHandler");

    public final String value;

    ViolationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
