// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Log entry source.
 */
public enum LogEntrySource {
    @SerializedName("xml")
    @JsonProperty("xml")
    Xml("xml"),

    @SerializedName("javascript")
    @JsonProperty("javascript")
    Javascript("javascript"),

    @SerializedName("network")
    @JsonProperty("network")
    Network("network"),

    @SerializedName("storage")
    @JsonProperty("storage")
    Storage("storage"),

    @SerializedName("appcache")
    @JsonProperty("appcache")
    Appcache("appcache"),

    @SerializedName("rendering")
    @JsonProperty("rendering")
    Rendering("rendering"),

    @SerializedName("security")
    @JsonProperty("security")
    Security("security"),

    @SerializedName("deprecation")
    @JsonProperty("deprecation")
    Deprecation("deprecation"),

    @SerializedName("worker")
    @JsonProperty("worker")
    Worker("worker"),

    @SerializedName("violation")
    @JsonProperty("violation")
    Violation("violation"),

    @SerializedName("intervention")
    @JsonProperty("intervention")
    Intervention("intervention"),

    @SerializedName("recommendation")
    @JsonProperty("recommendation")
    Recommendation("recommendation"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other");

    public final String value;

    LogEntrySource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
