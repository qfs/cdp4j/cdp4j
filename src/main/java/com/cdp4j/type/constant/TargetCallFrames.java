// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum TargetCallFrames {
    @SerializedName("any")
    @JsonProperty("any")
    Any("any"),

    @SerializedName("current")
    @JsonProperty("current")
    Current("current");

    public final String value;

    TargetCallFrames(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
