// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum InstrumentationName {
    @SerializedName("beforeScriptExecution")
    @JsonProperty("beforeScriptExecution")
    BeforeScriptExecution("beforeScriptExecution"),

    @SerializedName("beforeScriptWithSourceMapExecution")
    @JsonProperty("beforeScriptWithSourceMapExecution")
    BeforeScriptWithSourceMapExecution("beforeScriptWithSourceMapExecution");

    public final String value;

    InstrumentationName(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
