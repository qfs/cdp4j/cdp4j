// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum KeyEventType {
    @SerializedName("keyDown")
    @JsonProperty("keyDown")
    KeyDown("keyDown"),

    @SerializedName("keyUp")
    @JsonProperty("keyUp")
    KeyUp("keyUp"),

    @SerializedName("rawKeyDown")
    @JsonProperty("rawKeyDown")
    RawKeyDown("rawKeyDown"),

    @SerializedName("char")
    @JsonProperty("char")
    Char("char");

    public final String value;

    KeyEventType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
