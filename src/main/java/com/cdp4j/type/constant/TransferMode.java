// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum TransferMode {
    @SerializedName("ReturnAsBase64")
    @JsonProperty("ReturnAsBase64")
    ReturnAsBase64("ReturnAsBase64"),

    @SerializedName("ReturnAsStream")
    @JsonProperty("ReturnAsStream")
    ReturnAsStream("ReturnAsStream");

    public final String value;

    TransferMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
