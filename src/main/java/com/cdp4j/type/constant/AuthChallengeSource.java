// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Source of the authentication challenge.
 */
public enum AuthChallengeSource {
    @SerializedName("Server")
    @JsonProperty("Server")
    Server("Server"),

    @SerializedName("Proxy")
    @JsonProperty("Proxy")
    Proxy("Proxy");

    public final String value;

    AuthChallengeSource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
