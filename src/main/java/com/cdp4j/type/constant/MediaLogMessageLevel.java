// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Keep in sync with MediaLogMessageLevel
 * We are currently keeping the message level 'error' separate from the
 * PlayerError type because right now they represent different things,
 * this one being a DVLOG(ERROR) style log message that gets printed
 * based on what log level is selected in the UI, and the other is a
 * representation of a media::PipelineStatus object. Soon however we're
 * going to be moving away from using PipelineStatus for errors and
 * introducing a new error type which should hopefully let us integrate
 * the error log level into the PlayerError type.
 */
public enum MediaLogMessageLevel {
    @SerializedName("error")
    @JsonProperty("error")
    Error("error"),

    @SerializedName("warning")
    @JsonProperty("warning")
    Warning("warning"),

    @SerializedName("info")
    @JsonProperty("info")
    Info("info"),

    @SerializedName("debug")
    @JsonProperty("debug")
    Debug("debug");

    public final String value;

    MediaLogMessageLevel(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
