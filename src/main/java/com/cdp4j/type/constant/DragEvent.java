// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum DragEvent {
    @SerializedName("dragEnter")
    @JsonProperty("dragEnter")
    DragEnter("dragEnter"),

    @SerializedName("dragOver")
    @JsonProperty("dragOver")
    DragOver("dragOver"),

    @SerializedName("drop")
    @JsonProperty("drop")
    Drop("drop"),

    @SerializedName("dragCancel")
    @JsonProperty("dragCancel")
    DragCancel("dragCancel");

    public final String value;

    DragEvent(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
