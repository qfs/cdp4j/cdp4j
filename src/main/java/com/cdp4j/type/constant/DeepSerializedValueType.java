// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum DeepSerializedValueType {
    @SerializedName("undefined")
    @JsonProperty("undefined")
    Undefined("undefined"),

    @SerializedName("null")
    @JsonProperty("null")
    Null("null"),

    @SerializedName("string")
    @JsonProperty("string")
    String("string"),

    @SerializedName("number")
    @JsonProperty("number")
    Number("number"),

    @SerializedName("boolean")
    @JsonProperty("boolean")
    Boolean("boolean"),

    @SerializedName("bigint")
    @JsonProperty("bigint")
    Bigint("bigint"),

    @SerializedName("regexp")
    @JsonProperty("regexp")
    Regexp("regexp"),

    @SerializedName("date")
    @JsonProperty("date")
    Date("date"),

    @SerializedName("symbol")
    @JsonProperty("symbol")
    Symbol("symbol"),

    @SerializedName("array")
    @JsonProperty("array")
    Array("array"),

    @SerializedName("object")
    @JsonProperty("object")
    Object("object"),

    @SerializedName("function")
    @JsonProperty("function")
    Function("function"),

    @SerializedName("map")
    @JsonProperty("map")
    Map("map"),

    @SerializedName("set")
    @JsonProperty("set")
    Set("set"),

    @SerializedName("weakmap")
    @JsonProperty("weakmap")
    Weakmap("weakmap"),

    @SerializedName("weakset")
    @JsonProperty("weakset")
    Weakset("weakset"),

    @SerializedName("error")
    @JsonProperty("error")
    Error("error"),

    @SerializedName("proxy")
    @JsonProperty("proxy")
    Proxy("proxy"),

    @SerializedName("promise")
    @JsonProperty("promise")
    Promise("promise"),

    @SerializedName("typedarray")
    @JsonProperty("typedarray")
    Typedarray("typedarray"),

    @SerializedName("arraybuffer")
    @JsonProperty("arraybuffer")
    Arraybuffer("arraybuffer"),

    @SerializedName("node")
    @JsonProperty("node")
    Node("node"),

    @SerializedName("window")
    @JsonProperty("window")
    Window("window"),

    @SerializedName("generator")
    @JsonProperty("generator")
    Generator("generator");

    public final String value;

    DeepSerializedValueType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
