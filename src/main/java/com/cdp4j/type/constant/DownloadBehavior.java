// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum DownloadBehavior {
    @SerializedName("deny")
    @JsonProperty("deny")
    Deny("deny"),

    @SerializedName("allow")
    @JsonProperty("allow")
    Allow("allow"),

    @SerializedName("default")
    @JsonProperty("default")
    Default("default"),

    @SerializedName("allowAndName")
    @JsonProperty("allowAndName")
    AllowAndName("allowAndName");

    public final String value;

    DownloadBehavior(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
