// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum NavigationWithinDocumentType {
    @SerializedName("fragment")
    @JsonProperty("fragment")
    Fragment("fragment"),

    @SerializedName("historyApi")
    @JsonProperty("historyApi")
    HistoryApi("historyApi"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other");

    public final String value;

    NavigationWithinDocumentType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
