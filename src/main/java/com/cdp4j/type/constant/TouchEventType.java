// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum TouchEventType {
    @SerializedName("touchStart")
    @JsonProperty("touchStart")
    TouchStart("touchStart"),

    @SerializedName("touchEnd")
    @JsonProperty("touchEnd")
    TouchEnd("touchEnd"),

    @SerializedName("touchMove")
    @JsonProperty("touchMove")
    TouchMove("touchMove"),

    @SerializedName("touchCancel")
    @JsonProperty("touchCancel")
    TouchCancel("touchCancel");

    public final String value;

    TouchEventType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
