// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Current posture of the device
 */
public enum DevicePostureType {
    @SerializedName("continuous")
    @JsonProperty("continuous")
    Continuous("continuous"),

    @SerializedName("folded")
    @JsonProperty("folded")
    Folded("folded");

    public final String value;

    DevicePostureType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
