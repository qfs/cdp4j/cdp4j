// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum IncludeWhitespace {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("all")
    @JsonProperty("all")
    All("all");

    public final String value;

    IncludeWhitespace(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
