// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PointerType {
    @SerializedName("mouse")
    @JsonProperty("mouse")
    Mouse("mouse"),

    @SerializedName("pen")
    @JsonProperty("pen")
    Pen("pen");

    public final String value;

    PointerType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
