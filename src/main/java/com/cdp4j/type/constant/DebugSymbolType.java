// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Type of the debug symbols.
 */
public enum DebugSymbolType {
    @SerializedName("SourceMap")
    @JsonProperty("SourceMap")
    SourceMap("SourceMap"),

    @SerializedName("EmbeddedDWARF")
    @JsonProperty("EmbeddedDWARF")
    EmbeddedDWARF("EmbeddedDWARF"),

    @SerializedName("ExternalDWARF")
    @JsonProperty("ExternalDWARF")
    ExternalDWARF("ExternalDWARF");

    public final String value;

    DebugSymbolType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
