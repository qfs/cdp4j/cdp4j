// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The decision on what to do in response to the authorization challenge.  Default means
 * deferring to the default behavior of the net stack, which will likely either the Cancel
 * authentication or display a popup dialog box.
 */
public enum AuthResponse {
    @SerializedName("Default")
    @JsonProperty("Default")
    Default("Default"),

    @SerializedName("CancelAuth")
    @JsonProperty("CancelAuth")
    CancelAuth("CancelAuth"),

    @SerializedName("ProvideCredentials")
    @JsonProperty("ProvideCredentials")
    ProvideCredentials("ProvideCredentials");

    public final String value;

    AuthResponse(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
