// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Reason for rectangle to force scrolling on the main thread
 */
public enum RepaintReason {
    @SerializedName("RepaintsOnScroll")
    @JsonProperty("RepaintsOnScroll")
    RepaintsOnScroll("RepaintsOnScroll"),

    @SerializedName("TouchEventHandler")
    @JsonProperty("TouchEventHandler")
    TouchEventHandler("TouchEventHandler"),

    @SerializedName("WheelEventHandler")
    @JsonProperty("WheelEventHandler")
    WheelEventHandler("WheelEventHandler");

    public final String value;

    RepaintReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
