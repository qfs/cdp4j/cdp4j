// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Orientation type.
 */
public enum PortraitType {
    @SerializedName("portraitPrimary")
    @JsonProperty("portraitPrimary")
    PortraitPrimary("portraitPrimary"),

    @SerializedName("portraitSecondary")
    @JsonProperty("portraitSecondary")
    PortraitSecondary("portraitSecondary"),

    @SerializedName("landscapePrimary")
    @JsonProperty("landscapePrimary")
    LandscapePrimary("landscapePrimary"),

    @SerializedName("landscapeSecondary")
    @JsonProperty("landscapeSecondary")
    LandscapeSecondary("landscapeSecondary");

    public final String value;

    PortraitType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
