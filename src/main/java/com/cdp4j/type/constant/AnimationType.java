// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Animation type of Animation.
 */
public enum AnimationType {
    @SerializedName("CSSTransition")
    @JsonProperty("CSSTransition")
    CSSTransition("CSSTransition"),

    @SerializedName("CSSAnimation")
    @JsonProperty("CSSAnimation")
    CSSAnimation("CSSAnimation"),

    @SerializedName("WebAnimation")
    @JsonProperty("WebAnimation")
    WebAnimation("WebAnimation");

    public final String value;

    AnimationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
