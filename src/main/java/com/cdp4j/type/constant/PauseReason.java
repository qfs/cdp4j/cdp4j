// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PauseReason {
    @SerializedName("ambiguous")
    @JsonProperty("ambiguous")
    Ambiguous("ambiguous"),

    @SerializedName("assert")
    @JsonProperty("assert")
    Assert("assert"),

    @SerializedName("CSPViolation")
    @JsonProperty("CSPViolation")
    CSPViolation("CSPViolation"),

    @SerializedName("debugCommand")
    @JsonProperty("debugCommand")
    DebugCommand("debugCommand"),

    @SerializedName("DOM")
    @JsonProperty("DOM")
    DOM("DOM"),

    @SerializedName("EventListener")
    @JsonProperty("EventListener")
    EventListener("EventListener"),

    @SerializedName("exception")
    @JsonProperty("exception")
    Exception("exception"),

    @SerializedName("instrumentation")
    @JsonProperty("instrumentation")
    Instrumentation("instrumentation"),

    @SerializedName("OOM")
    @JsonProperty("OOM")
    OOM("OOM"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    @SerializedName("promiseRejection")
    @JsonProperty("promiseRejection")
    PromiseRejection("promiseRejection"),

    @SerializedName("XHR")
    @JsonProperty("XHR")
    XHR("XHR"),

    @SerializedName("step")
    @JsonProperty("step")
    Step("step");

    public final String value;

    PauseReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
