// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Source of the media query: "mediaRule" if specified by a @media rule, "importRule" if
 * specified by an @import rule, "linkedSheet" if specified by a "media" attribute in a linked
 * stylesheet's LINK tag, "inlineSheet" if specified by a "media" attribute in an inline
 * stylesheet's STYLE tag.
 */
public enum CSSMediaSource {
    @SerializedName("mediaRule")
    @JsonProperty("mediaRule")
    MediaRule("mediaRule"),

    @SerializedName("importRule")
    @JsonProperty("importRule")
    ImportRule("importRule"),

    @SerializedName("linkedSheet")
    @JsonProperty("linkedSheet")
    LinkedSheet("linkedSheet"),

    @SerializedName("inlineSheet")
    @JsonProperty("inlineSheet")
    InlineSheet("inlineSheet");

    public final String value;

    CSSMediaSource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
