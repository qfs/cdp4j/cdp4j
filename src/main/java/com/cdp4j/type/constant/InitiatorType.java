// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Type of this initiator.
 */
public enum InitiatorType {
    @SerializedName("parser")
    @JsonProperty("parser")
    Parser("parser"),

    @SerializedName("script")
    @JsonProperty("script")
    Script("script"),

    @SerializedName("preload")
    @JsonProperty("preload")
    Preload("preload"),

    @SerializedName("SignedExchange")
    @JsonProperty("SignedExchange")
    SignedExchange("SignedExchange"),

    @SerializedName("preflight")
    @JsonProperty("preflight")
    Preflight("preflight"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other");

    public final String value;

    InitiatorType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
