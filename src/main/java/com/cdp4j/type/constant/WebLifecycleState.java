// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum WebLifecycleState {
    @SerializedName("frozen")
    @JsonProperty("frozen")
    Frozen("frozen"),

    @SerializedName("active")
    @JsonProperty("active")
    Active("active");

    public final String value;

    WebLifecycleState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
