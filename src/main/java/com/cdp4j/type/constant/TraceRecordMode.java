// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Controls how the trace buffer stores data.
 */
public enum TraceRecordMode {
    @SerializedName("recordUntilFull")
    @JsonProperty("recordUntilFull")
    RecordUntilFull("recordUntilFull"),

    @SerializedName("recordContinuously")
    @JsonProperty("recordContinuously")
    RecordContinuously("recordContinuously"),

    @SerializedName("recordAsMuchAsPossible")
    @JsonProperty("recordAsMuchAsPossible")
    RecordAsMuchAsPossible("recordAsMuchAsPossible"),

    @SerializedName("echoToConsole")
    @JsonProperty("echoToConsole")
    EchoToConsole("echoToConsole");

    public final String value;

    TraceRecordMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
