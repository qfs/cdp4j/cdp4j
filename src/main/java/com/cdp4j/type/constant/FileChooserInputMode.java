// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum FileChooserInputMode {
    @SerializedName("selectSingle")
    @JsonProperty("selectSingle")
    SelectSingle("selectSingle"),

    @SerializedName("selectMultiple")
    @JsonProperty("selectMultiple")
    SelectMultiple("selectMultiple");

    public final String value;

    FileChooserInputMode(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
