// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum SnapshotType {
    @SerializedName("mhtml")
    @JsonProperty("mhtml")
    Mhtml("mhtml");

    public final String value;

    SnapshotType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
