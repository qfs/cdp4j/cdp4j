// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum BreakLocationType {
    @SerializedName("debuggerStatement")
    @JsonProperty("debuggerStatement")
    DebuggerStatement("debuggerStatement"),

    @SerializedName("call")
    @JsonProperty("call")
    Call("call"),

    @SerializedName("return")
    @JsonProperty("return")
    Return("return");

    public final String value;

    BreakLocationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
