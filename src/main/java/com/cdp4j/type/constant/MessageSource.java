// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Message source.
 */
public enum MessageSource {
    @SerializedName("xml")
    @JsonProperty("xml")
    Xml("xml"),

    @SerializedName("javascript")
    @JsonProperty("javascript")
    Javascript("javascript"),

    @SerializedName("network")
    @JsonProperty("network")
    Network("network"),

    @SerializedName("console-api")
    @JsonProperty("console-api")
    ConsoleApi("console-api"),

    @SerializedName("storage")
    @JsonProperty("storage")
    Storage("storage"),

    @SerializedName("appcache")
    @JsonProperty("appcache")
    Appcache("appcache"),

    @SerializedName("rendering")
    @JsonProperty("rendering")
    Rendering("rendering"),

    @SerializedName("security")
    @JsonProperty("security")
    Security("security"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    @SerializedName("deprecation")
    @JsonProperty("deprecation")
    Deprecation("deprecation"),

    @SerializedName("worker")
    @JsonProperty("worker")
    Worker("worker");

    public final String value;

    MessageSource(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
