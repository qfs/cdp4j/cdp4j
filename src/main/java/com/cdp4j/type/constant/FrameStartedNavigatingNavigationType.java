// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum FrameStartedNavigatingNavigationType {
    @SerializedName("reload")
    @JsonProperty("reload")
    Reload("reload"),

    @SerializedName("reloadBypassingCache")
    @JsonProperty("reloadBypassingCache")
    ReloadBypassingCache("reloadBypassingCache"),

    @SerializedName("restore")
    @JsonProperty("restore")
    Restore("restore"),

    @SerializedName("restoreWithPost")
    @JsonProperty("restoreWithPost")
    RestoreWithPost("restoreWithPost"),

    @SerializedName("historySameDocument")
    @JsonProperty("historySameDocument")
    HistorySameDocument("historySameDocument"),

    @SerializedName("historyDifferentDocument")
    @JsonProperty("historyDifferentDocument")
    HistoryDifferentDocument("historyDifferentDocument"),

    @SerializedName("sameDocument")
    @JsonProperty("sameDocument")
    SameDocument("sameDocument"),

    @SerializedName("differentDocument")
    @JsonProperty("differentDocument")
    DifferentDocument("differentDocument");

    public final String value;

    FrameStartedNavigatingNavigationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
