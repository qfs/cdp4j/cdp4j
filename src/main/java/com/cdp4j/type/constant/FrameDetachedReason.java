// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum FrameDetachedReason {
    @SerializedName("remove")
    @JsonProperty("remove")
    Remove("remove"),

    @SerializedName("swap")
    @JsonProperty("swap")
    Swap("swap");

    public final String value;

    FrameDetachedReason(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
