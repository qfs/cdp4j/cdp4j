// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Scope type.
 */
public enum ScopeType {
    @SerializedName("global")
    @JsonProperty("global")
    Global("global"),

    @SerializedName("local")
    @JsonProperty("local")
    Local("local"),

    @SerializedName("with")
    @JsonProperty("with")
    With("with"),

    @SerializedName("closure")
    @JsonProperty("closure")
    Closure("closure"),

    @SerializedName("catch")
    @JsonProperty("catch")
    Catch("catch"),

    @SerializedName("block")
    @JsonProperty("block")
    Block("block"),

    @SerializedName("script")
    @JsonProperty("script")
    Script("script"),

    @SerializedName("eval")
    @JsonProperty("eval")
    Eval("eval"),

    @SerializedName("module")
    @JsonProperty("module")
    Module("module"),

    @SerializedName("wasm-expression-stack")
    @JsonProperty("wasm-expression-stack")
    WasmExpressionStack("wasm-expression-stack");

    public final String value;

    ScopeType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
