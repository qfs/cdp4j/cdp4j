// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum SerializationOptionsType {
    @SerializedName("deep")
    @JsonProperty("deep")
    Deep("deep"),

    @SerializedName("json")
    @JsonProperty("json")
    Json("json"),

    @SerializedName("idOnly")
    @JsonProperty("idOnly")
    IdOnly("idOnly");

    public final String value;

    SerializationOptionsType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
