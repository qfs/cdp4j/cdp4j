// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The referrer policy of the request, as defined in https://www.w3.org/TR/referrer-policy/
 */
public enum ReferrerPolicy {
    @SerializedName("unsafe-url")
    @JsonProperty("unsafe-url")
    UnsafeUrl("unsafe-url"),

    @SerializedName("no-referrer-when-downgrade")
    @JsonProperty("no-referrer-when-downgrade")
    NoReferrerWhenDowngrade("no-referrer-when-downgrade"),

    @SerializedName("no-referrer")
    @JsonProperty("no-referrer")
    NoReferrer("no-referrer"),

    @SerializedName("origin")
    @JsonProperty("origin")
    Origin("origin"),

    @SerializedName("origin-when-cross-origin")
    @JsonProperty("origin-when-cross-origin")
    OriginWhenCrossOrigin("origin-when-cross-origin"),

    @SerializedName("same-origin")
    @JsonProperty("same-origin")
    SameOrigin("same-origin"),

    @SerializedName("strict-origin")
    @JsonProperty("strict-origin")
    StrictOrigin("strict-origin"),

    @SerializedName("strict-origin-when-cross-origin")
    @JsonProperty("strict-origin-when-cross-origin")
    StrictOriginWhenCrossOrigin("strict-origin-when-cross-origin");

    public final String value;

    ReferrerPolicy(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
