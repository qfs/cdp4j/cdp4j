// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum DownloadState {
    @SerializedName("inProgress")
    @JsonProperty("inProgress")
    InProgress("inProgress"),

    @SerializedName("completed")
    @JsonProperty("completed")
    Completed("completed"),

    @SerializedName("canceled")
    @JsonProperty("canceled")
    Canceled("canceled");

    public final String value;

    DownloadState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
