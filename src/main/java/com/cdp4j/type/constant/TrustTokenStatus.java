// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum TrustTokenStatus {
    @SerializedName("Ok")
    @JsonProperty("Ok")
    Ok("Ok"),

    @SerializedName("InvalidArgument")
    @JsonProperty("InvalidArgument")
    InvalidArgument("InvalidArgument"),

    @SerializedName("MissingIssuerKeys")
    @JsonProperty("MissingIssuerKeys")
    MissingIssuerKeys("MissingIssuerKeys"),

    @SerializedName("FailedPrecondition")
    @JsonProperty("FailedPrecondition")
    FailedPrecondition("FailedPrecondition"),

    @SerializedName("ResourceExhausted")
    @JsonProperty("ResourceExhausted")
    ResourceExhausted("ResourceExhausted"),

    @SerializedName("AlreadyExists")
    @JsonProperty("AlreadyExists")
    AlreadyExists("AlreadyExists"),

    @SerializedName("ResourceLimited")
    @JsonProperty("ResourceLimited")
    ResourceLimited("ResourceLimited"),

    @SerializedName("Unauthorized")
    @JsonProperty("Unauthorized")
    Unauthorized("Unauthorized"),

    @SerializedName("BadResponse")
    @JsonProperty("BadResponse")
    BadResponse("BadResponse"),

    @SerializedName("InternalError")
    @JsonProperty("InternalError")
    InternalError("InternalError"),

    @SerializedName("UnknownError")
    @JsonProperty("UnknownError")
    UnknownError("UnknownError"),

    @SerializedName("FulfilledLocally")
    @JsonProperty("FulfilledLocally")
    FulfilledLocally("FulfilledLocally"),

    @SerializedName("SiteIssuerLimit")
    @JsonProperty("SiteIssuerLimit")
    SiteIssuerLimit("SiteIssuerLimit");

    public final String value;

    TrustTokenStatus(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
