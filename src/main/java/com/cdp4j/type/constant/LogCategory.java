// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum LogCategory {
    @SerializedName("cors")
    @JsonProperty("cors")
    Cors("cors");

    public final String value;

    LogCategory(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
