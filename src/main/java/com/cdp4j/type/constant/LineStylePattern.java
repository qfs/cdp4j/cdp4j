// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The line pattern (default: solid)
 */
public enum LineStylePattern {
    @SerializedName("dashed")
    @JsonProperty("dashed")
    Dashed("dashed"),

    @SerializedName("dotted")
    @JsonProperty("dotted")
    Dotted("dotted");

    public final String value;

    LineStylePattern(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
