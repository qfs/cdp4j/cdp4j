// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Only set for "token-redemption" operation and determine whether
 * to request a fresh SRR or use a still valid cached SRR.
 */
public enum TrustTokenRefreshPolicy {
    @SerializedName("UseCached")
    @JsonProperty("UseCached")
    UseCached("UseCached"),

    @SerializedName("Refresh")
    @JsonProperty("Refresh")
    Refresh("Refresh");

    public final String value;

    TrustTokenRefreshPolicy(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
