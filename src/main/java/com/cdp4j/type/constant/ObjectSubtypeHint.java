// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Object subtype hint. Specified for object type values only.
 */
public enum ObjectSubtypeHint {
    @SerializedName("array")
    @JsonProperty("array")
    Array("array"),

    @SerializedName("null")
    @JsonProperty("null")
    Null("null"),

    @SerializedName("node")
    @JsonProperty("node")
    Node("node"),

    @SerializedName("regexp")
    @JsonProperty("regexp")
    Regexp("regexp"),

    @SerializedName("date")
    @JsonProperty("date")
    Date("date"),

    @SerializedName("map")
    @JsonProperty("map")
    Map("map"),

    @SerializedName("set")
    @JsonProperty("set")
    Set("set"),

    @SerializedName("weakmap")
    @JsonProperty("weakmap")
    Weakmap("weakmap"),

    @SerializedName("weakset")
    @JsonProperty("weakset")
    Weakset("weakset"),

    @SerializedName("iterator")
    @JsonProperty("iterator")
    Iterator("iterator"),

    @SerializedName("generator")
    @JsonProperty("generator")
    Generator("generator"),

    @SerializedName("error")
    @JsonProperty("error")
    Error("error"),

    @SerializedName("proxy")
    @JsonProperty("proxy")
    Proxy("proxy"),

    @SerializedName("promise")
    @JsonProperty("promise")
    Promise("promise"),

    @SerializedName("typedarray")
    @JsonProperty("typedarray")
    Typedarray("typedarray"),

    @SerializedName("arraybuffer")
    @JsonProperty("arraybuffer")
    Arraybuffer("arraybuffer"),

    @SerializedName("dataview")
    @JsonProperty("dataview")
    Dataview("dataview"),

    @SerializedName("webassemblymemory")
    @JsonProperty("webassemblymemory")
    Webassemblymemory("webassemblymemory"),

    @SerializedName("wasmvalue")
    @JsonProperty("wasmvalue")
    Wasmvalue("wasmvalue");

    public final String value;

    ObjectSubtypeHint(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
