// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum PauseOnExceptionState {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("caught")
    @JsonProperty("caught")
    Caught("caught"),

    @SerializedName("uncaught")
    @JsonProperty("uncaught")
    Uncaught("uncaught"),

    @SerializedName("all")
    @JsonProperty("all")
    All("all");

    public final String value;

    PauseOnExceptionState(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
