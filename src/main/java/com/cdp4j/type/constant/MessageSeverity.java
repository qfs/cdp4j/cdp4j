// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Message severity.
 */
public enum MessageSeverity {
    @SerializedName("log")
    @JsonProperty("log")
    Log("log"),

    @SerializedName("warning")
    @JsonProperty("warning")
    Warning("warning"),

    @SerializedName("error")
    @JsonProperty("error")
    Error("error"),

    @SerializedName("debug")
    @JsonProperty("debug")
    Debug("debug"),

    @SerializedName("info")
    @JsonProperty("info")
    Info("info");

    public final String value;

    MessageSeverity(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
