// SPDX-License-Identifier: MIT
package com.cdp4j.type.constant;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

public enum EmulatedVisionDeficiency {
    @SerializedName("none")
    @JsonProperty("none")
    None("none"),

    @SerializedName("blurredVision")
    @JsonProperty("blurredVision")
    BlurredVision("blurredVision"),

    @SerializedName("reducedContrast")
    @JsonProperty("reducedContrast")
    ReducedContrast("reducedContrast"),

    @SerializedName("achromatopsia")
    @JsonProperty("achromatopsia")
    Achromatopsia("achromatopsia"),

    @SerializedName("deuteranopia")
    @JsonProperty("deuteranopia")
    Deuteranopia("deuteranopia"),

    @SerializedName("protanopia")
    @JsonProperty("protanopia")
    Protanopia("protanopia"),

    @SerializedName("tritanopia")
    @JsonProperty("tritanopia")
    Tritanopia("tritanopia");

    public final String value;

    EmulatedVisionDeficiency(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
