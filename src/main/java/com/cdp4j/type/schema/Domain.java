// SPDX-License-Identifier: MIT
package com.cdp4j.type.schema;

/**
 * Description of the protocol domain.
 */
public class Domain {
    private String name;

    private String version;

    /**
     * Domain name.
     */
    public String getName() {
        return name;
    }

    /**
     * Domain name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Domain version.
     */
    public String getVersion() {
        return version;
    }

    /**
     * Domain version.
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public String toString() {
        return "Domain [name=" + name + ", version=" + version + "]";
    }
}
