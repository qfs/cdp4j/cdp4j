// SPDX-License-Identifier: MIT
package com.cdp4j.type.systeminfo;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Provides information about the GPU(s) on the system.
 */
public class GPUInfo {
    private List<GPUDevice> devices = emptyList();

    private Object auxAttributes;

    private Object featureStatus;

    private List<String> driverBugWorkarounds = emptyList();

    private List<VideoDecodeAcceleratorCapability> videoDecoding = emptyList();

    private List<VideoEncodeAcceleratorCapability> videoEncoding = emptyList();

    private List<ImageDecodeAcceleratorCapability> imageDecoding = emptyList();

    /**
     * The graphics devices on the system. Element 0 is the primary GPU.
     */
    public List<GPUDevice> getDevices() {
        return devices;
    }

    /**
     * The graphics devices on the system. Element 0 is the primary GPU.
     */
    public void setDevices(List<GPUDevice> devices) {
        this.devices = devices;
    }

    /**
     * An optional dictionary of additional GPU related attributes.
     */
    public Object getAuxAttributes() {
        return auxAttributes;
    }

    /**
     * An optional dictionary of additional GPU related attributes.
     */
    public void setAuxAttributes(Object auxAttributes) {
        this.auxAttributes = auxAttributes;
    }

    /**
     * An optional dictionary of graphics features and their status.
     */
    public Object getFeatureStatus() {
        return featureStatus;
    }

    /**
     * An optional dictionary of graphics features and their status.
     */
    public void setFeatureStatus(Object featureStatus) {
        this.featureStatus = featureStatus;
    }

    /**
     * An optional array of GPU driver bug workarounds.
     */
    public List<String> getDriverBugWorkarounds() {
        return driverBugWorkarounds;
    }

    /**
     * An optional array of GPU driver bug workarounds.
     */
    public void setDriverBugWorkarounds(List<String> driverBugWorkarounds) {
        this.driverBugWorkarounds = driverBugWorkarounds;
    }

    /**
     * Supported accelerated video decoding capabilities.
     */
    public List<VideoDecodeAcceleratorCapability> getVideoDecoding() {
        return videoDecoding;
    }

    /**
     * Supported accelerated video decoding capabilities.
     */
    public void setVideoDecoding(List<VideoDecodeAcceleratorCapability> videoDecoding) {
        this.videoDecoding = videoDecoding;
    }

    /**
     * Supported accelerated video encoding capabilities.
     */
    public List<VideoEncodeAcceleratorCapability> getVideoEncoding() {
        return videoEncoding;
    }

    /**
     * Supported accelerated video encoding capabilities.
     */
    public void setVideoEncoding(List<VideoEncodeAcceleratorCapability> videoEncoding) {
        this.videoEncoding = videoEncoding;
    }

    /**
     * Supported accelerated image decoding capabilities.
     */
    public List<ImageDecodeAcceleratorCapability> getImageDecoding() {
        return imageDecoding;
    }

    /**
     * Supported accelerated image decoding capabilities.
     */
    public void setImageDecoding(List<ImageDecodeAcceleratorCapability> imageDecoding) {
        this.imageDecoding = imageDecoding;
    }

    public String toString() {
        return "GPUInfo [devices=" + devices + ", auxAttributes=" + auxAttributes + ", featureStatus=" + featureStatus
                + ", driverBugWorkarounds=" + driverBugWorkarounds + ", videoDecoding=" + videoDecoding
                + ", videoEncoding=" + videoEncoding + ", imageDecoding=" + imageDecoding + "]";
    }
}
