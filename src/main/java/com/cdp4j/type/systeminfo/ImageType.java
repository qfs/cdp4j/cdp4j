// SPDX-License-Identifier: MIT
package com.cdp4j.type.systeminfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Image format of a given image.
 */
public enum ImageType {
    @SerializedName("jpeg")
    @JsonProperty("jpeg")
    Jpeg("jpeg"),

    @SerializedName("webp")
    @JsonProperty("webp")
    Webp("webp"),

    @SerializedName("unknown")
    @JsonProperty("unknown")
    Unknown("unknown"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    ImageType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
