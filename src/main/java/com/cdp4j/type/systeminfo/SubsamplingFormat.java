// SPDX-License-Identifier: MIT
package com.cdp4j.type.systeminfo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * YUV subsampling type of the pixels of a given image.
 */
public enum SubsamplingFormat {
    @SerializedName("yuv420")
    @JsonProperty("yuv420")
    Yuv420("yuv420"),

    @SerializedName("yuv422")
    @JsonProperty("yuv422")
    Yuv422("yuv422"),

    @SerializedName("yuv444")
    @JsonProperty("yuv444")
    Yuv444("yuv444"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    SubsamplingFormat(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
