// SPDX-License-Identifier: MIT
package com.cdp4j.type.animation;

/**
 * Keyframe Style
 */
public class KeyframeStyle {
    private String offset;

    private String easing;

    /**
     * Keyframe's time offset.
     */
    public String getOffset() {
        return offset;
    }

    /**
     * Keyframe's time offset.
     */
    public void setOffset(String offset) {
        this.offset = offset;
    }

    /**
     * AnimationEffect's timing function.
     */
    public String getEasing() {
        return easing;
    }

    /**
     * AnimationEffect's timing function.
     */
    public void setEasing(String easing) {
        this.easing = easing;
    }

    public String toString() {
        return "KeyframeStyle [offset=" + offset + ", easing=" + easing + "]";
    }
}
