// SPDX-License-Identifier: MIT
package com.cdp4j.type.animation;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * Keyframes Rule
 */
public class KeyframesRule {
    private String name;

    private List<KeyframeStyle> keyframes = emptyList();

    /**
     * CSS keyframed animation's name.
     */
    public String getName() {
        return name;
    }

    /**
     * CSS keyframed animation's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * List of animation keyframes.
     */
    public List<KeyframeStyle> getKeyframes() {
        return keyframes;
    }

    /**
     * List of animation keyframes.
     */
    public void setKeyframes(List<KeyframeStyle> keyframes) {
        this.keyframes = keyframes;
    }

    public String toString() {
        return "KeyframesRule [name=" + name + ", keyframes=" + keyframes + "]";
    }
}
