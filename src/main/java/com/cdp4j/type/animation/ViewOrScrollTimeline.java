// SPDX-License-Identifier: MIT
package com.cdp4j.type.animation;

import com.cdp4j.type.dom.ScrollOrientation;

/**
 * Timeline instance
 */
public class ViewOrScrollTimeline {
    private Integer sourceNodeId;

    private Double startOffset;

    private Double endOffset;

    private Integer subjectNodeId;

    private ScrollOrientation axis;

    /**
     * Scroll container node
     */
    public Integer getSourceNodeId() {
        return sourceNodeId;
    }

    /**
     * Scroll container node
     */
    public void setSourceNodeId(Integer sourceNodeId) {
        this.sourceNodeId = sourceNodeId;
    }

    /**
     * Represents the starting scroll position of the timeline as a length offset in
     * pixels from scroll origin.
     */
    public Double getStartOffset() {
        return startOffset;
    }

    /**
     * Represents the starting scroll position of the timeline as a length offset in
     * pixels from scroll origin.
     */
    public void setStartOffset(Double startOffset) {
        this.startOffset = startOffset;
    }

    /**
     * Represents the ending scroll position of the timeline as a length offset in
     * pixels from scroll origin.
     */
    public Double getEndOffset() {
        return endOffset;
    }

    /**
     * Represents the ending scroll position of the timeline as a length offset in
     * pixels from scroll origin.
     */
    public void setEndOffset(Double endOffset) {
        this.endOffset = endOffset;
    }

    /**
     * The element whose principal box's visibility in the scrollport defined the
     * progress of the timeline. Does not exist for animations with ScrollTimeline
     */
    public Integer getSubjectNodeId() {
        return subjectNodeId;
    }

    /**
     * The element whose principal box's visibility in the scrollport defined the
     * progress of the timeline. Does not exist for animations with ScrollTimeline
     */
    public void setSubjectNodeId(Integer subjectNodeId) {
        this.subjectNodeId = subjectNodeId;
    }

    /**
     * Orientation of the scroll
     */
    public ScrollOrientation getAxis() {
        return axis;
    }

    /**
     * Orientation of the scroll
     */
    public void setAxis(ScrollOrientation axis) {
        this.axis = axis;
    }

    public String toString() {
        return "ViewOrScrollTimeline [sourceNodeId=" + sourceNodeId + ", startOffset=" + startOffset + ", endOffset="
                + endOffset + ", subjectNodeId=" + subjectNodeId + ", axis=" + axis + "]";
    }
}
