// SPDX-License-Identifier: MIT
package com.cdp4j.type.accessibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Values of AXProperty name:
 * - from 'busy' to 'roledescription': states which apply to every AX node
 * - from 'live' to 'root': attributes which apply to nodes in live regions
 * - from 'autocomplete' to 'valuetext': attributes which apply to widgets
 * - from 'checked' to 'selected': states which apply to widgets
 * - from 'activedescendant' to 'owns' - relationships between elements other than parent/child/sibling.
 */
public enum AXPropertyName {
    @SerializedName("actions")
    @JsonProperty("actions")
    Actions("actions"),

    @SerializedName("busy")
    @JsonProperty("busy")
    Busy("busy"),

    @SerializedName("disabled")
    @JsonProperty("disabled")
    Disabled("disabled"),

    @SerializedName("editable")
    @JsonProperty("editable")
    Editable("editable"),

    @SerializedName("focusable")
    @JsonProperty("focusable")
    Focusable("focusable"),

    @SerializedName("focused")
    @JsonProperty("focused")
    Focused("focused"),

    @SerializedName("hidden")
    @JsonProperty("hidden")
    Hidden("hidden"),

    @SerializedName("hiddenRoot")
    @JsonProperty("hiddenRoot")
    HiddenRoot("hiddenRoot"),

    @SerializedName("invalid")
    @JsonProperty("invalid")
    Invalid("invalid"),

    @SerializedName("keyshortcuts")
    @JsonProperty("keyshortcuts")
    Keyshortcuts("keyshortcuts"),

    @SerializedName("settable")
    @JsonProperty("settable")
    Settable("settable"),

    @SerializedName("roledescription")
    @JsonProperty("roledescription")
    Roledescription("roledescription"),

    @SerializedName("live")
    @JsonProperty("live")
    Live("live"),

    @SerializedName("atomic")
    @JsonProperty("atomic")
    Atomic("atomic"),

    @SerializedName("relevant")
    @JsonProperty("relevant")
    Relevant("relevant"),

    @SerializedName("root")
    @JsonProperty("root")
    Root("root"),

    @SerializedName("autocomplete")
    @JsonProperty("autocomplete")
    Autocomplete("autocomplete"),

    @SerializedName("hasPopup")
    @JsonProperty("hasPopup")
    HasPopup("hasPopup"),

    @SerializedName("level")
    @JsonProperty("level")
    Level("level"),

    @SerializedName("multiselectable")
    @JsonProperty("multiselectable")
    Multiselectable("multiselectable"),

    @SerializedName("orientation")
    @JsonProperty("orientation")
    Orientation("orientation"),

    @SerializedName("multiline")
    @JsonProperty("multiline")
    Multiline("multiline"),

    @SerializedName("readonly")
    @JsonProperty("readonly")
    Readonly("readonly"),

    @SerializedName("required")
    @JsonProperty("required")
    Required("required"),

    @SerializedName("valuemin")
    @JsonProperty("valuemin")
    Valuemin("valuemin"),

    @SerializedName("valuemax")
    @JsonProperty("valuemax")
    Valuemax("valuemax"),

    @SerializedName("valuetext")
    @JsonProperty("valuetext")
    Valuetext("valuetext"),

    @SerializedName("checked")
    @JsonProperty("checked")
    Checked("checked"),

    @SerializedName("expanded")
    @JsonProperty("expanded")
    Expanded("expanded"),

    @SerializedName("modal")
    @JsonProperty("modal")
    Modal("modal"),

    @SerializedName("pressed")
    @JsonProperty("pressed")
    Pressed("pressed"),

    @SerializedName("selected")
    @JsonProperty("selected")
    Selected("selected"),

    @SerializedName("activedescendant")
    @JsonProperty("activedescendant")
    Activedescendant("activedescendant"),

    @SerializedName("controls")
    @JsonProperty("controls")
    Controls("controls"),

    @SerializedName("describedby")
    @JsonProperty("describedby")
    Describedby("describedby"),

    @SerializedName("details")
    @JsonProperty("details")
    Details("details"),

    @SerializedName("errormessage")
    @JsonProperty("errormessage")
    Errormessage("errormessage"),

    @SerializedName("flowto")
    @JsonProperty("flowto")
    Flowto("flowto"),

    @SerializedName("labelledby")
    @JsonProperty("labelledby")
    Labelledby("labelledby"),

    @SerializedName("owns")
    @JsonProperty("owns")
    Owns("owns"),

    @SerializedName("url")
    @JsonProperty("url")
    Url("url"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AXPropertyName(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
