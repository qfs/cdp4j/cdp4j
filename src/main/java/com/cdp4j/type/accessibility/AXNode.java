// SPDX-License-Identifier: MIT
package com.cdp4j.type.accessibility;

import static java.util.Collections.emptyList;

import java.util.List;

/**
 * A node in the accessibility tree.
 */
public class AXNode {
    private String nodeId;

    private Boolean ignored;

    private List<AXProperty> ignoredReasons = emptyList();

    private AXValue role;

    private AXValue chromeRole;

    private AXValue name;

    private AXValue description;

    private AXValue value;

    private List<AXProperty> properties = emptyList();

    private String parentId;

    private List<String> childIds = emptyList();

    private Integer backendDOMNodeId;

    private String frameId;

    /**
     * Unique identifier for this node.
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * Unique identifier for this node.
     */
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * Whether this node is ignored for accessibility
     */
    public Boolean isIgnored() {
        return ignored;
    }

    /**
     * Whether this node is ignored for accessibility
     */
    public void setIgnored(Boolean ignored) {
        this.ignored = ignored;
    }

    /**
     * Collection of reasons why this node is hidden.
     */
    public List<AXProperty> getIgnoredReasons() {
        return ignoredReasons;
    }

    /**
     * Collection of reasons why this node is hidden.
     */
    public void setIgnoredReasons(List<AXProperty> ignoredReasons) {
        this.ignoredReasons = ignoredReasons;
    }

    /**
     * This Node's role, whether explicit or implicit.
     */
    public AXValue getRole() {
        return role;
    }

    /**
     * This Node's role, whether explicit or implicit.
     */
    public void setRole(AXValue role) {
        this.role = role;
    }

    /**
     * This Node's Chrome raw role.
     */
    public AXValue getChromeRole() {
        return chromeRole;
    }

    /**
     * This Node's Chrome raw role.
     */
    public void setChromeRole(AXValue chromeRole) {
        this.chromeRole = chromeRole;
    }

    /**
     * The accessible name for this Node.
     */
    public AXValue getName() {
        return name;
    }

    /**
     * The accessible name for this Node.
     */
    public void setName(AXValue name) {
        this.name = name;
    }

    /**
     * The accessible description for this Node.
     */
    public AXValue getDescription() {
        return description;
    }

    /**
     * The accessible description for this Node.
     */
    public void setDescription(AXValue description) {
        this.description = description;
    }

    /**
     * The value for this Node.
     */
    public AXValue getValue() {
        return value;
    }

    /**
     * The value for this Node.
     */
    public void setValue(AXValue value) {
        this.value = value;
    }

    /**
     * All other properties
     */
    public List<AXProperty> getProperties() {
        return properties;
    }

    /**
     * All other properties
     */
    public void setProperties(List<AXProperty> properties) {
        this.properties = properties;
    }

    /**
     * ID for this node's parent.
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * ID for this node's parent.
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * IDs for each of this node's child nodes.
     */
    public List<String> getChildIds() {
        return childIds;
    }

    /**
     * IDs for each of this node's child nodes.
     */
    public void setChildIds(List<String> childIds) {
        this.childIds = childIds;
    }

    /**
     * The backend ID for the associated DOM node, if any.
     */
    public Integer getBackendDOMNodeId() {
        return backendDOMNodeId;
    }

    /**
     * The backend ID for the associated DOM node, if any.
     */
    public void setBackendDOMNodeId(Integer backendDOMNodeId) {
        this.backendDOMNodeId = backendDOMNodeId;
    }

    /**
     * The frame ID for the frame associated with this nodes document.
     */
    public String getFrameId() {
        return frameId;
    }

    /**
     * The frame ID for the frame associated with this nodes document.
     */
    public void setFrameId(String frameId) {
        this.frameId = frameId;
    }

    public String toString() {
        return "AXNode [nodeId=" + nodeId + ", ignored=" + ignored + ", ignoredReasons=" + ignoredReasons + ", role="
                + role + ", chromeRole=" + chromeRole + ", name=" + name + ", description=" + description + ", value="
                + value + ", properties=" + properties + ", parentId=" + parentId + ", childIds=" + childIds
                + ", backendDOMNodeId=" + backendDOMNodeId + ", frameId=" + frameId + "]";
    }
}
