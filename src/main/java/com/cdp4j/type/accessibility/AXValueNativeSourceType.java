// SPDX-License-Identifier: MIT
package com.cdp4j.type.accessibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of possible native property sources (as a subtype of a particular AXValueSourceType).
 */
public enum AXValueNativeSourceType {
    @SerializedName("description")
    @JsonProperty("description")
    Description("description"),

    @SerializedName("figcaption")
    @JsonProperty("figcaption")
    Figcaption("figcaption"),

    @SerializedName("label")
    @JsonProperty("label")
    Label("label"),

    @SerializedName("labelfor")
    @JsonProperty("labelfor")
    Labelfor("labelfor"),

    @SerializedName("labelwrapped")
    @JsonProperty("labelwrapped")
    Labelwrapped("labelwrapped"),

    @SerializedName("legend")
    @JsonProperty("legend")
    Legend("legend"),

    @SerializedName("rubyannotation")
    @JsonProperty("rubyannotation")
    Rubyannotation("rubyannotation"),

    @SerializedName("tablecaption")
    @JsonProperty("tablecaption")
    Tablecaption("tablecaption"),

    @SerializedName("title")
    @JsonProperty("title")
    Title("title"),

    @SerializedName("other")
    @JsonProperty("other")
    Other("other"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AXValueNativeSourceType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
