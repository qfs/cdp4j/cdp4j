// SPDX-License-Identifier: MIT
package com.cdp4j.type.accessibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of possible property sources.
 */
public enum AXValueSourceType {
    @SerializedName("attribute")
    @JsonProperty("attribute")
    Attribute("attribute"),

    @SerializedName("implicit")
    @JsonProperty("implicit")
    Implicit("implicit"),

    @SerializedName("style")
    @JsonProperty("style")
    Style("style"),

    @SerializedName("contents")
    @JsonProperty("contents")
    Contents("contents"),

    @SerializedName("placeholder")
    @JsonProperty("placeholder")
    Placeholder("placeholder"),

    @SerializedName("relatedElement")
    @JsonProperty("relatedElement")
    RelatedElement("relatedElement"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AXValueSourceType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
