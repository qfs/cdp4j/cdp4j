// SPDX-License-Identifier: MIT
package com.cdp4j.type.accessibility;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * Enum of possible property types.
 */
public enum AXValueType {
    @SerializedName("boolean")
    @JsonProperty("boolean")
    Boolean("boolean"),

    @SerializedName("tristate")
    @JsonProperty("tristate")
    Tristate("tristate"),

    @SerializedName("booleanOrUndefined")
    @JsonProperty("booleanOrUndefined")
    BooleanOrUndefined("booleanOrUndefined"),

    @SerializedName("idref")
    @JsonProperty("idref")
    Idref("idref"),

    @SerializedName("idrefList")
    @JsonProperty("idrefList")
    IdrefList("idrefList"),

    @SerializedName("integer")
    @JsonProperty("integer")
    Integer("integer"),

    @SerializedName("node")
    @JsonProperty("node")
    Node("node"),

    @SerializedName("nodeList")
    @JsonProperty("nodeList")
    NodeList("nodeList"),

    @SerializedName("number")
    @JsonProperty("number")
    Number("number"),

    @SerializedName("string")
    @JsonProperty("string")
    String("string"),

    @SerializedName("computedString")
    @JsonProperty("computedString")
    ComputedString("computedString"),

    @SerializedName("token")
    @JsonProperty("token")
    Token("token"),

    @SerializedName("tokenList")
    @JsonProperty("tokenList")
    TokenList("tokenList"),

    @SerializedName("domRelation")
    @JsonProperty("domRelation")
    DomRelation("domRelation"),

    @SerializedName("role")
    @JsonProperty("role")
    Role("role"),

    @SerializedName("internalRole")
    @JsonProperty("internalRole")
    InternalRole("internalRole"),

    @SerializedName("valueUndefined")
    @JsonProperty("valueUndefined")
    ValueUndefined("valueUndefined"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    AXValueType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
