// SPDX-License-Identifier: MIT
package com.cdp4j.type.headlessexperimental;

import com.cdp4j.type.constant.ImageFormat;

/**
 * Encoding options for a screenshot.
 */
public class ScreenshotParams {
    private ImageFormat format;

    private Integer quality;

    private Boolean optimizeForSpeed;

    /**
     * Image compression format (defaults to png).
     */
    public ImageFormat getFormat() {
        return format;
    }

    /**
     * Image compression format (defaults to png).
     */
    public void setFormat(ImageFormat format) {
        this.format = format;
    }

    /**
     * Compression quality from range [0..100] (jpeg and webp only).
     */
    public Integer getQuality() {
        return quality;
    }

    /**
     * Compression quality from range [0..100] (jpeg and webp only).
     */
    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    /**
     * Optimize image encoding for speed, not for resulting size (defaults to false)
     */
    public Boolean isOptimizeForSpeed() {
        return optimizeForSpeed;
    }

    /**
     * Optimize image encoding for speed, not for resulting size (defaults to false)
     */
    public void setOptimizeForSpeed(Boolean optimizeForSpeed) {
        this.optimizeForSpeed = optimizeForSpeed;
    }

    public String toString() {
        return "ScreenshotParams [format=" + format + ", quality=" + quality + ", optimizeForSpeed=" + optimizeForSpeed
                + "]";
    }
}
