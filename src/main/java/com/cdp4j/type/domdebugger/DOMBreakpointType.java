// SPDX-License-Identifier: MIT
package com.cdp4j.type.domdebugger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * DOM breakpoint type.
 */
public enum DOMBreakpointType {
    @SerializedName("subtree-modified")
    @JsonProperty("subtree-modified")
    SubtreeModified("subtree-modified"),

    @SerializedName("attribute-modified")
    @JsonProperty("attribute-modified")
    AttributeModified("attribute-modified"),

    @SerializedName("node-removed")
    @JsonProperty("node-removed")
    NodeRemoved("node-removed"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    DOMBreakpointType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
