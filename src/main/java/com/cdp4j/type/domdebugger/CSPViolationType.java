// SPDX-License-Identifier: MIT
package com.cdp4j.type.domdebugger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * CSP Violation type.
 */
public enum CSPViolationType {
    @SerializedName("trustedtype-sink-violation")
    @JsonProperty("trustedtype-sink-violation")
    TrustedtypeSinkViolation("trustedtype-sink-violation"),

    @SerializedName("trustedtype-policy-violation")
    @JsonProperty("trustedtype-policy-violation")
    TrustedtypePolicyViolation("trustedtype-policy-violation"),

    _UNKNOWN_("_UNKNOWN_");

    public final String value;

    CSPViolationType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
