// SPDX-License-Identifier: MIT
package com.cdp4j.listener;

import com.cdp4j.event.Events;

@FunctionalInterface
public interface EventListener {

    void onEvent(Events event, Object value);
}
