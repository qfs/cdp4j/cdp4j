// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.runtime.RemoteObject;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class AnimationAsyncImpl extends ParameterizedCommandImpl<AnimationAsync> implements AnimationAsync {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_CURRENT_TIME =
            new CommandReturnType("currentTime", Double.class, null);
    private static final CommandReturnType CRT_GET_PLAYBACK_RATE =
            new CommandReturnType("playbackRate", Double.class, null);
    private static final CommandReturnType CRT_RELEASE_ANIMATIONS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RESOLVE_ANIMATION =
            new CommandReturnType("remoteObject", RemoteObject.class, null);
    private static final CommandReturnType CRT_SEEK_ANIMATIONS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PAUSED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PLAYBACK_RATE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_TIMING = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_CURRENT_TIME_1 = new String[] {"id"};
    private static final String[] PARAMS_RELEASE_ANIMATIONS_1 = new String[] {"animations"};
    private static final String[] PARAMS_RESOLVE_ANIMATION_1 = new String[] {"animationId"};
    private static final String[] PARAMS_SEEK_ANIMATIONS_1 = new String[] {"animations", "currentTime"};
    private static final String[] PARAMS_SET_PAUSED_1 = new String[] {"animations", "paused"};
    private static final String[] PARAMS_SET_PLAYBACK_RATE_1 = new String[] {"playbackRate"};
    private static final String[] PARAMS_SET_TIMING_1 = new String[] {"animationId", "duration", "delay"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public AnimationAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Animation_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Animation_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Double> getCurrentTime(String id) {
        return (CompletableFuture<Double>) handler.invoke(
                this,
                DomainCommand.Animation_getCurrentTime,
                CRT_GET_CURRENT_TIME,
                PARAMS_GET_CURRENT_TIME_1,
                new Object[] {id},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Double> getPlaybackRate() {
        return (CompletableFuture<Double>) handler.invoke(
                this, DomainCommand.Animation_getPlaybackRate, CRT_GET_PLAYBACK_RATE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> releaseAnimations(List<String> animations) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Animation_releaseAnimations,
                CRT_RELEASE_ANIMATIONS,
                PARAMS_RELEASE_ANIMATIONS_1,
                new Object[] {animations},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RemoteObject> resolveAnimation(String animationId) {
        return (CompletableFuture<RemoteObject>) handler.invoke(
                this,
                DomainCommand.Animation_resolveAnimation,
                CRT_RESOLVE_ANIMATION,
                PARAMS_RESOLVE_ANIMATION_1,
                new Object[] {animationId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> seekAnimations(List<String> animations, Double currentTime) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Animation_seekAnimations,
                CRT_SEEK_ANIMATIONS,
                PARAMS_SEEK_ANIMATIONS_1,
                new Object[] {animations, currentTime},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPaused(List<String> animations, Boolean paused) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Animation_setPaused,
                CRT_SET_PAUSED,
                PARAMS_SET_PAUSED_1,
                new Object[] {animations, paused},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPlaybackRate(Double playbackRate) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Animation_setPlaybackRate,
                CRT_SET_PLAYBACK_RATE,
                PARAMS_SET_PLAYBACK_RATE_1,
                new Object[] {playbackRate},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setTiming(String animationId, Double duration, Double delay) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Animation_setTiming,
                CRT_SET_TIMING,
                PARAMS_SET_TIMING_1,
                new Object[] {animationId, duration, delay},
                false);
    }
}
