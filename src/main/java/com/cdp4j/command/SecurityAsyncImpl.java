// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.security.CertificateErrorAction;
import java.util.concurrent.CompletableFuture;

class SecurityAsyncImpl extends ParameterizedCommandImpl<SecurityAsync> implements SecurityAsync {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HANDLE_CERTIFICATE_ERROR = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_IGNORE_CERTIFICATE_ERRORS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_OVERRIDE_CERTIFICATE_ERRORS =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_HANDLE_CERTIFICATE_ERROR_1 = new String[] {"eventId", "action"};
    private static final String[] PARAMS_SET_IGNORE_CERTIFICATE_ERRORS_1 = new String[] {"ignore"};
    private static final String[] PARAMS_SET_OVERRIDE_CERTIFICATE_ERRORS_1 = new String[] {"override"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public SecurityAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Security_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Security_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> handleCertificateError(Integer eventId, CertificateErrorAction action) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Security_handleCertificateError,
                CRT_HANDLE_CERTIFICATE_ERROR,
                PARAMS_HANDLE_CERTIFICATE_ERROR_1,
                new Object[] {eventId, action},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setIgnoreCertificateErrors(Boolean ignore) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Security_setIgnoreCertificateErrors,
                CRT_SET_IGNORE_CERTIFICATE_ERRORS,
                PARAMS_SET_IGNORE_CERTIFICATE_ERRORS_1,
                new Object[] {ignore},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setOverrideCertificateErrors(Boolean override) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Security_setOverrideCertificateErrors,
                CRT_SET_OVERRIDE_CERTIFICATE_ERRORS,
                PARAMS_SET_OVERRIDE_CERTIFICATE_ERRORS_1,
                new Object[] {override},
                false);
    }
}
