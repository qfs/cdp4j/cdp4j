// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.debugger.SearchMatch;
import com.cdp4j.type.emulation.UserAgentMetadata;
import com.cdp4j.type.network.AuthChallengeResponse;
import com.cdp4j.type.network.ConnectionType;
import com.cdp4j.type.network.ContentEncoding;
import com.cdp4j.type.network.Cookie;
import com.cdp4j.type.network.CookieParam;
import com.cdp4j.type.network.CookiePartitionKey;
import com.cdp4j.type.network.CookiePriority;
import com.cdp4j.type.network.CookieSameSite;
import com.cdp4j.type.network.CookieSourceScheme;
import com.cdp4j.type.network.ErrorReason;
import com.cdp4j.type.network.GetResponseBodyForInterceptionResult;
import com.cdp4j.type.network.GetResponseBodyResult;
import com.cdp4j.type.network.LoadNetworkResourceOptions;
import com.cdp4j.type.network.LoadNetworkResourcePageResult;
import com.cdp4j.type.network.RequestPattern;
import com.cdp4j.type.network.SecurityIsolationStatus;
import java.util.List;
import java.util.Map;

/**
 * Network domain allows tracking network activities of the page. It exposes information about http,
 * file, data and other requests and responses, their headers, bodies, timing, etc.
 */
public interface Network extends ParameterizedCommand<Network> {
    /**
     * Tells whether clearing browser cache is supported.
     *
     * @return True if browser cache can be cleared.
     */
    @Deprecated
    Boolean canClearBrowserCache();

    /**
     * Tells whether clearing browser cookies is supported.
     *
     * @return True if browser cookies can be cleared.
     */
    @Deprecated
    Boolean canClearBrowserCookies();

    /**
     * Tells whether emulation of network conditions is supported.
     *
     * @return True if emulation of network conditions is supported.
     */
    @Deprecated
    Boolean canEmulateNetworkConditions();

    /**
     * Clears accepted encodings set by setAcceptedEncodings
     */
    @Experimental
    void clearAcceptedEncodingsOverride();

    /**
     * Clears browser cache.
     */
    void clearBrowserCache();

    /**
     * Clears browser cookies.
     */
    void clearBrowserCookies();

    /**
     * Response to Network.requestIntercepted which either modifies the request to continue with any
     * modifications, or blocks it, or completes it with the provided response bytes. If a network
     * fetch occurs as a result which encounters a redirect an additional Network.requestIntercepted
     * event will be sent with the same InterceptionId.
     * Deprecated, use Fetch.continueRequest, Fetch.fulfillRequest and Fetch.failRequest instead.
     *
     */
    @Experimental
    @Deprecated
    void continueInterceptedRequest(String interceptionId);

    /**
     * Response to Network.requestIntercepted which either modifies the request to continue with any
     * modifications, or blocks it, or completes it with the provided response bytes. If a network
     * fetch occurs as a result which encounters a redirect an additional Network.requestIntercepted
     * event will be sent with the same InterceptionId.
     * Deprecated, use Fetch.continueRequest, Fetch.fulfillRequest and Fetch.failRequest instead.
     *
     * @param errorReason If set this causes the request to fail with the given reason. Passing Aborted for requests
     * marked with isNavigationRequest also cancels the navigation. Must not be set in response
     * to an authChallenge.
     * @param rawResponse If set the requests completes using with the provided base64 encoded raw response, including
     * HTTP status line and headers etc... Must not be set in response to an authChallenge. (Encoded as a base64 string when passed over JSON)
     * @param url If set the request url will be modified in a way that's not observable by page. Must not be
     * set in response to an authChallenge.
     * @param method If set this allows the request method to be overridden. Must not be set in response to an
     * authChallenge.
     * @param postData If set this allows postData to be set. Must not be set in response to an authChallenge.
     * @param headers If set this allows the request headers to be changed. Must not be set in response to an
     * authChallenge.
     * @param authChallengeResponse Response to a requestIntercepted with an authChallenge. Must not be set otherwise.
     */
    @Experimental
    @Deprecated
    void continueInterceptedRequest(
            String interceptionId,
            @Optional ErrorReason errorReason,
            @Optional String rawResponse,
            @Optional String url,
            @Optional String method,
            @Optional String postData,
            @Optional Map<String, Object> headers,
            @Optional AuthChallengeResponse authChallengeResponse);

    /**
     * Deletes browser cookies with matching name and url or domain/path/partitionKey pair.
     *
     * @param name Name of the cookies to remove.
     */
    void deleteCookies(String name);

    /**
     * Deletes browser cookies with matching name and url or domain/path/partitionKey pair.
     *
     * @param name Name of the cookies to remove.
     * @param url If specified, deletes all the cookies with the given name where domain and path match
     * provided URL.
     * @param domain If specified, deletes only cookies with the exact domain.
     * @param path If specified, deletes only cookies with the exact path.
     * @param partitionKey If specified, deletes only cookies with the the given name and partitionKey where
     * all partition key attributes match the cookie partition key attribute.
     */
    void deleteCookies(
            String name,
            @Optional String url,
            @Optional String domain,
            @Optional String path,
            @Experimental @Optional CookiePartitionKey partitionKey);

    /**
     * Disables network tracking, prevents network events from being sent to the client.
     */
    void disable();

    /**
     * Activates emulation of network conditions.
     *
     * @param offline True to emulate internet disconnection.
     * @param latency Minimum latency from request sent to response headers received (ms).
     * @param downloadThroughput Maximal aggregated download throughput (bytes/sec). -1 disables download throttling.
     * @param uploadThroughput Maximal aggregated upload throughput (bytes/sec).  -1 disables upload throttling.
     */
    void emulateNetworkConditions(Boolean offline, Double latency, Double downloadThroughput, Double uploadThroughput);

    /**
     * Activates emulation of network conditions.
     *
     * @param offline True to emulate internet disconnection.
     * @param latency Minimum latency from request sent to response headers received (ms).
     * @param downloadThroughput Maximal aggregated download throughput (bytes/sec). -1 disables download throttling.
     * @param uploadThroughput Maximal aggregated upload throughput (bytes/sec).  -1 disables upload throttling.
     * @param connectionType Connection type if known.
     * @param packetLoss WebRTC packet loss (percent, 0-100). 0 disables packet loss emulation, 100 drops all the packets.
     * @param packetQueueLength WebRTC packet queue length (packet). 0 removes any queue length limitations.
     * @param packetReordering WebRTC packetReordering feature.
     */
    void emulateNetworkConditions(
            Boolean offline,
            Double latency,
            Double downloadThroughput,
            Double uploadThroughput,
            @Optional ConnectionType connectionType,
            @Experimental @Optional Double packetLoss,
            @Experimental @Optional Integer packetQueueLength,
            @Experimental @Optional Boolean packetReordering);

    /**
     * Enables network tracking, network events will now be delivered to the client.
     */
    void enable();

    /**
     * Enables network tracking, network events will now be delivered to the client.
     *
     * @param maxTotalBufferSize Buffer size in bytes to use when preserving network payloads (XHRs, etc).
     * @param maxResourceBufferSize Per-resource buffer size in bytes to use when preserving network payloads (XHRs, etc).
     * @param maxPostDataSize Longest post body size (in bytes) that would be included in requestWillBeSent notification
     */
    void enable(
            @Experimental @Optional Integer maxTotalBufferSize,
            @Experimental @Optional Integer maxResourceBufferSize,
            @Optional Integer maxPostDataSize);

    /**
     * Enables tracking for the Reporting API, events generated by the Reporting API will now be delivered to the client.
     * Enabling triggers 'reportingApiReportAdded' for all existing reports.
     *
     * @param enable Whether to enable or disable events for the Reporting API
     */
    @Experimental
    void enableReportingApi(Boolean enable);

    /**
     * Returns all browser cookies. Depending on the backend support, will return detailed cookie
     * information in the cookies field.
     * Deprecated. Use Storage.getCookies instead.
     *
     * @return Array of cookie objects.
     */
    @Deprecated
    List<Cookie> getAllCookies();

    /**
     * Returns the DER-encoded certificate.
     *
     * @param origin Origin to get certificate for.
     */
    @Experimental
    List<String> getCertificate(String origin);

    /**
     * Returns all browser cookies for the current URL. Depending on the backend support, will return
     * detailed cookie information in the cookies field.
     *
     * @return Array of cookie objects.
     */
    List<Cookie> getCookies();

    /**
     * Returns all browser cookies for the current URL. Depending on the backend support, will return
     * detailed cookie information in the cookies field.
     *
     * @param urls The list of URLs for which applicable cookies will be fetched.
     * If not specified, it's assumed to be set to the list containing
     * the URLs of the page and all of its subframes.
     *
     * @return Array of cookie objects.
     */
    List<Cookie> getCookies(@Optional List<String> urls);

    /**
     * Returns post data sent with the request. Returns an error when no data was sent with the request.
     *
     * @param requestId Identifier of the network request to get content for.
     *
     * @return Request body string, omitting files from multipart requests
     */
    String getRequestPostData(String requestId);

    /**
     * Returns content served for the given request.
     *
     * @param requestId Identifier of the network request to get content for.
     *
     * @return GetResponseBodyResult
     */
    GetResponseBodyResult getResponseBody(String requestId);

    /**
     * Returns content served for the given currently intercepted request.
     *
     * @param interceptionId Identifier for the intercepted request to get body for.
     *
     * @return GetResponseBodyForInterceptionResult
     */
    @Experimental
    GetResponseBodyForInterceptionResult getResponseBodyForInterception(String interceptionId);

    /**
     * Returns information about the COEP/COOP isolation status.
     */
    @Experimental
    SecurityIsolationStatus getSecurityIsolationStatus();

    /**
     * Returns information about the COEP/COOP isolation status.
     *
     * @param frameId If no frameId is provided, the status of the target is provided.
     */
    @Experimental
    SecurityIsolationStatus getSecurityIsolationStatus(@Optional String frameId);

    /**
     * Fetches the resource and returns the content.
     *
     * @param url URL of the resource to get content for.
     * @param options Options for the request.
     */
    @Experimental
    LoadNetworkResourcePageResult loadNetworkResource(String url, LoadNetworkResourceOptions options);

    /**
     * Fetches the resource and returns the content.
     *
     * @param frameId Frame id to get the resource for. Mandatory for frame targets, and
     * should be omitted for worker targets.
     * @param url URL of the resource to get content for.
     * @param options Options for the request.
     */
    @Experimental
    LoadNetworkResourcePageResult loadNetworkResource(
            @Optional String frameId, String url, LoadNetworkResourceOptions options);

    /**
     * This method sends a new XMLHttpRequest which is identical to the original one. The following
     * parameters should be identical: method, url, async, request body, extra headers, withCredentials
     * attribute, user, password.
     *
     * @param requestId Identifier of XHR to replay.
     */
    @Experimental
    void replayXHR(String requestId);

    /**
     * Searches for given string in response content.
     *
     * @param requestId Identifier of the network response to search.
     * @param query String to search for.
     *
     * @return List of search matches.
     */
    @Experimental
    List<SearchMatch> searchInResponseBody(String requestId, String query);

    /**
     * Searches for given string in response content.
     *
     * @param requestId Identifier of the network response to search.
     * @param query String to search for.
     * @param caseSensitive If true, search is case sensitive.
     * @param isRegex If true, treats string parameter as regex.
     *
     * @return List of search matches.
     */
    @Experimental
    List<SearchMatch> searchInResponseBody(
            String requestId, String query, @Optional Boolean caseSensitive, @Optional Boolean isRegex);

    /**
     * Sets a list of content encodings that will be accepted. Empty list means no encoding is accepted.
     *
     * @param encodings List of accepted content encodings.
     */
    @Experimental
    void setAcceptedEncodings(ContentEncoding encodings);

    /**
     * Specifies whether to attach a page script stack id in requests
     *
     * @param enabled Whether to attach a page script stack for debugging purpose.
     */
    @Experimental
    void setAttachDebugStack(Boolean enabled);

    /**
     * Blocks URLs from loading.
     *
     * @param urls URL patterns to block. Wildcards ('*') are allowed.
     */
    @Experimental
    void setBlockedURLs(List<String> urls);

    /**
     * Toggles ignoring of service worker for each request.
     *
     * @param bypass Bypass service worker and load from network.
     */
    void setBypassServiceWorker(Boolean bypass);

    /**
     * Toggles ignoring cache for each request. If true, cache will not be used.
     *
     * @param cacheDisabled Cache disabled state.
     */
    void setCacheDisabled(Boolean cacheDisabled);

    /**
     * Sets a cookie with the given cookie data; may overwrite equivalent cookies if they exist.
     *
     * @param name Cookie name.
     * @param value Cookie value.
     *
     * @return Always set to true. If an error occurs, the response indicates protocol error.
     */
    Boolean setCookie(String name, String value);

    /**
     * Sets a cookie with the given cookie data; may overwrite equivalent cookies if they exist.
     *
     * @param name Cookie name.
     * @param value Cookie value.
     * @param url The request-URI to associate with the setting of the cookie. This value can affect the
     * default domain, path, source port, and source scheme values of the created cookie.
     * @param domain Cookie domain.
     * @param path Cookie path.
     * @param secure True if cookie is secure.
     * @param httpOnly True if cookie is http-only.
     * @param sameSite Cookie SameSite type.
     * @param expires Cookie expiration date, session cookie if not set
     * @param priority Cookie Priority type.
     * @param sameParty True if cookie is SameParty.
     * @param sourceScheme Cookie source scheme type.
     * @param sourcePort Cookie source port. Valid values are {-1, [1, 65535]}, -1 indicates an unspecified port.
     * An unspecified port value allows protocol clients to emulate legacy cookie scope for the port.
     * This is a temporary ability and it will be removed in the future.
     * @param partitionKey Cookie partition key. If not set, the cookie will be set as not partitioned.
     *
     * @return Always set to true. If an error occurs, the response indicates protocol error.
     */
    Boolean setCookie(
            String name,
            String value,
            @Optional String url,
            @Optional String domain,
            @Optional String path,
            @Optional Boolean secure,
            @Optional Boolean httpOnly,
            @Optional CookieSameSite sameSite,
            @Optional Double expires,
            @Experimental @Optional CookiePriority priority,
            @Experimental @Optional Boolean sameParty,
            @Experimental @Optional CookieSourceScheme sourceScheme,
            @Experimental @Optional Integer sourcePort,
            @Experimental @Optional CookiePartitionKey partitionKey);

    /**
     * Sets Controls for third-party cookie access
     * Page reload is required before the new cookie bahavior will be observed
     *
     * @param enableThirdPartyCookieRestriction Whether 3pc restriction is enabled.
     * @param disableThirdPartyCookieMetadata Whether 3pc grace period exception should be enabled; false by default.
     * @param disableThirdPartyCookieHeuristics Whether 3pc heuristics exceptions should be enabled; false by default.
     */
    @Experimental
    void setCookieControls(
            Boolean enableThirdPartyCookieRestriction,
            Boolean disableThirdPartyCookieMetadata,
            Boolean disableThirdPartyCookieHeuristics);

    /**
     * Sets given cookies.
     *
     * @param cookies Cookies to be set.
     */
    void setCookies(List<CookieParam> cookies);

    /**
     * Specifies whether to always send extra HTTP headers with the requests from this page.
     *
     * @param headers Map with extra HTTP headers.
     */
    void setExtraHTTPHeaders(Map<String, Object> headers);

    /**
     * Sets the requests to intercept that match the provided patterns and optionally resource types.
     * Deprecated, please use Fetch.enable instead.
     *
     * @param patterns Requests matching any of these patterns will be forwarded and wait for the corresponding
     * continueInterceptedRequest call.
     */
    @Experimental
    @Deprecated
    void setRequestInterception(List<RequestPattern> patterns);

    /**
     * Allows overriding user agent with the given string.
     *
     * @param userAgent User agent to use.
     */
    void setUserAgentOverride(String userAgent);

    /**
     * Allows overriding user agent with the given string.
     *
     * @param userAgent User agent to use.
     * @param acceptLanguage Browser language to emulate.
     * @param platform The platform navigator.platform should return.
     * @param userAgentMetadata To be sent in Sec-CH-UA-* headers and returned in navigator.userAgentData
     */
    void setUserAgentOverride(
            String userAgent,
            @Optional String acceptLanguage,
            @Optional String platform,
            @Experimental @Optional UserAgentMetadata userAgentMetadata);

    /**
     * Enables streaming of the response for the given requestId.
     * If enabled, the dataReceived event contains the data that was received during streaming.
     *
     * @param requestId Identifier of the request to stream.
     *
     * @return Data that has been buffered until streaming is enabled. (Encoded as a base64 string when passed over JSON)
     */
    @Experimental
    String streamResourceContent(String requestId);

    /**
     * Returns a handle to the stream representing the response body. Note that after this command,
     * the intercepted request can't be continued as is -- you either need to cancel it or to provide
     * the response body. The stream only supports sequential read, IO.read will fail if the position
     * is specified.
     *
     */
    @Experimental
    String takeResponseBodyForInterceptionAsStream(String interceptionId);
}
