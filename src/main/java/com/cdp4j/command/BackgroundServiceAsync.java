// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.backgroundservice.ServiceName;
import java.util.concurrent.CompletableFuture;

/**
 * Defines events for background web platform features.
 */
@Experimental
public interface BackgroundServiceAsync extends ParameterizedCommand<BackgroundServiceAsync> {
    /**
     * Clears all stored data for the service.
     *
     */
    CompletableFuture<Void> clearEvents(ServiceName service);

    /**
     * Set the recording state for the service.
     *
     */
    CompletableFuture<Void> setRecording(Boolean shouldRecord, ServiceName service);

    /**
     * Enables event updates for the service.
     *
     */
    CompletableFuture<Void> startObserving(ServiceName service);

    /**
     * Disables event updates for the service.
     *
     */
    CompletableFuture<Void> stopObserving(ServiceName service);
}
