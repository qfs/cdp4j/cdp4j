// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

/**
 * EventBreakpoints permits setting JavaScript breakpoints on operations and
 * events occurring in native code invoked from JavaScript. Once breakpoint is
 * hit, it is reported through Debugger domain, similarly to regular breakpoints
 * being hit.
 */
@Experimental
public interface EventBreakpointsAsync extends ParameterizedCommand<EventBreakpointsAsync> {
    /**
     * Removes all breakpoints
     */
    CompletableFuture<Void> disable();

    /**
     * Removes breakpoint on particular native event.
     *
     * @param eventName
     *            Instrumentation name to stop on.
     */
    CompletableFuture<Void> removeInstrumentationBreakpoint(String eventName);

    /**
     * Sets breakpoint on particular native event.
     *
     * @param eventName
     *            Instrumentation name to stop on.
     */
    CompletableFuture<Void> setInstrumentationBreakpoint(String eventName);
}
