// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.audits.GenericIssueDetails;
import com.cdp4j.type.audits.GetEncodedResponseResult;
import com.cdp4j.type.constant.Encoding;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class AuditsAsyncImpl extends ParameterizedCommandImpl<AuditsAsync> implements AuditsAsync {

    private static final TypeReference<List<GenericIssueDetails>> LIST_GENERICISSUEDETAILS =
            new TypeReference<List<GenericIssueDetails>>() {};
    private static final CommandReturnType CRT_CHECK_CONTRAST = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CHECK_FORMS_ISSUES =
            new CommandReturnType("formIssues", List.class, LIST_GENERICISSUEDETAILS);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ENCODED_RESPONSE =
            new CommandReturnType(null, GetEncodedResponseResult.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CHECK_CONTRAST_2 = new String[] {"reportAAA"};
    private static final String[] PARAMS_GET_ENCODED_RESPONSE_1 = new String[] {"requestId", "encoding"};
    private static final String[] PARAMS_GET_ENCODED_RESPONSE_2 =
            new String[] {"requestId", "encoding", "quality", "sizeOnly"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public AuditsAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> checkContrast() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Audits_checkContrast, CRT_CHECK_CONTRAST, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> checkContrast(Boolean reportAAA) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Audits_checkContrast,
                CRT_CHECK_CONTRAST,
                PARAMS_CHECK_CONTRAST_2,
                new Object[] {reportAAA},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<GenericIssueDetails>> checkFormsIssues() {
        return (CompletableFuture<List<GenericIssueDetails>>) handler.invoke(
                this, DomainCommand.Audits_checkFormsIssues, CRT_CHECK_FORMS_ISSUES, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Audits_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Audits_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetEncodedResponseResult> getEncodedResponse(String requestId, Encoding encoding) {
        return (CompletableFuture<GetEncodedResponseResult>) handler.invoke(
                this,
                DomainCommand.Audits_getEncodedResponse,
                CRT_GET_ENCODED_RESPONSE,
                PARAMS_GET_ENCODED_RESPONSE_1,
                new Object[] {requestId, encoding},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetEncodedResponseResult> getEncodedResponse(
            String requestId, Encoding encoding, Double quality, Boolean sizeOnly) {
        return (CompletableFuture<GetEncodedResponseResult>) handler.invoke(
                this,
                DomainCommand.Audits_getEncodedResponse,
                CRT_GET_ENCODED_RESPONSE,
                PARAMS_GET_ENCODED_RESPONSE_2,
                new Object[] {requestId, encoding, quality, sizeOnly},
                false);
    }
}
