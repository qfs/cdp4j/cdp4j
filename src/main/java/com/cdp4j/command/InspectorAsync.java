// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface InspectorAsync extends ParameterizedCommand<InspectorAsync> {
    /**
     * Disables inspector domain notifications.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables inspector domain notifications.
     */
    CompletableFuture<Void> enable();
}
