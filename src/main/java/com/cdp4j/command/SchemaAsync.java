// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.schema.Domain;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This domain is deprecated.
 */
public interface SchemaAsync extends ParameterizedCommand<SchemaAsync> {
    /**
     * Returns supported domains.
     *
     * @return List of supported domains.
     */
    CompletableFuture<List<Domain>> getDomains();
}
