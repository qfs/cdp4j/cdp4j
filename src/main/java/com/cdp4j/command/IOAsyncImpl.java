// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.io.ReadResult;
import java.util.concurrent.CompletableFuture;

class IOAsyncImpl extends ParameterizedCommandImpl<IOAsync> implements IOAsync {

    private static final CommandReturnType CRT_CLOSE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_READ = new CommandReturnType(null, ReadResult.class, null);
    private static final CommandReturnType CRT_RESOLVE_BLOB = new CommandReturnType("uuid", String.class, null);
    private static final String[] PARAMS_CLOSE_1 = new String[] {"handle"};
    private static final String[] PARAMS_READ_1 = new String[] {"handle"};
    private static final String[] PARAMS_READ_2 = new String[] {"handle", "offset", "size"};
    private static final String[] PARAMS_RESOLVE_BLOB_1 = new String[] {"objectId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public IOAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> close(String handle) {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.IO_close, CRT_CLOSE, PARAMS_CLOSE_1, new Object[] {handle}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<ReadResult> read(String handle) {
        return (CompletableFuture<ReadResult>)
                handler.invoke(this, DomainCommand.IO_read, CRT_READ, PARAMS_READ_1, new Object[] {handle}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<ReadResult> read(String handle, Integer offset, Integer size) {
        return (CompletableFuture<ReadResult>) handler.invoke(
                this, DomainCommand.IO_read, CRT_READ, PARAMS_READ_2, new Object[] {handle, offset, size}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> resolveBlob(String objectId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.IO_resolveBlob,
                CRT_RESOLVE_BLOB,
                PARAMS_RESOLVE_BLOB_1,
                new Object[] {objectId},
                false);
    }
}
