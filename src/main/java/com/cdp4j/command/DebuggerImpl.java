// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.InstrumentationName;
import com.cdp4j.type.constant.PauseOnExceptionState;
import com.cdp4j.type.constant.RestartFrameMode;
import com.cdp4j.type.constant.TargetCallFrames;
import com.cdp4j.type.debugger.BreakLocation;
import com.cdp4j.type.debugger.DisassembleWasmModuleResult;
import com.cdp4j.type.debugger.EvaluateOnCallFrameResult;
import com.cdp4j.type.debugger.GetScriptSourceResult;
import com.cdp4j.type.debugger.Location;
import com.cdp4j.type.debugger.LocationRange;
import com.cdp4j.type.debugger.RestartFrameResult;
import com.cdp4j.type.debugger.ScriptPosition;
import com.cdp4j.type.debugger.SearchMatch;
import com.cdp4j.type.debugger.SetBreakpointByUrlResult;
import com.cdp4j.type.debugger.SetBreakpointResult;
import com.cdp4j.type.debugger.SetScriptSourceResult;
import com.cdp4j.type.debugger.WasmDisassemblyChunk;
import com.cdp4j.type.runtime.CallArgument;
import com.cdp4j.type.runtime.StackTrace;
import com.cdp4j.type.runtime.StackTraceId;
import java.util.List;

class DebuggerImpl extends ParameterizedCommandImpl<Debugger> implements Debugger {

    private static final TypeReference<List<BreakLocation>> LIST_BREAKLOCATION =
            new TypeReference<List<BreakLocation>>() {};
    private static final TypeReference<List<SearchMatch>> LIST_SEARCHMATCH = new TypeReference<List<SearchMatch>>() {};
    private static final CommandReturnType CRT_CONTINUE_TO_LOCATION = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISASSEMBLE_WASM_MODULE =
            new CommandReturnType(null, DisassembleWasmModuleResult.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType("debuggerId", String.class, null);
    private static final CommandReturnType CRT_EVALUATE_ON_CALL_FRAME =
            new CommandReturnType(null, EvaluateOnCallFrameResult.class, null);
    private static final CommandReturnType CRT_GET_POSSIBLE_BREAKPOINTS =
            new CommandReturnType("locations", List.class, LIST_BREAKLOCATION);
    private static final CommandReturnType CRT_GET_SCRIPT_SOURCE =
            new CommandReturnType(null, GetScriptSourceResult.class, null);
    private static final CommandReturnType CRT_GET_STACK_TRACE =
            new CommandReturnType("stackTrace", StackTrace.class, null);
    private static final CommandReturnType CRT_GET_WASM_BYTECODE =
            new CommandReturnType("bytecode", String.class, null);
    private static final CommandReturnType CRT_NEXT_WASM_DISASSEMBLY_CHUNK =
            new CommandReturnType("chunk", WasmDisassemblyChunk.class, null);
    private static final CommandReturnType CRT_PAUSE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_PAUSE_ON_ASYNC_CALL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_BREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RESTART_FRAME =
            new CommandReturnType(null, RestartFrameResult.class, null);
    private static final CommandReturnType CRT_RESUME = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SEARCH_IN_CONTENT =
            new CommandReturnType("result", List.class, LIST_SEARCHMATCH);
    private static final CommandReturnType CRT_SET_ASYNC_CALL_STACK_DEPTH =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BLACKBOXED_RANGES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BLACKBOX_EXECUTION_CONTEXTS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BLACKBOX_PATTERNS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BREAKPOINT =
            new CommandReturnType(null, SetBreakpointResult.class, null);
    private static final CommandReturnType CRT_SET_BREAKPOINTS_ACTIVE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BREAKPOINT_BY_URL =
            new CommandReturnType(null, SetBreakpointByUrlResult.class, null);
    private static final CommandReturnType CRT_SET_BREAKPOINT_ON_FUNCTION_CALL =
            new CommandReturnType("breakpointId", String.class, null);
    private static final CommandReturnType CRT_SET_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType("breakpointId", String.class, null);
    private static final CommandReturnType CRT_SET_PAUSE_ON_EXCEPTIONS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_RETURN_VALUE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SCRIPT_SOURCE =
            new CommandReturnType(null, SetScriptSourceResult.class, null);
    private static final CommandReturnType CRT_SET_SKIP_ALL_PAUSES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_VARIABLE_VALUE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STEP_INTO = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STEP_OUT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STEP_OVER = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CONTINUE_TO_LOCATION_1 = new String[] {"location"};
    private static final String[] PARAMS_CONTINUE_TO_LOCATION_2 = new String[] {"location", "targetCallFrames"};
    private static final String[] PARAMS_DISASSEMBLE_WASM_MODULE_1 = new String[] {"scriptId"};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"maxScriptsCacheSize"};
    private static final String[] PARAMS_EVALUATE_ON_CALL_FRAME_1 = new String[] {"callFrameId", "expression"};
    private static final String[] PARAMS_EVALUATE_ON_CALL_FRAME_2 = new String[] {
        "callFrameId",
        "expression",
        "objectGroup",
        "includeCommandLineAPI",
        "silent",
        "returnByValue",
        "generatePreview",
        "throwOnSideEffect",
        "timeout"
    };
    private static final String[] PARAMS_GET_POSSIBLE_BREAKPOINTS_1 = new String[] {"start"};
    private static final String[] PARAMS_GET_POSSIBLE_BREAKPOINTS_2 =
            new String[] {"start", "end", "restrictToFunction"};
    private static final String[] PARAMS_GET_SCRIPT_SOURCE_1 = new String[] {"scriptId"};
    private static final String[] PARAMS_GET_STACK_TRACE_1 = new String[] {"stackTraceId"};
    private static final String[] PARAMS_GET_WASM_BYTECODE_1 = new String[] {"scriptId"};
    private static final String[] PARAMS_NEXT_WASM_DISASSEMBLY_CHUNK_1 = new String[] {"streamId"};
    private static final String[] PARAMS_PAUSE_ON_ASYNC_CALL_1 = new String[] {"parentStackTraceId"};
    private static final String[] PARAMS_REMOVE_BREAKPOINT_1 = new String[] {"breakpointId"};
    private static final String[] PARAMS_RESTART_FRAME_1 = new String[] {"callFrameId"};
    private static final String[] PARAMS_RESTART_FRAME_2 = new String[] {"callFrameId", "mode"};
    private static final String[] PARAMS_RESUME_2 = new String[] {"terminateOnResume"};
    private static final String[] PARAMS_SEARCH_IN_CONTENT_1 = new String[] {"scriptId", "query"};
    private static final String[] PARAMS_SEARCH_IN_CONTENT_2 =
            new String[] {"scriptId", "query", "caseSensitive", "isRegex"};
    private static final String[] PARAMS_SET_ASYNC_CALL_STACK_DEPTH_1 = new String[] {"maxDepth"};
    private static final String[] PARAMS_SET_BLACKBOXED_RANGES_1 = new String[] {"scriptId", "positions"};
    private static final String[] PARAMS_SET_BLACKBOX_EXECUTION_CONTEXTS_1 = new String[] {"uniqueIds"};
    private static final String[] PARAMS_SET_BLACKBOX_PATTERNS_1 = new String[] {"patterns"};
    private static final String[] PARAMS_SET_BLACKBOX_PATTERNS_2 = new String[] {"patterns", "skipAnonymous"};
    private static final String[] PARAMS_SET_BREAKPOINTS_ACTIVE_1 = new String[] {"active"};
    private static final String[] PARAMS_SET_BREAKPOINT_1 = new String[] {"location"};
    private static final String[] PARAMS_SET_BREAKPOINT_2 = new String[] {"location", "condition"};
    private static final String[] PARAMS_SET_BREAKPOINT_BY_URL_1 = new String[] {"lineNumber"};
    private static final String[] PARAMS_SET_BREAKPOINT_BY_URL_2 =
            new String[] {"lineNumber", "url", "urlRegex", "scriptHash", "columnNumber", "condition"};
    private static final String[] PARAMS_SET_BREAKPOINT_ON_FUNCTION_CALL_1 = new String[] {"objectId"};
    private static final String[] PARAMS_SET_BREAKPOINT_ON_FUNCTION_CALL_2 = new String[] {"objectId", "condition"};
    private static final String[] PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"instrumentation"};
    private static final String[] PARAMS_SET_PAUSE_ON_EXCEPTIONS_1 = new String[] {"state"};
    private static final String[] PARAMS_SET_RETURN_VALUE_1 = new String[] {"newValue"};
    private static final String[] PARAMS_SET_SCRIPT_SOURCE_1 = new String[] {"scriptId", "scriptSource"};
    private static final String[] PARAMS_SET_SCRIPT_SOURCE_2 =
            new String[] {"scriptId", "scriptSource", "dryRun", "allowTopFrameEditing"};
    private static final String[] PARAMS_SET_SKIP_ALL_PAUSES_1 = new String[] {"skip"};
    private static final String[] PARAMS_SET_VARIABLE_VALUE_1 =
            new String[] {"scopeNumber", "variableName", "newValue", "callFrameId"};
    private static final String[] PARAMS_STEP_INTO_2 = new String[] {"breakOnAsyncCall", "skipList"};
    private static final String[] PARAMS_STEP_OVER_2 = new String[] {"skipList"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DebuggerImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void continueToLocation(Location location) {
        handler.invoke(
                this,
                DomainCommand.Debugger_continueToLocation,
                CRT_CONTINUE_TO_LOCATION,
                PARAMS_CONTINUE_TO_LOCATION_1,
                new Object[] {location},
                true);
    }

    @Override
    public void continueToLocation(Location location, TargetCallFrames targetCallFrames) {
        handler.invoke(
                this,
                DomainCommand.Debugger_continueToLocation,
                CRT_CONTINUE_TO_LOCATION,
                PARAMS_CONTINUE_TO_LOCATION_2,
                new Object[] {location, targetCallFrames},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Debugger_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public DisassembleWasmModuleResult disassembleWasmModule(String scriptId) {
        return (DisassembleWasmModuleResult) handler.invoke(
                this,
                DomainCommand.Debugger_disassembleWasmModule,
                CRT_DISASSEMBLE_WASM_MODULE,
                PARAMS_DISASSEMBLE_WASM_MODULE_1,
                new Object[] {scriptId},
                true);
    }

    @Override
    public String enable() {
        return (String) handler.invoke(this, DomainCommand.Debugger_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public String enable(Double maxScriptsCacheSize) {
        return (String) handler.invoke(
                this,
                DomainCommand.Debugger_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_2,
                new Object[] {maxScriptsCacheSize},
                true);
    }

    @Override
    public EvaluateOnCallFrameResult evaluateOnCallFrame(String callFrameId, String expression) {
        return (EvaluateOnCallFrameResult) handler.invoke(
                this,
                DomainCommand.Debugger_evaluateOnCallFrame,
                CRT_EVALUATE_ON_CALL_FRAME,
                PARAMS_EVALUATE_ON_CALL_FRAME_1,
                new Object[] {callFrameId, expression},
                true);
    }

    @Override
    public EvaluateOnCallFrameResult evaluateOnCallFrame(
            String callFrameId,
            String expression,
            String objectGroup,
            Boolean includeCommandLineAPI,
            Boolean silent,
            Boolean returnByValue,
            Boolean generatePreview,
            Boolean throwOnSideEffect,
            Double timeout) {
        return (EvaluateOnCallFrameResult) handler.invoke(
                this,
                DomainCommand.Debugger_evaluateOnCallFrame,
                CRT_EVALUATE_ON_CALL_FRAME,
                PARAMS_EVALUATE_ON_CALL_FRAME_2,
                new Object[] {
                    callFrameId,
                    expression,
                    objectGroup,
                    includeCommandLineAPI,
                    silent,
                    returnByValue,
                    generatePreview,
                    throwOnSideEffect,
                    timeout
                },
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<BreakLocation> getPossibleBreakpoints(Location start) {
        return (List<BreakLocation>) handler.invoke(
                this,
                DomainCommand.Debugger_getPossibleBreakpoints,
                CRT_GET_POSSIBLE_BREAKPOINTS,
                PARAMS_GET_POSSIBLE_BREAKPOINTS_1,
                new Object[] {start},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<BreakLocation> getPossibleBreakpoints(Location start, Location end, Boolean restrictToFunction) {
        return (List<BreakLocation>) handler.invoke(
                this,
                DomainCommand.Debugger_getPossibleBreakpoints,
                CRT_GET_POSSIBLE_BREAKPOINTS,
                PARAMS_GET_POSSIBLE_BREAKPOINTS_2,
                new Object[] {start, end, restrictToFunction},
                true);
    }

    @Override
    public GetScriptSourceResult getScriptSource(String scriptId) {
        return (GetScriptSourceResult) handler.invoke(
                this,
                DomainCommand.Debugger_getScriptSource,
                CRT_GET_SCRIPT_SOURCE,
                PARAMS_GET_SCRIPT_SOURCE_1,
                new Object[] {scriptId},
                true);
    }

    @Override
    public StackTrace getStackTrace(StackTraceId stackTraceId) {
        return (StackTrace) handler.invoke(
                this,
                DomainCommand.Debugger_getStackTrace,
                CRT_GET_STACK_TRACE,
                PARAMS_GET_STACK_TRACE_1,
                new Object[] {stackTraceId},
                true);
    }

    @Override
    public String getWasmBytecode(String scriptId) {
        return (String) handler.invoke(
                this,
                DomainCommand.Debugger_getWasmBytecode,
                CRT_GET_WASM_BYTECODE,
                PARAMS_GET_WASM_BYTECODE_1,
                new Object[] {scriptId},
                true);
    }

    @Override
    public WasmDisassemblyChunk nextWasmDisassemblyChunk(String streamId) {
        return (WasmDisassemblyChunk) handler.invoke(
                this,
                DomainCommand.Debugger_nextWasmDisassemblyChunk,
                CRT_NEXT_WASM_DISASSEMBLY_CHUNK,
                PARAMS_NEXT_WASM_DISASSEMBLY_CHUNK_1,
                new Object[] {streamId},
                true);
    }

    @Override
    public void pause() {
        handler.invoke(this, DomainCommand.Debugger_pause, CRT_PAUSE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void pauseOnAsyncCall(StackTraceId parentStackTraceId) {
        handler.invoke(
                this,
                DomainCommand.Debugger_pauseOnAsyncCall,
                CRT_PAUSE_ON_ASYNC_CALL,
                PARAMS_PAUSE_ON_ASYNC_CALL_1,
                new Object[] {parentStackTraceId},
                true);
    }

    @Override
    public void removeBreakpoint(String breakpointId) {
        handler.invoke(
                this,
                DomainCommand.Debugger_removeBreakpoint,
                CRT_REMOVE_BREAKPOINT,
                PARAMS_REMOVE_BREAKPOINT_1,
                new Object[] {breakpointId},
                true);
    }

    @Override
    public RestartFrameResult restartFrame(String callFrameId) {
        return (RestartFrameResult) handler.invoke(
                this,
                DomainCommand.Debugger_restartFrame,
                CRT_RESTART_FRAME,
                PARAMS_RESTART_FRAME_1,
                new Object[] {callFrameId},
                true);
    }

    @Override
    public RestartFrameResult restartFrame(String callFrameId, RestartFrameMode mode) {
        return (RestartFrameResult) handler.invoke(
                this,
                DomainCommand.Debugger_restartFrame,
                CRT_RESTART_FRAME,
                PARAMS_RESTART_FRAME_2,
                new Object[] {callFrameId, mode},
                true);
    }

    @Override
    public void resume() {
        handler.invoke(this, DomainCommand.Debugger_resume, CRT_RESUME, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void resume(Boolean terminateOnResume) {
        handler.invoke(
                this,
                DomainCommand.Debugger_resume,
                CRT_RESUME,
                PARAMS_RESUME_2,
                new Object[] {terminateOnResume},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<SearchMatch> searchInContent(String scriptId, String query) {
        return (List<SearchMatch>) handler.invoke(
                this,
                DomainCommand.Debugger_searchInContent,
                CRT_SEARCH_IN_CONTENT,
                PARAMS_SEARCH_IN_CONTENT_1,
                new Object[] {scriptId, query},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<SearchMatch> searchInContent(String scriptId, String query, Boolean caseSensitive, Boolean isRegex) {
        return (List<SearchMatch>) handler.invoke(
                this,
                DomainCommand.Debugger_searchInContent,
                CRT_SEARCH_IN_CONTENT,
                PARAMS_SEARCH_IN_CONTENT_2,
                new Object[] {scriptId, query, caseSensitive, isRegex},
                true);
    }

    @Override
    public void setAsyncCallStackDepth(Integer maxDepth) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setAsyncCallStackDepth,
                CRT_SET_ASYNC_CALL_STACK_DEPTH,
                PARAMS_SET_ASYNC_CALL_STACK_DEPTH_1,
                new Object[] {maxDepth},
                true);
    }

    @Override
    public void setBlackboxExecutionContexts(List<String> uniqueIds) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setBlackboxExecutionContexts,
                CRT_SET_BLACKBOX_EXECUTION_CONTEXTS,
                PARAMS_SET_BLACKBOX_EXECUTION_CONTEXTS_1,
                new Object[] {uniqueIds},
                true);
    }

    @Override
    public void setBlackboxPatterns(List<String> patterns) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setBlackboxPatterns,
                CRT_SET_BLACKBOX_PATTERNS,
                PARAMS_SET_BLACKBOX_PATTERNS_1,
                new Object[] {patterns},
                true);
    }

    @Override
    public void setBlackboxPatterns(List<String> patterns, Boolean skipAnonymous) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setBlackboxPatterns,
                CRT_SET_BLACKBOX_PATTERNS,
                PARAMS_SET_BLACKBOX_PATTERNS_2,
                new Object[] {patterns, skipAnonymous},
                true);
    }

    @Override
    public void setBlackboxedRanges(String scriptId, List<ScriptPosition> positions) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setBlackboxedRanges,
                CRT_SET_BLACKBOXED_RANGES,
                PARAMS_SET_BLACKBOXED_RANGES_1,
                new Object[] {scriptId, positions},
                true);
    }

    @Override
    public SetBreakpointResult setBreakpoint(Location location) {
        return (SetBreakpointResult) handler.invoke(
                this,
                DomainCommand.Debugger_setBreakpoint,
                CRT_SET_BREAKPOINT,
                PARAMS_SET_BREAKPOINT_1,
                new Object[] {location},
                true);
    }

    @Override
    public SetBreakpointResult setBreakpoint(Location location, String condition) {
        return (SetBreakpointResult) handler.invoke(
                this,
                DomainCommand.Debugger_setBreakpoint,
                CRT_SET_BREAKPOINT,
                PARAMS_SET_BREAKPOINT_2,
                new Object[] {location, condition},
                true);
    }

    @Override
    public SetBreakpointByUrlResult setBreakpointByUrl(Integer lineNumber) {
        return (SetBreakpointByUrlResult) handler.invoke(
                this,
                DomainCommand.Debugger_setBreakpointByUrl,
                CRT_SET_BREAKPOINT_BY_URL,
                PARAMS_SET_BREAKPOINT_BY_URL_1,
                new Object[] {lineNumber},
                true);
    }

    @Override
    public SetBreakpointByUrlResult setBreakpointByUrl(
            Integer lineNumber,
            String url,
            String urlRegex,
            String scriptHash,
            Integer columnNumber,
            String condition) {
        return (SetBreakpointByUrlResult) handler.invoke(
                this,
                DomainCommand.Debugger_setBreakpointByUrl,
                CRT_SET_BREAKPOINT_BY_URL,
                PARAMS_SET_BREAKPOINT_BY_URL_2,
                new Object[] {lineNumber, url, urlRegex, scriptHash, columnNumber, condition},
                true);
    }

    @Override
    public String setBreakpointOnFunctionCall(String objectId) {
        return (String) handler.invoke(
                this,
                DomainCommand.Debugger_setBreakpointOnFunctionCall,
                CRT_SET_BREAKPOINT_ON_FUNCTION_CALL,
                PARAMS_SET_BREAKPOINT_ON_FUNCTION_CALL_1,
                new Object[] {objectId},
                true);
    }

    @Override
    public String setBreakpointOnFunctionCall(String objectId, String condition) {
        return (String) handler.invoke(
                this,
                DomainCommand.Debugger_setBreakpointOnFunctionCall,
                CRT_SET_BREAKPOINT_ON_FUNCTION_CALL,
                PARAMS_SET_BREAKPOINT_ON_FUNCTION_CALL_2,
                new Object[] {objectId, condition},
                true);
    }

    @Override
    public void setBreakpointsActive(Boolean active) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setBreakpointsActive,
                CRT_SET_BREAKPOINTS_ACTIVE,
                PARAMS_SET_BREAKPOINTS_ACTIVE_1,
                new Object[] {active},
                true);
    }

    @Override
    public String setInstrumentationBreakpoint(InstrumentationName instrumentation) {
        return (String) handler.invoke(
                this,
                DomainCommand.Debugger_setInstrumentationBreakpoint,
                CRT_SET_INSTRUMENTATION_BREAKPOINT,
                PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {instrumentation},
                true);
    }

    @Override
    public void setPauseOnExceptions(PauseOnExceptionState state) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setPauseOnExceptions,
                CRT_SET_PAUSE_ON_EXCEPTIONS,
                PARAMS_SET_PAUSE_ON_EXCEPTIONS_1,
                new Object[] {state},
                true);
    }

    @Override
    public void setReturnValue(CallArgument newValue) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setReturnValue,
                CRT_SET_RETURN_VALUE,
                PARAMS_SET_RETURN_VALUE_1,
                new Object[] {newValue},
                true);
    }

    @Override
    public SetScriptSourceResult setScriptSource(String scriptId, String scriptSource) {
        return (SetScriptSourceResult) handler.invoke(
                this,
                DomainCommand.Debugger_setScriptSource,
                CRT_SET_SCRIPT_SOURCE,
                PARAMS_SET_SCRIPT_SOURCE_1,
                new Object[] {scriptId, scriptSource},
                true);
    }

    @Override
    public SetScriptSourceResult setScriptSource(
            String scriptId, String scriptSource, Boolean dryRun, Boolean allowTopFrameEditing) {
        return (SetScriptSourceResult) handler.invoke(
                this,
                DomainCommand.Debugger_setScriptSource,
                CRT_SET_SCRIPT_SOURCE,
                PARAMS_SET_SCRIPT_SOURCE_2,
                new Object[] {scriptId, scriptSource, dryRun, allowTopFrameEditing},
                true);
    }

    @Override
    public void setSkipAllPauses(Boolean skip) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setSkipAllPauses,
                CRT_SET_SKIP_ALL_PAUSES,
                PARAMS_SET_SKIP_ALL_PAUSES_1,
                new Object[] {skip},
                true);
    }

    @Override
    public void setVariableValue(Integer scopeNumber, String variableName, CallArgument newValue, String callFrameId) {
        handler.invoke(
                this,
                DomainCommand.Debugger_setVariableValue,
                CRT_SET_VARIABLE_VALUE,
                PARAMS_SET_VARIABLE_VALUE_1,
                new Object[] {scopeNumber, variableName, newValue, callFrameId},
                true);
    }

    @Override
    public void stepInto() {
        handler.invoke(this, DomainCommand.Debugger_stepInto, CRT_STEP_INTO, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void stepInto(Boolean breakOnAsyncCall, List<LocationRange> skipList) {
        handler.invoke(
                this,
                DomainCommand.Debugger_stepInto,
                CRT_STEP_INTO,
                PARAMS_STEP_INTO_2,
                new Object[] {breakOnAsyncCall, skipList},
                true);
    }

    @Override
    public void stepOut() {
        handler.invoke(this, DomainCommand.Debugger_stepOut, CRT_STEP_OUT, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void stepOver() {
        handler.invoke(this, DomainCommand.Debugger_stepOver, CRT_STEP_OVER, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void stepOver(List<LocationRange> skipList) {
        handler.invoke(
                this,
                DomainCommand.Debugger_stepOver,
                CRT_STEP_OVER,
                PARAMS_STEP_OVER_2,
                new Object[] {skipList},
                true);
    }
}
