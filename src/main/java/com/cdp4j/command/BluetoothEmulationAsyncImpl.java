// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.bluetoothemulation.CentralState;
import com.cdp4j.type.bluetoothemulation.ManufacturerData;
import com.cdp4j.type.bluetoothemulation.ScanEntry;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class BluetoothEmulationAsyncImpl extends ParameterizedCommandImpl<BluetoothEmulationAsync>
        implements BluetoothEmulationAsync {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SIMULATE_ADVERTISEMENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SIMULATE_PRECONNECTED_PERIPHERAL =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ENABLE_1 = new String[] {"state"};
    private static final String[] PARAMS_SIMULATE_ADVERTISEMENT_1 = new String[] {"entry"};
    private static final String[] PARAMS_SIMULATE_PRECONNECTED_PERIPHERAL_1 =
            new String[] {"address", "name", "manufacturerData", "knownServiceUuids"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public BluetoothEmulationAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.BluetoothEmulation_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable(CentralState state) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.BluetoothEmulation_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_1,
                new Object[] {state},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> simulateAdvertisement(ScanEntry entry) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.BluetoothEmulation_simulateAdvertisement,
                CRT_SIMULATE_ADVERTISEMENT,
                PARAMS_SIMULATE_ADVERTISEMENT_1,
                new Object[] {entry},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> simulatePreconnectedPeripheral(
            String address, String name, List<ManufacturerData> manufacturerData, List<String> knownServiceUuids) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.BluetoothEmulation_simulatePreconnectedPeripheral,
                CRT_SIMULATE_PRECONNECTED_PERIPHERAL,
                PARAMS_SIMULATE_PRECONNECTED_PERIPHERAL_1,
                new Object[] {address, name, manufacturerData, knownServiceUuids},
                false);
    }
}
