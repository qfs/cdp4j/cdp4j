// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.memory.DOMCounter;
import com.cdp4j.type.memory.GetDOMCountersResult;
import com.cdp4j.type.memory.PressureLevel;
import com.cdp4j.type.memory.SamplingProfile;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class MemoryAsyncImpl extends ParameterizedCommandImpl<MemoryAsync> implements MemoryAsync {

    private static final TypeReference<List<DOMCounter>> LIST_DOMCOUNTER = new TypeReference<List<DOMCounter>>() {};
    private static final CommandReturnType CRT_FORCIBLY_PURGE_JAVA_SCRIPT_MEMORY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ALL_TIME_SAMPLING_PROFILE =
            new CommandReturnType("profile", SamplingProfile.class, null);
    private static final CommandReturnType CRT_GET_BROWSER_SAMPLING_PROFILE =
            new CommandReturnType("profile", SamplingProfile.class, null);
    private static final CommandReturnType CRT_GET_DO_MCOUNTERS =
            new CommandReturnType(null, GetDOMCountersResult.class, null);
    private static final CommandReturnType CRT_GET_DO_MCOUNTERS_FOR_LEAK_DETECTION =
            new CommandReturnType("counters", List.class, LIST_DOMCOUNTER);
    private static final CommandReturnType CRT_GET_SAMPLING_PROFILE =
            new CommandReturnType("profile", SamplingProfile.class, null);
    private static final CommandReturnType CRT_PREPARE_FOR_LEAK_DETECTION =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PRESSURE_NOTIFICATIONS_SUPPRESSED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SIMULATE_PRESSURE_NOTIFICATION =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_SAMPLING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_SAMPLING = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_SET_PRESSURE_NOTIFICATIONS_SUPPRESSED_1 = new String[] {"suppressed"};
    private static final String[] PARAMS_SIMULATE_PRESSURE_NOTIFICATION_1 = new String[] {"level"};
    private static final String[] PARAMS_START_SAMPLING_2 = new String[] {"samplingInterval", "suppressRandomness"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public MemoryAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> forciblyPurgeJavaScriptMemory() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Memory_forciblyPurgeJavaScriptMemory,
                CRT_FORCIBLY_PURGE_JAVA_SCRIPT_MEMORY,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SamplingProfile> getAllTimeSamplingProfile() {
        return (CompletableFuture<SamplingProfile>) handler.invoke(
                this,
                DomainCommand.Memory_getAllTimeSamplingProfile,
                CRT_GET_ALL_TIME_SAMPLING_PROFILE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SamplingProfile> getBrowserSamplingProfile() {
        return (CompletableFuture<SamplingProfile>) handler.invoke(
                this,
                DomainCommand.Memory_getBrowserSamplingProfile,
                CRT_GET_BROWSER_SAMPLING_PROFILE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetDOMCountersResult> getDOMCounters() {
        return (CompletableFuture<GetDOMCountersResult>) handler.invoke(
                this, DomainCommand.Memory_getDOMCounters, CRT_GET_DO_MCOUNTERS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<DOMCounter>> getDOMCountersForLeakDetection() {
        return (CompletableFuture<List<DOMCounter>>) handler.invoke(
                this,
                DomainCommand.Memory_getDOMCountersForLeakDetection,
                CRT_GET_DO_MCOUNTERS_FOR_LEAK_DETECTION,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SamplingProfile> getSamplingProfile() {
        return (CompletableFuture<SamplingProfile>) handler.invoke(
                this,
                DomainCommand.Memory_getSamplingProfile,
                CRT_GET_SAMPLING_PROFILE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> prepareForLeakDetection() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Memory_prepareForLeakDetection,
                CRT_PREPARE_FOR_LEAK_DETECTION,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPressureNotificationsSuppressed(Boolean suppressed) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Memory_setPressureNotificationsSuppressed,
                CRT_SET_PRESSURE_NOTIFICATIONS_SUPPRESSED,
                PARAMS_SET_PRESSURE_NOTIFICATIONS_SUPPRESSED_1,
                new Object[] {suppressed},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> simulatePressureNotification(PressureLevel level) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Memory_simulatePressureNotification,
                CRT_SIMULATE_PRESSURE_NOTIFICATION,
                PARAMS_SIMULATE_PRESSURE_NOTIFICATION_1,
                new Object[] {level},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startSampling() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Memory_startSampling, CRT_START_SAMPLING, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startSampling(Integer samplingInterval, Boolean suppressRandomness) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Memory_startSampling,
                CRT_START_SAMPLING,
                PARAMS_START_SAMPLING_2,
                new Object[] {samplingInterval, suppressRandomness},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopSampling() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Memory_stopSampling, CRT_STOP_SAMPLING, EMPTY_ARGS, EMPTY_VALUES, false);
    }
}
