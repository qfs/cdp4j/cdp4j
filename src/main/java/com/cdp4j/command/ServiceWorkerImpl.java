// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;

class ServiceWorkerImpl extends ParameterizedCommandImpl<ServiceWorker> implements ServiceWorker {

    private static final CommandReturnType CRT_DELIVER_PUSH_MESSAGE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_PERIODIC_SYNC_EVENT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_SYNC_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_INSPECT_WORKER = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_FORCE_UPDATE_ON_PAGE_LOAD =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SKIP_WAITING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_WORKER = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_ALL_WORKERS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_WORKER = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNREGISTER = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UPDATE_REGISTRATION = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_DELIVER_PUSH_MESSAGE_1 = new String[] {"origin", "registrationId", "data"};
    private static final String[] PARAMS_DISPATCH_PERIODIC_SYNC_EVENT_1 =
            new String[] {"origin", "registrationId", "tag"};
    private static final String[] PARAMS_DISPATCH_SYNC_EVENT_1 =
            new String[] {"origin", "registrationId", "tag", "lastChance"};
    private static final String[] PARAMS_INSPECT_WORKER_1 = new String[] {"versionId"};
    private static final String[] PARAMS_SET_FORCE_UPDATE_ON_PAGE_LOAD_1 = new String[] {"forceUpdateOnPageLoad"};
    private static final String[] PARAMS_SKIP_WAITING_1 = new String[] {"scopeURL"};
    private static final String[] PARAMS_START_WORKER_1 = new String[] {"scopeURL"};
    private static final String[] PARAMS_STOP_WORKER_1 = new String[] {"versionId"};
    private static final String[] PARAMS_UNREGISTER_1 = new String[] {"scopeURL"};
    private static final String[] PARAMS_UPDATE_REGISTRATION_1 = new String[] {"scopeURL"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public ServiceWorkerImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void deliverPushMessage(String origin, String registrationId, String data) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_deliverPushMessage,
                CRT_DELIVER_PUSH_MESSAGE,
                PARAMS_DELIVER_PUSH_MESSAGE_1,
                new Object[] {origin, registrationId, data},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.ServiceWorker_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void dispatchPeriodicSyncEvent(String origin, String registrationId, String tag) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_dispatchPeriodicSyncEvent,
                CRT_DISPATCH_PERIODIC_SYNC_EVENT,
                PARAMS_DISPATCH_PERIODIC_SYNC_EVENT_1,
                new Object[] {origin, registrationId, tag},
                true);
    }

    @Override
    public void dispatchSyncEvent(String origin, String registrationId, String tag, Boolean lastChance) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_dispatchSyncEvent,
                CRT_DISPATCH_SYNC_EVENT,
                PARAMS_DISPATCH_SYNC_EVENT_1,
                new Object[] {origin, registrationId, tag, lastChance},
                true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.ServiceWorker_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void inspectWorker(String versionId) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_inspectWorker,
                CRT_INSPECT_WORKER,
                PARAMS_INSPECT_WORKER_1,
                new Object[] {versionId},
                true);
    }

    @Override
    public void setForceUpdateOnPageLoad(Boolean forceUpdateOnPageLoad) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_setForceUpdateOnPageLoad,
                CRT_SET_FORCE_UPDATE_ON_PAGE_LOAD,
                PARAMS_SET_FORCE_UPDATE_ON_PAGE_LOAD_1,
                new Object[] {forceUpdateOnPageLoad},
                true);
    }

    @Override
    public void skipWaiting(String scopeURL) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_skipWaiting,
                CRT_SKIP_WAITING,
                PARAMS_SKIP_WAITING_1,
                new Object[] {scopeURL},
                true);
    }

    @Override
    public void startWorker(String scopeURL) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_startWorker,
                CRT_START_WORKER,
                PARAMS_START_WORKER_1,
                new Object[] {scopeURL},
                true);
    }

    @Override
    public void stopAllWorkers() {
        handler.invoke(
                this, DomainCommand.ServiceWorker_stopAllWorkers, CRT_STOP_ALL_WORKERS, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void stopWorker(String versionId) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_stopWorker,
                CRT_STOP_WORKER,
                PARAMS_STOP_WORKER_1,
                new Object[] {versionId},
                true);
    }

    @Override
    public void unregister(String scopeURL) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_unregister,
                CRT_UNREGISTER,
                PARAMS_UNREGISTER_1,
                new Object[] {scopeURL},
                true);
    }

    @Override
    public void updateRegistration(String scopeURL) {
        handler.invoke(
                this,
                DomainCommand.ServiceWorker_updateRegistration,
                CRT_UPDATE_REGISTRATION,
                PARAMS_UPDATE_REGISTRATION_1,
                new Object[] {scopeURL},
                true);
    }
}
