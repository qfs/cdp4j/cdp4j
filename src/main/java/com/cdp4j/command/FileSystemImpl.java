// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.filesystem.BucketFileSystemLocator;
import com.cdp4j.type.filesystem.Directory;

class FileSystemImpl extends ParameterizedCommandImpl<FileSystem> implements FileSystem {

    private static final CommandReturnType CRT_GET_DIRECTORY =
            new CommandReturnType("directory", Directory.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_DIRECTORY_1 = new String[] {"bucketFileSystemLocator"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public FileSystemImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public Directory getDirectory(BucketFileSystemLocator bucketFileSystemLocator) {
        return (Directory) handler.invoke(
                this,
                DomainCommand.FileSystem_getDirectory,
                CRT_GET_DIRECTORY,
                PARAMS_GET_DIRECTORY_1,
                new Object[] {bucketFileSystemLocator},
                true);
    }
}
