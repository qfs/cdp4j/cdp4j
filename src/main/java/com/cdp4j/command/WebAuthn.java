// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.webauthn.Credential;
import com.cdp4j.type.webauthn.VirtualAuthenticatorOptions;
import java.util.List;

/**
 * This domain allows configuring virtual authenticators to test the WebAuthn
 * API.
 */
@Experimental
public interface WebAuthn extends ParameterizedCommand<WebAuthn> {
    /**
     * Adds the credential to the specified authenticator.
     *
     */
    void addCredential(String authenticatorId, Credential credential);

    /**
     * Creates and adds a virtual authenticator.
     *
     */
    String addVirtualAuthenticator(VirtualAuthenticatorOptions options);

    /**
     * Clears all the credentials from the specified device.
     *
     */
    void clearCredentials(String authenticatorId);

    /**
     * Disable the WebAuthn domain.
     */
    void disable();

    /**
     * Enable the WebAuthn domain and start intercepting credential storage and
     * retrieval with a virtual authenticator.
     */
    void enable();

    /**
     * Enable the WebAuthn domain and start intercepting credential storage and
     * retrieval with a virtual authenticator.
     *
     * @param enableUI Whether to enable the WebAuthn user interface. Enabling the UI is
     * recommended for debugging and demo purposes, as it is closer to the real
     * experience. Disabling the UI is recommended for automated testing.
     * Supported at the embedder's discretion if UI is available.
     * Defaults to false.
     */
    void enable(@Optional Boolean enableUI);

    /**
     * Returns a single credential stored in the given virtual authenticator that
     * matches the credential ID.
     *
     */
    Credential getCredential(String authenticatorId, String credentialId);

    /**
     * Returns all the credentials stored in the given virtual authenticator.
     *
     */
    List<Credential> getCredentials(String authenticatorId);

    /**
     * Removes a credential from the authenticator.
     *
     */
    void removeCredential(String authenticatorId, String credentialId);

    /**
     * Removes the given authenticator.
     *
     */
    void removeVirtualAuthenticator(String authenticatorId);

    /**
     * Sets whether tests of user presence will succeed immediately (if true) or fail to resolve (if false) for an authenticator.
     * The default is true.
     *
     */
    void setAutomaticPresenceSimulation(String authenticatorId, Boolean enabled);

    /**
     * Allows setting credential properties.
     * https://w3c.github.io/webauthn/#sctn-automation-set-credential-properties
     *
     */
    void setCredentialProperties(String authenticatorId, String credentialId);

    /**
     * Allows setting credential properties.
     * https://w3c.github.io/webauthn/#sctn-automation-set-credential-properties
     *
     */
    void setCredentialProperties(
            String authenticatorId,
            String credentialId,
            @Optional Boolean backupEligibility,
            @Optional Boolean backupState);

    /**
     * Resets parameters isBogusSignature, isBadUV, isBadUP to false if they are not present.
     *
     */
    void setResponseOverrideBits(String authenticatorId);

    /**
     * Resets parameters isBogusSignature, isBadUV, isBadUP to false if they are not present.
     *
     * @param isBogusSignature If isBogusSignature is set, overrides the signature in the authenticator response to be zero.
     * Defaults to false.
     * @param isBadUV If isBadUV is set, overrides the UV bit in the flags in the authenticator response to
     * be zero. Defaults to false.
     * @param isBadUP If isBadUP is set, overrides the UP bit in the flags in the authenticator response to
     * be zero. Defaults to false.
     */
    void setResponseOverrideBits(
            String authenticatorId,
            @Optional Boolean isBogusSignature,
            @Optional Boolean isBadUV,
            @Optional Boolean isBadUP);

    /**
     * Sets whether User Verification succeeds or fails for an authenticator.
     * The default is true.
     *
     */
    void setUserVerified(String authenticatorId, Boolean isUserVerified);
}
