// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.cachestorage.Cache;
import com.cdp4j.type.cachestorage.CachedResponse;
import com.cdp4j.type.cachestorage.Header;
import com.cdp4j.type.cachestorage.RequestEntriesResult;
import com.cdp4j.type.storage.StorageBucket;
import java.util.List;

@Experimental
public interface CacheStorage extends ParameterizedCommand<CacheStorage> {
    /**
     * Deletes a cache.
     *
     * @param cacheId Id of cache for deletion.
     */
    void deleteCache(String cacheId);

    /**
     * Deletes a cache entry.
     *
     * @param cacheId Id of cache where the entry will be deleted.
     * @param request URL spec of the request.
     */
    void deleteEntry(String cacheId, String request);

    /**
     * Requests cache names.
     *
     * @return Caches for the security origin.
     */
    List<Cache> requestCacheNames();

    /**
     * Requests cache names.
     *
     * @param securityOrigin At least and at most one of securityOrigin, storageKey, storageBucket must be specified.
     * Security origin.
     * @param storageKey Storage key.
     * @param storageBucket Storage bucket. If not specified, it uses the default bucket.
     *
     * @return Caches for the security origin.
     */
    List<Cache> requestCacheNames(
            @Optional String securityOrigin, @Optional String storageKey, @Optional StorageBucket storageBucket);

    /**
     * Fetches cache entry.
     *
     * @param cacheId Id of cache that contains the entry.
     * @param requestURL URL spec of the request.
     * @param requestHeaders headers of the request.
     *
     * @return Response read from the cache.
     */
    CachedResponse requestCachedResponse(String cacheId, String requestURL, List<Header> requestHeaders);

    /**
     * Requests data from cache.
     *
     * @param cacheId ID of cache to get entries from.
     *
     * @return RequestEntriesResult
     */
    RequestEntriesResult requestEntries(String cacheId);

    /**
     * Requests data from cache.
     *
     * @param cacheId ID of cache to get entries from.
     * @param skipCount Number of records to skip.
     * @param pageSize Number of records to fetch.
     * @param pathFilter If present, only return the entries containing this substring in the path
     *
     * @return RequestEntriesResult
     */
    RequestEntriesResult requestEntries(
            String cacheId, @Optional Integer skipCount, @Optional Integer pageSize, @Optional String pathFilter);
}
