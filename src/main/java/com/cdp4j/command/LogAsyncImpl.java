// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.log.ViolationSetting;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class LogAsyncImpl extends ParameterizedCommandImpl<LogAsync> implements LogAsync {

    private static final CommandReturnType CRT_CLEAR = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_VIOLATIONS_REPORT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_VIOLATIONS_REPORT = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_START_VIOLATIONS_REPORT_1 = new String[] {"config"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public LogAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clear() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Log_clear, CRT_CLEAR, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Log_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Log_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startViolationsReport(List<ViolationSetting> config) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Log_startViolationsReport,
                CRT_START_VIOLATIONS_REPORT,
                PARAMS_START_VIOLATIONS_REPORT_1,
                new Object[] {config},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopViolationsReport() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Log_stopViolationsReport,
                CRT_STOP_VIOLATIONS_REPORT,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }
}
