// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.log.ViolationSetting;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Provides access to log entries.
 */
public interface LogAsync extends ParameterizedCommand<LogAsync> {
    /**
     * Clears the log.
     */
    CompletableFuture<Void> clear();

    /**
     * Disables log domain, prevents further log entries from being reported to the
     * client.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables log domain, sends the entries collected so far to the client by means
     * of the entryAdded notification.
     */
    CompletableFuture<Void> enable();

    /**
     * start violation reporting.
     *
     * @param config
     *            Configuration for violations.
     */
    CompletableFuture<Void> startViolationsReport(List<ViolationSetting> config);

    /**
     * Stop violation reporting.
     */
    CompletableFuture<Void> stopViolationsReport();
}
