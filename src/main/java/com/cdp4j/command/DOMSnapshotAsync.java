// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.domsnapshot.CaptureSnapshotResult;
import com.cdp4j.type.domsnapshot.GetSnapshotResult;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This domain facilitates obtaining document snapshots with DOM, layout, and
 * style information.
 */
@Experimental
public interface DOMSnapshotAsync extends ParameterizedCommand<DOMSnapshotAsync> {
    /**
     * Returns a document snapshot, including the full DOM tree of the root node
     * (including iframes, template contents, and imported documents) in a flattened
     * array, as well as layout and white-listed computed style information for the
     * nodes. Shadow DOM in the returned DOM tree is flattened.
     *
     * @param computedStyles
     *            Whitelist of computed styles to return.
     *
     * @return CaptureSnapshotResult
     */
    CompletableFuture<CaptureSnapshotResult> captureSnapshot(List<String> computedStyles);

    /**
     * Returns a document snapshot, including the full DOM tree of the root node
     * (including iframes, template contents, and imported documents) in a flattened
     * array, as well as layout and white-listed computed style information for the
     * nodes. Shadow DOM in the returned DOM tree is flattened.
     *
     * @param computedStyles
     *            Whitelist of computed styles to return.
     * @param includePaintOrder
     *            Whether to include layout object paint orders into the snapshot.
     * @param includeDOMRects
     *            Whether to include DOM rectangles (offsetRects, clientRects,
     *            scrollRects) into the snapshot
     * @param includeBlendedBackgroundColors
     *            Whether to include blended background colors in the snapshot
     *            (default: false). Blended background color is achieved by blending
     *            background colors of all elements that overlap with the current
     *            element.
     * @param includeTextColorOpacities
     *            Whether to include text color opacity in the snapshot (default:
     *            false). An element might have the opacity property set that
     *            affects the text color of the element. The final text color
     *            opacity is computed based on the opacity of all overlapping
     *            elements.
     *
     * @return CaptureSnapshotResult
     */
    CompletableFuture<CaptureSnapshotResult> captureSnapshot(
            List<String> computedStyles,
            @Optional Boolean includePaintOrder,
            @Optional Boolean includeDOMRects,
            @Experimental @Optional Boolean includeBlendedBackgroundColors,
            @Experimental @Optional Boolean includeTextColorOpacities);

    /**
     * Disables DOM snapshot agent for the given page.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables DOM snapshot agent for the given page.
     */
    CompletableFuture<Void> enable();

    /**
     * Returns a document snapshot, including the full DOM tree of the root node
     * (including iframes, template contents, and imported documents) in a flattened
     * array, as well as layout and white-listed computed style information for the
     * nodes. Shadow DOM in the returned DOM tree is flattened.
     *
     * @param computedStyleWhitelist
     *            Whitelist of computed styles to return.
     *
     * @return GetSnapshotResult
     */
    @Deprecated
    CompletableFuture<GetSnapshotResult> getSnapshot(List<String> computedStyleWhitelist);

    /**
     * Returns a document snapshot, including the full DOM tree of the root node
     * (including iframes, template contents, and imported documents) in a flattened
     * array, as well as layout and white-listed computed style information for the
     * nodes. Shadow DOM in the returned DOM tree is flattened.
     *
     * @param computedStyleWhitelist
     *            Whitelist of computed styles to return.
     * @param includeEventListeners
     *            Whether or not to retrieve details of DOM listeners (default
     *            false).
     * @param includePaintOrder
     *            Whether to determine and include the paint order index of
     *            LayoutTreeNodes (default false).
     * @param includeUserAgentShadowTree
     *            Whether to include UA shadow tree in the snapshot (default false).
     *
     * @return GetSnapshotResult
     */
    @Deprecated
    CompletableFuture<GetSnapshotResult> getSnapshot(
            List<String> computedStyleWhitelist,
            @Optional Boolean includeEventListeners,
            @Optional Boolean includePaintOrder,
            @Optional Boolean includeUserAgentShadowTree);
}
