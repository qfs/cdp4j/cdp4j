// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

/**
 * The Tethering domain defines methods and events for browser port binding.
 */
@Experimental
public interface TetheringAsync extends ParameterizedCommand<TetheringAsync> {
    /**
     * Request browser port binding.
     *
     * @param port
     *            Port number to bind.
     */
    CompletableFuture<Void> bind(Integer port);

    /**
     * Request browser port unbinding.
     *
     * @param port
     *            Port number to unbind.
     */
    CompletableFuture<Void> unbind(Integer port);
}
