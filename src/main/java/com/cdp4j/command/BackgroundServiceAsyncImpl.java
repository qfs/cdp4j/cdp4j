// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.backgroundservice.ServiceName;
import java.util.concurrent.CompletableFuture;

class BackgroundServiceAsyncImpl extends ParameterizedCommandImpl<BackgroundServiceAsync>
        implements BackgroundServiceAsync {

    private static final CommandReturnType CRT_CLEAR_EVENTS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_RECORDING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_OBSERVING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_OBSERVING = new CommandReturnType(null, void.class, null);
    private static final String[] PARAMS_CLEAR_EVENTS_1 = new String[] {"service"};
    private static final String[] PARAMS_SET_RECORDING_1 = new String[] {"shouldRecord", "service"};
    private static final String[] PARAMS_START_OBSERVING_1 = new String[] {"service"};
    private static final String[] PARAMS_STOP_OBSERVING_1 = new String[] {"service"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public BackgroundServiceAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearEvents(ServiceName service) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.BackgroundService_clearEvents,
                CRT_CLEAR_EVENTS,
                PARAMS_CLEAR_EVENTS_1,
                new Object[] {service},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setRecording(Boolean shouldRecord, ServiceName service) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.BackgroundService_setRecording,
                CRT_SET_RECORDING,
                PARAMS_SET_RECORDING_1,
                new Object[] {shouldRecord, service},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startObserving(ServiceName service) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.BackgroundService_startObserving,
                CRT_START_OBSERVING,
                PARAMS_START_OBSERVING_1,
                new Object[] {service},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopObserving(ServiceName service) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.BackgroundService_stopObserving,
                CRT_STOP_OBSERVING,
                PARAMS_STOP_OBSERVING_1,
                new Object[] {service},
                false);
    }
}
