// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.runtime.RemoteObject;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface AnimationAsync extends ParameterizedCommand<AnimationAsync> {
    /**
     * Disables animation domain notifications.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables animation domain notifications.
     */
    CompletableFuture<Void> enable();

    /**
     * Returns the current time of the an animation.
     *
     * @param id
     *            Id of animation.
     *
     * @return Current time of the page.
     */
    CompletableFuture<Double> getCurrentTime(String id);

    /**
     * Gets the playback rate of the document timeline.
     *
     * @return Playback rate for animations on page.
     */
    CompletableFuture<Double> getPlaybackRate();

    /**
     * Releases a set of animations to no longer be manipulated.
     *
     * @param animations
     *            List of animation ids to seek.
     */
    CompletableFuture<Void> releaseAnimations(List<String> animations);

    /**
     * Gets the remote object of the Animation.
     *
     * @param animationId
     *            Animation id.
     *
     * @return Corresponding remote object.
     */
    CompletableFuture<RemoteObject> resolveAnimation(String animationId);

    /**
     * Seek a set of animations to a particular time within each animation.
     *
     * @param animations
     *            List of animation ids to seek.
     * @param currentTime
     *            Set the current time of each animation.
     */
    CompletableFuture<Void> seekAnimations(List<String> animations, Double currentTime);

    /**
     * Sets the paused state of a set of animations.
     *
     * @param animations
     *            Animations to set the pause state of.
     * @param paused
     *            Paused state to set to.
     */
    CompletableFuture<Void> setPaused(List<String> animations, Boolean paused);

    /**
     * Sets the playback rate of the document timeline.
     *
     * @param playbackRate
     *            Playback rate for animations on page
     */
    CompletableFuture<Void> setPlaybackRate(Double playbackRate);

    /**
     * Sets the timing of an animation node.
     *
     * @param animationId
     *            Animation id.
     * @param duration
     *            Duration of the animation.
     * @param delay
     *            Delay of the animation.
     */
    CompletableFuture<Void> setTiming(String animationId, Double duration, Double delay);
}
