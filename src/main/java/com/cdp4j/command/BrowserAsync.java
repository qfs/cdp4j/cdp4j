// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.browser.Bounds;
import com.cdp4j.type.browser.BrowserCommandId;
import com.cdp4j.type.browser.GetVersionResult;
import com.cdp4j.type.browser.GetWindowForTargetResult;
import com.cdp4j.type.browser.Histogram;
import com.cdp4j.type.browser.PermissionDescriptor;
import com.cdp4j.type.browser.PermissionSetting;
import com.cdp4j.type.browser.PermissionType;
import com.cdp4j.type.constant.DownloadBehavior;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * The Browser domain defines methods and events for browser managing.
 */
public interface BrowserAsync extends ParameterizedCommand<BrowserAsync> {
    /**
     * Allows a site to use privacy sandbox features that require enrollment without
     * the site actually being enrolled. Only supported on page targets.
     *
     */
    CompletableFuture<Void> addPrivacySandboxEnrollmentOverride(String url);

    /**
     * Cancel a download if in progress
     *
     * @param guid
     *            Global unique identifier of the download.
     */
    @Experimental
    CompletableFuture<Void> cancelDownload(String guid);

    /**
     * Cancel a download if in progress
     *
     * @param guid
     *            Global unique identifier of the download.
     * @param browserContextId
     *            BrowserContext to perform the action in. When omitted, default
     *            browser context is used.
     */
    @Experimental
    CompletableFuture<Void> cancelDownload(String guid, @Optional String browserContextId);

    /**
     * Close browser gracefully.
     */
    CompletableFuture<Void> close();

    /**
     * Crashes browser on the main thread.
     */
    @Experimental
    CompletableFuture<Void> crash();

    /**
     * Crashes GPU process.
     */
    @Experimental
    CompletableFuture<Void> crashGpuProcess();

    /**
     * Invoke custom browser commands used by telemetry.
     *
     */
    @Experimental
    CompletableFuture<Void> executeBrowserCommand(BrowserCommandId commandId);

    /**
     * Returns the command line switches for the browser process if, and only if
     * --enable-automation is on the commandline.
     *
     * @return Commandline parameters
     */
    @Experimental
    CompletableFuture<List<String>> getBrowserCommandLine();

    /**
     * Get a Chrome histogram by name.
     *
     * @param name
     *            Requested histogram name.
     *
     * @return Histogram.
     */
    @Experimental
    CompletableFuture<Histogram> getHistogram(String name);

    /**
     * Get a Chrome histogram by name.
     *
     * @param name
     *            Requested histogram name.
     * @param delta
     *            If true, retrieve delta since last delta call.
     *
     * @return Histogram.
     */
    @Experimental
    CompletableFuture<Histogram> getHistogram(String name, @Optional Boolean delta);

    /**
     * Get Chrome histograms.
     *
     * @return Histograms.
     */
    @Experimental
    CompletableFuture<List<Histogram>> getHistograms();

    /**
     * Get Chrome histograms.
     *
     * @param query
     *            Requested substring in name. Only histograms which have query as a
     *            substring in their name are extracted. An empty or absent query
     *            returns all histograms.
     * @param delta
     *            If true, retrieve delta since last delta call.
     *
     * @return Histograms.
     */
    @Experimental
    CompletableFuture<List<Histogram>> getHistograms(@Optional String query, @Optional Boolean delta);

    /**
     * Returns version information.
     *
     * @return GetVersionResult
     */
    CompletableFuture<GetVersionResult> getVersion();

    /**
     * Get position and size of the browser window.
     *
     * @param windowId
     *            Browser window id.
     *
     * @return Bounds information of the window. When window state is 'minimized',
     *         the restored window position and size are returned.
     */
    @Experimental
    CompletableFuture<Bounds> getWindowBounds(Integer windowId);

    /**
     * Get the browser window that contains the devtools target.
     *
     * @return GetWindowForTargetResult
     */
    @Experimental
    CompletableFuture<GetWindowForTargetResult> getWindowForTarget();

    /**
     * Get the browser window that contains the devtools target.
     *
     * @param targetId
     *            Devtools agent host id. If called as a part of the session,
     *            associated targetId is used.
     *
     * @return GetWindowForTargetResult
     */
    @Experimental
    CompletableFuture<GetWindowForTargetResult> getWindowForTarget(@Optional String targetId);

    /**
     * Grant specific permissions to the given origin and reject all others.
     *
     */
    @Experimental
    CompletableFuture<Void> grantPermissions(PermissionType permissions);

    /**
     * Grant specific permissions to the given origin and reject all others.
     *
     * @param origin
     *            Origin the permission applies to, all origins if not specified.
     * @param browserContextId
     *            BrowserContext to override permissions. When omitted, default
     *            browser context is used.
     */
    @Experimental
    CompletableFuture<Void> grantPermissions(
            PermissionType permissions, @Optional String origin, @Optional String browserContextId);

    /**
     * Reset all permission management for all origins.
     */
    CompletableFuture<Void> resetPermissions();

    /**
     * Reset all permission management for all origins.
     *
     * @param browserContextId
     *            BrowserContext to reset permissions. When omitted, default browser
     *            context is used.
     */
    CompletableFuture<Void> resetPermissions(@Optional String browserContextId);

    /**
     * Set dock tile details, platform-specific.
     */
    @Experimental
    CompletableFuture<Void> setDockTile();

    /**
     * Set dock tile details, platform-specific.
     *
     * @param image
     *            Png encoded image. (Encoded as a base64 string when passed over
     *            JSON)
     */
    @Experimental
    CompletableFuture<Void> setDockTile(@Optional String badgeLabel, @Optional String image);

    /**
     * Set the behavior when downloading a file.
     *
     * @param behavior
     *            Whether to allow all or deny all download requests, or use default
     *            Chrome behavior if available (otherwise deny). |allowAndName|
     *            allows download and names files according to their download guids.
     */
    @Experimental
    CompletableFuture<Void> setDownloadBehavior(DownloadBehavior behavior);

    /**
     * Set the behavior when downloading a file.
     *
     * @param behavior
     *            Whether to allow all or deny all download requests, or use default
     *            Chrome behavior if available (otherwise deny). |allowAndName|
     *            allows download and names files according to their download guids.
     * @param browserContextId
     *            BrowserContext to set download behavior. When omitted, default
     *            browser context is used.
     * @param downloadPath
     *            The default path to save downloaded files to. This is required if
     *            behavior is set to 'allow' or 'allowAndName'.
     * @param eventsEnabled
     *            Whether to emit download events (defaults to false).
     */
    @Experimental
    CompletableFuture<Void> setDownloadBehavior(
            DownloadBehavior behavior,
            @Optional String browserContextId,
            @Optional String downloadPath,
            @Optional Boolean eventsEnabled);

    /**
     * Set permission settings for given origin.
     *
     * @param permission
     *            Descriptor of permission to override.
     * @param setting
     *            Setting of the permission.
     */
    @Experimental
    CompletableFuture<Void> setPermission(PermissionDescriptor permission, PermissionSetting setting);

    /**
     * Set permission settings for given origin.
     *
     * @param permission
     *            Descriptor of permission to override.
     * @param setting
     *            Setting of the permission.
     * @param origin
     *            Origin the permission applies to, all origins if not specified.
     * @param browserContextId
     *            Context to override. When omitted, default browser context is
     *            used.
     */
    @Experimental
    CompletableFuture<Void> setPermission(
            PermissionDescriptor permission,
            PermissionSetting setting,
            @Optional String origin,
            @Optional String browserContextId);

    /**
     * Set position and/or size of the browser window.
     *
     * @param windowId
     *            Browser window id.
     * @param bounds
     *            New window bounds. The 'minimized', 'maximized' and 'fullscreen'
     *            states cannot be combined with 'left', 'top', 'width' or 'height'.
     *            Leaves unspecified fields unchanged.
     */
    @Experimental
    CompletableFuture<Void> setWindowBounds(Integer windowId, Bounds bounds);
}
