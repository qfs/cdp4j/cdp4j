// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.domstorage.StorageId;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Query and modify DOM storage.
 */
@Experimental
public interface DOMStorageAsync extends ParameterizedCommand<DOMStorageAsync> {
    CompletableFuture<Void> clear(StorageId storageId);

    /**
     * Disables storage tracking, prevents storage events from being sent to the
     * client.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables storage tracking, storage events will now be delivered to the client.
     */
    CompletableFuture<Void> enable();

    CompletableFuture<List<List<String>>> getDOMStorageItems(StorageId storageId);

    CompletableFuture<Void> removeDOMStorageItem(StorageId storageId, String key);

    CompletableFuture<Void> setDOMStorageItem(StorageId storageId, String key, String value);
}
