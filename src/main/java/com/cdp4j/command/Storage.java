// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.network.Cookie;
import com.cdp4j.type.network.CookieParam;
import com.cdp4j.type.storage.GetUsageAndQuotaResult;
import com.cdp4j.type.storage.RelatedWebsiteSet;
import com.cdp4j.type.storage.SharedStorageEntry;
import com.cdp4j.type.storage.SharedStorageMetadata;
import com.cdp4j.type.storage.StorageBucket;
import com.cdp4j.type.storage.TrustTokens;
import java.util.List;

@Experimental
public interface Storage extends ParameterizedCommand<Storage> {
    /**
     * Clears cookies.
     */
    void clearCookies();

    /**
     * Clears cookies.
     *
     * @param browserContextId Browser context to use when called on the browser endpoint.
     */
    void clearCookies(@Optional String browserContextId);

    /**
     * Clears storage for origin.
     *
     * @param origin Security origin.
     * @param storageTypes Comma separated list of StorageType to clear.
     */
    void clearDataForOrigin(String origin, String storageTypes);

    /**
     * Clears storage for storage key.
     *
     * @param storageKey Storage key.
     * @param storageTypes Comma separated list of StorageType to clear.
     */
    void clearDataForStorageKey(String storageKey, String storageTypes);

    /**
     * Clears all entries for a given origin's shared storage.
     *
     */
    @Experimental
    void clearSharedStorageEntries(String ownerOrigin);

    /**
     * Removes all Trust Tokens issued by the provided issuerOrigin.
     * Leaves other stored data, including the issuer's Redemption Records, intact.
     *
     *
     * @return True if any tokens were deleted, false otherwise.
     */
    @Experimental
    Boolean clearTrustTokens(String issuerOrigin);

    /**
     * Deletes entry for key (if it exists) for a given origin's shared storage.
     *
     */
    @Experimental
    void deleteSharedStorageEntry(String ownerOrigin, String key);

    /**
     * Deletes the Storage Bucket with the given storage key and bucket name.
     *
     */
    @Experimental
    void deleteStorageBucket(StorageBucket bucket);

    /**
     * Returns the list of URLs from a page and its embedded resources that match
     * existing grace period URL pattern rules.
     * https://developers.google.com/privacy-sandbox/cookies/temporary-exceptions/grace-period
     *
     * @param firstPartyUrl The URL of the page currently being visited.
     * @param thirdPartyUrls The list of embedded resource URLs from the page.
     *
     * @return Array of matching URLs. If there is a primary pattern match for the first-
     * party URL, only the first-party URL is returned in the array.
     */
    @Experimental
    List<String> getAffectedUrlsForThirdPartyCookieMetadata(String firstPartyUrl, List<String> thirdPartyUrls);

    /**
     * Returns all browser cookies.
     *
     * @return Array of cookie objects.
     */
    List<Cookie> getCookies();

    /**
     * Returns all browser cookies.
     *
     * @param browserContextId Browser context to use when called on the browser endpoint.
     *
     * @return Array of cookie objects.
     */
    List<Cookie> getCookies(@Optional String browserContextId);

    /**
     * Returns the effective Related Website Sets in use by this profile for the browser
     * session. The effective Related Website Sets will not change during a browser session.
     */
    @Experimental
    List<RelatedWebsiteSet> getRelatedWebsiteSets();

    /**
     * Gets the entries in an given origin's shared storage.
     *
     */
    @Experimental
    List<SharedStorageEntry> getSharedStorageEntries(String ownerOrigin);

    /**
     * Gets metadata for an origin's shared storage.
     *
     */
    @Experimental
    SharedStorageMetadata getSharedStorageMetadata(String ownerOrigin);

    /**
     * Returns a storage key given a frame id.
     *
     */
    String getStorageKeyForFrame(String frameId);

    /**
     * Returns the number of stored Trust Tokens per issuer for the
     * current browsing context.
     */
    @Experimental
    List<TrustTokens> getTrustTokens();

    /**
     * Returns usage and quota in bytes.
     *
     * @param origin Security origin.
     *
     * @return GetUsageAndQuotaResult
     */
    GetUsageAndQuotaResult getUsageAndQuota(String origin);

    /**
     * Override quota for the specified origin
     *
     * @param origin Security origin.
     */
    @Experimental
    void overrideQuotaForOrigin(String origin);

    /**
     * Override quota for the specified origin
     *
     * @param origin Security origin.
     * @param quotaSize The quota size (in bytes) to override the original quota with.
     * If this is called multiple times, the overridden quota will be equal to
     * the quotaSize provided in the final call. If this is called without
     * specifying a quotaSize, the quota will be reset to the default value for
     * the specified origin. If this is called multiple times with different
     * origins, the override will be maintained for each origin until it is
     * disabled (called without a quotaSize).
     */
    @Experimental
    void overrideQuotaForOrigin(String origin, @Optional Double quotaSize);

    /**
     * Resets the budget for ownerOrigin by clearing all budget withdrawals.
     *
     */
    @Experimental
    void resetSharedStorageBudget(String ownerOrigin);

    /**
     * Deletes state for sites identified as potential bounce trackers, immediately.
     */
    @Experimental
    List<String> runBounceTrackingMitigations();

    /**
     * Sends all pending Attribution Reports immediately, regardless of their
     * scheduled report time.
     *
     * @return The number of reports that were sent.
     */
    @Experimental
    Integer sendPendingAttributionReports();

    /**
     * https://wicg.github.io/attribution-reporting-api/
     *
     * @param enabled If enabled, noise is suppressed and reports are sent immediately.
     */
    @Experimental
    void setAttributionReportingLocalTestingMode(Boolean enabled);

    /**
     * Enables/disables issuing of Attribution Reporting events.
     *
     */
    @Experimental
    void setAttributionReportingTracking(Boolean enable);

    /**
     * Sets given cookies.
     *
     * @param cookies Cookies to be set.
     */
    void setCookies(List<CookieParam> cookies);

    /**
     * Sets given cookies.
     *
     * @param cookies Cookies to be set.
     * @param browserContextId Browser context to use when called on the browser endpoint.
     */
    void setCookies(List<CookieParam> cookies, @Optional String browserContextId);

    /**
     * Enables/Disables issuing of interestGroupAuctionEventOccurred and
     * interestGroupAuctionNetworkRequestCreated.
     *
     */
    @Experimental
    void setInterestGroupAuctionTracking(Boolean enable);

    /**
     * Enables/Disables issuing of interestGroupAccessed events.
     *
     */
    @Experimental
    void setInterestGroupTracking(Boolean enable);

    /**
     * Sets entry with key and value for a given origin's shared storage.
     *
     */
    @Experimental
    void setSharedStorageEntry(String ownerOrigin, String key, String value);

    /**
     * Sets entry with key and value for a given origin's shared storage.
     *
     * @param ignoreIfPresent If ignoreIfPresent is included and true, then only sets the entry if
     * key doesn't already exist.
     */
    @Experimental
    void setSharedStorageEntry(String ownerOrigin, String key, String value, @Optional Boolean ignoreIfPresent);

    /**
     * Enables/disables issuing of sharedStorageAccessed events.
     *
     */
    @Experimental
    void setSharedStorageTracking(Boolean enable);

    /**
     * Set tracking for a storage key's buckets.
     *
     */
    @Experimental
    void setStorageBucketTracking(String storageKey, Boolean enable);

    /**
     * Registers origin to be notified when an update occurs to its cache storage list.
     *
     * @param origin Security origin.
     */
    void trackCacheStorageForOrigin(String origin);

    /**
     * Registers storage key to be notified when an update occurs to its cache storage list.
     *
     * @param storageKey Storage key.
     */
    void trackCacheStorageForStorageKey(String storageKey);

    /**
     * Registers origin to be notified when an update occurs to its IndexedDB.
     *
     * @param origin Security origin.
     */
    void trackIndexedDBForOrigin(String origin);

    /**
     * Registers storage key to be notified when an update occurs to its IndexedDB.
     *
     * @param storageKey Storage key.
     */
    void trackIndexedDBForStorageKey(String storageKey);

    /**
     * Unregisters origin from receiving notifications for cache storage.
     *
     * @param origin Security origin.
     */
    void untrackCacheStorageForOrigin(String origin);

    /**
     * Unregisters storage key from receiving notifications for cache storage.
     *
     * @param storageKey Storage key.
     */
    void untrackCacheStorageForStorageKey(String storageKey);

    /**
     * Unregisters origin from receiving notifications for IndexedDB.
     *
     * @param origin Security origin.
     */
    void untrackIndexedDBForOrigin(String origin);

    /**
     * Unregisters storage key from receiving notifications for IndexedDB.
     *
     * @param storageKey Storage key.
     */
    void untrackIndexedDBForStorageKey(String storageKey);
}
