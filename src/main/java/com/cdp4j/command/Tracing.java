// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.constant.TracingTransferMode;
import com.cdp4j.type.tracing.MemoryDumpLevelOfDetail;
import com.cdp4j.type.tracing.RequestMemoryDumpResult;
import com.cdp4j.type.tracing.StreamCompression;
import com.cdp4j.type.tracing.StreamFormat;
import com.cdp4j.type.tracing.TraceConfig;
import com.cdp4j.type.tracing.TracingBackend;
import java.util.List;

public interface Tracing extends ParameterizedCommand<Tracing> {
    /**
     * Stop trace events collection.
     */
    void end();

    /**
     * Gets supported tracing categories.
     *
     * @return A list of supported tracing categories.
     */
    @Experimental
    List<String> getCategories();

    /**
     * Record a clock sync marker in the trace.
     *
     * @param syncId The ID of this clock sync marker
     */
    @Experimental
    void recordClockSyncMarker(String syncId);

    /**
     * Request a global memory dump.
     *
     * @return RequestMemoryDumpResult
     */
    @Experimental
    RequestMemoryDumpResult requestMemoryDump();

    /**
     * Request a global memory dump.
     *
     * @param deterministic Enables more deterministic results by forcing garbage collection
     * @param levelOfDetail Specifies level of details in memory dump. Defaults to "detailed".
     *
     * @return RequestMemoryDumpResult
     */
    @Experimental
    RequestMemoryDumpResult requestMemoryDump(
            @Optional Boolean deterministic, @Optional MemoryDumpLevelOfDetail levelOfDetail);

    /**
     * Start trace events collection.
     */
    void start();

    /**
     * Start trace events collection.
     *
     * @param categories Category/tag filter
     * @param options Tracing options
     * @param bufferUsageReportingInterval If set, the agent will issue bufferUsage events at this interval, specified in milliseconds
     * @param transferMode Whether to report trace events as series of dataCollected events or to save trace to a
     * stream (defaults to ReportEvents).
     * @param streamFormat Trace data format to use. This only applies when using ReturnAsStream
     * transfer mode (defaults to json).
     * @param streamCompression Compression format to use. This only applies when using ReturnAsStream
     * transfer mode (defaults to none)
     * @param perfettoConfig Base64-encoded serialized perfetto.protos.TraceConfig protobuf message
     * When specified, the parameters categories, options, traceConfig
     * are ignored. (Encoded as a base64 string when passed over JSON)
     * @param tracingBackend Backend type (defaults to auto)
     */
    void start(
            @Experimental @Optional @Deprecated String categories,
            @Experimental @Optional @Deprecated String options,
            @Experimental @Optional Double bufferUsageReportingInterval,
            @Optional TracingTransferMode transferMode,
            @Optional StreamFormat streamFormat,
            @Experimental @Optional StreamCompression streamCompression,
            @Optional TraceConfig traceConfig,
            @Experimental @Optional String perfettoConfig,
            @Experimental @Optional TracingBackend tracingBackend);
}
