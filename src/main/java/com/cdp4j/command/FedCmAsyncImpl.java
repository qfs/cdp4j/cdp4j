// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.fedcm.AccountUrlType;
import com.cdp4j.type.fedcm.DialogButton;
import java.util.concurrent.CompletableFuture;

class FedCmAsyncImpl extends ParameterizedCommandImpl<FedCmAsync> implements FedCmAsync {

    private static final CommandReturnType CRT_CLICK_DIALOG_BUTTON = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISMISS_DIALOG = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_OPEN_URL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RESET_COOLDOWN = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SELECT_ACCOUNT = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CLICK_DIALOG_BUTTON_1 = new String[] {"dialogId", "dialogButton"};
    private static final String[] PARAMS_DISMISS_DIALOG_1 = new String[] {"dialogId"};
    private static final String[] PARAMS_DISMISS_DIALOG_2 = new String[] {"dialogId", "triggerCooldown"};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"disableRejectionDelay"};
    private static final String[] PARAMS_OPEN_URL_1 = new String[] {"dialogId", "accountIndex", "accountUrlType"};
    private static final String[] PARAMS_SELECT_ACCOUNT_1 = new String[] {"dialogId", "accountIndex"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public FedCmAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clickDialogButton(String dialogId, DialogButton dialogButton) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.FedCm_clickDialogButton,
                CRT_CLICK_DIALOG_BUTTON,
                PARAMS_CLICK_DIALOG_BUTTON_1,
                new Object[] {dialogId, dialogButton},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.FedCm_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dismissDialog(String dialogId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.FedCm_dismissDialog,
                CRT_DISMISS_DIALOG,
                PARAMS_DISMISS_DIALOG_1,
                new Object[] {dialogId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dismissDialog(String dialogId, Boolean triggerCooldown) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.FedCm_dismissDialog,
                CRT_DISMISS_DIALOG,
                PARAMS_DISMISS_DIALOG_2,
                new Object[] {dialogId, triggerCooldown},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.FedCm_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable(Boolean disableRejectionDelay) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.FedCm_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_2,
                new Object[] {disableRejectionDelay},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> openUrl(String dialogId, Integer accountIndex, AccountUrlType accountUrlType) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.FedCm_openUrl,
                CRT_OPEN_URL,
                PARAMS_OPEN_URL_1,
                new Object[] {dialogId, accountIndex, accountUrlType},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> resetCooldown() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.FedCm_resetCooldown, CRT_RESET_COOLDOWN, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> selectAccount(String dialogId, Integer accountIndex) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.FedCm_selectAccount,
                CRT_SELECT_ACCOUNT,
                PARAMS_SELECT_ACCOUNT_1,
                new Object[] {dialogId, accountIndex},
                false);
    }
}
