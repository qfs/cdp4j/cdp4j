// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.target.FilterEntry;
import com.cdp4j.type.target.RemoteLocation;
import com.cdp4j.type.target.TargetInfo;
import com.cdp4j.type.target.WindowState;
import java.util.List;

class TargetImpl extends ParameterizedCommandImpl<Target> implements Target {

    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final TypeReference<List<TargetInfo>> LIST_TARGETINFO = new TypeReference<List<TargetInfo>>() {};
    private static final CommandReturnType CRT_ACTIVATE_TARGET = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ATTACH_TO_BROWSER_TARGET =
            new CommandReturnType("sessionId", String.class, null);
    private static final CommandReturnType CRT_ATTACH_TO_TARGET =
            new CommandReturnType("sessionId", String.class, null);
    private static final CommandReturnType CRT_AUTO_ATTACH_RELATED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLOSE_TARGET = new CommandReturnType("success", Boolean.class, null);
    private static final CommandReturnType CRT_CREATE_BROWSER_CONTEXT =
            new CommandReturnType("browserContextId", String.class, null);
    private static final CommandReturnType CRT_CREATE_TARGET = new CommandReturnType("targetId", String.class, null);
    private static final CommandReturnType CRT_DETACH_FROM_TARGET = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPOSE_BROWSER_CONTEXT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EXPOSE_DEV_TOOLS_PROTOCOL =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_BROWSER_CONTEXTS =
            new CommandReturnType("browserContextIds", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_TARGETS =
            new CommandReturnType("targetInfos", List.class, LIST_TARGETINFO);
    private static final CommandReturnType CRT_GET_TARGET_INFO =
            new CommandReturnType("targetInfo", TargetInfo.class, null);
    private static final CommandReturnType CRT_SEND_MESSAGE_TO_TARGET = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_AUTO_ATTACH = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DISCOVER_TARGETS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_REMOTE_LOCATIONS = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ACTIVATE_TARGET_1 = new String[] {"targetId"};
    private static final String[] PARAMS_ATTACH_TO_TARGET_1 = new String[] {"targetId"};
    private static final String[] PARAMS_ATTACH_TO_TARGET_2 = new String[] {"targetId", "flatten"};
    private static final String[] PARAMS_AUTO_ATTACH_RELATED_1 = new String[] {"targetId", "waitForDebuggerOnStart"};
    private static final String[] PARAMS_AUTO_ATTACH_RELATED_2 =
            new String[] {"targetId", "waitForDebuggerOnStart", "filter"};
    private static final String[] PARAMS_CLOSE_TARGET_1 = new String[] {"targetId"};
    private static final String[] PARAMS_CREATE_BROWSER_CONTEXT_2 =
            new String[] {"disposeOnDetach", "proxyServer", "proxyBypassList", "originsWithUniversalNetworkAccess"};
    private static final String[] PARAMS_CREATE_TARGET_1 = new String[] {"url"};
    private static final String[] PARAMS_CREATE_TARGET_2 = new String[] {
        "url",
        "left",
        "top",
        "width",
        "height",
        "windowState",
        "browserContextId",
        "enableBeginFrameControl",
        "newWindow",
        "background",
        "forTab"
    };
    private static final String[] PARAMS_DETACH_FROM_TARGET_2 = new String[] {"sessionId", "targetId"};
    private static final String[] PARAMS_DISPOSE_BROWSER_CONTEXT_1 = new String[] {"browserContextId"};
    private static final String[] PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_1 = new String[] {"targetId"};
    private static final String[] PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_2 =
            new String[] {"targetId", "bindingName", "inheritPermissions"};
    private static final String[] PARAMS_GET_TARGETS_2 = new String[] {"filter"};
    private static final String[] PARAMS_GET_TARGET_INFO_2 = new String[] {"targetId"};
    private static final String[] PARAMS_SEND_MESSAGE_TO_TARGET_1 = new String[] {"message"};
    private static final String[] PARAMS_SEND_MESSAGE_TO_TARGET_2 = new String[] {"message", "sessionId", "targetId"};
    private static final String[] PARAMS_SET_AUTO_ATTACH_1 = new String[] {"autoAttach", "waitForDebuggerOnStart"};
    private static final String[] PARAMS_SET_AUTO_ATTACH_2 =
            new String[] {"autoAttach", "waitForDebuggerOnStart", "flatten", "filter"};
    private static final String[] PARAMS_SET_DISCOVER_TARGETS_1 = new String[] {"discover"};
    private static final String[] PARAMS_SET_DISCOVER_TARGETS_2 = new String[] {"discover", "filter"};
    private static final String[] PARAMS_SET_REMOTE_LOCATIONS_1 = new String[] {"locations"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public TargetImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void activateTarget(String targetId) {
        handler.invoke(
                this,
                DomainCommand.Target_activateTarget,
                CRT_ACTIVATE_TARGET,
                PARAMS_ACTIVATE_TARGET_1,
                new Object[] {targetId},
                true);
    }

    @Override
    public String attachToBrowserTarget() {
        return (String) handler.invoke(
                this,
                DomainCommand.Target_attachToBrowserTarget,
                CRT_ATTACH_TO_BROWSER_TARGET,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public String attachToTarget(String targetId) {
        return (String) handler.invoke(
                this,
                DomainCommand.Target_attachToTarget,
                CRT_ATTACH_TO_TARGET,
                PARAMS_ATTACH_TO_TARGET_1,
                new Object[] {targetId},
                true);
    }

    @Override
    public String attachToTarget(String targetId, Boolean flatten) {
        return (String) handler.invoke(
                this,
                DomainCommand.Target_attachToTarget,
                CRT_ATTACH_TO_TARGET,
                PARAMS_ATTACH_TO_TARGET_2,
                new Object[] {targetId, flatten},
                true);
    }

    @Override
    public void autoAttachRelated(String targetId, Boolean waitForDebuggerOnStart) {
        handler.invoke(
                this,
                DomainCommand.Target_autoAttachRelated,
                CRT_AUTO_ATTACH_RELATED,
                PARAMS_AUTO_ATTACH_RELATED_1,
                new Object[] {targetId, waitForDebuggerOnStart},
                true);
    }

    @Override
    public void autoAttachRelated(String targetId, Boolean waitForDebuggerOnStart, List<FilterEntry> filter) {
        handler.invoke(
                this,
                DomainCommand.Target_autoAttachRelated,
                CRT_AUTO_ATTACH_RELATED,
                PARAMS_AUTO_ATTACH_RELATED_2,
                new Object[] {targetId, waitForDebuggerOnStart, filter},
                true);
    }

    @Override
    public Boolean closeTarget(String targetId) {
        return (Boolean) handler.invoke(
                this,
                DomainCommand.Target_closeTarget,
                CRT_CLOSE_TARGET,
                PARAMS_CLOSE_TARGET_1,
                new Object[] {targetId},
                true);
    }

    @Override
    public String createBrowserContext() {
        return (String) handler.invoke(
                this,
                DomainCommand.Target_createBrowserContext,
                CRT_CREATE_BROWSER_CONTEXT,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public String createBrowserContext(
            Boolean disposeOnDetach,
            String proxyServer,
            String proxyBypassList,
            List<String> originsWithUniversalNetworkAccess) {
        return (String) handler.invoke(
                this,
                DomainCommand.Target_createBrowserContext,
                CRT_CREATE_BROWSER_CONTEXT,
                PARAMS_CREATE_BROWSER_CONTEXT_2,
                new Object[] {disposeOnDetach, proxyServer, proxyBypassList, originsWithUniversalNetworkAccess},
                true);
    }

    @Override
    public String createTarget(String url) {
        return (String) handler.invoke(
                this,
                DomainCommand.Target_createTarget,
                CRT_CREATE_TARGET,
                PARAMS_CREATE_TARGET_1,
                new Object[] {url},
                true);
    }

    @Override
    public String createTarget(
            String url,
            Integer left,
            Integer top,
            Integer width,
            Integer height,
            WindowState windowState,
            String browserContextId,
            Boolean enableBeginFrameControl,
            Boolean newWindow,
            Boolean background,
            Boolean forTab) {
        return (String) handler.invoke(
                this,
                DomainCommand.Target_createTarget,
                CRT_CREATE_TARGET,
                PARAMS_CREATE_TARGET_2,
                new Object[] {
                    url,
                    left,
                    top,
                    width,
                    height,
                    windowState,
                    browserContextId,
                    enableBeginFrameControl,
                    newWindow,
                    background,
                    forTab
                },
                true);
    }

    @Override
    public void detachFromTarget() {
        handler.invoke(
                this, DomainCommand.Target_detachFromTarget, CRT_DETACH_FROM_TARGET, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void detachFromTarget(String sessionId, String targetId) {
        handler.invoke(
                this,
                DomainCommand.Target_detachFromTarget,
                CRT_DETACH_FROM_TARGET,
                PARAMS_DETACH_FROM_TARGET_2,
                new Object[] {sessionId, targetId},
                true);
    }

    @Override
    public void disposeBrowserContext(String browserContextId) {
        handler.invoke(
                this,
                DomainCommand.Target_disposeBrowserContext,
                CRT_DISPOSE_BROWSER_CONTEXT,
                PARAMS_DISPOSE_BROWSER_CONTEXT_1,
                new Object[] {browserContextId},
                true);
    }

    @Override
    public void exposeDevToolsProtocol(String targetId) {
        handler.invoke(
                this,
                DomainCommand.Target_exposeDevToolsProtocol,
                CRT_EXPOSE_DEV_TOOLS_PROTOCOL,
                PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_1,
                new Object[] {targetId},
                true);
    }

    @Override
    public void exposeDevToolsProtocol(String targetId, String bindingName, Boolean inheritPermissions) {
        handler.invoke(
                this,
                DomainCommand.Target_exposeDevToolsProtocol,
                CRT_EXPOSE_DEV_TOOLS_PROTOCOL,
                PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_2,
                new Object[] {targetId, bindingName, inheritPermissions},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> getBrowserContexts() {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.Target_getBrowserContexts,
                CRT_GET_BROWSER_CONTEXTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public TargetInfo getTargetInfo() {
        return (TargetInfo) handler.invoke(
                this, DomainCommand.Target_getTargetInfo, CRT_GET_TARGET_INFO, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public TargetInfo getTargetInfo(String targetId) {
        return (TargetInfo) handler.invoke(
                this,
                DomainCommand.Target_getTargetInfo,
                CRT_GET_TARGET_INFO,
                PARAMS_GET_TARGET_INFO_2,
                new Object[] {targetId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<TargetInfo> getTargets() {
        return (List<TargetInfo>)
                handler.invoke(this, DomainCommand.Target_getTargets, CRT_GET_TARGETS, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<TargetInfo> getTargets(List<FilterEntry> filter) {
        return (List<TargetInfo>) handler.invoke(
                this,
                DomainCommand.Target_getTargets,
                CRT_GET_TARGETS,
                PARAMS_GET_TARGETS_2,
                new Object[] {filter},
                true);
    }

    @Override
    public void sendMessageToTarget(String message) {
        handler.invoke(
                this,
                DomainCommand.Target_sendMessageToTarget,
                CRT_SEND_MESSAGE_TO_TARGET,
                PARAMS_SEND_MESSAGE_TO_TARGET_1,
                new Object[] {message},
                true);
    }

    @Override
    public void sendMessageToTarget(String message, String sessionId, String targetId) {
        handler.invoke(
                this,
                DomainCommand.Target_sendMessageToTarget,
                CRT_SEND_MESSAGE_TO_TARGET,
                PARAMS_SEND_MESSAGE_TO_TARGET_2,
                new Object[] {message, sessionId, targetId},
                true);
    }

    @Override
    public void setAutoAttach(Boolean autoAttach, Boolean waitForDebuggerOnStart) {
        handler.invoke(
                this,
                DomainCommand.Target_setAutoAttach,
                CRT_SET_AUTO_ATTACH,
                PARAMS_SET_AUTO_ATTACH_1,
                new Object[] {autoAttach, waitForDebuggerOnStart},
                true);
    }

    @Override
    public void setAutoAttach(
            Boolean autoAttach, Boolean waitForDebuggerOnStart, Boolean flatten, List<FilterEntry> filter) {
        handler.invoke(
                this,
                DomainCommand.Target_setAutoAttach,
                CRT_SET_AUTO_ATTACH,
                PARAMS_SET_AUTO_ATTACH_2,
                new Object[] {autoAttach, waitForDebuggerOnStart, flatten, filter},
                true);
    }

    @Override
    public void setDiscoverTargets(Boolean discover) {
        handler.invoke(
                this,
                DomainCommand.Target_setDiscoverTargets,
                CRT_SET_DISCOVER_TARGETS,
                PARAMS_SET_DISCOVER_TARGETS_1,
                new Object[] {discover},
                true);
    }

    @Override
    public void setDiscoverTargets(Boolean discover, List<FilterEntry> filter) {
        handler.invoke(
                this,
                DomainCommand.Target_setDiscoverTargets,
                CRT_SET_DISCOVER_TARGETS,
                PARAMS_SET_DISCOVER_TARGETS_2,
                new Object[] {discover, filter},
                true);
    }

    @Override
    public void setRemoteLocations(List<RemoteLocation> locations) {
        handler.invoke(
                this,
                DomainCommand.Target_setRemoteLocations,
                CRT_SET_REMOTE_LOCATIONS,
                PARAMS_SET_REMOTE_LOCATIONS_1,
                new Object[] {locations},
                true);
    }
}
