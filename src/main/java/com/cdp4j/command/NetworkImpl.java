// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.debugger.SearchMatch;
import com.cdp4j.type.emulation.UserAgentMetadata;
import com.cdp4j.type.network.AuthChallengeResponse;
import com.cdp4j.type.network.ConnectionType;
import com.cdp4j.type.network.ContentEncoding;
import com.cdp4j.type.network.Cookie;
import com.cdp4j.type.network.CookieParam;
import com.cdp4j.type.network.CookiePartitionKey;
import com.cdp4j.type.network.CookiePriority;
import com.cdp4j.type.network.CookieSameSite;
import com.cdp4j.type.network.CookieSourceScheme;
import com.cdp4j.type.network.ErrorReason;
import com.cdp4j.type.network.GetResponseBodyForInterceptionResult;
import com.cdp4j.type.network.GetResponseBodyResult;
import com.cdp4j.type.network.LoadNetworkResourceOptions;
import com.cdp4j.type.network.LoadNetworkResourcePageResult;
import com.cdp4j.type.network.RequestPattern;
import com.cdp4j.type.network.SecurityIsolationStatus;
import java.util.List;
import java.util.Map;

class NetworkImpl extends ParameterizedCommandImpl<Network> implements Network {

    private static final TypeReference<List<Cookie>> LIST_COOKIE = new TypeReference<List<Cookie>>() {};
    private static final TypeReference<List<SearchMatch>> LIST_SEARCHMATCH = new TypeReference<List<SearchMatch>>() {};
    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_CAN_CLEAR_BROWSER_CACHE =
            new CommandReturnType("result", Boolean.class, null);
    private static final CommandReturnType CRT_CAN_CLEAR_BROWSER_COOKIES =
            new CommandReturnType("result", Boolean.class, null);
    private static final CommandReturnType CRT_CAN_EMULATE_NETWORK_CONDITIONS =
            new CommandReturnType("result", Boolean.class, null);
    private static final CommandReturnType CRT_CLEAR_ACCEPTED_ENCODINGS_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_BROWSER_CACHE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_BROWSER_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CONTINUE_INTERCEPTED_REQUEST =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EMULATE_NETWORK_CONDITIONS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE_REPORTING_API = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ALL_COOKIES =
            new CommandReturnType("cookies", List.class, LIST_COOKIE);
    private static final CommandReturnType CRT_GET_CERTIFICATE =
            new CommandReturnType("tableNames", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_COOKIES = new CommandReturnType("cookies", List.class, LIST_COOKIE);
    private static final CommandReturnType CRT_GET_REQUEST_POST_DATA =
            new CommandReturnType("postData", String.class, null);
    private static final CommandReturnType CRT_GET_RESPONSE_BODY =
            new CommandReturnType(null, GetResponseBodyResult.class, null);
    private static final CommandReturnType CRT_GET_RESPONSE_BODY_FOR_INTERCEPTION =
            new CommandReturnType(null, GetResponseBodyForInterceptionResult.class, null);
    private static final CommandReturnType CRT_GET_SECURITY_ISOLATION_STATUS =
            new CommandReturnType("status", SecurityIsolationStatus.class, null);
    private static final CommandReturnType CRT_LOAD_NETWORK_RESOURCE =
            new CommandReturnType("resource", LoadNetworkResourcePageResult.class, null);
    private static final CommandReturnType CRT_REPLAY_XH_R = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SEARCH_IN_RESPONSE_BODY =
            new CommandReturnType("result", List.class, LIST_SEARCHMATCH);
    private static final CommandReturnType CRT_SET_ACCEPTED_ENCODINGS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ATTACH_DEBUG_STACK = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BLOCKED_UR_LS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BYPASS_SERVICE_WORKER =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_CACHE_DISABLED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_COOKIE = new CommandReturnType("success", Boolean.class, null);
    private static final CommandReturnType CRT_SET_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_COOKIE_CONTROLS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_EXTRA_HT_TP_HEADERS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_REQUEST_INTERCEPTION = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_USER_AGENT_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STREAM_RESOURCE_CONTENT =
            new CommandReturnType("bufferedData", String.class, null);
    private static final CommandReturnType CRT_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM =
            new CommandReturnType("stream", String.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CONTINUE_INTERCEPTED_REQUEST_1 = new String[] {"interceptionId"};
    private static final String[] PARAMS_CONTINUE_INTERCEPTED_REQUEST_2 = new String[] {
        "interceptionId", "errorReason", "rawResponse", "url", "method", "postData", "headers", "authChallengeResponse"
    };
    private static final String[] PARAMS_DELETE_COOKIES_1 = new String[] {"name"};
    private static final String[] PARAMS_DELETE_COOKIES_2 =
            new String[] {"name", "url", "domain", "path", "partitionKey"};
    private static final String[] PARAMS_EMULATE_NETWORK_CONDITIONS_1 =
            new String[] {"offline", "latency", "downloadThroughput", "uploadThroughput"};
    private static final String[] PARAMS_EMULATE_NETWORK_CONDITIONS_2 = new String[] {
        "offline",
        "latency",
        "downloadThroughput",
        "uploadThroughput",
        "connectionType",
        "packetLoss",
        "packetQueueLength",
        "packetReordering"
    };
    private static final String[] PARAMS_ENABLE_2 =
            new String[] {"maxTotalBufferSize", "maxResourceBufferSize", "maxPostDataSize"};
    private static final String[] PARAMS_ENABLE_REPORTING_API_1 = new String[] {"enable"};
    private static final String[] PARAMS_GET_CERTIFICATE_1 = new String[] {"origin"};
    private static final String[] PARAMS_GET_COOKIES_2 = new String[] {"urls"};
    private static final String[] PARAMS_GET_REQUEST_POST_DATA_1 = new String[] {"requestId"};
    private static final String[] PARAMS_GET_RESPONSE_BODY_1 = new String[] {"requestId"};
    private static final String[] PARAMS_GET_RESPONSE_BODY_FOR_INTERCEPTION_1 = new String[] {"interceptionId"};
    private static final String[] PARAMS_GET_SECURITY_ISOLATION_STATUS_2 = new String[] {"frameId"};
    private static final String[] PARAMS_LOAD_NETWORK_RESOURCE_1 = new String[] {"url", "options"};
    private static final String[] PARAMS_LOAD_NETWORK_RESOURCE_2 = new String[] {"frameId", "url", "options"};
    private static final String[] PARAMS_REPLAY_XH_R_1 = new String[] {"requestId"};
    private static final String[] PARAMS_SEARCH_IN_RESPONSE_BODY_1 = new String[] {"requestId", "query"};
    private static final String[] PARAMS_SEARCH_IN_RESPONSE_BODY_2 =
            new String[] {"requestId", "query", "caseSensitive", "isRegex"};
    private static final String[] PARAMS_SET_ACCEPTED_ENCODINGS_1 = new String[] {"encodings"};
    private static final String[] PARAMS_SET_ATTACH_DEBUG_STACK_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_BLOCKED_UR_LS_1 = new String[] {"urls"};
    private static final String[] PARAMS_SET_BYPASS_SERVICE_WORKER_1 = new String[] {"bypass"};
    private static final String[] PARAMS_SET_CACHE_DISABLED_1 = new String[] {"cacheDisabled"};
    private static final String[] PARAMS_SET_COOKIES_1 = new String[] {"cookies"};
    private static final String[] PARAMS_SET_COOKIE_1 = new String[] {"name", "value"};
    private static final String[] PARAMS_SET_COOKIE_2 = new String[] {
        "name",
        "value",
        "url",
        "domain",
        "path",
        "secure",
        "httpOnly",
        "sameSite",
        "expires",
        "priority",
        "sameParty",
        "sourceScheme",
        "sourcePort",
        "partitionKey"
    };
    private static final String[] PARAMS_SET_COOKIE_CONTROLS_1 = new String[] {
        "enableThirdPartyCookieRestriction", "disableThirdPartyCookieMetadata", "disableThirdPartyCookieHeuristics"
    };
    private static final String[] PARAMS_SET_EXTRA_HT_TP_HEADERS_1 = new String[] {"headers"};
    private static final String[] PARAMS_SET_REQUEST_INTERCEPTION_1 = new String[] {"patterns"};
    private static final String[] PARAMS_SET_USER_AGENT_OVERRIDE_1 = new String[] {"userAgent"};
    private static final String[] PARAMS_SET_USER_AGENT_OVERRIDE_2 =
            new String[] {"userAgent", "acceptLanguage", "platform", "userAgentMetadata"};
    private static final String[] PARAMS_STREAM_RESOURCE_CONTENT_1 = new String[] {"requestId"};
    private static final String[] PARAMS_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM_1 =
            new String[] {"interceptionId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public NetworkImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public Boolean canClearBrowserCache() {
        return (Boolean) handler.invoke(
                this,
                DomainCommand.Network_canClearBrowserCache,
                CRT_CAN_CLEAR_BROWSER_CACHE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public Boolean canClearBrowserCookies() {
        return (Boolean) handler.invoke(
                this,
                DomainCommand.Network_canClearBrowserCookies,
                CRT_CAN_CLEAR_BROWSER_COOKIES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public Boolean canEmulateNetworkConditions() {
        return (Boolean) handler.invoke(
                this,
                DomainCommand.Network_canEmulateNetworkConditions,
                CRT_CAN_EMULATE_NETWORK_CONDITIONS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void clearAcceptedEncodingsOverride() {
        handler.invoke(
                this,
                DomainCommand.Network_clearAcceptedEncodingsOverride,
                CRT_CLEAR_ACCEPTED_ENCODINGS_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void clearBrowserCache() {
        handler.invoke(
                this, DomainCommand.Network_clearBrowserCache, CRT_CLEAR_BROWSER_CACHE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void clearBrowserCookies() {
        handler.invoke(
                this,
                DomainCommand.Network_clearBrowserCookies,
                CRT_CLEAR_BROWSER_COOKIES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void continueInterceptedRequest(String interceptionId) {
        handler.invoke(
                this,
                DomainCommand.Network_continueInterceptedRequest,
                CRT_CONTINUE_INTERCEPTED_REQUEST,
                PARAMS_CONTINUE_INTERCEPTED_REQUEST_1,
                new Object[] {interceptionId},
                true);
    }

    @Override
    public void continueInterceptedRequest(
            String interceptionId,
            ErrorReason errorReason,
            String rawResponse,
            String url,
            String method,
            String postData,
            Map<String, Object> headers,
            AuthChallengeResponse authChallengeResponse) {
        handler.invoke(
                this,
                DomainCommand.Network_continueInterceptedRequest,
                CRT_CONTINUE_INTERCEPTED_REQUEST,
                PARAMS_CONTINUE_INTERCEPTED_REQUEST_2,
                new Object[] {
                    interceptionId, errorReason, rawResponse, url, method, postData, headers, authChallengeResponse
                },
                true);
    }

    @Override
    public void deleteCookies(String name) {
        handler.invoke(
                this,
                DomainCommand.Network_deleteCookies,
                CRT_DELETE_COOKIES,
                PARAMS_DELETE_COOKIES_1,
                new Object[] {name},
                true);
    }

    @Override
    public void deleteCookies(String name, String url, String domain, String path, CookiePartitionKey partitionKey) {
        handler.invoke(
                this,
                DomainCommand.Network_deleteCookies,
                CRT_DELETE_COOKIES,
                PARAMS_DELETE_COOKIES_2,
                new Object[] {name, url, domain, path, partitionKey},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Network_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void emulateNetworkConditions(
            Boolean offline, Double latency, Double downloadThroughput, Double uploadThroughput) {
        handler.invoke(
                this,
                DomainCommand.Network_emulateNetworkConditions,
                CRT_EMULATE_NETWORK_CONDITIONS,
                PARAMS_EMULATE_NETWORK_CONDITIONS_1,
                new Object[] {offline, latency, downloadThroughput, uploadThroughput},
                true);
    }

    @Override
    public void emulateNetworkConditions(
            Boolean offline,
            Double latency,
            Double downloadThroughput,
            Double uploadThroughput,
            ConnectionType connectionType,
            Double packetLoss,
            Integer packetQueueLength,
            Boolean packetReordering) {
        handler.invoke(
                this,
                DomainCommand.Network_emulateNetworkConditions,
                CRT_EMULATE_NETWORK_CONDITIONS,
                PARAMS_EMULATE_NETWORK_CONDITIONS_2,
                new Object[] {
                    offline,
                    latency,
                    downloadThroughput,
                    uploadThroughput,
                    connectionType,
                    packetLoss,
                    packetQueueLength,
                    packetReordering
                },
                true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Network_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable(Integer maxTotalBufferSize, Integer maxResourceBufferSize, Integer maxPostDataSize) {
        handler.invoke(
                this,
                DomainCommand.Network_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_2,
                new Object[] {maxTotalBufferSize, maxResourceBufferSize, maxPostDataSize},
                true);
    }

    @Override
    public void enableReportingApi(Boolean enable) {
        handler.invoke(
                this,
                DomainCommand.Network_enableReportingApi,
                CRT_ENABLE_REPORTING_API,
                PARAMS_ENABLE_REPORTING_API_1,
                new Object[] {enable},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Cookie> getAllCookies() {
        return (List<Cookie>) handler.invoke(
                this, DomainCommand.Network_getAllCookies, CRT_GET_ALL_COOKIES, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> getCertificate(String origin) {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.Network_getCertificate,
                CRT_GET_CERTIFICATE,
                PARAMS_GET_CERTIFICATE_1,
                new Object[] {origin},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Cookie> getCookies() {
        return (List<Cookie>)
                handler.invoke(this, DomainCommand.Network_getCookies, CRT_GET_COOKIES, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Cookie> getCookies(List<String> urls) {
        return (List<Cookie>) handler.invoke(
                this,
                DomainCommand.Network_getCookies,
                CRT_GET_COOKIES,
                PARAMS_GET_COOKIES_2,
                new Object[] {urls},
                true);
    }

    @Override
    public String getRequestPostData(String requestId) {
        return (String) handler.invoke(
                this,
                DomainCommand.Network_getRequestPostData,
                CRT_GET_REQUEST_POST_DATA,
                PARAMS_GET_REQUEST_POST_DATA_1,
                new Object[] {requestId},
                true);
    }

    @Override
    public GetResponseBodyResult getResponseBody(String requestId) {
        return (GetResponseBodyResult) handler.invoke(
                this,
                DomainCommand.Network_getResponseBody,
                CRT_GET_RESPONSE_BODY,
                PARAMS_GET_RESPONSE_BODY_1,
                new Object[] {requestId},
                true);
    }

    @Override
    public GetResponseBodyForInterceptionResult getResponseBodyForInterception(String interceptionId) {
        return (GetResponseBodyForInterceptionResult) handler.invoke(
                this,
                DomainCommand.Network_getResponseBodyForInterception,
                CRT_GET_RESPONSE_BODY_FOR_INTERCEPTION,
                PARAMS_GET_RESPONSE_BODY_FOR_INTERCEPTION_1,
                new Object[] {interceptionId},
                true);
    }

    @Override
    public SecurityIsolationStatus getSecurityIsolationStatus() {
        return (SecurityIsolationStatus) handler.invoke(
                this,
                DomainCommand.Network_getSecurityIsolationStatus,
                CRT_GET_SECURITY_ISOLATION_STATUS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public SecurityIsolationStatus getSecurityIsolationStatus(String frameId) {
        return (SecurityIsolationStatus) handler.invoke(
                this,
                DomainCommand.Network_getSecurityIsolationStatus,
                CRT_GET_SECURITY_ISOLATION_STATUS,
                PARAMS_GET_SECURITY_ISOLATION_STATUS_2,
                new Object[] {frameId},
                true);
    }

    @Override
    public LoadNetworkResourcePageResult loadNetworkResource(String url, LoadNetworkResourceOptions options) {
        return (LoadNetworkResourcePageResult) handler.invoke(
                this,
                DomainCommand.Network_loadNetworkResource,
                CRT_LOAD_NETWORK_RESOURCE,
                PARAMS_LOAD_NETWORK_RESOURCE_1,
                new Object[] {url, options},
                true);
    }

    @Override
    public LoadNetworkResourcePageResult loadNetworkResource(
            String frameId, String url, LoadNetworkResourceOptions options) {
        return (LoadNetworkResourcePageResult) handler.invoke(
                this,
                DomainCommand.Network_loadNetworkResource,
                CRT_LOAD_NETWORK_RESOURCE,
                PARAMS_LOAD_NETWORK_RESOURCE_2,
                new Object[] {frameId, url, options},
                true);
    }

    @Override
    public void replayXHR(String requestId) {
        handler.invoke(
                this,
                DomainCommand.Network_replayXHR,
                CRT_REPLAY_XH_R,
                PARAMS_REPLAY_XH_R_1,
                new Object[] {requestId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<SearchMatch> searchInResponseBody(String requestId, String query) {
        return (List<SearchMatch>) handler.invoke(
                this,
                DomainCommand.Network_searchInResponseBody,
                CRT_SEARCH_IN_RESPONSE_BODY,
                PARAMS_SEARCH_IN_RESPONSE_BODY_1,
                new Object[] {requestId, query},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<SearchMatch> searchInResponseBody(
            String requestId, String query, Boolean caseSensitive, Boolean isRegex) {
        return (List<SearchMatch>) handler.invoke(
                this,
                DomainCommand.Network_searchInResponseBody,
                CRT_SEARCH_IN_RESPONSE_BODY,
                PARAMS_SEARCH_IN_RESPONSE_BODY_2,
                new Object[] {requestId, query, caseSensitive, isRegex},
                true);
    }

    @Override
    public void setAcceptedEncodings(ContentEncoding encodings) {
        handler.invoke(
                this,
                DomainCommand.Network_setAcceptedEncodings,
                CRT_SET_ACCEPTED_ENCODINGS,
                PARAMS_SET_ACCEPTED_ENCODINGS_1,
                new Object[] {encodings},
                true);
    }

    @Override
    public void setAttachDebugStack(Boolean enabled) {
        handler.invoke(
                this,
                DomainCommand.Network_setAttachDebugStack,
                CRT_SET_ATTACH_DEBUG_STACK,
                PARAMS_SET_ATTACH_DEBUG_STACK_1,
                new Object[] {enabled},
                true);
    }

    @Override
    public void setBlockedURLs(List<String> urls) {
        handler.invoke(
                this,
                DomainCommand.Network_setBlockedURLs,
                CRT_SET_BLOCKED_UR_LS,
                PARAMS_SET_BLOCKED_UR_LS_1,
                new Object[] {urls},
                true);
    }

    @Override
    public void setBypassServiceWorker(Boolean bypass) {
        handler.invoke(
                this,
                DomainCommand.Network_setBypassServiceWorker,
                CRT_SET_BYPASS_SERVICE_WORKER,
                PARAMS_SET_BYPASS_SERVICE_WORKER_1,
                new Object[] {bypass},
                true);
    }

    @Override
    public void setCacheDisabled(Boolean cacheDisabled) {
        handler.invoke(
                this,
                DomainCommand.Network_setCacheDisabled,
                CRT_SET_CACHE_DISABLED,
                PARAMS_SET_CACHE_DISABLED_1,
                new Object[] {cacheDisabled},
                true);
    }

    @Override
    public Boolean setCookie(String name, String value) {
        return (Boolean) handler.invoke(
                this,
                DomainCommand.Network_setCookie,
                CRT_SET_COOKIE,
                PARAMS_SET_COOKIE_1,
                new Object[] {name, value},
                true);
    }

    @Override
    public Boolean setCookie(
            String name,
            String value,
            String url,
            String domain,
            String path,
            Boolean secure,
            Boolean httpOnly,
            CookieSameSite sameSite,
            Double expires,
            CookiePriority priority,
            Boolean sameParty,
            CookieSourceScheme sourceScheme,
            Integer sourcePort,
            CookiePartitionKey partitionKey) {
        return (Boolean) handler.invoke(
                this,
                DomainCommand.Network_setCookie,
                CRT_SET_COOKIE,
                PARAMS_SET_COOKIE_2,
                new Object[] {
                    name,
                    value,
                    url,
                    domain,
                    path,
                    secure,
                    httpOnly,
                    sameSite,
                    expires,
                    priority,
                    sameParty,
                    sourceScheme,
                    sourcePort,
                    partitionKey
                },
                true);
    }

    @Override
    public void setCookieControls(
            Boolean enableThirdPartyCookieRestriction,
            Boolean disableThirdPartyCookieMetadata,
            Boolean disableThirdPartyCookieHeuristics) {
        handler.invoke(
                this,
                DomainCommand.Network_setCookieControls,
                CRT_SET_COOKIE_CONTROLS,
                PARAMS_SET_COOKIE_CONTROLS_1,
                new Object[] {
                    enableThirdPartyCookieRestriction,
                    disableThirdPartyCookieMetadata,
                    disableThirdPartyCookieHeuristics
                },
                true);
    }

    @Override
    public void setCookies(List<CookieParam> cookies) {
        handler.invoke(
                this,
                DomainCommand.Network_setCookies,
                CRT_SET_COOKIES,
                PARAMS_SET_COOKIES_1,
                new Object[] {cookies},
                true);
    }

    @Override
    public void setExtraHTTPHeaders(Map<String, Object> headers) {
        handler.invoke(
                this,
                DomainCommand.Network_setExtraHTTPHeaders,
                CRT_SET_EXTRA_HT_TP_HEADERS,
                PARAMS_SET_EXTRA_HT_TP_HEADERS_1,
                new Object[] {headers},
                true);
    }

    @Override
    public void setRequestInterception(List<RequestPattern> patterns) {
        handler.invoke(
                this,
                DomainCommand.Network_setRequestInterception,
                CRT_SET_REQUEST_INTERCEPTION,
                PARAMS_SET_REQUEST_INTERCEPTION_1,
                new Object[] {patterns},
                true);
    }

    @Override
    public void setUserAgentOverride(String userAgent) {
        handler.invoke(
                this,
                DomainCommand.Network_setUserAgentOverride,
                CRT_SET_USER_AGENT_OVERRIDE,
                PARAMS_SET_USER_AGENT_OVERRIDE_1,
                new Object[] {userAgent},
                true);
    }

    @Override
    public void setUserAgentOverride(
            String userAgent, String acceptLanguage, String platform, UserAgentMetadata userAgentMetadata) {
        handler.invoke(
                this,
                DomainCommand.Network_setUserAgentOverride,
                CRT_SET_USER_AGENT_OVERRIDE,
                PARAMS_SET_USER_AGENT_OVERRIDE_2,
                new Object[] {userAgent, acceptLanguage, platform, userAgentMetadata},
                true);
    }

    @Override
    public String streamResourceContent(String requestId) {
        return (String) handler.invoke(
                this,
                DomainCommand.Network_streamResourceContent,
                CRT_STREAM_RESOURCE_CONTENT,
                PARAMS_STREAM_RESOURCE_CONTENT_1,
                new Object[] {requestId},
                true);
    }

    @Override
    public String takeResponseBodyForInterceptionAsStream(String interceptionId) {
        return (String) handler.invoke(
                this,
                DomainCommand.Network_takeResponseBodyForInterceptionAsStream,
                CRT_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM,
                PARAMS_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM_1,
                new Object[] {interceptionId},
                true);
    }
}
