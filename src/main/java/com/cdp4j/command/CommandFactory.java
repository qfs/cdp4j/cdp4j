// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.SessionInvocationHandler;

public class CommandFactory {
    private Accessibility accessibility;

    private Animation animation;

    private Audits audits;

    private Autofill autofill;

    private BackgroundService backgroundService;

    private BluetoothEmulation bluetoothEmulation;

    private Browser browser;

    private CSS css;

    private CacheStorage cacheStorage;

    private Cast cast;

    private Console console;

    private CustomCommand customCommand;

    private DOM dom;

    private DOMDebugger domDebugger;

    private DOMSnapshot domSnapshot;

    private DOMStorage domStorage;

    private Debugger debugger;

    private DeviceAccess deviceAccess;

    private DeviceOrientation deviceOrientation;

    private Emulation emulation;

    private EventBreakpoints eventBreakpoints;

    private Extensions extensions;

    private FedCm fedCm;

    private Fetch fetch;

    private FileSystem fileSystem;

    private HeadlessExperimental headlessExperimental;

    private HeapProfiler heapProfiler;

    private IO io;

    private IndexedDB indexedDB;

    private Input input;

    private Inspector inspector;

    private LayerTree layerTree;

    private Log log;

    private Media media;

    private Memory memory;

    private Network network;

    private Overlay overlay;

    private PWA pwa;

    private Page page;

    private Performance performance;

    private PerformanceTimeline performanceTimeline;

    private Preload preload;

    private Profiler profiler;

    private Runtime runtime;

    private Schema schema;

    private Security security;

    private ServiceWorker serviceWorker;

    private Storage storage;

    private SystemInfo systemInfo;

    private Target target;

    private Tethering tethering;

    private Tracing tracing;

    private WebAudio webAudio;

    private WebAuthn webAuthn;

    private final SessionInvocationHandler handler;

    public CommandFactory(final SessionInvocationHandler handler) {
        this.handler = handler;
    }

    public Accessibility getAccessibility() {
        if (accessibility == null) {
            return accessibility = new AccessibilityImpl(handler);
        }
        return accessibility;
    }

    public Animation getAnimation() {
        if (animation == null) {
            return animation = new AnimationImpl(handler);
        }
        return animation;
    }

    public Audits getAudits() {
        if (audits == null) {
            return audits = new AuditsImpl(handler);
        }
        return audits;
    }

    public Autofill getAutofill() {
        if (autofill == null) {
            return autofill = new AutofillImpl(handler);
        }
        return autofill;
    }

    public BackgroundService getBackgroundService() {
        if (backgroundService == null) {
            return backgroundService = new BackgroundServiceImpl(handler);
        }
        return backgroundService;
    }

    public BluetoothEmulation getBluetoothEmulation() {
        if (bluetoothEmulation == null) {
            return bluetoothEmulation = new BluetoothEmulationImpl(handler);
        }
        return bluetoothEmulation;
    }

    public Browser getBrowser() {
        if (browser == null) {
            return browser = new BrowserImpl(handler);
        }
        return browser;
    }

    public CSS getCSS() {
        if (css == null) {
            return css = new CSSImpl(handler);
        }
        return css;
    }

    public CacheStorage getCacheStorage() {
        if (cacheStorage == null) {
            return cacheStorage = new CacheStorageImpl(handler);
        }
        return cacheStorage;
    }

    public Cast getCast() {
        if (cast == null) {
            return cast = new CastImpl(handler);
        }
        return cast;
    }

    public Console getConsole() {
        if (console == null) {
            return console = new ConsoleImpl(handler);
        }
        return console;
    }

    public CustomCommand getCustomCommand() {
        if (customCommand == null) {
            return customCommand = new CustomCommandImpl(handler);
        }
        return customCommand;
    }

    public DOM getDOM() {
        if (dom == null) {
            return dom = new DOMImpl(handler);
        }
        return dom;
    }

    public DOMDebugger getDOMDebugger() {
        if (domDebugger == null) {
            return domDebugger = new DOMDebuggerImpl(handler);
        }
        return domDebugger;
    }

    public DOMSnapshot getDOMSnapshot() {
        if (domSnapshot == null) {
            return domSnapshot = new DOMSnapshotImpl(handler);
        }
        return domSnapshot;
    }

    public DOMStorage getDOMStorage() {
        if (domStorage == null) {
            return domStorage = new DOMStorageImpl(handler);
        }
        return domStorage;
    }

    public Debugger getDebugger() {
        if (debugger == null) {
            return debugger = new DebuggerImpl(handler);
        }
        return debugger;
    }

    public DeviceAccess getDeviceAccess() {
        if (deviceAccess == null) {
            return deviceAccess = new DeviceAccessImpl(handler);
        }
        return deviceAccess;
    }

    public DeviceOrientation getDeviceOrientation() {
        if (deviceOrientation == null) {
            return deviceOrientation = new DeviceOrientationImpl(handler);
        }
        return deviceOrientation;
    }

    public Emulation getEmulation() {
        if (emulation == null) {
            return emulation = new EmulationImpl(handler);
        }
        return emulation;
    }

    public EventBreakpoints getEventBreakpoints() {
        if (eventBreakpoints == null) {
            return eventBreakpoints = new EventBreakpointsImpl(handler);
        }
        return eventBreakpoints;
    }

    public Extensions getExtensions() {
        if (extensions == null) {
            return extensions = new ExtensionsImpl(handler);
        }
        return extensions;
    }

    public FedCm getFedCm() {
        if (fedCm == null) {
            return fedCm = new FedCmImpl(handler);
        }
        return fedCm;
    }

    public Fetch getFetch() {
        if (fetch == null) {
            return fetch = new FetchImpl(handler);
        }
        return fetch;
    }

    public FileSystem getFileSystem() {
        if (fileSystem == null) {
            return fileSystem = new FileSystemImpl(handler);
        }
        return fileSystem;
    }

    public HeadlessExperimental getHeadlessExperimental() {
        if (headlessExperimental == null) {
            return headlessExperimental = new HeadlessExperimentalImpl(handler);
        }
        return headlessExperimental;
    }

    public HeapProfiler getHeapProfiler() {
        if (heapProfiler == null) {
            return heapProfiler = new HeapProfilerImpl(handler);
        }
        return heapProfiler;
    }

    public IO getIO() {
        if (io == null) {
            return io = new IOImpl(handler);
        }
        return io;
    }

    public IndexedDB getIndexedDB() {
        if (indexedDB == null) {
            return indexedDB = new IndexedDBImpl(handler);
        }
        return indexedDB;
    }

    public Input getInput() {
        if (input == null) {
            return input = new InputImpl(handler);
        }
        return input;
    }

    public Inspector getInspector() {
        if (inspector == null) {
            return inspector = new InspectorImpl(handler);
        }
        return inspector;
    }

    public LayerTree getLayerTree() {
        if (layerTree == null) {
            return layerTree = new LayerTreeImpl(handler);
        }
        return layerTree;
    }

    public Log getLog() {
        if (log == null) {
            return log = new LogImpl(handler);
        }
        return log;
    }

    public Media getMedia() {
        if (media == null) {
            return media = new MediaImpl(handler);
        }
        return media;
    }

    public Memory getMemory() {
        if (memory == null) {
            return memory = new MemoryImpl(handler);
        }
        return memory;
    }

    public Network getNetwork() {
        if (network == null) {
            return network = new NetworkImpl(handler);
        }
        return network;
    }

    public Overlay getOverlay() {
        if (overlay == null) {
            return overlay = new OverlayImpl(handler);
        }
        return overlay;
    }

    public PWA getPWA() {
        if (pwa == null) {
            return pwa = new PWAImpl(handler);
        }
        return pwa;
    }

    public Page getPage() {
        if (page == null) {
            return page = new PageImpl(handler);
        }
        return page;
    }

    public Performance getPerformance() {
        if (performance == null) {
            return performance = new PerformanceImpl(handler);
        }
        return performance;
    }

    public PerformanceTimeline getPerformanceTimeline() {
        if (performanceTimeline == null) {
            return performanceTimeline = new PerformanceTimelineImpl(handler);
        }
        return performanceTimeline;
    }

    public Preload getPreload() {
        if (preload == null) {
            return preload = new PreloadImpl(handler);
        }
        return preload;
    }

    public Profiler getProfiler() {
        if (profiler == null) {
            return profiler = new ProfilerImpl(handler);
        }
        return profiler;
    }

    public Runtime getRuntime() {
        if (runtime == null) {
            return runtime = new RuntimeImpl(handler);
        }
        return runtime;
    }

    public Schema getSchema() {
        if (schema == null) {
            return schema = new SchemaImpl(handler);
        }
        return schema;
    }

    public Security getSecurity() {
        if (security == null) {
            return security = new SecurityImpl(handler);
        }
        return security;
    }

    public ServiceWorker getServiceWorker() {
        if (serviceWorker == null) {
            return serviceWorker = new ServiceWorkerImpl(handler);
        }
        return serviceWorker;
    }

    public Storage getStorage() {
        if (storage == null) {
            return storage = new StorageImpl(handler);
        }
        return storage;
    }

    public SystemInfo getSystemInfo() {
        if (systemInfo == null) {
            return systemInfo = new SystemInfoImpl(handler);
        }
        return systemInfo;
    }

    public Target getTarget() {
        if (target == null) {
            return target = new TargetImpl(handler);
        }
        return target;
    }

    public Tethering getTethering() {
        if (tethering == null) {
            return tethering = new TetheringImpl(handler);
        }
        return tethering;
    }

    public Tracing getTracing() {
        if (tracing == null) {
            return tracing = new TracingImpl(handler);
        }
        return tracing;
    }

    public WebAudio getWebAudio() {
        if (webAudio == null) {
            return webAudio = new WebAudioImpl(handler);
        }
        return webAudio;
    }

    public WebAuthn getWebAuthn() {
        if (webAuthn == null) {
            return webAuthn = new WebAuthnImpl(handler);
        }
        return webAuthn;
    }
}
