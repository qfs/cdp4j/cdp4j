// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.dom.Rect;
import com.cdp4j.type.layertree.CompositingReasonsResult;
import com.cdp4j.type.layertree.PictureTile;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class LayerTreeAsyncImpl extends ParameterizedCommandImpl<LayerTreeAsync> implements LayerTreeAsync {

    private static final TypeReference<List<List<Double>>> LIST_LIST_DOUBLE =
            new TypeReference<List<List<Double>>>() {};
    private static final CommandReturnType CRT_COMPOSITING_REASONS =
            new CommandReturnType(null, CompositingReasonsResult.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_LOAD_SNAPSHOT = new CommandReturnType("snapshotId", String.class, null);
    private static final CommandReturnType CRT_MAKE_SNAPSHOT = new CommandReturnType("snapshotId", String.class, null);
    private static final CommandReturnType CRT_PROFILE_SNAPSHOT =
            new CommandReturnType("timings", List.class, LIST_LIST_DOUBLE);
    private static final CommandReturnType CRT_RELEASE_SNAPSHOT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REPLAY_SNAPSHOT = new CommandReturnType("dataURL", String.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_COMPOSITING_REASONS_1 = new String[] {"layerId"};
    private static final String[] PARAMS_LOAD_SNAPSHOT_1 = new String[] {"tiles"};
    private static final String[] PARAMS_MAKE_SNAPSHOT_1 = new String[] {"layerId"};
    private static final String[] PARAMS_PROFILE_SNAPSHOT_1 = new String[] {"snapshotId"};
    private static final String[] PARAMS_PROFILE_SNAPSHOT_2 =
            new String[] {"snapshotId", "minRepeatCount", "minDuration", "clipRect"};
    private static final String[] PARAMS_RELEASE_SNAPSHOT_1 = new String[] {"snapshotId"};
    private static final String[] PARAMS_REPLAY_SNAPSHOT_1 = new String[] {"snapshotId"};
    private static final String[] PARAMS_REPLAY_SNAPSHOT_2 = new String[] {"snapshotId", "fromStep", "toStep", "scale"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public LayerTreeAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CompositingReasonsResult> compositingReasons(String layerId) {
        return (CompletableFuture<CompositingReasonsResult>) handler.invoke(
                this,
                DomainCommand.LayerTree_compositingReasons,
                CRT_COMPOSITING_REASONS,
                PARAMS_COMPOSITING_REASONS_1,
                new Object[] {layerId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.LayerTree_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.LayerTree_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> loadSnapshot(List<PictureTile> tiles) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.LayerTree_loadSnapshot,
                CRT_LOAD_SNAPSHOT,
                PARAMS_LOAD_SNAPSHOT_1,
                new Object[] {tiles},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> makeSnapshot(String layerId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.LayerTree_makeSnapshot,
                CRT_MAKE_SNAPSHOT,
                PARAMS_MAKE_SNAPSHOT_1,
                new Object[] {layerId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<List<Double>>> profileSnapshot(String snapshotId) {
        return (CompletableFuture<List<List<Double>>>) handler.invoke(
                this,
                DomainCommand.LayerTree_profileSnapshot,
                CRT_PROFILE_SNAPSHOT,
                PARAMS_PROFILE_SNAPSHOT_1,
                new Object[] {snapshotId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<List<Double>>> profileSnapshot(
            String snapshotId, Integer minRepeatCount, Double minDuration, Rect clipRect) {
        return (CompletableFuture<List<List<Double>>>) handler.invoke(
                this,
                DomainCommand.LayerTree_profileSnapshot,
                CRT_PROFILE_SNAPSHOT,
                PARAMS_PROFILE_SNAPSHOT_2,
                new Object[] {snapshotId, minRepeatCount, minDuration, clipRect},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> releaseSnapshot(String snapshotId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.LayerTree_releaseSnapshot,
                CRT_RELEASE_SNAPSHOT,
                PARAMS_RELEASE_SNAPSHOT_1,
                new Object[] {snapshotId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> replaySnapshot(String snapshotId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.LayerTree_replaySnapshot,
                CRT_REPLAY_SNAPSHOT,
                PARAMS_REPLAY_SNAPSHOT_1,
                new Object[] {snapshotId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> replaySnapshot(String snapshotId, Integer fromStep, Integer toStep, Double scale) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.LayerTree_replaySnapshot,
                CRT_REPLAY_SNAPSHOT,
                PARAMS_REPLAY_SNAPSHOT_2,
                new Object[] {snapshotId, fromStep, toStep, scale},
                false);
    }
}
