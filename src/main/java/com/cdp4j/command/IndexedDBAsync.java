// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.indexeddb.DatabaseWithObjectStores;
import com.cdp4j.type.indexeddb.GetMetadataResult;
import com.cdp4j.type.indexeddb.KeyRange;
import com.cdp4j.type.indexeddb.RequestDataResult;
import com.cdp4j.type.storage.StorageBucket;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface IndexedDBAsync extends ParameterizedCommand<IndexedDBAsync> {
    /**
     * Clears all entries from an object store.
     *
     * @param databaseName
     *            Database name.
     * @param objectStoreName
     *            Object store name.
     */
    CompletableFuture<Void> clearObjectStore(String databaseName, String objectStoreName);

    /**
     * Clears all entries from an object store.
     *
     * @param securityOrigin
     *            At least and at most one of securityOrigin, storageKey, or
     *            storageBucket must be specified. Security origin.
     * @param storageKey
     *            Storage key.
     * @param storageBucket
     *            Storage bucket. If not specified, it uses the default bucket.
     * @param databaseName
     *            Database name.
     * @param objectStoreName
     *            Object store name.
     */
    CompletableFuture<Void> clearObjectStore(
            @Optional String securityOrigin,
            @Optional String storageKey,
            @Optional StorageBucket storageBucket,
            String databaseName,
            String objectStoreName);

    /**
     * Deletes a database.
     *
     * @param databaseName
     *            Database name.
     */
    CompletableFuture<Void> deleteDatabase(String databaseName);

    /**
     * Deletes a database.
     *
     * @param securityOrigin
     *            At least and at most one of securityOrigin, storageKey, or
     *            storageBucket must be specified. Security origin.
     * @param storageKey
     *            Storage key.
     * @param storageBucket
     *            Storage bucket. If not specified, it uses the default bucket.
     * @param databaseName
     *            Database name.
     */
    CompletableFuture<Void> deleteDatabase(
            @Optional String securityOrigin,
            @Optional String storageKey,
            @Optional StorageBucket storageBucket,
            String databaseName);

    /**
     * Delete a range of entries from an object store
     *
     * @param keyRange
     *            Range of entry keys to delete
     */
    CompletableFuture<Void> deleteObjectStoreEntries(String databaseName, String objectStoreName, KeyRange keyRange);

    /**
     * Delete a range of entries from an object store
     *
     * @param securityOrigin
     *            At least and at most one of securityOrigin, storageKey, or
     *            storageBucket must be specified. Security origin.
     * @param storageKey
     *            Storage key.
     * @param storageBucket
     *            Storage bucket. If not specified, it uses the default bucket.
     * @param keyRange
     *            Range of entry keys to delete
     */
    CompletableFuture<Void> deleteObjectStoreEntries(
            @Optional String securityOrigin,
            @Optional String storageKey,
            @Optional StorageBucket storageBucket,
            String databaseName,
            String objectStoreName,
            KeyRange keyRange);

    /**
     * Disables events from backend.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables events from backend.
     */
    CompletableFuture<Void> enable();

    /**
     * Gets metadata of an object store.
     *
     * @param databaseName
     *            Database name.
     * @param objectStoreName
     *            Object store name.
     *
     * @return GetMetadataResult
     */
    CompletableFuture<GetMetadataResult> getMetadata(String databaseName, String objectStoreName);

    /**
     * Gets metadata of an object store.
     *
     * @param securityOrigin
     *            At least and at most one of securityOrigin, storageKey, or
     *            storageBucket must be specified. Security origin.
     * @param storageKey
     *            Storage key.
     * @param storageBucket
     *            Storage bucket. If not specified, it uses the default bucket.
     * @param databaseName
     *            Database name.
     * @param objectStoreName
     *            Object store name.
     *
     * @return GetMetadataResult
     */
    CompletableFuture<GetMetadataResult> getMetadata(
            @Optional String securityOrigin,
            @Optional String storageKey,
            @Optional StorageBucket storageBucket,
            String databaseName,
            String objectStoreName);

    /**
     * Requests data from object store or index.
     *
     * @param databaseName
     *            Database name.
     * @param objectStoreName
     *            Object store name.
     * @param indexName
     *            Index name, empty string for object store data requests.
     * @param skipCount
     *            Number of records to skip.
     * @param pageSize
     *            Number of records to fetch.
     *
     * @return RequestDataResult
     */
    CompletableFuture<RequestDataResult> requestData(
            String databaseName, String objectStoreName, String indexName, Integer skipCount, Integer pageSize);

    /**
     * Requests data from object store or index.
     *
     * @param securityOrigin
     *            At least and at most one of securityOrigin, storageKey, or
     *            storageBucket must be specified. Security origin.
     * @param storageKey
     *            Storage key.
     * @param storageBucket
     *            Storage bucket. If not specified, it uses the default bucket.
     * @param databaseName
     *            Database name.
     * @param objectStoreName
     *            Object store name.
     * @param indexName
     *            Index name, empty string for object store data requests.
     * @param skipCount
     *            Number of records to skip.
     * @param pageSize
     *            Number of records to fetch.
     * @param keyRange
     *            Key range.
     *
     * @return RequestDataResult
     */
    CompletableFuture<RequestDataResult> requestData(
            @Optional String securityOrigin,
            @Optional String storageKey,
            @Optional StorageBucket storageBucket,
            String databaseName,
            String objectStoreName,
            String indexName,
            Integer skipCount,
            Integer pageSize,
            @Optional KeyRange keyRange);

    /**
     * Requests database with given name in given frame.
     *
     * @param databaseName
     *            Database name.
     *
     * @return Database with an array of object stores.
     */
    CompletableFuture<DatabaseWithObjectStores> requestDatabase(String databaseName);

    /**
     * Requests database with given name in given frame.
     *
     * @param securityOrigin
     *            At least and at most one of securityOrigin, storageKey, or
     *            storageBucket must be specified. Security origin.
     * @param storageKey
     *            Storage key.
     * @param storageBucket
     *            Storage bucket. If not specified, it uses the default bucket.
     * @param databaseName
     *            Database name.
     *
     * @return Database with an array of object stores.
     */
    CompletableFuture<DatabaseWithObjectStores> requestDatabase(
            @Optional String securityOrigin,
            @Optional String storageKey,
            @Optional StorageBucket storageBucket,
            String databaseName);

    /**
     * Requests database names for given security origin.
     *
     * @return Database names for origin.
     */
    CompletableFuture<List<String>> requestDatabaseNames();

    /**
     * Requests database names for given security origin.
     *
     * @param securityOrigin
     *            At least and at most one of securityOrigin, storageKey, or
     *            storageBucket must be specified. Security origin.
     * @param storageKey
     *            Storage key.
     * @param storageBucket
     *            Storage bucket. If not specified, it uses the default bucket.
     *
     * @return Database names for origin.
     */
    CompletableFuture<List<String>> requestDatabaseNames(
            @Optional String securityOrigin, @Optional String storageKey, @Optional StorageBucket storageBucket);
}
