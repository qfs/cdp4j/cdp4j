// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.EmulatedVisionDeficiency;
import com.cdp4j.type.constant.Platform;
import com.cdp4j.type.dom.RGBA;
import com.cdp4j.type.emulation.DevicePosture;
import com.cdp4j.type.emulation.DisabledImageType;
import com.cdp4j.type.emulation.DisplayFeature;
import com.cdp4j.type.emulation.MediaFeature;
import com.cdp4j.type.emulation.PressureMetadata;
import com.cdp4j.type.emulation.PressureSource;
import com.cdp4j.type.emulation.PressureState;
import com.cdp4j.type.emulation.ScreenOrientation;
import com.cdp4j.type.emulation.SensorMetadata;
import com.cdp4j.type.emulation.SensorReading;
import com.cdp4j.type.emulation.SensorType;
import com.cdp4j.type.emulation.UserAgentMetadata;
import com.cdp4j.type.emulation.VirtualTimePolicy;
import com.cdp4j.type.page.Viewport;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class EmulationAsyncImpl extends ParameterizedCommandImpl<EmulationAsync> implements EmulationAsync {

    private static final CommandReturnType CRT_CAN_EMULATE = new CommandReturnType("result", Boolean.class, null);
    private static final CommandReturnType CRT_CLEAR_DEVICE_METRICS_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_DEVICE_POSTURE_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_GEOLOCATION_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_IDLE_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_OVERRIDDEN_SENSOR_INFORMATION =
            new CommandReturnType("requestedSamplingFrequency", Double.class, null);
    private static final CommandReturnType CRT_RESET_PAGE_SCALE_FACTOR = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_AUTOMATION_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_AUTO_DARK_MODE_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_CP_UTHROTTLING_RATE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DEFAULT_BACKGROUND_COLOR_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DEVICE_METRICS_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DEVICE_POSTURE_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DISABLED_IMAGE_TYPES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DOCUMENT_COOKIE_DISABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_EMIT_TOUCH_EVENTS_FOR_MOUSE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_EMULATED_MEDIA = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_EMULATED_VISION_DEFICIENCY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_FOCUS_EMULATION_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_GEOLOCATION_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_HARDWARE_CONCURRENCY_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_IDLE_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_LOCALE_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_NAVIGATOR_OVERRIDES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PAGE_SCALE_FACTOR = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PRESSURE_SOURCE_OVERRIDE_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PRESSURE_STATE_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SCRIPT_EXECUTION_DISABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SCROLLBARS_HIDDEN = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SENSOR_OVERRIDE_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SENSOR_OVERRIDE_READINGS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_TIMEZONE_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_TOUCH_EMULATION_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_USER_AGENT_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_VIRTUAL_TIME_POLICY =
            new CommandReturnType("virtualTimeTicksBase", Double.class, null);
    private static final CommandReturnType CRT_SET_VISIBLE_SIZE = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_OVERRIDDEN_SENSOR_INFORMATION_1 = new String[] {"type"};
    private static final String[] PARAMS_SET_AUTOMATION_OVERRIDE_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_AUTO_DARK_MODE_OVERRIDE_2 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_CP_UTHROTTLING_RATE_1 = new String[] {"rate"};
    private static final String[] PARAMS_SET_DEFAULT_BACKGROUND_COLOR_OVERRIDE_2 = new String[] {"color"};
    private static final String[] PARAMS_SET_DEVICE_METRICS_OVERRIDE_1 =
            new String[] {"width", "height", "deviceScaleFactor", "mobile"};
    private static final String[] PARAMS_SET_DEVICE_METRICS_OVERRIDE_2 = new String[] {
        "width",
        "height",
        "deviceScaleFactor",
        "mobile",
        "scale",
        "screenWidth",
        "screenHeight",
        "positionX",
        "positionY",
        "dontSetVisibleSize",
        "screenOrientation",
        "viewport",
        "displayFeature",
        "devicePosture"
    };
    private static final String[] PARAMS_SET_DEVICE_POSTURE_OVERRIDE_1 = new String[] {"posture"};
    private static final String[] PARAMS_SET_DISABLED_IMAGE_TYPES_1 = new String[] {"imageTypes"};
    private static final String[] PARAMS_SET_DOCUMENT_COOKIE_DISABLED_1 = new String[] {"disabled"};
    private static final String[] PARAMS_SET_EMIT_TOUCH_EVENTS_FOR_MOUSE_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_EMIT_TOUCH_EVENTS_FOR_MOUSE_2 = new String[] {"enabled", "configuration"};
    private static final String[] PARAMS_SET_EMULATED_MEDIA_2 = new String[] {"media", "features"};
    private static final String[] PARAMS_SET_EMULATED_VISION_DEFICIENCY_1 = new String[] {"type"};
    private static final String[] PARAMS_SET_FOCUS_EMULATION_ENABLED_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_GEOLOCATION_OVERRIDE_2 =
            new String[] {"latitude", "longitude", "accuracy"};
    private static final String[] PARAMS_SET_HARDWARE_CONCURRENCY_OVERRIDE_1 = new String[] {"hardwareConcurrency"};
    private static final String[] PARAMS_SET_IDLE_OVERRIDE_1 = new String[] {"isUserActive", "isScreenUnlocked"};
    private static final String[] PARAMS_SET_LOCALE_OVERRIDE_2 = new String[] {"locale"};
    private static final String[] PARAMS_SET_NAVIGATOR_OVERRIDES_1 = new String[] {"platform"};
    private static final String[] PARAMS_SET_PAGE_SCALE_FACTOR_1 = new String[] {"pageScaleFactor"};
    private static final String[] PARAMS_SET_PRESSURE_SOURCE_OVERRIDE_ENABLED_1 = new String[] {"enabled", "source"};
    private static final String[] PARAMS_SET_PRESSURE_SOURCE_OVERRIDE_ENABLED_2 =
            new String[] {"enabled", "source", "metadata"};
    private static final String[] PARAMS_SET_PRESSURE_STATE_OVERRIDE_1 = new String[] {"source", "state"};
    private static final String[] PARAMS_SET_SCRIPT_EXECUTION_DISABLED_1 = new String[] {"value"};
    private static final String[] PARAMS_SET_SCROLLBARS_HIDDEN_1 = new String[] {"hidden"};
    private static final String[] PARAMS_SET_SENSOR_OVERRIDE_ENABLED_1 = new String[] {"enabled", "type"};
    private static final String[] PARAMS_SET_SENSOR_OVERRIDE_ENABLED_2 = new String[] {"enabled", "type", "metadata"};
    private static final String[] PARAMS_SET_SENSOR_OVERRIDE_READINGS_1 = new String[] {"type", "reading"};
    private static final String[] PARAMS_SET_TIMEZONE_OVERRIDE_1 = new String[] {"timezoneId"};
    private static final String[] PARAMS_SET_TOUCH_EMULATION_ENABLED_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_TOUCH_EMULATION_ENABLED_2 = new String[] {"enabled", "maxTouchPoints"};
    private static final String[] PARAMS_SET_USER_AGENT_OVERRIDE_1 = new String[] {"userAgent"};
    private static final String[] PARAMS_SET_USER_AGENT_OVERRIDE_2 =
            new String[] {"userAgent", "acceptLanguage", "platform", "userAgentMetadata"};
    private static final String[] PARAMS_SET_VIRTUAL_TIME_POLICY_1 = new String[] {"policy"};
    private static final String[] PARAMS_SET_VIRTUAL_TIME_POLICY_2 =
            new String[] {"policy", "budget", "maxVirtualTimeTaskStarvationCount", "initialVirtualTime"};
    private static final String[] PARAMS_SET_VISIBLE_SIZE_1 = new String[] {"width", "height"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public EmulationAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> canEmulate() {
        return (CompletableFuture<Boolean>) handler.invoke(
                this, DomainCommand.Emulation_canEmulate, CRT_CAN_EMULATE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearDeviceMetricsOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_clearDeviceMetricsOverride,
                CRT_CLEAR_DEVICE_METRICS_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearDevicePostureOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_clearDevicePostureOverride,
                CRT_CLEAR_DEVICE_POSTURE_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearGeolocationOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_clearGeolocationOverride,
                CRT_CLEAR_GEOLOCATION_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearIdleOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_clearIdleOverride,
                CRT_CLEAR_IDLE_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Double> getOverriddenSensorInformation(SensorType type) {
        return (CompletableFuture<Double>) handler.invoke(
                this,
                DomainCommand.Emulation_getOverriddenSensorInformation,
                CRT_GET_OVERRIDDEN_SENSOR_INFORMATION,
                PARAMS_GET_OVERRIDDEN_SENSOR_INFORMATION_1,
                new Object[] {type},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> resetPageScaleFactor() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_resetPageScaleFactor,
                CRT_RESET_PAGE_SCALE_FACTOR,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAutoDarkModeOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setAutoDarkModeOverride,
                CRT_SET_AUTO_DARK_MODE_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAutoDarkModeOverride(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setAutoDarkModeOverride,
                CRT_SET_AUTO_DARK_MODE_OVERRIDE,
                PARAMS_SET_AUTO_DARK_MODE_OVERRIDE_2,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAutomationOverride(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setAutomationOverride,
                CRT_SET_AUTOMATION_OVERRIDE,
                PARAMS_SET_AUTOMATION_OVERRIDE_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setCPUThrottlingRate(Double rate) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setCPUThrottlingRate,
                CRT_SET_CP_UTHROTTLING_RATE,
                PARAMS_SET_CP_UTHROTTLING_RATE_1,
                new Object[] {rate},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDefaultBackgroundColorOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setDefaultBackgroundColorOverride,
                CRT_SET_DEFAULT_BACKGROUND_COLOR_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDefaultBackgroundColorOverride(RGBA color) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setDefaultBackgroundColorOverride,
                CRT_SET_DEFAULT_BACKGROUND_COLOR_OVERRIDE,
                PARAMS_SET_DEFAULT_BACKGROUND_COLOR_OVERRIDE_2,
                new Object[] {color},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDeviceMetricsOverride(
            Integer width, Integer height, Double deviceScaleFactor, Boolean mobile) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setDeviceMetricsOverride,
                CRT_SET_DEVICE_METRICS_OVERRIDE,
                PARAMS_SET_DEVICE_METRICS_OVERRIDE_1,
                new Object[] {width, height, deviceScaleFactor, mobile},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDeviceMetricsOverride(
            Integer width,
            Integer height,
            Double deviceScaleFactor,
            Boolean mobile,
            Double scale,
            Integer screenWidth,
            Integer screenHeight,
            Integer positionX,
            Integer positionY,
            Boolean dontSetVisibleSize,
            ScreenOrientation screenOrientation,
            Viewport viewport,
            DisplayFeature displayFeature,
            DevicePosture devicePosture) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setDeviceMetricsOverride,
                CRT_SET_DEVICE_METRICS_OVERRIDE,
                PARAMS_SET_DEVICE_METRICS_OVERRIDE_2,
                new Object[] {
                    width,
                    height,
                    deviceScaleFactor,
                    mobile,
                    scale,
                    screenWidth,
                    screenHeight,
                    positionX,
                    positionY,
                    dontSetVisibleSize,
                    screenOrientation,
                    viewport,
                    displayFeature,
                    devicePosture
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDevicePostureOverride(DevicePosture posture) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setDevicePostureOverride,
                CRT_SET_DEVICE_POSTURE_OVERRIDE,
                PARAMS_SET_DEVICE_POSTURE_OVERRIDE_1,
                new Object[] {posture},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDisabledImageTypes(DisabledImageType imageTypes) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setDisabledImageTypes,
                CRT_SET_DISABLED_IMAGE_TYPES,
                PARAMS_SET_DISABLED_IMAGE_TYPES_1,
                new Object[] {imageTypes},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDocumentCookieDisabled(Boolean disabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setDocumentCookieDisabled,
                CRT_SET_DOCUMENT_COOKIE_DISABLED,
                PARAMS_SET_DOCUMENT_COOKIE_DISABLED_1,
                new Object[] {disabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEmitTouchEventsForMouse(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setEmitTouchEventsForMouse,
                CRT_SET_EMIT_TOUCH_EVENTS_FOR_MOUSE,
                PARAMS_SET_EMIT_TOUCH_EVENTS_FOR_MOUSE_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEmitTouchEventsForMouse(Boolean enabled, Platform configuration) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setEmitTouchEventsForMouse,
                CRT_SET_EMIT_TOUCH_EVENTS_FOR_MOUSE,
                PARAMS_SET_EMIT_TOUCH_EVENTS_FOR_MOUSE_2,
                new Object[] {enabled, configuration},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEmulatedMedia() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setEmulatedMedia,
                CRT_SET_EMULATED_MEDIA,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEmulatedMedia(String media, List<MediaFeature> features) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setEmulatedMedia,
                CRT_SET_EMULATED_MEDIA,
                PARAMS_SET_EMULATED_MEDIA_2,
                new Object[] {media, features},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEmulatedVisionDeficiency(EmulatedVisionDeficiency type) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setEmulatedVisionDeficiency,
                CRT_SET_EMULATED_VISION_DEFICIENCY,
                PARAMS_SET_EMULATED_VISION_DEFICIENCY_1,
                new Object[] {type},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setFocusEmulationEnabled(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setFocusEmulationEnabled,
                CRT_SET_FOCUS_EMULATION_ENABLED,
                PARAMS_SET_FOCUS_EMULATION_ENABLED_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setGeolocationOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setGeolocationOverride,
                CRT_SET_GEOLOCATION_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setGeolocationOverride(Double latitude, Double longitude, Double accuracy) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setGeolocationOverride,
                CRT_SET_GEOLOCATION_OVERRIDE,
                PARAMS_SET_GEOLOCATION_OVERRIDE_2,
                new Object[] {latitude, longitude, accuracy},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setHardwareConcurrencyOverride(Integer hardwareConcurrency) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setHardwareConcurrencyOverride,
                CRT_SET_HARDWARE_CONCURRENCY_OVERRIDE,
                PARAMS_SET_HARDWARE_CONCURRENCY_OVERRIDE_1,
                new Object[] {hardwareConcurrency},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setIdleOverride(Boolean isUserActive, Boolean isScreenUnlocked) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setIdleOverride,
                CRT_SET_IDLE_OVERRIDE,
                PARAMS_SET_IDLE_OVERRIDE_1,
                new Object[] {isUserActive, isScreenUnlocked},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setLocaleOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setLocaleOverride,
                CRT_SET_LOCALE_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setLocaleOverride(String locale) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setLocaleOverride,
                CRT_SET_LOCALE_OVERRIDE,
                PARAMS_SET_LOCALE_OVERRIDE_2,
                new Object[] {locale},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setNavigatorOverrides(String platform) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setNavigatorOverrides,
                CRT_SET_NAVIGATOR_OVERRIDES,
                PARAMS_SET_NAVIGATOR_OVERRIDES_1,
                new Object[] {platform},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPageScaleFactor(Double pageScaleFactor) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setPageScaleFactor,
                CRT_SET_PAGE_SCALE_FACTOR,
                PARAMS_SET_PAGE_SCALE_FACTOR_1,
                new Object[] {pageScaleFactor},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPressureSourceOverrideEnabled(Boolean enabled, PressureSource source) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setPressureSourceOverrideEnabled,
                CRT_SET_PRESSURE_SOURCE_OVERRIDE_ENABLED,
                PARAMS_SET_PRESSURE_SOURCE_OVERRIDE_ENABLED_1,
                new Object[] {enabled, source},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPressureSourceOverrideEnabled(
            Boolean enabled, PressureSource source, PressureMetadata metadata) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setPressureSourceOverrideEnabled,
                CRT_SET_PRESSURE_SOURCE_OVERRIDE_ENABLED,
                PARAMS_SET_PRESSURE_SOURCE_OVERRIDE_ENABLED_2,
                new Object[] {enabled, source, metadata},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPressureStateOverride(PressureSource source, PressureState state) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setPressureStateOverride,
                CRT_SET_PRESSURE_STATE_OVERRIDE,
                PARAMS_SET_PRESSURE_STATE_OVERRIDE_1,
                new Object[] {source, state},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setScriptExecutionDisabled(Boolean value) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setScriptExecutionDisabled,
                CRT_SET_SCRIPT_EXECUTION_DISABLED,
                PARAMS_SET_SCRIPT_EXECUTION_DISABLED_1,
                new Object[] {value},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setScrollbarsHidden(Boolean hidden) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setScrollbarsHidden,
                CRT_SET_SCROLLBARS_HIDDEN,
                PARAMS_SET_SCROLLBARS_HIDDEN_1,
                new Object[] {hidden},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSensorOverrideEnabled(Boolean enabled, SensorType type) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setSensorOverrideEnabled,
                CRT_SET_SENSOR_OVERRIDE_ENABLED,
                PARAMS_SET_SENSOR_OVERRIDE_ENABLED_1,
                new Object[] {enabled, type},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSensorOverrideEnabled(Boolean enabled, SensorType type, SensorMetadata metadata) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setSensorOverrideEnabled,
                CRT_SET_SENSOR_OVERRIDE_ENABLED,
                PARAMS_SET_SENSOR_OVERRIDE_ENABLED_2,
                new Object[] {enabled, type, metadata},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSensorOverrideReadings(SensorType type, SensorReading reading) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setSensorOverrideReadings,
                CRT_SET_SENSOR_OVERRIDE_READINGS,
                PARAMS_SET_SENSOR_OVERRIDE_READINGS_1,
                new Object[] {type, reading},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setTimezoneOverride(String timezoneId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setTimezoneOverride,
                CRT_SET_TIMEZONE_OVERRIDE,
                PARAMS_SET_TIMEZONE_OVERRIDE_1,
                new Object[] {timezoneId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setTouchEmulationEnabled(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setTouchEmulationEnabled,
                CRT_SET_TOUCH_EMULATION_ENABLED,
                PARAMS_SET_TOUCH_EMULATION_ENABLED_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setTouchEmulationEnabled(Boolean enabled, Integer maxTouchPoints) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setTouchEmulationEnabled,
                CRT_SET_TOUCH_EMULATION_ENABLED,
                PARAMS_SET_TOUCH_EMULATION_ENABLED_2,
                new Object[] {enabled, maxTouchPoints},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setUserAgentOverride(String userAgent) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setUserAgentOverride,
                CRT_SET_USER_AGENT_OVERRIDE,
                PARAMS_SET_USER_AGENT_OVERRIDE_1,
                new Object[] {userAgent},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setUserAgentOverride(
            String userAgent, String acceptLanguage, String platform, UserAgentMetadata userAgentMetadata) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setUserAgentOverride,
                CRT_SET_USER_AGENT_OVERRIDE,
                PARAMS_SET_USER_AGENT_OVERRIDE_2,
                new Object[] {userAgent, acceptLanguage, platform, userAgentMetadata},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Double> setVirtualTimePolicy(VirtualTimePolicy policy) {
        return (CompletableFuture<Double>) handler.invoke(
                this,
                DomainCommand.Emulation_setVirtualTimePolicy,
                CRT_SET_VIRTUAL_TIME_POLICY,
                PARAMS_SET_VIRTUAL_TIME_POLICY_1,
                new Object[] {policy},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Double> setVirtualTimePolicy(
            VirtualTimePolicy policy,
            Double budget,
            Integer maxVirtualTimeTaskStarvationCount,
            Double initialVirtualTime) {
        return (CompletableFuture<Double>) handler.invoke(
                this,
                DomainCommand.Emulation_setVirtualTimePolicy,
                CRT_SET_VIRTUAL_TIME_POLICY,
                PARAMS_SET_VIRTUAL_TIME_POLICY_2,
                new Object[] {policy, budget, maxVirtualTimeTaskStarvationCount, initialVirtualTime},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setVisibleSize(Integer width, Integer height) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Emulation_setVisibleSize,
                CRT_SET_VISIBLE_SIZE,
                PARAMS_SET_VISIBLE_SIZE_1,
                new Object[] {width, height},
                false);
    }
}
