// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.network.Cookie;
import com.cdp4j.type.network.CookieParam;
import com.cdp4j.type.storage.GetUsageAndQuotaResult;
import com.cdp4j.type.storage.RelatedWebsiteSet;
import com.cdp4j.type.storage.SharedStorageEntry;
import com.cdp4j.type.storage.SharedStorageMetadata;
import com.cdp4j.type.storage.StorageBucket;
import com.cdp4j.type.storage.TrustTokens;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface StorageAsync extends ParameterizedCommand<StorageAsync> {
    /**
     * Clears cookies.
     */
    CompletableFuture<Void> clearCookies();

    /**
     * Clears cookies.
     *
     * @param browserContextId
     *            Browser context to use when called on the browser endpoint.
     */
    CompletableFuture<Void> clearCookies(@Optional String browserContextId);

    /**
     * Clears storage for origin.
     *
     * @param origin
     *            Security origin.
     * @param storageTypes
     *            Comma separated list of StorageType to clear.
     */
    CompletableFuture<Void> clearDataForOrigin(String origin, String storageTypes);

    /**
     * Clears storage for storage key.
     *
     * @param storageKey
     *            Storage key.
     * @param storageTypes
     *            Comma separated list of StorageType to clear.
     */
    CompletableFuture<Void> clearDataForStorageKey(String storageKey, String storageTypes);

    /**
     * Clears all entries for a given origin's shared storage.
     *
     */
    @Experimental
    CompletableFuture<Void> clearSharedStorageEntries(String ownerOrigin);

    /**
     * Removes all Trust Tokens issued by the provided issuerOrigin. Leaves other
     * stored data, including the issuer's Redemption Records, intact.
     *
     *
     * @return True if any tokens were deleted, false otherwise.
     */
    @Experimental
    CompletableFuture<Boolean> clearTrustTokens(String issuerOrigin);

    /**
     * Deletes entry for key (if it exists) for a given origin's shared storage.
     *
     */
    @Experimental
    CompletableFuture<Void> deleteSharedStorageEntry(String ownerOrigin, String key);

    /**
     * Deletes the Storage Bucket with the given storage key and bucket name.
     *
     */
    @Experimental
    CompletableFuture<Void> deleteStorageBucket(StorageBucket bucket);

    /**
     * Returns the list of URLs from a page and its embedded resources that match
     * existing grace period URL pattern rules.
     * https://developers.google.com/privacy-sandbox/cookies/temporary-exceptions/grace-period
     *
     * @param firstPartyUrl
     *            The URL of the page currently being visited.
     * @param thirdPartyUrls
     *            The list of embedded resource URLs from the page.
     *
     * @return Array of matching URLs. If there is a primary pattern match for the
     *         first- party URL, only the first-party URL is returned in the array.
     */
    @Experimental
    CompletableFuture<List<String>> getAffectedUrlsForThirdPartyCookieMetadata(
            String firstPartyUrl, List<String> thirdPartyUrls);

    /**
     * Returns all browser cookies.
     *
     * @return Array of cookie objects.
     */
    CompletableFuture<List<Cookie>> getCookies();

    /**
     * Returns all browser cookies.
     *
     * @param browserContextId
     *            Browser context to use when called on the browser endpoint.
     *
     * @return Array of cookie objects.
     */
    CompletableFuture<List<Cookie>> getCookies(@Optional String browserContextId);

    /**
     * Returns the effective Related Website Sets in use by this profile for the
     * browser session. The effective Related Website Sets will not change during a
     * browser session.
     */
    @Experimental
    CompletableFuture<List<RelatedWebsiteSet>> getRelatedWebsiteSets();

    /**
     * Gets the entries in an given origin's shared storage.
     *
     */
    @Experimental
    CompletableFuture<List<SharedStorageEntry>> getSharedStorageEntries(String ownerOrigin);

    /**
     * Gets metadata for an origin's shared storage.
     *
     */
    @Experimental
    CompletableFuture<SharedStorageMetadata> getSharedStorageMetadata(String ownerOrigin);

    /**
     * Returns a storage key given a frame id.
     *
     */
    CompletableFuture<String> getStorageKeyForFrame(String frameId);

    /**
     * Returns the number of stored Trust Tokens per issuer for the current browsing
     * context.
     */
    @Experimental
    CompletableFuture<List<TrustTokens>> getTrustTokens();

    /**
     * Returns usage and quota in bytes.
     *
     * @param origin
     *            Security origin.
     *
     * @return GetUsageAndQuotaResult
     */
    CompletableFuture<GetUsageAndQuotaResult> getUsageAndQuota(String origin);

    /**
     * Override quota for the specified origin
     *
     * @param origin
     *            Security origin.
     */
    @Experimental
    CompletableFuture<Void> overrideQuotaForOrigin(String origin);

    /**
     * Override quota for the specified origin
     *
     * @param origin
     *            Security origin.
     * @param quotaSize
     *            The quota size (in bytes) to override the original quota with. If
     *            this is called multiple times, the overridden quota will be equal
     *            to the quotaSize provided in the final call. If this is called
     *            without specifying a quotaSize, the quota will be reset to the
     *            default value for the specified origin. If this is called multiple
     *            times with different origins, the override will be maintained for
     *            each origin until it is disabled (called without a quotaSize).
     */
    @Experimental
    CompletableFuture<Void> overrideQuotaForOrigin(String origin, @Optional Double quotaSize);

    /**
     * Resets the budget for ownerOrigin by clearing all budget withdrawals.
     *
     */
    @Experimental
    CompletableFuture<Void> resetSharedStorageBudget(String ownerOrigin);

    /**
     * Deletes state for sites identified as potential bounce trackers, immediately.
     */
    @Experimental
    CompletableFuture<List<String>> runBounceTrackingMitigations();

    /**
     * Sends all pending Attribution Reports immediately, regardless of their
     * scheduled report time.
     *
     * @return The number of reports that were sent.
     */
    @Experimental
    CompletableFuture<Integer> sendPendingAttributionReports();

    /**
     * https://wicg.github.io/attribution-reporting-api/
     *
     * @param enabled
     *            If enabled, noise is suppressed and reports are sent immediately.
     */
    @Experimental
    CompletableFuture<Void> setAttributionReportingLocalTestingMode(Boolean enabled);

    /**
     * Enables/disables issuing of Attribution Reporting events.
     *
     */
    @Experimental
    CompletableFuture<Void> setAttributionReportingTracking(Boolean enable);

    /**
     * Sets given cookies.
     *
     * @param cookies
     *            Cookies to be set.
     */
    CompletableFuture<Void> setCookies(List<CookieParam> cookies);

    /**
     * Sets given cookies.
     *
     * @param cookies
     *            Cookies to be set.
     * @param browserContextId
     *            Browser context to use when called on the browser endpoint.
     */
    CompletableFuture<Void> setCookies(List<CookieParam> cookies, @Optional String browserContextId);

    /**
     * Enables/Disables issuing of interestGroupAuctionEventOccurred and
     * interestGroupAuctionNetworkRequestCreated.
     *
     */
    @Experimental
    CompletableFuture<Void> setInterestGroupAuctionTracking(Boolean enable);

    /**
     * Enables/Disables issuing of interestGroupAccessed events.
     *
     */
    @Experimental
    CompletableFuture<Void> setInterestGroupTracking(Boolean enable);

    /**
     * Sets entry with key and value for a given origin's shared storage.
     *
     */
    @Experimental
    CompletableFuture<Void> setSharedStorageEntry(String ownerOrigin, String key, String value);

    /**
     * Sets entry with key and value for a given origin's shared storage.
     *
     * @param ignoreIfPresent
     *            If ignoreIfPresent is included and true, then only sets the entry
     *            if key doesn't already exist.
     */
    @Experimental
    CompletableFuture<Void> setSharedStorageEntry(
            String ownerOrigin, String key, String value, @Optional Boolean ignoreIfPresent);

    /**
     * Enables/disables issuing of sharedStorageAccessed events.
     *
     */
    @Experimental
    CompletableFuture<Void> setSharedStorageTracking(Boolean enable);

    /**
     * Set tracking for a storage key's buckets.
     *
     */
    @Experimental
    CompletableFuture<Void> setStorageBucketTracking(String storageKey, Boolean enable);

    /**
     * Registers origin to be notified when an update occurs to its cache storage
     * list.
     *
     * @param origin
     *            Security origin.
     */
    CompletableFuture<Void> trackCacheStorageForOrigin(String origin);

    /**
     * Registers storage key to be notified when an update occurs to its cache
     * storage list.
     *
     * @param storageKey
     *            Storage key.
     */
    CompletableFuture<Void> trackCacheStorageForStorageKey(String storageKey);

    /**
     * Registers origin to be notified when an update occurs to its IndexedDB.
     *
     * @param origin
     *            Security origin.
     */
    CompletableFuture<Void> trackIndexedDBForOrigin(String origin);

    /**
     * Registers storage key to be notified when an update occurs to its IndexedDB.
     *
     * @param storageKey
     *            Storage key.
     */
    CompletableFuture<Void> trackIndexedDBForStorageKey(String storageKey);

    /**
     * Unregisters origin from receiving notifications for cache storage.
     *
     * @param origin
     *            Security origin.
     */
    CompletableFuture<Void> untrackCacheStorageForOrigin(String origin);

    /**
     * Unregisters storage key from receiving notifications for cache storage.
     *
     * @param storageKey
     *            Storage key.
     */
    CompletableFuture<Void> untrackCacheStorageForStorageKey(String storageKey);

    /**
     * Unregisters origin from receiving notifications for IndexedDB.
     *
     * @param origin
     *            Security origin.
     */
    CompletableFuture<Void> untrackIndexedDBForOrigin(String origin);

    /**
     * Unregisters storage key from receiving notifications for IndexedDB.
     *
     * @param storageKey
     *            Storage key.
     */
    CompletableFuture<Void> untrackIndexedDBForStorageKey(String storageKey);
}
