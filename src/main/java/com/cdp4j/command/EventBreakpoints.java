// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

/**
 * EventBreakpoints permits setting JavaScript breakpoints on operations and events
 * occurring in native code invoked from JavaScript. Once breakpoint is hit, it is
 * reported through Debugger domain, similarly to regular breakpoints being hit.
 */
@Experimental
public interface EventBreakpoints extends ParameterizedCommand<EventBreakpoints> {
    /**
     * Removes all breakpoints
     */
    void disable();

    /**
     * Removes breakpoint on particular native event.
     *
     * @param eventName Instrumentation name to stop on.
     */
    void removeInstrumentationBreakpoint(String eventName);

    /**
     * Sets breakpoint on particular native event.
     *
     * @param eventName Instrumentation name to stop on.
     */
    void setInstrumentationBreakpoint(String eventName);
}
