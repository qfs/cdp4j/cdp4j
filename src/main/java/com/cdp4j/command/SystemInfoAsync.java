// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.systeminfo.GetInfoResult;
import com.cdp4j.type.systeminfo.ProcessInfo;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * The SystemInfo domain defines methods and events for querying low-level
 * system information.
 */
@Experimental
public interface SystemInfoAsync extends ParameterizedCommand<SystemInfoAsync> {
    /**
     * Returns information about the feature state.
     *
     */
    CompletableFuture<Boolean> getFeatureState(String featureState);

    /**
     * Returns information about the system.
     *
     * @return GetInfoResult
     */
    CompletableFuture<GetInfoResult> getInfo();

    /**
     * Returns information about all running processes.
     *
     * @return An array of process info blocks.
     */
    CompletableFuture<List<ProcessInfo>> getProcessInfo();
}
