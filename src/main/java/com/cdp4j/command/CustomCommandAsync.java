// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.domsnapshot.GetSnapshotResult;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This domain contains commands which are only used internally
 */
public interface CustomCommandAsync extends ParameterizedCommand<CustomCommandAsync> {
    /**
     * Returns a document snapshot, including the full DOM tree of the root node
     * (including iframes, template contents, and imported documents) in a flattened
     * array, as well as layout and white-listed computed style information for the
     * nodes. Shadow DOM in the returned DOM tree is flattened.
     *
     * @param computedStyleWhitelist
     *            Whitelist of computed styles to return.
     *
     * @return GetSnapshotResult
     */
    @Deprecated
    CompletableFuture<GetSnapshotResult> getSnapshot(List<String> computedStyleWhitelist);
}
