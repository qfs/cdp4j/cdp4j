// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.target.FilterEntry;
import com.cdp4j.type.target.RemoteLocation;
import com.cdp4j.type.target.TargetInfo;
import com.cdp4j.type.target.WindowState;
import java.util.List;

/**
 * Supports additional targets discovery and allows to attach to them.
 */
public interface Target extends ParameterizedCommand<Target> {
    /**
     * Activates (focuses) the target.
     *
     */
    void activateTarget(String targetId);

    /**
     * Attaches to the browser target, only uses flat sessionId mode.
     *
     * @return Id assigned to the session.
     */
    @Experimental
    String attachToBrowserTarget();

    /**
     * Attaches to the target with given id.
     *
     *
     * @return Id assigned to the session.
     */
    String attachToTarget(String targetId);

    /**
     * Attaches to the target with given id.
     *
     * @param flatten Enables "flat" access to the session via specifying sessionId attribute in the commands.
     * We plan to make this the default, deprecate non-flattened mode,
     * and eventually retire it. See crbug.com/991325.
     *
     * @return Id assigned to the session.
     */
    String attachToTarget(String targetId, @Optional Boolean flatten);

    /**
     * Adds the specified target to the list of targets that will be monitored for any related target
     * creation (such as child frames, child workers and new versions of service worker) and reported
     * through attachedToTarget. The specified target is also auto-attached.
     * This cancels the effect of any previous setAutoAttach and is also cancelled by subsequent
     * setAutoAttach. Only available at the Browser target.
     *
     * @param waitForDebuggerOnStart Whether to pause new targets when attaching to them. Use Runtime.runIfWaitingForDebugger
     * to run paused targets.
     */
    @Experimental
    void autoAttachRelated(String targetId, Boolean waitForDebuggerOnStart);

    /**
     * Adds the specified target to the list of targets that will be monitored for any related target
     * creation (such as child frames, child workers and new versions of service worker) and reported
     * through attachedToTarget. The specified target is also auto-attached.
     * This cancels the effect of any previous setAutoAttach and is also cancelled by subsequent
     * setAutoAttach. Only available at the Browser target.
     *
     * @param waitForDebuggerOnStart Whether to pause new targets when attaching to them. Use Runtime.runIfWaitingForDebugger
     * to run paused targets.
     * @param filter Only targets matching filter will be attached.
     */
    @Experimental
    void autoAttachRelated(
            String targetId, Boolean waitForDebuggerOnStart, @Experimental @Optional List<FilterEntry> filter);

    /**
     * Closes the target. If the target is a page that gets closed too.
     *
     *
     * @return Always set to true. If an error occurs, the response indicates protocol error.
     */
    Boolean closeTarget(String targetId);

    /**
     * Creates a new empty BrowserContext. Similar to an incognito profile but you can have more than
     * one.
     *
     * @return The id of the context created.
     */
    String createBrowserContext();

    /**
     * Creates a new empty BrowserContext. Similar to an incognito profile but you can have more than
     * one.
     *
     * @param disposeOnDetach If specified, disposes this context when debugging session disconnects.
     * @param proxyServer Proxy server, similar to the one passed to --proxy-server
     * @param proxyBypassList Proxy bypass list, similar to the one passed to --proxy-bypass-list
     * @param originsWithUniversalNetworkAccess An optional list of origins to grant unlimited cross-origin access to.
     * Parts of the URL other than those constituting origin are ignored.
     *
     * @return The id of the context created.
     */
    String createBrowserContext(
            @Experimental @Optional Boolean disposeOnDetach,
            @Experimental @Optional String proxyServer,
            @Experimental @Optional String proxyBypassList,
            @Experimental @Optional List<String> originsWithUniversalNetworkAccess);

    /**
     * Creates a new page.
     *
     * @param url The initial URL the page will be navigated to. An empty string indicates about:blank.
     *
     * @return The id of the page opened.
     */
    String createTarget(String url);

    /**
     * Creates a new page.
     *
     * @param url The initial URL the page will be navigated to. An empty string indicates about:blank.
     * @param left Frame left origin in DIP (requires newWindow to be true or headless shell).
     * @param top Frame top origin in DIP (requires newWindow to be true or headless shell).
     * @param width Frame width in DIP (requires newWindow to be true or headless shell).
     * @param height Frame height in DIP (requires newWindow to be true or headless shell).
     * @param windowState Frame window state (requires newWindow to be true or headless shell).
     * Default is normal.
     * @param browserContextId The browser context to create the page in.
     * @param enableBeginFrameControl Whether BeginFrames for this target will be controlled via DevTools (headless shell only,
     * not supported on MacOS yet, false by default).
     * @param newWindow Whether to create a new Window or Tab (false by default, not supported by headless shell).
     * @param background Whether to create the target in background or foreground (false by default, not supported
     * by headless shell).
     * @param forTab Whether to create the target of type "tab".
     *
     * @return The id of the page opened.
     */
    String createTarget(
            String url,
            @Experimental @Optional Integer left,
            @Experimental @Optional Integer top,
            @Optional Integer width,
            @Optional Integer height,
            @Optional WindowState windowState,
            @Experimental @Optional String browserContextId,
            @Experimental @Optional Boolean enableBeginFrameControl,
            @Optional Boolean newWindow,
            @Optional Boolean background,
            @Experimental @Optional Boolean forTab);

    /**
     * Detaches session with given id.
     */
    void detachFromTarget();

    /**
     * Detaches session with given id.
     *
     * @param sessionId Session to detach.
     * @param targetId Deprecated.
     */
    void detachFromTarget(@Optional String sessionId, @Optional @Deprecated String targetId);

    /**
     * Deletes a BrowserContext. All the belonging pages will be closed without calling their
     * beforeunload hooks.
     *
     */
    void disposeBrowserContext(String browserContextId);

    /**
     * Inject object to the target's main frame that provides a communication
     * channel with browser target.
     *
     * Injected object will be available as window[bindingName].
     *
     * The object has the following API:
     * - binding.send(json) - a method to send messages over the remote debugging protocol
     * - binding.onmessage = json => handleMessage(json) - a callback that will be called for the protocol notifications and command responses.
     *
     */
    @Experimental
    void exposeDevToolsProtocol(String targetId);

    /**
     * Inject object to the target's main frame that provides a communication
     * channel with browser target.
     *
     * Injected object will be available as window[bindingName].
     *
     * The object has the following API:
     * - binding.send(json) - a method to send messages over the remote debugging protocol
     * - binding.onmessage = json => handleMessage(json) - a callback that will be called for the protocol notifications and command responses.
     *
     * @param bindingName Binding name, 'cdp' if not specified.
     * @param inheritPermissions If true, inherits the current root session's permissions (default: false).
     */
    @Experimental
    void exposeDevToolsProtocol(String targetId, @Optional String bindingName, @Optional Boolean inheritPermissions);

    /**
     * Returns all browser contexts created with Target.createBrowserContext method.
     *
     * @return An array of browser context ids.
     */
    List<String> getBrowserContexts();

    /**
     * Returns information about a target.
     */
    @Experimental
    TargetInfo getTargetInfo();

    /**
     * Returns information about a target.
     *
     */
    @Experimental
    TargetInfo getTargetInfo(@Optional String targetId);

    /**
     * Retrieves a list of available targets.
     *
     * @return The list of targets.
     */
    List<TargetInfo> getTargets();

    /**
     * Retrieves a list of available targets.
     *
     * @param filter Only targets matching filter will be reported. If filter is not specified
     * and target discovery is currently enabled, a filter used for target discovery
     * is used for consistency.
     *
     * @return The list of targets.
     */
    List<TargetInfo> getTargets(@Experimental @Optional List<FilterEntry> filter);

    /**
     * Sends protocol message over session with given id.
     * Consider using flat mode instead; see commands attachToTarget, setAutoAttach,
     * and crbug.com/991325.
     *
     */
    @Deprecated
    void sendMessageToTarget(String message);

    /**
     * Sends protocol message over session with given id.
     * Consider using flat mode instead; see commands attachToTarget, setAutoAttach,
     * and crbug.com/991325.
     *
     * @param sessionId Identifier of the session.
     * @param targetId Deprecated.
     */
    @Deprecated
    void sendMessageToTarget(String message, @Optional String sessionId, @Optional @Deprecated String targetId);

    /**
     * Controls whether to automatically attach to new targets which are considered to be related to
     * this one. When turned on, attaches to all existing related targets as well. When turned off,
     * automatically detaches from all currently attached targets.
     * This also clears all targets added by autoAttachRelated from the list of targets to watch
     * for creation of related targets.
     *
     * @param autoAttach Whether to auto-attach to related targets.
     * @param waitForDebuggerOnStart Whether to pause new targets when attaching to them. Use Runtime.runIfWaitingForDebugger
     * to run paused targets.
     */
    void setAutoAttach(Boolean autoAttach, Boolean waitForDebuggerOnStart);

    /**
     * Controls whether to automatically attach to new targets which are considered to be related to
     * this one. When turned on, attaches to all existing related targets as well. When turned off,
     * automatically detaches from all currently attached targets.
     * This also clears all targets added by autoAttachRelated from the list of targets to watch
     * for creation of related targets.
     *
     * @param autoAttach Whether to auto-attach to related targets.
     * @param waitForDebuggerOnStart Whether to pause new targets when attaching to them. Use Runtime.runIfWaitingForDebugger
     * to run paused targets.
     * @param flatten Enables "flat" access to the session via specifying sessionId attribute in the commands.
     * We plan to make this the default, deprecate non-flattened mode,
     * and eventually retire it. See crbug.com/991325.
     * @param filter Only targets matching filter will be attached.
     */
    void setAutoAttach(
            Boolean autoAttach,
            Boolean waitForDebuggerOnStart,
            @Experimental @Optional Boolean flatten,
            @Experimental @Optional List<FilterEntry> filter);

    /**
     * Controls whether to discover available targets and notify via
     * targetCreated/targetInfoChanged/targetDestroyed events.
     *
     * @param discover Whether to discover available targets.
     */
    void setDiscoverTargets(Boolean discover);

    /**
     * Controls whether to discover available targets and notify via
     * targetCreated/targetInfoChanged/targetDestroyed events.
     *
     * @param discover Whether to discover available targets.
     * @param filter Only targets matching filter will be attached. If discover is false,
     * filter must be omitted or empty.
     */
    void setDiscoverTargets(Boolean discover, @Experimental @Optional List<FilterEntry> filter);

    /**
     * Enables target discovery for the specified locations, when setDiscoverTargets was set to
     * true.
     *
     * @param locations List of remote locations.
     */
    @Experimental
    void setRemoteLocations(List<RemoteLocation> locations);
}
