// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.heapprofiler.SamplingHeapProfile;
import com.cdp4j.type.runtime.RemoteObject;

class HeapProfilerImpl extends ParameterizedCommandImpl<HeapProfiler> implements HeapProfiler {

    private static final CommandReturnType CRT_ADD_INSPECTED_HEAP_OBJECT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_COLLECT_GARBAGE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_HEAP_OBJECT_ID =
            new CommandReturnType("heapSnapshotObjectId", String.class, null);
    private static final CommandReturnType CRT_GET_OBJECT_BY_HEAP_OBJECT_ID =
            new CommandReturnType("result", RemoteObject.class, null);
    private static final CommandReturnType CRT_GET_SAMPLING_PROFILE =
            new CommandReturnType("profile", SamplingHeapProfile.class, null);
    private static final CommandReturnType CRT_START_SAMPLING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_TRACKING_HEAP_OBJECTS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_SAMPLING =
            new CommandReturnType("profile", SamplingHeapProfile.class, null);
    private static final CommandReturnType CRT_STOP_TRACKING_HEAP_OBJECTS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TAKE_HEAP_SNAPSHOT = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ADD_INSPECTED_HEAP_OBJECT_1 = new String[] {"heapObjectId"};
    private static final String[] PARAMS_GET_HEAP_OBJECT_ID_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_2 = new String[] {"objectId", "objectGroup"};
    private static final String[] PARAMS_START_SAMPLING_2 =
            new String[] {"samplingInterval", "includeObjectsCollectedByMajorGC", "includeObjectsCollectedByMinorGC"};
    private static final String[] PARAMS_START_TRACKING_HEAP_OBJECTS_2 = new String[] {"trackAllocations"};
    private static final String[] PARAMS_STOP_TRACKING_HEAP_OBJECTS_2 =
            new String[] {"reportProgress", "treatGlobalObjectsAsRoots", "captureNumericValue", "exposeInternals"};
    private static final String[] PARAMS_TAKE_HEAP_SNAPSHOT_2 =
            new String[] {"reportProgress", "treatGlobalObjectsAsRoots", "captureNumericValue", "exposeInternals"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public HeapProfilerImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void addInspectedHeapObject(String heapObjectId) {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_addInspectedHeapObject,
                CRT_ADD_INSPECTED_HEAP_OBJECT,
                PARAMS_ADD_INSPECTED_HEAP_OBJECT_1,
                new Object[] {heapObjectId},
                true);
    }

    @Override
    public void collectGarbage() {
        handler.invoke(
                this, DomainCommand.HeapProfiler_collectGarbage, CRT_COLLECT_GARBAGE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.HeapProfiler_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.HeapProfiler_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public String getHeapObjectId(String objectId) {
        return (String) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getHeapObjectId,
                CRT_GET_HEAP_OBJECT_ID,
                PARAMS_GET_HEAP_OBJECT_ID_1,
                new Object[] {objectId},
                true);
    }

    @Override
    public RemoteObject getObjectByHeapObjectId(String objectId) {
        return (RemoteObject) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getObjectByHeapObjectId,
                CRT_GET_OBJECT_BY_HEAP_OBJECT_ID,
                PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_1,
                new Object[] {objectId},
                true);
    }

    @Override
    public RemoteObject getObjectByHeapObjectId(String objectId, String objectGroup) {
        return (RemoteObject) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getObjectByHeapObjectId,
                CRT_GET_OBJECT_BY_HEAP_OBJECT_ID,
                PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_2,
                new Object[] {objectId, objectGroup},
                true);
    }

    @Override
    public SamplingHeapProfile getSamplingProfile() {
        return (SamplingHeapProfile) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getSamplingProfile,
                CRT_GET_SAMPLING_PROFILE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void startSampling() {
        handler.invoke(
                this, DomainCommand.HeapProfiler_startSampling, CRT_START_SAMPLING, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void startSampling(
            Double samplingInterval,
            Boolean includeObjectsCollectedByMajorGC,
            Boolean includeObjectsCollectedByMinorGC) {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_startSampling,
                CRT_START_SAMPLING,
                PARAMS_START_SAMPLING_2,
                new Object[] {samplingInterval, includeObjectsCollectedByMajorGC, includeObjectsCollectedByMinorGC},
                true);
    }

    @Override
    public void startTrackingHeapObjects() {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_startTrackingHeapObjects,
                CRT_START_TRACKING_HEAP_OBJECTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void startTrackingHeapObjects(Boolean trackAllocations) {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_startTrackingHeapObjects,
                CRT_START_TRACKING_HEAP_OBJECTS,
                PARAMS_START_TRACKING_HEAP_OBJECTS_2,
                new Object[] {trackAllocations},
                true);
    }

    @Override
    public SamplingHeapProfile stopSampling() {
        return (SamplingHeapProfile) handler.invoke(
                this, DomainCommand.HeapProfiler_stopSampling, CRT_STOP_SAMPLING, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void stopTrackingHeapObjects() {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_stopTrackingHeapObjects,
                CRT_STOP_TRACKING_HEAP_OBJECTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void stopTrackingHeapObjects(
            Boolean reportProgress,
            Boolean treatGlobalObjectsAsRoots,
            Boolean captureNumericValue,
            Boolean exposeInternals) {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_stopTrackingHeapObjects,
                CRT_STOP_TRACKING_HEAP_OBJECTS,
                PARAMS_STOP_TRACKING_HEAP_OBJECTS_2,
                new Object[] {reportProgress, treatGlobalObjectsAsRoots, captureNumericValue, exposeInternals},
                true);
    }

    @Override
    public void takeHeapSnapshot() {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_takeHeapSnapshot,
                CRT_TAKE_HEAP_SNAPSHOT,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void takeHeapSnapshot(
            Boolean reportProgress,
            Boolean treatGlobalObjectsAsRoots,
            Boolean captureNumericValue,
            Boolean exposeInternals) {
        handler.invoke(
                this,
                DomainCommand.HeapProfiler_takeHeapSnapshot,
                CRT_TAKE_HEAP_SNAPSHOT,
                PARAMS_TAKE_HEAP_SNAPSHOT_2,
                new Object[] {reportProgress, treatGlobalObjectsAsRoots, captureNumericValue, exposeInternals},
                true);
    }
}
