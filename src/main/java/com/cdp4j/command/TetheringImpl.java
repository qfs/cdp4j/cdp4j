// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;

class TetheringImpl extends ParameterizedCommandImpl<Tethering> implements Tethering {

    private static final CommandReturnType CRT_BIND = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNBIND = new CommandReturnType(null, void.class, null);
    private static final String[] PARAMS_BIND_1 = new String[] {"port"};
    private static final String[] PARAMS_UNBIND_1 = new String[] {"port"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public TetheringImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void bind(Integer port) {
        handler.invoke(this, DomainCommand.Tethering_bind, CRT_BIND, PARAMS_BIND_1, new Object[] {port}, true);
    }

    @Override
    public void unbind(Integer port) {
        handler.invoke(this, DomainCommand.Tethering_unbind, CRT_UNBIND, PARAMS_UNBIND_1, new Object[] {port}, true);
    }
}
