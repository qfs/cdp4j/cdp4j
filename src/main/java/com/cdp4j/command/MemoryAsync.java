// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.memory.DOMCounter;
import com.cdp4j.type.memory.GetDOMCountersResult;
import com.cdp4j.type.memory.PressureLevel;
import com.cdp4j.type.memory.SamplingProfile;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface MemoryAsync extends ParameterizedCommand<MemoryAsync> {
    /**
     * Simulate OomIntervention by purging V8 memory.
     */
    CompletableFuture<Void> forciblyPurgeJavaScriptMemory();

    /**
     * Retrieve native memory allocations profile collected since renderer process
     * startup.
     */
    CompletableFuture<SamplingProfile> getAllTimeSamplingProfile();

    /**
     * Retrieve native memory allocations profile collected since browser process
     * startup.
     */
    CompletableFuture<SamplingProfile> getBrowserSamplingProfile();

    /**
     * Retruns current DOM object counters.
     *
     * @return GetDOMCountersResult
     */
    CompletableFuture<GetDOMCountersResult> getDOMCounters();

    /**
     * Retruns DOM object counters after preparing renderer for leak detection.
     *
     * @return DOM object counters.
     */
    CompletableFuture<List<DOMCounter>> getDOMCountersForLeakDetection();

    /**
     * Retrieve native memory allocations profile collected since last startSampling
     * call.
     */
    CompletableFuture<SamplingProfile> getSamplingProfile();

    /**
     * Prepares for leak detection by terminating workers, stopping spellcheckers,
     * dropping non-essential internal caches, running garbage collections, etc.
     */
    CompletableFuture<Void> prepareForLeakDetection();

    /**
     * Enable/disable suppressing memory pressure notifications in all processes.
     *
     * @param suppressed
     *            If true, memory pressure notifications will be suppressed.
     */
    CompletableFuture<Void> setPressureNotificationsSuppressed(Boolean suppressed);

    /**
     * Simulate a memory pressure notification in all processes.
     *
     * @param level
     *            Memory pressure level of the notification.
     */
    CompletableFuture<Void> simulatePressureNotification(PressureLevel level);

    /**
     * Start collecting native memory profile.
     */
    CompletableFuture<Void> startSampling();

    /**
     * Start collecting native memory profile.
     *
     * @param samplingInterval
     *            Average number of bytes between samples.
     * @param suppressRandomness
     *            Do not randomize intervals between samples.
     */
    CompletableFuture<Void> startSampling(@Optional Integer samplingInterval, @Optional Boolean suppressRandomness);

    /**
     * Stop collecting native memory profile.
     */
    CompletableFuture<Void> stopSampling();
}
