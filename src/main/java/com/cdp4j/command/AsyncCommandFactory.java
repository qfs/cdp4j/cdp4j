// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.SessionInvocationHandler;

public class AsyncCommandFactory {
    private AccessibilityAsync accessibility;

    private AnimationAsync animation;

    private AuditsAsync audits;

    private AutofillAsync autofill;

    private BackgroundServiceAsync backgroundService;

    private BluetoothEmulationAsync bluetoothEmulation;

    private BrowserAsync browser;

    private CSSAsync css;

    private CacheStorageAsync cacheStorage;

    private CastAsync cast;

    private ConsoleAsync console;

    private CustomCommandAsync customCommand;

    private DOMAsync dom;

    private DOMDebuggerAsync domDebugger;

    private DOMSnapshotAsync domSnapshot;

    private DOMStorageAsync domStorage;

    private DebuggerAsync debugger;

    private DeviceAccessAsync deviceAccess;

    private DeviceOrientationAsync deviceOrientation;

    private EmulationAsync emulation;

    private EventBreakpointsAsync eventBreakpoints;

    private ExtensionsAsync extensions;

    private FedCmAsync fedCm;

    private FetchAsync fetch;

    private FileSystemAsync fileSystem;

    private HeadlessExperimentalAsync headlessExperimental;

    private HeapProfilerAsync heapProfiler;

    private IOAsync io;

    private IndexedDBAsync indexedDB;

    private InputAsync input;

    private InspectorAsync inspector;

    private LayerTreeAsync layerTree;

    private LogAsync log;

    private MediaAsync media;

    private MemoryAsync memory;

    private NetworkAsync network;

    private OverlayAsync overlay;

    private PWAAsync pwa;

    private PageAsync page;

    private PerformanceAsync performance;

    private PerformanceTimelineAsync performanceTimeline;

    private PreloadAsync preload;

    private ProfilerAsync profiler;

    private RuntimeAsync runtime;

    private SchemaAsync schema;

    private SecurityAsync security;

    private ServiceWorkerAsync serviceWorker;

    private StorageAsync storage;

    private SystemInfoAsync systemInfo;

    private TargetAsync target;

    private TetheringAsync tethering;

    private TracingAsync tracing;

    private WebAudioAsync webAudio;

    private WebAuthnAsync webAuthn;

    private final SessionInvocationHandler handler;

    public AsyncCommandFactory(final SessionInvocationHandler handler) {
        this.handler = handler;
    }

    public AccessibilityAsync getAccessibility() {
        if (accessibility == null) {
            return accessibility = new AccessibilityAsyncImpl(handler);
        }
        return accessibility;
    }

    public AnimationAsync getAnimation() {
        if (animation == null) {
            return animation = new AnimationAsyncImpl(handler);
        }
        return animation;
    }

    public AuditsAsync getAudits() {
        if (audits == null) {
            return audits = new AuditsAsyncImpl(handler);
        }
        return audits;
    }

    public AutofillAsync getAutofill() {
        if (autofill == null) {
            return autofill = new AutofillAsyncImpl(handler);
        }
        return autofill;
    }

    public BackgroundServiceAsync getBackgroundService() {
        if (backgroundService == null) {
            return backgroundService = new BackgroundServiceAsyncImpl(handler);
        }
        return backgroundService;
    }

    public BluetoothEmulationAsync getBluetoothEmulation() {
        if (bluetoothEmulation == null) {
            return bluetoothEmulation = new BluetoothEmulationAsyncImpl(handler);
        }
        return bluetoothEmulation;
    }

    public BrowserAsync getBrowser() {
        if (browser == null) {
            return browser = new BrowserAsyncImpl(handler);
        }
        return browser;
    }

    public CSSAsync getCSS() {
        if (css == null) {
            return css = new CSSAsyncImpl(handler);
        }
        return css;
    }

    public CacheStorageAsync getCacheStorage() {
        if (cacheStorage == null) {
            return cacheStorage = new CacheStorageAsyncImpl(handler);
        }
        return cacheStorage;
    }

    public CastAsync getCast() {
        if (cast == null) {
            return cast = new CastAsyncImpl(handler);
        }
        return cast;
    }

    public ConsoleAsync getConsole() {
        if (console == null) {
            return console = new ConsoleAsyncImpl(handler);
        }
        return console;
    }

    public CustomCommandAsync getCustomCommand() {
        if (customCommand == null) {
            return customCommand = new CustomCommandAsyncImpl(handler);
        }
        return customCommand;
    }

    public DOMAsync getDOM() {
        if (dom == null) {
            return dom = new DOMAsyncImpl(handler);
        }
        return dom;
    }

    public DOMDebuggerAsync getDOMDebugger() {
        if (domDebugger == null) {
            return domDebugger = new DOMDebuggerAsyncImpl(handler);
        }
        return domDebugger;
    }

    public DOMSnapshotAsync getDOMSnapshot() {
        if (domSnapshot == null) {
            return domSnapshot = new DOMSnapshotAsyncImpl(handler);
        }
        return domSnapshot;
    }

    public DOMStorageAsync getDOMStorage() {
        if (domStorage == null) {
            return domStorage = new DOMStorageAsyncImpl(handler);
        }
        return domStorage;
    }

    public DebuggerAsync getDebugger() {
        if (debugger == null) {
            return debugger = new DebuggerAsyncImpl(handler);
        }
        return debugger;
    }

    public DeviceAccessAsync getDeviceAccess() {
        if (deviceAccess == null) {
            return deviceAccess = new DeviceAccessAsyncImpl(handler);
        }
        return deviceAccess;
    }

    public DeviceOrientationAsync getDeviceOrientation() {
        if (deviceOrientation == null) {
            return deviceOrientation = new DeviceOrientationAsyncImpl(handler);
        }
        return deviceOrientation;
    }

    public EmulationAsync getEmulation() {
        if (emulation == null) {
            return emulation = new EmulationAsyncImpl(handler);
        }
        return emulation;
    }

    public EventBreakpointsAsync getEventBreakpoints() {
        if (eventBreakpoints == null) {
            return eventBreakpoints = new EventBreakpointsAsyncImpl(handler);
        }
        return eventBreakpoints;
    }

    public ExtensionsAsync getExtensions() {
        if (extensions == null) {
            return extensions = new ExtensionsAsyncImpl(handler);
        }
        return extensions;
    }

    public FedCmAsync getFedCm() {
        if (fedCm == null) {
            return fedCm = new FedCmAsyncImpl(handler);
        }
        return fedCm;
    }

    public FetchAsync getFetch() {
        if (fetch == null) {
            return fetch = new FetchAsyncImpl(handler);
        }
        return fetch;
    }

    public FileSystemAsync getFileSystem() {
        if (fileSystem == null) {
            return fileSystem = new FileSystemAsyncImpl(handler);
        }
        return fileSystem;
    }

    public HeadlessExperimentalAsync getHeadlessExperimental() {
        if (headlessExperimental == null) {
            return headlessExperimental = new HeadlessExperimentalAsyncImpl(handler);
        }
        return headlessExperimental;
    }

    public HeapProfilerAsync getHeapProfiler() {
        if (heapProfiler == null) {
            return heapProfiler = new HeapProfilerAsyncImpl(handler);
        }
        return heapProfiler;
    }

    public IOAsync getIO() {
        if (io == null) {
            return io = new IOAsyncImpl(handler);
        }
        return io;
    }

    public IndexedDBAsync getIndexedDB() {
        if (indexedDB == null) {
            return indexedDB = new IndexedDBAsyncImpl(handler);
        }
        return indexedDB;
    }

    public InputAsync getInput() {
        if (input == null) {
            return input = new InputAsyncImpl(handler);
        }
        return input;
    }

    public InspectorAsync getInspector() {
        if (inspector == null) {
            return inspector = new InspectorAsyncImpl(handler);
        }
        return inspector;
    }

    public LayerTreeAsync getLayerTree() {
        if (layerTree == null) {
            return layerTree = new LayerTreeAsyncImpl(handler);
        }
        return layerTree;
    }

    public LogAsync getLog() {
        if (log == null) {
            return log = new LogAsyncImpl(handler);
        }
        return log;
    }

    public MediaAsync getMedia() {
        if (media == null) {
            return media = new MediaAsyncImpl(handler);
        }
        return media;
    }

    public MemoryAsync getMemory() {
        if (memory == null) {
            return memory = new MemoryAsyncImpl(handler);
        }
        return memory;
    }

    public NetworkAsync getNetwork() {
        if (network == null) {
            return network = new NetworkAsyncImpl(handler);
        }
        return network;
    }

    public OverlayAsync getOverlay() {
        if (overlay == null) {
            return overlay = new OverlayAsyncImpl(handler);
        }
        return overlay;
    }

    public PWAAsync getPWA() {
        if (pwa == null) {
            return pwa = new PWAAsyncImpl(handler);
        }
        return pwa;
    }

    public PageAsync getPage() {
        if (page == null) {
            return page = new PageAsyncImpl(handler);
        }
        return page;
    }

    public PerformanceAsync getPerformance() {
        if (performance == null) {
            return performance = new PerformanceAsyncImpl(handler);
        }
        return performance;
    }

    public PerformanceTimelineAsync getPerformanceTimeline() {
        if (performanceTimeline == null) {
            return performanceTimeline = new PerformanceTimelineAsyncImpl(handler);
        }
        return performanceTimeline;
    }

    public PreloadAsync getPreload() {
        if (preload == null) {
            return preload = new PreloadAsyncImpl(handler);
        }
        return preload;
    }

    public ProfilerAsync getProfiler() {
        if (profiler == null) {
            return profiler = new ProfilerAsyncImpl(handler);
        }
        return profiler;
    }

    public RuntimeAsync getRuntime() {
        if (runtime == null) {
            return runtime = new RuntimeAsyncImpl(handler);
        }
        return runtime;
    }

    public SchemaAsync getSchema() {
        if (schema == null) {
            return schema = new SchemaAsyncImpl(handler);
        }
        return schema;
    }

    public SecurityAsync getSecurity() {
        if (security == null) {
            return security = new SecurityAsyncImpl(handler);
        }
        return security;
    }

    public ServiceWorkerAsync getServiceWorker() {
        if (serviceWorker == null) {
            return serviceWorker = new ServiceWorkerAsyncImpl(handler);
        }
        return serviceWorker;
    }

    public StorageAsync getStorage() {
        if (storage == null) {
            return storage = new StorageAsyncImpl(handler);
        }
        return storage;
    }

    public SystemInfoAsync getSystemInfo() {
        if (systemInfo == null) {
            return systemInfo = new SystemInfoAsyncImpl(handler);
        }
        return systemInfo;
    }

    public TargetAsync getTarget() {
        if (target == null) {
            return target = new TargetAsyncImpl(handler);
        }
        return target;
    }

    public TetheringAsync getTethering() {
        if (tethering == null) {
            return tethering = new TetheringAsyncImpl(handler);
        }
        return tethering;
    }

    public TracingAsync getTracing() {
        if (tracing == null) {
            return tracing = new TracingAsyncImpl(handler);
        }
        return tracing;
    }

    public WebAudioAsync getWebAudio() {
        if (webAudio == null) {
            return webAudio = new WebAudioAsyncImpl(handler);
        }
        return webAudio;
    }

    public WebAuthnAsync getWebAuthn() {
        if (webAuthn == null) {
            return webAuthn = new WebAuthnAsyncImpl(handler);
        }
        return webAuthn;
    }
}
