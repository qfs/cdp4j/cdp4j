// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.fetch.AuthChallengeResponse;
import com.cdp4j.type.fetch.GetResponseBodyResult;
import com.cdp4j.type.fetch.HeaderEntry;
import com.cdp4j.type.fetch.RequestPattern;
import com.cdp4j.type.network.ErrorReason;
import java.util.List;

class FetchImpl extends ParameterizedCommandImpl<Fetch> implements Fetch {

    private static final CommandReturnType CRT_CONTINUE_REQUEST = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CONTINUE_RESPONSE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CONTINUE_WITH_AUTH = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_FAIL_REQUEST = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_FULFILL_REQUEST = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_RESPONSE_BODY =
            new CommandReturnType(null, GetResponseBodyResult.class, null);
    private static final CommandReturnType CRT_TAKE_RESPONSE_BODY_AS_STREAM =
            new CommandReturnType("stream", String.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CONTINUE_REQUEST_1 = new String[] {"requestId"};
    private static final String[] PARAMS_CONTINUE_REQUEST_2 =
            new String[] {"requestId", "url", "method", "postData", "headers", "interceptResponse"};
    private static final String[] PARAMS_CONTINUE_RESPONSE_1 = new String[] {"requestId"};
    private static final String[] PARAMS_CONTINUE_RESPONSE_2 =
            new String[] {"requestId", "responseCode", "responsePhrase", "responseHeaders", "binaryResponseHeaders"};
    private static final String[] PARAMS_CONTINUE_WITH_AUTH_1 = new String[] {"requestId", "authChallengeResponse"};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"patterns", "handleAuthRequests"};
    private static final String[] PARAMS_FAIL_REQUEST_1 = new String[] {"requestId", "errorReason"};
    private static final String[] PARAMS_FULFILL_REQUEST_1 = new String[] {"requestId", "responseCode"};
    private static final String[] PARAMS_FULFILL_REQUEST_2 = new String[] {
        "requestId", "responseCode", "responseHeaders", "binaryResponseHeaders", "body", "responsePhrase"
    };
    private static final String[] PARAMS_GET_RESPONSE_BODY_1 = new String[] {"requestId"};
    private static final String[] PARAMS_TAKE_RESPONSE_BODY_AS_STREAM_1 = new String[] {"requestId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public FetchImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void continueRequest(String requestId) {
        handler.invoke(
                this,
                DomainCommand.Fetch_continueRequest,
                CRT_CONTINUE_REQUEST,
                PARAMS_CONTINUE_REQUEST_1,
                new Object[] {requestId},
                true);
    }

    @Override
    public void continueRequest(
            String requestId,
            String url,
            String method,
            String postData,
            List<HeaderEntry> headers,
            Boolean interceptResponse) {
        handler.invoke(
                this,
                DomainCommand.Fetch_continueRequest,
                CRT_CONTINUE_REQUEST,
                PARAMS_CONTINUE_REQUEST_2,
                new Object[] {requestId, url, method, postData, headers, interceptResponse},
                true);
    }

    @Override
    public void continueResponse(String requestId) {
        handler.invoke(
                this,
                DomainCommand.Fetch_continueResponse,
                CRT_CONTINUE_RESPONSE,
                PARAMS_CONTINUE_RESPONSE_1,
                new Object[] {requestId},
                true);
    }

    @Override
    public void continueResponse(
            String requestId,
            Integer responseCode,
            String responsePhrase,
            List<HeaderEntry> responseHeaders,
            String binaryResponseHeaders) {
        handler.invoke(
                this,
                DomainCommand.Fetch_continueResponse,
                CRT_CONTINUE_RESPONSE,
                PARAMS_CONTINUE_RESPONSE_2,
                new Object[] {requestId, responseCode, responsePhrase, responseHeaders, binaryResponseHeaders},
                true);
    }

    @Override
    public void continueWithAuth(String requestId, AuthChallengeResponse authChallengeResponse) {
        handler.invoke(
                this,
                DomainCommand.Fetch_continueWithAuth,
                CRT_CONTINUE_WITH_AUTH,
                PARAMS_CONTINUE_WITH_AUTH_1,
                new Object[] {requestId, authChallengeResponse},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Fetch_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Fetch_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable(List<RequestPattern> patterns, Boolean handleAuthRequests) {
        handler.invoke(
                this,
                DomainCommand.Fetch_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_2,
                new Object[] {patterns, handleAuthRequests},
                true);
    }

    @Override
    public void failRequest(String requestId, ErrorReason errorReason) {
        handler.invoke(
                this,
                DomainCommand.Fetch_failRequest,
                CRT_FAIL_REQUEST,
                PARAMS_FAIL_REQUEST_1,
                new Object[] {requestId, errorReason},
                true);
    }

    @Override
    public void fulfillRequest(String requestId, Integer responseCode) {
        handler.invoke(
                this,
                DomainCommand.Fetch_fulfillRequest,
                CRT_FULFILL_REQUEST,
                PARAMS_FULFILL_REQUEST_1,
                new Object[] {requestId, responseCode},
                true);
    }

    @Override
    public void fulfillRequest(
            String requestId,
            Integer responseCode,
            List<HeaderEntry> responseHeaders,
            String binaryResponseHeaders,
            String body,
            String responsePhrase) {
        handler.invoke(
                this,
                DomainCommand.Fetch_fulfillRequest,
                CRT_FULFILL_REQUEST,
                PARAMS_FULFILL_REQUEST_2,
                new Object[] {requestId, responseCode, responseHeaders, binaryResponseHeaders, body, responsePhrase},
                true);
    }

    @Override
    public GetResponseBodyResult getResponseBody(String requestId) {
        return (GetResponseBodyResult) handler.invoke(
                this,
                DomainCommand.Fetch_getResponseBody,
                CRT_GET_RESPONSE_BODY,
                PARAMS_GET_RESPONSE_BODY_1,
                new Object[] {requestId},
                true);
    }

    @Override
    public String takeResponseBodyAsStream(String requestId) {
        return (String) handler.invoke(
                this,
                DomainCommand.Fetch_takeResponseBodyAsStream,
                CRT_TAKE_RESPONSE_BODY_AS_STREAM,
                PARAMS_TAKE_RESPONSE_BODY_AS_STREAM_1,
                new Object[] {requestId},
                true);
    }
}
