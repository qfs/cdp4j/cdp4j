// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.heapprofiler.SamplingHeapProfile;
import com.cdp4j.type.runtime.RemoteObject;
import java.util.concurrent.CompletableFuture;

class HeapProfilerAsyncImpl extends ParameterizedCommandImpl<HeapProfilerAsync> implements HeapProfilerAsync {

    private static final CommandReturnType CRT_ADD_INSPECTED_HEAP_OBJECT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_COLLECT_GARBAGE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_HEAP_OBJECT_ID =
            new CommandReturnType("heapSnapshotObjectId", String.class, null);
    private static final CommandReturnType CRT_GET_OBJECT_BY_HEAP_OBJECT_ID =
            new CommandReturnType("result", RemoteObject.class, null);
    private static final CommandReturnType CRT_GET_SAMPLING_PROFILE =
            new CommandReturnType("profile", SamplingHeapProfile.class, null);
    private static final CommandReturnType CRT_START_SAMPLING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_TRACKING_HEAP_OBJECTS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_SAMPLING =
            new CommandReturnType("profile", SamplingHeapProfile.class, null);
    private static final CommandReturnType CRT_STOP_TRACKING_HEAP_OBJECTS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TAKE_HEAP_SNAPSHOT = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ADD_INSPECTED_HEAP_OBJECT_1 = new String[] {"heapObjectId"};
    private static final String[] PARAMS_GET_HEAP_OBJECT_ID_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_2 = new String[] {"objectId", "objectGroup"};
    private static final String[] PARAMS_START_SAMPLING_2 =
            new String[] {"samplingInterval", "includeObjectsCollectedByMajorGC", "includeObjectsCollectedByMinorGC"};
    private static final String[] PARAMS_START_TRACKING_HEAP_OBJECTS_2 = new String[] {"trackAllocations"};
    private static final String[] PARAMS_STOP_TRACKING_HEAP_OBJECTS_2 =
            new String[] {"reportProgress", "treatGlobalObjectsAsRoots", "captureNumericValue", "exposeInternals"};
    private static final String[] PARAMS_TAKE_HEAP_SNAPSHOT_2 =
            new String[] {"reportProgress", "treatGlobalObjectsAsRoots", "captureNumericValue", "exposeInternals"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public HeapProfilerAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> addInspectedHeapObject(String heapObjectId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_addInspectedHeapObject,
                CRT_ADD_INSPECTED_HEAP_OBJECT,
                PARAMS_ADD_INSPECTED_HEAP_OBJECT_1,
                new Object[] {heapObjectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> collectGarbage() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.HeapProfiler_collectGarbage, CRT_COLLECT_GARBAGE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.HeapProfiler_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.HeapProfiler_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getHeapObjectId(String objectId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getHeapObjectId,
                CRT_GET_HEAP_OBJECT_ID,
                PARAMS_GET_HEAP_OBJECT_ID_1,
                new Object[] {objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RemoteObject> getObjectByHeapObjectId(String objectId) {
        return (CompletableFuture<RemoteObject>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getObjectByHeapObjectId,
                CRT_GET_OBJECT_BY_HEAP_OBJECT_ID,
                PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_1,
                new Object[] {objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RemoteObject> getObjectByHeapObjectId(String objectId, String objectGroup) {
        return (CompletableFuture<RemoteObject>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getObjectByHeapObjectId,
                CRT_GET_OBJECT_BY_HEAP_OBJECT_ID,
                PARAMS_GET_OBJECT_BY_HEAP_OBJECT_ID_2,
                new Object[] {objectId, objectGroup},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SamplingHeapProfile> getSamplingProfile() {
        return (CompletableFuture<SamplingHeapProfile>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_getSamplingProfile,
                CRT_GET_SAMPLING_PROFILE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startSampling() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.HeapProfiler_startSampling, CRT_START_SAMPLING, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startSampling(
            Double samplingInterval,
            Boolean includeObjectsCollectedByMajorGC,
            Boolean includeObjectsCollectedByMinorGC) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_startSampling,
                CRT_START_SAMPLING,
                PARAMS_START_SAMPLING_2,
                new Object[] {samplingInterval, includeObjectsCollectedByMajorGC, includeObjectsCollectedByMinorGC},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startTrackingHeapObjects() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_startTrackingHeapObjects,
                CRT_START_TRACKING_HEAP_OBJECTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startTrackingHeapObjects(Boolean trackAllocations) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_startTrackingHeapObjects,
                CRT_START_TRACKING_HEAP_OBJECTS,
                PARAMS_START_TRACKING_HEAP_OBJECTS_2,
                new Object[] {trackAllocations},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SamplingHeapProfile> stopSampling() {
        return (CompletableFuture<SamplingHeapProfile>) handler.invoke(
                this, DomainCommand.HeapProfiler_stopSampling, CRT_STOP_SAMPLING, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopTrackingHeapObjects() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_stopTrackingHeapObjects,
                CRT_STOP_TRACKING_HEAP_OBJECTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopTrackingHeapObjects(
            Boolean reportProgress,
            Boolean treatGlobalObjectsAsRoots,
            Boolean captureNumericValue,
            Boolean exposeInternals) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_stopTrackingHeapObjects,
                CRT_STOP_TRACKING_HEAP_OBJECTS,
                PARAMS_STOP_TRACKING_HEAP_OBJECTS_2,
                new Object[] {reportProgress, treatGlobalObjectsAsRoots, captureNumericValue, exposeInternals},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> takeHeapSnapshot() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_takeHeapSnapshot,
                CRT_TAKE_HEAP_SNAPSHOT,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> takeHeapSnapshot(
            Boolean reportProgress,
            Boolean treatGlobalObjectsAsRoots,
            Boolean captureNumericValue,
            Boolean exposeInternals) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.HeapProfiler_takeHeapSnapshot,
                CRT_TAKE_HEAP_SNAPSHOT,
                PARAMS_TAKE_HEAP_SNAPSHOT_2,
                new Object[] {reportProgress, treatGlobalObjectsAsRoots, captureNumericValue, exposeInternals},
                false);
    }
}
