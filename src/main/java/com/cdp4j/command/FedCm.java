// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.fedcm.AccountUrlType;
import com.cdp4j.type.fedcm.DialogButton;

/**
 * This domain allows interacting with the FedCM dialog.
 */
@Experimental
public interface FedCm extends ParameterizedCommand<FedCm> {
    void clickDialogButton(String dialogId, DialogButton dialogButton);

    void disable();

    void dismissDialog(String dialogId);

    void dismissDialog(String dialogId, @Optional Boolean triggerCooldown);

    void enable();

    void enable(@Optional Boolean disableRejectionDelay);

    void openUrl(String dialogId, Integer accountIndex, AccountUrlType accountUrlType);

    /**
     * Resets the cooldown time, if any, to allow the next FedCM call to show
     * a dialog even if one was recently dismissed by the user.
     */
    void resetCooldown();

    void selectAccount(String dialogId, Integer accountIndex);
}
