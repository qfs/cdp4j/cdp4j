// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.fedcm.AccountUrlType;
import com.cdp4j.type.fedcm.DialogButton;

class FedCmImpl extends ParameterizedCommandImpl<FedCm> implements FedCm {

    private static final CommandReturnType CRT_CLICK_DIALOG_BUTTON = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISMISS_DIALOG = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_OPEN_URL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RESET_COOLDOWN = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SELECT_ACCOUNT = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CLICK_DIALOG_BUTTON_1 = new String[] {"dialogId", "dialogButton"};
    private static final String[] PARAMS_DISMISS_DIALOG_1 = new String[] {"dialogId"};
    private static final String[] PARAMS_DISMISS_DIALOG_2 = new String[] {"dialogId", "triggerCooldown"};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"disableRejectionDelay"};
    private static final String[] PARAMS_OPEN_URL_1 = new String[] {"dialogId", "accountIndex", "accountUrlType"};
    private static final String[] PARAMS_SELECT_ACCOUNT_1 = new String[] {"dialogId", "accountIndex"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public FedCmImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void clickDialogButton(String dialogId, DialogButton dialogButton) {
        handler.invoke(
                this,
                DomainCommand.FedCm_clickDialogButton,
                CRT_CLICK_DIALOG_BUTTON,
                PARAMS_CLICK_DIALOG_BUTTON_1,
                new Object[] {dialogId, dialogButton},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.FedCm_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void dismissDialog(String dialogId) {
        handler.invoke(
                this,
                DomainCommand.FedCm_dismissDialog,
                CRT_DISMISS_DIALOG,
                PARAMS_DISMISS_DIALOG_1,
                new Object[] {dialogId},
                true);
    }

    @Override
    public void dismissDialog(String dialogId, Boolean triggerCooldown) {
        handler.invoke(
                this,
                DomainCommand.FedCm_dismissDialog,
                CRT_DISMISS_DIALOG,
                PARAMS_DISMISS_DIALOG_2,
                new Object[] {dialogId, triggerCooldown},
                true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.FedCm_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable(Boolean disableRejectionDelay) {
        handler.invoke(
                this,
                DomainCommand.FedCm_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_2,
                new Object[] {disableRejectionDelay},
                true);
    }

    @Override
    public void openUrl(String dialogId, Integer accountIndex, AccountUrlType accountUrlType) {
        handler.invoke(
                this,
                DomainCommand.FedCm_openUrl,
                CRT_OPEN_URL,
                PARAMS_OPEN_URL_1,
                new Object[] {dialogId, accountIndex, accountUrlType},
                true);
    }

    @Override
    public void resetCooldown() {
        handler.invoke(this, DomainCommand.FedCm_resetCooldown, CRT_RESET_COOLDOWN, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void selectAccount(String dialogId, Integer accountIndex) {
        handler.invoke(
                this,
                DomainCommand.FedCm_selectAccount,
                CRT_SELECT_ACCOUNT,
                PARAMS_SELECT_ACCOUNT_1,
                new Object[] {dialogId, accountIndex},
                true);
    }
}
