// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

/**
 * This domain allows detailed inspection of media elements
 */
@Experimental
public interface MediaAsync extends ParameterizedCommand<MediaAsync> {
    /**
     * Disables the Media domain.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables the Media domain
     */
    CompletableFuture<Void> enable();
}
