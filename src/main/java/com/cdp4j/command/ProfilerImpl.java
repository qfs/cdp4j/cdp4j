// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.profiler.Profile;
import com.cdp4j.type.profiler.ScriptCoverage;
import com.cdp4j.type.profiler.TakePreciseCoverageResult;
import java.util.List;

class ProfilerImpl extends ParameterizedCommandImpl<Profiler> implements Profiler {

    private static final TypeReference<List<ScriptCoverage>> LIST_SCRIPTCOVERAGE =
            new TypeReference<List<ScriptCoverage>>() {};
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_BEST_EFFORT_COVERAGE =
            new CommandReturnType("result", List.class, LIST_SCRIPTCOVERAGE);
    private static final CommandReturnType CRT_SET_SAMPLING_INTERVAL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_PRECISE_COVERAGE =
            new CommandReturnType("timestamp", Double.class, null);
    private static final CommandReturnType CRT_STOP = new CommandReturnType("profile", Profile.class, null);
    private static final CommandReturnType CRT_STOP_PRECISE_COVERAGE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TAKE_PRECISE_COVERAGE =
            new CommandReturnType(null, TakePreciseCoverageResult.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_SET_SAMPLING_INTERVAL_1 = new String[] {"interval"};
    private static final String[] PARAMS_START_PRECISE_COVERAGE_2 =
            new String[] {"callCount", "detailed", "allowTriggeredUpdates"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public ProfilerImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Profiler_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Profiler_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<ScriptCoverage> getBestEffortCoverage() {
        return (List<ScriptCoverage>) handler.invoke(
                this,
                DomainCommand.Profiler_getBestEffortCoverage,
                CRT_GET_BEST_EFFORT_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void setSamplingInterval(Integer interval) {
        handler.invoke(
                this,
                DomainCommand.Profiler_setSamplingInterval,
                CRT_SET_SAMPLING_INTERVAL,
                PARAMS_SET_SAMPLING_INTERVAL_1,
                new Object[] {interval},
                true);
    }

    @Override
    public void start() {
        handler.invoke(this, DomainCommand.Profiler_start, CRT_START, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public Double startPreciseCoverage() {
        return (Double) handler.invoke(
                this,
                DomainCommand.Profiler_startPreciseCoverage,
                CRT_START_PRECISE_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public Double startPreciseCoverage(Boolean callCount, Boolean detailed, Boolean allowTriggeredUpdates) {
        return (Double) handler.invoke(
                this,
                DomainCommand.Profiler_startPreciseCoverage,
                CRT_START_PRECISE_COVERAGE,
                PARAMS_START_PRECISE_COVERAGE_2,
                new Object[] {callCount, detailed, allowTriggeredUpdates},
                true);
    }

    @Override
    public Profile stop() {
        return (Profile) handler.invoke(this, DomainCommand.Profiler_stop, CRT_STOP, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void stopPreciseCoverage() {
        handler.invoke(
                this,
                DomainCommand.Profiler_stopPreciseCoverage,
                CRT_STOP_PRECISE_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public TakePreciseCoverageResult takePreciseCoverage() {
        return (TakePreciseCoverageResult) handler.invoke(
                this,
                DomainCommand.Profiler_takePreciseCoverage,
                CRT_TAKE_PRECISE_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }
}
