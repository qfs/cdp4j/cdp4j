// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.bluetoothemulation.CentralState;
import com.cdp4j.type.bluetoothemulation.ManufacturerData;
import com.cdp4j.type.bluetoothemulation.ScanEntry;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This domain allows configuring virtual Bluetooth devices to test the
 * web-bluetooth API.
 */
@Experimental
public interface BluetoothEmulationAsync extends ParameterizedCommand<BluetoothEmulationAsync> {
    /**
     * Disable the BluetoothEmulation domain.
     */
    CompletableFuture<Void> disable();

    /**
     * Enable the BluetoothEmulation domain.
     *
     * @param state
     *            State of the simulated central.
     */
    CompletableFuture<Void> enable(CentralState state);

    /**
     * Simulates an advertisement packet described in |entry| being received by the
     * central.
     *
     */
    CompletableFuture<Void> simulateAdvertisement(ScanEntry entry);

    /**
     * Simulates a peripheral with |address|, |name| and |knownServiceUuids| that
     * has already been connected to the system.
     *
     */
    CompletableFuture<Void> simulatePreconnectedPeripheral(
            String address, String name, List<ManufacturerData> manufacturerData, List<String> knownServiceUuids);
}
