// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.heapprofiler.SamplingHeapProfile;
import com.cdp4j.type.runtime.RemoteObject;

@Experimental
public interface HeapProfiler extends ParameterizedCommand<HeapProfiler> {
    /**
     * Enables console to refer to the node with given id via  (see Command Line API for more details
     *  functions).
     *
     * @param heapObjectId Heap snapshot object id to be accessible by means of x command line API.
     */
    void addInspectedHeapObject(String heapObjectId);

    void collectGarbage();

    void disable();

    void enable();

    String getHeapObjectId(String objectId);

    RemoteObject getObjectByHeapObjectId(String objectId);

    RemoteObject getObjectByHeapObjectId(String objectId, @Optional String objectGroup);

    SamplingHeapProfile getSamplingProfile();

    void startSampling();

    void startSampling(
            @Optional Double samplingInterval,
            @Optional Boolean includeObjectsCollectedByMajorGC,
            @Optional Boolean includeObjectsCollectedByMinorGC);

    void startTrackingHeapObjects();

    void startTrackingHeapObjects(@Optional Boolean trackAllocations);

    SamplingHeapProfile stopSampling();

    void stopTrackingHeapObjects();

    void stopTrackingHeapObjects(
            @Optional Boolean reportProgress,
            @Optional @Deprecated Boolean treatGlobalObjectsAsRoots,
            @Optional Boolean captureNumericValue,
            @Experimental @Optional Boolean exposeInternals);

    void takeHeapSnapshot();

    void takeHeapSnapshot(
            @Optional Boolean reportProgress,
            @Optional @Deprecated Boolean treatGlobalObjectsAsRoots,
            @Optional Boolean captureNumericValue,
            @Experimental @Optional Boolean exposeInternals);
}
