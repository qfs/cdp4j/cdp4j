// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.dom.Rect;
import com.cdp4j.type.layertree.CompositingReasonsResult;
import com.cdp4j.type.layertree.PictureTile;
import java.util.List;

@Experimental
public interface LayerTree extends ParameterizedCommand<LayerTree> {
    /**
     * Provides the reasons why the given layer was composited.
     *
     * @param layerId The id of the layer for which we want to get the reasons it was composited.
     *
     * @return CompositingReasonsResult
     */
    CompositingReasonsResult compositingReasons(String layerId);

    /**
     * Disables compositing tree inspection.
     */
    void disable();

    /**
     * Enables compositing tree inspection.
     */
    void enable();

    /**
     * Returns the snapshot identifier.
     *
     * @param tiles An array of tiles composing the snapshot.
     *
     * @return The id of the snapshot.
     */
    String loadSnapshot(List<PictureTile> tiles);

    /**
     * Returns the layer snapshot identifier.
     *
     * @param layerId The id of the layer.
     *
     * @return The id of the layer snapshot.
     */
    String makeSnapshot(String layerId);

    List<List<Double>> profileSnapshot(String snapshotId);

    List<List<Double>> profileSnapshot(
            String snapshotId, @Optional Integer minRepeatCount, @Optional Double minDuration, @Optional Rect clipRect);

    /**
     * Releases layer snapshot captured by the back-end.
     *
     * @param snapshotId The id of the layer snapshot.
     */
    void releaseSnapshot(String snapshotId);

    /**
     * Replays the layer snapshot and returns the resulting bitmap.
     *
     * @param snapshotId The id of the layer snapshot.
     *
     * @return A data: URL for resulting image.
     */
    String replaySnapshot(String snapshotId);

    /**
     * Replays the layer snapshot and returns the resulting bitmap.
     *
     * @param snapshotId The id of the layer snapshot.
     * @param fromStep The first step to replay from (replay from the very start if not specified).
     * @param toStep The last step to replay to (replay till the end if not specified).
     * @param scale The scale to apply while replaying (defaults to 1).
     *
     * @return A data: URL for resulting image.
     */
    String replaySnapshot(
            String snapshotId, @Optional Integer fromStep, @Optional Integer toStep, @Optional Double scale);
}
