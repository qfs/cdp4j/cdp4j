// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.debugger.SearchMatch;
import com.cdp4j.type.emulation.UserAgentMetadata;
import com.cdp4j.type.network.AuthChallengeResponse;
import com.cdp4j.type.network.ConnectionType;
import com.cdp4j.type.network.ContentEncoding;
import com.cdp4j.type.network.Cookie;
import com.cdp4j.type.network.CookieParam;
import com.cdp4j.type.network.CookiePartitionKey;
import com.cdp4j.type.network.CookiePriority;
import com.cdp4j.type.network.CookieSameSite;
import com.cdp4j.type.network.CookieSourceScheme;
import com.cdp4j.type.network.ErrorReason;
import com.cdp4j.type.network.GetResponseBodyForInterceptionResult;
import com.cdp4j.type.network.GetResponseBodyResult;
import com.cdp4j.type.network.LoadNetworkResourceOptions;
import com.cdp4j.type.network.LoadNetworkResourcePageResult;
import com.cdp4j.type.network.RequestPattern;
import com.cdp4j.type.network.SecurityIsolationStatus;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

class NetworkAsyncImpl extends ParameterizedCommandImpl<NetworkAsync> implements NetworkAsync {

    private static final TypeReference<List<Cookie>> LIST_COOKIE = new TypeReference<List<Cookie>>() {};
    private static final TypeReference<List<SearchMatch>> LIST_SEARCHMATCH = new TypeReference<List<SearchMatch>>() {};
    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_CAN_CLEAR_BROWSER_CACHE =
            new CommandReturnType("result", Boolean.class, null);
    private static final CommandReturnType CRT_CAN_CLEAR_BROWSER_COOKIES =
            new CommandReturnType("result", Boolean.class, null);
    private static final CommandReturnType CRT_CAN_EMULATE_NETWORK_CONDITIONS =
            new CommandReturnType("result", Boolean.class, null);
    private static final CommandReturnType CRT_CLEAR_ACCEPTED_ENCODINGS_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_BROWSER_CACHE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_BROWSER_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CONTINUE_INTERCEPTED_REQUEST =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EMULATE_NETWORK_CONDITIONS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE_REPORTING_API = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ALL_COOKIES =
            new CommandReturnType("cookies", List.class, LIST_COOKIE);
    private static final CommandReturnType CRT_GET_CERTIFICATE =
            new CommandReturnType("tableNames", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_COOKIES = new CommandReturnType("cookies", List.class, LIST_COOKIE);
    private static final CommandReturnType CRT_GET_REQUEST_POST_DATA =
            new CommandReturnType("postData", String.class, null);
    private static final CommandReturnType CRT_GET_RESPONSE_BODY =
            new CommandReturnType(null, GetResponseBodyResult.class, null);
    private static final CommandReturnType CRT_GET_RESPONSE_BODY_FOR_INTERCEPTION =
            new CommandReturnType(null, GetResponseBodyForInterceptionResult.class, null);
    private static final CommandReturnType CRT_GET_SECURITY_ISOLATION_STATUS =
            new CommandReturnType("status", SecurityIsolationStatus.class, null);
    private static final CommandReturnType CRT_LOAD_NETWORK_RESOURCE =
            new CommandReturnType("resource", LoadNetworkResourcePageResult.class, null);
    private static final CommandReturnType CRT_REPLAY_XH_R = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SEARCH_IN_RESPONSE_BODY =
            new CommandReturnType("result", List.class, LIST_SEARCHMATCH);
    private static final CommandReturnType CRT_SET_ACCEPTED_ENCODINGS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ATTACH_DEBUG_STACK = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BLOCKED_UR_LS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BYPASS_SERVICE_WORKER =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_CACHE_DISABLED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_COOKIE = new CommandReturnType("success", Boolean.class, null);
    private static final CommandReturnType CRT_SET_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_COOKIE_CONTROLS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_EXTRA_HT_TP_HEADERS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_REQUEST_INTERCEPTION = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_USER_AGENT_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STREAM_RESOURCE_CONTENT =
            new CommandReturnType("bufferedData", String.class, null);
    private static final CommandReturnType CRT_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM =
            new CommandReturnType("stream", String.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CONTINUE_INTERCEPTED_REQUEST_1 = new String[] {"interceptionId"};
    private static final String[] PARAMS_CONTINUE_INTERCEPTED_REQUEST_2 = new String[] {
        "interceptionId", "errorReason", "rawResponse", "url", "method", "postData", "headers", "authChallengeResponse"
    };
    private static final String[] PARAMS_DELETE_COOKIES_1 = new String[] {"name"};
    private static final String[] PARAMS_DELETE_COOKIES_2 =
            new String[] {"name", "url", "domain", "path", "partitionKey"};
    private static final String[] PARAMS_EMULATE_NETWORK_CONDITIONS_1 =
            new String[] {"offline", "latency", "downloadThroughput", "uploadThroughput"};
    private static final String[] PARAMS_EMULATE_NETWORK_CONDITIONS_2 = new String[] {
        "offline",
        "latency",
        "downloadThroughput",
        "uploadThroughput",
        "connectionType",
        "packetLoss",
        "packetQueueLength",
        "packetReordering"
    };
    private static final String[] PARAMS_ENABLE_2 =
            new String[] {"maxTotalBufferSize", "maxResourceBufferSize", "maxPostDataSize"};
    private static final String[] PARAMS_ENABLE_REPORTING_API_1 = new String[] {"enable"};
    private static final String[] PARAMS_GET_CERTIFICATE_1 = new String[] {"origin"};
    private static final String[] PARAMS_GET_COOKIES_2 = new String[] {"urls"};
    private static final String[] PARAMS_GET_REQUEST_POST_DATA_1 = new String[] {"requestId"};
    private static final String[] PARAMS_GET_RESPONSE_BODY_1 = new String[] {"requestId"};
    private static final String[] PARAMS_GET_RESPONSE_BODY_FOR_INTERCEPTION_1 = new String[] {"interceptionId"};
    private static final String[] PARAMS_GET_SECURITY_ISOLATION_STATUS_2 = new String[] {"frameId"};
    private static final String[] PARAMS_LOAD_NETWORK_RESOURCE_1 = new String[] {"url", "options"};
    private static final String[] PARAMS_LOAD_NETWORK_RESOURCE_2 = new String[] {"frameId", "url", "options"};
    private static final String[] PARAMS_REPLAY_XH_R_1 = new String[] {"requestId"};
    private static final String[] PARAMS_SEARCH_IN_RESPONSE_BODY_1 = new String[] {"requestId", "query"};
    private static final String[] PARAMS_SEARCH_IN_RESPONSE_BODY_2 =
            new String[] {"requestId", "query", "caseSensitive", "isRegex"};
    private static final String[] PARAMS_SET_ACCEPTED_ENCODINGS_1 = new String[] {"encodings"};
    private static final String[] PARAMS_SET_ATTACH_DEBUG_STACK_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_BLOCKED_UR_LS_1 = new String[] {"urls"};
    private static final String[] PARAMS_SET_BYPASS_SERVICE_WORKER_1 = new String[] {"bypass"};
    private static final String[] PARAMS_SET_CACHE_DISABLED_1 = new String[] {"cacheDisabled"};
    private static final String[] PARAMS_SET_COOKIES_1 = new String[] {"cookies"};
    private static final String[] PARAMS_SET_COOKIE_1 = new String[] {"name", "value"};
    private static final String[] PARAMS_SET_COOKIE_2 = new String[] {
        "name",
        "value",
        "url",
        "domain",
        "path",
        "secure",
        "httpOnly",
        "sameSite",
        "expires",
        "priority",
        "sameParty",
        "sourceScheme",
        "sourcePort",
        "partitionKey"
    };
    private static final String[] PARAMS_SET_COOKIE_CONTROLS_1 = new String[] {
        "enableThirdPartyCookieRestriction", "disableThirdPartyCookieMetadata", "disableThirdPartyCookieHeuristics"
    };
    private static final String[] PARAMS_SET_EXTRA_HT_TP_HEADERS_1 = new String[] {"headers"};
    private static final String[] PARAMS_SET_REQUEST_INTERCEPTION_1 = new String[] {"patterns"};
    private static final String[] PARAMS_SET_USER_AGENT_OVERRIDE_1 = new String[] {"userAgent"};
    private static final String[] PARAMS_SET_USER_AGENT_OVERRIDE_2 =
            new String[] {"userAgent", "acceptLanguage", "platform", "userAgentMetadata"};
    private static final String[] PARAMS_STREAM_RESOURCE_CONTENT_1 = new String[] {"requestId"};
    private static final String[] PARAMS_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM_1 =
            new String[] {"interceptionId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public NetworkAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> canClearBrowserCache() {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.Network_canClearBrowserCache,
                CRT_CAN_CLEAR_BROWSER_CACHE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> canClearBrowserCookies() {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.Network_canClearBrowserCookies,
                CRT_CAN_CLEAR_BROWSER_COOKIES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> canEmulateNetworkConditions() {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.Network_canEmulateNetworkConditions,
                CRT_CAN_EMULATE_NETWORK_CONDITIONS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearAcceptedEncodingsOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_clearAcceptedEncodingsOverride,
                CRT_CLEAR_ACCEPTED_ENCODINGS_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearBrowserCache() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_clearBrowserCache,
                CRT_CLEAR_BROWSER_CACHE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearBrowserCookies() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_clearBrowserCookies,
                CRT_CLEAR_BROWSER_COOKIES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> continueInterceptedRequest(String interceptionId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_continueInterceptedRequest,
                CRT_CONTINUE_INTERCEPTED_REQUEST,
                PARAMS_CONTINUE_INTERCEPTED_REQUEST_1,
                new Object[] {interceptionId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> continueInterceptedRequest(
            String interceptionId,
            ErrorReason errorReason,
            String rawResponse,
            String url,
            String method,
            String postData,
            Map<String, Object> headers,
            AuthChallengeResponse authChallengeResponse) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_continueInterceptedRequest,
                CRT_CONTINUE_INTERCEPTED_REQUEST,
                PARAMS_CONTINUE_INTERCEPTED_REQUEST_2,
                new Object[] {
                    interceptionId, errorReason, rawResponse, url, method, postData, headers, authChallengeResponse
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteCookies(String name) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_deleteCookies,
                CRT_DELETE_COOKIES,
                PARAMS_DELETE_COOKIES_1,
                new Object[] {name},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteCookies(
            String name, String url, String domain, String path, CookiePartitionKey partitionKey) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_deleteCookies,
                CRT_DELETE_COOKIES,
                PARAMS_DELETE_COOKIES_2,
                new Object[] {name, url, domain, path, partitionKey},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Network_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> emulateNetworkConditions(
            Boolean offline, Double latency, Double downloadThroughput, Double uploadThroughput) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_emulateNetworkConditions,
                CRT_EMULATE_NETWORK_CONDITIONS,
                PARAMS_EMULATE_NETWORK_CONDITIONS_1,
                new Object[] {offline, latency, downloadThroughput, uploadThroughput},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> emulateNetworkConditions(
            Boolean offline,
            Double latency,
            Double downloadThroughput,
            Double uploadThroughput,
            ConnectionType connectionType,
            Double packetLoss,
            Integer packetQueueLength,
            Boolean packetReordering) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_emulateNetworkConditions,
                CRT_EMULATE_NETWORK_CONDITIONS,
                PARAMS_EMULATE_NETWORK_CONDITIONS_2,
                new Object[] {
                    offline,
                    latency,
                    downloadThroughput,
                    uploadThroughput,
                    connectionType,
                    packetLoss,
                    packetQueueLength,
                    packetReordering
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Network_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable(
            Integer maxTotalBufferSize, Integer maxResourceBufferSize, Integer maxPostDataSize) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_2,
                new Object[] {maxTotalBufferSize, maxResourceBufferSize, maxPostDataSize},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enableReportingApi(Boolean enable) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_enableReportingApi,
                CRT_ENABLE_REPORTING_API,
                PARAMS_ENABLE_REPORTING_API_1,
                new Object[] {enable},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Cookie>> getAllCookies() {
        return (CompletableFuture<List<Cookie>>) handler.invoke(
                this, DomainCommand.Network_getAllCookies, CRT_GET_ALL_COOKIES, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> getCertificate(String origin) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.Network_getCertificate,
                CRT_GET_CERTIFICATE,
                PARAMS_GET_CERTIFICATE_1,
                new Object[] {origin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Cookie>> getCookies() {
        return (CompletableFuture<List<Cookie>>) handler.invoke(
                this, DomainCommand.Network_getCookies, CRT_GET_COOKIES, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Cookie>> getCookies(List<String> urls) {
        return (CompletableFuture<List<Cookie>>) handler.invoke(
                this,
                DomainCommand.Network_getCookies,
                CRT_GET_COOKIES,
                PARAMS_GET_COOKIES_2,
                new Object[] {urls},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getRequestPostData(String requestId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Network_getRequestPostData,
                CRT_GET_REQUEST_POST_DATA,
                PARAMS_GET_REQUEST_POST_DATA_1,
                new Object[] {requestId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetResponseBodyResult> getResponseBody(String requestId) {
        return (CompletableFuture<GetResponseBodyResult>) handler.invoke(
                this,
                DomainCommand.Network_getResponseBody,
                CRT_GET_RESPONSE_BODY,
                PARAMS_GET_RESPONSE_BODY_1,
                new Object[] {requestId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetResponseBodyForInterceptionResult> getResponseBodyForInterception(
            String interceptionId) {
        return (CompletableFuture<GetResponseBodyForInterceptionResult>) handler.invoke(
                this,
                DomainCommand.Network_getResponseBodyForInterception,
                CRT_GET_RESPONSE_BODY_FOR_INTERCEPTION,
                PARAMS_GET_RESPONSE_BODY_FOR_INTERCEPTION_1,
                new Object[] {interceptionId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SecurityIsolationStatus> getSecurityIsolationStatus() {
        return (CompletableFuture<SecurityIsolationStatus>) handler.invoke(
                this,
                DomainCommand.Network_getSecurityIsolationStatus,
                CRT_GET_SECURITY_ISOLATION_STATUS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SecurityIsolationStatus> getSecurityIsolationStatus(String frameId) {
        return (CompletableFuture<SecurityIsolationStatus>) handler.invoke(
                this,
                DomainCommand.Network_getSecurityIsolationStatus,
                CRT_GET_SECURITY_ISOLATION_STATUS,
                PARAMS_GET_SECURITY_ISOLATION_STATUS_2,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<LoadNetworkResourcePageResult> loadNetworkResource(
            String url, LoadNetworkResourceOptions options) {
        return (CompletableFuture<LoadNetworkResourcePageResult>) handler.invoke(
                this,
                DomainCommand.Network_loadNetworkResource,
                CRT_LOAD_NETWORK_RESOURCE,
                PARAMS_LOAD_NETWORK_RESOURCE_1,
                new Object[] {url, options},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<LoadNetworkResourcePageResult> loadNetworkResource(
            String frameId, String url, LoadNetworkResourceOptions options) {
        return (CompletableFuture<LoadNetworkResourcePageResult>) handler.invoke(
                this,
                DomainCommand.Network_loadNetworkResource,
                CRT_LOAD_NETWORK_RESOURCE,
                PARAMS_LOAD_NETWORK_RESOURCE_2,
                new Object[] {frameId, url, options},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> replayXHR(String requestId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_replayXHR,
                CRT_REPLAY_XH_R,
                PARAMS_REPLAY_XH_R_1,
                new Object[] {requestId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<SearchMatch>> searchInResponseBody(String requestId, String query) {
        return (CompletableFuture<List<SearchMatch>>) handler.invoke(
                this,
                DomainCommand.Network_searchInResponseBody,
                CRT_SEARCH_IN_RESPONSE_BODY,
                PARAMS_SEARCH_IN_RESPONSE_BODY_1,
                new Object[] {requestId, query},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<SearchMatch>> searchInResponseBody(
            String requestId, String query, Boolean caseSensitive, Boolean isRegex) {
        return (CompletableFuture<List<SearchMatch>>) handler.invoke(
                this,
                DomainCommand.Network_searchInResponseBody,
                CRT_SEARCH_IN_RESPONSE_BODY,
                PARAMS_SEARCH_IN_RESPONSE_BODY_2,
                new Object[] {requestId, query, caseSensitive, isRegex},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAcceptedEncodings(ContentEncoding encodings) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setAcceptedEncodings,
                CRT_SET_ACCEPTED_ENCODINGS,
                PARAMS_SET_ACCEPTED_ENCODINGS_1,
                new Object[] {encodings},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAttachDebugStack(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setAttachDebugStack,
                CRT_SET_ATTACH_DEBUG_STACK,
                PARAMS_SET_ATTACH_DEBUG_STACK_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setBlockedURLs(List<String> urls) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setBlockedURLs,
                CRT_SET_BLOCKED_UR_LS,
                PARAMS_SET_BLOCKED_UR_LS_1,
                new Object[] {urls},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setBypassServiceWorker(Boolean bypass) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setBypassServiceWorker,
                CRT_SET_BYPASS_SERVICE_WORKER,
                PARAMS_SET_BYPASS_SERVICE_WORKER_1,
                new Object[] {bypass},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setCacheDisabled(Boolean cacheDisabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setCacheDisabled,
                CRT_SET_CACHE_DISABLED,
                PARAMS_SET_CACHE_DISABLED_1,
                new Object[] {cacheDisabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> setCookie(String name, String value) {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.Network_setCookie,
                CRT_SET_COOKIE,
                PARAMS_SET_COOKIE_1,
                new Object[] {name, value},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> setCookie(
            String name,
            String value,
            String url,
            String domain,
            String path,
            Boolean secure,
            Boolean httpOnly,
            CookieSameSite sameSite,
            Double expires,
            CookiePriority priority,
            Boolean sameParty,
            CookieSourceScheme sourceScheme,
            Integer sourcePort,
            CookiePartitionKey partitionKey) {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.Network_setCookie,
                CRT_SET_COOKIE,
                PARAMS_SET_COOKIE_2,
                new Object[] {
                    name,
                    value,
                    url,
                    domain,
                    path,
                    secure,
                    httpOnly,
                    sameSite,
                    expires,
                    priority,
                    sameParty,
                    sourceScheme,
                    sourcePort,
                    partitionKey
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setCookieControls(
            Boolean enableThirdPartyCookieRestriction,
            Boolean disableThirdPartyCookieMetadata,
            Boolean disableThirdPartyCookieHeuristics) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setCookieControls,
                CRT_SET_COOKIE_CONTROLS,
                PARAMS_SET_COOKIE_CONTROLS_1,
                new Object[] {
                    enableThirdPartyCookieRestriction,
                    disableThirdPartyCookieMetadata,
                    disableThirdPartyCookieHeuristics
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setCookies(List<CookieParam> cookies) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setCookies,
                CRT_SET_COOKIES,
                PARAMS_SET_COOKIES_1,
                new Object[] {cookies},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setExtraHTTPHeaders(Map<String, Object> headers) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setExtraHTTPHeaders,
                CRT_SET_EXTRA_HT_TP_HEADERS,
                PARAMS_SET_EXTRA_HT_TP_HEADERS_1,
                new Object[] {headers},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setRequestInterception(List<RequestPattern> patterns) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setRequestInterception,
                CRT_SET_REQUEST_INTERCEPTION,
                PARAMS_SET_REQUEST_INTERCEPTION_1,
                new Object[] {patterns},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setUserAgentOverride(String userAgent) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setUserAgentOverride,
                CRT_SET_USER_AGENT_OVERRIDE,
                PARAMS_SET_USER_AGENT_OVERRIDE_1,
                new Object[] {userAgent},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setUserAgentOverride(
            String userAgent, String acceptLanguage, String platform, UserAgentMetadata userAgentMetadata) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Network_setUserAgentOverride,
                CRT_SET_USER_AGENT_OVERRIDE,
                PARAMS_SET_USER_AGENT_OVERRIDE_2,
                new Object[] {userAgent, acceptLanguage, platform, userAgentMetadata},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> streamResourceContent(String requestId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Network_streamResourceContent,
                CRT_STREAM_RESOURCE_CONTENT,
                PARAMS_STREAM_RESOURCE_CONTENT_1,
                new Object[] {requestId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> takeResponseBodyForInterceptionAsStream(String interceptionId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Network_takeResponseBodyForInterceptionAsStream,
                CRT_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM,
                PARAMS_TAKE_RESPONSE_BODY_FOR_INTERCEPTION_AS_STREAM_1,
                new Object[] {interceptionId},
                false);
    }
}
