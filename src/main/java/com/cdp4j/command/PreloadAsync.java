// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface PreloadAsync extends ParameterizedCommand<PreloadAsync> {
    CompletableFuture<Void> disable();

    CompletableFuture<Void> enable();
}
