// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.DownloadBehavior;
import com.cdp4j.type.constant.ImageFormat;
import com.cdp4j.type.constant.Platform;
import com.cdp4j.type.constant.SnapshotType;
import com.cdp4j.type.constant.TransferMode;
import com.cdp4j.type.constant.WebLifecycleState;
import com.cdp4j.type.debugger.SearchMatch;
import com.cdp4j.type.emulation.ScreenOrientation;
import com.cdp4j.type.page.AdScriptId;
import com.cdp4j.type.page.AutoResponseMode;
import com.cdp4j.type.page.CompilationCacheParams;
import com.cdp4j.type.page.FontFamilies;
import com.cdp4j.type.page.FontSizes;
import com.cdp4j.type.page.FrameResourceTree;
import com.cdp4j.type.page.FrameTree;
import com.cdp4j.type.page.GetAppIdResult;
import com.cdp4j.type.page.GetAppManifestResult;
import com.cdp4j.type.page.GetLayoutMetricsResult;
import com.cdp4j.type.page.GetNavigationHistoryResult;
import com.cdp4j.type.page.GetResourceContentResult;
import com.cdp4j.type.page.InstallabilityError;
import com.cdp4j.type.page.NavigateResult;
import com.cdp4j.type.page.OriginTrial;
import com.cdp4j.type.page.PermissionsPolicyFeatureState;
import com.cdp4j.type.page.PrintToPDFResult;
import com.cdp4j.type.page.ReferrerPolicy;
import com.cdp4j.type.page.ScriptFontFamilies;
import com.cdp4j.type.page.TransitionType;
import com.cdp4j.type.page.Viewport;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class PageAsyncImpl extends ParameterizedCommandImpl<PageAsync> implements PageAsync {

    private static final TypeReference<List<InstallabilityError>> LIST_INSTALLABILITYERROR =
            new TypeReference<List<InstallabilityError>>() {};
    private static final TypeReference<List<OriginTrial>> LIST_ORIGINTRIAL = new TypeReference<List<OriginTrial>>() {};
    private static final TypeReference<List<PermissionsPolicyFeatureState>> LIST_PERMISSIONSPOLICYFEATURESTATE =
            new TypeReference<List<PermissionsPolicyFeatureState>>() {};
    private static final TypeReference<List<SearchMatch>> LIST_SEARCHMATCH = new TypeReference<List<SearchMatch>>() {};
    private static final CommandReturnType CRT_ADD_COMPILATION_CACHE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ADD_SCRIPT_TO_EVALUATE_ON_LOAD =
            new CommandReturnType("identifier", String.class, null);
    private static final CommandReturnType CRT_ADD_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT =
            new CommandReturnType("identifier", String.class, null);
    private static final CommandReturnType CRT_BRING_TO_FRONT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CAPTURE_SCREENSHOT = new CommandReturnType("data", byte[].class, null);
    private static final CommandReturnType CRT_CAPTURE_SNAPSHOT = new CommandReturnType("data", String.class, null);
    private static final CommandReturnType CRT_CLEAR_COMPILATION_CACHE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_DEVICE_METRICS_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_DEVICE_ORIENTATION_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_GEOLOCATION_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLOSE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CRASH = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CREATE_ISOLATED_WORLD =
            new CommandReturnType("executionContextId", Integer.class, null);
    private static final CommandReturnType CRT_DELETE_COOKIE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GENERATE_TEST_REPORT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_AD_SCRIPT_ID =
            new CommandReturnType("adScriptId", AdScriptId.class, null);
    private static final CommandReturnType CRT_GET_APP_ID = new CommandReturnType(null, GetAppIdResult.class, null);
    private static final CommandReturnType CRT_GET_APP_MANIFEST =
            new CommandReturnType(null, GetAppManifestResult.class, null);
    private static final CommandReturnType CRT_GET_FRAME_TREE =
            new CommandReturnType("frameTree", FrameTree.class, null);
    private static final CommandReturnType CRT_GET_INSTALLABILITY_ERRORS =
            new CommandReturnType("installabilityErrors", List.class, LIST_INSTALLABILITYERROR);
    private static final CommandReturnType CRT_GET_LAYOUT_METRICS =
            new CommandReturnType(null, GetLayoutMetricsResult.class, null);
    private static final CommandReturnType CRT_GET_MANIFEST_ICONS =
            new CommandReturnType("primaryIcon", String.class, null);
    private static final CommandReturnType CRT_GET_NAVIGATION_HISTORY =
            new CommandReturnType(null, GetNavigationHistoryResult.class, null);
    private static final CommandReturnType CRT_GET_ORIGIN_TRIALS =
            new CommandReturnType("originTrials", List.class, LIST_ORIGINTRIAL);
    private static final CommandReturnType CRT_GET_PERMISSIONS_POLICY_STATE =
            new CommandReturnType("states", List.class, LIST_PERMISSIONSPOLICYFEATURESTATE);
    private static final CommandReturnType CRT_GET_RESOURCE_CONTENT =
            new CommandReturnType(null, GetResourceContentResult.class, null);
    private static final CommandReturnType CRT_GET_RESOURCE_TREE =
            new CommandReturnType("frameTree", FrameResourceTree.class, null);
    private static final CommandReturnType CRT_HANDLE_JAVA_SCRIPT_DIALOG =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_NAVIGATE = new CommandReturnType(null, NavigateResult.class, null);
    private static final CommandReturnType CRT_NAVIGATE_TO_HISTORY_ENTRY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_PRINT_TO_PD_F =
            new CommandReturnType(null, PrintToPDFResult.class, null);
    private static final CommandReturnType CRT_PRODUCE_COMPILATION_CACHE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RELOAD = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_SCRIPT_TO_EVALUATE_ON_LOAD =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RESET_NAVIGATION_HISTORY = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SCREENCAST_FRAME_ACK = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SEARCH_IN_RESOURCE =
            new CommandReturnType("result", List.class, LIST_SEARCHMATCH);
    private static final CommandReturnType CRT_SET_AD_BLOCKING_ENABLED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BYPASS_CS_P = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DEVICE_METRICS_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DEVICE_ORIENTATION_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DOCUMENT_CONTENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DOWNLOAD_BEHAVIOR = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_FONT_FAMILIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_FONT_SIZES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_GEOLOCATION_OVERRIDE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INTERCEPT_FILE_CHOOSER_DIALOG =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_LIFECYCLE_EVENTS_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PRERENDERING_ALLOWED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_RP_HREGISTRATION_MODE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SP_CTRANSACTION_MODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_TOUCH_EMULATION_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_WEB_LIFECYCLE_STATE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_SCREENCAST = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_LOADING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_SCREENCAST = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_WAIT_FOR_DEBUGGER = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ADD_COMPILATION_CACHE_1 = new String[] {"url", "data"};
    private static final String[] PARAMS_ADD_SCRIPT_TO_EVALUATE_ON_LOAD_1 = new String[] {"scriptSource"};
    private static final String[] PARAMS_ADD_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT_1 = new String[] {"source"};
    private static final String[] PARAMS_ADD_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT_2 =
            new String[] {"source", "worldName", "includeCommandLineAPI", "runImmediately"};
    private static final String[] PARAMS_CAPTURE_SCREENSHOT_2 =
            new String[] {"format", "quality", "clip", "fromSurface", "captureBeyondViewport", "optimizeForSpeed"};
    private static final String[] PARAMS_CAPTURE_SNAPSHOT_2 = new String[] {"format"};
    private static final String[] PARAMS_CREATE_ISOLATED_WORLD_1 = new String[] {"frameId"};
    private static final String[] PARAMS_CREATE_ISOLATED_WORLD_2 =
            new String[] {"frameId", "worldName", "grantUniveralAccess"};
    private static final String[] PARAMS_DELETE_COOKIE_1 = new String[] {"cookieName", "url"};
    private static final String[] PARAMS_GENERATE_TEST_REPORT_1 = new String[] {"message"};
    private static final String[] PARAMS_GENERATE_TEST_REPORT_2 = new String[] {"message", "group"};
    private static final String[] PARAMS_GET_AD_SCRIPT_ID_1 = new String[] {"frameId"};
    private static final String[] PARAMS_GET_APP_MANIFEST_2 = new String[] {"manifestId"};
    private static final String[] PARAMS_GET_ORIGIN_TRIALS_1 = new String[] {"frameId"};
    private static final String[] PARAMS_GET_PERMISSIONS_POLICY_STATE_1 = new String[] {"frameId"};
    private static final String[] PARAMS_GET_RESOURCE_CONTENT_1 = new String[] {"frameId", "url"};
    private static final String[] PARAMS_HANDLE_JAVA_SCRIPT_DIALOG_1 = new String[] {"accept"};
    private static final String[] PARAMS_HANDLE_JAVA_SCRIPT_DIALOG_2 = new String[] {"accept", "promptText"};
    private static final String[] PARAMS_NAVIGATE_1 = new String[] {"url"};
    private static final String[] PARAMS_NAVIGATE_2 =
            new String[] {"url", "referrer", "transitionType", "frameId", "referrerPolicy"};
    private static final String[] PARAMS_NAVIGATE_TO_HISTORY_ENTRY_1 = new String[] {"entryId"};
    private static final String[] PARAMS_PRINT_TO_PD_F_2 = new String[] {
        "landscape",
        "displayHeaderFooter",
        "printBackground",
        "scale",
        "paperWidth",
        "paperHeight",
        "marginTop",
        "marginBottom",
        "marginLeft",
        "marginRight",
        "pageRanges",
        "headerTemplate",
        "footerTemplate",
        "preferCSSPageSize",
        "transferMode",
        "generateTaggedPDF",
        "generateDocumentOutline"
    };
    private static final String[] PARAMS_PRODUCE_COMPILATION_CACHE_1 = new String[] {"scripts"};
    private static final String[] PARAMS_RELOAD_2 = new String[] {"ignoreCache", "scriptToEvaluateOnLoad", "loaderId"};
    private static final String[] PARAMS_REMOVE_SCRIPT_TO_EVALUATE_ON_LOAD_1 = new String[] {"identifier"};
    private static final String[] PARAMS_REMOVE_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT_1 = new String[] {"identifier"};
    private static final String[] PARAMS_SCREENCAST_FRAME_ACK_1 = new String[] {"sessionId"};
    private static final String[] PARAMS_SEARCH_IN_RESOURCE_1 = new String[] {"frameId", "url", "query"};
    private static final String[] PARAMS_SEARCH_IN_RESOURCE_2 =
            new String[] {"frameId", "url", "query", "caseSensitive", "isRegex"};
    private static final String[] PARAMS_SET_AD_BLOCKING_ENABLED_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_BYPASS_CS_P_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_DEVICE_METRICS_OVERRIDE_1 =
            new String[] {"width", "height", "deviceScaleFactor", "mobile"};
    private static final String[] PARAMS_SET_DEVICE_METRICS_OVERRIDE_2 = new String[] {
        "width",
        "height",
        "deviceScaleFactor",
        "mobile",
        "scale",
        "screenWidth",
        "screenHeight",
        "positionX",
        "positionY",
        "dontSetVisibleSize",
        "screenOrientation",
        "viewport"
    };
    private static final String[] PARAMS_SET_DEVICE_ORIENTATION_OVERRIDE_1 = new String[] {"alpha", "beta", "gamma"};
    private static final String[] PARAMS_SET_DOCUMENT_CONTENT_1 = new String[] {"frameId", "html"};
    private static final String[] PARAMS_SET_DOWNLOAD_BEHAVIOR_1 = new String[] {"behavior"};
    private static final String[] PARAMS_SET_DOWNLOAD_BEHAVIOR_2 = new String[] {"behavior", "downloadPath"};
    private static final String[] PARAMS_SET_FONT_FAMILIES_1 = new String[] {"fontFamilies"};
    private static final String[] PARAMS_SET_FONT_FAMILIES_2 = new String[] {"fontFamilies", "forScripts"};
    private static final String[] PARAMS_SET_FONT_SIZES_1 = new String[] {"fontSizes"};
    private static final String[] PARAMS_SET_GEOLOCATION_OVERRIDE_2 =
            new String[] {"latitude", "longitude", "accuracy"};
    private static final String[] PARAMS_SET_INTERCEPT_FILE_CHOOSER_DIALOG_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_LIFECYCLE_EVENTS_ENABLED_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_PRERENDERING_ALLOWED_1 = new String[] {"isAllowed"};
    private static final String[] PARAMS_SET_RP_HREGISTRATION_MODE_1 = new String[] {"mode"};
    private static final String[] PARAMS_SET_SP_CTRANSACTION_MODE_1 = new String[] {"mode"};
    private static final String[] PARAMS_SET_TOUCH_EMULATION_ENABLED_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_TOUCH_EMULATION_ENABLED_2 = new String[] {"enabled", "configuration"};
    private static final String[] PARAMS_SET_WEB_LIFECYCLE_STATE_1 = new String[] {"state"};
    private static final String[] PARAMS_START_SCREENCAST_2 =
            new String[] {"format", "quality", "maxWidth", "maxHeight", "everyNthFrame"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public PageAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> addCompilationCache(String url, String data) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_addCompilationCache,
                CRT_ADD_COMPILATION_CACHE,
                PARAMS_ADD_COMPILATION_CACHE_1,
                new Object[] {url, data},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> addScriptToEvaluateOnLoad(String scriptSource) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Page_addScriptToEvaluateOnLoad,
                CRT_ADD_SCRIPT_TO_EVALUATE_ON_LOAD,
                PARAMS_ADD_SCRIPT_TO_EVALUATE_ON_LOAD_1,
                new Object[] {scriptSource},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> addScriptToEvaluateOnNewDocument(String source) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Page_addScriptToEvaluateOnNewDocument,
                CRT_ADD_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT,
                PARAMS_ADD_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT_1,
                new Object[] {source},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> addScriptToEvaluateOnNewDocument(
            String source, String worldName, Boolean includeCommandLineAPI, Boolean runImmediately) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Page_addScriptToEvaluateOnNewDocument,
                CRT_ADD_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT,
                PARAMS_ADD_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT_2,
                new Object[] {source, worldName, includeCommandLineAPI, runImmediately},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> bringToFront() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Page_bringToFront, CRT_BRING_TO_FRONT, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<byte[]> captureScreenshot() {
        return (CompletableFuture<byte[]>) handler.invoke(
                this, DomainCommand.Page_captureScreenshot, CRT_CAPTURE_SCREENSHOT, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<byte[]> captureScreenshot(
            ImageFormat format,
            Integer quality,
            Viewport clip,
            Boolean fromSurface,
            Boolean captureBeyondViewport,
            Boolean optimizeForSpeed) {
        return (CompletableFuture<byte[]>) handler.invoke(
                this,
                DomainCommand.Page_captureScreenshot,
                CRT_CAPTURE_SCREENSHOT,
                PARAMS_CAPTURE_SCREENSHOT_2,
                new Object[] {format, quality, clip, fromSurface, captureBeyondViewport, optimizeForSpeed},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> captureSnapshot() {
        return (CompletableFuture<String>) handler.invoke(
                this, DomainCommand.Page_captureSnapshot, CRT_CAPTURE_SNAPSHOT, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> captureSnapshot(SnapshotType format) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Page_captureSnapshot,
                CRT_CAPTURE_SNAPSHOT,
                PARAMS_CAPTURE_SNAPSHOT_2,
                new Object[] {format},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearCompilationCache() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_clearCompilationCache,
                CRT_CLEAR_COMPILATION_CACHE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearDeviceMetricsOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_clearDeviceMetricsOverride,
                CRT_CLEAR_DEVICE_METRICS_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearDeviceOrientationOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_clearDeviceOrientationOverride,
                CRT_CLEAR_DEVICE_ORIENTATION_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearGeolocationOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_clearGeolocationOverride,
                CRT_CLEAR_GEOLOCATION_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> close() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Page_close, CRT_CLOSE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> crash() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Page_crash, CRT_CRASH, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> createIsolatedWorld(String frameId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.Page_createIsolatedWorld,
                CRT_CREATE_ISOLATED_WORLD,
                PARAMS_CREATE_ISOLATED_WORLD_1,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> createIsolatedWorld(
            String frameId, String worldName, Boolean grantUniveralAccess) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.Page_createIsolatedWorld,
                CRT_CREATE_ISOLATED_WORLD,
                PARAMS_CREATE_ISOLATED_WORLD_2,
                new Object[] {frameId, worldName, grantUniveralAccess},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteCookie(String cookieName, String url) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_deleteCookie,
                CRT_DELETE_COOKIE,
                PARAMS_DELETE_COOKIE_1,
                new Object[] {cookieName, url},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Page_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Page_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> generateTestReport(String message) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_generateTestReport,
                CRT_GENERATE_TEST_REPORT,
                PARAMS_GENERATE_TEST_REPORT_1,
                new Object[] {message},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> generateTestReport(String message, String group) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_generateTestReport,
                CRT_GENERATE_TEST_REPORT,
                PARAMS_GENERATE_TEST_REPORT_2,
                new Object[] {message, group},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<AdScriptId> getAdScriptId(String frameId) {
        return (CompletableFuture<AdScriptId>) handler.invoke(
                this,
                DomainCommand.Page_getAdScriptId,
                CRT_GET_AD_SCRIPT_ID,
                PARAMS_GET_AD_SCRIPT_ID_1,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetAppIdResult> getAppId() {
        return (CompletableFuture<GetAppIdResult>)
                handler.invoke(this, DomainCommand.Page_getAppId, CRT_GET_APP_ID, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetAppManifestResult> getAppManifest() {
        return (CompletableFuture<GetAppManifestResult>) handler.invoke(
                this, DomainCommand.Page_getAppManifest, CRT_GET_APP_MANIFEST, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetAppManifestResult> getAppManifest(String manifestId) {
        return (CompletableFuture<GetAppManifestResult>) handler.invoke(
                this,
                DomainCommand.Page_getAppManifest,
                CRT_GET_APP_MANIFEST,
                PARAMS_GET_APP_MANIFEST_2,
                new Object[] {manifestId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<FrameTree> getFrameTree() {
        return (CompletableFuture<FrameTree>) handler.invoke(
                this, DomainCommand.Page_getFrameTree, CRT_GET_FRAME_TREE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<InstallabilityError>> getInstallabilityErrors() {
        return (CompletableFuture<List<InstallabilityError>>) handler.invoke(
                this,
                DomainCommand.Page_getInstallabilityErrors,
                CRT_GET_INSTALLABILITY_ERRORS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetLayoutMetricsResult> getLayoutMetrics() {
        return (CompletableFuture<GetLayoutMetricsResult>) handler.invoke(
                this, DomainCommand.Page_getLayoutMetrics, CRT_GET_LAYOUT_METRICS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getManifestIcons() {
        return (CompletableFuture<String>) handler.invoke(
                this, DomainCommand.Page_getManifestIcons, CRT_GET_MANIFEST_ICONS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetNavigationHistoryResult> getNavigationHistory() {
        return (CompletableFuture<GetNavigationHistoryResult>) handler.invoke(
                this,
                DomainCommand.Page_getNavigationHistory,
                CRT_GET_NAVIGATION_HISTORY,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<OriginTrial>> getOriginTrials(String frameId) {
        return (CompletableFuture<List<OriginTrial>>) handler.invoke(
                this,
                DomainCommand.Page_getOriginTrials,
                CRT_GET_ORIGIN_TRIALS,
                PARAMS_GET_ORIGIN_TRIALS_1,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<PermissionsPolicyFeatureState>> getPermissionsPolicyState(String frameId) {
        return (CompletableFuture<List<PermissionsPolicyFeatureState>>) handler.invoke(
                this,
                DomainCommand.Page_getPermissionsPolicyState,
                CRT_GET_PERMISSIONS_POLICY_STATE,
                PARAMS_GET_PERMISSIONS_POLICY_STATE_1,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetResourceContentResult> getResourceContent(String frameId, String url) {
        return (CompletableFuture<GetResourceContentResult>) handler.invoke(
                this,
                DomainCommand.Page_getResourceContent,
                CRT_GET_RESOURCE_CONTENT,
                PARAMS_GET_RESOURCE_CONTENT_1,
                new Object[] {frameId, url},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<FrameResourceTree> getResourceTree() {
        return (CompletableFuture<FrameResourceTree>) handler.invoke(
                this, DomainCommand.Page_getResourceTree, CRT_GET_RESOURCE_TREE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> handleJavaScriptDialog(Boolean accept) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_handleJavaScriptDialog,
                CRT_HANDLE_JAVA_SCRIPT_DIALOG,
                PARAMS_HANDLE_JAVA_SCRIPT_DIALOG_1,
                new Object[] {accept},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> handleJavaScriptDialog(Boolean accept, String promptText) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_handleJavaScriptDialog,
                CRT_HANDLE_JAVA_SCRIPT_DIALOG,
                PARAMS_HANDLE_JAVA_SCRIPT_DIALOG_2,
                new Object[] {accept, promptText},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<NavigateResult> navigate(String url) {
        return (CompletableFuture<NavigateResult>) handler.invoke(
                this, DomainCommand.Page_navigate, CRT_NAVIGATE, PARAMS_NAVIGATE_1, new Object[] {url}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<NavigateResult> navigate(
            String url, String referrer, TransitionType transitionType, String frameId, ReferrerPolicy referrerPolicy) {
        return (CompletableFuture<NavigateResult>) handler.invoke(
                this,
                DomainCommand.Page_navigate,
                CRT_NAVIGATE,
                PARAMS_NAVIGATE_2,
                new Object[] {url, referrer, transitionType, frameId, referrerPolicy},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> navigateToHistoryEntry(Integer entryId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_navigateToHistoryEntry,
                CRT_NAVIGATE_TO_HISTORY_ENTRY,
                PARAMS_NAVIGATE_TO_HISTORY_ENTRY_1,
                new Object[] {entryId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<PrintToPDFResult> printToPDF() {
        return (CompletableFuture<PrintToPDFResult>)
                handler.invoke(this, DomainCommand.Page_printToPDF, CRT_PRINT_TO_PD_F, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<PrintToPDFResult> printToPDF(
            Boolean landscape,
            Boolean displayHeaderFooter,
            Boolean printBackground,
            Double scale,
            Double paperWidth,
            Double paperHeight,
            Double marginTop,
            Double marginBottom,
            Double marginLeft,
            Double marginRight,
            String pageRanges,
            String headerTemplate,
            String footerTemplate,
            Boolean preferCSSPageSize,
            TransferMode transferMode,
            Boolean generateTaggedPDF,
            Boolean generateDocumentOutline) {
        return (CompletableFuture<PrintToPDFResult>) handler.invoke(
                this,
                DomainCommand.Page_printToPDF,
                CRT_PRINT_TO_PD_F,
                PARAMS_PRINT_TO_PD_F_2,
                new Object[] {
                    landscape,
                    displayHeaderFooter,
                    printBackground,
                    scale,
                    paperWidth,
                    paperHeight,
                    marginTop,
                    marginBottom,
                    marginLeft,
                    marginRight,
                    pageRanges,
                    headerTemplate,
                    footerTemplate,
                    preferCSSPageSize,
                    transferMode,
                    generateTaggedPDF,
                    generateDocumentOutline
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> produceCompilationCache(List<CompilationCacheParams> scripts) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_produceCompilationCache,
                CRT_PRODUCE_COMPILATION_CACHE,
                PARAMS_PRODUCE_COMPILATION_CACHE_1,
                new Object[] {scripts},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> reload() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Page_reload, CRT_RELOAD, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> reload(Boolean ignoreCache, String scriptToEvaluateOnLoad, String loaderId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_reload,
                CRT_RELOAD,
                PARAMS_RELOAD_2,
                new Object[] {ignoreCache, scriptToEvaluateOnLoad, loaderId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeScriptToEvaluateOnLoad(String identifier) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_removeScriptToEvaluateOnLoad,
                CRT_REMOVE_SCRIPT_TO_EVALUATE_ON_LOAD,
                PARAMS_REMOVE_SCRIPT_TO_EVALUATE_ON_LOAD_1,
                new Object[] {identifier},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeScriptToEvaluateOnNewDocument(String identifier) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_removeScriptToEvaluateOnNewDocument,
                CRT_REMOVE_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT,
                PARAMS_REMOVE_SCRIPT_TO_EVALUATE_ON_NEW_DOCUMENT_1,
                new Object[] {identifier},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> resetNavigationHistory() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_resetNavigationHistory,
                CRT_RESET_NAVIGATION_HISTORY,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> screencastFrameAck(Integer sessionId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_screencastFrameAck,
                CRT_SCREENCAST_FRAME_ACK,
                PARAMS_SCREENCAST_FRAME_ACK_1,
                new Object[] {sessionId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<SearchMatch>> searchInResource(String frameId, String url, String query) {
        return (CompletableFuture<List<SearchMatch>>) handler.invoke(
                this,
                DomainCommand.Page_searchInResource,
                CRT_SEARCH_IN_RESOURCE,
                PARAMS_SEARCH_IN_RESOURCE_1,
                new Object[] {frameId, url, query},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<SearchMatch>> searchInResource(
            String frameId, String url, String query, Boolean caseSensitive, Boolean isRegex) {
        return (CompletableFuture<List<SearchMatch>>) handler.invoke(
                this,
                DomainCommand.Page_searchInResource,
                CRT_SEARCH_IN_RESOURCE,
                PARAMS_SEARCH_IN_RESOURCE_2,
                new Object[] {frameId, url, query, caseSensitive, isRegex},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAdBlockingEnabled(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setAdBlockingEnabled,
                CRT_SET_AD_BLOCKING_ENABLED,
                PARAMS_SET_AD_BLOCKING_ENABLED_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setBypassCSP(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setBypassCSP,
                CRT_SET_BYPASS_CS_P,
                PARAMS_SET_BYPASS_CS_P_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDeviceMetricsOverride(
            Integer width, Integer height, Double deviceScaleFactor, Boolean mobile) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setDeviceMetricsOverride,
                CRT_SET_DEVICE_METRICS_OVERRIDE,
                PARAMS_SET_DEVICE_METRICS_OVERRIDE_1,
                new Object[] {width, height, deviceScaleFactor, mobile},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDeviceMetricsOverride(
            Integer width,
            Integer height,
            Double deviceScaleFactor,
            Boolean mobile,
            Double scale,
            Integer screenWidth,
            Integer screenHeight,
            Integer positionX,
            Integer positionY,
            Boolean dontSetVisibleSize,
            ScreenOrientation screenOrientation,
            Viewport viewport) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setDeviceMetricsOverride,
                CRT_SET_DEVICE_METRICS_OVERRIDE,
                PARAMS_SET_DEVICE_METRICS_OVERRIDE_2,
                new Object[] {
                    width,
                    height,
                    deviceScaleFactor,
                    mobile,
                    scale,
                    screenWidth,
                    screenHeight,
                    positionX,
                    positionY,
                    dontSetVisibleSize,
                    screenOrientation,
                    viewport
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDeviceOrientationOverride(Double alpha, Double beta, Double gamma) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setDeviceOrientationOverride,
                CRT_SET_DEVICE_ORIENTATION_OVERRIDE,
                PARAMS_SET_DEVICE_ORIENTATION_OVERRIDE_1,
                new Object[] {alpha, beta, gamma},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDocumentContent(String frameId, String html) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setDocumentContent,
                CRT_SET_DOCUMENT_CONTENT,
                PARAMS_SET_DOCUMENT_CONTENT_1,
                new Object[] {frameId, html},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDownloadBehavior(DownloadBehavior behavior) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setDownloadBehavior,
                CRT_SET_DOWNLOAD_BEHAVIOR,
                PARAMS_SET_DOWNLOAD_BEHAVIOR_1,
                new Object[] {behavior},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDownloadBehavior(DownloadBehavior behavior, String downloadPath) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setDownloadBehavior,
                CRT_SET_DOWNLOAD_BEHAVIOR,
                PARAMS_SET_DOWNLOAD_BEHAVIOR_2,
                new Object[] {behavior, downloadPath},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setFontFamilies(FontFamilies fontFamilies) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setFontFamilies,
                CRT_SET_FONT_FAMILIES,
                PARAMS_SET_FONT_FAMILIES_1,
                new Object[] {fontFamilies},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setFontFamilies(FontFamilies fontFamilies, List<ScriptFontFamilies> forScripts) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setFontFamilies,
                CRT_SET_FONT_FAMILIES,
                PARAMS_SET_FONT_FAMILIES_2,
                new Object[] {fontFamilies, forScripts},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setFontSizes(FontSizes fontSizes) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setFontSizes,
                CRT_SET_FONT_SIZES,
                PARAMS_SET_FONT_SIZES_1,
                new Object[] {fontSizes},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setGeolocationOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setGeolocationOverride,
                CRT_SET_GEOLOCATION_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setGeolocationOverride(Double latitude, Double longitude, Double accuracy) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setGeolocationOverride,
                CRT_SET_GEOLOCATION_OVERRIDE,
                PARAMS_SET_GEOLOCATION_OVERRIDE_2,
                new Object[] {latitude, longitude, accuracy},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setInterceptFileChooserDialog(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setInterceptFileChooserDialog,
                CRT_SET_INTERCEPT_FILE_CHOOSER_DIALOG,
                PARAMS_SET_INTERCEPT_FILE_CHOOSER_DIALOG_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setLifecycleEventsEnabled(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setLifecycleEventsEnabled,
                CRT_SET_LIFECYCLE_EVENTS_ENABLED,
                PARAMS_SET_LIFECYCLE_EVENTS_ENABLED_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPrerenderingAllowed(Boolean isAllowed) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setPrerenderingAllowed,
                CRT_SET_PRERENDERING_ALLOWED,
                PARAMS_SET_PRERENDERING_ALLOWED_1,
                new Object[] {isAllowed},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setRPHRegistrationMode(AutoResponseMode mode) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setRPHRegistrationMode,
                CRT_SET_RP_HREGISTRATION_MODE,
                PARAMS_SET_RP_HREGISTRATION_MODE_1,
                new Object[] {mode},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSPCTransactionMode(AutoResponseMode mode) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setSPCTransactionMode,
                CRT_SET_SP_CTRANSACTION_MODE,
                PARAMS_SET_SP_CTRANSACTION_MODE_1,
                new Object[] {mode},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setTouchEmulationEnabled(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setTouchEmulationEnabled,
                CRT_SET_TOUCH_EMULATION_ENABLED,
                PARAMS_SET_TOUCH_EMULATION_ENABLED_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setTouchEmulationEnabled(Boolean enabled, Platform configuration) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setTouchEmulationEnabled,
                CRT_SET_TOUCH_EMULATION_ENABLED,
                PARAMS_SET_TOUCH_EMULATION_ENABLED_2,
                new Object[] {enabled, configuration},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setWebLifecycleState(WebLifecycleState state) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_setWebLifecycleState,
                CRT_SET_WEB_LIFECYCLE_STATE,
                PARAMS_SET_WEB_LIFECYCLE_STATE_1,
                new Object[] {state},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startScreencast() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Page_startScreencast, CRT_START_SCREENCAST, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startScreencast(
            ImageFormat format, Integer quality, Integer maxWidth, Integer maxHeight, Integer everyNthFrame) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Page_startScreencast,
                CRT_START_SCREENCAST,
                PARAMS_START_SCREENCAST_2,
                new Object[] {format, quality, maxWidth, maxHeight, everyNthFrame},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopLoading() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Page_stopLoading, CRT_STOP_LOADING, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopScreencast() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Page_stopScreencast, CRT_STOP_SCREENCAST, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> waitForDebugger() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Page_waitForDebugger, CRT_WAIT_FOR_DEBUGGER, EMPTY_ARGS, EMPTY_VALUES, false);
    }
}
