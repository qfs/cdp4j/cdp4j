// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.browser.Bounds;
import com.cdp4j.type.browser.BrowserCommandId;
import com.cdp4j.type.browser.GetVersionResult;
import com.cdp4j.type.browser.GetWindowForTargetResult;
import com.cdp4j.type.browser.Histogram;
import com.cdp4j.type.browser.PermissionDescriptor;
import com.cdp4j.type.browser.PermissionSetting;
import com.cdp4j.type.browser.PermissionType;
import com.cdp4j.type.constant.DownloadBehavior;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class BrowserAsyncImpl extends ParameterizedCommandImpl<BrowserAsync> implements BrowserAsync {

    private static final TypeReference<List<Histogram>> LIST_HISTOGRAM = new TypeReference<List<Histogram>>() {};
    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_ADD_PRIVACY_SANDBOX_ENROLLMENT_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CANCEL_DOWNLOAD = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLOSE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CRASH = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CRASH_GPU_PROCESS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EXECUTE_BROWSER_COMMAND = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_BROWSER_COMMAND_LINE =
            new CommandReturnType("arguments", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_HISTOGRAM =
            new CommandReturnType("histogram", Histogram.class, null);
    private static final CommandReturnType CRT_GET_HISTOGRAMS =
            new CommandReturnType("histograms", List.class, LIST_HISTOGRAM);
    private static final CommandReturnType CRT_GET_VERSION = new CommandReturnType(null, GetVersionResult.class, null);
    private static final CommandReturnType CRT_GET_WINDOW_BOUNDS = new CommandReturnType("bounds", Bounds.class, null);
    private static final CommandReturnType CRT_GET_WINDOW_FOR_TARGET =
            new CommandReturnType(null, GetWindowForTargetResult.class, null);
    private static final CommandReturnType CRT_GRANT_PERMISSIONS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RESET_PERMISSIONS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DOCK_TILE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DOWNLOAD_BEHAVIOR = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PERMISSION = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_WINDOW_BOUNDS = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ADD_PRIVACY_SANDBOX_ENROLLMENT_OVERRIDE_1 = new String[] {"url"};
    private static final String[] PARAMS_CANCEL_DOWNLOAD_1 = new String[] {"guid"};
    private static final String[] PARAMS_CANCEL_DOWNLOAD_2 = new String[] {"guid", "browserContextId"};
    private static final String[] PARAMS_EXECUTE_BROWSER_COMMAND_1 = new String[] {"commandId"};
    private static final String[] PARAMS_GET_HISTOGRAMS_2 = new String[] {"query", "delta"};
    private static final String[] PARAMS_GET_HISTOGRAM_1 = new String[] {"name"};
    private static final String[] PARAMS_GET_HISTOGRAM_2 = new String[] {"name", "delta"};
    private static final String[] PARAMS_GET_WINDOW_BOUNDS_1 = new String[] {"windowId"};
    private static final String[] PARAMS_GET_WINDOW_FOR_TARGET_2 = new String[] {"targetId"};
    private static final String[] PARAMS_GRANT_PERMISSIONS_1 = new String[] {"permissions"};
    private static final String[] PARAMS_GRANT_PERMISSIONS_2 =
            new String[] {"permissions", "origin", "browserContextId"};
    private static final String[] PARAMS_RESET_PERMISSIONS_2 = new String[] {"browserContextId"};
    private static final String[] PARAMS_SET_DOCK_TILE_2 = new String[] {"badgeLabel", "image"};
    private static final String[] PARAMS_SET_DOWNLOAD_BEHAVIOR_1 = new String[] {"behavior"};
    private static final String[] PARAMS_SET_DOWNLOAD_BEHAVIOR_2 =
            new String[] {"behavior", "browserContextId", "downloadPath", "eventsEnabled"};
    private static final String[] PARAMS_SET_PERMISSION_1 = new String[] {"permission", "setting"};
    private static final String[] PARAMS_SET_PERMISSION_2 =
            new String[] {"permission", "setting", "origin", "browserContextId"};
    private static final String[] PARAMS_SET_WINDOW_BOUNDS_1 = new String[] {"windowId", "bounds"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public BrowserAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> addPrivacySandboxEnrollmentOverride(String url) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_addPrivacySandboxEnrollmentOverride,
                CRT_ADD_PRIVACY_SANDBOX_ENROLLMENT_OVERRIDE,
                PARAMS_ADD_PRIVACY_SANDBOX_ENROLLMENT_OVERRIDE_1,
                new Object[] {url},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> cancelDownload(String guid) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_cancelDownload,
                CRT_CANCEL_DOWNLOAD,
                PARAMS_CANCEL_DOWNLOAD_1,
                new Object[] {guid},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> cancelDownload(String guid, String browserContextId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_cancelDownload,
                CRT_CANCEL_DOWNLOAD,
                PARAMS_CANCEL_DOWNLOAD_2,
                new Object[] {guid, browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> close() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Browser_close, CRT_CLOSE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> crash() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Browser_crash, CRT_CRASH, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> crashGpuProcess() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Browser_crashGpuProcess, CRT_CRASH_GPU_PROCESS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> executeBrowserCommand(BrowserCommandId commandId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_executeBrowserCommand,
                CRT_EXECUTE_BROWSER_COMMAND,
                PARAMS_EXECUTE_BROWSER_COMMAND_1,
                new Object[] {commandId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> getBrowserCommandLine() {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.Browser_getBrowserCommandLine,
                CRT_GET_BROWSER_COMMAND_LINE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Histogram> getHistogram(String name) {
        return (CompletableFuture<Histogram>) handler.invoke(
                this,
                DomainCommand.Browser_getHistogram,
                CRT_GET_HISTOGRAM,
                PARAMS_GET_HISTOGRAM_1,
                new Object[] {name},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Histogram> getHistogram(String name, Boolean delta) {
        return (CompletableFuture<Histogram>) handler.invoke(
                this,
                DomainCommand.Browser_getHistogram,
                CRT_GET_HISTOGRAM,
                PARAMS_GET_HISTOGRAM_2,
                new Object[] {name, delta},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Histogram>> getHistograms() {
        return (CompletableFuture<List<Histogram>>) handler.invoke(
                this, DomainCommand.Browser_getHistograms, CRT_GET_HISTOGRAMS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Histogram>> getHistograms(String query, Boolean delta) {
        return (CompletableFuture<List<Histogram>>) handler.invoke(
                this,
                DomainCommand.Browser_getHistograms,
                CRT_GET_HISTOGRAMS,
                PARAMS_GET_HISTOGRAMS_2,
                new Object[] {query, delta},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetVersionResult> getVersion() {
        return (CompletableFuture<GetVersionResult>) handler.invoke(
                this, DomainCommand.Browser_getVersion, CRT_GET_VERSION, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Bounds> getWindowBounds(Integer windowId) {
        return (CompletableFuture<Bounds>) handler.invoke(
                this,
                DomainCommand.Browser_getWindowBounds,
                CRT_GET_WINDOW_BOUNDS,
                PARAMS_GET_WINDOW_BOUNDS_1,
                new Object[] {windowId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetWindowForTargetResult> getWindowForTarget() {
        return (CompletableFuture<GetWindowForTargetResult>) handler.invoke(
                this,
                DomainCommand.Browser_getWindowForTarget,
                CRT_GET_WINDOW_FOR_TARGET,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetWindowForTargetResult> getWindowForTarget(String targetId) {
        return (CompletableFuture<GetWindowForTargetResult>) handler.invoke(
                this,
                DomainCommand.Browser_getWindowForTarget,
                CRT_GET_WINDOW_FOR_TARGET,
                PARAMS_GET_WINDOW_FOR_TARGET_2,
                new Object[] {targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> grantPermissions(PermissionType permissions) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_grantPermissions,
                CRT_GRANT_PERMISSIONS,
                PARAMS_GRANT_PERMISSIONS_1,
                new Object[] {permissions},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> grantPermissions(
            PermissionType permissions, String origin, String browserContextId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_grantPermissions,
                CRT_GRANT_PERMISSIONS,
                PARAMS_GRANT_PERMISSIONS_2,
                new Object[] {permissions, origin, browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> resetPermissions() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Browser_resetPermissions, CRT_RESET_PERMISSIONS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> resetPermissions(String browserContextId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_resetPermissions,
                CRT_RESET_PERMISSIONS,
                PARAMS_RESET_PERMISSIONS_2,
                new Object[] {browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDockTile() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Browser_setDockTile, CRT_SET_DOCK_TILE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDockTile(String badgeLabel, String image) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_setDockTile,
                CRT_SET_DOCK_TILE,
                PARAMS_SET_DOCK_TILE_2,
                new Object[] {badgeLabel, image},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDownloadBehavior(DownloadBehavior behavior) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_setDownloadBehavior,
                CRT_SET_DOWNLOAD_BEHAVIOR,
                PARAMS_SET_DOWNLOAD_BEHAVIOR_1,
                new Object[] {behavior},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDownloadBehavior(
            DownloadBehavior behavior, String browserContextId, String downloadPath, Boolean eventsEnabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_setDownloadBehavior,
                CRT_SET_DOWNLOAD_BEHAVIOR,
                PARAMS_SET_DOWNLOAD_BEHAVIOR_2,
                new Object[] {behavior, browserContextId, downloadPath, eventsEnabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPermission(PermissionDescriptor permission, PermissionSetting setting) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_setPermission,
                CRT_SET_PERMISSION,
                PARAMS_SET_PERMISSION_1,
                new Object[] {permission, setting},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setPermission(
            PermissionDescriptor permission, PermissionSetting setting, String origin, String browserContextId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_setPermission,
                CRT_SET_PERMISSION,
                PARAMS_SET_PERMISSION_2,
                new Object[] {permission, setting, origin, browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setWindowBounds(Integer windowId, Bounds bounds) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Browser_setWindowBounds,
                CRT_SET_WINDOW_BOUNDS,
                PARAMS_SET_WINDOW_BOUNDS_1,
                new Object[] {windowId, bounds},
                false);
    }
}
