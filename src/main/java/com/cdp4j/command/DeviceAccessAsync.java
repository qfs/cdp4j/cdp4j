// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface DeviceAccessAsync extends ParameterizedCommand<DeviceAccessAsync> {
    /**
     * Cancel a prompt in response to a DeviceAccess.deviceRequestPrompted event.
     *
     */
    CompletableFuture<Void> cancelPrompt(String id);

    /**
     * Disable events in this domain.
     */
    CompletableFuture<Void> disable();

    /**
     * Enable events in this domain.
     */
    CompletableFuture<Void> enable();

    /**
     * Select a device in response to a DeviceAccess.deviceRequestPrompted event.
     *
     */
    CompletableFuture<Void> selectPrompt(String id, String deviceId);
}
