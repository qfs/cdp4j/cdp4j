// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.network.Cookie;
import com.cdp4j.type.network.CookieParam;
import com.cdp4j.type.storage.GetUsageAndQuotaResult;
import com.cdp4j.type.storage.RelatedWebsiteSet;
import com.cdp4j.type.storage.SharedStorageEntry;
import com.cdp4j.type.storage.SharedStorageMetadata;
import com.cdp4j.type.storage.StorageBucket;
import com.cdp4j.type.storage.TrustTokens;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class StorageAsyncImpl extends ParameterizedCommandImpl<StorageAsync> implements StorageAsync {

    private static final TypeReference<List<Cookie>> LIST_COOKIE = new TypeReference<List<Cookie>>() {};
    private static final TypeReference<List<RelatedWebsiteSet>> LIST_RELATEDWEBSITESET =
            new TypeReference<List<RelatedWebsiteSet>>() {};
    private static final TypeReference<List<SharedStorageEntry>> LIST_SHAREDSTORAGEENTRY =
            new TypeReference<List<SharedStorageEntry>>() {};
    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final TypeReference<List<TrustTokens>> LIST_TRUSTTOKENS = new TypeReference<List<TrustTokens>>() {};
    private static final CommandReturnType CRT_CLEAR_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_DATA_FOR_ORIGIN = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_DATA_FOR_STORAGE_KEY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_SHARED_STORAGE_ENTRIES =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLEAR_TRUST_TOKENS =
            new CommandReturnType("didDeleteTokens", Boolean.class, null);
    private static final CommandReturnType CRT_DELETE_SHARED_STORAGE_ENTRY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_STORAGE_BUCKET = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_AFFECTED_URLS_FOR_THIRD_PARTY_COOKIE_METADATA =
            new CommandReturnType("matchedUrls", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_COOKIES = new CommandReturnType("cookies", List.class, LIST_COOKIE);
    private static final CommandReturnType CRT_GET_RELATED_WEBSITE_SETS =
            new CommandReturnType("sets", List.class, LIST_RELATEDWEBSITESET);
    private static final CommandReturnType CRT_GET_SHARED_STORAGE_ENTRIES =
            new CommandReturnType("entries", List.class, LIST_SHAREDSTORAGEENTRY);
    private static final CommandReturnType CRT_GET_SHARED_STORAGE_METADATA =
            new CommandReturnType("metadata", SharedStorageMetadata.class, null);
    private static final CommandReturnType CRT_GET_STORAGE_KEY_FOR_FRAME =
            new CommandReturnType("storageKey", String.class, null);
    private static final CommandReturnType CRT_GET_TRUST_TOKENS =
            new CommandReturnType("tokens", List.class, LIST_TRUSTTOKENS);
    private static final CommandReturnType CRT_GET_USAGE_AND_QUOTA =
            new CommandReturnType(null, GetUsageAndQuotaResult.class, null);
    private static final CommandReturnType CRT_OVERRIDE_QUOTA_FOR_ORIGIN =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RESET_SHARED_STORAGE_BUDGET =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RUN_BOUNCE_TRACKING_MITIGATIONS =
            new CommandReturnType("deletedSites", List.class, LIST_STRING);
    private static final CommandReturnType CRT_SEND_PENDING_ATTRIBUTION_REPORTS =
            new CommandReturnType("numSent", Integer.class, null);
    private static final CommandReturnType CRT_SET_ATTRIBUTION_REPORTING_LOCAL_TESTING_MODE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ATTRIBUTION_REPORTING_TRACKING =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_COOKIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INTEREST_GROUP_AUCTION_TRACKING =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INTEREST_GROUP_TRACKING =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHARED_STORAGE_ENTRY = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHARED_STORAGE_TRACKING =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_STORAGE_BUCKET_TRACKING =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TRACK_CACHE_STORAGE_FOR_ORIGIN =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TRACK_CACHE_STORAGE_FOR_STORAGE_KEY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TRACK_INDEXED_DB_FOR_ORIGIN =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TRACK_INDEXED_DB_FOR_STORAGE_KEY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNTRACK_CACHE_STORAGE_FOR_ORIGIN =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNTRACK_CACHE_STORAGE_FOR_STORAGE_KEY =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNTRACK_INDEXED_DB_FOR_ORIGIN =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNTRACK_INDEXED_DB_FOR_STORAGE_KEY =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CLEAR_COOKIES_2 = new String[] {"browserContextId"};
    private static final String[] PARAMS_CLEAR_DATA_FOR_ORIGIN_1 = new String[] {"origin", "storageTypes"};
    private static final String[] PARAMS_CLEAR_DATA_FOR_STORAGE_KEY_1 = new String[] {"storageKey", "storageTypes"};
    private static final String[] PARAMS_CLEAR_SHARED_STORAGE_ENTRIES_1 = new String[] {"ownerOrigin"};
    private static final String[] PARAMS_CLEAR_TRUST_TOKENS_1 = new String[] {"issuerOrigin"};
    private static final String[] PARAMS_DELETE_SHARED_STORAGE_ENTRY_1 = new String[] {"ownerOrigin", "key"};
    private static final String[] PARAMS_DELETE_STORAGE_BUCKET_1 = new String[] {"bucket"};
    private static final String[] PARAMS_GET_AFFECTED_URLS_FOR_THIRD_PARTY_COOKIE_METADATA_1 =
            new String[] {"firstPartyUrl", "thirdPartyUrls"};
    private static final String[] PARAMS_GET_COOKIES_2 = new String[] {"browserContextId"};
    private static final String[] PARAMS_GET_SHARED_STORAGE_ENTRIES_1 = new String[] {"ownerOrigin"};
    private static final String[] PARAMS_GET_SHARED_STORAGE_METADATA_1 = new String[] {"ownerOrigin"};
    private static final String[] PARAMS_GET_STORAGE_KEY_FOR_FRAME_1 = new String[] {"frameId"};
    private static final String[] PARAMS_GET_USAGE_AND_QUOTA_1 = new String[] {"origin"};
    private static final String[] PARAMS_OVERRIDE_QUOTA_FOR_ORIGIN_1 = new String[] {"origin"};
    private static final String[] PARAMS_OVERRIDE_QUOTA_FOR_ORIGIN_2 = new String[] {"origin", "quotaSize"};
    private static final String[] PARAMS_RESET_SHARED_STORAGE_BUDGET_1 = new String[] {"ownerOrigin"};
    private static final String[] PARAMS_SET_ATTRIBUTION_REPORTING_LOCAL_TESTING_MODE_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_ATTRIBUTION_REPORTING_TRACKING_1 = new String[] {"enable"};
    private static final String[] PARAMS_SET_COOKIES_1 = new String[] {"cookies"};
    private static final String[] PARAMS_SET_COOKIES_2 = new String[] {"cookies", "browserContextId"};
    private static final String[] PARAMS_SET_INTEREST_GROUP_AUCTION_TRACKING_1 = new String[] {"enable"};
    private static final String[] PARAMS_SET_INTEREST_GROUP_TRACKING_1 = new String[] {"enable"};
    private static final String[] PARAMS_SET_SHARED_STORAGE_ENTRY_1 = new String[] {"ownerOrigin", "key", "value"};
    private static final String[] PARAMS_SET_SHARED_STORAGE_ENTRY_2 =
            new String[] {"ownerOrigin", "key", "value", "ignoreIfPresent"};
    private static final String[] PARAMS_SET_SHARED_STORAGE_TRACKING_1 = new String[] {"enable"};
    private static final String[] PARAMS_SET_STORAGE_BUCKET_TRACKING_1 = new String[] {"storageKey", "enable"};
    private static final String[] PARAMS_TRACK_CACHE_STORAGE_FOR_ORIGIN_1 = new String[] {"origin"};
    private static final String[] PARAMS_TRACK_CACHE_STORAGE_FOR_STORAGE_KEY_1 = new String[] {"storageKey"};
    private static final String[] PARAMS_TRACK_INDEXED_DB_FOR_ORIGIN_1 = new String[] {"origin"};
    private static final String[] PARAMS_TRACK_INDEXED_DB_FOR_STORAGE_KEY_1 = new String[] {"storageKey"};
    private static final String[] PARAMS_UNTRACK_CACHE_STORAGE_FOR_ORIGIN_1 = new String[] {"origin"};
    private static final String[] PARAMS_UNTRACK_CACHE_STORAGE_FOR_STORAGE_KEY_1 = new String[] {"storageKey"};
    private static final String[] PARAMS_UNTRACK_INDEXED_DB_FOR_ORIGIN_1 = new String[] {"origin"};
    private static final String[] PARAMS_UNTRACK_INDEXED_DB_FOR_STORAGE_KEY_1 = new String[] {"storageKey"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public StorageAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearCookies() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Storage_clearCookies, CRT_CLEAR_COOKIES, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearCookies(String browserContextId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_clearCookies,
                CRT_CLEAR_COOKIES,
                PARAMS_CLEAR_COOKIES_2,
                new Object[] {browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearDataForOrigin(String origin, String storageTypes) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_clearDataForOrigin,
                CRT_CLEAR_DATA_FOR_ORIGIN,
                PARAMS_CLEAR_DATA_FOR_ORIGIN_1,
                new Object[] {origin, storageTypes},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearDataForStorageKey(String storageKey, String storageTypes) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_clearDataForStorageKey,
                CRT_CLEAR_DATA_FOR_STORAGE_KEY,
                PARAMS_CLEAR_DATA_FOR_STORAGE_KEY_1,
                new Object[] {storageKey, storageTypes},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearSharedStorageEntries(String ownerOrigin) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_clearSharedStorageEntries,
                CRT_CLEAR_SHARED_STORAGE_ENTRIES,
                PARAMS_CLEAR_SHARED_STORAGE_ENTRIES_1,
                new Object[] {ownerOrigin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> clearTrustTokens(String issuerOrigin) {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.Storage_clearTrustTokens,
                CRT_CLEAR_TRUST_TOKENS,
                PARAMS_CLEAR_TRUST_TOKENS_1,
                new Object[] {issuerOrigin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteSharedStorageEntry(String ownerOrigin, String key) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_deleteSharedStorageEntry,
                CRT_DELETE_SHARED_STORAGE_ENTRY,
                PARAMS_DELETE_SHARED_STORAGE_ENTRY_1,
                new Object[] {ownerOrigin, key},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteStorageBucket(StorageBucket bucket) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_deleteStorageBucket,
                CRT_DELETE_STORAGE_BUCKET,
                PARAMS_DELETE_STORAGE_BUCKET_1,
                new Object[] {bucket},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> getAffectedUrlsForThirdPartyCookieMetadata(
            String firstPartyUrl, List<String> thirdPartyUrls) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.Storage_getAffectedUrlsForThirdPartyCookieMetadata,
                CRT_GET_AFFECTED_URLS_FOR_THIRD_PARTY_COOKIE_METADATA,
                PARAMS_GET_AFFECTED_URLS_FOR_THIRD_PARTY_COOKIE_METADATA_1,
                new Object[] {firstPartyUrl, thirdPartyUrls},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Cookie>> getCookies() {
        return (CompletableFuture<List<Cookie>>) handler.invoke(
                this, DomainCommand.Storage_getCookies, CRT_GET_COOKIES, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Cookie>> getCookies(String browserContextId) {
        return (CompletableFuture<List<Cookie>>) handler.invoke(
                this,
                DomainCommand.Storage_getCookies,
                CRT_GET_COOKIES,
                PARAMS_GET_COOKIES_2,
                new Object[] {browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<RelatedWebsiteSet>> getRelatedWebsiteSets() {
        return (CompletableFuture<List<RelatedWebsiteSet>>) handler.invoke(
                this,
                DomainCommand.Storage_getRelatedWebsiteSets,
                CRT_GET_RELATED_WEBSITE_SETS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<SharedStorageEntry>> getSharedStorageEntries(String ownerOrigin) {
        return (CompletableFuture<List<SharedStorageEntry>>) handler.invoke(
                this,
                DomainCommand.Storage_getSharedStorageEntries,
                CRT_GET_SHARED_STORAGE_ENTRIES,
                PARAMS_GET_SHARED_STORAGE_ENTRIES_1,
                new Object[] {ownerOrigin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SharedStorageMetadata> getSharedStorageMetadata(String ownerOrigin) {
        return (CompletableFuture<SharedStorageMetadata>) handler.invoke(
                this,
                DomainCommand.Storage_getSharedStorageMetadata,
                CRT_GET_SHARED_STORAGE_METADATA,
                PARAMS_GET_SHARED_STORAGE_METADATA_1,
                new Object[] {ownerOrigin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getStorageKeyForFrame(String frameId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Storage_getStorageKeyForFrame,
                CRT_GET_STORAGE_KEY_FOR_FRAME,
                PARAMS_GET_STORAGE_KEY_FOR_FRAME_1,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<TrustTokens>> getTrustTokens() {
        return (CompletableFuture<List<TrustTokens>>) handler.invoke(
                this, DomainCommand.Storage_getTrustTokens, CRT_GET_TRUST_TOKENS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetUsageAndQuotaResult> getUsageAndQuota(String origin) {
        return (CompletableFuture<GetUsageAndQuotaResult>) handler.invoke(
                this,
                DomainCommand.Storage_getUsageAndQuota,
                CRT_GET_USAGE_AND_QUOTA,
                PARAMS_GET_USAGE_AND_QUOTA_1,
                new Object[] {origin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> overrideQuotaForOrigin(String origin) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_overrideQuotaForOrigin,
                CRT_OVERRIDE_QUOTA_FOR_ORIGIN,
                PARAMS_OVERRIDE_QUOTA_FOR_ORIGIN_1,
                new Object[] {origin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> overrideQuotaForOrigin(String origin, Double quotaSize) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_overrideQuotaForOrigin,
                CRT_OVERRIDE_QUOTA_FOR_ORIGIN,
                PARAMS_OVERRIDE_QUOTA_FOR_ORIGIN_2,
                new Object[] {origin, quotaSize},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> resetSharedStorageBudget(String ownerOrigin) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_resetSharedStorageBudget,
                CRT_RESET_SHARED_STORAGE_BUDGET,
                PARAMS_RESET_SHARED_STORAGE_BUDGET_1,
                new Object[] {ownerOrigin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> runBounceTrackingMitigations() {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.Storage_runBounceTrackingMitigations,
                CRT_RUN_BOUNCE_TRACKING_MITIGATIONS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> sendPendingAttributionReports() {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.Storage_sendPendingAttributionReports,
                CRT_SEND_PENDING_ATTRIBUTION_REPORTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAttributionReportingLocalTestingMode(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setAttributionReportingLocalTestingMode,
                CRT_SET_ATTRIBUTION_REPORTING_LOCAL_TESTING_MODE,
                PARAMS_SET_ATTRIBUTION_REPORTING_LOCAL_TESTING_MODE_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAttributionReportingTracking(Boolean enable) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setAttributionReportingTracking,
                CRT_SET_ATTRIBUTION_REPORTING_TRACKING,
                PARAMS_SET_ATTRIBUTION_REPORTING_TRACKING_1,
                new Object[] {enable},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setCookies(List<CookieParam> cookies) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setCookies,
                CRT_SET_COOKIES,
                PARAMS_SET_COOKIES_1,
                new Object[] {cookies},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setCookies(List<CookieParam> cookies, String browserContextId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setCookies,
                CRT_SET_COOKIES,
                PARAMS_SET_COOKIES_2,
                new Object[] {cookies, browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setInterestGroupAuctionTracking(Boolean enable) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setInterestGroupAuctionTracking,
                CRT_SET_INTEREST_GROUP_AUCTION_TRACKING,
                PARAMS_SET_INTEREST_GROUP_AUCTION_TRACKING_1,
                new Object[] {enable},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setInterestGroupTracking(Boolean enable) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setInterestGroupTracking,
                CRT_SET_INTEREST_GROUP_TRACKING,
                PARAMS_SET_INTEREST_GROUP_TRACKING_1,
                new Object[] {enable},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSharedStorageEntry(String ownerOrigin, String key, String value) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setSharedStorageEntry,
                CRT_SET_SHARED_STORAGE_ENTRY,
                PARAMS_SET_SHARED_STORAGE_ENTRY_1,
                new Object[] {ownerOrigin, key, value},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSharedStorageEntry(
            String ownerOrigin, String key, String value, Boolean ignoreIfPresent) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setSharedStorageEntry,
                CRT_SET_SHARED_STORAGE_ENTRY,
                PARAMS_SET_SHARED_STORAGE_ENTRY_2,
                new Object[] {ownerOrigin, key, value, ignoreIfPresent},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSharedStorageTracking(Boolean enable) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setSharedStorageTracking,
                CRT_SET_SHARED_STORAGE_TRACKING,
                PARAMS_SET_SHARED_STORAGE_TRACKING_1,
                new Object[] {enable},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setStorageBucketTracking(String storageKey, Boolean enable) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_setStorageBucketTracking,
                CRT_SET_STORAGE_BUCKET_TRACKING,
                PARAMS_SET_STORAGE_BUCKET_TRACKING_1,
                new Object[] {storageKey, enable},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trackCacheStorageForOrigin(String origin) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_trackCacheStorageForOrigin,
                CRT_TRACK_CACHE_STORAGE_FOR_ORIGIN,
                PARAMS_TRACK_CACHE_STORAGE_FOR_ORIGIN_1,
                new Object[] {origin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trackCacheStorageForStorageKey(String storageKey) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_trackCacheStorageForStorageKey,
                CRT_TRACK_CACHE_STORAGE_FOR_STORAGE_KEY,
                PARAMS_TRACK_CACHE_STORAGE_FOR_STORAGE_KEY_1,
                new Object[] {storageKey},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trackIndexedDBForOrigin(String origin) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_trackIndexedDBForOrigin,
                CRT_TRACK_INDEXED_DB_FOR_ORIGIN,
                PARAMS_TRACK_INDEXED_DB_FOR_ORIGIN_1,
                new Object[] {origin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trackIndexedDBForStorageKey(String storageKey) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_trackIndexedDBForStorageKey,
                CRT_TRACK_INDEXED_DB_FOR_STORAGE_KEY,
                PARAMS_TRACK_INDEXED_DB_FOR_STORAGE_KEY_1,
                new Object[] {storageKey},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> untrackCacheStorageForOrigin(String origin) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_untrackCacheStorageForOrigin,
                CRT_UNTRACK_CACHE_STORAGE_FOR_ORIGIN,
                PARAMS_UNTRACK_CACHE_STORAGE_FOR_ORIGIN_1,
                new Object[] {origin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> untrackCacheStorageForStorageKey(String storageKey) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_untrackCacheStorageForStorageKey,
                CRT_UNTRACK_CACHE_STORAGE_FOR_STORAGE_KEY,
                PARAMS_UNTRACK_CACHE_STORAGE_FOR_STORAGE_KEY_1,
                new Object[] {storageKey},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> untrackIndexedDBForOrigin(String origin) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_untrackIndexedDBForOrigin,
                CRT_UNTRACK_INDEXED_DB_FOR_ORIGIN,
                PARAMS_UNTRACK_INDEXED_DB_FOR_ORIGIN_1,
                new Object[] {origin},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> untrackIndexedDBForStorageKey(String storageKey) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Storage_untrackIndexedDBForStorageKey,
                CRT_UNTRACK_INDEXED_DB_FOR_STORAGE_KEY,
                PARAMS_UNTRACK_INDEXED_DB_FOR_STORAGE_KEY_1,
                new Object[] {storageKey},
                false);
    }
}
