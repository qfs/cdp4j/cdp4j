// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.css.CSSComputedStyleProperty;
import com.cdp4j.type.css.CSSContainerQuery;
import com.cdp4j.type.css.CSSLayerData;
import com.cdp4j.type.css.CSSMedia;
import com.cdp4j.type.css.CSSProperty;
import com.cdp4j.type.css.CSSRule;
import com.cdp4j.type.css.CSSScope;
import com.cdp4j.type.css.CSSStyle;
import com.cdp4j.type.css.CSSSupports;
import com.cdp4j.type.css.GetAnimatedStylesForNodeResult;
import com.cdp4j.type.css.GetBackgroundColorsResult;
import com.cdp4j.type.css.GetInlineStylesForNodeResult;
import com.cdp4j.type.css.GetMatchedStylesForNodeResult;
import com.cdp4j.type.css.PlatformFontUsage;
import com.cdp4j.type.css.RuleUsage;
import com.cdp4j.type.css.SelectorList;
import com.cdp4j.type.css.SourceRange;
import com.cdp4j.type.css.StyleDeclarationEdit;
import com.cdp4j.type.css.TakeCoverageDeltaResult;
import com.cdp4j.type.css.Value;
import com.cdp4j.type.dom.PseudoType;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This domain exposes CSS read/write operations. All CSS objects (stylesheets,
 * rules, and styles) have an associated id used in subsequent operations on the
 * related object. Each object type has a specific id structure, and those are
 * not interchangeable between objects of different kinds. CSS objects can be
 * loaded using the get*ForNode() calls (which accept a DOM node id). A client
 * can also keep track of stylesheets via the styleSheetAdded/styleSheetRemoved
 * events and subsequently load the required stylesheet contents using the
 * getStyleSheet[Text]() methods.
 */
@Experimental
public interface CSSAsync extends ParameterizedCommand<CSSAsync> {
    /**
     * Inserts a new rule with the given ruleText in a stylesheet with given
     * styleSheetId, at the position specified by location.
     *
     * @param styleSheetId
     *            The css style sheet identifier where a new rule should be
     *            inserted.
     * @param ruleText
     *            The text of a new rule.
     * @param location
     *            Text position of a new rule in the target style sheet.
     *
     * @return The newly created rule.
     */
    CompletableFuture<CSSRule> addRule(String styleSheetId, String ruleText, SourceRange location);

    /**
     * Inserts a new rule with the given ruleText in a stylesheet with given
     * styleSheetId, at the position specified by location.
     *
     * @param styleSheetId
     *            The css style sheet identifier where a new rule should be
     *            inserted.
     * @param ruleText
     *            The text of a new rule.
     * @param location
     *            Text position of a new rule in the target style sheet.
     * @param nodeForPropertySyntaxValidation
     *            NodeId for the DOM node in whose context custom property
     *            declarations for registered properties should be validated. If
     *            omitted, declarations in the new rule text can only be validated
     *            statically, which may produce incorrect results if the declaration
     *            contains a var() for example.
     *
     * @return The newly created rule.
     */
    CompletableFuture<CSSRule> addRule(
            String styleSheetId,
            String ruleText,
            SourceRange location,
            @Experimental @Optional Integer nodeForPropertySyntaxValidation);

    /**
     * Returns all class names from specified stylesheet.
     *
     *
     * @return Class name list.
     */
    CompletableFuture<List<String>> collectClassNames(String styleSheetId);

    /**
     * Creates a new special "via-inspector" stylesheet in the frame with given
     * frameId.
     *
     * @param frameId
     *            Identifier of the frame where "via-inspector" stylesheet should be
     *            created.
     *
     * @return Identifier of the created "via-inspector" stylesheet.
     */
    CompletableFuture<String> createStyleSheet(String frameId);

    /**
     * Creates a new special "via-inspector" stylesheet in the frame with given
     * frameId.
     *
     * @param frameId
     *            Identifier of the frame where "via-inspector" stylesheet should be
     *            created.
     * @param force
     *            If true, creates a new stylesheet for every call. If false,
     *            returns a stylesheet previously created by a call with force=false
     *            for the frame's document if it exists or creates a new stylesheet
     *            (default: false).
     *
     * @return Identifier of the created "via-inspector" stylesheet.
     */
    CompletableFuture<String> createStyleSheet(String frameId, @Optional Boolean force);

    /**
     * Disables the CSS agent for the given page.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables the CSS agent for the given page. Clients should not assume that the
     * CSS agent has been enabled until the result of this command is received.
     */
    CompletableFuture<Void> enable();

    /**
     * Ensures that the given node will have specified pseudo-classes whenever its
     * style is computed by the browser.
     *
     * @param nodeId
     *            The element id for which to force the pseudo state.
     * @param forcedPseudoClasses
     *            Element pseudo classes to force when computing the element's
     *            style.
     */
    CompletableFuture<Void> forcePseudoState(Integer nodeId, List<String> forcedPseudoClasses);

    /**
     * Ensures that the given node is in its starting-style state.
     *
     * @param nodeId
     *            The element id for which to force the starting-style state.
     * @param forced
     *            Boolean indicating if this is on or off.
     */
    CompletableFuture<Void> forceStartingStyle(Integer nodeId, Boolean forced);

    /**
     * Returns the styles coming from animations & transitions including the
     * animation & transition styles coming from inheritance chain.
     *
     *
     * @return GetAnimatedStylesForNodeResult
     */
    @Experimental
    CompletableFuture<GetAnimatedStylesForNodeResult> getAnimatedStylesForNode(Integer nodeId);

    /**
     *
     * @return GetBackgroundColorsResult
     */
    CompletableFuture<GetBackgroundColorsResult> getBackgroundColors(Integer nodeId);

    /**
     * Returns the computed style for a DOM node identified by nodeId.
     *
     *
     * @return Computed style for the specified DOM node.
     */
    CompletableFuture<List<CSSComputedStyleProperty>> getComputedStyleForNode(Integer nodeId);

    /**
     * Returns the styles defined inline (explicitly in the "style" attribute and
     * implicitly, using DOM attributes) for a DOM node identified by nodeId.
     *
     *
     * @return GetInlineStylesForNodeResult
     */
    CompletableFuture<GetInlineStylesForNodeResult> getInlineStylesForNode(Integer nodeId);

    /**
     * Returns all layers parsed by the rendering engine for the tree scope of a
     * node. Given a DOM element identified by nodeId, getLayersForNode returns the
     * root layer for the nearest ancestor document or shadow root. The layer root
     * contains the full layer tree for the tree scope and their ordering.
     *
     */
    @Experimental
    CompletableFuture<CSSLayerData> getLayersForNode(Integer nodeId);

    /**
     * Given a CSS selector text and a style sheet ID, getLocationForSelector
     * returns an array of locations of the CSS selector in the style sheet.
     *
     */
    @Experimental
    CompletableFuture<List<SourceRange>> getLocationForSelector(String styleSheetId, String selectorText);

    @Experimental
    CompletableFuture<List<CSSProperty>> getLonghandProperties(String shorthandName, String value);

    /**
     * Returns requested styles for a DOM node identified by nodeId.
     *
     *
     * @return GetMatchedStylesForNodeResult
     */
    CompletableFuture<GetMatchedStylesForNodeResult> getMatchedStylesForNode(Integer nodeId);

    /**
     * Returns all media queries parsed by the rendering engine.
     */
    CompletableFuture<List<CSSMedia>> getMediaQueries();

    /**
     * Requests information about platform fonts which we used to render child
     * TextNodes in the given node.
     *
     *
     * @return Usage statistics for every employed platform font.
     */
    CompletableFuture<List<PlatformFontUsage>> getPlatformFontsForNode(Integer nodeId);

    /**
     * Returns the current textual content for a stylesheet.
     *
     *
     * @return The stylesheet text.
     */
    CompletableFuture<String> getStyleSheetText(String styleSheetId);

    /**
     * Resolve the specified values in the context of the provided element. For
     * example, a value of '1em' is evaluated according to the computed 'font-size'
     * of the element and a value 'calc(1px + 2px)' will be resolved to '3px'.
     *
     * @param values
     *            Substitution functions (var()/env()/attr()) and cascade-dependent
     *            keywords (revert/revert-layer) do not work.
     * @param nodeId
     *            Id of the node in whose context the expression is evaluated
     */
    CompletableFuture<List<String>> resolveValues(List<String> values, Integer nodeId);

    /**
     * Resolve the specified values in the context of the provided element. For
     * example, a value of '1em' is evaluated according to the computed 'font-size'
     * of the element and a value 'calc(1px + 2px)' will be resolved to '3px'.
     *
     * @param values
     *            Substitution functions (var()/env()/attr()) and cascade-dependent
     *            keywords (revert/revert-layer) do not work.
     * @param nodeId
     *            Id of the node in whose context the expression is evaluated
     * @param propertyName
     *            Only longhands and custom property names are accepted.
     * @param pseudoType
     *            Pseudo element type, only works for pseudo elements that generate
     *            elements in the tree, such as ::before and ::after.
     * @param pseudoIdentifier
     *            Pseudo element custom ident.
     */
    CompletableFuture<List<String>> resolveValues(
            List<String> values,
            Integer nodeId,
            @Optional String propertyName,
            @Experimental @Optional PseudoType pseudoType,
            @Experimental @Optional String pseudoIdentifier);

    /**
     * Modifies the expression of a container query.
     *
     *
     * @return The resulting CSS container query rule after modification.
     */
    @Experimental
    CompletableFuture<CSSContainerQuery> setContainerQueryText(String styleSheetId, SourceRange range, String text);

    /**
     * Find a rule with the given active property for the given node and set the new
     * value for this property
     *
     * @param nodeId
     *            The element id for which to set property.
     */
    CompletableFuture<Void> setEffectivePropertyValueForNode(Integer nodeId, String propertyName, String value);

    /**
     * Modifies the keyframe rule key text.
     *
     *
     * @return The resulting key text after modification.
     */
    CompletableFuture<Value> setKeyframeKey(String styleSheetId, SourceRange range, String keyText);

    /**
     * Enables/disables rendering of local CSS fonts (enabled by default).
     *
     * @param enabled
     *            Whether rendering of local fonts is enabled.
     */
    @Experimental
    CompletableFuture<Void> setLocalFontsEnabled(Boolean enabled);

    /**
     * Modifies the rule selector.
     *
     *
     * @return The resulting CSS media rule after modification.
     */
    CompletableFuture<CSSMedia> setMediaText(String styleSheetId, SourceRange range, String text);

    /**
     * Modifies the property rule property name.
     *
     *
     * @return The resulting key text after modification.
     */
    CompletableFuture<Value> setPropertyRulePropertyName(String styleSheetId, SourceRange range, String propertyName);

    /**
     * Modifies the rule selector.
     *
     *
     * @return The resulting selector list after modification.
     */
    CompletableFuture<SelectorList> setRuleSelector(String styleSheetId, SourceRange range, String selector);

    /**
     * Modifies the expression of a scope at-rule.
     *
     *
     * @return The resulting CSS Scope rule after modification.
     */
    @Experimental
    CompletableFuture<CSSScope> setScopeText(String styleSheetId, SourceRange range, String text);

    /**
     * Sets the new stylesheet text.
     *
     *
     * @return URL of source map associated with script (if any).
     */
    CompletableFuture<String> setStyleSheetText(String styleSheetId, String text);

    /**
     * Applies specified style edits one after another in the given order.
     *
     *
     * @return The resulting styles after modification.
     */
    CompletableFuture<List<CSSStyle>> setStyleTexts(List<StyleDeclarationEdit> edits);

    /**
     * Applies specified style edits one after another in the given order.
     *
     * @param nodeForPropertySyntaxValidation
     *            NodeId for the DOM node in whose context custom property
     *            declarations for registered properties should be validated. If
     *            omitted, declarations in the new rule text can only be validated
     *            statically, which may produce incorrect results if the declaration
     *            contains a var() for example.
     *
     * @return The resulting styles after modification.
     */
    CompletableFuture<List<CSSStyle>> setStyleTexts(
            List<StyleDeclarationEdit> edits, @Experimental @Optional Integer nodeForPropertySyntaxValidation);

    /**
     * Modifies the expression of a supports at-rule.
     *
     *
     * @return The resulting CSS Supports rule after modification.
     */
    @Experimental
    CompletableFuture<CSSSupports> setSupportsText(String styleSheetId, SourceRange range, String text);

    /**
     * Enables the selector recording.
     */
    CompletableFuture<Void> startRuleUsageTracking();

    /**
     * Stop tracking rule usage and return the list of rules that were used since
     * last call to takeCoverageDelta (or since start of coverage instrumentation).
     */
    CompletableFuture<List<RuleUsage>> stopRuleUsageTracking();

    /**
     * Polls the next batch of computed style updates.
     *
     * @return The list of node Ids that have their tracked computed styles updated.
     */
    @Experimental
    CompletableFuture<List<Integer>> takeComputedStyleUpdates();

    /**
     * Obtain list of rules that became used since last call to this method (or
     * since start of coverage instrumentation).
     *
     * @return TakeCoverageDeltaResult
     */
    CompletableFuture<TakeCoverageDeltaResult> takeCoverageDelta();

    /**
     * Starts tracking the given computed styles for updates. The specified array of
     * properties replaces the one previously specified. Pass empty array to disable
     * tracking. Use takeComputedStyleUpdates to retrieve the list of nodes that had
     * properties modified. The changes to computed style properties are only
     * tracked for nodes pushed to the front-end by the DOM agent. If no changes to
     * the tracked properties occur after the node has been pushed to the front-end,
     * no updates will be issued for the node.
     *
     */
    @Experimental
    CompletableFuture<Void> trackComputedStyleUpdates(List<CSSComputedStyleProperty> propertiesToTrack);

    /**
     * Starts tracking the given node for the computed style updates and whenever
     * the computed style is updated for node, it queues a computedStyleUpdated
     * event with throttling. There can only be 1 node tracked for computed style
     * updates so passing a new node id removes tracking from the previous node.
     * Pass undefined to disable tracking.
     */
    @Experimental
    CompletableFuture<Void> trackComputedStyleUpdatesForNode();

    /**
     * Starts tracking the given node for the computed style updates and whenever
     * the computed style is updated for node, it queues a computedStyleUpdated
     * event with throttling. There can only be 1 node tracked for computed style
     * updates so passing a new node id removes tracking from the previous node.
     * Pass undefined to disable tracking.
     *
     */
    @Experimental
    CompletableFuture<Void> trackComputedStyleUpdatesForNode(@Optional Integer nodeId);
}
