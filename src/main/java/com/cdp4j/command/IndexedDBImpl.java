// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.indexeddb.DatabaseWithObjectStores;
import com.cdp4j.type.indexeddb.GetMetadataResult;
import com.cdp4j.type.indexeddb.KeyRange;
import com.cdp4j.type.indexeddb.RequestDataResult;
import com.cdp4j.type.storage.StorageBucket;
import java.util.List;

class IndexedDBImpl extends ParameterizedCommandImpl<IndexedDB> implements IndexedDB {

    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_CLEAR_OBJECT_STORE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_DATABASE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_OBJECT_STORE_ENTRIES =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_METADATA =
            new CommandReturnType(null, GetMetadataResult.class, null);
    private static final CommandReturnType CRT_REQUEST_DATA =
            new CommandReturnType(null, RequestDataResult.class, null);
    private static final CommandReturnType CRT_REQUEST_DATABASE =
            new CommandReturnType("databaseWithObjectStores", DatabaseWithObjectStores.class, null);
    private static final CommandReturnType CRT_REQUEST_DATABASE_NAMES =
            new CommandReturnType("databaseNames", List.class, LIST_STRING);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CLEAR_OBJECT_STORE_1 = new String[] {"databaseName", "objectStoreName"};
    private static final String[] PARAMS_CLEAR_OBJECT_STORE_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName", "objectStoreName"};
    private static final String[] PARAMS_DELETE_DATABASE_1 = new String[] {"databaseName"};
    private static final String[] PARAMS_DELETE_DATABASE_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName"};
    private static final String[] PARAMS_DELETE_OBJECT_STORE_ENTRIES_1 =
            new String[] {"databaseName", "objectStoreName", "keyRange"};
    private static final String[] PARAMS_DELETE_OBJECT_STORE_ENTRIES_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName", "objectStoreName", "keyRange"
            };
    private static final String[] PARAMS_GET_METADATA_1 = new String[] {"databaseName", "objectStoreName"};
    private static final String[] PARAMS_GET_METADATA_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName", "objectStoreName"};
    private static final String[] PARAMS_REQUEST_DATABASE_1 = new String[] {"databaseName"};
    private static final String[] PARAMS_REQUEST_DATABASE_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName"};
    private static final String[] PARAMS_REQUEST_DATABASE_NAMES_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket"};
    private static final String[] PARAMS_REQUEST_DATA_1 =
            new String[] {"databaseName", "objectStoreName", "indexName", "skipCount", "pageSize"};
    private static final String[] PARAMS_REQUEST_DATA_2 = new String[] {
        "securityOrigin",
        "storageKey",
        "storageBucket",
        "databaseName",
        "objectStoreName",
        "indexName",
        "skipCount",
        "pageSize",
        "keyRange"
    };
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public IndexedDBImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void clearObjectStore(String databaseName, String objectStoreName) {
        handler.invoke(
                this,
                DomainCommand.IndexedDB_clearObjectStore,
                CRT_CLEAR_OBJECT_STORE,
                PARAMS_CLEAR_OBJECT_STORE_1,
                new Object[] {databaseName, objectStoreName},
                true);
    }

    @Override
    public void clearObjectStore(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName) {
        handler.invoke(
                this,
                DomainCommand.IndexedDB_clearObjectStore,
                CRT_CLEAR_OBJECT_STORE,
                PARAMS_CLEAR_OBJECT_STORE_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName, objectStoreName},
                true);
    }

    @Override
    public void deleteDatabase(String databaseName) {
        handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteDatabase,
                CRT_DELETE_DATABASE,
                PARAMS_DELETE_DATABASE_1,
                new Object[] {databaseName},
                true);
    }

    @Override
    public void deleteDatabase(
            String securityOrigin, String storageKey, StorageBucket storageBucket, String databaseName) {
        handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteDatabase,
                CRT_DELETE_DATABASE,
                PARAMS_DELETE_DATABASE_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName},
                true);
    }

    @Override
    public void deleteObjectStoreEntries(String databaseName, String objectStoreName, KeyRange keyRange) {
        handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteObjectStoreEntries,
                CRT_DELETE_OBJECT_STORE_ENTRIES,
                PARAMS_DELETE_OBJECT_STORE_ENTRIES_1,
                new Object[] {databaseName, objectStoreName, keyRange},
                true);
    }

    @Override
    public void deleteObjectStoreEntries(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName,
            KeyRange keyRange) {
        handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteObjectStoreEntries,
                CRT_DELETE_OBJECT_STORE_ENTRIES,
                PARAMS_DELETE_OBJECT_STORE_ENTRIES_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName, objectStoreName, keyRange},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.IndexedDB_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.IndexedDB_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public GetMetadataResult getMetadata(String databaseName, String objectStoreName) {
        return (GetMetadataResult) handler.invoke(
                this,
                DomainCommand.IndexedDB_getMetadata,
                CRT_GET_METADATA,
                PARAMS_GET_METADATA_1,
                new Object[] {databaseName, objectStoreName},
                true);
    }

    @Override
    public GetMetadataResult getMetadata(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName) {
        return (GetMetadataResult) handler.invoke(
                this,
                DomainCommand.IndexedDB_getMetadata,
                CRT_GET_METADATA,
                PARAMS_GET_METADATA_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName, objectStoreName},
                true);
    }

    @Override
    public RequestDataResult requestData(
            String databaseName, String objectStoreName, String indexName, Integer skipCount, Integer pageSize) {
        return (RequestDataResult) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestData,
                CRT_REQUEST_DATA,
                PARAMS_REQUEST_DATA_1,
                new Object[] {databaseName, objectStoreName, indexName, skipCount, pageSize},
                true);
    }

    @Override
    public RequestDataResult requestData(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName,
            String indexName,
            Integer skipCount,
            Integer pageSize,
            KeyRange keyRange) {
        return (RequestDataResult) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestData,
                CRT_REQUEST_DATA,
                PARAMS_REQUEST_DATA_2,
                new Object[] {
                    securityOrigin,
                    storageKey,
                    storageBucket,
                    databaseName,
                    objectStoreName,
                    indexName,
                    skipCount,
                    pageSize,
                    keyRange
                },
                true);
    }

    @Override
    public DatabaseWithObjectStores requestDatabase(String databaseName) {
        return (DatabaseWithObjectStores) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabase,
                CRT_REQUEST_DATABASE,
                PARAMS_REQUEST_DATABASE_1,
                new Object[] {databaseName},
                true);
    }

    @Override
    public DatabaseWithObjectStores requestDatabase(
            String securityOrigin, String storageKey, StorageBucket storageBucket, String databaseName) {
        return (DatabaseWithObjectStores) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabase,
                CRT_REQUEST_DATABASE,
                PARAMS_REQUEST_DATABASE_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> requestDatabaseNames() {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabaseNames,
                CRT_REQUEST_DATABASE_NAMES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> requestDatabaseNames(String securityOrigin, String storageKey, StorageBucket storageBucket) {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabaseNames,
                CRT_REQUEST_DATABASE_NAMES,
                PARAMS_REQUEST_DATABASE_NAMES_2,
                new Object[] {securityOrigin, storageKey, storageBucket},
                true);
    }
}
