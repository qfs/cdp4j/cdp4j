// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.DragEvent;
import com.cdp4j.type.constant.KeyEventType;
import com.cdp4j.type.constant.MouseEventType;
import com.cdp4j.type.constant.PointerType;
import com.cdp4j.type.constant.TouchEventType;
import com.cdp4j.type.input.DragData;
import com.cdp4j.type.input.GestureSourceType;
import com.cdp4j.type.input.MouseButton;
import com.cdp4j.type.input.TouchPoint;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class InputAsyncImpl extends ParameterizedCommandImpl<InputAsync> implements InputAsync {

    private static final CommandReturnType CRT_CANCEL_DRAGGING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_DRAG_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_KEY_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_MOUSE_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_TOUCH_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EMULATE_TOUCH_FROM_MOUSE_EVENT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_IME_SET_COMPOSITION = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_INSERT_TEXT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_IGNORE_INPUT_EVENTS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INTERCEPT_DRAGS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SYNTHESIZE_PINCH_GESTURE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SYNTHESIZE_SCROLL_GESTURE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SYNTHESIZE_TAP_GESTURE = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_DISPATCH_DRAG_EVENT_1 = new String[] {"type", "x", "y", "data"};
    private static final String[] PARAMS_DISPATCH_DRAG_EVENT_2 = new String[] {"type", "x", "y", "data", "modifiers"};
    private static final String[] PARAMS_DISPATCH_KEY_EVENT_1 = new String[] {"type"};
    private static final String[] PARAMS_DISPATCH_KEY_EVENT_2 = new String[] {
        "type",
        "modifiers",
        "timestamp",
        "text",
        "unmodifiedText",
        "keyIdentifier",
        "code",
        "key",
        "windowsVirtualKeyCode",
        "nativeVirtualKeyCode",
        "autoRepeat",
        "isKeypad",
        "isSystemKey",
        "location",
        "commands"
    };
    private static final String[] PARAMS_DISPATCH_MOUSE_EVENT_1 = new String[] {"type", "x", "y"};
    private static final String[] PARAMS_DISPATCH_MOUSE_EVENT_2 = new String[] {
        "type",
        "x",
        "y",
        "modifiers",
        "timestamp",
        "button",
        "buttons",
        "clickCount",
        "force",
        "tangentialPressure",
        "tiltX",
        "tiltY",
        "twist",
        "deltaX",
        "deltaY",
        "pointerType"
    };
    private static final String[] PARAMS_DISPATCH_TOUCH_EVENT_1 = new String[] {"type", "touchPoints"};
    private static final String[] PARAMS_DISPATCH_TOUCH_EVENT_2 =
            new String[] {"type", "touchPoints", "modifiers", "timestamp"};
    private static final String[] PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_1 = new String[] {"type", "x", "y", "button"};
    private static final String[] PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_2 =
            new String[] {"type", "x", "y", "button", "timestamp", "deltaX", "deltaY", "modifiers", "clickCount"};
    private static final String[] PARAMS_IME_SET_COMPOSITION_1 =
            new String[] {"text", "selectionStart", "selectionEnd"};
    private static final String[] PARAMS_IME_SET_COMPOSITION_2 =
            new String[] {"text", "selectionStart", "selectionEnd", "replacementStart", "replacementEnd"};
    private static final String[] PARAMS_INSERT_TEXT_1 = new String[] {"text"};
    private static final String[] PARAMS_SET_IGNORE_INPUT_EVENTS_1 = new String[] {"ignore"};
    private static final String[] PARAMS_SET_INTERCEPT_DRAGS_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SYNTHESIZE_PINCH_GESTURE_1 = new String[] {"x", "y", "scaleFactor"};
    private static final String[] PARAMS_SYNTHESIZE_PINCH_GESTURE_2 =
            new String[] {"x", "y", "scaleFactor", "relativeSpeed", "gestureSourceType"};
    private static final String[] PARAMS_SYNTHESIZE_SCROLL_GESTURE_1 = new String[] {"x", "y"};
    private static final String[] PARAMS_SYNTHESIZE_SCROLL_GESTURE_2 = new String[] {
        "x",
        "y",
        "xDistance",
        "yDistance",
        "xOverscroll",
        "yOverscroll",
        "preventFling",
        "speed",
        "gestureSourceType",
        "repeatCount",
        "repeatDelayMs",
        "interactionMarkerName"
    };
    private static final String[] PARAMS_SYNTHESIZE_TAP_GESTURE_1 = new String[] {"x", "y"};
    private static final String[] PARAMS_SYNTHESIZE_TAP_GESTURE_2 =
            new String[] {"x", "y", "duration", "tapCount", "gestureSourceType"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public InputAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> cancelDragging() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Input_cancelDragging, CRT_CANCEL_DRAGGING, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchDragEvent(DragEvent type, Double x, Double y, DragData data) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchDragEvent,
                CRT_DISPATCH_DRAG_EVENT,
                PARAMS_DISPATCH_DRAG_EVENT_1,
                new Object[] {type, x, y, data},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchDragEvent(
            DragEvent type, Double x, Double y, DragData data, Integer modifiers) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchDragEvent,
                CRT_DISPATCH_DRAG_EVENT,
                PARAMS_DISPATCH_DRAG_EVENT_2,
                new Object[] {type, x, y, data, modifiers},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchKeyEvent(KeyEventType type) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchKeyEvent,
                CRT_DISPATCH_KEY_EVENT,
                PARAMS_DISPATCH_KEY_EVENT_1,
                new Object[] {type},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchKeyEvent(
            KeyEventType type,
            Integer modifiers,
            Double timestamp,
            String text,
            String unmodifiedText,
            String keyIdentifier,
            String code,
            String key,
            Integer windowsVirtualKeyCode,
            Integer nativeVirtualKeyCode,
            Boolean autoRepeat,
            Boolean isKeypad,
            Boolean isSystemKey,
            Integer location,
            List<String> commands) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchKeyEvent,
                CRT_DISPATCH_KEY_EVENT,
                PARAMS_DISPATCH_KEY_EVENT_2,
                new Object[] {
                    type,
                    modifiers,
                    timestamp,
                    text,
                    unmodifiedText,
                    keyIdentifier,
                    code,
                    key,
                    windowsVirtualKeyCode,
                    nativeVirtualKeyCode,
                    autoRepeat,
                    isKeypad,
                    isSystemKey,
                    location,
                    commands
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchMouseEvent(MouseEventType type, Double x, Double y) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchMouseEvent,
                CRT_DISPATCH_MOUSE_EVENT,
                PARAMS_DISPATCH_MOUSE_EVENT_1,
                new Object[] {type, x, y},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchMouseEvent(
            MouseEventType type,
            Double x,
            Double y,
            Integer modifiers,
            Double timestamp,
            MouseButton button,
            Integer buttons,
            Integer clickCount,
            Double force,
            Double tangentialPressure,
            Double tiltX,
            Double tiltY,
            Integer twist,
            Double deltaX,
            Double deltaY,
            PointerType pointerType) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchMouseEvent,
                CRT_DISPATCH_MOUSE_EVENT,
                PARAMS_DISPATCH_MOUSE_EVENT_2,
                new Object[] {
                    type,
                    x,
                    y,
                    modifiers,
                    timestamp,
                    button,
                    buttons,
                    clickCount,
                    force,
                    tangentialPressure,
                    tiltX,
                    tiltY,
                    twist,
                    deltaX,
                    deltaY,
                    pointerType
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchTouchEvent(TouchEventType type, List<TouchPoint> touchPoints) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchTouchEvent,
                CRT_DISPATCH_TOUCH_EVENT,
                PARAMS_DISPATCH_TOUCH_EVENT_1,
                new Object[] {type, touchPoints},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> dispatchTouchEvent(
            TouchEventType type, List<TouchPoint> touchPoints, Integer modifiers, Double timestamp) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_dispatchTouchEvent,
                CRT_DISPATCH_TOUCH_EVENT,
                PARAMS_DISPATCH_TOUCH_EVENT_2,
                new Object[] {type, touchPoints, modifiers, timestamp},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> emulateTouchFromMouseEvent(
            MouseEventType type, Integer x, Integer y, MouseButton button) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_emulateTouchFromMouseEvent,
                CRT_EMULATE_TOUCH_FROM_MOUSE_EVENT,
                PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_1,
                new Object[] {type, x, y, button},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> emulateTouchFromMouseEvent(
            MouseEventType type,
            Integer x,
            Integer y,
            MouseButton button,
            Double timestamp,
            Double deltaX,
            Double deltaY,
            Integer modifiers,
            Integer clickCount) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_emulateTouchFromMouseEvent,
                CRT_EMULATE_TOUCH_FROM_MOUSE_EVENT,
                PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_2,
                new Object[] {type, x, y, button, timestamp, deltaX, deltaY, modifiers, clickCount},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> imeSetComposition(String text, Integer selectionStart, Integer selectionEnd) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_imeSetComposition,
                CRT_IME_SET_COMPOSITION,
                PARAMS_IME_SET_COMPOSITION_1,
                new Object[] {text, selectionStart, selectionEnd},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> imeSetComposition(
            String text,
            Integer selectionStart,
            Integer selectionEnd,
            Integer replacementStart,
            Integer replacementEnd) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_imeSetComposition,
                CRT_IME_SET_COMPOSITION,
                PARAMS_IME_SET_COMPOSITION_2,
                new Object[] {text, selectionStart, selectionEnd, replacementStart, replacementEnd},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> insertText(String text) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_insertText,
                CRT_INSERT_TEXT,
                PARAMS_INSERT_TEXT_1,
                new Object[] {text},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setIgnoreInputEvents(Boolean ignore) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_setIgnoreInputEvents,
                CRT_SET_IGNORE_INPUT_EVENTS,
                PARAMS_SET_IGNORE_INPUT_EVENTS_1,
                new Object[] {ignore},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setInterceptDrags(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_setInterceptDrags,
                CRT_SET_INTERCEPT_DRAGS,
                PARAMS_SET_INTERCEPT_DRAGS_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> synthesizePinchGesture(Double x, Double y, Double scaleFactor) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_synthesizePinchGesture,
                CRT_SYNTHESIZE_PINCH_GESTURE,
                PARAMS_SYNTHESIZE_PINCH_GESTURE_1,
                new Object[] {x, y, scaleFactor},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> synthesizePinchGesture(
            Double x, Double y, Double scaleFactor, Integer relativeSpeed, GestureSourceType gestureSourceType) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_synthesizePinchGesture,
                CRT_SYNTHESIZE_PINCH_GESTURE,
                PARAMS_SYNTHESIZE_PINCH_GESTURE_2,
                new Object[] {x, y, scaleFactor, relativeSpeed, gestureSourceType},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> synthesizeScrollGesture(Double x, Double y) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_synthesizeScrollGesture,
                CRT_SYNTHESIZE_SCROLL_GESTURE,
                PARAMS_SYNTHESIZE_SCROLL_GESTURE_1,
                new Object[] {x, y},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> synthesizeScrollGesture(
            Double x,
            Double y,
            Double xDistance,
            Double yDistance,
            Double xOverscroll,
            Double yOverscroll,
            Boolean preventFling,
            Integer speed,
            GestureSourceType gestureSourceType,
            Integer repeatCount,
            Integer repeatDelayMs,
            String interactionMarkerName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_synthesizeScrollGesture,
                CRT_SYNTHESIZE_SCROLL_GESTURE,
                PARAMS_SYNTHESIZE_SCROLL_GESTURE_2,
                new Object[] {
                    x,
                    y,
                    xDistance,
                    yDistance,
                    xOverscroll,
                    yOverscroll,
                    preventFling,
                    speed,
                    gestureSourceType,
                    repeatCount,
                    repeatDelayMs,
                    interactionMarkerName
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> synthesizeTapGesture(Double x, Double y) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_synthesizeTapGesture,
                CRT_SYNTHESIZE_TAP_GESTURE,
                PARAMS_SYNTHESIZE_TAP_GESTURE_1,
                new Object[] {x, y},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> synthesizeTapGesture(
            Double x, Double y, Integer duration, Integer tapCount, GestureSourceType gestureSourceType) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Input_synthesizeTapGesture,
                CRT_SYNTHESIZE_TAP_GESTURE,
                PARAMS_SYNTHESIZE_TAP_GESTURE_2,
                new Object[] {x, y, duration, tapCount, gestureSourceType},
                false);
    }
}
