// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface DeviceOrientationAsync extends ParameterizedCommand<DeviceOrientationAsync> {
    /**
     * Clears the overridden Device Orientation.
     */
    CompletableFuture<Void> clearDeviceOrientationOverride();

    /**
     * Overrides the Device Orientation.
     *
     * @param alpha
     *            Mock alpha
     * @param beta
     *            Mock beta
     * @param gamma
     *            Mock gamma
     */
    CompletableFuture<Void> setDeviceOrientationOverride(Double alpha, Double beta, Double gamma);
}
