// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.constant.InstrumentationName;
import com.cdp4j.type.constant.PauseOnExceptionState;
import com.cdp4j.type.constant.RestartFrameMode;
import com.cdp4j.type.constant.TargetCallFrames;
import com.cdp4j.type.debugger.BreakLocation;
import com.cdp4j.type.debugger.DisassembleWasmModuleResult;
import com.cdp4j.type.debugger.EvaluateOnCallFrameResult;
import com.cdp4j.type.debugger.GetScriptSourceResult;
import com.cdp4j.type.debugger.Location;
import com.cdp4j.type.debugger.LocationRange;
import com.cdp4j.type.debugger.RestartFrameResult;
import com.cdp4j.type.debugger.ScriptPosition;
import com.cdp4j.type.debugger.SearchMatch;
import com.cdp4j.type.debugger.SetBreakpointByUrlResult;
import com.cdp4j.type.debugger.SetBreakpointResult;
import com.cdp4j.type.debugger.SetScriptSourceResult;
import com.cdp4j.type.debugger.WasmDisassemblyChunk;
import com.cdp4j.type.runtime.CallArgument;
import com.cdp4j.type.runtime.StackTrace;
import com.cdp4j.type.runtime.StackTraceId;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Debugger domain exposes JavaScript debugging capabilities. It allows setting
 * and removing breakpoints, stepping through execution, exploring stack traces,
 * etc.
 */
public interface DebuggerAsync extends ParameterizedCommand<DebuggerAsync> {
    /**
     * Continues execution until specific location is reached.
     *
     * @param location
     *            Location to continue to.
     */
    CompletableFuture<Void> continueToLocation(Location location);

    /**
     * Continues execution until specific location is reached.
     *
     * @param location
     *            Location to continue to.
     */
    CompletableFuture<Void> continueToLocation(Location location, @Optional TargetCallFrames targetCallFrames);

    /**
     * Disables debugger for given page.
     */
    CompletableFuture<Void> disable();

    /**
     *
     * @return DisassembleWasmModuleResult
     */
    @Experimental
    CompletableFuture<DisassembleWasmModuleResult> disassembleWasmModule(String scriptId);

    /**
     * Enables debugger for the given page. Clients should not assume that the
     * debugging has been enabled until the result for this command is received.
     *
     * @return Unique identifier of the debugger.
     */
    CompletableFuture<String> enable();

    /**
     * Enables debugger for the given page. Clients should not assume that the
     * debugging has been enabled until the result for this command is received.
     *
     * @param maxScriptsCacheSize
     *            The maximum size in bytes of collected scripts (not referenced by
     *            other heap objects) the debugger can hold. Puts no limit if
     *            parameter is omitted.
     *
     * @return Unique identifier of the debugger.
     */
    CompletableFuture<String> enable(@Experimental @Optional Double maxScriptsCacheSize);

    /**
     * Evaluates expression on a given call frame.
     *
     * @param callFrameId
     *            Call frame identifier to evaluate on.
     * @param expression
     *            Expression to evaluate.
     *
     * @return EvaluateOnCallFrameResult
     */
    CompletableFuture<EvaluateOnCallFrameResult> evaluateOnCallFrame(String callFrameId, String expression);

    /**
     * Evaluates expression on a given call frame.
     *
     * @param callFrameId
     *            Call frame identifier to evaluate on.
     * @param expression
     *            Expression to evaluate.
     * @param objectGroup
     *            String object group name to put result into (allows rapid
     *            releasing resulting object handles using releaseObjectGroup).
     * @param includeCommandLineAPI
     *            Specifies whether command line API should be available to the
     *            evaluated expression, defaults to false.
     * @param silent
     *            In silent mode exceptions thrown during evaluation are not
     *            reported and do not pause execution. Overrides setPauseOnException
     *            state.
     * @param returnByValue
     *            Whether the result is expected to be a JSON object that should be
     *            sent by value.
     * @param generatePreview
     *            Whether preview should be generated for the result.
     * @param throwOnSideEffect
     *            Whether to throw an exception if side effect cannot be ruled out
     *            during evaluation.
     * @param timeout
     *            Terminate execution after timing out (number of milliseconds).
     *
     * @return EvaluateOnCallFrameResult
     */
    CompletableFuture<EvaluateOnCallFrameResult> evaluateOnCallFrame(
            String callFrameId,
            String expression,
            @Optional String objectGroup,
            @Optional Boolean includeCommandLineAPI,
            @Optional Boolean silent,
            @Optional Boolean returnByValue,
            @Experimental @Optional Boolean generatePreview,
            @Optional Boolean throwOnSideEffect,
            @Experimental @Optional Double timeout);

    /**
     * Returns possible locations for breakpoint. scriptId in start and end range
     * locations should be the same.
     *
     * @param start
     *            Start of range to search possible breakpoint locations in.
     *
     * @return List of the possible breakpoint locations.
     */
    CompletableFuture<List<BreakLocation>> getPossibleBreakpoints(Location start);

    /**
     * Returns possible locations for breakpoint. scriptId in start and end range
     * locations should be the same.
     *
     * @param start
     *            Start of range to search possible breakpoint locations in.
     * @param end
     *            End of range to search possible breakpoint locations in
     *            (excluding). When not specified, end of scripts is used as end of
     *            range.
     * @param restrictToFunction
     *            Only consider locations which are in the same (non-nested)
     *            function as start.
     *
     * @return List of the possible breakpoint locations.
     */
    CompletableFuture<List<BreakLocation>> getPossibleBreakpoints(
            Location start, @Optional Location end, @Optional Boolean restrictToFunction);

    /**
     * Returns source for the script with given id.
     *
     * @param scriptId
     *            Id of the script to get source for.
     *
     * @return GetScriptSourceResult
     */
    CompletableFuture<GetScriptSourceResult> getScriptSource(String scriptId);

    /**
     * Returns stack trace with given stackTraceId.
     *
     */
    @Experimental
    CompletableFuture<StackTrace> getStackTrace(StackTraceId stackTraceId);

    /**
     * This command is deprecated. Use getScriptSource instead.
     *
     * @param scriptId
     *            Id of the Wasm script to get source for.
     *
     * @return Script source. (Encoded as a base64 string when passed over JSON)
     */
    @Deprecated
    CompletableFuture<String> getWasmBytecode(String scriptId);

    /**
     * Disassemble the next chunk of lines for the module corresponding to the
     * stream. If disassembly is complete, this API will invalidate the streamId and
     * return an empty chunk. Any subsequent calls for the now invalid stream will
     * return errors.
     *
     *
     * @return The next chunk of disassembly.
     */
    @Experimental
    CompletableFuture<WasmDisassemblyChunk> nextWasmDisassemblyChunk(String streamId);

    /**
     * Stops on the next JavaScript statement.
     */
    CompletableFuture<Void> pause();

    @Experimental
    @Deprecated
    CompletableFuture<Void> pauseOnAsyncCall(StackTraceId parentStackTraceId);

    /**
     * Removes JavaScript breakpoint.
     *
     */
    CompletableFuture<Void> removeBreakpoint(String breakpointId);

    /**
     * Restarts particular call frame from the beginning. The old, deprecated
     * behavior of restartFrame is to stay paused and allow further CDP commands
     * after a restart was scheduled. This can cause problems with restarting, so we
     * now continue execution immediatly after it has been scheduled until we reach
     * the beginning of the restarted frame.
     *
     * To stay back-wards compatible, restartFrame now expects a mode parameter to
     * be present. If the mode parameter is missing, restartFrame errors out.
     *
     * The various return values are deprecated and callFrames is always empty. Use
     * the call frames from the Debugger#paused events instead, that fires once V8
     * pauses at the beginning of the restarted function.
     *
     * @param callFrameId
     *            Call frame identifier to evaluate on.
     *
     * @return RestartFrameResult
     */
    CompletableFuture<RestartFrameResult> restartFrame(String callFrameId);

    /**
     * Restarts particular call frame from the beginning. The old, deprecated
     * behavior of restartFrame is to stay paused and allow further CDP commands
     * after a restart was scheduled. This can cause problems with restarting, so we
     * now continue execution immediatly after it has been scheduled until we reach
     * the beginning of the restarted frame.
     *
     * To stay back-wards compatible, restartFrame now expects a mode parameter to
     * be present. If the mode parameter is missing, restartFrame errors out.
     *
     * The various return values are deprecated and callFrames is always empty. Use
     * the call frames from the Debugger#paused events instead, that fires once V8
     * pauses at the beginning of the restarted function.
     *
     * @param callFrameId
     *            Call frame identifier to evaluate on.
     * @param mode
     *            The mode parameter must be present and set to 'StepInto',
     *            otherwise restartFrame will error out.
     *
     * @return RestartFrameResult
     */
    CompletableFuture<RestartFrameResult> restartFrame(
            String callFrameId, @Experimental @Optional RestartFrameMode mode);

    /**
     * Resumes JavaScript execution.
     */
    CompletableFuture<Void> resume();

    /**
     * Resumes JavaScript execution.
     *
     * @param terminateOnResume
     *            Set to true to terminate execution upon resuming execution. In
     *            contrast to Runtime.terminateExecution, this will allows to
     *            execute further JavaScript (i.e. via evaluation) until execution
     *            of the paused code is actually resumed, at which point termination
     *            is triggered. If execution is currently not paused, this parameter
     *            has no effect.
     */
    CompletableFuture<Void> resume(@Optional Boolean terminateOnResume);

    /**
     * Searches for given string in script content.
     *
     * @param scriptId
     *            Id of the script to search in.
     * @param query
     *            String to search for.
     *
     * @return List of search matches.
     */
    CompletableFuture<List<SearchMatch>> searchInContent(String scriptId, String query);

    /**
     * Searches for given string in script content.
     *
     * @param scriptId
     *            Id of the script to search in.
     * @param query
     *            String to search for.
     * @param caseSensitive
     *            If true, search is case sensitive.
     * @param isRegex
     *            If true, treats string parameter as regex.
     *
     * @return List of search matches.
     */
    CompletableFuture<List<SearchMatch>> searchInContent(
            String scriptId, String query, @Optional Boolean caseSensitive, @Optional Boolean isRegex);

    /**
     * Enables or disables async call stacks tracking.
     *
     * @param maxDepth
     *            Maximum depth of async call stacks. Setting to 0 will effectively
     *            disable collecting async call stacks (default).
     */
    CompletableFuture<Void> setAsyncCallStackDepth(Integer maxDepth);

    /**
     * Replace previous blackbox execution contexts with passed ones. Forces backend
     * to skip stepping/pausing in scripts in these execution contexts. VM will try
     * to leave blackboxed script by performing 'step in' several times, finally
     * resorting to 'step out' if unsuccessful.
     *
     * @param uniqueIds
     *            Array of execution context unique ids for the debugger to ignore.
     */
    @Experimental
    CompletableFuture<Void> setBlackboxExecutionContexts(List<String> uniqueIds);

    /**
     * Replace previous blackbox patterns with passed ones. Forces backend to skip
     * stepping/pausing in scripts with url matching one of the patterns. VM will
     * try to leave blackboxed script by performing 'step in' several times, finally
     * resorting to 'step out' if unsuccessful.
     *
     * @param patterns
     *            Array of regexps that will be used to check script url for
     *            blackbox state.
     */
    @Experimental
    CompletableFuture<Void> setBlackboxPatterns(List<String> patterns);

    /**
     * Replace previous blackbox patterns with passed ones. Forces backend to skip
     * stepping/pausing in scripts with url matching one of the patterns. VM will
     * try to leave blackboxed script by performing 'step in' several times, finally
     * resorting to 'step out' if unsuccessful.
     *
     * @param patterns
     *            Array of regexps that will be used to check script url for
     *            blackbox state.
     * @param skipAnonymous
     *            If true, also ignore scripts with no source url.
     */
    @Experimental
    CompletableFuture<Void> setBlackboxPatterns(List<String> patterns, @Optional Boolean skipAnonymous);

    /**
     * Makes backend skip steps in the script in blackboxed ranges. VM will try
     * leave blacklisted scripts by performing 'step in' several times, finally
     * resorting to 'step out' if unsuccessful. Positions array contains positions
     * where blackbox state is changed. First interval isn't blackboxed. Array
     * should be sorted.
     *
     * @param scriptId
     *            Id of the script.
     */
    @Experimental
    CompletableFuture<Void> setBlackboxedRanges(String scriptId, List<ScriptPosition> positions);

    /**
     * Sets JavaScript breakpoint at a given location.
     *
     * @param location
     *            Location to set breakpoint in.
     *
     * @return SetBreakpointResult
     */
    CompletableFuture<SetBreakpointResult> setBreakpoint(Location location);

    /**
     * Sets JavaScript breakpoint at a given location.
     *
     * @param location
     *            Location to set breakpoint in.
     * @param condition
     *            Expression to use as a breakpoint condition. When specified,
     *            debugger will only stop on the breakpoint if this expression
     *            evaluates to true.
     *
     * @return SetBreakpointResult
     */
    CompletableFuture<SetBreakpointResult> setBreakpoint(Location location, @Optional String condition);

    /**
     * Sets JavaScript breakpoint at given location specified either by URL or URL
     * regex. Once this command is issued, all existing parsed scripts will have
     * breakpoints resolved and returned in locations property. Further matching
     * script parsing will result in subsequent breakpointResolved events issued.
     * This logical breakpoint will survive page reloads.
     *
     * @param lineNumber
     *            Line number to set breakpoint at.
     *
     * @return SetBreakpointByUrlResult
     */
    CompletableFuture<SetBreakpointByUrlResult> setBreakpointByUrl(Integer lineNumber);

    /**
     * Sets JavaScript breakpoint at given location specified either by URL or URL
     * regex. Once this command is issued, all existing parsed scripts will have
     * breakpoints resolved and returned in locations property. Further matching
     * script parsing will result in subsequent breakpointResolved events issued.
     * This logical breakpoint will survive page reloads.
     *
     * @param lineNumber
     *            Line number to set breakpoint at.
     * @param url
     *            URL of the resources to set breakpoint on.
     * @param urlRegex
     *            Regex pattern for the URLs of the resources to set breakpoints on.
     *            Either url or urlRegex must be specified.
     * @param scriptHash
     *            Script hash of the resources to set breakpoint on.
     * @param columnNumber
     *            Offset in the line to set breakpoint at.
     * @param condition
     *            Expression to use as a breakpoint condition. When specified,
     *            debugger will only stop on the breakpoint if this expression
     *            evaluates to true.
     *
     * @return SetBreakpointByUrlResult
     */
    CompletableFuture<SetBreakpointByUrlResult> setBreakpointByUrl(
            Integer lineNumber,
            @Optional String url,
            @Optional String urlRegex,
            @Optional String scriptHash,
            @Optional Integer columnNumber,
            @Optional String condition);

    /**
     * Sets JavaScript breakpoint before each call to the given function. If another
     * function was created from the same source as a given one, calling it will
     * also trigger the breakpoint.
     *
     * @param objectId
     *            Function object id.
     *
     * @return Id of the created breakpoint for further reference.
     */
    @Experimental
    CompletableFuture<String> setBreakpointOnFunctionCall(String objectId);

    /**
     * Sets JavaScript breakpoint before each call to the given function. If another
     * function was created from the same source as a given one, calling it will
     * also trigger the breakpoint.
     *
     * @param objectId
     *            Function object id.
     * @param condition
     *            Expression to use as a breakpoint condition. When specified,
     *            debugger will stop on the breakpoint if this expression evaluates
     *            to true.
     *
     * @return Id of the created breakpoint for further reference.
     */
    @Experimental
    CompletableFuture<String> setBreakpointOnFunctionCall(String objectId, @Optional String condition);

    /**
     * Activates / deactivates all breakpoints on the page.
     *
     * @param active
     *            New value for breakpoints active state.
     */
    CompletableFuture<Void> setBreakpointsActive(Boolean active);

    /**
     * Sets instrumentation breakpoint.
     *
     * @param instrumentation
     *            Instrumentation name.
     *
     * @return Id of the created breakpoint for further reference.
     */
    CompletableFuture<String> setInstrumentationBreakpoint(InstrumentationName instrumentation);

    /**
     * Defines pause on exceptions state. Can be set to stop on all exceptions,
     * uncaught exceptions, or caught exceptions, no exceptions. Initial pause on
     * exceptions state is none.
     *
     * @param state
     *            Pause on exceptions mode.
     */
    CompletableFuture<Void> setPauseOnExceptions(PauseOnExceptionState state);

    /**
     * Changes return value in top frame. Available only at return break position.
     *
     * @param newValue
     *            New return value.
     */
    @Experimental
    CompletableFuture<Void> setReturnValue(CallArgument newValue);

    /**
     * Edits JavaScript source live.
     *
     * In general, functions that are currently on the stack can not be edited with
     * a single exception: If the edited function is the top-most stack frame and
     * that is the only activation of that function on the stack. In this case the
     * live edit will be successful and a Debugger.restartFrame for the top-most
     * function is automatically triggered.
     *
     * @param scriptId
     *            Id of the script to edit.
     * @param scriptSource
     *            New content of the script.
     *
     * @return SetScriptSourceResult
     */
    CompletableFuture<SetScriptSourceResult> setScriptSource(String scriptId, String scriptSource);

    /**
     * Edits JavaScript source live.
     *
     * In general, functions that are currently on the stack can not be edited with
     * a single exception: If the edited function is the top-most stack frame and
     * that is the only activation of that function on the stack. In this case the
     * live edit will be successful and a Debugger.restartFrame for the top-most
     * function is automatically triggered.
     *
     * @param scriptId
     *            Id of the script to edit.
     * @param scriptSource
     *            New content of the script.
     * @param dryRun
     *            If true the change will not actually be applied. Dry run may be
     *            used to get result description without actually modifying the
     *            code.
     * @param allowTopFrameEditing
     *            If true, then scriptSource is allowed to change the function on
     *            top of the stack as long as the top-most stack frame is the only
     *            activation of that function.
     *
     * @return SetScriptSourceResult
     */
    CompletableFuture<SetScriptSourceResult> setScriptSource(
            String scriptId,
            String scriptSource,
            @Optional Boolean dryRun,
            @Experimental @Optional Boolean allowTopFrameEditing);

    /**
     * Makes page not interrupt on any pauses (breakpoint, exception, dom exception
     * etc).
     *
     * @param skip
     *            New value for skip pauses state.
     */
    CompletableFuture<Void> setSkipAllPauses(Boolean skip);

    /**
     * Changes value of variable in a callframe. Object-based scopes are not
     * supported and must be mutated manually.
     *
     * @param scopeNumber
     *            0-based number of scope as was listed in scope chain. Only
     *            'local', 'closure' and 'catch' scope types are allowed. Other
     *            scopes could be manipulated manually.
     * @param variableName
     *            Variable name.
     * @param newValue
     *            New variable value.
     * @param callFrameId
     *            Id of callframe that holds variable.
     */
    CompletableFuture<Void> setVariableValue(
            Integer scopeNumber, String variableName, CallArgument newValue, String callFrameId);

    /**
     * Steps into the function call.
     */
    CompletableFuture<Void> stepInto();

    /**
     * Steps into the function call.
     *
     * @param breakOnAsyncCall
     *            Debugger will pause on the execution of the first async task which
     *            was scheduled before next pause.
     * @param skipList
     *            The skipList specifies location ranges that should be skipped on
     *            step into.
     */
    CompletableFuture<Void> stepInto(
            @Experimental @Optional Boolean breakOnAsyncCall, @Experimental @Optional List<LocationRange> skipList);

    /**
     * Steps out of the function call.
     */
    CompletableFuture<Void> stepOut();

    /**
     * Steps over the statement.
     */
    CompletableFuture<Void> stepOver();

    /**
     * Steps over the statement.
     *
     * @param skipList
     *            The skipList specifies location ranges that should be skipped on
     *            step over.
     */
    CompletableFuture<Void> stepOver(@Experimental @Optional List<LocationRange> skipList);
}
