// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.domdebugger.CSPViolationType;
import com.cdp4j.type.domdebugger.DOMBreakpointType;
import com.cdp4j.type.domdebugger.EventListener;
import java.util.List;

class DOMDebuggerImpl extends ParameterizedCommandImpl<DOMDebugger> implements DOMDebugger {

    private static final TypeReference<List<EventListener>> LIST_EVENTLISTENER =
            new TypeReference<List<EventListener>>() {};
    private static final CommandReturnType CRT_GET_EVENT_LISTENERS =
            new CommandReturnType("listeners", List.class, LIST_EVENTLISTENER);
    private static final CommandReturnType CRT_REMOVE_DO_MBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_EVENT_LISTENER_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_XH_RBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BREAK_ON_CS_PVIOLATION =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DO_MBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_EVENT_LISTENER_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_XH_RBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final String[] PARAMS_GET_EVENT_LISTENERS_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_EVENT_LISTENERS_2 = new String[] {"objectId", "depth", "pierce"};
    private static final String[] PARAMS_REMOVE_DO_MBREAKPOINT_1 = new String[] {"nodeId", "type"};
    private static final String[] PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_2 = new String[] {"eventName", "targetName"};
    private static final String[] PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_REMOVE_XH_RBREAKPOINT_1 = new String[] {"url"};
    private static final String[] PARAMS_SET_BREAK_ON_CS_PVIOLATION_1 = new String[] {"violationTypes"};
    private static final String[] PARAMS_SET_DO_MBREAKPOINT_1 = new String[] {"nodeId", "type"};
    private static final String[] PARAMS_SET_EVENT_LISTENER_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_SET_EVENT_LISTENER_BREAKPOINT_2 = new String[] {"eventName", "targetName"};
    private static final String[] PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_SET_XH_RBREAKPOINT_1 = new String[] {"url"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DOMDebuggerImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<EventListener> getEventListeners(String objectId) {
        return (List<EventListener>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_getEventListeners,
                CRT_GET_EVENT_LISTENERS,
                PARAMS_GET_EVENT_LISTENERS_1,
                new Object[] {objectId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<EventListener> getEventListeners(String objectId, Integer depth, Boolean pierce) {
        return (List<EventListener>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_getEventListeners,
                CRT_GET_EVENT_LISTENERS,
                PARAMS_GET_EVENT_LISTENERS_2,
                new Object[] {objectId, depth, pierce},
                true);
    }

    @Override
    public void removeDOMBreakpoint(Integer nodeId, DOMBreakpointType type) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeDOMBreakpoint,
                CRT_REMOVE_DO_MBREAKPOINT,
                PARAMS_REMOVE_DO_MBREAKPOINT_1,
                new Object[] {nodeId, type},
                true);
    }

    @Override
    public void removeEventListenerBreakpoint(String eventName) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeEventListenerBreakpoint,
                CRT_REMOVE_EVENT_LISTENER_BREAKPOINT,
                PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_1,
                new Object[] {eventName},
                true);
    }

    @Override
    public void removeEventListenerBreakpoint(String eventName, String targetName) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeEventListenerBreakpoint,
                CRT_REMOVE_EVENT_LISTENER_BREAKPOINT,
                PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_2,
                new Object[] {eventName, targetName},
                true);
    }

    @Override
    public void removeInstrumentationBreakpoint(String eventName) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeInstrumentationBreakpoint,
                CRT_REMOVE_INSTRUMENTATION_BREAKPOINT,
                PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                true);
    }

    @Override
    public void removeXHRBreakpoint(String url) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeXHRBreakpoint,
                CRT_REMOVE_XH_RBREAKPOINT,
                PARAMS_REMOVE_XH_RBREAKPOINT_1,
                new Object[] {url},
                true);
    }

    @Override
    public void setBreakOnCSPViolation(CSPViolationType violationTypes) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_setBreakOnCSPViolation,
                CRT_SET_BREAK_ON_CS_PVIOLATION,
                PARAMS_SET_BREAK_ON_CS_PVIOLATION_1,
                new Object[] {violationTypes},
                true);
    }

    @Override
    public void setDOMBreakpoint(Integer nodeId, DOMBreakpointType type) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_setDOMBreakpoint,
                CRT_SET_DO_MBREAKPOINT,
                PARAMS_SET_DO_MBREAKPOINT_1,
                new Object[] {nodeId, type},
                true);
    }

    @Override
    public void setEventListenerBreakpoint(String eventName) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_setEventListenerBreakpoint,
                CRT_SET_EVENT_LISTENER_BREAKPOINT,
                PARAMS_SET_EVENT_LISTENER_BREAKPOINT_1,
                new Object[] {eventName},
                true);
    }

    @Override
    public void setEventListenerBreakpoint(String eventName, String targetName) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_setEventListenerBreakpoint,
                CRT_SET_EVENT_LISTENER_BREAKPOINT,
                PARAMS_SET_EVENT_LISTENER_BREAKPOINT_2,
                new Object[] {eventName, targetName},
                true);
    }

    @Override
    public void setInstrumentationBreakpoint(String eventName) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_setInstrumentationBreakpoint,
                CRT_SET_INSTRUMENTATION_BREAKPOINT,
                PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                true);
    }

    @Override
    public void setXHRBreakpoint(String url) {
        handler.invoke(
                this,
                DomainCommand.DOMDebugger_setXHRBreakpoint,
                CRT_SET_XH_RBREAKPOINT,
                PARAMS_SET_XH_RBREAKPOINT_1,
                new Object[] {url},
                true);
    }
}
