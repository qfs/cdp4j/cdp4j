// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import java.util.concurrent.CompletableFuture;

class DeviceAccessAsyncImpl extends ParameterizedCommandImpl<DeviceAccessAsync> implements DeviceAccessAsync {

    private static final CommandReturnType CRT_CANCEL_PROMPT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SELECT_PROMPT = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CANCEL_PROMPT_1 = new String[] {"id"};
    private static final String[] PARAMS_SELECT_PROMPT_1 = new String[] {"id", "deviceId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DeviceAccessAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> cancelPrompt(String id) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DeviceAccess_cancelPrompt,
                CRT_CANCEL_PROMPT,
                PARAMS_CANCEL_PROMPT_1,
                new Object[] {id},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DeviceAccess_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DeviceAccess_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> selectPrompt(String id, String deviceId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DeviceAccess_selectPrompt,
                CRT_SELECT_PROMPT,
                PARAMS_SELECT_PROMPT_1,
                new Object[] {id, deviceId},
                false);
    }
}
