// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.webaudio.ContextRealtimeData;

/**
 * This domain allows inspection of Web Audio API.
 * https://webaudio.github.io/web-audio-api/
 */
@Experimental
public interface WebAudio extends ParameterizedCommand<WebAudio> {
    /**
     * Disables the WebAudio domain.
     */
    void disable();

    /**
     * Enables the WebAudio domain and starts sending context lifetime events.
     */
    void enable();

    /**
     * Fetch the realtime data from the registered contexts.
     *
     */
    ContextRealtimeData getRealtimeData(String contextId);
}
