// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.profiler.Profile;
import com.cdp4j.type.profiler.ScriptCoverage;
import com.cdp4j.type.profiler.TakePreciseCoverageResult;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ProfilerAsync extends ParameterizedCommand<ProfilerAsync> {
    CompletableFuture<Void> disable();

    CompletableFuture<Void> enable();

    /**
     * Collect coverage data for the current isolate. The coverage data may be
     * incomplete due to garbage collection.
     *
     * @return Coverage data for the current isolate.
     */
    CompletableFuture<List<ScriptCoverage>> getBestEffortCoverage();

    /**
     * Changes CPU profiler sampling interval. Must be called before CPU profiles
     * recording started.
     *
     * @param interval
     *            New sampling interval in microseconds.
     */
    CompletableFuture<Void> setSamplingInterval(Integer interval);

    CompletableFuture<Void> start();

    /**
     * Enable precise code coverage. Coverage data for JavaScript executed before
     * enabling precise code coverage may be incomplete. Enabling prevents running
     * optimized code and resets execution counters.
     *
     * @return Monotonically increasing time (in seconds) when the coverage update
     *         was taken in the backend.
     */
    CompletableFuture<Double> startPreciseCoverage();

    /**
     * Enable precise code coverage. Coverage data for JavaScript executed before
     * enabling precise code coverage may be incomplete. Enabling prevents running
     * optimized code and resets execution counters.
     *
     * @param callCount
     *            Collect accurate call counts beyond simple 'covered' or 'not
     *            covered'.
     * @param detailed
     *            Collect block-based coverage.
     * @param allowTriggeredUpdates
     *            Allow the backend to send updates on its own initiative
     *
     * @return Monotonically increasing time (in seconds) when the coverage update
     *         was taken in the backend.
     */
    CompletableFuture<Double> startPreciseCoverage(
            @Optional Boolean callCount, @Optional Boolean detailed, @Optional Boolean allowTriggeredUpdates);

    CompletableFuture<Profile> stop();

    /**
     * Disable precise code coverage. Disabling releases unnecessary execution count
     * records and allows executing optimized code.
     */
    CompletableFuture<Void> stopPreciseCoverage();

    /**
     * Collect coverage data for the current isolate, and resets execution counters.
     * Precise code coverage needs to have started.
     *
     * @return TakePreciseCoverageResult
     */
    CompletableFuture<TakePreciseCoverageResult> takePreciseCoverage();
}
