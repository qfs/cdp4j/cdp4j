// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.accessibility.AXNode;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class AccessibilityAsyncImpl extends ParameterizedCommandImpl<AccessibilityAsync> implements AccessibilityAsync {

    private static final TypeReference<List<AXNode>> LIST_AXNODE = new TypeReference<List<AXNode>>() {};
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_AX_NODE_AND_ANCESTORS =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_CHILD_AX_NODES =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_FULL_AX_TREE =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_PARTIAL_AX_TREE =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_ROOT_AX_NODE = new CommandReturnType("node", AXNode.class, null);
    private static final CommandReturnType CRT_QUERY_AX_TREE = new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_AX_NODE_AND_ANCESTORS_2 =
            new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_CHILD_AX_NODES_1 = new String[] {"id"};
    private static final String[] PARAMS_GET_CHILD_AX_NODES_2 = new String[] {"id", "frameId"};
    private static final String[] PARAMS_GET_FULL_AX_TREE_2 = new String[] {"depth", "frameId"};
    private static final String[] PARAMS_GET_PARTIAL_AX_TREE_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "fetchRelatives"};
    private static final String[] PARAMS_GET_ROOT_AX_NODE_2 = new String[] {"frameId"};
    private static final String[] PARAMS_QUERY_AX_TREE_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "accessibleName", "role"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public AccessibilityAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Accessibility_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Accessibility_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getAXNodeAndAncestors() {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_getAXNodeAndAncestors,
                CRT_GET_AX_NODE_AND_ANCESTORS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getAXNodeAndAncestors(
            Integer nodeId, Integer backendNodeId, String objectId) {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_getAXNodeAndAncestors,
                CRT_GET_AX_NODE_AND_ANCESTORS,
                PARAMS_GET_AX_NODE_AND_ANCESTORS_2,
                new Object[] {nodeId, backendNodeId, objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getChildAXNodes(String id) {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_getChildAXNodes,
                CRT_GET_CHILD_AX_NODES,
                PARAMS_GET_CHILD_AX_NODES_1,
                new Object[] {id},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getChildAXNodes(String id, String frameId) {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_getChildAXNodes,
                CRT_GET_CHILD_AX_NODES,
                PARAMS_GET_CHILD_AX_NODES_2,
                new Object[] {id, frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getFullAXTree() {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this, DomainCommand.Accessibility_getFullAXTree, CRT_GET_FULL_AX_TREE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getFullAXTree(Integer depth, String frameId) {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_getFullAXTree,
                CRT_GET_FULL_AX_TREE,
                PARAMS_GET_FULL_AX_TREE_2,
                new Object[] {depth, frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getPartialAXTree() {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_getPartialAXTree,
                CRT_GET_PARTIAL_AX_TREE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> getPartialAXTree(
            Integer nodeId, Integer backendNodeId, String objectId, Boolean fetchRelatives) {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_getPartialAXTree,
                CRT_GET_PARTIAL_AX_TREE,
                PARAMS_GET_PARTIAL_AX_TREE_2,
                new Object[] {nodeId, backendNodeId, objectId, fetchRelatives},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<AXNode> getRootAXNode() {
        return (CompletableFuture<AXNode>) handler.invoke(
                this, DomainCommand.Accessibility_getRootAXNode, CRT_GET_ROOT_AX_NODE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<AXNode> getRootAXNode(String frameId) {
        return (CompletableFuture<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getRootAXNode,
                CRT_GET_ROOT_AX_NODE,
                PARAMS_GET_ROOT_AX_NODE_2,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> queryAXTree() {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this, DomainCommand.Accessibility_queryAXTree, CRT_QUERY_AX_TREE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<AXNode>> queryAXTree(
            Integer nodeId, Integer backendNodeId, String objectId, String accessibleName, String role) {
        return (CompletableFuture<List<AXNode>>) handler.invoke(
                this,
                DomainCommand.Accessibility_queryAXTree,
                CRT_QUERY_AX_TREE,
                PARAMS_QUERY_AX_TREE_2,
                new Object[] {nodeId, backendNodeId, objectId, accessibleName, role},
                false);
    }
}
