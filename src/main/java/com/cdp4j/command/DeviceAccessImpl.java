// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;

class DeviceAccessImpl extends ParameterizedCommandImpl<DeviceAccess> implements DeviceAccess {

    private static final CommandReturnType CRT_CANCEL_PROMPT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SELECT_PROMPT = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CANCEL_PROMPT_1 = new String[] {"id"};
    private static final String[] PARAMS_SELECT_PROMPT_1 = new String[] {"id", "deviceId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DeviceAccessImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void cancelPrompt(String id) {
        handler.invoke(
                this,
                DomainCommand.DeviceAccess_cancelPrompt,
                CRT_CANCEL_PROMPT,
                PARAMS_CANCEL_PROMPT_1,
                new Object[] {id},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.DeviceAccess_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.DeviceAccess_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void selectPrompt(String id, String deviceId) {
        handler.invoke(
                this,
                DomainCommand.DeviceAccess_selectPrompt,
                CRT_SELECT_PROMPT,
                PARAMS_SELECT_PROMPT_1,
                new Object[] {id, deviceId},
                true);
    }
}
