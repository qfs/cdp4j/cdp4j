// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.audits.GenericIssueDetails;
import com.cdp4j.type.audits.GetEncodedResponseResult;
import com.cdp4j.type.constant.Encoding;
import java.util.List;

class AuditsImpl extends ParameterizedCommandImpl<Audits> implements Audits {

    private static final TypeReference<List<GenericIssueDetails>> LIST_GENERICISSUEDETAILS =
            new TypeReference<List<GenericIssueDetails>>() {};
    private static final CommandReturnType CRT_CHECK_CONTRAST = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CHECK_FORMS_ISSUES =
            new CommandReturnType("formIssues", List.class, LIST_GENERICISSUEDETAILS);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ENCODED_RESPONSE =
            new CommandReturnType(null, GetEncodedResponseResult.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CHECK_CONTRAST_2 = new String[] {"reportAAA"};
    private static final String[] PARAMS_GET_ENCODED_RESPONSE_1 = new String[] {"requestId", "encoding"};
    private static final String[] PARAMS_GET_ENCODED_RESPONSE_2 =
            new String[] {"requestId", "encoding", "quality", "sizeOnly"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public AuditsImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void checkContrast() {
        handler.invoke(this, DomainCommand.Audits_checkContrast, CRT_CHECK_CONTRAST, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void checkContrast(Boolean reportAAA) {
        handler.invoke(
                this,
                DomainCommand.Audits_checkContrast,
                CRT_CHECK_CONTRAST,
                PARAMS_CHECK_CONTRAST_2,
                new Object[] {reportAAA},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<GenericIssueDetails> checkFormsIssues() {
        return (List<GenericIssueDetails>) handler.invoke(
                this, DomainCommand.Audits_checkFormsIssues, CRT_CHECK_FORMS_ISSUES, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Audits_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Audits_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public GetEncodedResponseResult getEncodedResponse(String requestId, Encoding encoding) {
        return (GetEncodedResponseResult) handler.invoke(
                this,
                DomainCommand.Audits_getEncodedResponse,
                CRT_GET_ENCODED_RESPONSE,
                PARAMS_GET_ENCODED_RESPONSE_1,
                new Object[] {requestId, encoding},
                true);
    }

    @Override
    public GetEncodedResponseResult getEncodedResponse(
            String requestId, Encoding encoding, Double quality, Boolean sizeOnly) {
        return (GetEncodedResponseResult) handler.invoke(
                this,
                DomainCommand.Audits_getEncodedResponse,
                CRT_GET_ENCODED_RESPONSE,
                PARAMS_GET_ENCODED_RESPONSE_2,
                new Object[] {requestId, encoding, quality, sizeOnly},
                true);
    }
}
