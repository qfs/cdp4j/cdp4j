// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.accessibility.AXNode;
import java.util.List;

class AccessibilityImpl extends ParameterizedCommandImpl<Accessibility> implements Accessibility {

    private static final TypeReference<List<AXNode>> LIST_AXNODE = new TypeReference<List<AXNode>>() {};
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_AX_NODE_AND_ANCESTORS =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_CHILD_AX_NODES =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_FULL_AX_TREE =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_PARTIAL_AX_TREE =
            new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final CommandReturnType CRT_GET_ROOT_AX_NODE = new CommandReturnType("node", AXNode.class, null);
    private static final CommandReturnType CRT_QUERY_AX_TREE = new CommandReturnType("nodes", List.class, LIST_AXNODE);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_AX_NODE_AND_ANCESTORS_2 =
            new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_CHILD_AX_NODES_1 = new String[] {"id"};
    private static final String[] PARAMS_GET_CHILD_AX_NODES_2 = new String[] {"id", "frameId"};
    private static final String[] PARAMS_GET_FULL_AX_TREE_2 = new String[] {"depth", "frameId"};
    private static final String[] PARAMS_GET_PARTIAL_AX_TREE_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "fetchRelatives"};
    private static final String[] PARAMS_GET_ROOT_AX_NODE_2 = new String[] {"frameId"};
    private static final String[] PARAMS_QUERY_AX_TREE_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "accessibleName", "role"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public AccessibilityImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Accessibility_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Accessibility_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getAXNodeAndAncestors() {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getAXNodeAndAncestors,
                CRT_GET_AX_NODE_AND_ANCESTORS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getAXNodeAndAncestors(Integer nodeId, Integer backendNodeId, String objectId) {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getAXNodeAndAncestors,
                CRT_GET_AX_NODE_AND_ANCESTORS,
                PARAMS_GET_AX_NODE_AND_ANCESTORS_2,
                new Object[] {nodeId, backendNodeId, objectId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getChildAXNodes(String id) {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getChildAXNodes,
                CRT_GET_CHILD_AX_NODES,
                PARAMS_GET_CHILD_AX_NODES_1,
                new Object[] {id},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getChildAXNodes(String id, String frameId) {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getChildAXNodes,
                CRT_GET_CHILD_AX_NODES,
                PARAMS_GET_CHILD_AX_NODES_2,
                new Object[] {id, frameId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getFullAXTree() {
        return (List<AXNode>) handler.invoke(
                this, DomainCommand.Accessibility_getFullAXTree, CRT_GET_FULL_AX_TREE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getFullAXTree(Integer depth, String frameId) {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getFullAXTree,
                CRT_GET_FULL_AX_TREE,
                PARAMS_GET_FULL_AX_TREE_2,
                new Object[] {depth, frameId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getPartialAXTree() {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getPartialAXTree,
                CRT_GET_PARTIAL_AX_TREE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> getPartialAXTree(
            Integer nodeId, Integer backendNodeId, String objectId, Boolean fetchRelatives) {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_getPartialAXTree,
                CRT_GET_PARTIAL_AX_TREE,
                PARAMS_GET_PARTIAL_AX_TREE_2,
                new Object[] {nodeId, backendNodeId, objectId, fetchRelatives},
                true);
    }

    @Override
    public AXNode getRootAXNode() {
        return (AXNode) handler.invoke(
                this, DomainCommand.Accessibility_getRootAXNode, CRT_GET_ROOT_AX_NODE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public AXNode getRootAXNode(String frameId) {
        return (AXNode) handler.invoke(
                this,
                DomainCommand.Accessibility_getRootAXNode,
                CRT_GET_ROOT_AX_NODE,
                PARAMS_GET_ROOT_AX_NODE_2,
                new Object[] {frameId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> queryAXTree() {
        return (List<AXNode>) handler.invoke(
                this, DomainCommand.Accessibility_queryAXTree, CRT_QUERY_AX_TREE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<AXNode> queryAXTree(
            Integer nodeId, Integer backendNodeId, String objectId, String accessibleName, String role) {
        return (List<AXNode>) handler.invoke(
                this,
                DomainCommand.Accessibility_queryAXTree,
                CRT_QUERY_AX_TREE,
                PARAMS_QUERY_AX_TREE_2,
                new Object[] {nodeId, backendNodeId, objectId, accessibleName, role},
                true);
    }
}
