// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.cachestorage.Cache;
import com.cdp4j.type.cachestorage.CachedResponse;
import com.cdp4j.type.cachestorage.Header;
import com.cdp4j.type.cachestorage.RequestEntriesResult;
import com.cdp4j.type.storage.StorageBucket;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class CacheStorageAsyncImpl extends ParameterizedCommandImpl<CacheStorageAsync> implements CacheStorageAsync {

    private static final TypeReference<List<Cache>> LIST_CACHE = new TypeReference<List<Cache>>() {};
    private static final CommandReturnType CRT_DELETE_CACHE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_ENTRY = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REQUEST_CACHED_RESPONSE =
            new CommandReturnType("response", CachedResponse.class, null);
    private static final CommandReturnType CRT_REQUEST_CACHE_NAMES =
            new CommandReturnType("caches", List.class, LIST_CACHE);
    private static final CommandReturnType CRT_REQUEST_ENTRIES =
            new CommandReturnType(null, RequestEntriesResult.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_DELETE_CACHE_1 = new String[] {"cacheId"};
    private static final String[] PARAMS_DELETE_ENTRY_1 = new String[] {"cacheId", "request"};
    private static final String[] PARAMS_REQUEST_CACHED_RESPONSE_1 =
            new String[] {"cacheId", "requestURL", "requestHeaders"};
    private static final String[] PARAMS_REQUEST_CACHE_NAMES_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket"};
    private static final String[] PARAMS_REQUEST_ENTRIES_1 = new String[] {"cacheId"};
    private static final String[] PARAMS_REQUEST_ENTRIES_2 =
            new String[] {"cacheId", "skipCount", "pageSize", "pathFilter"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public CacheStorageAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteCache(String cacheId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CacheStorage_deleteCache,
                CRT_DELETE_CACHE,
                PARAMS_DELETE_CACHE_1,
                new Object[] {cacheId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteEntry(String cacheId, String request) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CacheStorage_deleteEntry,
                CRT_DELETE_ENTRY,
                PARAMS_DELETE_ENTRY_1,
                new Object[] {cacheId, request},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Cache>> requestCacheNames() {
        return (CompletableFuture<List<Cache>>) handler.invoke(
                this,
                DomainCommand.CacheStorage_requestCacheNames,
                CRT_REQUEST_CACHE_NAMES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Cache>> requestCacheNames(
            String securityOrigin, String storageKey, StorageBucket storageBucket) {
        return (CompletableFuture<List<Cache>>) handler.invoke(
                this,
                DomainCommand.CacheStorage_requestCacheNames,
                CRT_REQUEST_CACHE_NAMES,
                PARAMS_REQUEST_CACHE_NAMES_2,
                new Object[] {securityOrigin, storageKey, storageBucket},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CachedResponse> requestCachedResponse(
            String cacheId, String requestURL, List<Header> requestHeaders) {
        return (CompletableFuture<CachedResponse>) handler.invoke(
                this,
                DomainCommand.CacheStorage_requestCachedResponse,
                CRT_REQUEST_CACHED_RESPONSE,
                PARAMS_REQUEST_CACHED_RESPONSE_1,
                new Object[] {cacheId, requestURL, requestHeaders},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RequestEntriesResult> requestEntries(String cacheId) {
        return (CompletableFuture<RequestEntriesResult>) handler.invoke(
                this,
                DomainCommand.CacheStorage_requestEntries,
                CRT_REQUEST_ENTRIES,
                PARAMS_REQUEST_ENTRIES_1,
                new Object[] {cacheId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RequestEntriesResult> requestEntries(
            String cacheId, Integer skipCount, Integer pageSize, String pathFilter) {
        return (CompletableFuture<RequestEntriesResult>) handler.invoke(
                this,
                DomainCommand.CacheStorage_requestEntries,
                CRT_REQUEST_ENTRIES,
                PARAMS_REQUEST_ENTRIES_2,
                new Object[] {cacheId, skipCount, pageSize, pathFilter},
                false);
    }
}
