// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.fedcm.AccountUrlType;
import com.cdp4j.type.fedcm.DialogButton;
import java.util.concurrent.CompletableFuture;

/**
 * This domain allows interacting with the FedCM dialog.
 */
@Experimental
public interface FedCmAsync extends ParameterizedCommand<FedCmAsync> {
    CompletableFuture<Void> clickDialogButton(String dialogId, DialogButton dialogButton);

    CompletableFuture<Void> disable();

    CompletableFuture<Void> dismissDialog(String dialogId);

    CompletableFuture<Void> dismissDialog(String dialogId, @Optional Boolean triggerCooldown);

    CompletableFuture<Void> enable();

    CompletableFuture<Void> enable(@Optional Boolean disableRejectionDelay);

    CompletableFuture<Void> openUrl(String dialogId, Integer accountIndex, AccountUrlType accountUrlType);

    /**
     * Resets the cooldown time, if any, to allow the next FedCM call to show a
     * dialog even if one was recently dismissed by the user.
     */
    CompletableFuture<Void> resetCooldown();

    CompletableFuture<Void> selectAccount(String dialogId, Integer accountIndex);
}
