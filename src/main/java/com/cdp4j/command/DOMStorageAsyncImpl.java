// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.domstorage.StorageId;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class DOMStorageAsyncImpl extends ParameterizedCommandImpl<DOMStorageAsync> implements DOMStorageAsync {

    private static final TypeReference<List<List<String>>> LIST_LIST_STRING =
            new TypeReference<List<List<String>>>() {};
    private static final CommandReturnType CRT_CLEAR = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_DO_MSTORAGE_ITEMS =
            new CommandReturnType("entries", List.class, LIST_LIST_STRING);
    private static final CommandReturnType CRT_REMOVE_DO_MSTORAGE_ITEM = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DO_MSTORAGE_ITEM = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CLEAR_1 = new String[] {"storageId"};
    private static final String[] PARAMS_GET_DO_MSTORAGE_ITEMS_1 = new String[] {"storageId"};
    private static final String[] PARAMS_REMOVE_DO_MSTORAGE_ITEM_1 = new String[] {"storageId", "key"};
    private static final String[] PARAMS_SET_DO_MSTORAGE_ITEM_1 = new String[] {"storageId", "key", "value"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DOMStorageAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clear(StorageId storageId) {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.DOMStorage_clear, CRT_CLEAR, PARAMS_CLEAR_1, new Object[] {storageId}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOMStorage_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOMStorage_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<List<String>>> getDOMStorageItems(StorageId storageId) {
        return (CompletableFuture<List<List<String>>>) handler.invoke(
                this,
                DomainCommand.DOMStorage_getDOMStorageItems,
                CRT_GET_DO_MSTORAGE_ITEMS,
                PARAMS_GET_DO_MSTORAGE_ITEMS_1,
                new Object[] {storageId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeDOMStorageItem(StorageId storageId, String key) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMStorage_removeDOMStorageItem,
                CRT_REMOVE_DO_MSTORAGE_ITEM,
                PARAMS_REMOVE_DO_MSTORAGE_ITEM_1,
                new Object[] {storageId, key},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDOMStorageItem(StorageId storageId, String key, String value) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMStorage_setDOMStorageItem,
                CRT_SET_DO_MSTORAGE_ITEM,
                PARAMS_SET_DO_MSTORAGE_ITEM_1,
                new Object[] {storageId, key, value},
                false);
    }
}
