// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class PerformanceTimelineAsyncImpl extends ParameterizedCommandImpl<PerformanceTimelineAsync>
        implements PerformanceTimelineAsync {

    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final String[] PARAMS_ENABLE_1 = new String[] {"eventTypes"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public PerformanceTimelineAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable(List<String> eventTypes) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.PerformanceTimeline_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_1,
                new Object[] {eventTypes},
                false);
    }
}
