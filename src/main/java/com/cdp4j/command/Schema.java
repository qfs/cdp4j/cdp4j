// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.schema.Domain;
import java.util.List;

/**
 * This domain is deprecated.
 */
public interface Schema extends ParameterizedCommand<Schema> {
    /**
     * Returns supported domains.
     *
     * @return List of supported domains.
     */
    List<Domain> getDomains();
}
