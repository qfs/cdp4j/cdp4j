// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

@Experimental
public interface DeviceAccess extends ParameterizedCommand<DeviceAccess> {
    /**
     * Cancel a prompt in response to a DeviceAccess.deviceRequestPrompted event.
     *
     */
    void cancelPrompt(String id);

    /**
     * Disable events in this domain.
     */
    void disable();

    /**
     * Enable events in this domain.
     */
    void enable();

    /**
     * Select a device in response to a DeviceAccess.deviceRequestPrompted event.
     *
     */
    void selectPrompt(String id, String deviceId);
}
