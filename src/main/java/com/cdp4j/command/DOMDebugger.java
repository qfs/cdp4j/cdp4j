// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.domdebugger.CSPViolationType;
import com.cdp4j.type.domdebugger.DOMBreakpointType;
import com.cdp4j.type.domdebugger.EventListener;
import java.util.List;

/**
 * DOM debugging allows setting breakpoints on particular DOM operations and events. JavaScript
 * execution will stop on these operations as if there was a regular breakpoint set.
 */
public interface DOMDebugger extends ParameterizedCommand<DOMDebugger> {
    /**
     * Returns event listeners of the given object.
     *
     * @param objectId Identifier of the object to return listeners for.
     *
     * @return Array of relevant listeners.
     */
    List<EventListener> getEventListeners(String objectId);

    /**
     * Returns event listeners of the given object.
     *
     * @param objectId Identifier of the object to return listeners for.
     * @param depth The maximum depth at which Node children should be retrieved, defaults to 1. Use -1 for the
     * entire subtree or provide an integer larger than 0.
     * @param pierce Whether or not iframes and shadow roots should be traversed when returning the subtree
     * (default is false). Reports listeners for all contexts if pierce is enabled.
     *
     * @return Array of relevant listeners.
     */
    List<EventListener> getEventListeners(String objectId, @Optional Integer depth, @Optional Boolean pierce);

    /**
     * Removes DOM breakpoint that was set using setDOMBreakpoint.
     *
     * @param nodeId Identifier of the node to remove breakpoint from.
     * @param type Type of the breakpoint to remove.
     */
    void removeDOMBreakpoint(Integer nodeId, DOMBreakpointType type);

    /**
     * Removes breakpoint on particular DOM event.
     *
     * @param eventName Event name.
     */
    void removeEventListenerBreakpoint(String eventName);

    /**
     * Removes breakpoint on particular DOM event.
     *
     * @param eventName Event name.
     * @param targetName EventTarget interface name.
     */
    void removeEventListenerBreakpoint(String eventName, @Experimental @Optional String targetName);

    /**
     * Removes breakpoint on particular native event.
     *
     * @param eventName Instrumentation name to stop on.
     */
    @Experimental
    @Deprecated
    void removeInstrumentationBreakpoint(String eventName);

    /**
     * Removes breakpoint from XMLHttpRequest.
     *
     * @param url Resource URL substring.
     */
    void removeXHRBreakpoint(String url);

    /**
     * Sets breakpoint on particular CSP violations.
     *
     * @param violationTypes CSP Violations to stop upon.
     */
    @Experimental
    void setBreakOnCSPViolation(CSPViolationType violationTypes);

    /**
     * Sets breakpoint on particular operation with DOM.
     *
     * @param nodeId Identifier of the node to set breakpoint on.
     * @param type Type of the operation to stop upon.
     */
    void setDOMBreakpoint(Integer nodeId, DOMBreakpointType type);

    /**
     * Sets breakpoint on particular DOM event.
     *
     * @param eventName DOM Event name to stop on (any DOM event will do).
     */
    void setEventListenerBreakpoint(String eventName);

    /**
     * Sets breakpoint on particular DOM event.
     *
     * @param eventName DOM Event name to stop on (any DOM event will do).
     * @param targetName EventTarget interface name to stop on. If equal to "*" or not provided, will stop on any
     * EventTarget.
     */
    void setEventListenerBreakpoint(String eventName, @Experimental @Optional String targetName);

    /**
     * Sets breakpoint on particular native event.
     *
     * @param eventName Instrumentation name to stop on.
     */
    @Experimental
    @Deprecated
    void setInstrumentationBreakpoint(String eventName);

    /**
     * Sets breakpoint on XMLHttpRequest.
     *
     * @param url Resource URL substring. All XHRs having this substring in the URL will get stopped upon.
     */
    void setXHRBreakpoint(String url);
}
