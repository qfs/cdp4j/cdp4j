// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

@Experimental
public interface DeviceOrientation extends ParameterizedCommand<DeviceOrientation> {
    /**
     * Clears the overridden Device Orientation.
     */
    void clearDeviceOrientationOverride();

    /**
     * Overrides the Device Orientation.
     *
     * @param alpha Mock alpha
     * @param beta Mock beta
     * @param gamma Mock gamma
     */
    void setDeviceOrientationOverride(Double alpha, Double beta, Double gamma);
}
