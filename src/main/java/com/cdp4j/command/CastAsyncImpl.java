// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import java.util.concurrent.CompletableFuture;

class CastAsyncImpl extends ParameterizedCommandImpl<CastAsync> implements CastAsync {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SINK_TO_USE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_DESKTOP_MIRRORING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_TAB_MIRRORING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_CASTING = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"presentationUrl"};
    private static final String[] PARAMS_SET_SINK_TO_USE_1 = new String[] {"sinkName"};
    private static final String[] PARAMS_START_DESKTOP_MIRRORING_1 = new String[] {"sinkName"};
    private static final String[] PARAMS_START_TAB_MIRRORING_1 = new String[] {"sinkName"};
    private static final String[] PARAMS_STOP_CASTING_1 = new String[] {"sinkName"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public CastAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Cast_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Cast_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable(String presentationUrl) {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Cast_enable, CRT_ENABLE, PARAMS_ENABLE_2, new Object[] {presentationUrl}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSinkToUse(String sinkName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Cast_setSinkToUse,
                CRT_SET_SINK_TO_USE,
                PARAMS_SET_SINK_TO_USE_1,
                new Object[] {sinkName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startDesktopMirroring(String sinkName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Cast_startDesktopMirroring,
                CRT_START_DESKTOP_MIRRORING,
                PARAMS_START_DESKTOP_MIRRORING_1,
                new Object[] {sinkName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startTabMirroring(String sinkName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Cast_startTabMirroring,
                CRT_START_TAB_MIRRORING,
                PARAMS_START_TAB_MIRRORING_1,
                new Object[] {sinkName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopCasting(String sinkName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Cast_stopCasting,
                CRT_STOP_CASTING,
                PARAMS_STOP_CASTING_1,
                new Object[] {sinkName},
                false);
    }
}
