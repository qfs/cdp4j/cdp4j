// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.constant.EmulatedVisionDeficiency;
import com.cdp4j.type.constant.Platform;
import com.cdp4j.type.dom.RGBA;
import com.cdp4j.type.emulation.DevicePosture;
import com.cdp4j.type.emulation.DisabledImageType;
import com.cdp4j.type.emulation.DisplayFeature;
import com.cdp4j.type.emulation.MediaFeature;
import com.cdp4j.type.emulation.PressureMetadata;
import com.cdp4j.type.emulation.PressureSource;
import com.cdp4j.type.emulation.PressureState;
import com.cdp4j.type.emulation.ScreenOrientation;
import com.cdp4j.type.emulation.SensorMetadata;
import com.cdp4j.type.emulation.SensorReading;
import com.cdp4j.type.emulation.SensorType;
import com.cdp4j.type.emulation.UserAgentMetadata;
import com.cdp4j.type.emulation.VirtualTimePolicy;
import com.cdp4j.type.page.Viewport;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This domain emulates different environments for the page.
 */
public interface EmulationAsync extends ParameterizedCommand<EmulationAsync> {
    /**
     * Tells whether emulation is supported.
     *
     * @return True if emulation is supported.
     */
    @Deprecated
    CompletableFuture<Boolean> canEmulate();

    /**
     * Clears the overridden device metrics.
     */
    CompletableFuture<Void> clearDeviceMetricsOverride();

    /**
     * Clears a device posture override set with either setDeviceMetricsOverride()
     * or setDevicePostureOverride() and starts using posture information from the
     * platform again. Does nothing if no override is set.
     */
    @Experimental
    CompletableFuture<Void> clearDevicePostureOverride();

    /**
     * Clears the overridden Geolocation Position and Error.
     */
    CompletableFuture<Void> clearGeolocationOverride();

    /**
     * Clears Idle state overrides.
     */
    CompletableFuture<Void> clearIdleOverride();

    @Experimental
    CompletableFuture<Double> getOverriddenSensorInformation(SensorType type);

    /**
     * Requests that page scale factor is reset to initial values.
     */
    @Experimental
    CompletableFuture<Void> resetPageScaleFactor();

    /**
     * Automatically render all web contents using a dark theme.
     */
    @Experimental
    CompletableFuture<Void> setAutoDarkModeOverride();

    /**
     * Automatically render all web contents using a dark theme.
     *
     * @param enabled
     *            Whether to enable or disable automatic dark mode. If not
     *            specified, any existing override will be cleared.
     */
    @Experimental
    CompletableFuture<Void> setAutoDarkModeOverride(@Optional Boolean enabled);

    /**
     * Allows overriding the automation flag.
     *
     * @param enabled
     *            Whether the override should be enabled.
     */
    @Experimental
    CompletableFuture<Void> setAutomationOverride(Boolean enabled);

    /**
     * Enables CPU throttling to emulate slow CPUs.
     *
     * @param rate
     *            Throttling rate as a slowdown factor (1 is no throttle, 2 is 2x
     *            slowdown, etc).
     */
    CompletableFuture<Void> setCPUThrottlingRate(Double rate);

    /**
     * Sets or clears an override of the default background color of the frame. This
     * override is used if the content does not specify one.
     */
    CompletableFuture<Void> setDefaultBackgroundColorOverride();

    /**
     * Sets or clears an override of the default background color of the frame. This
     * override is used if the content does not specify one.
     *
     * @param color
     *            RGBA of the default background color. If not specified, any
     *            existing override will be cleared.
     */
    CompletableFuture<Void> setDefaultBackgroundColorOverride(@Optional RGBA color);

    /**
     * Overrides the values of device screen dimensions (window.screen.width,
     * window.screen.height, window.innerWidth, window.innerHeight, and
     * "device-width"/"device-height"-related CSS media query results).
     *
     * @param width
     *            Overriding width value in pixels (minimum 0, maximum 10000000). 0
     *            disables the override.
     * @param height
     *            Overriding height value in pixels (minimum 0, maximum 10000000). 0
     *            disables the override.
     * @param deviceScaleFactor
     *            Overriding device scale factor value. 0 disables the override.
     * @param mobile
     *            Whether to emulate mobile device. This includes viewport meta tag,
     *            overlay scrollbars, text autosizing and more.
     */
    CompletableFuture<Void> setDeviceMetricsOverride(
            Integer width, Integer height, Double deviceScaleFactor, Boolean mobile);

    /**
     * Overrides the values of device screen dimensions (window.screen.width,
     * window.screen.height, window.innerWidth, window.innerHeight, and
     * "device-width"/"device-height"-related CSS media query results).
     *
     * @param width
     *            Overriding width value in pixels (minimum 0, maximum 10000000). 0
     *            disables the override.
     * @param height
     *            Overriding height value in pixels (minimum 0, maximum 10000000). 0
     *            disables the override.
     * @param deviceScaleFactor
     *            Overriding device scale factor value. 0 disables the override.
     * @param mobile
     *            Whether to emulate mobile device. This includes viewport meta tag,
     *            overlay scrollbars, text autosizing and more.
     * @param scale
     *            Scale to apply to resulting view image.
     * @param screenWidth
     *            Overriding screen width value in pixels (minimum 0, maximum
     *            10000000).
     * @param screenHeight
     *            Overriding screen height value in pixels (minimum 0, maximum
     *            10000000).
     * @param positionX
     *            Overriding view X position on screen in pixels (minimum 0, maximum
     *            10000000).
     * @param positionY
     *            Overriding view Y position on screen in pixels (minimum 0, maximum
     *            10000000).
     * @param dontSetVisibleSize
     *            Do not set visible view size, rely upon explicit setVisibleSize
     *            call.
     * @param screenOrientation
     *            Screen orientation override.
     * @param viewport
     *            If set, the visible area of the page will be overridden to this
     *            viewport. This viewport change is not observed by the page, e.g.
     *            viewport-relative elements do not change positions.
     * @param displayFeature
     *            If set, the display feature of a multi-segment screen. If not set,
     *            multi-segment support is turned-off.
     * @param devicePosture
     *            If set, the posture of a foldable device. If not set the posture
     *            is set to continuous. Deprecated, use
     *            Emulation.setDevicePostureOverride.
     */
    CompletableFuture<Void> setDeviceMetricsOverride(
            Integer width,
            Integer height,
            Double deviceScaleFactor,
            Boolean mobile,
            @Experimental @Optional Double scale,
            @Experimental @Optional Integer screenWidth,
            @Experimental @Optional Integer screenHeight,
            @Experimental @Optional Integer positionX,
            @Experimental @Optional Integer positionY,
            @Experimental @Optional Boolean dontSetVisibleSize,
            @Optional ScreenOrientation screenOrientation,
            @Experimental @Optional Viewport viewport,
            @Experimental @Optional DisplayFeature displayFeature,
            @Experimental @Optional @Deprecated DevicePosture devicePosture);

    /**
     * Start reporting the given posture value to the Device Posture API. This
     * override can also be set in setDeviceMetricsOverride().
     *
     */
    @Experimental
    CompletableFuture<Void> setDevicePostureOverride(DevicePosture posture);

    @Experimental
    CompletableFuture<Void> setDisabledImageTypes(DisabledImageType imageTypes);

    @Experimental
    CompletableFuture<Void> setDocumentCookieDisabled(Boolean disabled);

    @Experimental
    CompletableFuture<Void> setEmitTouchEventsForMouse(Boolean enabled);

    @Experimental
    CompletableFuture<Void> setEmitTouchEventsForMouse(Boolean enabled, @Optional Platform configuration);

    /**
     * Emulates the given media type or media feature for CSS media queries.
     */
    CompletableFuture<Void> setEmulatedMedia();

    /**
     * Emulates the given media type or media feature for CSS media queries.
     *
     * @param media
     *            Media type to emulate. Empty string disables the override.
     * @param features
     *            Media features to emulate.
     */
    CompletableFuture<Void> setEmulatedMedia(@Optional String media, @Optional List<MediaFeature> features);

    /**
     * Emulates the given vision deficiency.
     *
     * @param type
     *            Vision deficiency to emulate. Order: best-effort emulations come
     *            first, followed by any physiologically accurate emulations for
     *            medically recognized color vision deficiencies.
     */
    CompletableFuture<Void> setEmulatedVisionDeficiency(EmulatedVisionDeficiency type);

    /**
     * Enables or disables simulating a focused and active page.
     *
     * @param enabled
     *            Whether to enable to disable focus emulation.
     */
    @Experimental
    CompletableFuture<Void> setFocusEmulationEnabled(Boolean enabled);

    /**
     * Overrides the Geolocation Position or Error. Omitting any of the parameters
     * emulates position unavailable.
     */
    CompletableFuture<Void> setGeolocationOverride();

    /**
     * Overrides the Geolocation Position or Error. Omitting any of the parameters
     * emulates position unavailable.
     *
     * @param latitude
     *            Mock latitude
     * @param longitude
     *            Mock longitude
     * @param accuracy
     *            Mock accuracy
     */
    CompletableFuture<Void> setGeolocationOverride(
            @Optional Double latitude, @Optional Double longitude, @Optional Double accuracy);

    @Experimental
    CompletableFuture<Void> setHardwareConcurrencyOverride(Integer hardwareConcurrency);

    /**
     * Overrides the Idle state.
     *
     * @param isUserActive
     *            Mock isUserActive
     * @param isScreenUnlocked
     *            Mock isScreenUnlocked
     */
    CompletableFuture<Void> setIdleOverride(Boolean isUserActive, Boolean isScreenUnlocked);

    /**
     * Overrides default host system locale with the specified one.
     */
    @Experimental
    CompletableFuture<Void> setLocaleOverride();

    /**
     * Overrides default host system locale with the specified one.
     *
     * @param locale
     *            ICU style C locale (e.g. "en_US"). If not specified or empty,
     *            disables the override and restores default host system locale.
     */
    @Experimental
    CompletableFuture<Void> setLocaleOverride(@Optional String locale);

    /**
     * Overrides value returned by the javascript navigator object.
     *
     * @param platform
     *            The platform navigator.platform should return.
     */
    @Experimental
    @Deprecated
    CompletableFuture<Void> setNavigatorOverrides(String platform);

    /**
     * Sets a specified page scale factor.
     *
     * @param pageScaleFactor
     *            Page scale factor.
     */
    @Experimental
    CompletableFuture<Void> setPageScaleFactor(Double pageScaleFactor);

    /**
     * Overrides a pressure source of a given type, as used by the Compute Pressure
     * API, so that updates to PressureObserver.observe() are provided via
     * setPressureStateOverride instead of being retrieved from platform-provided
     * telemetry data.
     *
     */
    @Experimental
    CompletableFuture<Void> setPressureSourceOverrideEnabled(Boolean enabled, PressureSource source);

    /**
     * Overrides a pressure source of a given type, as used by the Compute Pressure
     * API, so that updates to PressureObserver.observe() are provided via
     * setPressureStateOverride instead of being retrieved from platform-provided
     * telemetry data.
     *
     */
    @Experimental
    CompletableFuture<Void> setPressureSourceOverrideEnabled(
            Boolean enabled, PressureSource source, @Optional PressureMetadata metadata);

    /**
     * Provides a given pressure state that will be processed and eventually be
     * delivered to PressureObserver users. |source| must have been previously
     * overridden by setPressureSourceOverrideEnabled.
     *
     */
    @Experimental
    CompletableFuture<Void> setPressureStateOverride(PressureSource source, PressureState state);

    /**
     * Switches script execution in the page.
     *
     * @param value
     *            Whether script execution should be disabled in the page.
     */
    CompletableFuture<Void> setScriptExecutionDisabled(Boolean value);

    @Experimental
    CompletableFuture<Void> setScrollbarsHidden(Boolean hidden);

    /**
     * Overrides a platform sensor of a given type. If |enabled| is true, calls to
     * Sensor.start() will use a virtual sensor as backend rather than fetching data
     * from a real hardware sensor. Otherwise, existing virtual sensor-backend
     * Sensor objects will fire an error event and new calls to Sensor.start() will
     * attempt to use a real sensor instead.
     *
     */
    @Experimental
    CompletableFuture<Void> setSensorOverrideEnabled(Boolean enabled, SensorType type);

    /**
     * Overrides a platform sensor of a given type. If |enabled| is true, calls to
     * Sensor.start() will use a virtual sensor as backend rather than fetching data
     * from a real hardware sensor. Otherwise, existing virtual sensor-backend
     * Sensor objects will fire an error event and new calls to Sensor.start() will
     * attempt to use a real sensor instead.
     *
     */
    @Experimental
    CompletableFuture<Void> setSensorOverrideEnabled(
            Boolean enabled, SensorType type, @Optional SensorMetadata metadata);

    /**
     * Updates the sensor readings reported by a sensor type previously overridden
     * by setSensorOverrideEnabled.
     *
     */
    @Experimental
    CompletableFuture<Void> setSensorOverrideReadings(SensorType type, SensorReading reading);

    /**
     * Overrides default host system timezone with the specified one.
     *
     * @param timezoneId
     *            The timezone identifier. List of supported timezones:
     *            https://source.chromium.org/chromium/chromium/deps/icu.git/+/faee8bc70570192d82d2978a71e2a615788597d1:source/data/misc/metaZones.txt
     *            If empty, disables the override and restores default host system
     *            timezone.
     */
    CompletableFuture<Void> setTimezoneOverride(String timezoneId);

    /**
     * Enables touch on platforms which do not support them.
     *
     * @param enabled
     *            Whether the touch event emulation should be enabled.
     */
    CompletableFuture<Void> setTouchEmulationEnabled(Boolean enabled);

    /**
     * Enables touch on platforms which do not support them.
     *
     * @param enabled
     *            Whether the touch event emulation should be enabled.
     * @param maxTouchPoints
     *            Maximum touch points supported. Defaults to one.
     */
    CompletableFuture<Void> setTouchEmulationEnabled(Boolean enabled, @Optional Integer maxTouchPoints);

    /**
     * Allows overriding user agent with the given string. userAgentMetadata must be
     * set for Client Hint headers to be sent.
     *
     * @param userAgent
     *            User agent to use.
     */
    CompletableFuture<Void> setUserAgentOverride(String userAgent);

    /**
     * Allows overriding user agent with the given string. userAgentMetadata must be
     * set for Client Hint headers to be sent.
     *
     * @param userAgent
     *            User agent to use.
     * @param acceptLanguage
     *            Browser language to emulate.
     * @param platform
     *            The platform navigator.platform should return.
     * @param userAgentMetadata
     *            To be sent in Sec-CH-UA-* headers and returned in
     *            navigator.userAgentData
     */
    CompletableFuture<Void> setUserAgentOverride(
            String userAgent,
            @Optional String acceptLanguage,
            @Optional String platform,
            @Experimental @Optional UserAgentMetadata userAgentMetadata);

    /**
     * Turns on virtual time for all frames (replacing real-time with a synthetic
     * time source) and sets the current virtual time policy. Note this supersedes
     * any previous time budget.
     *
     *
     * @return Absolute timestamp at which virtual time was first enabled (up time
     *         in milliseconds).
     */
    @Experimental
    CompletableFuture<Double> setVirtualTimePolicy(VirtualTimePolicy policy);

    /**
     * Turns on virtual time for all frames (replacing real-time with a synthetic
     * time source) and sets the current virtual time policy. Note this supersedes
     * any previous time budget.
     *
     * @param budget
     *            If set, after this many virtual milliseconds have elapsed virtual
     *            time will be paused and a virtualTimeBudgetExpired event is sent.
     * @param maxVirtualTimeTaskStarvationCount
     *            If set this specifies the maximum number of tasks that can be run
     *            before virtual is forced forwards to prevent deadlock.
     * @param initialVirtualTime
     *            If set, base::Time::Now will be overridden to initially return
     *            this value.
     *
     * @return Absolute timestamp at which virtual time was first enabled (up time
     *         in milliseconds).
     */
    @Experimental
    CompletableFuture<Double> setVirtualTimePolicy(
            VirtualTimePolicy policy,
            @Optional Double budget,
            @Optional Integer maxVirtualTimeTaskStarvationCount,
            @Optional Double initialVirtualTime);

    /**
     * Resizes the frame/viewport of the page. Note that this does not affect the
     * frame's container (e.g. browser window). Can be used to produce screenshots
     * of the specified size. Not supported on Android.
     *
     * @param width
     *            Frame width (DIP).
     * @param height
     *            Frame height (DIP).
     */
    @Experimental
    @Deprecated
    CompletableFuture<Void> setVisibleSize(Integer width, Integer height);
}
