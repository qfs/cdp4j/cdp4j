// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.webauthn.Credential;
import com.cdp4j.type.webauthn.VirtualAuthenticatorOptions;
import java.util.List;

class WebAuthnImpl extends ParameterizedCommandImpl<WebAuthn> implements WebAuthn {

    private static final TypeReference<List<Credential>> LIST_CREDENTIAL = new TypeReference<List<Credential>>() {};
    private static final CommandReturnType CRT_ADD_CREDENTIAL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ADD_VIRTUAL_AUTHENTICATOR =
            new CommandReturnType("authenticatorId", String.class, null);
    private static final CommandReturnType CRT_CLEAR_CREDENTIALS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_CREDENTIAL =
            new CommandReturnType("credential", Credential.class, null);
    private static final CommandReturnType CRT_GET_CREDENTIALS =
            new CommandReturnType("credentials", List.class, LIST_CREDENTIAL);
    private static final CommandReturnType CRT_REMOVE_CREDENTIAL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_VIRTUAL_AUTHENTICATOR =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_AUTOMATIC_PRESENCE_SIMULATION =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_CREDENTIAL_PROPERTIES =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_RESPONSE_OVERRIDE_BITS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_USER_VERIFIED = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ADD_CREDENTIAL_1 = new String[] {"authenticatorId", "credential"};
    private static final String[] PARAMS_ADD_VIRTUAL_AUTHENTICATOR_1 = new String[] {"options"};
    private static final String[] PARAMS_CLEAR_CREDENTIALS_1 = new String[] {"authenticatorId"};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"enableUI"};
    private static final String[] PARAMS_GET_CREDENTIALS_1 = new String[] {"authenticatorId"};
    private static final String[] PARAMS_GET_CREDENTIAL_1 = new String[] {"authenticatorId", "credentialId"};
    private static final String[] PARAMS_REMOVE_CREDENTIAL_1 = new String[] {"authenticatorId", "credentialId"};
    private static final String[] PARAMS_REMOVE_VIRTUAL_AUTHENTICATOR_1 = new String[] {"authenticatorId"};
    private static final String[] PARAMS_SET_AUTOMATIC_PRESENCE_SIMULATION_1 =
            new String[] {"authenticatorId", "enabled"};
    private static final String[] PARAMS_SET_CREDENTIAL_PROPERTIES_1 = new String[] {"authenticatorId", "credentialId"};
    private static final String[] PARAMS_SET_CREDENTIAL_PROPERTIES_2 =
            new String[] {"authenticatorId", "credentialId", "backupEligibility", "backupState"};
    private static final String[] PARAMS_SET_RESPONSE_OVERRIDE_BITS_1 = new String[] {"authenticatorId"};
    private static final String[] PARAMS_SET_RESPONSE_OVERRIDE_BITS_2 =
            new String[] {"authenticatorId", "isBogusSignature", "isBadUV", "isBadUP"};
    private static final String[] PARAMS_SET_USER_VERIFIED_1 = new String[] {"authenticatorId", "isUserVerified"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public WebAuthnImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void addCredential(String authenticatorId, Credential credential) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_addCredential,
                CRT_ADD_CREDENTIAL,
                PARAMS_ADD_CREDENTIAL_1,
                new Object[] {authenticatorId, credential},
                true);
    }

    @Override
    public String addVirtualAuthenticator(VirtualAuthenticatorOptions options) {
        return (String) handler.invoke(
                this,
                DomainCommand.WebAuthn_addVirtualAuthenticator,
                CRT_ADD_VIRTUAL_AUTHENTICATOR,
                PARAMS_ADD_VIRTUAL_AUTHENTICATOR_1,
                new Object[] {options},
                true);
    }

    @Override
    public void clearCredentials(String authenticatorId) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_clearCredentials,
                CRT_CLEAR_CREDENTIALS,
                PARAMS_CLEAR_CREDENTIALS_1,
                new Object[] {authenticatorId},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.WebAuthn_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.WebAuthn_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable(Boolean enableUI) {
        handler.invoke(this, DomainCommand.WebAuthn_enable, CRT_ENABLE, PARAMS_ENABLE_2, new Object[] {enableUI}, true);
    }

    @Override
    public Credential getCredential(String authenticatorId, String credentialId) {
        return (Credential) handler.invoke(
                this,
                DomainCommand.WebAuthn_getCredential,
                CRT_GET_CREDENTIAL,
                PARAMS_GET_CREDENTIAL_1,
                new Object[] {authenticatorId, credentialId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Credential> getCredentials(String authenticatorId) {
        return (List<Credential>) handler.invoke(
                this,
                DomainCommand.WebAuthn_getCredentials,
                CRT_GET_CREDENTIALS,
                PARAMS_GET_CREDENTIALS_1,
                new Object[] {authenticatorId},
                true);
    }

    @Override
    public void removeCredential(String authenticatorId, String credentialId) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_removeCredential,
                CRT_REMOVE_CREDENTIAL,
                PARAMS_REMOVE_CREDENTIAL_1,
                new Object[] {authenticatorId, credentialId},
                true);
    }

    @Override
    public void removeVirtualAuthenticator(String authenticatorId) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_removeVirtualAuthenticator,
                CRT_REMOVE_VIRTUAL_AUTHENTICATOR,
                PARAMS_REMOVE_VIRTUAL_AUTHENTICATOR_1,
                new Object[] {authenticatorId},
                true);
    }

    @Override
    public void setAutomaticPresenceSimulation(String authenticatorId, Boolean enabled) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_setAutomaticPresenceSimulation,
                CRT_SET_AUTOMATIC_PRESENCE_SIMULATION,
                PARAMS_SET_AUTOMATIC_PRESENCE_SIMULATION_1,
                new Object[] {authenticatorId, enabled},
                true);
    }

    @Override
    public void setCredentialProperties(String authenticatorId, String credentialId) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_setCredentialProperties,
                CRT_SET_CREDENTIAL_PROPERTIES,
                PARAMS_SET_CREDENTIAL_PROPERTIES_1,
                new Object[] {authenticatorId, credentialId},
                true);
    }

    @Override
    public void setCredentialProperties(
            String authenticatorId, String credentialId, Boolean backupEligibility, Boolean backupState) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_setCredentialProperties,
                CRT_SET_CREDENTIAL_PROPERTIES,
                PARAMS_SET_CREDENTIAL_PROPERTIES_2,
                new Object[] {authenticatorId, credentialId, backupEligibility, backupState},
                true);
    }

    @Override
    public void setResponseOverrideBits(String authenticatorId) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_setResponseOverrideBits,
                CRT_SET_RESPONSE_OVERRIDE_BITS,
                PARAMS_SET_RESPONSE_OVERRIDE_BITS_1,
                new Object[] {authenticatorId},
                true);
    }

    @Override
    public void setResponseOverrideBits(
            String authenticatorId, Boolean isBogusSignature, Boolean isBadUV, Boolean isBadUP) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_setResponseOverrideBits,
                CRT_SET_RESPONSE_OVERRIDE_BITS,
                PARAMS_SET_RESPONSE_OVERRIDE_BITS_2,
                new Object[] {authenticatorId, isBogusSignature, isBadUV, isBadUP},
                true);
    }

    @Override
    public void setUserVerified(String authenticatorId, Boolean isUserVerified) {
        handler.invoke(
                this,
                DomainCommand.WebAuthn_setUserVerified,
                CRT_SET_USER_VERIFIED,
                PARAMS_SET_USER_VERIFIED_1,
                new Object[] {authenticatorId, isUserVerified},
                true);
    }
}
