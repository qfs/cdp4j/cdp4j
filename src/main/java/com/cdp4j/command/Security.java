// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.security.CertificateErrorAction;

/**
 * Security
 */
public interface Security extends ParameterizedCommand<Security> {
    /**
     * Disables tracking security state changes.
     */
    void disable();

    /**
     * Enables tracking security state changes.
     */
    void enable();

    /**
     * Handles a certificate error that fired a certificateError event.
     *
     * @param eventId The ID of the event.
     * @param action The action to take on the certificate error.
     */
    @Deprecated
    void handleCertificateError(Integer eventId, CertificateErrorAction action);

    /**
     * Enable/disable whether all certificate errors should be ignored.
     *
     * @param ignore If true, all certificate errors will be ignored.
     */
    void setIgnoreCertificateErrors(Boolean ignore);

    /**
     * Enable/disable overriding certificate errors. If enabled, all certificate error events need to
     * be handled by the DevTools client and should be answered with handleCertificateError commands.
     *
     * @param override If true, certificate errors will be overridden.
     */
    @Deprecated
    void setOverrideCertificateErrors(Boolean override);
}
