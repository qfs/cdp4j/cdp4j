// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import java.util.concurrent.CompletableFuture;

class DeviceOrientationAsyncImpl extends ParameterizedCommandImpl<DeviceOrientationAsync>
        implements DeviceOrientationAsync {

    private static final CommandReturnType CRT_CLEAR_DEVICE_ORIENTATION_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DEVICE_ORIENTATION_OVERRIDE =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_SET_DEVICE_ORIENTATION_OVERRIDE_1 = new String[] {"alpha", "beta", "gamma"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DeviceOrientationAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearDeviceOrientationOverride() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DeviceOrientation_clearDeviceOrientationOverride,
                CRT_CLEAR_DEVICE_ORIENTATION_OVERRIDE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDeviceOrientationOverride(Double alpha, Double beta, Double gamma) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DeviceOrientation_setDeviceOrientationOverride,
                CRT_SET_DEVICE_ORIENTATION_OVERRIDE,
                PARAMS_SET_DEVICE_ORIENTATION_OVERRIDE_1,
                new Object[] {alpha, beta, gamma},
                false);
    }
}
