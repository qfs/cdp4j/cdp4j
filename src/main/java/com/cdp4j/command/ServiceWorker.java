// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

@Experimental
public interface ServiceWorker extends ParameterizedCommand<ServiceWorker> {
    void deliverPushMessage(String origin, String registrationId, String data);

    void disable();

    void dispatchPeriodicSyncEvent(String origin, String registrationId, String tag);

    void dispatchSyncEvent(String origin, String registrationId, String tag, Boolean lastChance);

    void enable();

    void inspectWorker(String versionId);

    void setForceUpdateOnPageLoad(Boolean forceUpdateOnPageLoad);

    void skipWaiting(String scopeURL);

    void startWorker(String scopeURL);

    void stopAllWorkers();

    void stopWorker(String versionId);

    void unregister(String scopeURL);

    void updateRegistration(String scopeURL);
}
