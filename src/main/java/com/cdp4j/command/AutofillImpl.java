// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.autofill.Address;
import com.cdp4j.type.autofill.CreditCard;
import java.util.List;

class AutofillImpl extends ParameterizedCommandImpl<Autofill> implements Autofill {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ADDRESSES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TRIGGER = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_SET_ADDRESSES_1 = new String[] {"addresses"};
    private static final String[] PARAMS_TRIGGER_1 = new String[] {"fieldId", "card"};
    private static final String[] PARAMS_TRIGGER_2 = new String[] {"fieldId", "frameId", "card"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public AutofillImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Autofill_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Autofill_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void setAddresses(List<Address> addresses) {
        handler.invoke(
                this,
                DomainCommand.Autofill_setAddresses,
                CRT_SET_ADDRESSES,
                PARAMS_SET_ADDRESSES_1,
                new Object[] {addresses},
                true);
    }

    @Override
    public void trigger(Integer fieldId, CreditCard card) {
        handler.invoke(
                this,
                DomainCommand.Autofill_trigger,
                CRT_TRIGGER,
                PARAMS_TRIGGER_1,
                new Object[] {fieldId, card},
                true);
    }

    @Override
    public void trigger(Integer fieldId, String frameId, CreditCard card) {
        handler.invoke(
                this,
                DomainCommand.Autofill_trigger,
                CRT_TRIGGER,
                PARAMS_TRIGGER_2,
                new Object[] {fieldId, frameId, card},
                true);
    }
}
