// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.constant.TimeDomain;
import com.cdp4j.type.performance.Metric;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface PerformanceAsync extends ParameterizedCommand<PerformanceAsync> {
    /**
     * Disable collecting and reporting metrics.
     */
    CompletableFuture<Void> disable();

    /**
     * Enable collecting and reporting metrics.
     */
    CompletableFuture<Void> enable();

    /**
     * Enable collecting and reporting metrics.
     *
     * @param timeDomain
     *            Time domain to use for collecting and reporting duration metrics.
     */
    CompletableFuture<Void> enable(@Optional TimeDomain timeDomain);

    /**
     * Retrieve current values of run-time metrics.
     *
     * @return Current values for run-time metrics.
     */
    CompletableFuture<List<Metric>> getMetrics();

    /**
     * Sets time domain to use for collecting and reporting duration metrics. Note
     * that this must be called before enabling metrics collection. Calling this
     * method while metrics collection is enabled returns an error.
     *
     * @param timeDomain
     *            Time domain
     */
    @Experimental
    @Deprecated
    CompletableFuture<Void> setTimeDomain(TimeDomain timeDomain);
}
