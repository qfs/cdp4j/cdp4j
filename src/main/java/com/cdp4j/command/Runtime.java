// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.runtime.AwaitPromiseResult;
import com.cdp4j.type.runtime.CallArgument;
import com.cdp4j.type.runtime.CallFunctionOnResult;
import com.cdp4j.type.runtime.CompileScriptResult;
import com.cdp4j.type.runtime.EvaluateResult;
import com.cdp4j.type.runtime.ExceptionDetails;
import com.cdp4j.type.runtime.GetHeapUsageResult;
import com.cdp4j.type.runtime.GetPropertiesResult;
import com.cdp4j.type.runtime.RemoteObject;
import com.cdp4j.type.runtime.RunScriptResult;
import com.cdp4j.type.runtime.SerializationOptions;
import java.util.List;

/**
 * Runtime domain exposes JavaScript runtime by means of remote evaluation and mirror objects.
 * Evaluation results are returned as mirror object that expose object type, string representation
 * and unique identifier that can be used for further object reference. Original objects are
 * maintained in memory unless they are either explicitly released or are released along with the
 * other objects in their object group.
 */
public interface Runtime extends ParameterizedCommand<Runtime> {
    /**
     * If executionContextId is empty, adds binding with the given name on the
     * global objects of all inspected contexts, including those created later,
     * bindings survive reloads.
     * Binding function takes exactly one argument, this argument should be string,
     * in case of any other input, function throws an exception.
     * Each binding function call produces Runtime.bindingCalled notification.
     *
     */
    void addBinding(String name);

    /**
     * If executionContextId is empty, adds binding with the given name on the
     * global objects of all inspected contexts, including those created later,
     * bindings survive reloads.
     * Binding function takes exactly one argument, this argument should be string,
     * in case of any other input, function throws an exception.
     * Each binding function call produces Runtime.bindingCalled notification.
     *
     * @param executionContextId If specified, the binding would only be exposed to the specified
     * execution context. If omitted and executionContextName is not set,
     * the binding is exposed to all execution contexts of the target.
     * This parameter is mutually exclusive with executionContextName.
     * Deprecated in favor of executionContextName due to an unclear use case
     * and bugs in implementation (crbug.com/1169639). executionContextId will be
     * removed in the future.
     * @param executionContextName If specified, the binding is exposed to the executionContext with
     * matching name, even for contexts created after the binding is added.
     * See also ExecutionContext.name and worldName parameter to
     * Page.addScriptToEvaluateOnNewDocument.
     * This parameter is mutually exclusive with executionContextId.
     */
    void addBinding(
            String name,
            @Experimental @Optional @Deprecated Integer executionContextId,
            @Optional String executionContextName);

    /**
     * Add handler to promise with given promise object id.
     *
     * @param promiseObjectId Identifier of the promise.
     *
     * @return AwaitPromiseResult
     */
    AwaitPromiseResult awaitPromise(String promiseObjectId);

    /**
     * Add handler to promise with given promise object id.
     *
     * @param promiseObjectId Identifier of the promise.
     * @param returnByValue Whether the result is expected to be a JSON object that should be sent by value.
     * @param generatePreview Whether preview should be generated for the result.
     *
     * @return AwaitPromiseResult
     */
    AwaitPromiseResult awaitPromise(
            String promiseObjectId, @Optional Boolean returnByValue, @Optional Boolean generatePreview);

    /**
     * Calls function with given declaration on the given object. Object group of the result is
     * inherited from the target object.
     *
     * @param functionDeclaration Declaration of the function to call.
     *
     * @return CallFunctionOnResult
     */
    CallFunctionOnResult callFunctionOn(String functionDeclaration);

    /**
     * Calls function with given declaration on the given object. Object group of the result is
     * inherited from the target object.
     *
     * @param functionDeclaration Declaration of the function to call.
     * @param objectId Identifier of the object to call function on. Either objectId or executionContextId should
     * be specified.
     * @param arguments Call arguments. All call arguments must belong to the same JavaScript world as the target
     * object.
     * @param silent In silent mode exceptions thrown during evaluation are not reported and do not pause
     * execution. Overrides setPauseOnException state.
     * @param returnByValue Whether the result is expected to be a JSON object which should be sent by value.
     * Can be overriden by serializationOptions.
     * @param generatePreview Whether preview should be generated for the result.
     * @param userGesture Whether execution should be treated as initiated by user in the UI.
     * @param awaitPromise Whether execution should await for resulting value and return once awaited promise is
     * resolved.
     * @param executionContextId Specifies execution context which global object will be used to call function on. Either
     * executionContextId or objectId should be specified.
     * @param objectGroup Symbolic group name that can be used to release multiple objects. If objectGroup is not
     * specified and objectId is, objectGroup will be inherited from object.
     * @param throwOnSideEffect Whether to throw an exception if side effect cannot be ruled out during evaluation.
     * @param uniqueContextId An alternative way to specify the execution context to call function on.
     * Compared to contextId that may be reused across processes, this is guaranteed to be
     * system-unique, so it can be used to prevent accidental function call
     * in context different than intended (e.g. as a result of navigation across process
     * boundaries).
     * This is mutually exclusive with executionContextId.
     * @param serializationOptions Specifies the result serialization. If provided, overrides
     * generatePreview and returnByValue.
     *
     * @return CallFunctionOnResult
     */
    CallFunctionOnResult callFunctionOn(
            String functionDeclaration,
            @Optional String objectId,
            @Optional List<CallArgument> arguments,
            @Optional Boolean silent,
            @Optional Boolean returnByValue,
            @Experimental @Optional Boolean generatePreview,
            @Optional Boolean userGesture,
            @Optional Boolean awaitPromise,
            @Optional Integer executionContextId,
            @Optional String objectGroup,
            @Experimental @Optional Boolean throwOnSideEffect,
            @Experimental @Optional String uniqueContextId,
            @Experimental @Optional SerializationOptions serializationOptions);

    /**
     * Compiles expression.
     *
     * @param expression Expression to compile.
     * @param sourceURL Source url to be set for the script.
     * @param persistScript Specifies whether the compiled script should be persisted.
     *
     * @return CompileScriptResult
     */
    CompileScriptResult compileScript(String expression, String sourceURL, Boolean persistScript);

    /**
     * Compiles expression.
     *
     * @param expression Expression to compile.
     * @param sourceURL Source url to be set for the script.
     * @param persistScript Specifies whether the compiled script should be persisted.
     * @param executionContextId Specifies in which execution context to perform script run. If the parameter is omitted the
     * evaluation will be performed in the context of the inspected page.
     *
     * @return CompileScriptResult
     */
    CompileScriptResult compileScript(
            String expression, String sourceURL, Boolean persistScript, @Optional Integer executionContextId);

    /**
     * Disables reporting of execution contexts creation.
     */
    void disable();

    /**
     * Discards collected exceptions and console API calls.
     */
    void discardConsoleEntries();

    /**
     * Enables reporting of execution contexts creation by means of executionContextCreated event.
     * When the reporting gets enabled the event will be sent immediately for each existing execution
     * context.
     */
    void enable();

    /**
     * Evaluates expression on global object.
     *
     * @param expression Expression to evaluate.
     *
     * @return EvaluateResult
     */
    EvaluateResult evaluate(String expression);

    /**
     * Evaluates expression on global object.
     *
     * @param expression Expression to evaluate.
     * @param objectGroup Symbolic group name that can be used to release multiple objects.
     * @param includeCommandLineAPI Determines whether Command Line API should be available during the evaluation.
     * @param silent In silent mode exceptions thrown during evaluation are not reported and do not pause
     * execution. Overrides setPauseOnException state.
     * @param contextId Specifies in which execution context to perform evaluation. If the parameter is omitted the
     * evaluation will be performed in the context of the inspected page.
     * This is mutually exclusive with uniqueContextId, which offers an
     * alternative way to identify the execution context that is more reliable
     * in a multi-process environment.
     * @param returnByValue Whether the result is expected to be a JSON object that should be sent by value.
     * @param generatePreview Whether preview should be generated for the result.
     * @param userGesture Whether execution should be treated as initiated by user in the UI.
     * @param awaitPromise Whether execution should await for resulting value and return once awaited promise is
     * resolved.
     * @param throwOnSideEffect Whether to throw an exception if side effect cannot be ruled out during evaluation.
     * This implies disableBreaks below.
     * @param timeout Terminate execution after timing out (number of milliseconds).
     * @param disableBreaks Disable breakpoints during execution.
     * @param replMode Setting this flag to true enables let re-declaration and top-level await.
     * Note that let variables can only be re-declared if they originate from
     * replMode themselves.
     * @param allowUnsafeEvalBlockedByCSP The Content Security Policy (CSP) for the target might block 'unsafe-eval'
     * which includes eval(), Function(), setTimeout() and setInterval()
     * when called with non-callable arguments. This flag bypasses CSP for this
     * evaluation and allows unsafe-eval. Defaults to true.
     * @param uniqueContextId An alternative way to specify the execution context to evaluate in.
     * Compared to contextId that may be reused across processes, this is guaranteed to be
     * system-unique, so it can be used to prevent accidental evaluation of the expression
     * in context different than intended (e.g. as a result of navigation across process
     * boundaries).
     * This is mutually exclusive with contextId.
     * @param serializationOptions Specifies the result serialization. If provided, overrides
     * generatePreview and returnByValue.
     *
     * @return EvaluateResult
     */
    EvaluateResult evaluate(
            String expression,
            @Optional String objectGroup,
            @Optional Boolean includeCommandLineAPI,
            @Optional Boolean silent,
            @Optional Integer contextId,
            @Optional Boolean returnByValue,
            @Experimental @Optional Boolean generatePreview,
            @Optional Boolean userGesture,
            @Optional Boolean awaitPromise,
            @Experimental @Optional Boolean throwOnSideEffect,
            @Experimental @Optional Double timeout,
            @Experimental @Optional Boolean disableBreaks,
            @Experimental @Optional Boolean replMode,
            @Experimental @Optional Boolean allowUnsafeEvalBlockedByCSP,
            @Experimental @Optional String uniqueContextId,
            @Experimental @Optional SerializationOptions serializationOptions);

    /**
     * This method tries to lookup and populate exception details for a
     * JavaScript Error object.
     * Note that the stackTrace portion of the resulting exceptionDetails will
     * only be populated if the Runtime domain was enabled at the time when the
     * Error was thrown.
     *
     * @param errorObjectId The error object for which to resolve the exception details.
     */
    @Experimental
    ExceptionDetails getExceptionDetails(String errorObjectId);

    /**
     * Returns the JavaScript heap usage.
     * It is the total usage of the corresponding isolate not scoped to a particular Runtime.
     *
     * @return GetHeapUsageResult
     */
    @Experimental
    GetHeapUsageResult getHeapUsage();

    /**
     * Returns the isolate id.
     *
     * @return The isolate id.
     */
    @Experimental
    String getIsolateId();

    /**
     * Returns properties of a given object. Object group of the result is inherited from the target
     * object.
     *
     * @param objectId Identifier of the object to return properties for.
     *
     * @return GetPropertiesResult
     */
    GetPropertiesResult getProperties(String objectId);

    /**
     * Returns properties of a given object. Object group of the result is inherited from the target
     * object.
     *
     * @param objectId Identifier of the object to return properties for.
     * @param ownProperties If true, returns properties belonging only to the element itself, not to its prototype
     * chain.
     * @param accessorPropertiesOnly If true, returns accessor properties (with getter/setter) only; internal properties are not
     * returned either.
     * @param generatePreview Whether preview should be generated for the results.
     * @param nonIndexedPropertiesOnly If true, returns non-indexed properties only.
     *
     * @return GetPropertiesResult
     */
    GetPropertiesResult getProperties(
            String objectId,
            @Optional Boolean ownProperties,
            @Experimental @Optional Boolean accessorPropertiesOnly,
            @Experimental @Optional Boolean generatePreview,
            @Experimental @Optional Boolean nonIndexedPropertiesOnly);

    /**
     * Returns all let, const and class variables from global scope.
     */
    List<String> globalLexicalScopeNames();

    /**
     * Returns all let, const and class variables from global scope.
     *
     * @param executionContextId Specifies in which execution context to lookup global scope variables.
     */
    List<String> globalLexicalScopeNames(@Optional Integer executionContextId);

    RemoteObject queryObjects(String prototypeObjectId);

    RemoteObject queryObjects(String prototypeObjectId, @Optional String objectGroup);

    /**
     * Releases remote object with given id.
     *
     * @param objectId Identifier of the object to release.
     */
    void releaseObject(String objectId);

    /**
     * Releases all remote objects that belong to a given group.
     *
     * @param objectGroup Symbolic object group name.
     */
    void releaseObjectGroup(String objectGroup);

    /**
     * This method does not remove binding function from global object but
     * unsubscribes current runtime agent from Runtime.bindingCalled notifications.
     *
     */
    void removeBinding(String name);

    /**
     * Tells inspected instance to run if it was waiting for debugger to attach.
     */
    void runIfWaitingForDebugger();

    /**
     * Runs script with given id in a given context.
     *
     * @param scriptId Id of the script to run.
     *
     * @return RunScriptResult
     */
    RunScriptResult runScript(String scriptId);

    /**
     * Runs script with given id in a given context.
     *
     * @param scriptId Id of the script to run.
     * @param executionContextId Specifies in which execution context to perform script run. If the parameter is omitted the
     * evaluation will be performed in the context of the inspected page.
     * @param objectGroup Symbolic group name that can be used to release multiple objects.
     * @param silent In silent mode exceptions thrown during evaluation are not reported and do not pause
     * execution. Overrides setPauseOnException state.
     * @param includeCommandLineAPI Determines whether Command Line API should be available during the evaluation.
     * @param returnByValue Whether the result is expected to be a JSON object which should be sent by value.
     * @param generatePreview Whether preview should be generated for the result.
     * @param awaitPromise Whether execution should await for resulting value and return once awaited promise is
     * resolved.
     *
     * @return RunScriptResult
     */
    RunScriptResult runScript(
            String scriptId,
            @Optional Integer executionContextId,
            @Optional String objectGroup,
            @Optional Boolean silent,
            @Optional Boolean includeCommandLineAPI,
            @Optional Boolean returnByValue,
            @Optional Boolean generatePreview,
            @Optional Boolean awaitPromise);

    /**
     * Enables or disables async call stacks tracking.
     *
     * @param maxDepth Maximum depth of async call stacks. Setting to 0 will effectively disable collecting async
     * call stacks (default).
     */
    void setAsyncCallStackDepth(Integer maxDepth);

    @Experimental
    void setCustomObjectFormatterEnabled(Boolean enabled);

    @Experimental
    void setMaxCallStackSizeToCapture(Integer size);

    /**
     * Terminate current or next JavaScript execution.
     * Will cancel the termination when the outer-most script execution ends.
     */
    @Experimental
    void terminateExecution();
}
