// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.autofill.Address;
import com.cdp4j.type.autofill.CreditCard;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class AutofillAsyncImpl extends ParameterizedCommandImpl<AutofillAsync> implements AutofillAsync {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ADDRESSES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TRIGGER = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_SET_ADDRESSES_1 = new String[] {"addresses"};
    private static final String[] PARAMS_TRIGGER_1 = new String[] {"fieldId", "card"};
    private static final String[] PARAMS_TRIGGER_2 = new String[] {"fieldId", "frameId", "card"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public AutofillAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Autofill_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Autofill_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAddresses(List<Address> addresses) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Autofill_setAddresses,
                CRT_SET_ADDRESSES,
                PARAMS_SET_ADDRESSES_1,
                new Object[] {addresses},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trigger(Integer fieldId, CreditCard card) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Autofill_trigger,
                CRT_TRIGGER,
                PARAMS_TRIGGER_1,
                new Object[] {fieldId, card},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trigger(Integer fieldId, String frameId, CreditCard card) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Autofill_trigger,
                CRT_TRIGGER,
                PARAMS_TRIGGER_2,
                new Object[] {fieldId, frameId, card},
                false);
    }
}
