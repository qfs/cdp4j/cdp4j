// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.schema.Domain;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class SchemaAsyncImpl extends ParameterizedCommandImpl<SchemaAsync> implements SchemaAsync {

    private static final TypeReference<List<Domain>> LIST_DOMAIN = new TypeReference<List<Domain>>() {};
    private static final CommandReturnType CRT_GET_DOMAINS = new CommandReturnType("domains", List.class, LIST_DOMAIN);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public SchemaAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Domain>> getDomains() {
        return (CompletableFuture<List<Domain>>)
                handler.invoke(this, DomainCommand.Schema_getDomains, CRT_GET_DOMAINS, EMPTY_ARGS, EMPTY_VALUES, false);
    }
}
