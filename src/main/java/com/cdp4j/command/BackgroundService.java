// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.backgroundservice.ServiceName;

/**
 * Defines events for background web platform features.
 */
@Experimental
public interface BackgroundService extends ParameterizedCommand<BackgroundService> {
    /**
     * Clears all stored data for the service.
     *
     */
    void clearEvents(ServiceName service);

    /**
     * Set the recording state for the service.
     *
     */
    void setRecording(Boolean shouldRecord, ServiceName service);

    /**
     * Enables event updates for the service.
     *
     */
    void startObserving(ServiceName service);

    /**
     * Disables event updates for the service.
     *
     */
    void stopObserving(ServiceName service);
}
