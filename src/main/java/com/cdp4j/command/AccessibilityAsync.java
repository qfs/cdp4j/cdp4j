// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.accessibility.AXNode;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface AccessibilityAsync extends ParameterizedCommand<AccessibilityAsync> {
    /**
     * Disables the accessibility domain.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables the accessibility domain which causes AXNodeIds to remain consistent
     * between method calls. This turns on accessibility for the page, which can
     * impact performance until accessibility is disabled.
     */
    CompletableFuture<Void> enable();

    /**
     * Fetches a node and all ancestors up to and including the root. Requires
     * enable() to have been called previously.
     */
    @Experimental
    CompletableFuture<List<AXNode>> getAXNodeAndAncestors();

    /**
     * Fetches a node and all ancestors up to and including the root. Requires
     * enable() to have been called previously.
     *
     * @param nodeId
     *            Identifier of the node to get.
     * @param backendNodeId
     *            Identifier of the backend node to get.
     * @param objectId
     *            JavaScript object id of the node wrapper to get.
     */
    @Experimental
    CompletableFuture<List<AXNode>> getAXNodeAndAncestors(
            @Optional Integer nodeId, @Optional Integer backendNodeId, @Optional String objectId);

    /**
     * Fetches a particular accessibility node by AXNodeId. Requires enable() to
     * have been called previously.
     *
     */
    @Experimental
    CompletableFuture<List<AXNode>> getChildAXNodes(String id);

    /**
     * Fetches a particular accessibility node by AXNodeId. Requires enable() to
     * have been called previously.
     *
     * @param frameId
     *            The frame in whose document the node resides. If omitted, the root
     *            frame is used.
     */
    @Experimental
    CompletableFuture<List<AXNode>> getChildAXNodes(String id, @Optional String frameId);

    /**
     * Fetches the entire accessibility tree for the root Document
     */
    @Experimental
    CompletableFuture<List<AXNode>> getFullAXTree();

    /**
     * Fetches the entire accessibility tree for the root Document
     *
     * @param depth
     *            The maximum depth at which descendants of the root node should be
     *            retrieved. If omitted, the full tree is returned.
     * @param frameId
     *            The frame for whose document the AX tree should be retrieved. If
     *            omitted, the root frame is used.
     */
    @Experimental
    CompletableFuture<List<AXNode>> getFullAXTree(@Optional Integer depth, @Optional String frameId);

    /**
     * Fetches the accessibility node and partial accessibility tree for this DOM
     * node, if it exists.
     *
     * @return The Accessibility.AXNode for this DOM node, if it exists, plus its
     *         ancestors, siblings and children, if requested.
     */
    @Experimental
    CompletableFuture<List<AXNode>> getPartialAXTree();

    /**
     * Fetches the accessibility node and partial accessibility tree for this DOM
     * node, if it exists.
     *
     * @param nodeId
     *            Identifier of the node to get the partial accessibility tree for.
     * @param backendNodeId
     *            Identifier of the backend node to get the partial accessibility
     *            tree for.
     * @param objectId
     *            JavaScript object id of the node wrapper to get the partial
     *            accessibility tree for.
     * @param fetchRelatives
     *            Whether to fetch this node's ancestors, siblings and children.
     *            Defaults to true.
     *
     * @return The Accessibility.AXNode for this DOM node, if it exists, plus its
     *         ancestors, siblings and children, if requested.
     */
    @Experimental
    CompletableFuture<List<AXNode>> getPartialAXTree(
            @Optional Integer nodeId,
            @Optional Integer backendNodeId,
            @Optional String objectId,
            @Optional Boolean fetchRelatives);

    /**
     * Fetches the root node. Requires enable() to have been called previously.
     */
    @Experimental
    CompletableFuture<AXNode> getRootAXNode();

    /**
     * Fetches the root node. Requires enable() to have been called previously.
     *
     * @param frameId
     *            The frame in whose document the node resides. If omitted, the root
     *            frame is used.
     */
    @Experimental
    CompletableFuture<AXNode> getRootAXNode(@Optional String frameId);

    /**
     * Query a DOM node's accessibility subtree for accessible name and role. This
     * command computes the name and role for all nodes in the subtree, including
     * those that are ignored for accessibility, and returns those that match the
     * specified name and role. If no DOM node is specified, or the DOM node does
     * not exist, the command returns an error. If neither accessibleName or role is
     * specified, it returns all the accessibility nodes in the subtree.
     *
     * @return A list of Accessibility.AXNode matching the specified attributes,
     *         including nodes that are ignored for accessibility.
     */
    @Experimental
    CompletableFuture<List<AXNode>> queryAXTree();

    /**
     * Query a DOM node's accessibility subtree for accessible name and role. This
     * command computes the name and role for all nodes in the subtree, including
     * those that are ignored for accessibility, and returns those that match the
     * specified name and role. If no DOM node is specified, or the DOM node does
     * not exist, the command returns an error. If neither accessibleName or role is
     * specified, it returns all the accessibility nodes in the subtree.
     *
     * @param nodeId
     *            Identifier of the node for the root to query.
     * @param backendNodeId
     *            Identifier of the backend node for the root to query.
     * @param objectId
     *            JavaScript object id of the node wrapper for the root to query.
     * @param accessibleName
     *            Find nodes with this computed name.
     * @param role
     *            Find nodes with this computed role.
     *
     * @return A list of Accessibility.AXNode matching the specified attributes,
     *         including nodes that are ignored for accessibility.
     */
    @Experimental
    CompletableFuture<List<AXNode>> queryAXTree(
            @Optional Integer nodeId,
            @Optional Integer backendNodeId,
            @Optional String objectId,
            @Optional String accessibleName,
            @Optional String role);
}
