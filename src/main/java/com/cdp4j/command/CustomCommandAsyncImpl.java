// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.serialization.GetSnapshotParser;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.domsnapshot.GetSnapshotResult;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class CustomCommandAsyncImpl extends ParameterizedCommandImpl<CustomCommandAsync> implements CustomCommandAsync {

    private static final CommandReturnType CRT_GET_SNAPSHOT =
            new CommandReturnType(null, GetSnapshotResult.class, null);
    private static final String[] PARAMS_GET_SNAPSHOT_1 = new String[] {"computedStyleWhitelist"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public CustomCommandAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetSnapshotResult> getSnapshot(List<String> computedStyleWhitelist) {
        return (CompletableFuture<GetSnapshotResult>) handler.invokeWithCustomParser(
                this,
                DomainCommand.DOMSnapshot_getSnapshot,
                CRT_GET_SNAPSHOT,
                PARAMS_GET_SNAPSHOT_1,
                new Object[] {computedStyleWhitelist},
                false,
                GetSnapshotParser.INSTANCE);
    }
}
