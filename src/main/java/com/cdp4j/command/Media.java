// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

/**
 * This domain allows detailed inspection of media elements
 */
@Experimental
public interface Media extends ParameterizedCommand<Media> {
    /**
     * Disables the Media domain.
     */
    void disable();

    /**
     * Enables the Media domain
     */
    void enable();
}
