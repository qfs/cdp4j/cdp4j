// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import java.util.concurrent.CompletableFuture;

class EventBreakpointsAsyncImpl extends ParameterizedCommandImpl<EventBreakpointsAsync>
        implements EventBreakpointsAsync {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public EventBreakpointsAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.EventBreakpoints_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeInstrumentationBreakpoint(String eventName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.EventBreakpoints_removeInstrumentationBreakpoint,
                CRT_REMOVE_INSTRUMENTATION_BREAKPOINT,
                PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setInstrumentationBreakpoint(String eventName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.EventBreakpoints_setInstrumentationBreakpoint,
                CRT_SET_INSTRUMENTATION_BREAKPOINT,
                PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                false);
    }
}
