// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import java.util.List;

class PerformanceTimelineImpl extends ParameterizedCommandImpl<PerformanceTimeline> implements PerformanceTimeline {

    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final String[] PARAMS_ENABLE_1 = new String[] {"eventTypes"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public PerformanceTimelineImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void enable(List<String> eventTypes) {
        handler.invoke(
                this,
                DomainCommand.PerformanceTimeline_enable,
                CRT_ENABLE,
                PARAMS_ENABLE_1,
                new Object[] {eventTypes},
                true);
    }
}
