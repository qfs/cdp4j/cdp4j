// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

@Experimental
public interface Inspector extends ParameterizedCommand<Inspector> {
    /**
     * Disables inspector domain notifications.
     */
    void disable();

    /**
     * Enables inspector domain notifications.
     */
    void enable();
}
