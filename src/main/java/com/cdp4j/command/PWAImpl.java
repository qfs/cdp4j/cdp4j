// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.pwa.DisplayMode;
import com.cdp4j.type.pwa.GetOsAppStateResult;
import java.util.List;

class PWAImpl extends ParameterizedCommandImpl<PWA> implements PWA {

    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_CHANGE_APP_USER_SETTINGS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_OS_APP_STATE =
            new CommandReturnType(null, GetOsAppStateResult.class, null);
    private static final CommandReturnType CRT_INSTALL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_LAUNCH = new CommandReturnType("targetId", String.class, null);
    private static final CommandReturnType CRT_LAUNCH_FILES_IN_APP =
            new CommandReturnType("targetIds", List.class, LIST_STRING);
    private static final CommandReturnType CRT_OPEN_CURRENT_PAGE_IN_APP = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNINSTALL = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CHANGE_APP_USER_SETTINGS_1 = new String[] {"manifestId"};
    private static final String[] PARAMS_CHANGE_APP_USER_SETTINGS_2 =
            new String[] {"manifestId", "linkCapturing", "displayMode"};
    private static final String[] PARAMS_GET_OS_APP_STATE_1 = new String[] {"manifestId"};
    private static final String[] PARAMS_INSTALL_1 = new String[] {"manifestId"};
    private static final String[] PARAMS_INSTALL_2 = new String[] {"manifestId", "installUrlOrBundleUrl"};
    private static final String[] PARAMS_LAUNCH_1 = new String[] {"manifestId"};
    private static final String[] PARAMS_LAUNCH_2 = new String[] {"manifestId", "url"};
    private static final String[] PARAMS_LAUNCH_FILES_IN_APP_1 = new String[] {"manifestId", "files"};
    private static final String[] PARAMS_OPEN_CURRENT_PAGE_IN_APP_1 = new String[] {"manifestId"};
    private static final String[] PARAMS_UNINSTALL_1 = new String[] {"manifestId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public PWAImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void changeAppUserSettings(String manifestId) {
        handler.invoke(
                this,
                DomainCommand.PWA_changeAppUserSettings,
                CRT_CHANGE_APP_USER_SETTINGS,
                PARAMS_CHANGE_APP_USER_SETTINGS_1,
                new Object[] {manifestId},
                true);
    }

    @Override
    public void changeAppUserSettings(String manifestId, Boolean linkCapturing, DisplayMode displayMode) {
        handler.invoke(
                this,
                DomainCommand.PWA_changeAppUserSettings,
                CRT_CHANGE_APP_USER_SETTINGS,
                PARAMS_CHANGE_APP_USER_SETTINGS_2,
                new Object[] {manifestId, linkCapturing, displayMode},
                true);
    }

    @Override
    public GetOsAppStateResult getOsAppState(String manifestId) {
        return (GetOsAppStateResult) handler.invoke(
                this,
                DomainCommand.PWA_getOsAppState,
                CRT_GET_OS_APP_STATE,
                PARAMS_GET_OS_APP_STATE_1,
                new Object[] {manifestId},
                true);
    }

    @Override
    public void install(String manifestId) {
        handler.invoke(this, DomainCommand.PWA_install, CRT_INSTALL, PARAMS_INSTALL_1, new Object[] {manifestId}, true);
    }

    @Override
    public void install(String manifestId, String installUrlOrBundleUrl) {
        handler.invoke(
                this,
                DomainCommand.PWA_install,
                CRT_INSTALL,
                PARAMS_INSTALL_2,
                new Object[] {manifestId, installUrlOrBundleUrl},
                true);
    }

    @Override
    public String launch(String manifestId) {
        return (String) handler.invoke(
                this, DomainCommand.PWA_launch, CRT_LAUNCH, PARAMS_LAUNCH_1, new Object[] {manifestId}, true);
    }

    @Override
    public String launch(String manifestId, String url) {
        return (String) handler.invoke(
                this, DomainCommand.PWA_launch, CRT_LAUNCH, PARAMS_LAUNCH_2, new Object[] {manifestId, url}, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> launchFilesInApp(String manifestId, List<String> files) {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.PWA_launchFilesInApp,
                CRT_LAUNCH_FILES_IN_APP,
                PARAMS_LAUNCH_FILES_IN_APP_1,
                new Object[] {manifestId, files},
                true);
    }

    @Override
    public void openCurrentPageInApp(String manifestId) {
        handler.invoke(
                this,
                DomainCommand.PWA_openCurrentPageInApp,
                CRT_OPEN_CURRENT_PAGE_IN_APP,
                PARAMS_OPEN_CURRENT_PAGE_IN_APP_1,
                new Object[] {manifestId},
                true);
    }

    @Override
    public void uninstall(String manifestId) {
        handler.invoke(
                this, DomainCommand.PWA_uninstall, CRT_UNINSTALL, PARAMS_UNINSTALL_1, new Object[] {manifestId}, true);
    }
}
