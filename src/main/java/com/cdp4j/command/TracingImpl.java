// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.TracingTransferMode;
import com.cdp4j.type.tracing.MemoryDumpLevelOfDetail;
import com.cdp4j.type.tracing.RequestMemoryDumpResult;
import com.cdp4j.type.tracing.StreamCompression;
import com.cdp4j.type.tracing.StreamFormat;
import com.cdp4j.type.tracing.TraceConfig;
import com.cdp4j.type.tracing.TracingBackend;
import java.util.List;

class TracingImpl extends ParameterizedCommandImpl<Tracing> implements Tracing {

    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_END = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_CATEGORIES =
            new CommandReturnType("categories", List.class, LIST_STRING);
    private static final CommandReturnType CRT_RECORD_CLOCK_SYNC_MARKER = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REQUEST_MEMORY_DUMP =
            new CommandReturnType(null, RequestMemoryDumpResult.class, null);
    private static final CommandReturnType CRT_START = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_RECORD_CLOCK_SYNC_MARKER_1 = new String[] {"syncId"};
    private static final String[] PARAMS_REQUEST_MEMORY_DUMP_2 = new String[] {"deterministic", "levelOfDetail"};
    private static final String[] PARAMS_START_2 = new String[] {
        "categories",
        "options",
        "bufferUsageReportingInterval",
        "transferMode",
        "streamFormat",
        "streamCompression",
        "traceConfig",
        "perfettoConfig",
        "tracingBackend"
    };
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public TracingImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void end() {
        handler.invoke(this, DomainCommand.Tracing_end, CRT_END, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> getCategories() {
        return (List<String>) handler.invoke(
                this, DomainCommand.Tracing_getCategories, CRT_GET_CATEGORIES, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void recordClockSyncMarker(String syncId) {
        handler.invoke(
                this,
                DomainCommand.Tracing_recordClockSyncMarker,
                CRT_RECORD_CLOCK_SYNC_MARKER,
                PARAMS_RECORD_CLOCK_SYNC_MARKER_1,
                new Object[] {syncId},
                true);
    }

    @Override
    public RequestMemoryDumpResult requestMemoryDump() {
        return (RequestMemoryDumpResult) handler.invoke(
                this, DomainCommand.Tracing_requestMemoryDump, CRT_REQUEST_MEMORY_DUMP, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public RequestMemoryDumpResult requestMemoryDump(Boolean deterministic, MemoryDumpLevelOfDetail levelOfDetail) {
        return (RequestMemoryDumpResult) handler.invoke(
                this,
                DomainCommand.Tracing_requestMemoryDump,
                CRT_REQUEST_MEMORY_DUMP,
                PARAMS_REQUEST_MEMORY_DUMP_2,
                new Object[] {deterministic, levelOfDetail},
                true);
    }

    @Override
    public void start() {
        handler.invoke(this, DomainCommand.Tracing_start, CRT_START, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void start(
            String categories,
            String options,
            Double bufferUsageReportingInterval,
            TracingTransferMode transferMode,
            StreamFormat streamFormat,
            StreamCompression streamCompression,
            TraceConfig traceConfig,
            String perfettoConfig,
            TracingBackend tracingBackend) {
        handler.invoke(
                this,
                DomainCommand.Tracing_start,
                CRT_START,
                PARAMS_START_2,
                new Object[] {
                    categories,
                    options,
                    bufferUsageReportingInterval,
                    transferMode,
                    streamFormat,
                    streamCompression,
                    traceConfig,
                    perfettoConfig,
                    tracingBackend
                },
                true);
    }
}
