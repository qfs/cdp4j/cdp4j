// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.audits.GenericIssueDetails;
import com.cdp4j.type.audits.GetEncodedResponseResult;
import com.cdp4j.type.constant.Encoding;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Audits domain allows investigation of page violations and possible
 * improvements.
 */
@Experimental
public interface AuditsAsync extends ParameterizedCommand<AuditsAsync> {
    /**
     * Runs the contrast check for the target page. Found issues are reported using
     * Audits.issueAdded event.
     */
    CompletableFuture<Void> checkContrast();

    /**
     * Runs the contrast check for the target page. Found issues are reported using
     * Audits.issueAdded event.
     *
     * @param reportAAA
     *            Whether to report WCAG AAA level issues. Default is false.
     */
    CompletableFuture<Void> checkContrast(@Optional Boolean reportAAA);

    /**
     * Runs the form issues check for the target page. Found issues are reported
     * using Audits.issueAdded event.
     */
    CompletableFuture<List<GenericIssueDetails>> checkFormsIssues();

    /**
     * Disables issues domain, prevents further issues from being reported to the
     * client.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables issues domain, sends the issues collected so far to the client by
     * means of the issueAdded event.
     */
    CompletableFuture<Void> enable();

    /**
     * Returns the response body and size if it were re-encoded with the specified
     * settings. Only applies to images.
     *
     * @param requestId
     *            Identifier of the network request to get content for.
     * @param encoding
     *            The encoding to use.
     *
     * @return GetEncodedResponseResult
     */
    CompletableFuture<GetEncodedResponseResult> getEncodedResponse(String requestId, Encoding encoding);

    /**
     * Returns the response body and size if it were re-encoded with the specified
     * settings. Only applies to images.
     *
     * @param requestId
     *            Identifier of the network request to get content for.
     * @param encoding
     *            The encoding to use.
     * @param quality
     *            The quality of the encoding (0-1). (defaults to 1)
     * @param sizeOnly
     *            Whether to only return the size information (defaults to false).
     *
     * @return GetEncodedResponseResult
     */
    CompletableFuture<GetEncodedResponseResult> getEncodedResponse(
            String requestId, Encoding encoding, @Optional Double quality, @Optional Boolean sizeOnly);
}
