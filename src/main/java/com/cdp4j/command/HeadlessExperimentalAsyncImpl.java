// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.headlessexperimental.BeginFrameResult;
import com.cdp4j.type.headlessexperimental.ScreenshotParams;
import java.util.concurrent.CompletableFuture;

class HeadlessExperimentalAsyncImpl extends ParameterizedCommandImpl<HeadlessExperimentalAsync>
        implements HeadlessExperimentalAsync {

    private static final CommandReturnType CRT_BEGIN_FRAME = new CommandReturnType(null, BeginFrameResult.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_BEGIN_FRAME_2 =
            new String[] {"frameTimeTicks", "interval", "noDisplayUpdates", "screenshot"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public HeadlessExperimentalAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<BeginFrameResult> beginFrame() {
        return (CompletableFuture<BeginFrameResult>) handler.invoke(
                this, DomainCommand.HeadlessExperimental_beginFrame, CRT_BEGIN_FRAME, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<BeginFrameResult> beginFrame(
            Double frameTimeTicks, Double interval, Boolean noDisplayUpdates, ScreenshotParams screenshot) {
        return (CompletableFuture<BeginFrameResult>) handler.invoke(
                this,
                DomainCommand.HeadlessExperimental_beginFrame,
                CRT_BEGIN_FRAME,
                PARAMS_BEGIN_FRAME_2,
                new Object[] {frameTimeTicks, interval, noDisplayUpdates, screenshot},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.HeadlessExperimental_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.HeadlessExperimental_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }
}
