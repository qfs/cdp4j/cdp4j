// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.ElementRelation;
import com.cdp4j.type.constant.IncludeWhitespace;
import com.cdp4j.type.dom.BoxModel;
import com.cdp4j.type.dom.CSSComputedStyleProperty;
import com.cdp4j.type.dom.DetachedElementInfo;
import com.cdp4j.type.dom.GetFrameOwnerResult;
import com.cdp4j.type.dom.GetNodeForLocationResult;
import com.cdp4j.type.dom.LogicalAxes;
import com.cdp4j.type.dom.Node;
import com.cdp4j.type.dom.PerformSearchResult;
import com.cdp4j.type.dom.PhysicalAxes;
import com.cdp4j.type.dom.Rect;
import com.cdp4j.type.runtime.RemoteObject;
import com.cdp4j.type.runtime.StackTrace;
import java.util.List;

class DOMImpl extends ParameterizedCommandImpl<DOM> implements DOM {

    private static final TypeReference<List<DetachedElementInfo>> LIST_DETACHEDELEMENTINFO =
            new TypeReference<List<DetachedElementInfo>>() {};
    private static final TypeReference<List<Integer>> LIST_INTEGER = new TypeReference<List<Integer>>() {};
    private static final TypeReference<List<List<Double>>> LIST_LIST_DOUBLE =
            new TypeReference<List<List<Double>>>() {};
    private static final TypeReference<List<Node>> LIST_NODE = new TypeReference<List<Node>>() {};
    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_COLLECT_CLASS_NAMES_FROM_SUBTREE =
            new CommandReturnType("classNames", List.class, LIST_STRING);
    private static final CommandReturnType CRT_COPY_TO = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_DESCRIBE_NODE = new CommandReturnType("node", Node.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISCARD_SEARCH_RESULTS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_FOCUS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ANCHOR_ELEMENT =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_ATTRIBUTES =
            new CommandReturnType("attributes", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_BOX_MODEL = new CommandReturnType("model", BoxModel.class, null);
    private static final CommandReturnType CRT_GET_CONTAINER_FOR_NODE =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_CONTENT_QUADS =
            new CommandReturnType("quads", List.class, LIST_LIST_DOUBLE);
    private static final CommandReturnType CRT_GET_DETACHED_DOM_NODES =
            new CommandReturnType("detachedNodes", List.class, LIST_DETACHEDELEMENTINFO);
    private static final CommandReturnType CRT_GET_DOCUMENT = new CommandReturnType("root", Node.class, null);
    private static final CommandReturnType CRT_GET_ELEMENT_BY_RELATION =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_FILE_INFO = new CommandReturnType("path", String.class, null);
    private static final CommandReturnType CRT_GET_FLATTENED_DOCUMENT =
            new CommandReturnType("nodes", List.class, LIST_NODE);
    private static final CommandReturnType CRT_GET_FRAME_OWNER =
            new CommandReturnType(null, GetFrameOwnerResult.class, null);
    private static final CommandReturnType CRT_GET_NODES_FOR_SUBTREE_BY_STYLE =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_GET_NODE_FOR_LOCATION =
            new CommandReturnType(null, GetNodeForLocationResult.class, null);
    private static final CommandReturnType CRT_GET_NODE_STACK_TRACES =
            new CommandReturnType("creation", StackTrace.class, null);
    private static final CommandReturnType CRT_GET_OUTER_HT_ML = new CommandReturnType("outerHTML", String.class, null);
    private static final CommandReturnType CRT_GET_QUERYING_DESCENDANTS_FOR_CONTAINER =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_GET_RELAYOUT_BOUNDARY =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_SEARCH_RESULTS =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_GET_TOP_LAYER_ELEMENTS =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_HIDE_HIGHLIGHT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_NODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_RECT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_MARK_UNDOABLE_STATE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_MOVE_TO = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_PERFORM_SEARCH =
            new CommandReturnType(null, PerformSearchResult.class, null);
    private static final CommandReturnType CRT_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_PUSH_NODE_BY_PATH_TO_FRONTEND =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_QUERY_SELECTOR = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_QUERY_SELECTOR_ALL =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_REDO = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_ATTRIBUTE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_NODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REQUEST_CHILD_NODES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REQUEST_NODE = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_RESOLVE_NODE = new CommandReturnType("object", RemoteObject.class, null);
    private static final CommandReturnType CRT_SCROLL_INTO_VIEW_IF_NEEDED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ATTRIBUTES_AS_TEXT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ATTRIBUTE_VALUE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_FILE_INPUT_FILES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INSPECTED_NODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_NODE_NAME = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_SET_NODE_STACK_TRACES_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_NODE_VALUE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_OUTER_HT_ML = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNDO = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_COLLECT_CLASS_NAMES_FROM_SUBTREE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_COPY_TO_1 = new String[] {"nodeId", "targetNodeId"};
    private static final String[] PARAMS_COPY_TO_2 = new String[] {"nodeId", "targetNodeId", "insertBeforeNodeId"};
    private static final String[] PARAMS_DESCRIBE_NODE_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "depth", "pierce"};
    private static final String[] PARAMS_DISCARD_SEARCH_RESULTS_1 = new String[] {"searchId"};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"includeWhitespace"};
    private static final String[] PARAMS_FOCUS_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_ANCHOR_ELEMENT_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_ANCHOR_ELEMENT_2 = new String[] {"nodeId", "anchorSpecifier"};
    private static final String[] PARAMS_GET_ATTRIBUTES_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_BOX_MODEL_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_CONTAINER_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_CONTAINER_FOR_NODE_2 =
            new String[] {"nodeId", "containerName", "physicalAxes", "logicalAxes", "queriesScrollState"};
    private static final String[] PARAMS_GET_CONTENT_QUADS_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_DOCUMENT_2 = new String[] {"depth", "pierce"};
    private static final String[] PARAMS_GET_ELEMENT_BY_RELATION_1 = new String[] {"nodeId", "relation"};
    private static final String[] PARAMS_GET_FILE_INFO_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_FLATTENED_DOCUMENT_2 = new String[] {"depth", "pierce"};
    private static final String[] PARAMS_GET_FRAME_OWNER_1 = new String[] {"frameId"};
    private static final String[] PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_1 = new String[] {"nodeId", "computedStyles"};
    private static final String[] PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_2 =
            new String[] {"nodeId", "computedStyles", "pierce"};
    private static final String[] PARAMS_GET_NODE_FOR_LOCATION_1 = new String[] {"x", "y"};
    private static final String[] PARAMS_GET_NODE_FOR_LOCATION_2 =
            new String[] {"x", "y", "includeUserAgentShadowDOM", "ignorePointerEventsNone"};
    private static final String[] PARAMS_GET_NODE_STACK_TRACES_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_OUTER_HT_ML_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_QUERYING_DESCENDANTS_FOR_CONTAINER_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_RELAYOUT_BOUNDARY_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_SEARCH_RESULTS_1 = new String[] {"searchId", "fromIndex", "toIndex"};
    private static final String[] PARAMS_MOVE_TO_1 = new String[] {"nodeId", "targetNodeId"};
    private static final String[] PARAMS_MOVE_TO_2 = new String[] {"nodeId", "targetNodeId", "insertBeforeNodeId"};
    private static final String[] PARAMS_PERFORM_SEARCH_1 = new String[] {"query"};
    private static final String[] PARAMS_PERFORM_SEARCH_2 = new String[] {"query", "includeUserAgentShadowDOM"};
    private static final String[] PARAMS_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND_1 = new String[] {"backendNodeIds"};
    private static final String[] PARAMS_PUSH_NODE_BY_PATH_TO_FRONTEND_1 = new String[] {"path"};
    private static final String[] PARAMS_QUERY_SELECTOR_1 = new String[] {"nodeId", "selector"};
    private static final String[] PARAMS_QUERY_SELECTOR_ALL_1 = new String[] {"nodeId", "selector"};
    private static final String[] PARAMS_REMOVE_ATTRIBUTE_1 = new String[] {"nodeId", "name"};
    private static final String[] PARAMS_REMOVE_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_REQUEST_CHILD_NODES_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_REQUEST_CHILD_NODES_2 = new String[] {"nodeId", "depth", "pierce"};
    private static final String[] PARAMS_REQUEST_NODE_1 = new String[] {"objectId"};
    private static final String[] PARAMS_RESOLVE_NODE_2 =
            new String[] {"nodeId", "backendNodeId", "objectGroup", "executionContextId"};
    private static final String[] PARAMS_SCROLL_INTO_VIEW_IF_NEEDED_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "rect"};
    private static final String[] PARAMS_SET_ATTRIBUTES_AS_TEXT_1 = new String[] {"nodeId", "text"};
    private static final String[] PARAMS_SET_ATTRIBUTES_AS_TEXT_2 = new String[] {"nodeId", "text", "name"};
    private static final String[] PARAMS_SET_ATTRIBUTE_VALUE_1 = new String[] {"nodeId", "name", "value"};
    private static final String[] PARAMS_SET_FILE_INPUT_FILES_1 = new String[] {"files"};
    private static final String[] PARAMS_SET_FILE_INPUT_FILES_2 =
            new String[] {"files", "nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_SET_INSPECTED_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_SET_NODE_NAME_1 = new String[] {"nodeId", "name"};
    private static final String[] PARAMS_SET_NODE_STACK_TRACES_ENABLED_1 = new String[] {"enable"};
    private static final String[] PARAMS_SET_NODE_VALUE_1 = new String[] {"nodeId", "value"};
    private static final String[] PARAMS_SET_OUTER_HT_ML_1 = new String[] {"nodeId", "outerHTML"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DOMImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> collectClassNamesFromSubtree(Integer nodeId) {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.DOM_collectClassNamesFromSubtree,
                CRT_COLLECT_CLASS_NAMES_FROM_SUBTREE,
                PARAMS_COLLECT_CLASS_NAMES_FROM_SUBTREE_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    public Integer copyTo(Integer nodeId, Integer targetNodeId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_copyTo,
                CRT_COPY_TO,
                PARAMS_COPY_TO_1,
                new Object[] {nodeId, targetNodeId},
                true);
    }

    @Override
    public Integer copyTo(Integer nodeId, Integer targetNodeId, Integer insertBeforeNodeId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_copyTo,
                CRT_COPY_TO,
                PARAMS_COPY_TO_2,
                new Object[] {nodeId, targetNodeId, insertBeforeNodeId},
                true);
    }

    @Override
    public Node describeNode() {
        return (Node)
                handler.invoke(this, DomainCommand.DOM_describeNode, CRT_DESCRIBE_NODE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public Node describeNode(Integer nodeId, Integer backendNodeId, String objectId, Integer depth, Boolean pierce) {
        return (Node) handler.invoke(
                this,
                DomainCommand.DOM_describeNode,
                CRT_DESCRIBE_NODE,
                PARAMS_DESCRIBE_NODE_2,
                new Object[] {nodeId, backendNodeId, objectId, depth, pierce},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.DOM_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void discardSearchResults(String searchId) {
        handler.invoke(
                this,
                DomainCommand.DOM_discardSearchResults,
                CRT_DISCARD_SEARCH_RESULTS,
                PARAMS_DISCARD_SEARCH_RESULTS_1,
                new Object[] {searchId},
                true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.DOM_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable(IncludeWhitespace includeWhitespace) {
        handler.invoke(
                this, DomainCommand.DOM_enable, CRT_ENABLE, PARAMS_ENABLE_2, new Object[] {includeWhitespace}, true);
    }

    @Override
    public void focus() {
        handler.invoke(this, DomainCommand.DOM_focus, CRT_FOCUS, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void focus(Integer nodeId, Integer backendNodeId, String objectId) {
        handler.invoke(
                this,
                DomainCommand.DOM_focus,
                CRT_FOCUS,
                PARAMS_FOCUS_2,
                new Object[] {nodeId, backendNodeId, objectId},
                true);
    }

    @Override
    public Integer getAnchorElement(Integer nodeId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_getAnchorElement,
                CRT_GET_ANCHOR_ELEMENT,
                PARAMS_GET_ANCHOR_ELEMENT_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    public Integer getAnchorElement(Integer nodeId, String anchorSpecifier) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_getAnchorElement,
                CRT_GET_ANCHOR_ELEMENT,
                PARAMS_GET_ANCHOR_ELEMENT_2,
                new Object[] {nodeId, anchorSpecifier},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> getAttributes(Integer nodeId) {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.DOM_getAttributes,
                CRT_GET_ATTRIBUTES,
                PARAMS_GET_ATTRIBUTES_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    public BoxModel getBoxModel() {
        return (BoxModel)
                handler.invoke(this, DomainCommand.DOM_getBoxModel, CRT_GET_BOX_MODEL, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public BoxModel getBoxModel(Integer nodeId, Integer backendNodeId, String objectId) {
        return (BoxModel) handler.invoke(
                this,
                DomainCommand.DOM_getBoxModel,
                CRT_GET_BOX_MODEL,
                PARAMS_GET_BOX_MODEL_2,
                new Object[] {nodeId, backendNodeId, objectId},
                true);
    }

    @Override
    public Integer getContainerForNode(Integer nodeId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_getContainerForNode,
                CRT_GET_CONTAINER_FOR_NODE,
                PARAMS_GET_CONTAINER_FOR_NODE_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    public Integer getContainerForNode(
            Integer nodeId,
            String containerName,
            PhysicalAxes physicalAxes,
            LogicalAxes logicalAxes,
            Boolean queriesScrollState) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_getContainerForNode,
                CRT_GET_CONTAINER_FOR_NODE,
                PARAMS_GET_CONTAINER_FOR_NODE_2,
                new Object[] {nodeId, containerName, physicalAxes, logicalAxes, queriesScrollState},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<List<Double>> getContentQuads() {
        return (List<List<Double>>) handler.invoke(
                this, DomainCommand.DOM_getContentQuads, CRT_GET_CONTENT_QUADS, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<List<Double>> getContentQuads(Integer nodeId, Integer backendNodeId, String objectId) {
        return (List<List<Double>>) handler.invoke(
                this,
                DomainCommand.DOM_getContentQuads,
                CRT_GET_CONTENT_QUADS,
                PARAMS_GET_CONTENT_QUADS_2,
                new Object[] {nodeId, backendNodeId, objectId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<DetachedElementInfo> getDetachedDomNodes() {
        return (List<DetachedElementInfo>) handler.invoke(
                this,
                DomainCommand.DOM_getDetachedDomNodes,
                CRT_GET_DETACHED_DOM_NODES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public Node getDocument() {
        return (Node)
                handler.invoke(this, DomainCommand.DOM_getDocument, CRT_GET_DOCUMENT, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public Node getDocument(Integer depth, Boolean pierce) {
        return (Node) handler.invoke(
                this,
                DomainCommand.DOM_getDocument,
                CRT_GET_DOCUMENT,
                PARAMS_GET_DOCUMENT_2,
                new Object[] {depth, pierce},
                true);
    }

    @Override
    public Integer getElementByRelation(Integer nodeId, ElementRelation relation) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_getElementByRelation,
                CRT_GET_ELEMENT_BY_RELATION,
                PARAMS_GET_ELEMENT_BY_RELATION_1,
                new Object[] {nodeId, relation},
                true);
    }

    @Override
    public String getFileInfo(String objectId) {
        return (String) handler.invoke(
                this,
                DomainCommand.DOM_getFileInfo,
                CRT_GET_FILE_INFO,
                PARAMS_GET_FILE_INFO_1,
                new Object[] {objectId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Node> getFlattenedDocument() {
        return (List<Node>) handler.invoke(
                this,
                DomainCommand.DOM_getFlattenedDocument,
                CRT_GET_FLATTENED_DOCUMENT,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Node> getFlattenedDocument(Integer depth, Boolean pierce) {
        return (List<Node>) handler.invoke(
                this,
                DomainCommand.DOM_getFlattenedDocument,
                CRT_GET_FLATTENED_DOCUMENT,
                PARAMS_GET_FLATTENED_DOCUMENT_2,
                new Object[] {depth, pierce},
                true);
    }

    @Override
    public GetFrameOwnerResult getFrameOwner(String frameId) {
        return (GetFrameOwnerResult) handler.invoke(
                this,
                DomainCommand.DOM_getFrameOwner,
                CRT_GET_FRAME_OWNER,
                PARAMS_GET_FRAME_OWNER_1,
                new Object[] {frameId},
                true);
    }

    @Override
    public GetNodeForLocationResult getNodeForLocation(Integer x, Integer y) {
        return (GetNodeForLocationResult) handler.invoke(
                this,
                DomainCommand.DOM_getNodeForLocation,
                CRT_GET_NODE_FOR_LOCATION,
                PARAMS_GET_NODE_FOR_LOCATION_1,
                new Object[] {x, y},
                true);
    }

    @Override
    public GetNodeForLocationResult getNodeForLocation(
            Integer x, Integer y, Boolean includeUserAgentShadowDOM, Boolean ignorePointerEventsNone) {
        return (GetNodeForLocationResult) handler.invoke(
                this,
                DomainCommand.DOM_getNodeForLocation,
                CRT_GET_NODE_FOR_LOCATION,
                PARAMS_GET_NODE_FOR_LOCATION_2,
                new Object[] {x, y, includeUserAgentShadowDOM, ignorePointerEventsNone},
                true);
    }

    @Override
    public StackTrace getNodeStackTraces(Integer nodeId) {
        return (StackTrace) handler.invoke(
                this,
                DomainCommand.DOM_getNodeStackTraces,
                CRT_GET_NODE_STACK_TRACES,
                PARAMS_GET_NODE_STACK_TRACES_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Integer> getNodesForSubtreeByStyle(Integer nodeId, List<CSSComputedStyleProperty> computedStyles) {
        return (List<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getNodesForSubtreeByStyle,
                CRT_GET_NODES_FOR_SUBTREE_BY_STYLE,
                PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_1,
                new Object[] {nodeId, computedStyles},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Integer> getNodesForSubtreeByStyle(
            Integer nodeId, List<CSSComputedStyleProperty> computedStyles, Boolean pierce) {
        return (List<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getNodesForSubtreeByStyle,
                CRT_GET_NODES_FOR_SUBTREE_BY_STYLE,
                PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_2,
                new Object[] {nodeId, computedStyles, pierce},
                true);
    }

    @Override
    public String getOuterHTML() {
        return (String) handler.invoke(
                this, DomainCommand.DOM_getOuterHTML, CRT_GET_OUTER_HT_ML, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public String getOuterHTML(Integer nodeId, Integer backendNodeId, String objectId) {
        return (String) handler.invoke(
                this,
                DomainCommand.DOM_getOuterHTML,
                CRT_GET_OUTER_HT_ML,
                PARAMS_GET_OUTER_HT_ML_2,
                new Object[] {nodeId, backendNodeId, objectId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Integer> getQueryingDescendantsForContainer(Integer nodeId) {
        return (List<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getQueryingDescendantsForContainer,
                CRT_GET_QUERYING_DESCENDANTS_FOR_CONTAINER,
                PARAMS_GET_QUERYING_DESCENDANTS_FOR_CONTAINER_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    public Integer getRelayoutBoundary(Integer nodeId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_getRelayoutBoundary,
                CRT_GET_RELAYOUT_BOUNDARY,
                PARAMS_GET_RELAYOUT_BOUNDARY_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Integer> getSearchResults(String searchId, Integer fromIndex, Integer toIndex) {
        return (List<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getSearchResults,
                CRT_GET_SEARCH_RESULTS,
                PARAMS_GET_SEARCH_RESULTS_1,
                new Object[] {searchId, fromIndex, toIndex},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Integer> getTopLayerElements() {
        return (List<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getTopLayerElements,
                CRT_GET_TOP_LAYER_ELEMENTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void hideHighlight() {
        handler.invoke(this, DomainCommand.DOM_hideHighlight, CRT_HIDE_HIGHLIGHT, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void highlightNode() {
        handler.invoke(this, DomainCommand.DOM_highlightNode, CRT_HIGHLIGHT_NODE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void highlightRect() {
        handler.invoke(this, DomainCommand.DOM_highlightRect, CRT_HIGHLIGHT_RECT, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void markUndoableState() {
        handler.invoke(
                this, DomainCommand.DOM_markUndoableState, CRT_MARK_UNDOABLE_STATE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public Integer moveTo(Integer nodeId, Integer targetNodeId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_moveTo,
                CRT_MOVE_TO,
                PARAMS_MOVE_TO_1,
                new Object[] {nodeId, targetNodeId},
                true);
    }

    @Override
    public Integer moveTo(Integer nodeId, Integer targetNodeId, Integer insertBeforeNodeId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_moveTo,
                CRT_MOVE_TO,
                PARAMS_MOVE_TO_2,
                new Object[] {nodeId, targetNodeId, insertBeforeNodeId},
                true);
    }

    @Override
    public PerformSearchResult performSearch(String query) {
        return (PerformSearchResult) handler.invoke(
                this,
                DomainCommand.DOM_performSearch,
                CRT_PERFORM_SEARCH,
                PARAMS_PERFORM_SEARCH_1,
                new Object[] {query},
                true);
    }

    @Override
    public PerformSearchResult performSearch(String query, Boolean includeUserAgentShadowDOM) {
        return (PerformSearchResult) handler.invoke(
                this,
                DomainCommand.DOM_performSearch,
                CRT_PERFORM_SEARCH,
                PARAMS_PERFORM_SEARCH_2,
                new Object[] {query, includeUserAgentShadowDOM},
                true);
    }

    @Override
    public Integer pushNodeByPathToFrontend(String path) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_pushNodeByPathToFrontend,
                CRT_PUSH_NODE_BY_PATH_TO_FRONTEND,
                PARAMS_PUSH_NODE_BY_PATH_TO_FRONTEND_1,
                new Object[] {path},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Integer> pushNodesByBackendIdsToFrontend(List<Integer> backendNodeIds) {
        return (List<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_pushNodesByBackendIdsToFrontend,
                CRT_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND,
                PARAMS_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND_1,
                new Object[] {backendNodeIds},
                true);
    }

    @Override
    public Integer querySelector(Integer nodeId, String selector) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_querySelector,
                CRT_QUERY_SELECTOR,
                PARAMS_QUERY_SELECTOR_1,
                new Object[] {nodeId, selector},
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Integer> querySelectorAll(Integer nodeId, String selector) {
        return (List<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_querySelectorAll,
                CRT_QUERY_SELECTOR_ALL,
                PARAMS_QUERY_SELECTOR_ALL_1,
                new Object[] {nodeId, selector},
                true);
    }

    @Override
    public void redo() {
        handler.invoke(this, DomainCommand.DOM_redo, CRT_REDO, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void removeAttribute(Integer nodeId, String name) {
        handler.invoke(
                this,
                DomainCommand.DOM_removeAttribute,
                CRT_REMOVE_ATTRIBUTE,
                PARAMS_REMOVE_ATTRIBUTE_1,
                new Object[] {nodeId, name},
                true);
    }

    @Override
    public void removeNode(Integer nodeId) {
        handler.invoke(
                this, DomainCommand.DOM_removeNode, CRT_REMOVE_NODE, PARAMS_REMOVE_NODE_1, new Object[] {nodeId}, true);
    }

    @Override
    public void requestChildNodes(Integer nodeId) {
        handler.invoke(
                this,
                DomainCommand.DOM_requestChildNodes,
                CRT_REQUEST_CHILD_NODES,
                PARAMS_REQUEST_CHILD_NODES_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    public void requestChildNodes(Integer nodeId, Integer depth, Boolean pierce) {
        handler.invoke(
                this,
                DomainCommand.DOM_requestChildNodes,
                CRT_REQUEST_CHILD_NODES,
                PARAMS_REQUEST_CHILD_NODES_2,
                new Object[] {nodeId, depth, pierce},
                true);
    }

    @Override
    public Integer requestNode(String objectId) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_requestNode,
                CRT_REQUEST_NODE,
                PARAMS_REQUEST_NODE_1,
                new Object[] {objectId},
                true);
    }

    @Override
    public RemoteObject resolveNode() {
        return (RemoteObject)
                handler.invoke(this, DomainCommand.DOM_resolveNode, CRT_RESOLVE_NODE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public RemoteObject resolveNode(
            Integer nodeId, Integer backendNodeId, String objectGroup, Integer executionContextId) {
        return (RemoteObject) handler.invoke(
                this,
                DomainCommand.DOM_resolveNode,
                CRT_RESOLVE_NODE,
                PARAMS_RESOLVE_NODE_2,
                new Object[] {nodeId, backendNodeId, objectGroup, executionContextId},
                true);
    }

    @Override
    public void scrollIntoViewIfNeeded() {
        handler.invoke(
                this,
                DomainCommand.DOM_scrollIntoViewIfNeeded,
                CRT_SCROLL_INTO_VIEW_IF_NEEDED,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void scrollIntoViewIfNeeded(Integer nodeId, Integer backendNodeId, String objectId, Rect rect) {
        handler.invoke(
                this,
                DomainCommand.DOM_scrollIntoViewIfNeeded,
                CRT_SCROLL_INTO_VIEW_IF_NEEDED,
                PARAMS_SCROLL_INTO_VIEW_IF_NEEDED_2,
                new Object[] {nodeId, backendNodeId, objectId, rect},
                true);
    }

    @Override
    public void setAttributeValue(Integer nodeId, String name, String value) {
        handler.invoke(
                this,
                DomainCommand.DOM_setAttributeValue,
                CRT_SET_ATTRIBUTE_VALUE,
                PARAMS_SET_ATTRIBUTE_VALUE_1,
                new Object[] {nodeId, name, value},
                true);
    }

    @Override
    public void setAttributesAsText(Integer nodeId, String text) {
        handler.invoke(
                this,
                DomainCommand.DOM_setAttributesAsText,
                CRT_SET_ATTRIBUTES_AS_TEXT,
                PARAMS_SET_ATTRIBUTES_AS_TEXT_1,
                new Object[] {nodeId, text},
                true);
    }

    @Override
    public void setAttributesAsText(Integer nodeId, String text, String name) {
        handler.invoke(
                this,
                DomainCommand.DOM_setAttributesAsText,
                CRT_SET_ATTRIBUTES_AS_TEXT,
                PARAMS_SET_ATTRIBUTES_AS_TEXT_2,
                new Object[] {nodeId, text, name},
                true);
    }

    @Override
    public void setFileInputFiles(List<String> files) {
        handler.invoke(
                this,
                DomainCommand.DOM_setFileInputFiles,
                CRT_SET_FILE_INPUT_FILES,
                PARAMS_SET_FILE_INPUT_FILES_1,
                new Object[] {files},
                true);
    }

    @Override
    public void setFileInputFiles(List<String> files, Integer nodeId, Integer backendNodeId, String objectId) {
        handler.invoke(
                this,
                DomainCommand.DOM_setFileInputFiles,
                CRT_SET_FILE_INPUT_FILES,
                PARAMS_SET_FILE_INPUT_FILES_2,
                new Object[] {files, nodeId, backendNodeId, objectId},
                true);
    }

    @Override
    public void setInspectedNode(Integer nodeId) {
        handler.invoke(
                this,
                DomainCommand.DOM_setInspectedNode,
                CRT_SET_INSPECTED_NODE,
                PARAMS_SET_INSPECTED_NODE_1,
                new Object[] {nodeId},
                true);
    }

    @Override
    public Integer setNodeName(Integer nodeId, String name) {
        return (Integer) handler.invoke(
                this,
                DomainCommand.DOM_setNodeName,
                CRT_SET_NODE_NAME,
                PARAMS_SET_NODE_NAME_1,
                new Object[] {nodeId, name},
                true);
    }

    @Override
    public void setNodeStackTracesEnabled(Boolean enable) {
        handler.invoke(
                this,
                DomainCommand.DOM_setNodeStackTracesEnabled,
                CRT_SET_NODE_STACK_TRACES_ENABLED,
                PARAMS_SET_NODE_STACK_TRACES_ENABLED_1,
                new Object[] {enable},
                true);
    }

    @Override
    public void setNodeValue(Integer nodeId, String value) {
        handler.invoke(
                this,
                DomainCommand.DOM_setNodeValue,
                CRT_SET_NODE_VALUE,
                PARAMS_SET_NODE_VALUE_1,
                new Object[] {nodeId, value},
                true);
    }

    @Override
    public void setOuterHTML(Integer nodeId, String outerHTML) {
        handler.invoke(
                this,
                DomainCommand.DOM_setOuterHTML,
                CRT_SET_OUTER_HT_ML,
                PARAMS_SET_OUTER_HT_ML_1,
                new Object[] {nodeId, outerHTML},
                true);
    }

    @Override
    public void undo() {
        handler.invoke(this, DomainCommand.DOM_undo, CRT_UNDO, EMPTY_ARGS, EMPTY_VALUES, true);
    }
}
