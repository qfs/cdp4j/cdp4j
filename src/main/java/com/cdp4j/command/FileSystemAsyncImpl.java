// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.filesystem.BucketFileSystemLocator;
import com.cdp4j.type.filesystem.Directory;
import java.util.concurrent.CompletableFuture;

class FileSystemAsyncImpl extends ParameterizedCommandImpl<FileSystemAsync> implements FileSystemAsync {

    private static final CommandReturnType CRT_GET_DIRECTORY =
            new CommandReturnType("directory", Directory.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_DIRECTORY_1 = new String[] {"bucketFileSystemLocator"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public FileSystemAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Directory> getDirectory(BucketFileSystemLocator bucketFileSystemLocator) {
        return (CompletableFuture<Directory>) handler.invoke(
                this,
                DomainCommand.FileSystem_getDirectory,
                CRT_GET_DIRECTORY,
                PARAMS_GET_DIRECTORY_1,
                new Object[] {bucketFileSystemLocator},
                false);
    }
}
