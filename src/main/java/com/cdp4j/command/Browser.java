// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.browser.Bounds;
import com.cdp4j.type.browser.BrowserCommandId;
import com.cdp4j.type.browser.GetVersionResult;
import com.cdp4j.type.browser.GetWindowForTargetResult;
import com.cdp4j.type.browser.Histogram;
import com.cdp4j.type.browser.PermissionDescriptor;
import com.cdp4j.type.browser.PermissionSetting;
import com.cdp4j.type.browser.PermissionType;
import com.cdp4j.type.constant.DownloadBehavior;
import java.util.List;

/**
 * The Browser domain defines methods and events for browser managing.
 */
public interface Browser extends ParameterizedCommand<Browser> {
    /**
     * Allows a site to use privacy sandbox features that require enrollment
     * without the site actually being enrolled. Only supported on page targets.
     *
     */
    void addPrivacySandboxEnrollmentOverride(String url);

    /**
     * Cancel a download if in progress
     *
     * @param guid Global unique identifier of the download.
     */
    @Experimental
    void cancelDownload(String guid);

    /**
     * Cancel a download if in progress
     *
     * @param guid Global unique identifier of the download.
     * @param browserContextId BrowserContext to perform the action in. When omitted, default browser context is used.
     */
    @Experimental
    void cancelDownload(String guid, @Optional String browserContextId);

    /**
     * Close browser gracefully.
     */
    void close();

    /**
     * Crashes browser on the main thread.
     */
    @Experimental
    void crash();

    /**
     * Crashes GPU process.
     */
    @Experimental
    void crashGpuProcess();

    /**
     * Invoke custom browser commands used by telemetry.
     *
     */
    @Experimental
    void executeBrowserCommand(BrowserCommandId commandId);

    /**
     * Returns the command line switches for the browser process if, and only if
     * --enable-automation is on the commandline.
     *
     * @return Commandline parameters
     */
    @Experimental
    List<String> getBrowserCommandLine();

    /**
     * Get a Chrome histogram by name.
     *
     * @param name Requested histogram name.
     *
     * @return Histogram.
     */
    @Experimental
    Histogram getHistogram(String name);

    /**
     * Get a Chrome histogram by name.
     *
     * @param name Requested histogram name.
     * @param delta If true, retrieve delta since last delta call.
     *
     * @return Histogram.
     */
    @Experimental
    Histogram getHistogram(String name, @Optional Boolean delta);

    /**
     * Get Chrome histograms.
     *
     * @return Histograms.
     */
    @Experimental
    List<Histogram> getHistograms();

    /**
     * Get Chrome histograms.
     *
     * @param query Requested substring in name. Only histograms which have query as a
     * substring in their name are extracted. An empty or absent query returns
     * all histograms.
     * @param delta If true, retrieve delta since last delta call.
     *
     * @return Histograms.
     */
    @Experimental
    List<Histogram> getHistograms(@Optional String query, @Optional Boolean delta);

    /**
     * Returns version information.
     *
     * @return GetVersionResult
     */
    GetVersionResult getVersion();

    /**
     * Get position and size of the browser window.
     *
     * @param windowId Browser window id.
     *
     * @return Bounds information of the window. When window state is 'minimized', the restored window
     * position and size are returned.
     */
    @Experimental
    Bounds getWindowBounds(Integer windowId);

    /**
     * Get the browser window that contains the devtools target.
     *
     * @return GetWindowForTargetResult
     */
    @Experimental
    GetWindowForTargetResult getWindowForTarget();

    /**
     * Get the browser window that contains the devtools target.
     *
     * @param targetId Devtools agent host id. If called as a part of the session, associated targetId is used.
     *
     * @return GetWindowForTargetResult
     */
    @Experimental
    GetWindowForTargetResult getWindowForTarget(@Optional String targetId);

    /**
     * Grant specific permissions to the given origin and reject all others.
     *
     */
    @Experimental
    void grantPermissions(PermissionType permissions);

    /**
     * Grant specific permissions to the given origin and reject all others.
     *
     * @param origin Origin the permission applies to, all origins if not specified.
     * @param browserContextId BrowserContext to override permissions. When omitted, default browser context is used.
     */
    @Experimental
    void grantPermissions(PermissionType permissions, @Optional String origin, @Optional String browserContextId);

    /**
     * Reset all permission management for all origins.
     */
    void resetPermissions();

    /**
     * Reset all permission management for all origins.
     *
     * @param browserContextId BrowserContext to reset permissions. When omitted, default browser context is used.
     */
    void resetPermissions(@Optional String browserContextId);

    /**
     * Set dock tile details, platform-specific.
     */
    @Experimental
    void setDockTile();

    /**
     * Set dock tile details, platform-specific.
     *
     * @param image Png encoded image. (Encoded as a base64 string when passed over JSON)
     */
    @Experimental
    void setDockTile(@Optional String badgeLabel, @Optional String image);

    /**
     * Set the behavior when downloading a file.
     *
     * @param behavior Whether to allow all or deny all download requests, or use default Chrome behavior if
     * available (otherwise deny). |allowAndName| allows download and names files according to
     * their download guids.
     */
    @Experimental
    void setDownloadBehavior(DownloadBehavior behavior);

    /**
     * Set the behavior when downloading a file.
     *
     * @param behavior Whether to allow all or deny all download requests, or use default Chrome behavior if
     * available (otherwise deny). |allowAndName| allows download and names files according to
     * their download guids.
     * @param browserContextId BrowserContext to set download behavior. When omitted, default browser context is used.
     * @param downloadPath The default path to save downloaded files to. This is required if behavior is set to 'allow'
     * or 'allowAndName'.
     * @param eventsEnabled Whether to emit download events (defaults to false).
     */
    @Experimental
    void setDownloadBehavior(
            DownloadBehavior behavior,
            @Optional String browserContextId,
            @Optional String downloadPath,
            @Optional Boolean eventsEnabled);

    /**
     * Set permission settings for given origin.
     *
     * @param permission Descriptor of permission to override.
     * @param setting Setting of the permission.
     */
    @Experimental
    void setPermission(PermissionDescriptor permission, PermissionSetting setting);

    /**
     * Set permission settings for given origin.
     *
     * @param permission Descriptor of permission to override.
     * @param setting Setting of the permission.
     * @param origin Origin the permission applies to, all origins if not specified.
     * @param browserContextId Context to override. When omitted, default browser context is used.
     */
    @Experimental
    void setPermission(
            PermissionDescriptor permission,
            PermissionSetting setting,
            @Optional String origin,
            @Optional String browserContextId);

    /**
     * Set position and/or size of the browser window.
     *
     * @param windowId Browser window id.
     * @param bounds New window bounds. The 'minimized', 'maximized' and 'fullscreen' states cannot be combined
     * with 'left', 'top', 'width' or 'height'. Leaves unspecified fields unchanged.
     */
    @Experimental
    void setWindowBounds(Integer windowId, Bounds bounds);
}
