// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.indexeddb.DatabaseWithObjectStores;
import com.cdp4j.type.indexeddb.GetMetadataResult;
import com.cdp4j.type.indexeddb.KeyRange;
import com.cdp4j.type.indexeddb.RequestDataResult;
import com.cdp4j.type.storage.StorageBucket;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class IndexedDBAsyncImpl extends ParameterizedCommandImpl<IndexedDBAsync> implements IndexedDBAsync {

    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_CLEAR_OBJECT_STORE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_DATABASE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DELETE_OBJECT_STORE_ENTRIES =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_METADATA =
            new CommandReturnType(null, GetMetadataResult.class, null);
    private static final CommandReturnType CRT_REQUEST_DATA =
            new CommandReturnType(null, RequestDataResult.class, null);
    private static final CommandReturnType CRT_REQUEST_DATABASE =
            new CommandReturnType("databaseWithObjectStores", DatabaseWithObjectStores.class, null);
    private static final CommandReturnType CRT_REQUEST_DATABASE_NAMES =
            new CommandReturnType("databaseNames", List.class, LIST_STRING);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CLEAR_OBJECT_STORE_1 = new String[] {"databaseName", "objectStoreName"};
    private static final String[] PARAMS_CLEAR_OBJECT_STORE_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName", "objectStoreName"};
    private static final String[] PARAMS_DELETE_DATABASE_1 = new String[] {"databaseName"};
    private static final String[] PARAMS_DELETE_DATABASE_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName"};
    private static final String[] PARAMS_DELETE_OBJECT_STORE_ENTRIES_1 =
            new String[] {"databaseName", "objectStoreName", "keyRange"};
    private static final String[] PARAMS_DELETE_OBJECT_STORE_ENTRIES_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName", "objectStoreName", "keyRange"
            };
    private static final String[] PARAMS_GET_METADATA_1 = new String[] {"databaseName", "objectStoreName"};
    private static final String[] PARAMS_GET_METADATA_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName", "objectStoreName"};
    private static final String[] PARAMS_REQUEST_DATABASE_1 = new String[] {"databaseName"};
    private static final String[] PARAMS_REQUEST_DATABASE_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket", "databaseName"};
    private static final String[] PARAMS_REQUEST_DATABASE_NAMES_2 =
            new String[] {"securityOrigin", "storageKey", "storageBucket"};
    private static final String[] PARAMS_REQUEST_DATA_1 =
            new String[] {"databaseName", "objectStoreName", "indexName", "skipCount", "pageSize"};
    private static final String[] PARAMS_REQUEST_DATA_2 = new String[] {
        "securityOrigin",
        "storageKey",
        "storageBucket",
        "databaseName",
        "objectStoreName",
        "indexName",
        "skipCount",
        "pageSize",
        "keyRange"
    };
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public IndexedDBAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearObjectStore(String databaseName, String objectStoreName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.IndexedDB_clearObjectStore,
                CRT_CLEAR_OBJECT_STORE,
                PARAMS_CLEAR_OBJECT_STORE_1,
                new Object[] {databaseName, objectStoreName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearObjectStore(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.IndexedDB_clearObjectStore,
                CRT_CLEAR_OBJECT_STORE,
                PARAMS_CLEAR_OBJECT_STORE_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName, objectStoreName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteDatabase(String databaseName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteDatabase,
                CRT_DELETE_DATABASE,
                PARAMS_DELETE_DATABASE_1,
                new Object[] {databaseName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteDatabase(
            String securityOrigin, String storageKey, StorageBucket storageBucket, String databaseName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteDatabase,
                CRT_DELETE_DATABASE,
                PARAMS_DELETE_DATABASE_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteObjectStoreEntries(
            String databaseName, String objectStoreName, KeyRange keyRange) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteObjectStoreEntries,
                CRT_DELETE_OBJECT_STORE_ENTRIES,
                PARAMS_DELETE_OBJECT_STORE_ENTRIES_1,
                new Object[] {databaseName, objectStoreName, keyRange},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> deleteObjectStoreEntries(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName,
            KeyRange keyRange) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.IndexedDB_deleteObjectStoreEntries,
                CRT_DELETE_OBJECT_STORE_ENTRIES,
                PARAMS_DELETE_OBJECT_STORE_ENTRIES_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName, objectStoreName, keyRange},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.IndexedDB_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.IndexedDB_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetMetadataResult> getMetadata(String databaseName, String objectStoreName) {
        return (CompletableFuture<GetMetadataResult>) handler.invoke(
                this,
                DomainCommand.IndexedDB_getMetadata,
                CRT_GET_METADATA,
                PARAMS_GET_METADATA_1,
                new Object[] {databaseName, objectStoreName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetMetadataResult> getMetadata(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName) {
        return (CompletableFuture<GetMetadataResult>) handler.invoke(
                this,
                DomainCommand.IndexedDB_getMetadata,
                CRT_GET_METADATA,
                PARAMS_GET_METADATA_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName, objectStoreName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RequestDataResult> requestData(
            String databaseName, String objectStoreName, String indexName, Integer skipCount, Integer pageSize) {
        return (CompletableFuture<RequestDataResult>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestData,
                CRT_REQUEST_DATA,
                PARAMS_REQUEST_DATA_1,
                new Object[] {databaseName, objectStoreName, indexName, skipCount, pageSize},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RequestDataResult> requestData(
            String securityOrigin,
            String storageKey,
            StorageBucket storageBucket,
            String databaseName,
            String objectStoreName,
            String indexName,
            Integer skipCount,
            Integer pageSize,
            KeyRange keyRange) {
        return (CompletableFuture<RequestDataResult>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestData,
                CRT_REQUEST_DATA,
                PARAMS_REQUEST_DATA_2,
                new Object[] {
                    securityOrigin,
                    storageKey,
                    storageBucket,
                    databaseName,
                    objectStoreName,
                    indexName,
                    skipCount,
                    pageSize,
                    keyRange
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<DatabaseWithObjectStores> requestDatabase(String databaseName) {
        return (CompletableFuture<DatabaseWithObjectStores>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabase,
                CRT_REQUEST_DATABASE,
                PARAMS_REQUEST_DATABASE_1,
                new Object[] {databaseName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<DatabaseWithObjectStores> requestDatabase(
            String securityOrigin, String storageKey, StorageBucket storageBucket, String databaseName) {
        return (CompletableFuture<DatabaseWithObjectStores>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabase,
                CRT_REQUEST_DATABASE,
                PARAMS_REQUEST_DATABASE_2,
                new Object[] {securityOrigin, storageKey, storageBucket, databaseName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> requestDatabaseNames() {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabaseNames,
                CRT_REQUEST_DATABASE_NAMES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> requestDatabaseNames(
            String securityOrigin, String storageKey, StorageBucket storageBucket) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.IndexedDB_requestDatabaseNames,
                CRT_REQUEST_DATABASE_NAMES,
                PARAMS_REQUEST_DATABASE_NAMES_2,
                new Object[] {securityOrigin, storageKey, storageBucket},
                false);
    }
}
