// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.filesystem.BucketFileSystemLocator;
import com.cdp4j.type.filesystem.Directory;

@Experimental
public interface FileSystem extends ParameterizedCommand<FileSystem> {
    Directory getDirectory(BucketFileSystemLocator bucketFileSystemLocator);
}
