// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.security.CertificateErrorAction;
import java.util.concurrent.CompletableFuture;

/**
 * Security
 */
public interface SecurityAsync extends ParameterizedCommand<SecurityAsync> {
    /**
     * Disables tracking security state changes.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables tracking security state changes.
     */
    CompletableFuture<Void> enable();

    /**
     * Handles a certificate error that fired a certificateError event.
     *
     * @param eventId
     *            The ID of the event.
     * @param action
     *            The action to take on the certificate error.
     */
    @Deprecated
    CompletableFuture<Void> handleCertificateError(Integer eventId, CertificateErrorAction action);

    /**
     * Enable/disable whether all certificate errors should be ignored.
     *
     * @param ignore
     *            If true, all certificate errors will be ignored.
     */
    CompletableFuture<Void> setIgnoreCertificateErrors(Boolean ignore);

    /**
     * Enable/disable overriding certificate errors. If enabled, all certificate
     * error events need to be handled by the DevTools client and should be answered
     * with handleCertificateError commands.
     *
     * @param override
     *            If true, certificate errors will be overridden.
     */
    @Deprecated
    CompletableFuture<Void> setOverrideCertificateErrors(Boolean override);
}
