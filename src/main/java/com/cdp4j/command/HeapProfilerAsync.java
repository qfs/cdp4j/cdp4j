// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.heapprofiler.SamplingHeapProfile;
import com.cdp4j.type.runtime.RemoteObject;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface HeapProfilerAsync extends ParameterizedCommand<HeapProfilerAsync> {
    /**
     * Enables console to refer to the node with given id via (see Command Line API
     * for more details functions).
     *
     * @param heapObjectId
     *            Heap snapshot object id to be accessible by means of x command
     *            line API.
     */
    CompletableFuture<Void> addInspectedHeapObject(String heapObjectId);

    CompletableFuture<Void> collectGarbage();

    CompletableFuture<Void> disable();

    CompletableFuture<Void> enable();

    CompletableFuture<String> getHeapObjectId(String objectId);

    CompletableFuture<RemoteObject> getObjectByHeapObjectId(String objectId);

    CompletableFuture<RemoteObject> getObjectByHeapObjectId(String objectId, @Optional String objectGroup);

    CompletableFuture<SamplingHeapProfile> getSamplingProfile();

    CompletableFuture<Void> startSampling();

    CompletableFuture<Void> startSampling(
            @Optional Double samplingInterval,
            @Optional Boolean includeObjectsCollectedByMajorGC,
            @Optional Boolean includeObjectsCollectedByMinorGC);

    CompletableFuture<Void> startTrackingHeapObjects();

    CompletableFuture<Void> startTrackingHeapObjects(@Optional Boolean trackAllocations);

    CompletableFuture<SamplingHeapProfile> stopSampling();

    CompletableFuture<Void> stopTrackingHeapObjects();

    CompletableFuture<Void> stopTrackingHeapObjects(
            @Optional Boolean reportProgress,
            @Optional @Deprecated Boolean treatGlobalObjectsAsRoots,
            @Optional Boolean captureNumericValue,
            @Experimental @Optional Boolean exposeInternals);

    CompletableFuture<Void> takeHeapSnapshot();

    CompletableFuture<Void> takeHeapSnapshot(
            @Optional Boolean reportProgress,
            @Optional @Deprecated Boolean treatGlobalObjectsAsRoots,
            @Optional Boolean captureNumericValue,
            @Experimental @Optional Boolean exposeInternals);
}
