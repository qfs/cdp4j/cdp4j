// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

/**
 * This domain is deprecated - use Runtime or Log instead.
 */
@Deprecated
public interface ConsoleAsync extends ParameterizedCommand<ConsoleAsync> {
    /**
     * Does nothing.
     */
    CompletableFuture<Void> clearMessages();

    /**
     * Disables console domain, prevents further console messages from being
     * reported to the client.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables console domain, sends the messages collected so far to the client by
     * means of the messageAdded notification.
     */
    CompletableFuture<Void> enable();
}
