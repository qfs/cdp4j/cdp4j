// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

/**
 * The Tethering domain defines methods and events for browser port binding.
 */
@Experimental
public interface Tethering extends ParameterizedCommand<Tethering> {
    /**
     * Request browser port binding.
     *
     * @param port Port number to bind.
     */
    void bind(Integer port);

    /**
     * Request browser port unbinding.
     *
     * @param port Port number to unbind.
     */
    void unbind(Integer port);
}
