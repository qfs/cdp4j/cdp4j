// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;

class EventBreakpointsImpl extends ParameterizedCommandImpl<EventBreakpoints> implements EventBreakpoints {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public EventBreakpointsImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.EventBreakpoints_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void removeInstrumentationBreakpoint(String eventName) {
        handler.invoke(
                this,
                DomainCommand.EventBreakpoints_removeInstrumentationBreakpoint,
                CRT_REMOVE_INSTRUMENTATION_BREAKPOINT,
                PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                true);
    }

    @Override
    public void setInstrumentationBreakpoint(String eventName) {
        handler.invoke(
                this,
                DomainCommand.EventBreakpoints_setInstrumentationBreakpoint,
                CRT_SET_INSTRUMENTATION_BREAKPOINT,
                PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                true);
    }
}
