// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.constant.TimeDomain;
import com.cdp4j.type.performance.Metric;
import java.util.List;

public interface Performance extends ParameterizedCommand<Performance> {
    /**
     * Disable collecting and reporting metrics.
     */
    void disable();

    /**
     * Enable collecting and reporting metrics.
     */
    void enable();

    /**
     * Enable collecting and reporting metrics.
     *
     * @param timeDomain Time domain to use for collecting and reporting duration metrics.
     */
    void enable(@Optional TimeDomain timeDomain);

    /**
     * Retrieve current values of run-time metrics.
     *
     * @return Current values for run-time metrics.
     */
    List<Metric> getMetrics();

    /**
     * Sets time domain to use for collecting and reporting duration metrics.
     * Note that this must be called before enabling metrics collection. Calling
     * this method while metrics collection is enabled returns an error.
     *
     * @param timeDomain Time domain
     */
    @Experimental
    @Deprecated
    void setTimeDomain(TimeDomain timeDomain);
}
