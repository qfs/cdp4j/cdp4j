// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.systeminfo.GetInfoResult;
import com.cdp4j.type.systeminfo.ProcessInfo;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class SystemInfoAsyncImpl extends ParameterizedCommandImpl<SystemInfoAsync> implements SystemInfoAsync {

    private static final TypeReference<List<ProcessInfo>> LIST_PROCESSINFO = new TypeReference<List<ProcessInfo>>() {};
    private static final CommandReturnType CRT_GET_FEATURE_STATE =
            new CommandReturnType("featureEnabled", Boolean.class, null);
    private static final CommandReturnType CRT_GET_INFO = new CommandReturnType(null, GetInfoResult.class, null);
    private static final CommandReturnType CRT_GET_PROCESS_INFO =
            new CommandReturnType("processInfo", List.class, LIST_PROCESSINFO);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_FEATURE_STATE_1 = new String[] {"featureState"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public SystemInfoAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> getFeatureState(String featureState) {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.SystemInfo_getFeatureState,
                CRT_GET_FEATURE_STATE,
                PARAMS_GET_FEATURE_STATE_1,
                new Object[] {featureState},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetInfoResult> getInfo() {
        return (CompletableFuture<GetInfoResult>)
                handler.invoke(this, DomainCommand.SystemInfo_getInfo, CRT_GET_INFO, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<ProcessInfo>> getProcessInfo() {
        return (CompletableFuture<List<ProcessInfo>>) handler.invoke(
                this, DomainCommand.SystemInfo_getProcessInfo, CRT_GET_PROCESS_INFO, EMPTY_ARGS, EMPTY_VALUES, false);
    }
}
