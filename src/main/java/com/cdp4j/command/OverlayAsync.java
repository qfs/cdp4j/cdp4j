// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.dom.RGBA;
import com.cdp4j.type.overlay.ContainerQueryHighlightConfig;
import com.cdp4j.type.overlay.FlexNodeHighlightConfig;
import com.cdp4j.type.overlay.GridNodeHighlightConfig;
import com.cdp4j.type.overlay.HighlightConfig;
import com.cdp4j.type.overlay.HingeConfig;
import com.cdp4j.type.overlay.InspectMode;
import com.cdp4j.type.overlay.IsolatedElementHighlightConfig;
import com.cdp4j.type.overlay.ScrollSnapHighlightConfig;
import com.cdp4j.type.overlay.SourceOrderConfig;
import com.cdp4j.type.overlay.WindowControlsOverlayConfig;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * This domain provides various functionality related to drawing atop the
 * inspected page.
 */
@Experimental
public interface OverlayAsync extends ParameterizedCommand<OverlayAsync> {
    /**
     * Disables domain notifications.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables domain notifications.
     */
    CompletableFuture<Void> enable();

    /**
     * Hides any highlight.
     */
    CompletableFuture<Void> hideHighlight();

    /**
     * Highlights owner element of the frame with given id. Deprecated: Doesn't work
     * reliably and cannot be fixed due to process separation (the owner node might
     * be in a different process). Determine the owner node in the client and use
     * highlightNode.
     *
     * @param frameId
     *            Identifier of the frame to highlight.
     */
    @Deprecated
    CompletableFuture<Void> highlightFrame(String frameId);

    /**
     * Highlights owner element of the frame with given id. Deprecated: Doesn't work
     * reliably and cannot be fixed due to process separation (the owner node might
     * be in a different process). Determine the owner node in the client and use
     * highlightNode.
     *
     * @param frameId
     *            Identifier of the frame to highlight.
     * @param contentColor
     *            The content box highlight fill color (default: transparent).
     * @param contentOutlineColor
     *            The content box highlight outline color (default: transparent).
     */
    @Deprecated
    CompletableFuture<Void> highlightFrame(
            String frameId, @Optional RGBA contentColor, @Optional RGBA contentOutlineColor);

    /**
     * Highlights DOM node with given id or with the given JavaScript object
     * wrapper. Either nodeId or objectId must be specified.
     *
     * @param highlightConfig
     *            A descriptor for the highlight appearance.
     */
    CompletableFuture<Void> highlightNode(HighlightConfig highlightConfig);

    /**
     * Highlights DOM node with given id or with the given JavaScript object
     * wrapper. Either nodeId or objectId must be specified.
     *
     * @param highlightConfig
     *            A descriptor for the highlight appearance.
     * @param nodeId
     *            Identifier of the node to highlight.
     * @param backendNodeId
     *            Identifier of the backend node to highlight.
     * @param objectId
     *            JavaScript object id of the node to be highlighted.
     * @param selector
     *            Selectors to highlight relevant nodes.
     */
    CompletableFuture<Void> highlightNode(
            HighlightConfig highlightConfig,
            @Optional Integer nodeId,
            @Optional Integer backendNodeId,
            @Optional String objectId,
            @Optional String selector);

    /**
     * Highlights given quad. Coordinates are absolute with respect to the main
     * frame viewport.
     *
     * @param quad
     *            Quad to highlight
     */
    CompletableFuture<Void> highlightQuad(List<Double> quad);

    /**
     * Highlights given quad. Coordinates are absolute with respect to the main
     * frame viewport.
     *
     * @param quad
     *            Quad to highlight
     * @param color
     *            The highlight fill color (default: transparent).
     * @param outlineColor
     *            The highlight outline color (default: transparent).
     */
    CompletableFuture<Void> highlightQuad(List<Double> quad, @Optional RGBA color, @Optional RGBA outlineColor);

    /**
     * Highlights given rectangle. Coordinates are absolute with respect to the main
     * frame viewport.
     *
     * @param x
     *            X coordinate
     * @param y
     *            Y coordinate
     * @param width
     *            Rectangle width
     * @param height
     *            Rectangle height
     */
    CompletableFuture<Void> highlightRect(Integer x, Integer y, Integer width, Integer height);

    /**
     * Highlights given rectangle. Coordinates are absolute with respect to the main
     * frame viewport.
     *
     * @param x
     *            X coordinate
     * @param y
     *            Y coordinate
     * @param width
     *            Rectangle width
     * @param height
     *            Rectangle height
     * @param color
     *            The highlight fill color (default: transparent).
     * @param outlineColor
     *            The highlight outline color (default: transparent).
     */
    CompletableFuture<Void> highlightRect(
            Integer x, Integer y, Integer width, Integer height, @Optional RGBA color, @Optional RGBA outlineColor);

    /**
     * Highlights the source order of the children of the DOM node with given id or
     * with the given JavaScript object wrapper. Either nodeId or objectId must be
     * specified.
     *
     * @param sourceOrderConfig
     *            A descriptor for the appearance of the overlay drawing.
     */
    CompletableFuture<Void> highlightSourceOrder(SourceOrderConfig sourceOrderConfig);

    /**
     * Highlights the source order of the children of the DOM node with given id or
     * with the given JavaScript object wrapper. Either nodeId or objectId must be
     * specified.
     *
     * @param sourceOrderConfig
     *            A descriptor for the appearance of the overlay drawing.
     * @param nodeId
     *            Identifier of the node to highlight.
     * @param backendNodeId
     *            Identifier of the backend node to highlight.
     * @param objectId
     *            JavaScript object id of the node to be highlighted.
     */
    CompletableFuture<Void> highlightSourceOrder(
            SourceOrderConfig sourceOrderConfig,
            @Optional Integer nodeId,
            @Optional Integer backendNodeId,
            @Optional String objectId);

    /**
     * Enters the 'inspect' mode. In this mode, elements that user is hovering over
     * are highlighted. Backend then generates 'inspectNodeRequested' event upon
     * element selection.
     *
     * @param mode
     *            Set an inspection mode.
     */
    CompletableFuture<Void> setInspectMode(InspectMode mode);

    /**
     * Enters the 'inspect' mode. In this mode, elements that user is hovering over
     * are highlighted. Backend then generates 'inspectNodeRequested' event upon
     * element selection.
     *
     * @param mode
     *            Set an inspection mode.
     * @param highlightConfig
     *            A descriptor for the highlight appearance of hovered-over nodes.
     *            May be omitted if enabled == false.
     */
    CompletableFuture<Void> setInspectMode(InspectMode mode, @Optional HighlightConfig highlightConfig);

    CompletableFuture<Void> setPausedInDebuggerMessage();

    CompletableFuture<Void> setPausedInDebuggerMessage(@Optional String message);

    /**
     * Highlights owner element of all frames detected to be ads.
     *
     * @param show
     *            True for showing ad highlights
     */
    CompletableFuture<Void> setShowAdHighlights(Boolean show);

    CompletableFuture<Void> setShowContainerQueryOverlays(
            List<ContainerQueryHighlightConfig> containerQueryHighlightConfigs);

    /**
     * Requests that backend shows debug borders on layers
     *
     * @param show
     *            True for showing debug borders
     */
    CompletableFuture<Void> setShowDebugBorders(Boolean show);

    /**
     * Requests that backend shows the FPS counter
     *
     * @param show
     *            True for showing the FPS counter
     */
    CompletableFuture<Void> setShowFPSCounter(Boolean show);

    CompletableFuture<Void> setShowFlexOverlays(List<FlexNodeHighlightConfig> flexNodeHighlightConfigs);

    /**
     * Highlight multiple elements with the CSS Grid overlay.
     *
     * @param gridNodeHighlightConfigs
     *            An array of node identifiers and descriptors for the highlight
     *            appearance.
     */
    CompletableFuture<Void> setShowGridOverlays(List<GridNodeHighlightConfig> gridNodeHighlightConfigs);

    /**
     * Add a dual screen device hinge
     */
    CompletableFuture<Void> setShowHinge();

    /**
     * Add a dual screen device hinge
     *
     * @param hingeConfig
     *            hinge data, null means hideHinge
     */
    CompletableFuture<Void> setShowHinge(@Optional HingeConfig hingeConfig);

    /**
     * Deprecated, no longer has any effect.
     *
     * @param show
     *            True for showing hit-test borders
     */
    @Deprecated
    CompletableFuture<Void> setShowHitTestBorders(Boolean show);

    /**
     * Show elements in isolation mode with overlays.
     *
     * @param isolatedElementHighlightConfigs
     *            An array of node identifiers and descriptors for the highlight
     *            appearance.
     */
    CompletableFuture<Void> setShowIsolatedElements(
            List<IsolatedElementHighlightConfig> isolatedElementHighlightConfigs);

    /**
     * Requests that backend shows layout shift regions
     *
     * @param result
     *            True for showing layout shift regions
     */
    CompletableFuture<Void> setShowLayoutShiftRegions(Boolean result);

    /**
     * Requests that backend shows paint rectangles
     *
     * @param result
     *            True for showing paint rectangles
     */
    CompletableFuture<Void> setShowPaintRects(Boolean result);

    /**
     * Requests that backend shows scroll bottleneck rects
     *
     * @param show
     *            True for showing scroll bottleneck rects
     */
    CompletableFuture<Void> setShowScrollBottleneckRects(Boolean show);

    CompletableFuture<Void> setShowScrollSnapOverlays(List<ScrollSnapHighlightConfig> scrollSnapHighlightConfigs);

    /**
     * Paints viewport size upon main frame resize.
     *
     * @param show
     *            Whether to paint size or not.
     */
    CompletableFuture<Void> setShowViewportSizeOnResize(Boolean show);

    /**
     * Deprecated, no longer has any effect.
     *
     */
    @Deprecated
    CompletableFuture<Void> setShowWebVitals(Boolean show);

    /**
     * Show Window Controls Overlay for PWA
     */
    CompletableFuture<Void> setShowWindowControlsOverlay();

    /**
     * Show Window Controls Overlay for PWA
     *
     * @param windowControlsOverlayConfig
     *            Window Controls Overlay data, null means hide Window Controls
     *            Overlay
     */
    CompletableFuture<Void> setShowWindowControlsOverlay(
            @Optional WindowControlsOverlayConfig windowControlsOverlayConfig);
}
