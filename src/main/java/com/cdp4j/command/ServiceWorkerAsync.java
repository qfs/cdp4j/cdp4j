// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

@Experimental
public interface ServiceWorkerAsync extends ParameterizedCommand<ServiceWorkerAsync> {
    CompletableFuture<Void> deliverPushMessage(String origin, String registrationId, String data);

    CompletableFuture<Void> disable();

    CompletableFuture<Void> dispatchPeriodicSyncEvent(String origin, String registrationId, String tag);

    CompletableFuture<Void> dispatchSyncEvent(String origin, String registrationId, String tag, Boolean lastChance);

    CompletableFuture<Void> enable();

    CompletableFuture<Void> inspectWorker(String versionId);

    CompletableFuture<Void> setForceUpdateOnPageLoad(Boolean forceUpdateOnPageLoad);

    CompletableFuture<Void> skipWaiting(String scopeURL);

    CompletableFuture<Void> startWorker(String scopeURL);

    CompletableFuture<Void> stopAllWorkers();

    CompletableFuture<Void> stopWorker(String versionId);

    CompletableFuture<Void> unregister(String scopeURL);

    CompletableFuture<Void> updateRegistration(String scopeURL);
}
