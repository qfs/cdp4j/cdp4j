// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.systeminfo.GetInfoResult;
import com.cdp4j.type.systeminfo.ProcessInfo;
import java.util.List;

class SystemInfoImpl extends ParameterizedCommandImpl<SystemInfo> implements SystemInfo {

    private static final TypeReference<List<ProcessInfo>> LIST_PROCESSINFO = new TypeReference<List<ProcessInfo>>() {};
    private static final CommandReturnType CRT_GET_FEATURE_STATE =
            new CommandReturnType("featureEnabled", Boolean.class, null);
    private static final CommandReturnType CRT_GET_INFO = new CommandReturnType(null, GetInfoResult.class, null);
    private static final CommandReturnType CRT_GET_PROCESS_INFO =
            new CommandReturnType("processInfo", List.class, LIST_PROCESSINFO);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_FEATURE_STATE_1 = new String[] {"featureState"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public SystemInfoImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public Boolean getFeatureState(String featureState) {
        return (Boolean) handler.invoke(
                this,
                DomainCommand.SystemInfo_getFeatureState,
                CRT_GET_FEATURE_STATE,
                PARAMS_GET_FEATURE_STATE_1,
                new Object[] {featureState},
                true);
    }

    @Override
    public GetInfoResult getInfo() {
        return (GetInfoResult)
                handler.invoke(this, DomainCommand.SystemInfo_getInfo, CRT_GET_INFO, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<ProcessInfo> getProcessInfo() {
        return (List<ProcessInfo>) handler.invoke(
                this, DomainCommand.SystemInfo_getProcessInfo, CRT_GET_PROCESS_INFO, EMPTY_ARGS, EMPTY_VALUES, true);
    }
}
