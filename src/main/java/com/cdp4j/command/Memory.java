// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.memory.DOMCounter;
import com.cdp4j.type.memory.GetDOMCountersResult;
import com.cdp4j.type.memory.PressureLevel;
import com.cdp4j.type.memory.SamplingProfile;
import java.util.List;

@Experimental
public interface Memory extends ParameterizedCommand<Memory> {
    /**
     * Simulate OomIntervention by purging V8 memory.
     */
    void forciblyPurgeJavaScriptMemory();

    /**
     * Retrieve native memory allocations profile
     * collected since renderer process startup.
     */
    SamplingProfile getAllTimeSamplingProfile();

    /**
     * Retrieve native memory allocations profile
     * collected since browser process startup.
     */
    SamplingProfile getBrowserSamplingProfile();

    /**
     * Retruns current DOM object counters.
     *
     * @return GetDOMCountersResult
     */
    GetDOMCountersResult getDOMCounters();

    /**
     * Retruns DOM object counters after preparing renderer for leak detection.
     *
     * @return DOM object counters.
     */
    List<DOMCounter> getDOMCountersForLeakDetection();

    /**
     * Retrieve native memory allocations profile collected since last
     * startSampling call.
     */
    SamplingProfile getSamplingProfile();

    /**
     * Prepares for leak detection by terminating workers, stopping spellcheckers,
     * dropping non-essential internal caches, running garbage collections, etc.
     */
    void prepareForLeakDetection();

    /**
     * Enable/disable suppressing memory pressure notifications in all processes.
     *
     * @param suppressed If true, memory pressure notifications will be suppressed.
     */
    void setPressureNotificationsSuppressed(Boolean suppressed);

    /**
     * Simulate a memory pressure notification in all processes.
     *
     * @param level Memory pressure level of the notification.
     */
    void simulatePressureNotification(PressureLevel level);

    /**
     * Start collecting native memory profile.
     */
    void startSampling();

    /**
     * Start collecting native memory profile.
     *
     * @param samplingInterval Average number of bytes between samples.
     * @param suppressRandomness Do not randomize intervals between samples.
     */
    void startSampling(@Optional Integer samplingInterval, @Optional Boolean suppressRandomness);

    /**
     * Stop collecting native memory profile.
     */
    void stopSampling();
}
