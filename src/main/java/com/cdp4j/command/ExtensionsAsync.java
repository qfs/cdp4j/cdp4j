// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.extensions.StorageArea;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Defines commands and events for browser extensions.
 */
@Experimental
public interface ExtensionsAsync extends ParameterizedCommand<ExtensionsAsync> {
    /**
     * Clears extension storage in the given storageArea.
     *
     * @param id
     *            ID of extension.
     * @param storageArea
     *            StorageArea to remove data from.
     */
    CompletableFuture<Void> clearStorageItems(String id, StorageArea storageArea);

    /**
     * Installs an unpacked extension from the filesystem similar to
     * --load-extension CLI flags. Returns extension ID once the extension has been
     * installed. Available if the client is connected using the
     * --remote-debugging-pipe flag and the --enable-unsafe-extension-debugging flag
     * is set.
     *
     * @param path
     *            Absolute file path.
     *
     * @return Extension id.
     */
    CompletableFuture<String> loadUnpacked(String path);

    /**
     * Removes keys from extension storage in the given storageArea.
     *
     * @param id
     *            ID of extension.
     * @param storageArea
     *            StorageArea to remove data from.
     * @param keys
     *            Keys to remove.
     */
    CompletableFuture<Void> removeStorageItems(String id, StorageArea storageArea, List<String> keys);

    /**
     * Sets values in extension storage in the given storageArea. The provided
     * values will be merged with existing values in the storage area.
     *
     * @param id
     *            ID of extension.
     * @param storageArea
     *            StorageArea to set data in.
     */
    CompletableFuture<Void> setStorageItems(String id, StorageArea storageArea);

    /**
     * Uninstalls an unpacked extension (others not supported) from the profile.
     * Available if the client is connected using the --remote-debugging-pipe flag
     * and the --enable-unsafe-extension-debugging.
     *
     * @param id
     *            Extension id.
     */
    CompletableFuture<Void> uninstall(String id);
}
