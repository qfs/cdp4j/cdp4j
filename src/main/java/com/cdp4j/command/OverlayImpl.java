// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.dom.RGBA;
import com.cdp4j.type.overlay.ContainerQueryHighlightConfig;
import com.cdp4j.type.overlay.FlexNodeHighlightConfig;
import com.cdp4j.type.overlay.GridNodeHighlightConfig;
import com.cdp4j.type.overlay.HighlightConfig;
import com.cdp4j.type.overlay.HingeConfig;
import com.cdp4j.type.overlay.InspectMode;
import com.cdp4j.type.overlay.IsolatedElementHighlightConfig;
import com.cdp4j.type.overlay.ScrollSnapHighlightConfig;
import com.cdp4j.type.overlay.SourceOrderConfig;
import com.cdp4j.type.overlay.WindowControlsOverlayConfig;
import java.util.List;

class OverlayImpl extends ParameterizedCommandImpl<Overlay> implements Overlay {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIDE_HIGHLIGHT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_FRAME = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_NODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_QUAD = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_RECT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_SOURCE_ORDER = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INSPECT_MODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_PAUSED_IN_DEBUGGER_MESSAGE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_AD_HIGHLIGHTS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_CONTAINER_QUERY_OVERLAYS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_DEBUG_BORDERS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_FLEX_OVERLAYS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_FP_SCOUNTER = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_GRID_OVERLAYS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_HINGE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_HIT_TEST_BORDERS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_ISOLATED_ELEMENTS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_LAYOUT_SHIFT_REGIONS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_PAINT_RECTS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_SCROLL_BOTTLENECK_RECTS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_SCROLL_SNAP_OVERLAYS =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_VIEWPORT_SIZE_ON_RESIZE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_WEB_VITALS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_SHOW_WINDOW_CONTROLS_OVERLAY =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_HIGHLIGHT_FRAME_1 = new String[] {"frameId"};
    private static final String[] PARAMS_HIGHLIGHT_FRAME_2 =
            new String[] {"frameId", "contentColor", "contentOutlineColor"};
    private static final String[] PARAMS_HIGHLIGHT_NODE_1 = new String[] {"highlightConfig"};
    private static final String[] PARAMS_HIGHLIGHT_NODE_2 =
            new String[] {"highlightConfig", "nodeId", "backendNodeId", "objectId", "selector"};
    private static final String[] PARAMS_HIGHLIGHT_QUAD_1 = new String[] {"quad"};
    private static final String[] PARAMS_HIGHLIGHT_QUAD_2 = new String[] {"quad", "color", "outlineColor"};
    private static final String[] PARAMS_HIGHLIGHT_RECT_1 = new String[] {"x", "y", "width", "height"};
    private static final String[] PARAMS_HIGHLIGHT_RECT_2 =
            new String[] {"x", "y", "width", "height", "color", "outlineColor"};
    private static final String[] PARAMS_HIGHLIGHT_SOURCE_ORDER_1 = new String[] {"sourceOrderConfig"};
    private static final String[] PARAMS_HIGHLIGHT_SOURCE_ORDER_2 =
            new String[] {"sourceOrderConfig", "nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_SET_INSPECT_MODE_1 = new String[] {"mode"};
    private static final String[] PARAMS_SET_INSPECT_MODE_2 = new String[] {"mode", "highlightConfig"};
    private static final String[] PARAMS_SET_PAUSED_IN_DEBUGGER_MESSAGE_2 = new String[] {"message"};
    private static final String[] PARAMS_SET_SHOW_AD_HIGHLIGHTS_1 = new String[] {"show"};
    private static final String[] PARAMS_SET_SHOW_CONTAINER_QUERY_OVERLAYS_1 =
            new String[] {"containerQueryHighlightConfigs"};
    private static final String[] PARAMS_SET_SHOW_DEBUG_BORDERS_1 = new String[] {"show"};
    private static final String[] PARAMS_SET_SHOW_FLEX_OVERLAYS_1 = new String[] {"flexNodeHighlightConfigs"};
    private static final String[] PARAMS_SET_SHOW_FP_SCOUNTER_1 = new String[] {"show"};
    private static final String[] PARAMS_SET_SHOW_GRID_OVERLAYS_1 = new String[] {"gridNodeHighlightConfigs"};
    private static final String[] PARAMS_SET_SHOW_HINGE_2 = new String[] {"hingeConfig"};
    private static final String[] PARAMS_SET_SHOW_HIT_TEST_BORDERS_1 = new String[] {"show"};
    private static final String[] PARAMS_SET_SHOW_ISOLATED_ELEMENTS_1 =
            new String[] {"isolatedElementHighlightConfigs"};
    private static final String[] PARAMS_SET_SHOW_LAYOUT_SHIFT_REGIONS_1 = new String[] {"result"};
    private static final String[] PARAMS_SET_SHOW_PAINT_RECTS_1 = new String[] {"result"};
    private static final String[] PARAMS_SET_SHOW_SCROLL_BOTTLENECK_RECTS_1 = new String[] {"show"};
    private static final String[] PARAMS_SET_SHOW_SCROLL_SNAP_OVERLAYS_1 = new String[] {"scrollSnapHighlightConfigs"};
    private static final String[] PARAMS_SET_SHOW_VIEWPORT_SIZE_ON_RESIZE_1 = new String[] {"show"};
    private static final String[] PARAMS_SET_SHOW_WEB_VITALS_1 = new String[] {"show"};
    private static final String[] PARAMS_SET_SHOW_WINDOW_CONTROLS_OVERLAY_2 =
            new String[] {"windowControlsOverlayConfig"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public OverlayImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Overlay_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Overlay_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void hideHighlight() {
        handler.invoke(this, DomainCommand.Overlay_hideHighlight, CRT_HIDE_HIGHLIGHT, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void highlightFrame(String frameId) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightFrame,
                CRT_HIGHLIGHT_FRAME,
                PARAMS_HIGHLIGHT_FRAME_1,
                new Object[] {frameId},
                true);
    }

    @Override
    public void highlightFrame(String frameId, RGBA contentColor, RGBA contentOutlineColor) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightFrame,
                CRT_HIGHLIGHT_FRAME,
                PARAMS_HIGHLIGHT_FRAME_2,
                new Object[] {frameId, contentColor, contentOutlineColor},
                true);
    }

    @Override
    public void highlightNode(HighlightConfig highlightConfig) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightNode,
                CRT_HIGHLIGHT_NODE,
                PARAMS_HIGHLIGHT_NODE_1,
                new Object[] {highlightConfig},
                true);
    }

    @Override
    public void highlightNode(
            HighlightConfig highlightConfig, Integer nodeId, Integer backendNodeId, String objectId, String selector) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightNode,
                CRT_HIGHLIGHT_NODE,
                PARAMS_HIGHLIGHT_NODE_2,
                new Object[] {highlightConfig, nodeId, backendNodeId, objectId, selector},
                true);
    }

    @Override
    public void highlightQuad(List<Double> quad) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightQuad,
                CRT_HIGHLIGHT_QUAD,
                PARAMS_HIGHLIGHT_QUAD_1,
                new Object[] {quad},
                true);
    }

    @Override
    public void highlightQuad(List<Double> quad, RGBA color, RGBA outlineColor) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightQuad,
                CRT_HIGHLIGHT_QUAD,
                PARAMS_HIGHLIGHT_QUAD_2,
                new Object[] {quad, color, outlineColor},
                true);
    }

    @Override
    public void highlightRect(Integer x, Integer y, Integer width, Integer height) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightRect,
                CRT_HIGHLIGHT_RECT,
                PARAMS_HIGHLIGHT_RECT_1,
                new Object[] {x, y, width, height},
                true);
    }

    @Override
    public void highlightRect(Integer x, Integer y, Integer width, Integer height, RGBA color, RGBA outlineColor) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightRect,
                CRT_HIGHLIGHT_RECT,
                PARAMS_HIGHLIGHT_RECT_2,
                new Object[] {x, y, width, height, color, outlineColor},
                true);
    }

    @Override
    public void highlightSourceOrder(SourceOrderConfig sourceOrderConfig) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightSourceOrder,
                CRT_HIGHLIGHT_SOURCE_ORDER,
                PARAMS_HIGHLIGHT_SOURCE_ORDER_1,
                new Object[] {sourceOrderConfig},
                true);
    }

    @Override
    public void highlightSourceOrder(
            SourceOrderConfig sourceOrderConfig, Integer nodeId, Integer backendNodeId, String objectId) {
        handler.invoke(
                this,
                DomainCommand.Overlay_highlightSourceOrder,
                CRT_HIGHLIGHT_SOURCE_ORDER,
                PARAMS_HIGHLIGHT_SOURCE_ORDER_2,
                new Object[] {sourceOrderConfig, nodeId, backendNodeId, objectId},
                true);
    }

    @Override
    public void setInspectMode(InspectMode mode) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setInspectMode,
                CRT_SET_INSPECT_MODE,
                PARAMS_SET_INSPECT_MODE_1,
                new Object[] {mode},
                true);
    }

    @Override
    public void setInspectMode(InspectMode mode, HighlightConfig highlightConfig) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setInspectMode,
                CRT_SET_INSPECT_MODE,
                PARAMS_SET_INSPECT_MODE_2,
                new Object[] {mode, highlightConfig},
                true);
    }

    @Override
    public void setPausedInDebuggerMessage() {
        handler.invoke(
                this,
                DomainCommand.Overlay_setPausedInDebuggerMessage,
                CRT_SET_PAUSED_IN_DEBUGGER_MESSAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void setPausedInDebuggerMessage(String message) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setPausedInDebuggerMessage,
                CRT_SET_PAUSED_IN_DEBUGGER_MESSAGE,
                PARAMS_SET_PAUSED_IN_DEBUGGER_MESSAGE_2,
                new Object[] {message},
                true);
    }

    @Override
    public void setShowAdHighlights(Boolean show) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowAdHighlights,
                CRT_SET_SHOW_AD_HIGHLIGHTS,
                PARAMS_SET_SHOW_AD_HIGHLIGHTS_1,
                new Object[] {show},
                true);
    }

    @Override
    public void setShowContainerQueryOverlays(List<ContainerQueryHighlightConfig> containerQueryHighlightConfigs) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowContainerQueryOverlays,
                CRT_SET_SHOW_CONTAINER_QUERY_OVERLAYS,
                PARAMS_SET_SHOW_CONTAINER_QUERY_OVERLAYS_1,
                new Object[] {containerQueryHighlightConfigs},
                true);
    }

    @Override
    public void setShowDebugBorders(Boolean show) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowDebugBorders,
                CRT_SET_SHOW_DEBUG_BORDERS,
                PARAMS_SET_SHOW_DEBUG_BORDERS_1,
                new Object[] {show},
                true);
    }

    @Override
    public void setShowFPSCounter(Boolean show) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowFPSCounter,
                CRT_SET_SHOW_FP_SCOUNTER,
                PARAMS_SET_SHOW_FP_SCOUNTER_1,
                new Object[] {show},
                true);
    }

    @Override
    public void setShowFlexOverlays(List<FlexNodeHighlightConfig> flexNodeHighlightConfigs) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowFlexOverlays,
                CRT_SET_SHOW_FLEX_OVERLAYS,
                PARAMS_SET_SHOW_FLEX_OVERLAYS_1,
                new Object[] {flexNodeHighlightConfigs},
                true);
    }

    @Override
    public void setShowGridOverlays(List<GridNodeHighlightConfig> gridNodeHighlightConfigs) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowGridOverlays,
                CRT_SET_SHOW_GRID_OVERLAYS,
                PARAMS_SET_SHOW_GRID_OVERLAYS_1,
                new Object[] {gridNodeHighlightConfigs},
                true);
    }

    @Override
    public void setShowHinge() {
        handler.invoke(this, DomainCommand.Overlay_setShowHinge, CRT_SET_SHOW_HINGE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void setShowHinge(HingeConfig hingeConfig) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowHinge,
                CRT_SET_SHOW_HINGE,
                PARAMS_SET_SHOW_HINGE_2,
                new Object[] {hingeConfig},
                true);
    }

    @Override
    public void setShowHitTestBorders(Boolean show) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowHitTestBorders,
                CRT_SET_SHOW_HIT_TEST_BORDERS,
                PARAMS_SET_SHOW_HIT_TEST_BORDERS_1,
                new Object[] {show},
                true);
    }

    @Override
    public void setShowIsolatedElements(List<IsolatedElementHighlightConfig> isolatedElementHighlightConfigs) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowIsolatedElements,
                CRT_SET_SHOW_ISOLATED_ELEMENTS,
                PARAMS_SET_SHOW_ISOLATED_ELEMENTS_1,
                new Object[] {isolatedElementHighlightConfigs},
                true);
    }

    @Override
    public void setShowLayoutShiftRegions(Boolean result) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowLayoutShiftRegions,
                CRT_SET_SHOW_LAYOUT_SHIFT_REGIONS,
                PARAMS_SET_SHOW_LAYOUT_SHIFT_REGIONS_1,
                new Object[] {result},
                true);
    }

    @Override
    public void setShowPaintRects(Boolean result) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowPaintRects,
                CRT_SET_SHOW_PAINT_RECTS,
                PARAMS_SET_SHOW_PAINT_RECTS_1,
                new Object[] {result},
                true);
    }

    @Override
    public void setShowScrollBottleneckRects(Boolean show) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowScrollBottleneckRects,
                CRT_SET_SHOW_SCROLL_BOTTLENECK_RECTS,
                PARAMS_SET_SHOW_SCROLL_BOTTLENECK_RECTS_1,
                new Object[] {show},
                true);
    }

    @Override
    public void setShowScrollSnapOverlays(List<ScrollSnapHighlightConfig> scrollSnapHighlightConfigs) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowScrollSnapOverlays,
                CRT_SET_SHOW_SCROLL_SNAP_OVERLAYS,
                PARAMS_SET_SHOW_SCROLL_SNAP_OVERLAYS_1,
                new Object[] {scrollSnapHighlightConfigs},
                true);
    }

    @Override
    public void setShowViewportSizeOnResize(Boolean show) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowViewportSizeOnResize,
                CRT_SET_SHOW_VIEWPORT_SIZE_ON_RESIZE,
                PARAMS_SET_SHOW_VIEWPORT_SIZE_ON_RESIZE_1,
                new Object[] {show},
                true);
    }

    @Override
    public void setShowWebVitals(Boolean show) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowWebVitals,
                CRT_SET_SHOW_WEB_VITALS,
                PARAMS_SET_SHOW_WEB_VITALS_1,
                new Object[] {show},
                true);
    }

    @Override
    public void setShowWindowControlsOverlay() {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowWindowControlsOverlay,
                CRT_SET_SHOW_WINDOW_CONTROLS_OVERLAY,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void setShowWindowControlsOverlay(WindowControlsOverlayConfig windowControlsOverlayConfig) {
        handler.invoke(
                this,
                DomainCommand.Overlay_setShowWindowControlsOverlay,
                CRT_SET_SHOW_WINDOW_CONTROLS_OVERLAY,
                PARAMS_SET_SHOW_WINDOW_CONTROLS_OVERLAY_2,
                new Object[] {windowControlsOverlayConfig},
                true);
    }
}
