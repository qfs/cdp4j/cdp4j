// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.headlessexperimental.BeginFrameResult;
import com.cdp4j.type.headlessexperimental.ScreenshotParams;
import java.util.concurrent.CompletableFuture;

/**
 * This domain provides experimental commands only supported in headless mode.
 */
@Experimental
public interface HeadlessExperimentalAsync extends ParameterizedCommand<HeadlessExperimentalAsync> {
    /**
     * Sends a BeginFrame to the target and returns when the frame was completed.
     * Optionally captures a screenshot from the resulting frame. Requires that the
     * target was created with enabled BeginFrameControl. Designed for use with
     * --run-all-compositor-stages-before-draw, see also
     * https://goo.gle/chrome-headless-rendering for more background.
     *
     * @return BeginFrameResult
     */
    CompletableFuture<BeginFrameResult> beginFrame();

    /**
     * Sends a BeginFrame to the target and returns when the frame was completed.
     * Optionally captures a screenshot from the resulting frame. Requires that the
     * target was created with enabled BeginFrameControl. Designed for use with
     * --run-all-compositor-stages-before-draw, see also
     * https://goo.gle/chrome-headless-rendering for more background.
     *
     * @param frameTimeTicks
     *            Timestamp of this BeginFrame in Renderer TimeTicks (milliseconds
     *            of uptime). If not set, the current time will be used.
     * @param interval
     *            The interval between BeginFrames that is reported to the
     *            compositor, in milliseconds. Defaults to a 60 frames/second
     *            interval, i.e. about 16.666 milliseconds.
     * @param noDisplayUpdates
     *            Whether updates should not be committed and drawn onto the
     *            display. False by default. If true, only side effects of the
     *            BeginFrame will be run, such as layout and animations, but any
     *            visual updates may not be visible on the display or in
     *            screenshots.
     * @param screenshot
     *            If set, a screenshot of the frame will be captured and returned in
     *            the response. Otherwise, no screenshot will be captured. Note that
     *            capturing a screenshot can fail, for example, during renderer
     *            initialization. In such a case, no screenshot data will be
     *            returned.
     *
     * @return BeginFrameResult
     */
    CompletableFuture<BeginFrameResult> beginFrame(
            @Optional Double frameTimeTicks,
            @Optional Double interval,
            @Optional Boolean noDisplayUpdates,
            @Optional ScreenshotParams screenshot);

    /**
     * Disables headless events for the target.
     */
    @Deprecated
    CompletableFuture<Void> disable();

    /**
     * Enables headless events for the target.
     */
    @Deprecated
    CompletableFuture<Void> enable();
}
