// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.css.CSSComputedStyleProperty;
import com.cdp4j.type.css.CSSContainerQuery;
import com.cdp4j.type.css.CSSLayerData;
import com.cdp4j.type.css.CSSMedia;
import com.cdp4j.type.css.CSSProperty;
import com.cdp4j.type.css.CSSRule;
import com.cdp4j.type.css.CSSScope;
import com.cdp4j.type.css.CSSStyle;
import com.cdp4j.type.css.CSSSupports;
import com.cdp4j.type.css.GetAnimatedStylesForNodeResult;
import com.cdp4j.type.css.GetBackgroundColorsResult;
import com.cdp4j.type.css.GetInlineStylesForNodeResult;
import com.cdp4j.type.css.GetMatchedStylesForNodeResult;
import com.cdp4j.type.css.PlatformFontUsage;
import com.cdp4j.type.css.RuleUsage;
import com.cdp4j.type.css.SelectorList;
import com.cdp4j.type.css.SourceRange;
import com.cdp4j.type.css.StyleDeclarationEdit;
import com.cdp4j.type.css.TakeCoverageDeltaResult;
import com.cdp4j.type.css.Value;
import com.cdp4j.type.dom.PseudoType;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class CSSAsyncImpl extends ParameterizedCommandImpl<CSSAsync> implements CSSAsync {

    private static final TypeReference<List<CSSComputedStyleProperty>> LIST_CSSCOMPUTEDSTYLEPROPERTY =
            new TypeReference<List<CSSComputedStyleProperty>>() {};
    private static final TypeReference<List<CSSMedia>> LIST_CSSMEDIA = new TypeReference<List<CSSMedia>>() {};
    private static final TypeReference<List<CSSProperty>> LIST_CSSPROPERTY = new TypeReference<List<CSSProperty>>() {};
    private static final TypeReference<List<CSSStyle>> LIST_CSSSTYLE = new TypeReference<List<CSSStyle>>() {};
    private static final TypeReference<List<Integer>> LIST_INTEGER = new TypeReference<List<Integer>>() {};
    private static final TypeReference<List<PlatformFontUsage>> LIST_PLATFORMFONTUSAGE =
            new TypeReference<List<PlatformFontUsage>>() {};
    private static final TypeReference<List<RuleUsage>> LIST_RULEUSAGE = new TypeReference<List<RuleUsage>>() {};
    private static final TypeReference<List<SourceRange>> LIST_SOURCERANGE = new TypeReference<List<SourceRange>>() {};
    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_ADD_RULE = new CommandReturnType("rule", CSSRule.class, null);
    private static final CommandReturnType CRT_COLLECT_CLASS_NAMES =
            new CommandReturnType("classNames", List.class, LIST_STRING);
    private static final CommandReturnType CRT_CREATE_STYLE_SHEET =
            new CommandReturnType("styleSheetId", String.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_FORCE_PSEUDO_STATE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_FORCE_STARTING_STYLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ANIMATED_STYLES_FOR_NODE =
            new CommandReturnType(null, GetAnimatedStylesForNodeResult.class, null);
    private static final CommandReturnType CRT_GET_BACKGROUND_COLORS =
            new CommandReturnType(null, GetBackgroundColorsResult.class, null);
    private static final CommandReturnType CRT_GET_COMPUTED_STYLE_FOR_NODE =
            new CommandReturnType("computedStyle", List.class, LIST_CSSCOMPUTEDSTYLEPROPERTY);
    private static final CommandReturnType CRT_GET_INLINE_STYLES_FOR_NODE =
            new CommandReturnType(null, GetInlineStylesForNodeResult.class, null);
    private static final CommandReturnType CRT_GET_LAYERS_FOR_NODE =
            new CommandReturnType("rootLayer", CSSLayerData.class, null);
    private static final CommandReturnType CRT_GET_LOCATION_FOR_SELECTOR =
            new CommandReturnType("ranges", List.class, LIST_SOURCERANGE);
    private static final CommandReturnType CRT_GET_LONGHAND_PROPERTIES =
            new CommandReturnType("longhandProperties", List.class, LIST_CSSPROPERTY);
    private static final CommandReturnType CRT_GET_MATCHED_STYLES_FOR_NODE =
            new CommandReturnType(null, GetMatchedStylesForNodeResult.class, null);
    private static final CommandReturnType CRT_GET_MEDIA_QUERIES =
            new CommandReturnType("medias", List.class, LIST_CSSMEDIA);
    private static final CommandReturnType CRT_GET_PLATFORM_FONTS_FOR_NODE =
            new CommandReturnType("fonts", List.class, LIST_PLATFORMFONTUSAGE);
    private static final CommandReturnType CRT_GET_STYLE_SHEET_TEXT = new CommandReturnType("text", String.class, null);
    private static final CommandReturnType CRT_RESOLVE_VALUES =
            new CommandReturnType("results", List.class, LIST_STRING);
    private static final CommandReturnType CRT_SET_CONTAINER_QUERY_TEXT =
            new CommandReturnType("containerQuery", CSSContainerQuery.class, null);
    private static final CommandReturnType CRT_SET_EFFECTIVE_PROPERTY_VALUE_FOR_NODE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_KEYFRAME_KEY = new CommandReturnType("keyText", Value.class, null);
    private static final CommandReturnType CRT_SET_LOCAL_FONTS_ENABLED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_MEDIA_TEXT = new CommandReturnType("media", CSSMedia.class, null);
    private static final CommandReturnType CRT_SET_PROPERTY_RULE_PROPERTY_NAME =
            new CommandReturnType("propertyName", Value.class, null);
    private static final CommandReturnType CRT_SET_RULE_SELECTOR =
            new CommandReturnType("selectorList", SelectorList.class, null);
    private static final CommandReturnType CRT_SET_SCOPE_TEXT = new CommandReturnType("scope", CSSScope.class, null);
    private static final CommandReturnType CRT_SET_STYLE_SHEET_TEXT =
            new CommandReturnType("sourceMapURL", String.class, null);
    private static final CommandReturnType CRT_SET_STYLE_TEXTS =
            new CommandReturnType("styles", List.class, LIST_CSSSTYLE);
    private static final CommandReturnType CRT_SET_SUPPORTS_TEXT =
            new CommandReturnType("supports", CSSSupports.class, null);
    private static final CommandReturnType CRT_START_RULE_USAGE_TRACKING =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_STOP_RULE_USAGE_TRACKING =
            new CommandReturnType("ruleUsage", List.class, LIST_RULEUSAGE);
    private static final CommandReturnType CRT_TAKE_COMPUTED_STYLE_UPDATES =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_TAKE_COVERAGE_DELTA =
            new CommandReturnType(null, TakeCoverageDeltaResult.class, null);
    private static final CommandReturnType CRT_TRACK_COMPUTED_STYLE_UPDATES =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TRACK_COMPUTED_STYLE_UPDATES_FOR_NODE =
            new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ADD_RULE_1 = new String[] {"styleSheetId", "ruleText", "location"};
    private static final String[] PARAMS_ADD_RULE_2 =
            new String[] {"styleSheetId", "ruleText", "location", "nodeForPropertySyntaxValidation"};
    private static final String[] PARAMS_COLLECT_CLASS_NAMES_1 = new String[] {"styleSheetId"};
    private static final String[] PARAMS_CREATE_STYLE_SHEET_1 = new String[] {"frameId"};
    private static final String[] PARAMS_CREATE_STYLE_SHEET_2 = new String[] {"frameId", "force"};
    private static final String[] PARAMS_FORCE_PSEUDO_STATE_1 = new String[] {"nodeId", "forcedPseudoClasses"};
    private static final String[] PARAMS_FORCE_STARTING_STYLE_1 = new String[] {"nodeId", "forced"};
    private static final String[] PARAMS_GET_ANIMATED_STYLES_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_BACKGROUND_COLORS_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_COMPUTED_STYLE_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_INLINE_STYLES_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_LAYERS_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_LOCATION_FOR_SELECTOR_1 = new String[] {"styleSheetId", "selectorText"};
    private static final String[] PARAMS_GET_LONGHAND_PROPERTIES_1 = new String[] {"shorthandName", "value"};
    private static final String[] PARAMS_GET_MATCHED_STYLES_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_PLATFORM_FONTS_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_STYLE_SHEET_TEXT_1 = new String[] {"styleSheetId"};
    private static final String[] PARAMS_RESOLVE_VALUES_1 = new String[] {"values", "nodeId"};
    private static final String[] PARAMS_RESOLVE_VALUES_2 =
            new String[] {"values", "nodeId", "propertyName", "pseudoType", "pseudoIdentifier"};
    private static final String[] PARAMS_SET_CONTAINER_QUERY_TEXT_1 = new String[] {"styleSheetId", "range", "text"};
    private static final String[] PARAMS_SET_EFFECTIVE_PROPERTY_VALUE_FOR_NODE_1 =
            new String[] {"nodeId", "propertyName", "value"};
    private static final String[] PARAMS_SET_KEYFRAME_KEY_1 = new String[] {"styleSheetId", "range", "keyText"};
    private static final String[] PARAMS_SET_LOCAL_FONTS_ENABLED_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_MEDIA_TEXT_1 = new String[] {"styleSheetId", "range", "text"};
    private static final String[] PARAMS_SET_PROPERTY_RULE_PROPERTY_NAME_1 =
            new String[] {"styleSheetId", "range", "propertyName"};
    private static final String[] PARAMS_SET_RULE_SELECTOR_1 = new String[] {"styleSheetId", "range", "selector"};
    private static final String[] PARAMS_SET_SCOPE_TEXT_1 = new String[] {"styleSheetId", "range", "text"};
    private static final String[] PARAMS_SET_STYLE_SHEET_TEXT_1 = new String[] {"styleSheetId", "text"};
    private static final String[] PARAMS_SET_STYLE_TEXTS_1 = new String[] {"edits"};
    private static final String[] PARAMS_SET_STYLE_TEXTS_2 = new String[] {"edits", "nodeForPropertySyntaxValidation"};
    private static final String[] PARAMS_SET_SUPPORTS_TEXT_1 = new String[] {"styleSheetId", "range", "text"};
    private static final String[] PARAMS_TRACK_COMPUTED_STYLE_UPDATES_1 = new String[] {"propertiesToTrack"};
    private static final String[] PARAMS_TRACK_COMPUTED_STYLE_UPDATES_FOR_NODE_2 = new String[] {"nodeId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public CSSAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CSSRule> addRule(String styleSheetId, String ruleText, SourceRange location) {
        return (CompletableFuture<CSSRule>) handler.invoke(
                this,
                DomainCommand.CSS_addRule,
                CRT_ADD_RULE,
                PARAMS_ADD_RULE_1,
                new Object[] {styleSheetId, ruleText, location},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CSSRule> addRule(
            String styleSheetId, String ruleText, SourceRange location, Integer nodeForPropertySyntaxValidation) {
        return (CompletableFuture<CSSRule>) handler.invoke(
                this,
                DomainCommand.CSS_addRule,
                CRT_ADD_RULE,
                PARAMS_ADD_RULE_2,
                new Object[] {styleSheetId, ruleText, location, nodeForPropertySyntaxValidation},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> collectClassNames(String styleSheetId) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.CSS_collectClassNames,
                CRT_COLLECT_CLASS_NAMES,
                PARAMS_COLLECT_CLASS_NAMES_1,
                new Object[] {styleSheetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> createStyleSheet(String frameId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.CSS_createStyleSheet,
                CRT_CREATE_STYLE_SHEET,
                PARAMS_CREATE_STYLE_SHEET_1,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> createStyleSheet(String frameId, Boolean force) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.CSS_createStyleSheet,
                CRT_CREATE_STYLE_SHEET,
                PARAMS_CREATE_STYLE_SHEET_2,
                new Object[] {frameId, force},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.CSS_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.CSS_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> forcePseudoState(Integer nodeId, List<String> forcedPseudoClasses) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_forcePseudoState,
                CRT_FORCE_PSEUDO_STATE,
                PARAMS_FORCE_PSEUDO_STATE_1,
                new Object[] {nodeId, forcedPseudoClasses},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> forceStartingStyle(Integer nodeId, Boolean forced) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_forceStartingStyle,
                CRT_FORCE_STARTING_STYLE,
                PARAMS_FORCE_STARTING_STYLE_1,
                new Object[] {nodeId, forced},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetAnimatedStylesForNodeResult> getAnimatedStylesForNode(Integer nodeId) {
        return (CompletableFuture<GetAnimatedStylesForNodeResult>) handler.invoke(
                this,
                DomainCommand.CSS_getAnimatedStylesForNode,
                CRT_GET_ANIMATED_STYLES_FOR_NODE,
                PARAMS_GET_ANIMATED_STYLES_FOR_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetBackgroundColorsResult> getBackgroundColors(Integer nodeId) {
        return (CompletableFuture<GetBackgroundColorsResult>) handler.invoke(
                this,
                DomainCommand.CSS_getBackgroundColors,
                CRT_GET_BACKGROUND_COLORS,
                PARAMS_GET_BACKGROUND_COLORS_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<CSSComputedStyleProperty>> getComputedStyleForNode(Integer nodeId) {
        return (CompletableFuture<List<CSSComputedStyleProperty>>) handler.invoke(
                this,
                DomainCommand.CSS_getComputedStyleForNode,
                CRT_GET_COMPUTED_STYLE_FOR_NODE,
                PARAMS_GET_COMPUTED_STYLE_FOR_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetInlineStylesForNodeResult> getInlineStylesForNode(Integer nodeId) {
        return (CompletableFuture<GetInlineStylesForNodeResult>) handler.invoke(
                this,
                DomainCommand.CSS_getInlineStylesForNode,
                CRT_GET_INLINE_STYLES_FOR_NODE,
                PARAMS_GET_INLINE_STYLES_FOR_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CSSLayerData> getLayersForNode(Integer nodeId) {
        return (CompletableFuture<CSSLayerData>) handler.invoke(
                this,
                DomainCommand.CSS_getLayersForNode,
                CRT_GET_LAYERS_FOR_NODE,
                PARAMS_GET_LAYERS_FOR_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<SourceRange>> getLocationForSelector(String styleSheetId, String selectorText) {
        return (CompletableFuture<List<SourceRange>>) handler.invoke(
                this,
                DomainCommand.CSS_getLocationForSelector,
                CRT_GET_LOCATION_FOR_SELECTOR,
                PARAMS_GET_LOCATION_FOR_SELECTOR_1,
                new Object[] {styleSheetId, selectorText},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<CSSProperty>> getLonghandProperties(String shorthandName, String value) {
        return (CompletableFuture<List<CSSProperty>>) handler.invoke(
                this,
                DomainCommand.CSS_getLonghandProperties,
                CRT_GET_LONGHAND_PROPERTIES,
                PARAMS_GET_LONGHAND_PROPERTIES_1,
                new Object[] {shorthandName, value},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetMatchedStylesForNodeResult> getMatchedStylesForNode(Integer nodeId) {
        return (CompletableFuture<GetMatchedStylesForNodeResult>) handler.invoke(
                this,
                DomainCommand.CSS_getMatchedStylesForNode,
                CRT_GET_MATCHED_STYLES_FOR_NODE,
                PARAMS_GET_MATCHED_STYLES_FOR_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<CSSMedia>> getMediaQueries() {
        return (CompletableFuture<List<CSSMedia>>) handler.invoke(
                this, DomainCommand.CSS_getMediaQueries, CRT_GET_MEDIA_QUERIES, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<PlatformFontUsage>> getPlatformFontsForNode(Integer nodeId) {
        return (CompletableFuture<List<PlatformFontUsage>>) handler.invoke(
                this,
                DomainCommand.CSS_getPlatformFontsForNode,
                CRT_GET_PLATFORM_FONTS_FOR_NODE,
                PARAMS_GET_PLATFORM_FONTS_FOR_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getStyleSheetText(String styleSheetId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.CSS_getStyleSheetText,
                CRT_GET_STYLE_SHEET_TEXT,
                PARAMS_GET_STYLE_SHEET_TEXT_1,
                new Object[] {styleSheetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> resolveValues(List<String> values, Integer nodeId) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.CSS_resolveValues,
                CRT_RESOLVE_VALUES,
                PARAMS_RESOLVE_VALUES_1,
                new Object[] {values, nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> resolveValues(
            List<String> values, Integer nodeId, String propertyName, PseudoType pseudoType, String pseudoIdentifier) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.CSS_resolveValues,
                CRT_RESOLVE_VALUES,
                PARAMS_RESOLVE_VALUES_2,
                new Object[] {values, nodeId, propertyName, pseudoType, pseudoIdentifier},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CSSContainerQuery> setContainerQueryText(
            String styleSheetId, SourceRange range, String text) {
        return (CompletableFuture<CSSContainerQuery>) handler.invoke(
                this,
                DomainCommand.CSS_setContainerQueryText,
                CRT_SET_CONTAINER_QUERY_TEXT,
                PARAMS_SET_CONTAINER_QUERY_TEXT_1,
                new Object[] {styleSheetId, range, text},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEffectivePropertyValueForNode(Integer nodeId, String propertyName, String value) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_setEffectivePropertyValueForNode,
                CRT_SET_EFFECTIVE_PROPERTY_VALUE_FOR_NODE,
                PARAMS_SET_EFFECTIVE_PROPERTY_VALUE_FOR_NODE_1,
                new Object[] {nodeId, propertyName, value},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Value> setKeyframeKey(String styleSheetId, SourceRange range, String keyText) {
        return (CompletableFuture<Value>) handler.invoke(
                this,
                DomainCommand.CSS_setKeyframeKey,
                CRT_SET_KEYFRAME_KEY,
                PARAMS_SET_KEYFRAME_KEY_1,
                new Object[] {styleSheetId, range, keyText},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setLocalFontsEnabled(Boolean enabled) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_setLocalFontsEnabled,
                CRT_SET_LOCAL_FONTS_ENABLED,
                PARAMS_SET_LOCAL_FONTS_ENABLED_1,
                new Object[] {enabled},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CSSMedia> setMediaText(String styleSheetId, SourceRange range, String text) {
        return (CompletableFuture<CSSMedia>) handler.invoke(
                this,
                DomainCommand.CSS_setMediaText,
                CRT_SET_MEDIA_TEXT,
                PARAMS_SET_MEDIA_TEXT_1,
                new Object[] {styleSheetId, range, text},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Value> setPropertyRulePropertyName(
            String styleSheetId, SourceRange range, String propertyName) {
        return (CompletableFuture<Value>) handler.invoke(
                this,
                DomainCommand.CSS_setPropertyRulePropertyName,
                CRT_SET_PROPERTY_RULE_PROPERTY_NAME,
                PARAMS_SET_PROPERTY_RULE_PROPERTY_NAME_1,
                new Object[] {styleSheetId, range, propertyName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<SelectorList> setRuleSelector(String styleSheetId, SourceRange range, String selector) {
        return (CompletableFuture<SelectorList>) handler.invoke(
                this,
                DomainCommand.CSS_setRuleSelector,
                CRT_SET_RULE_SELECTOR,
                PARAMS_SET_RULE_SELECTOR_1,
                new Object[] {styleSheetId, range, selector},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CSSScope> setScopeText(String styleSheetId, SourceRange range, String text) {
        return (CompletableFuture<CSSScope>) handler.invoke(
                this,
                DomainCommand.CSS_setScopeText,
                CRT_SET_SCOPE_TEXT,
                PARAMS_SET_SCOPE_TEXT_1,
                new Object[] {styleSheetId, range, text},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> setStyleSheetText(String styleSheetId, String text) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.CSS_setStyleSheetText,
                CRT_SET_STYLE_SHEET_TEXT,
                PARAMS_SET_STYLE_SHEET_TEXT_1,
                new Object[] {styleSheetId, text},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<CSSStyle>> setStyleTexts(List<StyleDeclarationEdit> edits) {
        return (CompletableFuture<List<CSSStyle>>) handler.invoke(
                this,
                DomainCommand.CSS_setStyleTexts,
                CRT_SET_STYLE_TEXTS,
                PARAMS_SET_STYLE_TEXTS_1,
                new Object[] {edits},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<CSSStyle>> setStyleTexts(
            List<StyleDeclarationEdit> edits, Integer nodeForPropertySyntaxValidation) {
        return (CompletableFuture<List<CSSStyle>>) handler.invoke(
                this,
                DomainCommand.CSS_setStyleTexts,
                CRT_SET_STYLE_TEXTS,
                PARAMS_SET_STYLE_TEXTS_2,
                new Object[] {edits, nodeForPropertySyntaxValidation},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CSSSupports> setSupportsText(String styleSheetId, SourceRange range, String text) {
        return (CompletableFuture<CSSSupports>) handler.invoke(
                this,
                DomainCommand.CSS_setSupportsText,
                CRT_SET_SUPPORTS_TEXT,
                PARAMS_SET_SUPPORTS_TEXT_1,
                new Object[] {styleSheetId, range, text},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> startRuleUsageTracking() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_startRuleUsageTracking,
                CRT_START_RULE_USAGE_TRACKING,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<RuleUsage>> stopRuleUsageTracking() {
        return (CompletableFuture<List<RuleUsage>>) handler.invoke(
                this,
                DomainCommand.CSS_stopRuleUsageTracking,
                CRT_STOP_RULE_USAGE_TRACKING,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> takeComputedStyleUpdates() {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.CSS_takeComputedStyleUpdates,
                CRT_TAKE_COMPUTED_STYLE_UPDATES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<TakeCoverageDeltaResult> takeCoverageDelta() {
        return (CompletableFuture<TakeCoverageDeltaResult>) handler.invoke(
                this, DomainCommand.CSS_takeCoverageDelta, CRT_TAKE_COVERAGE_DELTA, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trackComputedStyleUpdates(List<CSSComputedStyleProperty> propertiesToTrack) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_trackComputedStyleUpdates,
                CRT_TRACK_COMPUTED_STYLE_UPDATES,
                PARAMS_TRACK_COMPUTED_STYLE_UPDATES_1,
                new Object[] {propertiesToTrack},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trackComputedStyleUpdatesForNode() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_trackComputedStyleUpdatesForNode,
                CRT_TRACK_COMPUTED_STYLE_UPDATES_FOR_NODE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> trackComputedStyleUpdatesForNode(Integer nodeId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.CSS_trackComputedStyleUpdatesForNode,
                CRT_TRACK_COMPUTED_STYLE_UPDATES_FOR_NODE,
                PARAMS_TRACK_COMPUTED_STYLE_UPDATES_FOR_NODE_2,
                new Object[] {nodeId},
                false);
    }
}
