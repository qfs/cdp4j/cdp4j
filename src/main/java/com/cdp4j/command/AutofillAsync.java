// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.autofill.Address;
import com.cdp4j.type.autofill.CreditCard;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Defines commands and events for Autofill.
 */
@Experimental
public interface AutofillAsync extends ParameterizedCommand<AutofillAsync> {
    /**
     * Disables autofill domain notifications.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables autofill domain notifications.
     */
    CompletableFuture<Void> enable();

    /**
     * Set addresses so that developers can verify their forms implementation.
     *
     */
    CompletableFuture<Void> setAddresses(List<Address> addresses);

    /**
     * Trigger autofill on a form identified by the fieldId. If the field and
     * related form cannot be autofilled, returns an error.
     *
     * @param fieldId
     *            Identifies a field that serves as an anchor for autofill.
     * @param card
     *            Credit card information to fill out the form. Credit card data is
     *            not saved.
     */
    CompletableFuture<Void> trigger(Integer fieldId, CreditCard card);

    /**
     * Trigger autofill on a form identified by the fieldId. If the field and
     * related form cannot be autofilled, returns an error.
     *
     * @param fieldId
     *            Identifies a field that serves as an anchor for autofill.
     * @param frameId
     *            Identifies the frame that field belongs to.
     * @param card
     *            Credit card information to fill out the form. Credit card data is
     *            not saved.
     */
    CompletableFuture<Void> trigger(Integer fieldId, @Optional String frameId, CreditCard card);
}
