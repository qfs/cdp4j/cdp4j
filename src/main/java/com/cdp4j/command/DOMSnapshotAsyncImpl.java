// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.domsnapshot.CaptureSnapshotResult;
import com.cdp4j.type.domsnapshot.GetSnapshotResult;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class DOMSnapshotAsyncImpl extends ParameterizedCommandImpl<DOMSnapshotAsync> implements DOMSnapshotAsync {

    private static final CommandReturnType CRT_CAPTURE_SNAPSHOT =
            new CommandReturnType(null, CaptureSnapshotResult.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_SNAPSHOT =
            new CommandReturnType(null, GetSnapshotResult.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CAPTURE_SNAPSHOT_1 = new String[] {"computedStyles"};
    private static final String[] PARAMS_CAPTURE_SNAPSHOT_2 = new String[] {
        "computedStyles",
        "includePaintOrder",
        "includeDOMRects",
        "includeBlendedBackgroundColors",
        "includeTextColorOpacities"
    };
    private static final String[] PARAMS_GET_SNAPSHOT_1 = new String[] {"computedStyleWhitelist"};
    private static final String[] PARAMS_GET_SNAPSHOT_2 = new String[] {
        "computedStyleWhitelist", "includeEventListeners", "includePaintOrder", "includeUserAgentShadowTree"
    };
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DOMSnapshotAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CaptureSnapshotResult> captureSnapshot(List<String> computedStyles) {
        return (CompletableFuture<CaptureSnapshotResult>) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_captureSnapshot,
                CRT_CAPTURE_SNAPSHOT,
                PARAMS_CAPTURE_SNAPSHOT_1,
                new Object[] {computedStyles},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<CaptureSnapshotResult> captureSnapshot(
            List<String> computedStyles,
            Boolean includePaintOrder,
            Boolean includeDOMRects,
            Boolean includeBlendedBackgroundColors,
            Boolean includeTextColorOpacities) {
        return (CompletableFuture<CaptureSnapshotResult>) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_captureSnapshot,
                CRT_CAPTURE_SNAPSHOT,
                PARAMS_CAPTURE_SNAPSHOT_2,
                new Object[] {
                    computedStyles,
                    includePaintOrder,
                    includeDOMRects,
                    includeBlendedBackgroundColors,
                    includeTextColorOpacities
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOMSnapshot_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOMSnapshot_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetSnapshotResult> getSnapshot(List<String> computedStyleWhitelist) {
        return (CompletableFuture<GetSnapshotResult>) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_getSnapshot,
                CRT_GET_SNAPSHOT,
                PARAMS_GET_SNAPSHOT_1,
                new Object[] {computedStyleWhitelist},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetSnapshotResult> getSnapshot(
            List<String> computedStyleWhitelist,
            Boolean includeEventListeners,
            Boolean includePaintOrder,
            Boolean includeUserAgentShadowTree) {
        return (CompletableFuture<GetSnapshotResult>) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_getSnapshot,
                CRT_GET_SNAPSHOT,
                PARAMS_GET_SNAPSHOT_2,
                new Object[] {
                    computedStyleWhitelist, includeEventListeners, includePaintOrder, includeUserAgentShadowTree
                },
                false);
    }
}
