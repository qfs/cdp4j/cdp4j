// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;

@java.lang.SuppressWarnings("deprecation")
class ConsoleImpl extends ParameterizedCommandImpl<Console> implements Console {

    private static final CommandReturnType CRT_CLEAR_MESSAGES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public ConsoleImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void clearMessages() {
        handler.invoke(this, DomainCommand.Console_clearMessages, CRT_CLEAR_MESSAGES, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Console_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Console_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }
}
