// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.domstorage.StorageId;
import java.util.List;

/**
 * Query and modify DOM storage.
 */
@Experimental
public interface DOMStorage extends ParameterizedCommand<DOMStorage> {
    void clear(StorageId storageId);

    /**
     * Disables storage tracking, prevents storage events from being sent to the client.
     */
    void disable();

    /**
     * Enables storage tracking, storage events will now be delivered to the client.
     */
    void enable();

    List<List<String>> getDOMStorageItems(StorageId storageId);

    void removeDOMStorageItem(StorageId storageId, String key);

    void setDOMStorageItem(StorageId storageId, String key, String value);
}
