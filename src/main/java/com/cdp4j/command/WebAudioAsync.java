// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.webaudio.ContextRealtimeData;
import java.util.concurrent.CompletableFuture;

/**
 * This domain allows inspection of Web Audio API.
 * https://webaudio.github.io/web-audio-api/
 */
@Experimental
public interface WebAudioAsync extends ParameterizedCommand<WebAudioAsync> {
    /**
     * Disables the WebAudio domain.
     */
    CompletableFuture<Void> disable();

    /**
     * Enables the WebAudio domain and starts sending context lifetime events.
     */
    CompletableFuture<Void> enable();

    /**
     * Fetch the realtime data from the registered contexts.
     *
     */
    CompletableFuture<ContextRealtimeData> getRealtimeData(String contextId);
}
