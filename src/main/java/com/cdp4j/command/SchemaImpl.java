// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.schema.Domain;
import java.util.List;

class SchemaImpl extends ParameterizedCommandImpl<Schema> implements Schema {

    private static final TypeReference<List<Domain>> LIST_DOMAIN = new TypeReference<List<Domain>>() {};
    private static final CommandReturnType CRT_GET_DOMAINS = new CommandReturnType("domains", List.class, LIST_DOMAIN);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public SchemaImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<Domain> getDomains() {
        return (List<Domain>)
                handler.invoke(this, DomainCommand.Schema_getDomains, CRT_GET_DOMAINS, EMPTY_ARGS, EMPTY_VALUES, true);
    }
}
