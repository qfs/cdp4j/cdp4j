// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import java.util.concurrent.CompletableFuture;

/**
 * A domain for interacting with Cast, Presentation API, and Remote Playback API
 * functionalities.
 */
@Experimental
public interface CastAsync extends ParameterizedCommand<CastAsync> {
    /**
     * Stops observing for sinks and issues.
     */
    CompletableFuture<Void> disable();

    /**
     * Starts observing for sinks that can be used for tab mirroring, and if set,
     * sinks compatible with |presentationUrl| as well. When sinks are found, a
     * |sinksUpdated| event is fired. Also starts observing for issue messages. When
     * an issue is added or removed, an |issueUpdated| event is fired.
     */
    CompletableFuture<Void> enable();

    /**
     * Starts observing for sinks that can be used for tab mirroring, and if set,
     * sinks compatible with |presentationUrl| as well. When sinks are found, a
     * |sinksUpdated| event is fired. Also starts observing for issue messages. When
     * an issue is added or removed, an |issueUpdated| event is fired.
     *
     */
    CompletableFuture<Void> enable(@Optional String presentationUrl);

    /**
     * Sets a sink to be used when the web page requests the browser to choose a
     * sink via Presentation API, Remote Playback API, or Cast SDK.
     *
     */
    CompletableFuture<Void> setSinkToUse(String sinkName);

    /**
     * Starts mirroring the desktop to the sink.
     *
     */
    CompletableFuture<Void> startDesktopMirroring(String sinkName);

    /**
     * Starts mirroring the tab to the sink.
     *
     */
    CompletableFuture<Void> startTabMirroring(String sinkName);

    /**
     * Stops the active Cast session on the sink.
     *
     */
    CompletableFuture<Void> stopCasting(String sinkName);
}
