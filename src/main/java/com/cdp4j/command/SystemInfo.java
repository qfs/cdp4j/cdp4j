// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.systeminfo.GetInfoResult;
import com.cdp4j.type.systeminfo.ProcessInfo;
import java.util.List;

/**
 * The SystemInfo domain defines methods and events for querying low-level system information.
 */
@Experimental
public interface SystemInfo extends ParameterizedCommand<SystemInfo> {
    /**
     * Returns information about the feature state.
     *
     */
    Boolean getFeatureState(String featureState);

    /**
     * Returns information about the system.
     *
     * @return GetInfoResult
     */
    GetInfoResult getInfo();

    /**
     * Returns information about all running processes.
     *
     * @return An array of process info blocks.
     */
    List<ProcessInfo> getProcessInfo();
}
