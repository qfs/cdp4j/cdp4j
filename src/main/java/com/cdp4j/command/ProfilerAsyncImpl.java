// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.profiler.Profile;
import com.cdp4j.type.profiler.ScriptCoverage;
import com.cdp4j.type.profiler.TakePreciseCoverageResult;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class ProfilerAsyncImpl extends ParameterizedCommandImpl<ProfilerAsync> implements ProfilerAsync {

    private static final TypeReference<List<ScriptCoverage>> LIST_SCRIPTCOVERAGE =
            new TypeReference<List<ScriptCoverage>>() {};
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_BEST_EFFORT_COVERAGE =
            new CommandReturnType("result", List.class, LIST_SCRIPTCOVERAGE);
    private static final CommandReturnType CRT_SET_SAMPLING_INTERVAL = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_START_PRECISE_COVERAGE =
            new CommandReturnType("timestamp", Double.class, null);
    private static final CommandReturnType CRT_STOP = new CommandReturnType("profile", Profile.class, null);
    private static final CommandReturnType CRT_STOP_PRECISE_COVERAGE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TAKE_PRECISE_COVERAGE =
            new CommandReturnType(null, TakePreciseCoverageResult.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_SET_SAMPLING_INTERVAL_1 = new String[] {"interval"};
    private static final String[] PARAMS_START_PRECISE_COVERAGE_2 =
            new String[] {"callCount", "detailed", "allowTriggeredUpdates"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public ProfilerAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Profiler_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Profiler_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<ScriptCoverage>> getBestEffortCoverage() {
        return (CompletableFuture<List<ScriptCoverage>>) handler.invoke(
                this,
                DomainCommand.Profiler_getBestEffortCoverage,
                CRT_GET_BEST_EFFORT_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setSamplingInterval(Integer interval) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Profiler_setSamplingInterval,
                CRT_SET_SAMPLING_INTERVAL,
                PARAMS_SET_SAMPLING_INTERVAL_1,
                new Object[] {interval},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> start() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Profiler_start, CRT_START, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Double> startPreciseCoverage() {
        return (CompletableFuture<Double>) handler.invoke(
                this,
                DomainCommand.Profiler_startPreciseCoverage,
                CRT_START_PRECISE_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Double> startPreciseCoverage(
            Boolean callCount, Boolean detailed, Boolean allowTriggeredUpdates) {
        return (CompletableFuture<Double>) handler.invoke(
                this,
                DomainCommand.Profiler_startPreciseCoverage,
                CRT_START_PRECISE_COVERAGE,
                PARAMS_START_PRECISE_COVERAGE_2,
                new Object[] {callCount, detailed, allowTriggeredUpdates},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Profile> stop() {
        return (CompletableFuture<Profile>)
                handler.invoke(this, DomainCommand.Profiler_stop, CRT_STOP, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> stopPreciseCoverage() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Profiler_stopPreciseCoverage,
                CRT_STOP_PRECISE_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<TakePreciseCoverageResult> takePreciseCoverage() {
        return (CompletableFuture<TakePreciseCoverageResult>) handler.invoke(
                this,
                DomainCommand.Profiler_takePreciseCoverage,
                CRT_TAKE_PRECISE_COVERAGE,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }
}
