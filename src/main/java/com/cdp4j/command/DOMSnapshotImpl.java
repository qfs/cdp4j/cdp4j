// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.domsnapshot.CaptureSnapshotResult;
import com.cdp4j.type.domsnapshot.GetSnapshotResult;
import java.util.List;

class DOMSnapshotImpl extends ParameterizedCommandImpl<DOMSnapshot> implements DOMSnapshot {

    private static final CommandReturnType CRT_CAPTURE_SNAPSHOT =
            new CommandReturnType(null, CaptureSnapshotResult.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_SNAPSHOT =
            new CommandReturnType(null, GetSnapshotResult.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CAPTURE_SNAPSHOT_1 = new String[] {"computedStyles"};
    private static final String[] PARAMS_CAPTURE_SNAPSHOT_2 = new String[] {
        "computedStyles",
        "includePaintOrder",
        "includeDOMRects",
        "includeBlendedBackgroundColors",
        "includeTextColorOpacities"
    };
    private static final String[] PARAMS_GET_SNAPSHOT_1 = new String[] {"computedStyleWhitelist"};
    private static final String[] PARAMS_GET_SNAPSHOT_2 = new String[] {
        "computedStyleWhitelist", "includeEventListeners", "includePaintOrder", "includeUserAgentShadowTree"
    };
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DOMSnapshotImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public CaptureSnapshotResult captureSnapshot(List<String> computedStyles) {
        return (CaptureSnapshotResult) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_captureSnapshot,
                CRT_CAPTURE_SNAPSHOT,
                PARAMS_CAPTURE_SNAPSHOT_1,
                new Object[] {computedStyles},
                true);
    }

    @Override
    public CaptureSnapshotResult captureSnapshot(
            List<String> computedStyles,
            Boolean includePaintOrder,
            Boolean includeDOMRects,
            Boolean includeBlendedBackgroundColors,
            Boolean includeTextColorOpacities) {
        return (CaptureSnapshotResult) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_captureSnapshot,
                CRT_CAPTURE_SNAPSHOT,
                PARAMS_CAPTURE_SNAPSHOT_2,
                new Object[] {
                    computedStyles,
                    includePaintOrder,
                    includeDOMRects,
                    includeBlendedBackgroundColors,
                    includeTextColorOpacities
                },
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.DOMSnapshot_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.DOMSnapshot_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public GetSnapshotResult getSnapshot(List<String> computedStyleWhitelist) {
        return (GetSnapshotResult) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_getSnapshot,
                CRT_GET_SNAPSHOT,
                PARAMS_GET_SNAPSHOT_1,
                new Object[] {computedStyleWhitelist},
                true);
    }

    @Override
    public GetSnapshotResult getSnapshot(
            List<String> computedStyleWhitelist,
            Boolean includeEventListeners,
            Boolean includePaintOrder,
            Boolean includeUserAgentShadowTree) {
        return (GetSnapshotResult) handler.invoke(
                this,
                DomainCommand.DOMSnapshot_getSnapshot,
                CRT_GET_SNAPSHOT,
                PARAMS_GET_SNAPSHOT_2,
                new Object[] {
                    computedStyleWhitelist, includeEventListeners, includePaintOrder, includeUserAgentShadowTree
                },
                true);
    }
}
