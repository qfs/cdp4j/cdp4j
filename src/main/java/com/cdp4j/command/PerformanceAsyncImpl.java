// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.TimeDomain;
import com.cdp4j.type.performance.Metric;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class PerformanceAsyncImpl extends ParameterizedCommandImpl<PerformanceAsync> implements PerformanceAsync {

    private static final TypeReference<List<Metric>> LIST_METRIC = new TypeReference<List<Metric>>() {};
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_METRICS = new CommandReturnType("metrics", List.class, LIST_METRIC);
    private static final CommandReturnType CRT_SET_TIME_DOMAIN = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"timeDomain"};
    private static final String[] PARAMS_SET_TIME_DOMAIN_1 = new String[] {"timeDomain"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public PerformanceAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Performance_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.Performance_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable(TimeDomain timeDomain) {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Performance_enable, CRT_ENABLE, PARAMS_ENABLE_2, new Object[] {timeDomain}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Metric>> getMetrics() {
        return (CompletableFuture<List<Metric>>) handler.invoke(
                this, DomainCommand.Performance_getMetrics, CRT_GET_METRICS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setTimeDomain(TimeDomain timeDomain) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Performance_setTimeDomain,
                CRT_SET_TIME_DOMAIN,
                PARAMS_SET_TIME_DOMAIN_1,
                new Object[] {timeDomain},
                false);
    }
}
