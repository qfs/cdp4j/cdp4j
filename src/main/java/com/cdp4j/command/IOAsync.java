// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.io.ReadResult;
import java.util.concurrent.CompletableFuture;

/**
 * Input/Output operations for streams produced by DevTools.
 */
public interface IOAsync extends ParameterizedCommand<IOAsync> {
    /**
     * Close the stream, discard any temporary backing storage.
     *
     * @param handle
     *            Handle of the stream to close.
     */
    CompletableFuture<Void> close(String handle);

    /**
     * Read a chunk of the stream
     *
     * @param handle
     *            Handle of the stream to read.
     *
     * @return ReadResult
     */
    CompletableFuture<ReadResult> read(String handle);

    /**
     * Read a chunk of the stream
     *
     * @param handle
     *            Handle of the stream to read.
     * @param offset
     *            Seek to the specified offset before reading (if not specified,
     *            proceed with offset following the last read). Some types of
     *            streams may only support sequential reads.
     * @param size
     *            Maximum number of bytes to read (left upon the agent discretion if
     *            not specified).
     *
     * @return ReadResult
     */
    CompletableFuture<ReadResult> read(String handle, @Optional Integer offset, @Optional Integer size);

    /**
     * Return UUID of Blob object specified by a remote object id.
     *
     * @param objectId
     *            Object id of a Blob object wrapper.
     *
     * @return UUID of the specified Blob.
     */
    CompletableFuture<String> resolveBlob(String objectId);
}
