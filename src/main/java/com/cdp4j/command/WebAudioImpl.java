// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.webaudio.ContextRealtimeData;

class WebAudioImpl extends ParameterizedCommandImpl<WebAudio> implements WebAudio {

    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_REALTIME_DATA =
            new CommandReturnType("realtimeData", ContextRealtimeData.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_GET_REALTIME_DATA_1 = new String[] {"contextId"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public WebAudioImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.WebAudio_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.WebAudio_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public ContextRealtimeData getRealtimeData(String contextId) {
        return (ContextRealtimeData) handler.invoke(
                this,
                DomainCommand.WebAudio_getRealtimeData,
                CRT_GET_REALTIME_DATA,
                PARAMS_GET_REALTIME_DATA_1,
                new Object[] {contextId},
                true);
    }
}
