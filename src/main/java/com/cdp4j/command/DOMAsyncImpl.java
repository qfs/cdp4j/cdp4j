// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.ElementRelation;
import com.cdp4j.type.constant.IncludeWhitespace;
import com.cdp4j.type.dom.BoxModel;
import com.cdp4j.type.dom.CSSComputedStyleProperty;
import com.cdp4j.type.dom.DetachedElementInfo;
import com.cdp4j.type.dom.GetFrameOwnerResult;
import com.cdp4j.type.dom.GetNodeForLocationResult;
import com.cdp4j.type.dom.LogicalAxes;
import com.cdp4j.type.dom.Node;
import com.cdp4j.type.dom.PerformSearchResult;
import com.cdp4j.type.dom.PhysicalAxes;
import com.cdp4j.type.dom.Rect;
import com.cdp4j.type.runtime.RemoteObject;
import com.cdp4j.type.runtime.StackTrace;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class DOMAsyncImpl extends ParameterizedCommandImpl<DOMAsync> implements DOMAsync {

    private static final TypeReference<List<DetachedElementInfo>> LIST_DETACHEDELEMENTINFO =
            new TypeReference<List<DetachedElementInfo>>() {};
    private static final TypeReference<List<Integer>> LIST_INTEGER = new TypeReference<List<Integer>>() {};
    private static final TypeReference<List<List<Double>>> LIST_LIST_DOUBLE =
            new TypeReference<List<List<Double>>>() {};
    private static final TypeReference<List<Node>> LIST_NODE = new TypeReference<List<Node>>() {};
    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_COLLECT_CLASS_NAMES_FROM_SUBTREE =
            new CommandReturnType("classNames", List.class, LIST_STRING);
    private static final CommandReturnType CRT_COPY_TO = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_DESCRIBE_NODE = new CommandReturnType("node", Node.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISCARD_SEARCH_RESULTS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_FOCUS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_ANCHOR_ELEMENT =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_ATTRIBUTES =
            new CommandReturnType("attributes", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_BOX_MODEL = new CommandReturnType("model", BoxModel.class, null);
    private static final CommandReturnType CRT_GET_CONTAINER_FOR_NODE =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_CONTENT_QUADS =
            new CommandReturnType("quads", List.class, LIST_LIST_DOUBLE);
    private static final CommandReturnType CRT_GET_DETACHED_DOM_NODES =
            new CommandReturnType("detachedNodes", List.class, LIST_DETACHEDELEMENTINFO);
    private static final CommandReturnType CRT_GET_DOCUMENT = new CommandReturnType("root", Node.class, null);
    private static final CommandReturnType CRT_GET_ELEMENT_BY_RELATION =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_FILE_INFO = new CommandReturnType("path", String.class, null);
    private static final CommandReturnType CRT_GET_FLATTENED_DOCUMENT =
            new CommandReturnType("nodes", List.class, LIST_NODE);
    private static final CommandReturnType CRT_GET_FRAME_OWNER =
            new CommandReturnType(null, GetFrameOwnerResult.class, null);
    private static final CommandReturnType CRT_GET_NODES_FOR_SUBTREE_BY_STYLE =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_GET_NODE_FOR_LOCATION =
            new CommandReturnType(null, GetNodeForLocationResult.class, null);
    private static final CommandReturnType CRT_GET_NODE_STACK_TRACES =
            new CommandReturnType("creation", StackTrace.class, null);
    private static final CommandReturnType CRT_GET_OUTER_HT_ML = new CommandReturnType("outerHTML", String.class, null);
    private static final CommandReturnType CRT_GET_QUERYING_DESCENDANTS_FOR_CONTAINER =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_GET_RELAYOUT_BOUNDARY =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_GET_SEARCH_RESULTS =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_GET_TOP_LAYER_ELEMENTS =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_HIDE_HIGHLIGHT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_NODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_HIGHLIGHT_RECT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_MARK_UNDOABLE_STATE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_MOVE_TO = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_PERFORM_SEARCH =
            new CommandReturnType(null, PerformSearchResult.class, null);
    private static final CommandReturnType CRT_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_PUSH_NODE_BY_PATH_TO_FRONTEND =
            new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_QUERY_SELECTOR = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_QUERY_SELECTOR_ALL =
            new CommandReturnType("nodeIds", List.class, LIST_INTEGER);
    private static final CommandReturnType CRT_REDO = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_ATTRIBUTE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_NODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REQUEST_CHILD_NODES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REQUEST_NODE = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_RESOLVE_NODE = new CommandReturnType("object", RemoteObject.class, null);
    private static final CommandReturnType CRT_SCROLL_INTO_VIEW_IF_NEEDED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ATTRIBUTES_AS_TEXT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_ATTRIBUTE_VALUE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_FILE_INPUT_FILES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INSPECTED_NODE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_NODE_NAME = new CommandReturnType("nodeId", Integer.class, null);
    private static final CommandReturnType CRT_SET_NODE_STACK_TRACES_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_NODE_VALUE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_OUTER_HT_ML = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNDO = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_COLLECT_CLASS_NAMES_FROM_SUBTREE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_COPY_TO_1 = new String[] {"nodeId", "targetNodeId"};
    private static final String[] PARAMS_COPY_TO_2 = new String[] {"nodeId", "targetNodeId", "insertBeforeNodeId"};
    private static final String[] PARAMS_DESCRIBE_NODE_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "depth", "pierce"};
    private static final String[] PARAMS_DISCARD_SEARCH_RESULTS_1 = new String[] {"searchId"};
    private static final String[] PARAMS_ENABLE_2 = new String[] {"includeWhitespace"};
    private static final String[] PARAMS_FOCUS_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_ANCHOR_ELEMENT_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_ANCHOR_ELEMENT_2 = new String[] {"nodeId", "anchorSpecifier"};
    private static final String[] PARAMS_GET_ATTRIBUTES_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_BOX_MODEL_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_CONTAINER_FOR_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_CONTAINER_FOR_NODE_2 =
            new String[] {"nodeId", "containerName", "physicalAxes", "logicalAxes", "queriesScrollState"};
    private static final String[] PARAMS_GET_CONTENT_QUADS_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_DOCUMENT_2 = new String[] {"depth", "pierce"};
    private static final String[] PARAMS_GET_ELEMENT_BY_RELATION_1 = new String[] {"nodeId", "relation"};
    private static final String[] PARAMS_GET_FILE_INFO_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_FLATTENED_DOCUMENT_2 = new String[] {"depth", "pierce"};
    private static final String[] PARAMS_GET_FRAME_OWNER_1 = new String[] {"frameId"};
    private static final String[] PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_1 = new String[] {"nodeId", "computedStyles"};
    private static final String[] PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_2 =
            new String[] {"nodeId", "computedStyles", "pierce"};
    private static final String[] PARAMS_GET_NODE_FOR_LOCATION_1 = new String[] {"x", "y"};
    private static final String[] PARAMS_GET_NODE_FOR_LOCATION_2 =
            new String[] {"x", "y", "includeUserAgentShadowDOM", "ignorePointerEventsNone"};
    private static final String[] PARAMS_GET_NODE_STACK_TRACES_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_OUTER_HT_ML_2 = new String[] {"nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_GET_QUERYING_DESCENDANTS_FOR_CONTAINER_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_RELAYOUT_BOUNDARY_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_GET_SEARCH_RESULTS_1 = new String[] {"searchId", "fromIndex", "toIndex"};
    private static final String[] PARAMS_MOVE_TO_1 = new String[] {"nodeId", "targetNodeId"};
    private static final String[] PARAMS_MOVE_TO_2 = new String[] {"nodeId", "targetNodeId", "insertBeforeNodeId"};
    private static final String[] PARAMS_PERFORM_SEARCH_1 = new String[] {"query"};
    private static final String[] PARAMS_PERFORM_SEARCH_2 = new String[] {"query", "includeUserAgentShadowDOM"};
    private static final String[] PARAMS_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND_1 = new String[] {"backendNodeIds"};
    private static final String[] PARAMS_PUSH_NODE_BY_PATH_TO_FRONTEND_1 = new String[] {"path"};
    private static final String[] PARAMS_QUERY_SELECTOR_1 = new String[] {"nodeId", "selector"};
    private static final String[] PARAMS_QUERY_SELECTOR_ALL_1 = new String[] {"nodeId", "selector"};
    private static final String[] PARAMS_REMOVE_ATTRIBUTE_1 = new String[] {"nodeId", "name"};
    private static final String[] PARAMS_REMOVE_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_REQUEST_CHILD_NODES_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_REQUEST_CHILD_NODES_2 = new String[] {"nodeId", "depth", "pierce"};
    private static final String[] PARAMS_REQUEST_NODE_1 = new String[] {"objectId"};
    private static final String[] PARAMS_RESOLVE_NODE_2 =
            new String[] {"nodeId", "backendNodeId", "objectGroup", "executionContextId"};
    private static final String[] PARAMS_SCROLL_INTO_VIEW_IF_NEEDED_2 =
            new String[] {"nodeId", "backendNodeId", "objectId", "rect"};
    private static final String[] PARAMS_SET_ATTRIBUTES_AS_TEXT_1 = new String[] {"nodeId", "text"};
    private static final String[] PARAMS_SET_ATTRIBUTES_AS_TEXT_2 = new String[] {"nodeId", "text", "name"};
    private static final String[] PARAMS_SET_ATTRIBUTE_VALUE_1 = new String[] {"nodeId", "name", "value"};
    private static final String[] PARAMS_SET_FILE_INPUT_FILES_1 = new String[] {"files"};
    private static final String[] PARAMS_SET_FILE_INPUT_FILES_2 =
            new String[] {"files", "nodeId", "backendNodeId", "objectId"};
    private static final String[] PARAMS_SET_INSPECTED_NODE_1 = new String[] {"nodeId"};
    private static final String[] PARAMS_SET_NODE_NAME_1 = new String[] {"nodeId", "name"};
    private static final String[] PARAMS_SET_NODE_STACK_TRACES_ENABLED_1 = new String[] {"enable"};
    private static final String[] PARAMS_SET_NODE_VALUE_1 = new String[] {"nodeId", "value"};
    private static final String[] PARAMS_SET_OUTER_HT_ML_1 = new String[] {"nodeId", "outerHTML"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DOMAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> collectClassNamesFromSubtree(Integer nodeId) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.DOM_collectClassNamesFromSubtree,
                CRT_COLLECT_CLASS_NAMES_FROM_SUBTREE,
                PARAMS_COLLECT_CLASS_NAMES_FROM_SUBTREE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> copyTo(Integer nodeId, Integer targetNodeId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_copyTo,
                CRT_COPY_TO,
                PARAMS_COPY_TO_1,
                new Object[] {nodeId, targetNodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> copyTo(Integer nodeId, Integer targetNodeId, Integer insertBeforeNodeId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_copyTo,
                CRT_COPY_TO,
                PARAMS_COPY_TO_2,
                new Object[] {nodeId, targetNodeId, insertBeforeNodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Node> describeNode() {
        return (CompletableFuture<Node>) handler.invoke(
                this, DomainCommand.DOM_describeNode, CRT_DESCRIBE_NODE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Node> describeNode(
            Integer nodeId, Integer backendNodeId, String objectId, Integer depth, Boolean pierce) {
        return (CompletableFuture<Node>) handler.invoke(
                this,
                DomainCommand.DOM_describeNode,
                CRT_DESCRIBE_NODE,
                PARAMS_DESCRIBE_NODE_2,
                new Object[] {nodeId, backendNodeId, objectId, depth, pierce},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOM_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> discardSearchResults(String searchId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_discardSearchResults,
                CRT_DISCARD_SEARCH_RESULTS,
                PARAMS_DISCARD_SEARCH_RESULTS_1,
                new Object[] {searchId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOM_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> enable(IncludeWhitespace includeWhitespace) {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.DOM_enable, CRT_ENABLE, PARAMS_ENABLE_2, new Object[] {includeWhitespace}, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> focus() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOM_focus, CRT_FOCUS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> focus(Integer nodeId, Integer backendNodeId, String objectId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_focus,
                CRT_FOCUS,
                PARAMS_FOCUS_2,
                new Object[] {nodeId, backendNodeId, objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> getAnchorElement(Integer nodeId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getAnchorElement,
                CRT_GET_ANCHOR_ELEMENT,
                PARAMS_GET_ANCHOR_ELEMENT_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> getAnchorElement(Integer nodeId, String anchorSpecifier) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getAnchorElement,
                CRT_GET_ANCHOR_ELEMENT,
                PARAMS_GET_ANCHOR_ELEMENT_2,
                new Object[] {nodeId, anchorSpecifier},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> getAttributes(Integer nodeId) {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.DOM_getAttributes,
                CRT_GET_ATTRIBUTES,
                PARAMS_GET_ATTRIBUTES_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<BoxModel> getBoxModel() {
        return (CompletableFuture<BoxModel>)
                handler.invoke(this, DomainCommand.DOM_getBoxModel, CRT_GET_BOX_MODEL, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<BoxModel> getBoxModel(Integer nodeId, Integer backendNodeId, String objectId) {
        return (CompletableFuture<BoxModel>) handler.invoke(
                this,
                DomainCommand.DOM_getBoxModel,
                CRT_GET_BOX_MODEL,
                PARAMS_GET_BOX_MODEL_2,
                new Object[] {nodeId, backendNodeId, objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> getContainerForNode(Integer nodeId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getContainerForNode,
                CRT_GET_CONTAINER_FOR_NODE,
                PARAMS_GET_CONTAINER_FOR_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> getContainerForNode(
            Integer nodeId,
            String containerName,
            PhysicalAxes physicalAxes,
            LogicalAxes logicalAxes,
            Boolean queriesScrollState) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getContainerForNode,
                CRT_GET_CONTAINER_FOR_NODE,
                PARAMS_GET_CONTAINER_FOR_NODE_2,
                new Object[] {nodeId, containerName, physicalAxes, logicalAxes, queriesScrollState},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<List<Double>>> getContentQuads() {
        return (CompletableFuture<List<List<Double>>>) handler.invoke(
                this, DomainCommand.DOM_getContentQuads, CRT_GET_CONTENT_QUADS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<List<Double>>> getContentQuads(
            Integer nodeId, Integer backendNodeId, String objectId) {
        return (CompletableFuture<List<List<Double>>>) handler.invoke(
                this,
                DomainCommand.DOM_getContentQuads,
                CRT_GET_CONTENT_QUADS,
                PARAMS_GET_CONTENT_QUADS_2,
                new Object[] {nodeId, backendNodeId, objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<DetachedElementInfo>> getDetachedDomNodes() {
        return (CompletableFuture<List<DetachedElementInfo>>) handler.invoke(
                this,
                DomainCommand.DOM_getDetachedDomNodes,
                CRT_GET_DETACHED_DOM_NODES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Node> getDocument() {
        return (CompletableFuture<Node>)
                handler.invoke(this, DomainCommand.DOM_getDocument, CRT_GET_DOCUMENT, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Node> getDocument(Integer depth, Boolean pierce) {
        return (CompletableFuture<Node>) handler.invoke(
                this,
                DomainCommand.DOM_getDocument,
                CRT_GET_DOCUMENT,
                PARAMS_GET_DOCUMENT_2,
                new Object[] {depth, pierce},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> getElementByRelation(Integer nodeId, ElementRelation relation) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getElementByRelation,
                CRT_GET_ELEMENT_BY_RELATION,
                PARAMS_GET_ELEMENT_BY_RELATION_1,
                new Object[] {nodeId, relation},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getFileInfo(String objectId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.DOM_getFileInfo,
                CRT_GET_FILE_INFO,
                PARAMS_GET_FILE_INFO_1,
                new Object[] {objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Node>> getFlattenedDocument() {
        return (CompletableFuture<List<Node>>) handler.invoke(
                this,
                DomainCommand.DOM_getFlattenedDocument,
                CRT_GET_FLATTENED_DOCUMENT,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Node>> getFlattenedDocument(Integer depth, Boolean pierce) {
        return (CompletableFuture<List<Node>>) handler.invoke(
                this,
                DomainCommand.DOM_getFlattenedDocument,
                CRT_GET_FLATTENED_DOCUMENT,
                PARAMS_GET_FLATTENED_DOCUMENT_2,
                new Object[] {depth, pierce},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetFrameOwnerResult> getFrameOwner(String frameId) {
        return (CompletableFuture<GetFrameOwnerResult>) handler.invoke(
                this,
                DomainCommand.DOM_getFrameOwner,
                CRT_GET_FRAME_OWNER,
                PARAMS_GET_FRAME_OWNER_1,
                new Object[] {frameId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetNodeForLocationResult> getNodeForLocation(Integer x, Integer y) {
        return (CompletableFuture<GetNodeForLocationResult>) handler.invoke(
                this,
                DomainCommand.DOM_getNodeForLocation,
                CRT_GET_NODE_FOR_LOCATION,
                PARAMS_GET_NODE_FOR_LOCATION_1,
                new Object[] {x, y},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<GetNodeForLocationResult> getNodeForLocation(
            Integer x, Integer y, Boolean includeUserAgentShadowDOM, Boolean ignorePointerEventsNone) {
        return (CompletableFuture<GetNodeForLocationResult>) handler.invoke(
                this,
                DomainCommand.DOM_getNodeForLocation,
                CRT_GET_NODE_FOR_LOCATION,
                PARAMS_GET_NODE_FOR_LOCATION_2,
                new Object[] {x, y, includeUserAgentShadowDOM, ignorePointerEventsNone},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<StackTrace> getNodeStackTraces(Integer nodeId) {
        return (CompletableFuture<StackTrace>) handler.invoke(
                this,
                DomainCommand.DOM_getNodeStackTraces,
                CRT_GET_NODE_STACK_TRACES,
                PARAMS_GET_NODE_STACK_TRACES_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> getNodesForSubtreeByStyle(
            Integer nodeId, List<CSSComputedStyleProperty> computedStyles) {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.DOM_getNodesForSubtreeByStyle,
                CRT_GET_NODES_FOR_SUBTREE_BY_STYLE,
                PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_1,
                new Object[] {nodeId, computedStyles},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> getNodesForSubtreeByStyle(
            Integer nodeId, List<CSSComputedStyleProperty> computedStyles, Boolean pierce) {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.DOM_getNodesForSubtreeByStyle,
                CRT_GET_NODES_FOR_SUBTREE_BY_STYLE,
                PARAMS_GET_NODES_FOR_SUBTREE_BY_STYLE_2,
                new Object[] {nodeId, computedStyles, pierce},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getOuterHTML() {
        return (CompletableFuture<String>) handler.invoke(
                this, DomainCommand.DOM_getOuterHTML, CRT_GET_OUTER_HT_ML, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> getOuterHTML(Integer nodeId, Integer backendNodeId, String objectId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.DOM_getOuterHTML,
                CRT_GET_OUTER_HT_ML,
                PARAMS_GET_OUTER_HT_ML_2,
                new Object[] {nodeId, backendNodeId, objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> getQueryingDescendantsForContainer(Integer nodeId) {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.DOM_getQueryingDescendantsForContainer,
                CRT_GET_QUERYING_DESCENDANTS_FOR_CONTAINER,
                PARAMS_GET_QUERYING_DESCENDANTS_FOR_CONTAINER_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> getRelayoutBoundary(Integer nodeId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_getRelayoutBoundary,
                CRT_GET_RELAYOUT_BOUNDARY,
                PARAMS_GET_RELAYOUT_BOUNDARY_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> getSearchResults(String searchId, Integer fromIndex, Integer toIndex) {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.DOM_getSearchResults,
                CRT_GET_SEARCH_RESULTS,
                PARAMS_GET_SEARCH_RESULTS_1,
                new Object[] {searchId, fromIndex, toIndex},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> getTopLayerElements() {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.DOM_getTopLayerElements,
                CRT_GET_TOP_LAYER_ELEMENTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> hideHighlight() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.DOM_hideHighlight, CRT_HIDE_HIGHLIGHT, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> highlightNode() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.DOM_highlightNode, CRT_HIGHLIGHT_NODE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> highlightRect() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.DOM_highlightRect, CRT_HIGHLIGHT_RECT, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> markUndoableState() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.DOM_markUndoableState, CRT_MARK_UNDOABLE_STATE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> moveTo(Integer nodeId, Integer targetNodeId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_moveTo,
                CRT_MOVE_TO,
                PARAMS_MOVE_TO_1,
                new Object[] {nodeId, targetNodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> moveTo(Integer nodeId, Integer targetNodeId, Integer insertBeforeNodeId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_moveTo,
                CRT_MOVE_TO,
                PARAMS_MOVE_TO_2,
                new Object[] {nodeId, targetNodeId, insertBeforeNodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<PerformSearchResult> performSearch(String query) {
        return (CompletableFuture<PerformSearchResult>) handler.invoke(
                this,
                DomainCommand.DOM_performSearch,
                CRT_PERFORM_SEARCH,
                PARAMS_PERFORM_SEARCH_1,
                new Object[] {query},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<PerformSearchResult> performSearch(String query, Boolean includeUserAgentShadowDOM) {
        return (CompletableFuture<PerformSearchResult>) handler.invoke(
                this,
                DomainCommand.DOM_performSearch,
                CRT_PERFORM_SEARCH,
                PARAMS_PERFORM_SEARCH_2,
                new Object[] {query, includeUserAgentShadowDOM},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> pushNodeByPathToFrontend(String path) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_pushNodeByPathToFrontend,
                CRT_PUSH_NODE_BY_PATH_TO_FRONTEND,
                PARAMS_PUSH_NODE_BY_PATH_TO_FRONTEND_1,
                new Object[] {path},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> pushNodesByBackendIdsToFrontend(List<Integer> backendNodeIds) {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.DOM_pushNodesByBackendIdsToFrontend,
                CRT_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND,
                PARAMS_PUSH_NODES_BY_BACKEND_IDS_TO_FRONTEND_1,
                new Object[] {backendNodeIds},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> querySelector(Integer nodeId, String selector) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_querySelector,
                CRT_QUERY_SELECTOR,
                PARAMS_QUERY_SELECTOR_1,
                new Object[] {nodeId, selector},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<Integer>> querySelectorAll(Integer nodeId, String selector) {
        return (CompletableFuture<List<Integer>>) handler.invoke(
                this,
                DomainCommand.DOM_querySelectorAll,
                CRT_QUERY_SELECTOR_ALL,
                PARAMS_QUERY_SELECTOR_ALL_1,
                new Object[] {nodeId, selector},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> redo() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOM_redo, CRT_REDO, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeAttribute(Integer nodeId, String name) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_removeAttribute,
                CRT_REMOVE_ATTRIBUTE,
                PARAMS_REMOVE_ATTRIBUTE_1,
                new Object[] {nodeId, name},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeNode(Integer nodeId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_removeNode,
                CRT_REMOVE_NODE,
                PARAMS_REMOVE_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> requestChildNodes(Integer nodeId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_requestChildNodes,
                CRT_REQUEST_CHILD_NODES,
                PARAMS_REQUEST_CHILD_NODES_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> requestChildNodes(Integer nodeId, Integer depth, Boolean pierce) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_requestChildNodes,
                CRT_REQUEST_CHILD_NODES,
                PARAMS_REQUEST_CHILD_NODES_2,
                new Object[] {nodeId, depth, pierce},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> requestNode(String objectId) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_requestNode,
                CRT_REQUEST_NODE,
                PARAMS_REQUEST_NODE_1,
                new Object[] {objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RemoteObject> resolveNode() {
        return (CompletableFuture<RemoteObject>)
                handler.invoke(this, DomainCommand.DOM_resolveNode, CRT_RESOLVE_NODE, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<RemoteObject> resolveNode(
            Integer nodeId, Integer backendNodeId, String objectGroup, Integer executionContextId) {
        return (CompletableFuture<RemoteObject>) handler.invoke(
                this,
                DomainCommand.DOM_resolveNode,
                CRT_RESOLVE_NODE,
                PARAMS_RESOLVE_NODE_2,
                new Object[] {nodeId, backendNodeId, objectGroup, executionContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> scrollIntoViewIfNeeded() {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_scrollIntoViewIfNeeded,
                CRT_SCROLL_INTO_VIEW_IF_NEEDED,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> scrollIntoViewIfNeeded(
            Integer nodeId, Integer backendNodeId, String objectId, Rect rect) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_scrollIntoViewIfNeeded,
                CRT_SCROLL_INTO_VIEW_IF_NEEDED,
                PARAMS_SCROLL_INTO_VIEW_IF_NEEDED_2,
                new Object[] {nodeId, backendNodeId, objectId, rect},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAttributeValue(Integer nodeId, String name, String value) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setAttributeValue,
                CRT_SET_ATTRIBUTE_VALUE,
                PARAMS_SET_ATTRIBUTE_VALUE_1,
                new Object[] {nodeId, name, value},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAttributesAsText(Integer nodeId, String text) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setAttributesAsText,
                CRT_SET_ATTRIBUTES_AS_TEXT,
                PARAMS_SET_ATTRIBUTES_AS_TEXT_1,
                new Object[] {nodeId, text},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAttributesAsText(Integer nodeId, String text, String name) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setAttributesAsText,
                CRT_SET_ATTRIBUTES_AS_TEXT,
                PARAMS_SET_ATTRIBUTES_AS_TEXT_2,
                new Object[] {nodeId, text, name},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setFileInputFiles(List<String> files) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setFileInputFiles,
                CRT_SET_FILE_INPUT_FILES,
                PARAMS_SET_FILE_INPUT_FILES_1,
                new Object[] {files},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setFileInputFiles(
            List<String> files, Integer nodeId, Integer backendNodeId, String objectId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setFileInputFiles,
                CRT_SET_FILE_INPUT_FILES,
                PARAMS_SET_FILE_INPUT_FILES_2,
                new Object[] {files, nodeId, backendNodeId, objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setInspectedNode(Integer nodeId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setInspectedNode,
                CRT_SET_INSPECTED_NODE,
                PARAMS_SET_INSPECTED_NODE_1,
                new Object[] {nodeId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Integer> setNodeName(Integer nodeId, String name) {
        return (CompletableFuture<Integer>) handler.invoke(
                this,
                DomainCommand.DOM_setNodeName,
                CRT_SET_NODE_NAME,
                PARAMS_SET_NODE_NAME_1,
                new Object[] {nodeId, name},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setNodeStackTracesEnabled(Boolean enable) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setNodeStackTracesEnabled,
                CRT_SET_NODE_STACK_TRACES_ENABLED,
                PARAMS_SET_NODE_STACK_TRACES_ENABLED_1,
                new Object[] {enable},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setNodeValue(Integer nodeId, String value) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setNodeValue,
                CRT_SET_NODE_VALUE,
                PARAMS_SET_NODE_VALUE_1,
                new Object[] {nodeId, value},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setOuterHTML(Integer nodeId, String outerHTML) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOM_setOuterHTML,
                CRT_SET_OUTER_HT_ML,
                PARAMS_SET_OUTER_HT_ML_1,
                new Object[] {nodeId, outerHTML},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> undo() {
        return (CompletableFuture<Void>)
                handler.invoke(this, DomainCommand.DOM_undo, CRT_UNDO, EMPTY_ARGS, EMPTY_VALUES, false);
    }
}
