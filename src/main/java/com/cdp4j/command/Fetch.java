// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.type.fetch.AuthChallengeResponse;
import com.cdp4j.type.fetch.GetResponseBodyResult;
import com.cdp4j.type.fetch.HeaderEntry;
import com.cdp4j.type.fetch.RequestPattern;
import com.cdp4j.type.network.ErrorReason;
import java.util.List;

/**
 * A domain for letting clients substitute browser's network layer with client code.
 */
public interface Fetch extends ParameterizedCommand<Fetch> {
    /**
     * Continues the request, optionally modifying some of its parameters.
     *
     * @param requestId An id the client received in requestPaused event.
     */
    void continueRequest(String requestId);

    /**
     * Continues the request, optionally modifying some of its parameters.
     *
     * @param requestId An id the client received in requestPaused event.
     * @param url If set, the request url will be modified in a way that's not observable by page.
     * @param method If set, the request method is overridden.
     * @param postData If set, overrides the post data in the request. (Encoded as a base64 string when passed over JSON)
     * @param headers If set, overrides the request headers. Note that the overrides do not
     * extend to subsequent redirect hops, if a redirect happens. Another override
     * may be applied to a different request produced by a redirect.
     * @param interceptResponse If set, overrides response interception behavior for this request.
     */
    void continueRequest(
            String requestId,
            @Optional String url,
            @Optional String method,
            @Optional String postData,
            @Optional List<HeaderEntry> headers,
            @Experimental @Optional Boolean interceptResponse);

    /**
     * Continues loading of the paused response, optionally modifying the
     * response headers. If either responseCode or headers are modified, all of them
     * must be present.
     *
     * @param requestId An id the client received in requestPaused event.
     */
    @Experimental
    void continueResponse(String requestId);

    /**
     * Continues loading of the paused response, optionally modifying the
     * response headers. If either responseCode or headers are modified, all of them
     * must be present.
     *
     * @param requestId An id the client received in requestPaused event.
     * @param responseCode An HTTP response code. If absent, original response code will be used.
     * @param responsePhrase A textual representation of responseCode.
     * If absent, a standard phrase matching responseCode is used.
     * @param responseHeaders Response headers. If absent, original response headers will be used.
     * @param binaryResponseHeaders Alternative way of specifying response headers as a \0-separated
     * series of name: value pairs. Prefer the above method unless you
     * need to represent some non-UTF8 values that can't be transmitted
     * over the protocol as text. (Encoded as a base64 string when passed over JSON)
     */
    @Experimental
    void continueResponse(
            String requestId,
            @Optional Integer responseCode,
            @Optional String responsePhrase,
            @Optional List<HeaderEntry> responseHeaders,
            @Optional String binaryResponseHeaders);

    /**
     * Continues a request supplying authChallengeResponse following authRequired event.
     *
     * @param requestId An id the client received in authRequired event.
     * @param authChallengeResponse Response to  with an authChallenge.
     */
    void continueWithAuth(String requestId, AuthChallengeResponse authChallengeResponse);

    /**
     * Disables the fetch domain.
     */
    void disable();

    /**
     * Enables issuing of requestPaused events. A request will be paused until client
     * calls one of failRequest, fulfillRequest or continueRequest/continueWithAuth.
     */
    void enable();

    /**
     * Enables issuing of requestPaused events. A request will be paused until client
     * calls one of failRequest, fulfillRequest or continueRequest/continueWithAuth.
     *
     * @param patterns If specified, only requests matching any of these patterns will produce
     * fetchRequested event and will be paused until clients response. If not set,
     * all requests will be affected.
     * @param handleAuthRequests If true, authRequired events will be issued and requests will be paused
     * expecting a call to continueWithAuth.
     */
    void enable(@Optional List<RequestPattern> patterns, @Optional Boolean handleAuthRequests);

    /**
     * Causes the request to fail with specified reason.
     *
     * @param requestId An id the client received in requestPaused event.
     * @param errorReason Causes the request to fail with the given reason.
     */
    void failRequest(String requestId, ErrorReason errorReason);

    /**
     * Provides response to the request.
     *
     * @param requestId An id the client received in requestPaused event.
     * @param responseCode An HTTP response code.
     */
    void fulfillRequest(String requestId, Integer responseCode);

    /**
     * Provides response to the request.
     *
     * @param requestId An id the client received in requestPaused event.
     * @param responseCode An HTTP response code.
     * @param responseHeaders Response headers.
     * @param binaryResponseHeaders Alternative way of specifying response headers as a \0-separated
     * series of name: value pairs. Prefer the above method unless you
     * need to represent some non-UTF8 values that can't be transmitted
     * over the protocol as text. (Encoded as a base64 string when passed over JSON)
     * @param body A response body. If absent, original response body will be used if
     * the request is intercepted at the response stage and empty body
     * will be used if the request is intercepted at the request stage. (Encoded as a base64 string when passed over JSON)
     * @param responsePhrase A textual representation of responseCode.
     * If absent, a standard phrase matching responseCode is used.
     */
    void fulfillRequest(
            String requestId,
            Integer responseCode,
            @Optional List<HeaderEntry> responseHeaders,
            @Optional String binaryResponseHeaders,
            @Optional String body,
            @Optional String responsePhrase);

    /**
     * Causes the body of the response to be received from the server and
     * returned as a single string. May only be issued for a request that
     * is paused in the Response stage and is mutually exclusive with
     * takeResponseBodyForInterceptionAsStream. Calling other methods that
     * affect the request or disabling fetch domain before body is received
     * results in an undefined behavior.
     * Note that the response body is not available for redirects. Requests
     * paused in the _redirect received_ state may be differentiated by
     * responseCode and presence of location response header, see
     * comments to requestPaused for details.
     *
     * @param requestId Identifier for the intercepted request to get body for.
     *
     * @return GetResponseBodyResult
     */
    GetResponseBodyResult getResponseBody(String requestId);

    /**
     * Returns a handle to the stream representing the response body.
     * The request must be paused in the HeadersReceived stage.
     * Note that after this command the request can't be continued
     * as is -- client either needs to cancel it or to provide the
     * response body.
     * The stream only supports sequential read, IO.read will fail if the position
     * is specified.
     * This method is mutually exclusive with getResponseBody.
     * Calling other methods that affect the request or disabling fetch
     * domain before body is received results in an undefined behavior.
     *
     */
    String takeResponseBodyAsStream(String requestId);
}
