// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.target.FilterEntry;
import com.cdp4j.type.target.RemoteLocation;
import com.cdp4j.type.target.TargetInfo;
import com.cdp4j.type.target.WindowState;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class TargetAsyncImpl extends ParameterizedCommandImpl<TargetAsync> implements TargetAsync {

    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final TypeReference<List<TargetInfo>> LIST_TARGETINFO = new TypeReference<List<TargetInfo>>() {};
    private static final CommandReturnType CRT_ACTIVATE_TARGET = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ATTACH_TO_BROWSER_TARGET =
            new CommandReturnType("sessionId", String.class, null);
    private static final CommandReturnType CRT_ATTACH_TO_TARGET =
            new CommandReturnType("sessionId", String.class, null);
    private static final CommandReturnType CRT_AUTO_ATTACH_RELATED = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_CLOSE_TARGET = new CommandReturnType("success", Boolean.class, null);
    private static final CommandReturnType CRT_CREATE_BROWSER_CONTEXT =
            new CommandReturnType("browserContextId", String.class, null);
    private static final CommandReturnType CRT_CREATE_TARGET = new CommandReturnType("targetId", String.class, null);
    private static final CommandReturnType CRT_DETACH_FROM_TARGET = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPOSE_BROWSER_CONTEXT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EXPOSE_DEV_TOOLS_PROTOCOL =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_GET_BROWSER_CONTEXTS =
            new CommandReturnType("browserContextIds", List.class, LIST_STRING);
    private static final CommandReturnType CRT_GET_TARGETS =
            new CommandReturnType("targetInfos", List.class, LIST_TARGETINFO);
    private static final CommandReturnType CRT_GET_TARGET_INFO =
            new CommandReturnType("targetInfo", TargetInfo.class, null);
    private static final CommandReturnType CRT_SEND_MESSAGE_TO_TARGET = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_AUTO_ATTACH = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DISCOVER_TARGETS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_REMOTE_LOCATIONS = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ACTIVATE_TARGET_1 = new String[] {"targetId"};
    private static final String[] PARAMS_ATTACH_TO_TARGET_1 = new String[] {"targetId"};
    private static final String[] PARAMS_ATTACH_TO_TARGET_2 = new String[] {"targetId", "flatten"};
    private static final String[] PARAMS_AUTO_ATTACH_RELATED_1 = new String[] {"targetId", "waitForDebuggerOnStart"};
    private static final String[] PARAMS_AUTO_ATTACH_RELATED_2 =
            new String[] {"targetId", "waitForDebuggerOnStart", "filter"};
    private static final String[] PARAMS_CLOSE_TARGET_1 = new String[] {"targetId"};
    private static final String[] PARAMS_CREATE_BROWSER_CONTEXT_2 =
            new String[] {"disposeOnDetach", "proxyServer", "proxyBypassList", "originsWithUniversalNetworkAccess"};
    private static final String[] PARAMS_CREATE_TARGET_1 = new String[] {"url"};
    private static final String[] PARAMS_CREATE_TARGET_2 = new String[] {
        "url",
        "left",
        "top",
        "width",
        "height",
        "windowState",
        "browserContextId",
        "enableBeginFrameControl",
        "newWindow",
        "background",
        "forTab"
    };
    private static final String[] PARAMS_DETACH_FROM_TARGET_2 = new String[] {"sessionId", "targetId"};
    private static final String[] PARAMS_DISPOSE_BROWSER_CONTEXT_1 = new String[] {"browserContextId"};
    private static final String[] PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_1 = new String[] {"targetId"};
    private static final String[] PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_2 =
            new String[] {"targetId", "bindingName", "inheritPermissions"};
    private static final String[] PARAMS_GET_TARGETS_2 = new String[] {"filter"};
    private static final String[] PARAMS_GET_TARGET_INFO_2 = new String[] {"targetId"};
    private static final String[] PARAMS_SEND_MESSAGE_TO_TARGET_1 = new String[] {"message"};
    private static final String[] PARAMS_SEND_MESSAGE_TO_TARGET_2 = new String[] {"message", "sessionId", "targetId"};
    private static final String[] PARAMS_SET_AUTO_ATTACH_1 = new String[] {"autoAttach", "waitForDebuggerOnStart"};
    private static final String[] PARAMS_SET_AUTO_ATTACH_2 =
            new String[] {"autoAttach", "waitForDebuggerOnStart", "flatten", "filter"};
    private static final String[] PARAMS_SET_DISCOVER_TARGETS_1 = new String[] {"discover"};
    private static final String[] PARAMS_SET_DISCOVER_TARGETS_2 = new String[] {"discover", "filter"};
    private static final String[] PARAMS_SET_REMOTE_LOCATIONS_1 = new String[] {"locations"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public TargetAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> activateTarget(String targetId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_activateTarget,
                CRT_ACTIVATE_TARGET,
                PARAMS_ACTIVATE_TARGET_1,
                new Object[] {targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> attachToBrowserTarget() {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Target_attachToBrowserTarget,
                CRT_ATTACH_TO_BROWSER_TARGET,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> attachToTarget(String targetId) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Target_attachToTarget,
                CRT_ATTACH_TO_TARGET,
                PARAMS_ATTACH_TO_TARGET_1,
                new Object[] {targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> attachToTarget(String targetId, Boolean flatten) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Target_attachToTarget,
                CRT_ATTACH_TO_TARGET,
                PARAMS_ATTACH_TO_TARGET_2,
                new Object[] {targetId, flatten},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> autoAttachRelated(String targetId, Boolean waitForDebuggerOnStart) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_autoAttachRelated,
                CRT_AUTO_ATTACH_RELATED,
                PARAMS_AUTO_ATTACH_RELATED_1,
                new Object[] {targetId, waitForDebuggerOnStart},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> autoAttachRelated(
            String targetId, Boolean waitForDebuggerOnStart, List<FilterEntry> filter) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_autoAttachRelated,
                CRT_AUTO_ATTACH_RELATED,
                PARAMS_AUTO_ATTACH_RELATED_2,
                new Object[] {targetId, waitForDebuggerOnStart, filter},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Boolean> closeTarget(String targetId) {
        return (CompletableFuture<Boolean>) handler.invoke(
                this,
                DomainCommand.Target_closeTarget,
                CRT_CLOSE_TARGET,
                PARAMS_CLOSE_TARGET_1,
                new Object[] {targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> createBrowserContext() {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Target_createBrowserContext,
                CRT_CREATE_BROWSER_CONTEXT,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> createBrowserContext(
            Boolean disposeOnDetach,
            String proxyServer,
            String proxyBypassList,
            List<String> originsWithUniversalNetworkAccess) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Target_createBrowserContext,
                CRT_CREATE_BROWSER_CONTEXT,
                PARAMS_CREATE_BROWSER_CONTEXT_2,
                new Object[] {disposeOnDetach, proxyServer, proxyBypassList, originsWithUniversalNetworkAccess},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> createTarget(String url) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Target_createTarget,
                CRT_CREATE_TARGET,
                PARAMS_CREATE_TARGET_1,
                new Object[] {url},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> createTarget(
            String url,
            Integer left,
            Integer top,
            Integer width,
            Integer height,
            WindowState windowState,
            String browserContextId,
            Boolean enableBeginFrameControl,
            Boolean newWindow,
            Boolean background,
            Boolean forTab) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Target_createTarget,
                CRT_CREATE_TARGET,
                PARAMS_CREATE_TARGET_2,
                new Object[] {
                    url,
                    left,
                    top,
                    width,
                    height,
                    windowState,
                    browserContextId,
                    enableBeginFrameControl,
                    newWindow,
                    background,
                    forTab
                },
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> detachFromTarget() {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Target_detachFromTarget, CRT_DETACH_FROM_TARGET, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> detachFromTarget(String sessionId, String targetId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_detachFromTarget,
                CRT_DETACH_FROM_TARGET,
                PARAMS_DETACH_FROM_TARGET_2,
                new Object[] {sessionId, targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> disposeBrowserContext(String browserContextId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_disposeBrowserContext,
                CRT_DISPOSE_BROWSER_CONTEXT,
                PARAMS_DISPOSE_BROWSER_CONTEXT_1,
                new Object[] {browserContextId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> exposeDevToolsProtocol(String targetId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_exposeDevToolsProtocol,
                CRT_EXPOSE_DEV_TOOLS_PROTOCOL,
                PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_1,
                new Object[] {targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> exposeDevToolsProtocol(
            String targetId, String bindingName, Boolean inheritPermissions) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_exposeDevToolsProtocol,
                CRT_EXPOSE_DEV_TOOLS_PROTOCOL,
                PARAMS_EXPOSE_DEV_TOOLS_PROTOCOL_2,
                new Object[] {targetId, bindingName, inheritPermissions},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<String>> getBrowserContexts() {
        return (CompletableFuture<List<String>>) handler.invoke(
                this,
                DomainCommand.Target_getBrowserContexts,
                CRT_GET_BROWSER_CONTEXTS,
                EMPTY_ARGS,
                EMPTY_VALUES,
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<TargetInfo> getTargetInfo() {
        return (CompletableFuture<TargetInfo>) handler.invoke(
                this, DomainCommand.Target_getTargetInfo, CRT_GET_TARGET_INFO, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<TargetInfo> getTargetInfo(String targetId) {
        return (CompletableFuture<TargetInfo>) handler.invoke(
                this,
                DomainCommand.Target_getTargetInfo,
                CRT_GET_TARGET_INFO,
                PARAMS_GET_TARGET_INFO_2,
                new Object[] {targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<TargetInfo>> getTargets() {
        return (CompletableFuture<List<TargetInfo>>)
                handler.invoke(this, DomainCommand.Target_getTargets, CRT_GET_TARGETS, EMPTY_ARGS, EMPTY_VALUES, false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<TargetInfo>> getTargets(List<FilterEntry> filter) {
        return (CompletableFuture<List<TargetInfo>>) handler.invoke(
                this,
                DomainCommand.Target_getTargets,
                CRT_GET_TARGETS,
                PARAMS_GET_TARGETS_2,
                new Object[] {filter},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> sendMessageToTarget(String message) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_sendMessageToTarget,
                CRT_SEND_MESSAGE_TO_TARGET,
                PARAMS_SEND_MESSAGE_TO_TARGET_1,
                new Object[] {message},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> sendMessageToTarget(String message, String sessionId, String targetId) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_sendMessageToTarget,
                CRT_SEND_MESSAGE_TO_TARGET,
                PARAMS_SEND_MESSAGE_TO_TARGET_2,
                new Object[] {message, sessionId, targetId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAutoAttach(Boolean autoAttach, Boolean waitForDebuggerOnStart) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_setAutoAttach,
                CRT_SET_AUTO_ATTACH,
                PARAMS_SET_AUTO_ATTACH_1,
                new Object[] {autoAttach, waitForDebuggerOnStart},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setAutoAttach(
            Boolean autoAttach, Boolean waitForDebuggerOnStart, Boolean flatten, List<FilterEntry> filter) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_setAutoAttach,
                CRT_SET_AUTO_ATTACH,
                PARAMS_SET_AUTO_ATTACH_2,
                new Object[] {autoAttach, waitForDebuggerOnStart, flatten, filter},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDiscoverTargets(Boolean discover) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_setDiscoverTargets,
                CRT_SET_DISCOVER_TARGETS,
                PARAMS_SET_DISCOVER_TARGETS_1,
                new Object[] {discover},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDiscoverTargets(Boolean discover, List<FilterEntry> filter) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_setDiscoverTargets,
                CRT_SET_DISCOVER_TARGETS,
                PARAMS_SET_DISCOVER_TARGETS_2,
                new Object[] {discover, filter},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setRemoteLocations(List<RemoteLocation> locations) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Target_setRemoteLocations,
                CRT_SET_REMOTE_LOCATIONS,
                PARAMS_SET_REMOTE_LOCATIONS_1,
                new Object[] {locations},
                false);
    }
}
