// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.constant.DragEvent;
import com.cdp4j.type.constant.KeyEventType;
import com.cdp4j.type.constant.MouseEventType;
import com.cdp4j.type.constant.PointerType;
import com.cdp4j.type.constant.TouchEventType;
import com.cdp4j.type.input.DragData;
import com.cdp4j.type.input.GestureSourceType;
import com.cdp4j.type.input.MouseButton;
import com.cdp4j.type.input.TouchPoint;
import java.util.List;

class InputImpl extends ParameterizedCommandImpl<Input> implements Input {

    private static final CommandReturnType CRT_CANCEL_DRAGGING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_DRAG_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_KEY_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_MOUSE_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISPATCH_TOUCH_EVENT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EMULATE_TOUCH_FROM_MOUSE_EVENT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_IME_SET_COMPOSITION = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_INSERT_TEXT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_IGNORE_INPUT_EVENTS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INTERCEPT_DRAGS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SYNTHESIZE_PINCH_GESTURE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SYNTHESIZE_SCROLL_GESTURE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SYNTHESIZE_TAP_GESTURE = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_DISPATCH_DRAG_EVENT_1 = new String[] {"type", "x", "y", "data"};
    private static final String[] PARAMS_DISPATCH_DRAG_EVENT_2 = new String[] {"type", "x", "y", "data", "modifiers"};
    private static final String[] PARAMS_DISPATCH_KEY_EVENT_1 = new String[] {"type"};
    private static final String[] PARAMS_DISPATCH_KEY_EVENT_2 = new String[] {
        "type",
        "modifiers",
        "timestamp",
        "text",
        "unmodifiedText",
        "keyIdentifier",
        "code",
        "key",
        "windowsVirtualKeyCode",
        "nativeVirtualKeyCode",
        "autoRepeat",
        "isKeypad",
        "isSystemKey",
        "location",
        "commands"
    };
    private static final String[] PARAMS_DISPATCH_MOUSE_EVENT_1 = new String[] {"type", "x", "y"};
    private static final String[] PARAMS_DISPATCH_MOUSE_EVENT_2 = new String[] {
        "type",
        "x",
        "y",
        "modifiers",
        "timestamp",
        "button",
        "buttons",
        "clickCount",
        "force",
        "tangentialPressure",
        "tiltX",
        "tiltY",
        "twist",
        "deltaX",
        "deltaY",
        "pointerType"
    };
    private static final String[] PARAMS_DISPATCH_TOUCH_EVENT_1 = new String[] {"type", "touchPoints"};
    private static final String[] PARAMS_DISPATCH_TOUCH_EVENT_2 =
            new String[] {"type", "touchPoints", "modifiers", "timestamp"};
    private static final String[] PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_1 = new String[] {"type", "x", "y", "button"};
    private static final String[] PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_2 =
            new String[] {"type", "x", "y", "button", "timestamp", "deltaX", "deltaY", "modifiers", "clickCount"};
    private static final String[] PARAMS_IME_SET_COMPOSITION_1 =
            new String[] {"text", "selectionStart", "selectionEnd"};
    private static final String[] PARAMS_IME_SET_COMPOSITION_2 =
            new String[] {"text", "selectionStart", "selectionEnd", "replacementStart", "replacementEnd"};
    private static final String[] PARAMS_INSERT_TEXT_1 = new String[] {"text"};
    private static final String[] PARAMS_SET_IGNORE_INPUT_EVENTS_1 = new String[] {"ignore"};
    private static final String[] PARAMS_SET_INTERCEPT_DRAGS_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SYNTHESIZE_PINCH_GESTURE_1 = new String[] {"x", "y", "scaleFactor"};
    private static final String[] PARAMS_SYNTHESIZE_PINCH_GESTURE_2 =
            new String[] {"x", "y", "scaleFactor", "relativeSpeed", "gestureSourceType"};
    private static final String[] PARAMS_SYNTHESIZE_SCROLL_GESTURE_1 = new String[] {"x", "y"};
    private static final String[] PARAMS_SYNTHESIZE_SCROLL_GESTURE_2 = new String[] {
        "x",
        "y",
        "xDistance",
        "yDistance",
        "xOverscroll",
        "yOverscroll",
        "preventFling",
        "speed",
        "gestureSourceType",
        "repeatCount",
        "repeatDelayMs",
        "interactionMarkerName"
    };
    private static final String[] PARAMS_SYNTHESIZE_TAP_GESTURE_1 = new String[] {"x", "y"};
    private static final String[] PARAMS_SYNTHESIZE_TAP_GESTURE_2 =
            new String[] {"x", "y", "duration", "tapCount", "gestureSourceType"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public InputImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void cancelDragging() {
        handler.invoke(this, DomainCommand.Input_cancelDragging, CRT_CANCEL_DRAGGING, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void dispatchDragEvent(DragEvent type, Double x, Double y, DragData data) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchDragEvent,
                CRT_DISPATCH_DRAG_EVENT,
                PARAMS_DISPATCH_DRAG_EVENT_1,
                new Object[] {type, x, y, data},
                true);
    }

    @Override
    public void dispatchDragEvent(DragEvent type, Double x, Double y, DragData data, Integer modifiers) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchDragEvent,
                CRT_DISPATCH_DRAG_EVENT,
                PARAMS_DISPATCH_DRAG_EVENT_2,
                new Object[] {type, x, y, data, modifiers},
                true);
    }

    @Override
    public void dispatchKeyEvent(KeyEventType type) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchKeyEvent,
                CRT_DISPATCH_KEY_EVENT,
                PARAMS_DISPATCH_KEY_EVENT_1,
                new Object[] {type},
                true);
    }

    @Override
    public void dispatchKeyEvent(
            KeyEventType type,
            Integer modifiers,
            Double timestamp,
            String text,
            String unmodifiedText,
            String keyIdentifier,
            String code,
            String key,
            Integer windowsVirtualKeyCode,
            Integer nativeVirtualKeyCode,
            Boolean autoRepeat,
            Boolean isKeypad,
            Boolean isSystemKey,
            Integer location,
            List<String> commands) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchKeyEvent,
                CRT_DISPATCH_KEY_EVENT,
                PARAMS_DISPATCH_KEY_EVENT_2,
                new Object[] {
                    type,
                    modifiers,
                    timestamp,
                    text,
                    unmodifiedText,
                    keyIdentifier,
                    code,
                    key,
                    windowsVirtualKeyCode,
                    nativeVirtualKeyCode,
                    autoRepeat,
                    isKeypad,
                    isSystemKey,
                    location,
                    commands
                },
                true);
    }

    @Override
    public void dispatchMouseEvent(MouseEventType type, Double x, Double y) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchMouseEvent,
                CRT_DISPATCH_MOUSE_EVENT,
                PARAMS_DISPATCH_MOUSE_EVENT_1,
                new Object[] {type, x, y},
                true);
    }

    @Override
    public void dispatchMouseEvent(
            MouseEventType type,
            Double x,
            Double y,
            Integer modifiers,
            Double timestamp,
            MouseButton button,
            Integer buttons,
            Integer clickCount,
            Double force,
            Double tangentialPressure,
            Double tiltX,
            Double tiltY,
            Integer twist,
            Double deltaX,
            Double deltaY,
            PointerType pointerType) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchMouseEvent,
                CRT_DISPATCH_MOUSE_EVENT,
                PARAMS_DISPATCH_MOUSE_EVENT_2,
                new Object[] {
                    type,
                    x,
                    y,
                    modifiers,
                    timestamp,
                    button,
                    buttons,
                    clickCount,
                    force,
                    tangentialPressure,
                    tiltX,
                    tiltY,
                    twist,
                    deltaX,
                    deltaY,
                    pointerType
                },
                true);
    }

    @Override
    public void dispatchTouchEvent(TouchEventType type, List<TouchPoint> touchPoints) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchTouchEvent,
                CRT_DISPATCH_TOUCH_EVENT,
                PARAMS_DISPATCH_TOUCH_EVENT_1,
                new Object[] {type, touchPoints},
                true);
    }

    @Override
    public void dispatchTouchEvent(
            TouchEventType type, List<TouchPoint> touchPoints, Integer modifiers, Double timestamp) {
        handler.invoke(
                this,
                DomainCommand.Input_dispatchTouchEvent,
                CRT_DISPATCH_TOUCH_EVENT,
                PARAMS_DISPATCH_TOUCH_EVENT_2,
                new Object[] {type, touchPoints, modifiers, timestamp},
                true);
    }

    @Override
    public void emulateTouchFromMouseEvent(MouseEventType type, Integer x, Integer y, MouseButton button) {
        handler.invoke(
                this,
                DomainCommand.Input_emulateTouchFromMouseEvent,
                CRT_EMULATE_TOUCH_FROM_MOUSE_EVENT,
                PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_1,
                new Object[] {type, x, y, button},
                true);
    }

    @Override
    public void emulateTouchFromMouseEvent(
            MouseEventType type,
            Integer x,
            Integer y,
            MouseButton button,
            Double timestamp,
            Double deltaX,
            Double deltaY,
            Integer modifiers,
            Integer clickCount) {
        handler.invoke(
                this,
                DomainCommand.Input_emulateTouchFromMouseEvent,
                CRT_EMULATE_TOUCH_FROM_MOUSE_EVENT,
                PARAMS_EMULATE_TOUCH_FROM_MOUSE_EVENT_2,
                new Object[] {type, x, y, button, timestamp, deltaX, deltaY, modifiers, clickCount},
                true);
    }

    @Override
    public void imeSetComposition(String text, Integer selectionStart, Integer selectionEnd) {
        handler.invoke(
                this,
                DomainCommand.Input_imeSetComposition,
                CRT_IME_SET_COMPOSITION,
                PARAMS_IME_SET_COMPOSITION_1,
                new Object[] {text, selectionStart, selectionEnd},
                true);
    }

    @Override
    public void imeSetComposition(
            String text,
            Integer selectionStart,
            Integer selectionEnd,
            Integer replacementStart,
            Integer replacementEnd) {
        handler.invoke(
                this,
                DomainCommand.Input_imeSetComposition,
                CRT_IME_SET_COMPOSITION,
                PARAMS_IME_SET_COMPOSITION_2,
                new Object[] {text, selectionStart, selectionEnd, replacementStart, replacementEnd},
                true);
    }

    @Override
    public void insertText(String text) {
        handler.invoke(
                this, DomainCommand.Input_insertText, CRT_INSERT_TEXT, PARAMS_INSERT_TEXT_1, new Object[] {text}, true);
    }

    @Override
    public void setIgnoreInputEvents(Boolean ignore) {
        handler.invoke(
                this,
                DomainCommand.Input_setIgnoreInputEvents,
                CRT_SET_IGNORE_INPUT_EVENTS,
                PARAMS_SET_IGNORE_INPUT_EVENTS_1,
                new Object[] {ignore},
                true);
    }

    @Override
    public void setInterceptDrags(Boolean enabled) {
        handler.invoke(
                this,
                DomainCommand.Input_setInterceptDrags,
                CRT_SET_INTERCEPT_DRAGS,
                PARAMS_SET_INTERCEPT_DRAGS_1,
                new Object[] {enabled},
                true);
    }

    @Override
    public void synthesizePinchGesture(Double x, Double y, Double scaleFactor) {
        handler.invoke(
                this,
                DomainCommand.Input_synthesizePinchGesture,
                CRT_SYNTHESIZE_PINCH_GESTURE,
                PARAMS_SYNTHESIZE_PINCH_GESTURE_1,
                new Object[] {x, y, scaleFactor},
                true);
    }

    @Override
    public void synthesizePinchGesture(
            Double x, Double y, Double scaleFactor, Integer relativeSpeed, GestureSourceType gestureSourceType) {
        handler.invoke(
                this,
                DomainCommand.Input_synthesizePinchGesture,
                CRT_SYNTHESIZE_PINCH_GESTURE,
                PARAMS_SYNTHESIZE_PINCH_GESTURE_2,
                new Object[] {x, y, scaleFactor, relativeSpeed, gestureSourceType},
                true);
    }

    @Override
    public void synthesizeScrollGesture(Double x, Double y) {
        handler.invoke(
                this,
                DomainCommand.Input_synthesizeScrollGesture,
                CRT_SYNTHESIZE_SCROLL_GESTURE,
                PARAMS_SYNTHESIZE_SCROLL_GESTURE_1,
                new Object[] {x, y},
                true);
    }

    @Override
    public void synthesizeScrollGesture(
            Double x,
            Double y,
            Double xDistance,
            Double yDistance,
            Double xOverscroll,
            Double yOverscroll,
            Boolean preventFling,
            Integer speed,
            GestureSourceType gestureSourceType,
            Integer repeatCount,
            Integer repeatDelayMs,
            String interactionMarkerName) {
        handler.invoke(
                this,
                DomainCommand.Input_synthesizeScrollGesture,
                CRT_SYNTHESIZE_SCROLL_GESTURE,
                PARAMS_SYNTHESIZE_SCROLL_GESTURE_2,
                new Object[] {
                    x,
                    y,
                    xDistance,
                    yDistance,
                    xOverscroll,
                    yOverscroll,
                    preventFling,
                    speed,
                    gestureSourceType,
                    repeatCount,
                    repeatDelayMs,
                    interactionMarkerName
                },
                true);
    }

    @Override
    public void synthesizeTapGesture(Double x, Double y) {
        handler.invoke(
                this,
                DomainCommand.Input_synthesizeTapGesture,
                CRT_SYNTHESIZE_TAP_GESTURE,
                PARAMS_SYNTHESIZE_TAP_GESTURE_1,
                new Object[] {x, y},
                true);
    }

    @Override
    public void synthesizeTapGesture(
            Double x, Double y, Integer duration, Integer tapCount, GestureSourceType gestureSourceType) {
        handler.invoke(
                this,
                DomainCommand.Input_synthesizeTapGesture,
                CRT_SYNTHESIZE_TAP_GESTURE,
                PARAMS_SYNTHESIZE_TAP_GESTURE_2,
                new Object[] {x, y, duration, tapCount, gestureSourceType},
                true);
    }
}
