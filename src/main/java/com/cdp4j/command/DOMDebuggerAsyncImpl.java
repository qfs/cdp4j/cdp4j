// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.domdebugger.CSPViolationType;
import com.cdp4j.type.domdebugger.DOMBreakpointType;
import com.cdp4j.type.domdebugger.EventListener;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class DOMDebuggerAsyncImpl extends ParameterizedCommandImpl<DOMDebuggerAsync> implements DOMDebuggerAsync {

    private static final TypeReference<List<EventListener>> LIST_EVENTLISTENER =
            new TypeReference<List<EventListener>>() {};
    private static final CommandReturnType CRT_GET_EVENT_LISTENERS =
            new CommandReturnType("listeners", List.class, LIST_EVENTLISTENER);
    private static final CommandReturnType CRT_REMOVE_DO_MBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_EVENT_LISTENER_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_XH_RBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_BREAK_ON_CS_PVIOLATION =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_DO_MBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_EVENT_LISTENER_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_INSTRUMENTATION_BREAKPOINT =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_XH_RBREAKPOINT = new CommandReturnType(null, void.class, null);
    private static final String[] PARAMS_GET_EVENT_LISTENERS_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_EVENT_LISTENERS_2 = new String[] {"objectId", "depth", "pierce"};
    private static final String[] PARAMS_REMOVE_DO_MBREAKPOINT_1 = new String[] {"nodeId", "type"};
    private static final String[] PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_2 = new String[] {"eventName", "targetName"};
    private static final String[] PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_REMOVE_XH_RBREAKPOINT_1 = new String[] {"url"};
    private static final String[] PARAMS_SET_BREAK_ON_CS_PVIOLATION_1 = new String[] {"violationTypes"};
    private static final String[] PARAMS_SET_DO_MBREAKPOINT_1 = new String[] {"nodeId", "type"};
    private static final String[] PARAMS_SET_EVENT_LISTENER_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_SET_EVENT_LISTENER_BREAKPOINT_2 = new String[] {"eventName", "targetName"};
    private static final String[] PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1 = new String[] {"eventName"};
    private static final String[] PARAMS_SET_XH_RBREAKPOINT_1 = new String[] {"url"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public DOMDebuggerAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<EventListener>> getEventListeners(String objectId) {
        return (CompletableFuture<List<EventListener>>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_getEventListeners,
                CRT_GET_EVENT_LISTENERS,
                PARAMS_GET_EVENT_LISTENERS_1,
                new Object[] {objectId},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<List<EventListener>> getEventListeners(String objectId, Integer depth, Boolean pierce) {
        return (CompletableFuture<List<EventListener>>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_getEventListeners,
                CRT_GET_EVENT_LISTENERS,
                PARAMS_GET_EVENT_LISTENERS_2,
                new Object[] {objectId, depth, pierce},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeDOMBreakpoint(Integer nodeId, DOMBreakpointType type) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeDOMBreakpoint,
                CRT_REMOVE_DO_MBREAKPOINT,
                PARAMS_REMOVE_DO_MBREAKPOINT_1,
                new Object[] {nodeId, type},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeEventListenerBreakpoint(String eventName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeEventListenerBreakpoint,
                CRT_REMOVE_EVENT_LISTENER_BREAKPOINT,
                PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_1,
                new Object[] {eventName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeEventListenerBreakpoint(String eventName, String targetName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeEventListenerBreakpoint,
                CRT_REMOVE_EVENT_LISTENER_BREAKPOINT,
                PARAMS_REMOVE_EVENT_LISTENER_BREAKPOINT_2,
                new Object[] {eventName, targetName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeInstrumentationBreakpoint(String eventName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeInstrumentationBreakpoint,
                CRT_REMOVE_INSTRUMENTATION_BREAKPOINT,
                PARAMS_REMOVE_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeXHRBreakpoint(String url) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_removeXHRBreakpoint,
                CRT_REMOVE_XH_RBREAKPOINT,
                PARAMS_REMOVE_XH_RBREAKPOINT_1,
                new Object[] {url},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setBreakOnCSPViolation(CSPViolationType violationTypes) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_setBreakOnCSPViolation,
                CRT_SET_BREAK_ON_CS_PVIOLATION,
                PARAMS_SET_BREAK_ON_CS_PVIOLATION_1,
                new Object[] {violationTypes},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setDOMBreakpoint(Integer nodeId, DOMBreakpointType type) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_setDOMBreakpoint,
                CRT_SET_DO_MBREAKPOINT,
                PARAMS_SET_DO_MBREAKPOINT_1,
                new Object[] {nodeId, type},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEventListenerBreakpoint(String eventName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_setEventListenerBreakpoint,
                CRT_SET_EVENT_LISTENER_BREAKPOINT,
                PARAMS_SET_EVENT_LISTENER_BREAKPOINT_1,
                new Object[] {eventName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setEventListenerBreakpoint(String eventName, String targetName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_setEventListenerBreakpoint,
                CRT_SET_EVENT_LISTENER_BREAKPOINT,
                PARAMS_SET_EVENT_LISTENER_BREAKPOINT_2,
                new Object[] {eventName, targetName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setInstrumentationBreakpoint(String eventName) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_setInstrumentationBreakpoint,
                CRT_SET_INSTRUMENTATION_BREAKPOINT,
                PARAMS_SET_INSTRUMENTATION_BREAKPOINT_1,
                new Object[] {eventName},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setXHRBreakpoint(String url) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.DOMDebugger_setXHRBreakpoint,
                CRT_SET_XH_RBREAKPOINT,
                PARAMS_SET_XH_RBREAKPOINT_1,
                new Object[] {url},
                false);
    }
}
