// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.TypeReference;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.runtime.AwaitPromiseResult;
import com.cdp4j.type.runtime.CallArgument;
import com.cdp4j.type.runtime.CallFunctionOnResult;
import com.cdp4j.type.runtime.CompileScriptResult;
import com.cdp4j.type.runtime.EvaluateResult;
import com.cdp4j.type.runtime.ExceptionDetails;
import com.cdp4j.type.runtime.GetHeapUsageResult;
import com.cdp4j.type.runtime.GetPropertiesResult;
import com.cdp4j.type.runtime.RemoteObject;
import com.cdp4j.type.runtime.RunScriptResult;
import com.cdp4j.type.runtime.SerializationOptions;
import java.util.List;

class RuntimeImpl extends ParameterizedCommandImpl<Runtime> implements Runtime {

    private static final TypeReference<List<String>> LIST_STRING = new TypeReference<List<String>>() {};
    private static final CommandReturnType CRT_ADD_BINDING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_AWAIT_PROMISE =
            new CommandReturnType(null, AwaitPromiseResult.class, null);
    private static final CommandReturnType CRT_CALL_FUNCTION_ON =
            new CommandReturnType(null, CallFunctionOnResult.class, null);
    private static final CommandReturnType CRT_COMPILE_SCRIPT =
            new CommandReturnType(null, CompileScriptResult.class, null);
    private static final CommandReturnType CRT_DISABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_DISCARD_CONSOLE_ENTRIES = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_ENABLE = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_EVALUATE = new CommandReturnType(null, EvaluateResult.class, null);
    private static final CommandReturnType CRT_GET_EXCEPTION_DETAILS =
            new CommandReturnType("exceptionDetails", ExceptionDetails.class, null);
    private static final CommandReturnType CRT_GET_HEAP_USAGE =
            new CommandReturnType(null, GetHeapUsageResult.class, null);
    private static final CommandReturnType CRT_GET_ISOLATE_ID = new CommandReturnType("id", String.class, null);
    private static final CommandReturnType CRT_GET_PROPERTIES =
            new CommandReturnType(null, GetPropertiesResult.class, null);
    private static final CommandReturnType CRT_GLOBAL_LEXICAL_SCOPE_NAMES =
            new CommandReturnType("names", List.class, LIST_STRING);
    private static final CommandReturnType CRT_QUERY_OBJECTS =
            new CommandReturnType("objects", RemoteObject.class, null);
    private static final CommandReturnType CRT_RELEASE_OBJECT = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RELEASE_OBJECT_GROUP = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_REMOVE_BINDING = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RUN_IF_WAITING_FOR_DEBUGGER =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_RUN_SCRIPT = new CommandReturnType(null, RunScriptResult.class, null);
    private static final CommandReturnType CRT_SET_ASYNC_CALL_STACK_DEPTH =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_CUSTOM_OBJECT_FORMATTER_ENABLED =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_MAX_CALL_STACK_SIZE_TO_CAPTURE =
            new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_TERMINATE_EXECUTION = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_ADD_BINDING_1 = new String[] {"name"};
    private static final String[] PARAMS_ADD_BINDING_2 =
            new String[] {"name", "executionContextId", "executionContextName"};
    private static final String[] PARAMS_AWAIT_PROMISE_1 = new String[] {"promiseObjectId"};
    private static final String[] PARAMS_AWAIT_PROMISE_2 =
            new String[] {"promiseObjectId", "returnByValue", "generatePreview"};
    private static final String[] PARAMS_CALL_FUNCTION_ON_1 = new String[] {"functionDeclaration"};
    private static final String[] PARAMS_CALL_FUNCTION_ON_2 = new String[] {
        "functionDeclaration",
        "objectId",
        "arguments",
        "silent",
        "returnByValue",
        "generatePreview",
        "userGesture",
        "awaitPromise",
        "executionContextId",
        "objectGroup",
        "throwOnSideEffect",
        "uniqueContextId",
        "serializationOptions"
    };
    private static final String[] PARAMS_COMPILE_SCRIPT_1 = new String[] {"expression", "sourceURL", "persistScript"};
    private static final String[] PARAMS_COMPILE_SCRIPT_2 =
            new String[] {"expression", "sourceURL", "persistScript", "executionContextId"};
    private static final String[] PARAMS_EVALUATE_1 = new String[] {"expression"};
    private static final String[] PARAMS_EVALUATE_2 = new String[] {
        "expression",
        "objectGroup",
        "includeCommandLineAPI",
        "silent",
        "contextId",
        "returnByValue",
        "generatePreview",
        "userGesture",
        "awaitPromise",
        "throwOnSideEffect",
        "timeout",
        "disableBreaks",
        "replMode",
        "allowUnsafeEvalBlockedByCSP",
        "uniqueContextId",
        "serializationOptions"
    };
    private static final String[] PARAMS_GET_EXCEPTION_DETAILS_1 = new String[] {"errorObjectId"};
    private static final String[] PARAMS_GET_PROPERTIES_1 = new String[] {"objectId"};
    private static final String[] PARAMS_GET_PROPERTIES_2 = new String[] {
        "objectId", "ownProperties", "accessorPropertiesOnly", "generatePreview", "nonIndexedPropertiesOnly"
    };
    private static final String[] PARAMS_GLOBAL_LEXICAL_SCOPE_NAMES_2 = new String[] {"executionContextId"};
    private static final String[] PARAMS_QUERY_OBJECTS_1 = new String[] {"prototypeObjectId"};
    private static final String[] PARAMS_QUERY_OBJECTS_2 = new String[] {"prototypeObjectId", "objectGroup"};
    private static final String[] PARAMS_RELEASE_OBJECT_1 = new String[] {"objectId"};
    private static final String[] PARAMS_RELEASE_OBJECT_GROUP_1 = new String[] {"objectGroup"};
    private static final String[] PARAMS_REMOVE_BINDING_1 = new String[] {"name"};
    private static final String[] PARAMS_RUN_SCRIPT_1 = new String[] {"scriptId"};
    private static final String[] PARAMS_RUN_SCRIPT_2 = new String[] {
        "scriptId",
        "executionContextId",
        "objectGroup",
        "silent",
        "includeCommandLineAPI",
        "returnByValue",
        "generatePreview",
        "awaitPromise"
    };
    private static final String[] PARAMS_SET_ASYNC_CALL_STACK_DEPTH_1 = new String[] {"maxDepth"};
    private static final String[] PARAMS_SET_CUSTOM_OBJECT_FORMATTER_ENABLED_1 = new String[] {"enabled"};
    private static final String[] PARAMS_SET_MAX_CALL_STACK_SIZE_TO_CAPTURE_1 = new String[] {"size"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public RuntimeImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    public void addBinding(String name) {
        handler.invoke(
                this,
                DomainCommand.Runtime_addBinding,
                CRT_ADD_BINDING,
                PARAMS_ADD_BINDING_1,
                new Object[] {name},
                true);
    }

    @Override
    public void addBinding(String name, Integer executionContextId, String executionContextName) {
        handler.invoke(
                this,
                DomainCommand.Runtime_addBinding,
                CRT_ADD_BINDING,
                PARAMS_ADD_BINDING_2,
                new Object[] {name, executionContextId, executionContextName},
                true);
    }

    @Override
    public AwaitPromiseResult awaitPromise(String promiseObjectId) {
        return (AwaitPromiseResult) handler.invoke(
                this,
                DomainCommand.Runtime_awaitPromise,
                CRT_AWAIT_PROMISE,
                PARAMS_AWAIT_PROMISE_1,
                new Object[] {promiseObjectId},
                true);
    }

    @Override
    public AwaitPromiseResult awaitPromise(String promiseObjectId, Boolean returnByValue, Boolean generatePreview) {
        return (AwaitPromiseResult) handler.invoke(
                this,
                DomainCommand.Runtime_awaitPromise,
                CRT_AWAIT_PROMISE,
                PARAMS_AWAIT_PROMISE_2,
                new Object[] {promiseObjectId, returnByValue, generatePreview},
                true);
    }

    @Override
    public CallFunctionOnResult callFunctionOn(String functionDeclaration) {
        return (CallFunctionOnResult) handler.invoke(
                this,
                DomainCommand.Runtime_callFunctionOn,
                CRT_CALL_FUNCTION_ON,
                PARAMS_CALL_FUNCTION_ON_1,
                new Object[] {functionDeclaration},
                true);
    }

    @Override
    public CallFunctionOnResult callFunctionOn(
            String functionDeclaration,
            String objectId,
            List<CallArgument> arguments,
            Boolean silent,
            Boolean returnByValue,
            Boolean generatePreview,
            Boolean userGesture,
            Boolean awaitPromise,
            Integer executionContextId,
            String objectGroup,
            Boolean throwOnSideEffect,
            String uniqueContextId,
            SerializationOptions serializationOptions) {
        return (CallFunctionOnResult) handler.invoke(
                this,
                DomainCommand.Runtime_callFunctionOn,
                CRT_CALL_FUNCTION_ON,
                PARAMS_CALL_FUNCTION_ON_2,
                new Object[] {
                    functionDeclaration,
                    objectId,
                    arguments,
                    silent,
                    returnByValue,
                    generatePreview,
                    userGesture,
                    awaitPromise,
                    executionContextId,
                    objectGroup,
                    throwOnSideEffect,
                    uniqueContextId,
                    serializationOptions
                },
                true);
    }

    @Override
    public CompileScriptResult compileScript(String expression, String sourceURL, Boolean persistScript) {
        return (CompileScriptResult) handler.invoke(
                this,
                DomainCommand.Runtime_compileScript,
                CRT_COMPILE_SCRIPT,
                PARAMS_COMPILE_SCRIPT_1,
                new Object[] {expression, sourceURL, persistScript},
                true);
    }

    @Override
    public CompileScriptResult compileScript(
            String expression, String sourceURL, Boolean persistScript, Integer executionContextId) {
        return (CompileScriptResult) handler.invoke(
                this,
                DomainCommand.Runtime_compileScript,
                CRT_COMPILE_SCRIPT,
                PARAMS_COMPILE_SCRIPT_2,
                new Object[] {expression, sourceURL, persistScript, executionContextId},
                true);
    }

    @Override
    public void disable() {
        handler.invoke(this, DomainCommand.Runtime_disable, CRT_DISABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public void discardConsoleEntries() {
        handler.invoke(
                this,
                DomainCommand.Runtime_discardConsoleEntries,
                CRT_DISCARD_CONSOLE_ENTRIES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public void enable() {
        handler.invoke(this, DomainCommand.Runtime_enable, CRT_ENABLE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public EvaluateResult evaluate(String expression) {
        return (EvaluateResult) handler.invoke(
                this, DomainCommand.Runtime_evaluate, CRT_EVALUATE, PARAMS_EVALUATE_1, new Object[] {expression}, true);
    }

    @Override
    public EvaluateResult evaluate(
            String expression,
            String objectGroup,
            Boolean includeCommandLineAPI,
            Boolean silent,
            Integer contextId,
            Boolean returnByValue,
            Boolean generatePreview,
            Boolean userGesture,
            Boolean awaitPromise,
            Boolean throwOnSideEffect,
            Double timeout,
            Boolean disableBreaks,
            Boolean replMode,
            Boolean allowUnsafeEvalBlockedByCSP,
            String uniqueContextId,
            SerializationOptions serializationOptions) {
        return (EvaluateResult) handler.invoke(
                this,
                DomainCommand.Runtime_evaluate,
                CRT_EVALUATE,
                PARAMS_EVALUATE_2,
                new Object[] {
                    expression,
                    objectGroup,
                    includeCommandLineAPI,
                    silent,
                    contextId,
                    returnByValue,
                    generatePreview,
                    userGesture,
                    awaitPromise,
                    throwOnSideEffect,
                    timeout,
                    disableBreaks,
                    replMode,
                    allowUnsafeEvalBlockedByCSP,
                    uniqueContextId,
                    serializationOptions
                },
                true);
    }

    @Override
    public ExceptionDetails getExceptionDetails(String errorObjectId) {
        return (ExceptionDetails) handler.invoke(
                this,
                DomainCommand.Runtime_getExceptionDetails,
                CRT_GET_EXCEPTION_DETAILS,
                PARAMS_GET_EXCEPTION_DETAILS_1,
                new Object[] {errorObjectId},
                true);
    }

    @Override
    public GetHeapUsageResult getHeapUsage() {
        return (GetHeapUsageResult) handler.invoke(
                this, DomainCommand.Runtime_getHeapUsage, CRT_GET_HEAP_USAGE, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public String getIsolateId() {
        return (String) handler.invoke(
                this, DomainCommand.Runtime_getIsolateId, CRT_GET_ISOLATE_ID, EMPTY_ARGS, EMPTY_VALUES, true);
    }

    @Override
    public GetPropertiesResult getProperties(String objectId) {
        return (GetPropertiesResult) handler.invoke(
                this,
                DomainCommand.Runtime_getProperties,
                CRT_GET_PROPERTIES,
                PARAMS_GET_PROPERTIES_1,
                new Object[] {objectId},
                true);
    }

    @Override
    public GetPropertiesResult getProperties(
            String objectId,
            Boolean ownProperties,
            Boolean accessorPropertiesOnly,
            Boolean generatePreview,
            Boolean nonIndexedPropertiesOnly) {
        return (GetPropertiesResult) handler.invoke(
                this,
                DomainCommand.Runtime_getProperties,
                CRT_GET_PROPERTIES,
                PARAMS_GET_PROPERTIES_2,
                new Object[] {objectId, ownProperties, accessorPropertiesOnly, generatePreview, nonIndexedPropertiesOnly
                },
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> globalLexicalScopeNames() {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.Runtime_globalLexicalScopeNames,
                CRT_GLOBAL_LEXICAL_SCOPE_NAMES,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public List<String> globalLexicalScopeNames(Integer executionContextId) {
        return (List<String>) handler.invoke(
                this,
                DomainCommand.Runtime_globalLexicalScopeNames,
                CRT_GLOBAL_LEXICAL_SCOPE_NAMES,
                PARAMS_GLOBAL_LEXICAL_SCOPE_NAMES_2,
                new Object[] {executionContextId},
                true);
    }

    @Override
    public RemoteObject queryObjects(String prototypeObjectId) {
        return (RemoteObject) handler.invoke(
                this,
                DomainCommand.Runtime_queryObjects,
                CRT_QUERY_OBJECTS,
                PARAMS_QUERY_OBJECTS_1,
                new Object[] {prototypeObjectId},
                true);
    }

    @Override
    public RemoteObject queryObjects(String prototypeObjectId, String objectGroup) {
        return (RemoteObject) handler.invoke(
                this,
                DomainCommand.Runtime_queryObjects,
                CRT_QUERY_OBJECTS,
                PARAMS_QUERY_OBJECTS_2,
                new Object[] {prototypeObjectId, objectGroup},
                true);
    }

    @Override
    public void releaseObject(String objectId) {
        handler.invoke(
                this,
                DomainCommand.Runtime_releaseObject,
                CRT_RELEASE_OBJECT,
                PARAMS_RELEASE_OBJECT_1,
                new Object[] {objectId},
                true);
    }

    @Override
    public void releaseObjectGroup(String objectGroup) {
        handler.invoke(
                this,
                DomainCommand.Runtime_releaseObjectGroup,
                CRT_RELEASE_OBJECT_GROUP,
                PARAMS_RELEASE_OBJECT_GROUP_1,
                new Object[] {objectGroup},
                true);
    }

    @Override
    public void removeBinding(String name) {
        handler.invoke(
                this,
                DomainCommand.Runtime_removeBinding,
                CRT_REMOVE_BINDING,
                PARAMS_REMOVE_BINDING_1,
                new Object[] {name},
                true);
    }

    @Override
    public void runIfWaitingForDebugger() {
        handler.invoke(
                this,
                DomainCommand.Runtime_runIfWaitingForDebugger,
                CRT_RUN_IF_WAITING_FOR_DEBUGGER,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }

    @Override
    public RunScriptResult runScript(String scriptId) {
        return (RunScriptResult) handler.invoke(
                this,
                DomainCommand.Runtime_runScript,
                CRT_RUN_SCRIPT,
                PARAMS_RUN_SCRIPT_1,
                new Object[] {scriptId},
                true);
    }

    @Override
    public RunScriptResult runScript(
            String scriptId,
            Integer executionContextId,
            String objectGroup,
            Boolean silent,
            Boolean includeCommandLineAPI,
            Boolean returnByValue,
            Boolean generatePreview,
            Boolean awaitPromise) {
        return (RunScriptResult) handler.invoke(
                this,
                DomainCommand.Runtime_runScript,
                CRT_RUN_SCRIPT,
                PARAMS_RUN_SCRIPT_2,
                new Object[] {
                    scriptId,
                    executionContextId,
                    objectGroup,
                    silent,
                    includeCommandLineAPI,
                    returnByValue,
                    generatePreview,
                    awaitPromise
                },
                true);
    }

    @Override
    public void setAsyncCallStackDepth(Integer maxDepth) {
        handler.invoke(
                this,
                DomainCommand.Runtime_setAsyncCallStackDepth,
                CRT_SET_ASYNC_CALL_STACK_DEPTH,
                PARAMS_SET_ASYNC_CALL_STACK_DEPTH_1,
                new Object[] {maxDepth},
                true);
    }

    @Override
    public void setCustomObjectFormatterEnabled(Boolean enabled) {
        handler.invoke(
                this,
                DomainCommand.Runtime_setCustomObjectFormatterEnabled,
                CRT_SET_CUSTOM_OBJECT_FORMATTER_ENABLED,
                PARAMS_SET_CUSTOM_OBJECT_FORMATTER_ENABLED_1,
                new Object[] {enabled},
                true);
    }

    @Override
    public void setMaxCallStackSizeToCapture(Integer size) {
        handler.invoke(
                this,
                DomainCommand.Runtime_setMaxCallStackSizeToCapture,
                CRT_SET_MAX_CALL_STACK_SIZE_TO_CAPTURE,
                PARAMS_SET_MAX_CALL_STACK_SIZE_TO_CAPTURE_1,
                new Object[] {size},
                true);
    }

    @Override
    public void terminateExecution() {
        handler.invoke(
                this,
                DomainCommand.Runtime_terminateExecution,
                CRT_TERMINATE_EXECUTION,
                EMPTY_ARGS,
                EMPTY_VALUES,
                true);
    }
}
