// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.session.ParameterizedCommand;

@Experimental
public interface Preload extends ParameterizedCommand<Preload> {
    void disable();

    void enable();
}
