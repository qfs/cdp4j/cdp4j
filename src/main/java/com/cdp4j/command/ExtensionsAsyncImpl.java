// SPDX-License-Identifier: MIT
package com.cdp4j.command;

import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.DomainCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.cdp4j.type.extensions.StorageArea;
import java.util.List;
import java.util.concurrent.CompletableFuture;

class ExtensionsAsyncImpl extends ParameterizedCommandImpl<ExtensionsAsync> implements ExtensionsAsync {

    private static final CommandReturnType CRT_CLEAR_STORAGE_ITEMS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_LOAD_UNPACKED = new CommandReturnType("id", String.class, null);
    private static final CommandReturnType CRT_REMOVE_STORAGE_ITEMS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_SET_STORAGE_ITEMS = new CommandReturnType(null, void.class, null);
    private static final CommandReturnType CRT_UNINSTALL = new CommandReturnType(null, void.class, null);
    private static final String[] EMPTY_ARGS = new String[] {};
    private static final Object[] EMPTY_VALUES = new Object[] {};
    private static final String[] PARAMS_CLEAR_STORAGE_ITEMS_1 = new String[] {"id", "storageArea"};
    private static final String[] PARAMS_LOAD_UNPACKED_1 = new String[] {"path"};
    private static final String[] PARAMS_REMOVE_STORAGE_ITEMS_1 = new String[] {"id", "storageArea", "keys"};
    private static final String[] PARAMS_SET_STORAGE_ITEMS_1 = new String[] {"id", "storageArea"};
    private static final String[] PARAMS_UNINSTALL_1 = new String[] {"id"};
    /**
     * instance fields
     */
    private final SessionInvocationHandler handler;

    public ExtensionsAsyncImpl(SessionInvocationHandler handler) {
        this.handler = handler;
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> clearStorageItems(String id, StorageArea storageArea) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Extensions_clearStorageItems,
                CRT_CLEAR_STORAGE_ITEMS,
                PARAMS_CLEAR_STORAGE_ITEMS_1,
                new Object[] {id, storageArea},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<String> loadUnpacked(String path) {
        return (CompletableFuture<String>) handler.invoke(
                this,
                DomainCommand.Extensions_loadUnpacked,
                CRT_LOAD_UNPACKED,
                PARAMS_LOAD_UNPACKED_1,
                new Object[] {path},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> removeStorageItems(String id, StorageArea storageArea, List<String> keys) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Extensions_removeStorageItems,
                CRT_REMOVE_STORAGE_ITEMS,
                PARAMS_REMOVE_STORAGE_ITEMS_1,
                new Object[] {id, storageArea, keys},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> setStorageItems(String id, StorageArea storageArea) {
        return (CompletableFuture<Void>) handler.invoke(
                this,
                DomainCommand.Extensions_setStorageItems,
                CRT_SET_STORAGE_ITEMS,
                PARAMS_SET_STORAGE_ITEMS_1,
                new Object[] {id, storageArea},
                false);
    }

    @Override
    @java.lang.SuppressWarnings("unchecked")
    public CompletableFuture<Void> uninstall(String id) {
        return (CompletableFuture<Void>) handler.invoke(
                this, DomainCommand.Extensions_uninstall, CRT_UNINSTALL, PARAMS_UNINSTALL_1, new Object[] {id}, false);
    }
}
