// playwright license:
/**
 * Copyright (c) Microsoft Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// cssTokenizer.js license:
/*
 * Original at https://github.com/tabatkins/parse-css
 * licensed under http://creativecommons.org/publicdomain/zero/1.0/
 *
 * Modifications copyright (c) Microsoft Corporation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Changes from https://github.com/tabatkins/parse-css
//   - Tabs are replaced with two spaces.
//   - Everything not related to tokenizing - below the first exports block - is removed.
const cdp4j = (function(exports) {
    'use strict';

    const XPathEngine = {
        queryAll(root, selector) {
            if (selector.startsWith('/'))
                selector = '.' + selector;
            const result = [];
            const document = root instanceof Document ? root : root.ownerDocument;
            if (!document)
                return result;
            const it = document.evaluate(selector, root, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE);
            for (let node = it.iterateNext(); node; node = it.iterateNext()) {
                if (node.nodeType === Node.ELEMENT_NODE)
                    result.push(node);
            }
            return result;
        }
    };

    class InvalidSelectorError extends Error {}

    var between = function(num, first, last) {
        return num >= first && num <= last;
    };

    function digit(code) {
        return between(code, 0x30, 0x39);
    }

    function hexdigit(code) {
        return digit(code) || between(code, 0x41, 0x46) || between(code, 0x61, 0x66);
    }

    function uppercaseletter(code) {
        return between(code, 0x41, 0x5a);
    }

    function lowercaseletter(code) {
        return between(code, 0x61, 0x7a);
    }

    function letter(code) {
        return uppercaseletter(code) || lowercaseletter(code);
    }

    function nonascii(code) {
        return code >= 0x80;
    }

    function namestartchar(code) {
        return letter(code) || nonascii(code) || code == 0x5f;
    }

    function namechar(code) {
        return namestartchar(code) || digit(code) || code == 0x2d;
    }

    function nonprintable(code) {
        return between(code, 0, 8) || code == 0xb || between(code, 0xe, 0x1f) || code == 0x7f;
    }

    function newline(code) {
        return code == 0xa;
    }

    function whitespace(code) {
        return newline(code) || code == 9 || code == 0x20;
    }
    var maximumallowedcodepoint = 0x10ffff;
    var InvalidCharacterError = function(message) {
        this.message = message;
    };
    InvalidCharacterError.prototype = new Error;
    InvalidCharacterError.prototype.name = 'InvalidCharacterError';

    function preprocess(str) {
        var codepoints = [];
        for (var i = 0; i < str.length; i++) {
            var code = str.charCodeAt(i);
            if (code == 0xd && str.charCodeAt(i + 1) == 0xa) {
                code = 0xa;
                i++;
            }
            if (code == 0xd || code == 0xc)
                code = 0xa;
            if (code == 0x0)
                code = 0xfffd;
            if (between(code, 0xd800, 0xdbff) && between(str.charCodeAt(i + 1), 0xdc00, 0xdfff)) {
                var lead = code - 0xd800;
                var trail = str.charCodeAt(i + 1) - 0xdc00;
                code = Math.pow(2, 16) + lead * Math.pow(2, 10) + trail;
                i++;
            }
            codepoints.push(code);
        }
        return codepoints;
    }

    function stringFromCode(code) {
        if (code <= 0xffff)
            return String.fromCharCode(code);
        code -= Math.pow(2, 16);
        var lead = Math.floor(code / Math.pow(2, 10)) + 0xd800;
        var trail = code % Math.pow(2, 10) + 0xdc00;
        return String.fromCharCode(lead) + String.fromCharCode(trail);
    }

    function tokenize(str) {
        str = preprocess(str);
        var i = -1;
        var tokens = [];
        var code;
        var codepoint = function(i) {
            if (i >= str.length) {
                return -1;
            }
            return str[i];
        };
        var next = function(num) {
            if (num === undefined)
                num = 1;
            if (num > 3)
                throw "Spec Error: no more than three codepoints of lookahead.";
            return codepoint(i + num);
        };
        var consume = function(num) {
            if (num === undefined)
                num = 1;
            i += num;
            code = codepoint(i);
            return true;
        };
        var reconsume = function() {
            i -= 1;
            return true;
        };
        var eof = function(codepoint) {
            if (codepoint === undefined)
                codepoint = code;
            return codepoint == -1;
        };
        var parseerror = function() {
            console.log("Parse error at index " + i + ", processing codepoint 0x" + code.toString(16) + ".");
            return true;
        };
        var consumeAToken = function() {
            consumeComments();
            consume();
            if (whitespace(code)) {
                while (whitespace(next()))
                    consume();
                return new WhitespaceToken;
            } else if (code == 0x22)
                return consumeAStringToken();
            else if (code == 0x23) {
                if (namechar(next()) || areAValidEscape(next(1), next(2))) {
                    var token = new HashToken();
                    if (wouldStartAnIdentifier(next(1), next(2), next(3)))
                        token.type = "id";
                    token.value = consumeAName();
                    return token;
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x24) {
                if (next() == 0x3d) {
                    consume();
                    return new SuffixMatchToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x27)
                return consumeAStringToken();
            else if (code == 0x28)
                return new OpenParenToken();
            else if (code == 0x29)
                return new CloseParenToken();
            else if (code == 0x2a) {
                if (next() == 0x3d) {
                    consume();
                    return new SubstringMatchToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x2b) {
                if (startsWithANumber()) {
                    reconsume();
                    return consumeANumericToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x2c)
                return new CommaToken();
            else if (code == 0x2d) {
                if (startsWithANumber()) {
                    reconsume();
                    return consumeANumericToken();
                } else if (next(1) == 0x2d && next(2) == 0x3e) {
                    consume(2);
                    return new CDCToken();
                } else if (startsWithAnIdentifier()) {
                    reconsume();
                    return consumeAnIdentlikeToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x2e) {
                if (startsWithANumber()) {
                    reconsume();
                    return consumeANumericToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x3a)
                return new ColonToken;
            else if (code == 0x3b)
                return new SemicolonToken;
            else if (code == 0x3c) {
                if (next(1) == 0x21 && next(2) == 0x2d && next(3) == 0x2d) {
                    consume(3);
                    return new CDOToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x40) {
                if (wouldStartAnIdentifier(next(1), next(2), next(3))) {
                    return new AtKeywordToken(consumeAName());
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x5b)
                return new OpenSquareToken();
            else if (code == 0x5c) {
                if (startsWithAValidEscape()) {
                    reconsume();
                    return consumeAnIdentlikeToken();
                } else {
                    parseerror();
                    return new DelimToken(code);
                }
            } else if (code == 0x5d)
                return new CloseSquareToken();
            else if (code == 0x5e) {
                if (next() == 0x3d) {
                    consume();
                    return new PrefixMatchToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x7b)
                return new OpenCurlyToken();
            else if (code == 0x7c) {
                if (next() == 0x3d) {
                    consume();
                    return new DashMatchToken();
                } else if (next() == 0x7c) {
                    consume();
                    return new ColumnToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (code == 0x7d)
                return new CloseCurlyToken();
            else if (code == 0x7e) {
                if (next() == 0x3d) {
                    consume();
                    return new IncludeMatchToken();
                } else {
                    return new DelimToken(code);
                }
            } else if (digit(code)) {
                reconsume();
                return consumeANumericToken();
            } else if (namestartchar(code)) {
                reconsume();
                return consumeAnIdentlikeToken();
            } else if (eof())
                return new EOFToken();
            else
                return new DelimToken(code);
        };
        var consumeComments = function() {
            while (next(1) == 0x2f && next(2) == 0x2a) {
                consume(2);
                while (true) {
                    consume();
                    if (code == 0x2a && next() == 0x2f) {
                        consume();
                        break;
                    } else if (eof()) {
                        parseerror();
                        return;
                    }
                }
            }
        };
        var consumeANumericToken = function() {
            var num = consumeANumber();
            if (wouldStartAnIdentifier(next(1), next(2), next(3))) {
                var token = new DimensionToken();
                token.value = num.value;
                token.repr = num.repr;
                token.type = num.type;
                token.unit = consumeAName();
                return token;
            } else if (next() == 0x25) {
                consume();
                var token = new PercentageToken();
                token.value = num.value;
                token.repr = num.repr;
                return token;
            } else {
                var token = new NumberToken();
                token.value = num.value;
                token.repr = num.repr;
                token.type = num.type;
                return token;
            }
        };
        var consumeAnIdentlikeToken = function() {
            var str = consumeAName();
            if (str.toLowerCase() == "url" && next() == 0x28) {
                consume();
                while (whitespace(next(1)) && whitespace(next(2)))
                    consume();
                if (next() == 0x22 || next() == 0x27) {
                    return new FunctionToken(str);
                } else if (whitespace(next()) && (next(2) == 0x22 || next(2) == 0x27)) {
                    return new FunctionToken(str);
                } else {
                    return consumeAURLToken();
                }
            } else if (next() == 0x28) {
                consume();
                return new FunctionToken(str);
            } else {
                return new IdentToken(str);
            }
        };
        var consumeAStringToken = function(endingCodePoint) {
            if (endingCodePoint === undefined)
                endingCodePoint = code;
            var string = "";
            while (consume()) {
                if (code == endingCodePoint || eof()) {
                    return new StringToken(string);
                } else if (newline(code)) {
                    parseerror();
                    reconsume();
                    return new BadStringToken();
                } else if (code == 0x5c) {
                    if (eof(next()));
                    else if (newline(next())) {
                        consume();
                    } else {
                        string += stringFromCode(consumeEscape());
                    }
                } else {
                    string += stringFromCode(code);
                }
            }
        };
        var consumeAURLToken = function() {
            var token = new URLToken("");
            while (whitespace(next()))
                consume();
            if (eof(next()))
                return token;
            while (consume()) {
                if (code == 0x29 || eof()) {
                    return token;
                } else if (whitespace(code)) {
                    while (whitespace(next()))
                        consume();
                    if (next() == 0x29 || eof(next())) {
                        consume();
                        return token;
                    } else {
                        consumeTheRemnantsOfABadURL();
                        return new BadURLToken();
                    }
                } else if (code == 0x22 || code == 0x27 || code == 0x28 || nonprintable(code)) {
                    parseerror();
                    consumeTheRemnantsOfABadURL();
                    return new BadURLToken();
                } else if (code == 0x5c) {
                    if (startsWithAValidEscape()) {
                        token.value += stringFromCode(consumeEscape());
                    } else {
                        parseerror();
                        consumeTheRemnantsOfABadURL();
                        return new BadURLToken();
                    }
                } else {
                    token.value += stringFromCode(code);
                }
            }
        };
        var consumeEscape = function() {
            consume();
            if (hexdigit(code)) {
                var digits = [code];
                for (var total = 0; total < 5; total++) {
                    if (hexdigit(next())) {
                        consume();
                        digits.push(code);
                    } else {
                        break;
                    }
                }
                if (whitespace(next()))
                    consume();
                var value = parseInt(digits.map(function(x) {
                    return String.fromCharCode(x);
                }).join(''), 16);
                if (value > maximumallowedcodepoint)
                    value = 0xfffd;
                return value;
            } else if (eof()) {
                return 0xfffd;
            } else {
                return code;
            }
        };
        var areAValidEscape = function(c1, c2) {
            if (c1 != 0x5c)
                return false;
            if (newline(c2))
                return false;
            return true;
        };
        var startsWithAValidEscape = function() {
            return areAValidEscape(code, next());
        };
        var wouldStartAnIdentifier = function(c1, c2, c3) {
            if (c1 == 0x2d) {
                return namestartchar(c2) || c2 == 0x2d || areAValidEscape(c2, c3);
            } else if (namestartchar(c1)) {
                return true;
            } else if (c1 == 0x5c) {
                return areAValidEscape(c1, c2);
            } else {
                return false;
            }
        };
        var startsWithAnIdentifier = function() {
            return wouldStartAnIdentifier(code, next(1), next(2));
        };
        var wouldStartANumber = function(c1, c2, c3) {
            if (c1 == 0x2b || c1 == 0x2d) {
                if (digit(c2))
                    return true;
                if (c2 == 0x2e && digit(c3))
                    return true;
                return false;
            } else if (c1 == 0x2e) {
                if (digit(c2))
                    return true;
                return false;
            } else if (digit(c1)) {
                return true;
            } else {
                return false;
            }
        };
        var startsWithANumber = function() {
            return wouldStartANumber(code, next(1), next(2));
        };
        var consumeAName = function() {
            var result = "";
            while (consume()) {
                if (namechar(code)) {
                    result += stringFromCode(code);
                } else if (startsWithAValidEscape()) {
                    result += stringFromCode(consumeEscape());
                } else {
                    reconsume();
                    return result;
                }
            }
        };
        var consumeANumber = function() {
            var repr = [];
            var type = "integer";
            if (next() == 0x2b || next() == 0x2d) {
                consume();
                repr += stringFromCode(code);
            }
            while (digit(next())) {
                consume();
                repr += stringFromCode(code);
            }
            if (next(1) == 0x2e && digit(next(2))) {
                consume();
                repr += stringFromCode(code);
                consume();
                repr += stringFromCode(code);
                type = "number";
                while (digit(next())) {
                    consume();
                    repr += stringFromCode(code);
                }
            }
            var c1 = next(1),
                c2 = next(2),
                c3 = next(3);
            if ((c1 == 0x45 || c1 == 0x65) && digit(c2)) {
                consume();
                repr += stringFromCode(code);
                consume();
                repr += stringFromCode(code);
                type = "number";
                while (digit(next())) {
                    consume();
                    repr += stringFromCode(code);
                }
            } else if ((c1 == 0x45 || c1 == 0x65) && (c2 == 0x2b || c2 == 0x2d) && digit(c3)) {
                consume();
                repr += stringFromCode(code);
                consume();
                repr += stringFromCode(code);
                consume();
                repr += stringFromCode(code);
                type = "number";
                while (digit(next())) {
                    consume();
                    repr += stringFromCode(code);
                }
            }
            var value = convertAStringToANumber(repr);
            return {
                type: type,
                value: value,
                repr: repr
            };
        };
        var convertAStringToANumber = function(string) {
            return +string;
        };
        var consumeTheRemnantsOfABadURL = function() {
            while (consume()) {
                if (code == 0x29 || eof()) {
                    return;
                } else if (startsWithAValidEscape()) {
                    consumeEscape();
                } else;
            }
        };
        var iterationCount = 0;
        while (!eof(next())) {
            tokens.push(consumeAToken());
            iterationCount++;
            if (iterationCount > str.length * 2)
                return "I'm infinite-looping!";
        }
        return tokens;
    }

    function CSSParserToken() {
        throw "Abstract Base Class";
    }
    CSSParserToken.prototype.toJSON = function() {
        return {
            token: this.tokenType
        };
    };
    CSSParserToken.prototype.toString = function() {
        return this.tokenType;
    };
    CSSParserToken.prototype.toSource = function() {
        return '' + this;
    };

    function BadStringToken() {
        return this;
    }
    BadStringToken.prototype = Object.create(CSSParserToken.prototype);
    BadStringToken.prototype.tokenType = "BADSTRING";

    function BadURLToken() {
        return this;
    }
    BadURLToken.prototype = Object.create(CSSParserToken.prototype);
    BadURLToken.prototype.tokenType = "BADURL";

    function WhitespaceToken() {
        return this;
    }
    WhitespaceToken.prototype = Object.create(CSSParserToken.prototype);
    WhitespaceToken.prototype.tokenType = "WHITESPACE";
    WhitespaceToken.prototype.toString = function() {
        return "WS";
    };
    WhitespaceToken.prototype.toSource = function() {
        return " ";
    };

    function CDOToken() {
        return this;
    }
    CDOToken.prototype = Object.create(CSSParserToken.prototype);
    CDOToken.prototype.tokenType = "CDO";
    CDOToken.prototype.toSource = function() {
        return "<!--";
    };

    function CDCToken() {
        return this;
    }
    CDCToken.prototype = Object.create(CSSParserToken.prototype);
    CDCToken.prototype.tokenType = "CDC";
    CDCToken.prototype.toSource = function() {
        return "-->";
    };

    function ColonToken() {
        return this;
    }
    ColonToken.prototype = Object.create(CSSParserToken.prototype);
    ColonToken.prototype.tokenType = ":";

    function SemicolonToken() {
        return this;
    }
    SemicolonToken.prototype = Object.create(CSSParserToken.prototype);
    SemicolonToken.prototype.tokenType = ";";

    function CommaToken() {
        return this;
    }
    CommaToken.prototype = Object.create(CSSParserToken.prototype);
    CommaToken.prototype.tokenType = ",";

    function GroupingToken() {
        throw "Abstract Base Class";
    }
    GroupingToken.prototype = Object.create(CSSParserToken.prototype);

    function OpenCurlyToken() {
        this.value = "{";
        this.mirror = "}";
        return this;
    }
    OpenCurlyToken.prototype = Object.create(GroupingToken.prototype);
    OpenCurlyToken.prototype.tokenType = "{";

    function CloseCurlyToken() {
        this.value = "}";
        this.mirror = "{";
        return this;
    }
    CloseCurlyToken.prototype = Object.create(GroupingToken.prototype);
    CloseCurlyToken.prototype.tokenType = "}";

    function OpenSquareToken() {
        this.value = "[";
        this.mirror = "]";
        return this;
    }
    OpenSquareToken.prototype = Object.create(GroupingToken.prototype);
    OpenSquareToken.prototype.tokenType = "[";

    function CloseSquareToken() {
        this.value = "]";
        this.mirror = "[";
        return this;
    }
    CloseSquareToken.prototype = Object.create(GroupingToken.prototype);
    CloseSquareToken.prototype.tokenType = "]";

    function OpenParenToken() {
        this.value = "(";
        this.mirror = ")";
        return this;
    }
    OpenParenToken.prototype = Object.create(GroupingToken.prototype);
    OpenParenToken.prototype.tokenType = "(";

    function CloseParenToken() {
        this.value = ")";
        this.mirror = "(";
        return this;
    }
    CloseParenToken.prototype = Object.create(GroupingToken.prototype);
    CloseParenToken.prototype.tokenType = ")";

    function IncludeMatchToken() {
        return this;
    }
    IncludeMatchToken.prototype = Object.create(CSSParserToken.prototype);
    IncludeMatchToken.prototype.tokenType = "~=";

    function DashMatchToken() {
        return this;
    }
    DashMatchToken.prototype = Object.create(CSSParserToken.prototype);
    DashMatchToken.prototype.tokenType = "|=";

    function PrefixMatchToken() {
        return this;
    }
    PrefixMatchToken.prototype = Object.create(CSSParserToken.prototype);
    PrefixMatchToken.prototype.tokenType = "^=";

    function SuffixMatchToken() {
        return this;
    }
    SuffixMatchToken.prototype = Object.create(CSSParserToken.prototype);
    SuffixMatchToken.prototype.tokenType = "$=";

    function SubstringMatchToken() {
        return this;
    }
    SubstringMatchToken.prototype = Object.create(CSSParserToken.prototype);
    SubstringMatchToken.prototype.tokenType = "*=";

    function ColumnToken() {
        return this;
    }
    ColumnToken.prototype = Object.create(CSSParserToken.prototype);
    ColumnToken.prototype.tokenType = "||";

    function EOFToken() {
        return this;
    }
    EOFToken.prototype = Object.create(CSSParserToken.prototype);
    EOFToken.prototype.tokenType = "EOF";
    EOFToken.prototype.toSource = function() {
        return "";
    };

    function DelimToken(code) {
        this.value = stringFromCode(code);
        return this;
    }
    DelimToken.prototype = Object.create(CSSParserToken.prototype);
    DelimToken.prototype.tokenType = "DELIM";
    DelimToken.prototype.toString = function() {
        return "DELIM(" + this.value + ")";
    };
    DelimToken.prototype.toJSON = function() {
        var json = this.constructor.prototype.constructor.prototype.toJSON.call(this);
        json.value = this.value;
        return json;
    };
    DelimToken.prototype.toSource = function() {
        if (this.value == "\\")
            return "\\\n";
        else
            return this.value;
    };

    function StringValuedToken() {
        throw "Abstract Base Class";
    }
    StringValuedToken.prototype = Object.create(CSSParserToken.prototype);
    StringValuedToken.prototype.ASCIIMatch = function(str) {
        return this.value.toLowerCase() == str.toLowerCase();
    };
    StringValuedToken.prototype.toJSON = function() {
        var json = this.constructor.prototype.constructor.prototype.toJSON.call(this);
        json.value = this.value;
        return json;
    };

    function IdentToken(val) {
        this.value = val;
    }
    IdentToken.prototype = Object.create(StringValuedToken.prototype);
    IdentToken.prototype.tokenType = "IDENT";
    IdentToken.prototype.toString = function() {
        return "IDENT(" + this.value + ")";
    };
    IdentToken.prototype.toSource = function() {
        return escapeIdent(this.value);
    };

    function FunctionToken(val) {
        this.value = val;
        this.mirror = ")";
    }
    FunctionToken.prototype = Object.create(StringValuedToken.prototype);
    FunctionToken.prototype.tokenType = "FUNCTION";
    FunctionToken.prototype.toString = function() {
        return "FUNCTION(" + this.value + ")";
    };
    FunctionToken.prototype.toSource = function() {
        return escapeIdent(this.value) + "(";
    };

    function AtKeywordToken(val) {
        this.value = val;
    }
    AtKeywordToken.prototype = Object.create(StringValuedToken.prototype);
    AtKeywordToken.prototype.tokenType = "AT-KEYWORD";
    AtKeywordToken.prototype.toString = function() {
        return "AT(" + this.value + ")";
    };
    AtKeywordToken.prototype.toSource = function() {
        return "@" + escapeIdent(this.value);
    };

    function HashToken(val) {
        this.value = val;
        this.type = "unrestricted";
    }
    HashToken.prototype = Object.create(StringValuedToken.prototype);
    HashToken.prototype.tokenType = "HASH";
    HashToken.prototype.toString = function() {
        return "HASH(" + this.value + ")";
    };
    HashToken.prototype.toJSON = function() {
        var json = this.constructor.prototype.constructor.prototype.toJSON.call(this);
        json.value = this.value;
        json.type = this.type;
        return json;
    };
    HashToken.prototype.toSource = function() {
        if (this.type == "id") {
            return "#" + escapeIdent(this.value);
        } else {
            return "#" + escapeHash(this.value);
        }
    };

    function StringToken(val) {
        this.value = val;
    }
    StringToken.prototype = Object.create(StringValuedToken.prototype);
    StringToken.prototype.tokenType = "STRING";
    StringToken.prototype.toString = function() {
        return '"' + escapeString(this.value) + '"';
    };

    function URLToken(val) {
        this.value = val;
    }
    URLToken.prototype = Object.create(StringValuedToken.prototype);
    URLToken.prototype.tokenType = "URL";
    URLToken.prototype.toString = function() {
        return "URL(" + this.value + ")";
    };
    URLToken.prototype.toSource = function() {
        return 'url("' + escapeString(this.value) + '")';
    };

    function NumberToken() {
        this.value = null;
        this.type = "integer";
        this.repr = "";
    }
    NumberToken.prototype = Object.create(CSSParserToken.prototype);
    NumberToken.prototype.tokenType = "NUMBER";
    NumberToken.prototype.toString = function() {
        if (this.type == "integer")
            return "INT(" + this.value + ")";
        return "NUMBER(" + this.value + ")";
    };
    NumberToken.prototype.toJSON = function() {
        var json = this.constructor.prototype.constructor.prototype.toJSON.call(this);
        json.value = this.value;
        json.type = this.type;
        json.repr = this.repr;
        return json;
    };
    NumberToken.prototype.toSource = function() {
        return this.repr;
    };

    function PercentageToken() {
        this.value = null;
        this.repr = "";
    }
    PercentageToken.prototype = Object.create(CSSParserToken.prototype);
    PercentageToken.prototype.tokenType = "PERCENTAGE";
    PercentageToken.prototype.toString = function() {
        return "PERCENTAGE(" + this.value + ")";
    };
    PercentageToken.prototype.toJSON = function() {
        var json = this.constructor.prototype.constructor.prototype.toJSON.call(this);
        json.value = this.value;
        json.repr = this.repr;
        return json;
    };
    PercentageToken.prototype.toSource = function() {
        return this.repr + "%";
    };

    function DimensionToken() {
        this.value = null;
        this.type = "integer";
        this.repr = "";
        this.unit = "";
    }
    DimensionToken.prototype = Object.create(CSSParserToken.prototype);
    DimensionToken.prototype.tokenType = "DIMENSION";
    DimensionToken.prototype.toString = function() {
        return "DIM(" + this.value + "," + this.unit + ")";
    };
    DimensionToken.prototype.toJSON = function() {
        var json = this.constructor.prototype.constructor.prototype.toJSON.call(this);
        json.value = this.value;
        json.type = this.type;
        json.repr = this.repr;
        json.unit = this.unit;
        return json;
    };
    DimensionToken.prototype.toSource = function() {
        var source = this.repr;
        var unit = escapeIdent(this.unit);
        if (unit[0].toLowerCase() == "e" && (unit[1] == "-" || between(unit.charCodeAt(1), 0x30, 0x39))) {
            unit = "\\65 " + unit.slice(1, unit.length);
        }
        return source + unit;
    };

    function escapeIdent(string) {
        string = '' + string;
        var result = '';
        var firstcode = string.charCodeAt(0);
        for (var i = 0; i < string.length; i++) {
            var code = string.charCodeAt(i);
            if (code == 0x0) {
                throw new InvalidCharacterError('Invalid character: the input contains U+0000.');
            }
            if (between(code, 0x1, 0x1f) || code == 0x7f ||
                (i == 0 && between(code, 0x30, 0x39)) ||
                (i == 1 && between(code, 0x30, 0x39) && firstcode == 0x2d)) {
                result += '\\' + code.toString(16) + ' ';
            } else if (code >= 0x80 ||
                code == 0x2d ||
                code == 0x5f ||
                between(code, 0x30, 0x39) ||
                between(code, 0x41, 0x5a) ||
                between(code, 0x61, 0x7a)) {
                result += string[i];
            } else {
                result += '\\' + string[i];
            }
        }
        return result;
    }

    function escapeHash(string) {
        string = '' + string;
        var result = '';
        string.charCodeAt(0);
        for (var i = 0; i < string.length; i++) {
            var code = string.charCodeAt(i);
            if (code == 0x0) {
                throw new InvalidCharacterError('Invalid character: the input contains U+0000.');
            }
            if (code >= 0x80 ||
                code == 0x2d ||
                code == 0x5f ||
                between(code, 0x30, 0x39) ||
                between(code, 0x41, 0x5a) ||
                between(code, 0x61, 0x7a)) {
                result += string[i];
            } else {
                result += '\\' + code.toString(16) + ' ';
            }
        }
        return result;
    }

    function escapeString(string) {
        string = '' + string;
        var result = '';
        for (var i = 0; i < string.length; i++) {
            var code = string.charCodeAt(i);
            if (code == 0x0) {
                throw new InvalidCharacterError('Invalid character: the input contains U+0000.');
            }
            if (between(code, 0x1, 0x1f) || code == 0x7f) {
                result += '\\' + code.toString(16) + ' ';
            } else if (code == 0x22 || code == 0x5c) {
                result += '\\' + string[i];
            } else {
                result += string[i];
            }
        }
        return result;
    }

    function parseCSS(selector, customNames) {
        let tokens;
        try {
            tokens = tokenize(selector);
            if (!(tokens[tokens.length - 1] instanceof EOFToken))
                tokens.push(new EOFToken());
        } catch (e) {
            const newMessage = e.message + ` while parsing selector "${selector}"`;
            const index = (e.stack || '').indexOf(e.message);
            if (index !== -1)
                e.stack = e.stack.substring(0, index) + newMessage + e.stack.substring(index + e.message.length);
            e.message = newMessage;
            throw e;
        }
        const unsupportedToken = tokens.find(token => {
            return (token instanceof AtKeywordToken) ||
                (token instanceof BadStringToken) ||
                (token instanceof BadURLToken) ||
                (token instanceof ColumnToken) ||
                (token instanceof CDOToken) ||
                (token instanceof CDCToken) ||
                (token instanceof SemicolonToken) ||
                (token instanceof OpenCurlyToken) ||
                (token instanceof CloseCurlyToken) ||
                (token instanceof URLToken) ||
                (token instanceof PercentageToken);
        });
        if (unsupportedToken)
            throw new InvalidSelectorError(`Unsupported token "${unsupportedToken.toSource()}" while parsing selector "${selector}"`);
        let pos = 0;
        const names = new Set();

        function unexpected() {
            return new InvalidSelectorError(`Unexpected token "${tokens[pos].toSource()}" while parsing selector "${selector}"`);
        }

        function skipWhitespace() {
            while (tokens[pos] instanceof WhitespaceToken)
                pos++;
        }

        function isIdent(p = pos) {
            return tokens[p] instanceof IdentToken;
        }

        function isString(p = pos) {
            return tokens[p] instanceof StringToken;
        }

        function isNumber(p = pos) {
            return tokens[p] instanceof NumberToken;
        }

        function isComma(p = pos) {
            return tokens[p] instanceof CommaToken;
        }

        function isCloseParen(p = pos) {
            return tokens[p] instanceof CloseParenToken;
        }

        function isStar(p = pos) {
            return (tokens[p] instanceof DelimToken) && tokens[p].value === '*';
        }

        function isEOF(p = pos) {
            return tokens[p] instanceof EOFToken;
        }

        function isClauseCombinator(p = pos) {
            return (tokens[p] instanceof DelimToken) && (['>', '+', '~'].includes(tokens[p].value));
        }

        function isSelectorClauseEnd(p = pos) {
            return isComma(p) || isCloseParen(p) || isEOF(p) || isClauseCombinator(p) || (tokens[p] instanceof WhitespaceToken);
        }

        function consumeFunctionArguments() {
            const result = [consumeArgument()];
            while (true) {
                skipWhitespace();
                if (!isComma())
                    break;
                pos++;
                result.push(consumeArgument());
            }
            return result;
        }

        function consumeArgument() {
            skipWhitespace();
            if (isNumber())
                return tokens[pos++].value;
            if (isString())
                return tokens[pos++].value;
            return consumeComplexSelector();
        }

        function consumeComplexSelector() {
            const result = {
                simples: []
            };
            skipWhitespace();
            if (isClauseCombinator()) {
                result.simples.push({
                    selector: {
                        functions: [{
                            name: 'scope',
                            args: []
                        }]
                    },
                    combinator: ''
                });
            } else {
                result.simples.push({
                    selector: consumeSimpleSelector(),
                    combinator: ''
                });
            }
            while (true) {
                skipWhitespace();
                if (isClauseCombinator()) {
                    result.simples[result.simples.length - 1].combinator = tokens[pos++].value;
                    skipWhitespace();
                } else if (isSelectorClauseEnd()) {
                    break;
                }
                result.simples.push({
                    combinator: '',
                    selector: consumeSimpleSelector()
                });
            }
            return result;
        }

        function consumeSimpleSelector() {
            let rawCSSString = '';
            const functions = [];
            while (!isSelectorClauseEnd()) {
                if (isIdent() || isStar()) {
                    rawCSSString += tokens[pos++].toSource();
                } else if (tokens[pos] instanceof HashToken) {
                    rawCSSString += tokens[pos++].toSource();
                } else if ((tokens[pos] instanceof DelimToken) && tokens[pos].value === '.') {
                    pos++;
                    if (isIdent())
                        rawCSSString += '.' + tokens[pos++].toSource();
                    else
                        throw unexpected();
                } else if (tokens[pos] instanceof ColonToken) {
                    pos++;
                    if (isIdent()) {
                        if (!customNames.has(tokens[pos].value.toLowerCase())) {
                            rawCSSString += ':' + tokens[pos++].toSource();
                        } else {
                            const name = tokens[pos++].value.toLowerCase();
                            functions.push({
                                name,
                                args: []
                            });
                            names.add(name);
                        }
                    } else if (tokens[pos] instanceof FunctionToken) {
                        const name = tokens[pos++].value.toLowerCase();
                        if (!customNames.has(name)) {
                            rawCSSString += `:${name}(${consumeBuiltinFunctionArguments()})`;
                        } else {
                            functions.push({
                                name,
                                args: consumeFunctionArguments()
                            });
                            names.add(name);
                        }
                        skipWhitespace();
                        if (!isCloseParen())
                            throw unexpected();
                        pos++;
                    } else {
                        throw unexpected();
                    }
                } else if (tokens[pos] instanceof OpenSquareToken) {
                    rawCSSString += '[';
                    pos++;
                    while (!(tokens[pos] instanceof CloseSquareToken) && !isEOF())
                        rawCSSString += tokens[pos++].toSource();
                    if (!(tokens[pos] instanceof CloseSquareToken))
                        throw unexpected();
                    rawCSSString += ']';
                    pos++;
                } else {
                    throw unexpected();
                }
            }
            if (!rawCSSString && !functions.length)
                throw unexpected();
            return {
                css: rawCSSString || undefined,
                functions
            };
        }

        function consumeBuiltinFunctionArguments() {
            let s = '';
            while (!isCloseParen() && !isEOF())
                s += tokens[pos++].toSource();
            return s;
        }
        const result = consumeFunctionArguments();
        if (!isEOF())
            throw new InvalidSelectorError(`Error while parsing selector "${selector}"`);
        if (result.some(arg => typeof arg !== 'object' || !('simples' in arg)))
            throw new InvalidSelectorError(`Error while parsing selector "${selector}"`);
        return {
            selector: result,
            names: Array.from(names)
        };
    }

    const customCSSNames = new Set(['not', 'is', 'where', 'has', 'scope', 'light', 'visible', 'text', 'text-matches', 'text-is', 'has-text', 'above', 'below', 'right-of', 'left-of', 'near', 'nth-match']);

    function parseSelector(selector) {
        const result = parseSelectorString(selector);
        const parts = result.parts.map(part => {
            if (part.name === 'css' || part.name === 'css:light') {
                if (part.name === 'css:light')
                    part.body = ':light(' + part.body + ')';
                const parsedCSS = parseCSS(part.body, customCSSNames);
                return {
                    name: 'css',
                    body: parsedCSS.selector,
                    source: part.body
                };
            }
            return {
                ...part,
                source: part.body
            };
        });
        return {
            capture: result.capture,
            parts
        };
    }

    function stringifySelector(selector) {
        if (typeof selector === 'string')
            return selector;
        return selector.parts.map((p, i) => {
            const prefix = p.name === 'css' ? '' : p.name + '=';
            return `${i === selector.capture ? '*' : ''}${prefix}${p.source}`;
        }).join(' >> ');
    }

    function parseSelectorString(selector) {
        let index = 0;
        let quote;
        let start = 0;
        const result = {
            parts: []
        };
        const append = () => {
            const part = selector.substring(start, index).trim();
            const eqIndex = part.indexOf('=');
            let name;
            let body;
            if (eqIndex !== -1 && part.substring(0, eqIndex).trim().match(/^[a-zA-Z_0-9-+:*]+$/)) {
                name = part.substring(0, eqIndex).trim();
                body = part.substring(eqIndex + 1);
            } else if (part.length > 1 && part[0] === '"' && part[part.length - 1] === '"') {
                name = 'text';
                body = part;
            } else if (part.length > 1 && part[0] === "'" && part[part.length - 1] === "'") {
                name = 'text';
                body = part;
            } else if (/^\(*\/\//.test(part) || part.startsWith('..')) {
                name = 'xpath';
                body = part;
            } else {
                name = 'css';
                body = part;
            }
            let capture = false;
            if (name[0] === '*') {
                capture = true;
                name = name.substring(1);
            }
            result.parts.push({
                name,
                body
            });
            if (capture) {
                if (result.capture !== undefined)
                    throw new InvalidSelectorError(`Only one of the selectors can capture using * modifier`);
                result.capture = result.parts.length - 1;
            }
        };
        if (!selector.includes('>>')) {
            index = selector.length;
            append();
            return result;
        }
        while (index < selector.length) {
            const c = selector[index];
            if (c === '\\' && index + 1 < selector.length) {
                index += 2;
            } else if (c === quote) {
                quote = undefined;
                index++;
            } else if (!quote && (c === '"' || c === '\'' || c === '`')) {
                quote = c;
                index++;
            } else if (!quote && c === '>' && selector[index + 1] === '>') {
                append();
                index += 2;
                start = index;
            } else {
                index++;
            }
        }
        append();
        return result;
    }

    class SelectorEvaluatorImpl {
        _engines = new Map();
        _cacheQueryCSS = new Map();
        _cacheMatches = new Map();
        _cacheQuery = new Map();
        _cacheMatchesSimple = new Map();
        _cacheMatchesParents = new Map();
        _cacheCallMatches = new Map();
        _cacheCallQuery = new Map();
        _cacheQuerySimple = new Map();
        _cacheText = new Map();
        _scoreMap;
        _retainCacheCounter = 0;
        constructor(extraEngines) {
            for (const [name, engine] of extraEngines)
                this._engines.set(name, engine);
            this._engines.set('not', notEngine);
            this._engines.set('is', isEngine);
            this._engines.set('where', isEngine);
            this._engines.set('has', hasEngine);
            this._engines.set('scope', scopeEngine);
            this._engines.set('light', lightEngine);
            this._engines.set('visible', visibleEngine);
            this._engines.set('text', textEngine);
            this._engines.set('text-is', textIsEngine);
            this._engines.set('text-matches', textMatchesEngine);
            this._engines.set('has-text', hasTextEngine);
            this._engines.set('right-of', createPositionEngine('right-of', boxRightOf));
            this._engines.set('left-of', createPositionEngine('left-of', boxLeftOf));
            this._engines.set('above', createPositionEngine('above', boxAbove));
            this._engines.set('below', createPositionEngine('below', boxBelow));
            this._engines.set('near', createPositionEngine('near', boxNear));
            this._engines.set('nth-match', nthMatchEngine);
            const allNames = [...this._engines.keys()];
            allNames.sort();
            const parserNames = [...customCSSNames];
            parserNames.sort();
            if (allNames.join('|') !== parserNames.join('|'))
                throw new Error(`Please keep customCSSNames in sync with evaluator engines: ${allNames.join('|')} vs ${parserNames.join('|')}`);
        }
        begin() {
            ++this._retainCacheCounter;
        }
        end() {
            --this._retainCacheCounter;
            if (!this._retainCacheCounter) {
                this._cacheQueryCSS.clear();
                this._cacheMatches.clear();
                this._cacheQuery.clear();
                this._cacheMatchesSimple.clear();
                this._cacheMatchesParents.clear();
                this._cacheCallMatches.clear();
                this._cacheCallQuery.clear();
                this._cacheQuerySimple.clear();
                this._cacheText.clear();
            }
        }
        _cached(cache, main, rest, cb) {
            if (!cache.has(main))
                cache.set(main, []);
            const entries = cache.get(main);
            const entry = entries.find(e => rest.every((value, index) => e.rest[index] === value));
            if (entry)
                return entry.result;
            const result = cb();
            entries.push({
                rest,
                result
            });
            return result;
        }
        _checkSelector(s) {
            const wellFormed = typeof s === 'object' && s &&
                (Array.isArray(s) || ('simples' in s) && (s.simples.length));
            if (!wellFormed)
                throw new Error(`Malformed selector "${s}"`);
            return s;
        }
        matches(element, s, context) {
            const selector = this._checkSelector(s);
            this.begin();
            try {
                return this._cached(this._cacheMatches, element, [selector, context.scope, context.pierceShadow], () => {
                    if (Array.isArray(selector))
                        return this._matchesEngine(isEngine, element, selector, context);
                    if (!this._matchesSimple(element, selector.simples[selector.simples.length - 1].selector, context))
                        return false;
                    return this._matchesParents(element, selector, selector.simples.length - 2, context);
                });
            } finally {
                this.end();
            }
        }
        query(context, s) {
            const selector = this._checkSelector(s);
            this.begin();
            try {
                return this._cached(this._cacheQuery, selector, [context.scope, context.pierceShadow], () => {
                    if (Array.isArray(selector))
                        return this._queryEngine(isEngine, context, selector);
                    const previousScoreMap = this._scoreMap;
                    this._scoreMap = new Map();
                    let elements = this._querySimple(context, selector.simples[selector.simples.length - 1].selector);
                    elements = elements.filter(element => this._matchesParents(element, selector, selector.simples.length - 2, context));
                    if (this._scoreMap.size) {
                        elements.sort((a, b) => {
                            const aScore = this._scoreMap.get(a);
                            const bScore = this._scoreMap.get(b);
                            if (aScore === bScore)
                                return 0;
                            if (aScore === undefined)
                                return 1;
                            if (bScore === undefined)
                                return -1;
                            return aScore - bScore;
                        });
                    }
                    this._scoreMap = previousScoreMap;
                    return elements;
                });
            } finally {
                this.end();
            }
        }
        _markScore(element, score) {
            if (this._scoreMap)
                this._scoreMap.set(element, score);
        }
        _matchesSimple(element, simple, context) {
            return this._cached(this._cacheMatchesSimple, element, [simple, context.scope, context.pierceShadow], () => {
                const isPossiblyScopeClause = simple.functions.some(f => f.name === 'scope' || f.name === 'is');
                if (!isPossiblyScopeClause && element === context.scope)
                    return false;
                if (simple.css && !this._matchesCSS(element, simple.css))
                    return false;
                for (const func of simple.functions) {
                    if (!this._matchesEngine(this._getEngine(func.name), element, func.args, context))
                        return false;
                }
                return true;
            });
        }
        _querySimple(context, simple) {
            if (!simple.functions.length)
                return this._queryCSS(context, simple.css || '*');
            return this._cached(this._cacheQuerySimple, simple, [context.scope, context.pierceShadow], () => {
                let css = simple.css;
                const funcs = simple.functions;
                if (css === '*' && funcs.length)
                    css = undefined;
                let elements;
                let firstIndex = -1;
                if (css !== undefined) {
                    elements = this._queryCSS(context, css);
                    const hasScopeClause = funcs.some(f => f.name === 'scope');
                    if (hasScopeClause && context.scope.nodeType === 1)
                        elements.unshift(context.scope);
                } else {
                    firstIndex = funcs.findIndex(func => this._getEngine(func.name).query !== undefined);
                    if (firstIndex === -1)
                        firstIndex = 0;
                    elements = this._queryEngine(this._getEngine(funcs[firstIndex].name), context, funcs[firstIndex].args);
                }
                for (let i = 0; i < funcs.length; i++) {
                    if (i === firstIndex)
                        continue;
                    const engine = this._getEngine(funcs[i].name);
                    if (engine.matches !== undefined)
                        elements = elements.filter(e => this._matchesEngine(engine, e, funcs[i].args, context));
                }
                for (let i = 0; i < funcs.length; i++) {
                    if (i === firstIndex)
                        continue;
                    const engine = this._getEngine(funcs[i].name);
                    if (engine.matches === undefined)
                        elements = elements.filter(e => this._matchesEngine(engine, e, funcs[i].args, context));
                }
                return elements;
            });
        }
        _matchesParents(element, complex, index, context) {
            if (index < 0)
                return true;
            return this._cached(this._cacheMatchesParents, element, [complex, index, context.scope, context.pierceShadow], () => {
                const {
                    selector: simple,
                    combinator
                } = complex.simples[index];
                if (combinator === '>') {
                    const parent = parentElementOrShadowHostInContext(element, context);
                    if (!parent || !this._matchesSimple(parent, simple, context))
                        return false;
                    return this._matchesParents(parent, complex, index - 1, context);
                }
                if (combinator === '+') {
                    const previousSibling = previousSiblingInContext(element, context);
                    if (!previousSibling || !this._matchesSimple(previousSibling, simple, context))
                        return false;
                    return this._matchesParents(previousSibling, complex, index - 1, context);
                }
                if (combinator === '') {
                    let parent = parentElementOrShadowHostInContext(element, context);
                    while (parent) {
                        if (this._matchesSimple(parent, simple, context)) {
                            if (this._matchesParents(parent, complex, index - 1, context))
                                return true;
                            if (complex.simples[index - 1].combinator === '')
                                break;
                        }
                        parent = parentElementOrShadowHostInContext(parent, context);
                    }
                    return false;
                }
                if (combinator === '~') {
                    let previousSibling = previousSiblingInContext(element, context);
                    while (previousSibling) {
                        if (this._matchesSimple(previousSibling, simple, context)) {
                            if (this._matchesParents(previousSibling, complex, index - 1, context))
                                return true;
                            if (complex.simples[index - 1].combinator === '~')
                                break;
                        }
                        previousSibling = previousSiblingInContext(previousSibling, context);
                    }
                    return false;
                }
                if (combinator === '>=') {
                    let parent = element;
                    while (parent) {
                        if (this._matchesSimple(parent, simple, context)) {
                            if (this._matchesParents(parent, complex, index - 1, context))
                                return true;
                            if (complex.simples[index - 1].combinator === '')
                                break;
                        }
                        parent = parentElementOrShadowHostInContext(parent, context);
                    }
                    return false;
                }
                throw new Error(`Unsupported combinator "${combinator}"`);
            });
        }
        _matchesEngine(engine, element, args, context) {
            if (engine.matches)
                return this._callMatches(engine, element, args, context);
            if (engine.query)
                return this._callQuery(engine, args, context).includes(element);
            throw new Error(`Selector engine should implement "matches" or "query"`);
        }
        _queryEngine(engine, context, args) {
            if (engine.query)
                return this._callQuery(engine, args, context);
            if (engine.matches)
                return this._queryCSS(context, '*').filter(element => this._callMatches(engine, element, args, context));
            throw new Error(`Selector engine should implement "matches" or "query"`);
        }
        _callMatches(engine, element, args, context) {
            return this._cached(this._cacheCallMatches, element, [engine, context.scope, context.pierceShadow, ...args], () => {
                return engine.matches(element, args, context, this);
            });
        }
        _callQuery(engine, args, context) {
            return this._cached(this._cacheCallQuery, engine, [context.scope, context.pierceShadow, ...args], () => {
                return engine.query(context, args, this);
            });
        }
        _matchesCSS(element, css) {
            return element.matches(css);
        }
        _queryCSS(context, css) {
            return this._cached(this._cacheQueryCSS, css, [context.scope, context.pierceShadow], () => {
                let result = [];

                function query(root) {
                    result = result.concat([...root.querySelectorAll(css)]);
                    if (!context.pierceShadow)
                        return;
                    if (root.shadowRoot)
                        query(root.shadowRoot);
                    for (const element of root.querySelectorAll('*')) {
                        if (element.shadowRoot)
                            query(element.shadowRoot);
                    }
                }
                query(context.scope);
                return result;
            });
        }
        _getEngine(name) {
            const engine = this._engines.get(name);
            if (!engine)
                throw new Error(`Unknown selector engine "${name}"`);
            return engine;
        }
    }
    const isEngine = {
        matches(element, args, context, evaluator) {
            if (args.length === 0)
                throw new Error(`"is" engine expects non-empty selector list`);
            return args.some(selector => evaluator.matches(element, selector, context));
        },
        query(context, args, evaluator) {
            if (args.length === 0)
                throw new Error(`"is" engine expects non-empty selector list`);
            let elements = [];
            for (const arg of args)
                elements = elements.concat(evaluator.query(context, arg));
            return args.length === 1 ? elements : sortInDOMOrder(elements);
        },
    };
    const hasEngine = {
        matches(element, args, context, evaluator) {
            if (args.length === 0)
                throw new Error(`"has" engine expects non-empty selector list`);
            return evaluator.query({
                ...context,
                scope: element
            }, args).length > 0;
        },
    };
    const scopeEngine = {
        matches(element, args, context, evaluator) {
            if (args.length !== 0)
                throw new Error(`"scope" engine expects no arguments`);
            if (context.scope.nodeType === 9)
                return element === context.scope.documentElement;
            return element === context.scope;
        },
        query(context, args, evaluator) {
            if (args.length !== 0)
                throw new Error(`"scope" engine expects no arguments`);
            if (context.scope.nodeType === 9) {
                const root = context.scope.documentElement;
                return root ? [root] : [];
            }
            if (context.scope.nodeType === 1)
                return [context.scope];
            return [];
        },
    };
    const notEngine = {
        matches(element, args, context, evaluator) {
            if (args.length === 0)
                throw new Error(`"not" engine expects non-empty selector list`);
            return !evaluator.matches(element, args, context);
        },
    };
    const lightEngine = {
        query(context, args, evaluator) {
            return evaluator.query({
                ...context,
                pierceShadow: false
            }, args);
        },
        matches(element, args, context, evaluator) {
            return evaluator.matches(element, args, {
                ...context,
                pierceShadow: false
            });
        }
    };
    const visibleEngine = {
        matches(element, args, context, evaluator) {
            if (args.length)
                throw new Error(`"visible" engine expects no arguments`);
            return isVisible(element);
        }
    };
    const textEngine = {
        matches(element, args, context, evaluator) {
            if (args.length !== 1 || typeof args[0] !== 'string')
                throw new Error(`"text" engine expects a single string`);
            const matcher = createLaxTextMatcher(args[0]);
            return elementMatchesText(evaluator, element, matcher) === 'self';
        },
    };
    const textIsEngine = {
        matches(element, args, context, evaluator) {
            if (args.length !== 1 || typeof args[0] !== 'string')
                throw new Error(`"text-is" engine expects a single string`);
            const matcher = createStrictTextMatcher(args[0]);
            return elementMatchesText(evaluator, element, matcher) !== 'none';
        },
    };
    const textMatchesEngine = {
        matches(element, args, context, evaluator) {
            if (args.length === 0 || typeof args[0] !== 'string' || args.length > 2 || (args.length === 2 && typeof args[1] !== 'string'))
                throw new Error(`"text-matches" engine expects a regexp body and optional regexp flags`);
            const matcher = createRegexTextMatcher(args[0], args.length === 2 ? args[1] : undefined);
            return elementMatchesText(evaluator, element, matcher) === 'self';
        },
    };
    const hasTextEngine = {
        matches(element, args, context, evaluator) {
            if (args.length !== 1 || typeof args[0] !== 'string')
                throw new Error(`"has-text" engine expects a single string`);
            if (shouldSkipForTextMatching(element))
                return false;
            const matcher = createLaxTextMatcher(args[0]);
            return matcher(elementText(evaluator, element));
        },
    };

    function createLaxTextMatcher(text) {
        text = text.trim().replace(/\s+/g, ' ').toLowerCase();
        return (elementText) => {
            const s = elementText.full.trim().replace(/\s+/g, ' ').toLowerCase();
            return s.includes(text);
        };
    }

    function createStrictTextMatcher(text) {
        text = text.trim().replace(/\s+/g, ' ');
        return (elementText) => {
            if (!text && !elementText.immediate.length)
                return true;
            return elementText.immediate.some(s => s.trim().replace(/\s+/g, ' ') === text);
        };
    }

    function createRegexTextMatcher(source, flags) {
        const re = new RegExp(source, flags);
        return (elementText) => {
            return re.test(elementText.full);
        };
    }

    function shouldSkipForTextMatching(element) {
        return element.nodeName === 'SCRIPT' || element.nodeName === 'STYLE' || document.head && document.head.contains(element);
    }

    function elementText(evaluator, root) {
        let value = evaluator._cacheText.get(root);
        if (value === undefined) {
            value = {
                full: '',
                immediate: []
            };
            if (!shouldSkipForTextMatching(root)) {
                let currentImmediate = '';
                if ((root instanceof HTMLInputElement) && (root.type === 'submit' || root.type === 'button')) {
                    value = {
                        full: root.value,
                        immediate: [root.value]
                    };
                } else {
                    for (let child = root.firstChild; child; child = child.nextSibling) {
                        if (child.nodeType === Node.TEXT_NODE) {
                            value.full += child.nodeValue || '';
                            currentImmediate += child.nodeValue || '';
                        } else {
                            if (currentImmediate)
                                value.immediate.push(currentImmediate);
                            currentImmediate = '';
                            if (child.nodeType === Node.ELEMENT_NODE)
                                value.full += elementText(evaluator, child).full;
                        }
                    }
                    if (currentImmediate)
                        value.immediate.push(currentImmediate);
                    if (root.shadowRoot)
                        value.full += elementText(evaluator, root.shadowRoot).full;
                }
            }
            evaluator._cacheText.set(root, value);
        }
        return value;
    }

    function elementMatchesText(evaluator, element, matcher) {
        if (shouldSkipForTextMatching(element))
            return 'none';
        if (!matcher(elementText(evaluator, element)))
            return 'none';
        for (let child = element.firstChild; child; child = child.nextSibling) {
            if (child.nodeType === Node.ELEMENT_NODE && matcher(elementText(evaluator, child)))
                return 'selfAndChildren';
        }
        if (element.shadowRoot && matcher(elementText(evaluator, element.shadowRoot)))
            return 'selfAndChildren';
        return 'self';
    }

    function boxRightOf(box1, box2, maxDistance) {
        const distance = box1.left - box2.right;
        if (distance < 0 || (maxDistance !== undefined && distance > maxDistance))
            return;
        return distance + Math.max(box2.bottom - box1.bottom, 0) + Math.max(box1.top - box2.top, 0);
    }

    function boxLeftOf(box1, box2, maxDistance) {
        const distance = box2.left - box1.right;
        if (distance < 0 || (maxDistance !== undefined && distance > maxDistance))
            return;
        return distance + Math.max(box2.bottom - box1.bottom, 0) + Math.max(box1.top - box2.top, 0);
    }

    function boxAbove(box1, box2, maxDistance) {
        const distance = box2.top - box1.bottom;
        if (distance < 0 || (maxDistance !== undefined && distance > maxDistance))
            return;
        return distance + Math.max(box1.left - box2.left, 0) + Math.max(box2.right - box1.right, 0);
    }

    function boxBelow(box1, box2, maxDistance) {
        const distance = box1.top - box2.bottom;
        if (distance < 0 || (maxDistance !== undefined && distance > maxDistance))
            return;
        return distance + Math.max(box1.left - box2.left, 0) + Math.max(box2.right - box1.right, 0);
    }

    function boxNear(box1, box2, maxDistance) {
        const kThreshold = maxDistance === undefined ? 50 : maxDistance;
        let score = 0;
        if (box1.left - box2.right >= 0)
            score += box1.left - box2.right;
        if (box2.left - box1.right >= 0)
            score += box2.left - box1.right;
        if (box2.top - box1.bottom >= 0)
            score += box2.top - box1.bottom;
        if (box1.top - box2.bottom >= 0)
            score += box1.top - box2.bottom;
        return score > kThreshold ? undefined : score;
    }

    function createPositionEngine(name, scorer) {
        return {
            matches(element, args, context, evaluator) {
                const maxDistance = args.length && typeof args[args.length - 1] === 'number' ? args[args.length - 1] : undefined;
                const queryArgs = maxDistance === undefined ? args : args.slice(0, args.length - 1);
                if (args.length < 1 + (maxDistance === undefined ? 0 : 1))
                    throw new Error(`"${name}" engine expects a selector list and optional maximum distance in pixels`);
                const box = element.getBoundingClientRect();
                let bestScore;
                for (const e of evaluator.query(context, queryArgs)) {
                    if (e === element)
                        continue;
                    const score = scorer(box, e.getBoundingClientRect(), maxDistance);
                    if (score === undefined)
                        continue;
                    if (bestScore === undefined || score < bestScore)
                        bestScore = score;
                }
                if (bestScore === undefined)
                    return false;
                evaluator._markScore(element, bestScore);
                return true;
            }
        };
    }
    const nthMatchEngine = {
        query(context, args, evaluator) {
            let index = args[args.length - 1];
            if (args.length < 2)
                throw new Error(`"nth-match" engine expects non-empty selector list and an index argument`);
            if (typeof index !== 'number' || index < 1)
                throw new Error(`"nth-match" engine expects a one-based index as the last argument`);
            const elements = isEngine.query(context, args.slice(0, args.length - 1), evaluator);
            index--;
            return index < elements.length ? [elements[index]] : [];
        },
    };

    function isInsideScope(scope, element) {
        while (element) {
            if (scope.contains(element))
                return true;
            while (element.parentElement)
                element = element.parentElement;
            element = parentElementOrShadowHost$1(element);
        }
        return false;
    }

    function parentElementOrShadowHost$1(element) {
        if (element.parentElement)
            return element.parentElement;
        if (!element.parentNode)
            return;
        if (element.parentNode.nodeType === Node.DOCUMENT_FRAGMENT_NODE && element.parentNode.host)
            return element.parentNode.host;
    }

    function parentElementOrShadowHostInContext(element, context) {
        if (element === context.scope)
            return;
        if (!context.pierceShadow)
            return element.parentElement || undefined;
        return parentElementOrShadowHost$1(element);
    }

    function previousSiblingInContext(element, context) {
        if (element === context.scope)
            return;
        return element.previousElementSibling || undefined;
    }

    function isVisible(element) {
        if (!element.ownerDocument || !element.ownerDocument.defaultView)
            return true;
        const style = element.ownerDocument.defaultView.getComputedStyle(element);
        if (!style || style.visibility === 'hidden')
            return false;
        if (style.display === 'contents') {
            for (let child = element.firstChild; child; child = child.nextSibling) {
                if (child.nodeType === 1 && isVisible(child))
                    return true;
                if (child.nodeType === 3 && isVisibleTextNode(child))
                    return true;
            }
            return false;
        }
        const rect = element.getBoundingClientRect();
        return rect.width > 0 && rect.height > 0;
    }

    function isVisibleTextNode(node) {
        const range = document.createRange();
        range.selectNode(node);
        const rect = range.getBoundingClientRect();
        return rect.width > 0 && rect.height > 0;
    }

    function sortInDOMOrder(elements) {
        const elementToEntry = new Map();
        const roots = [];
        const result = [];

        function append(element) {
            let entry = elementToEntry.get(element);
            if (entry)
                return entry;
            const parent = parentElementOrShadowHost$1(element);
            if (parent) {
                const parentEntry = append(parent);
                parentEntry.children.push(element);
            } else {
                roots.push(element);
            }
            entry = {
                children: [],
                taken: false
            };
            elementToEntry.set(element, entry);
            return entry;
        }
        elements.forEach(e => append(e).taken = true);

        function visit(element) {
            const entry = elementToEntry.get(element);
            if (entry.taken)
                result.push(element);
            if (entry.children.length > 1) {
                const set = new Set(entry.children);
                entry.children = [];
                let child = element.firstElementChild;
                while (child && entry.children.length < set.size) {
                    if (set.has(child))
                        entry.children.push(child);
                    child = child.nextElementSibling;
                }
                child = element.shadowRoot ? element.shadowRoot.firstElementChild : null;
                while (child && entry.children.length < set.size) {
                    if (set.has(child))
                        entry.children.push(child);
                    child = child.nextElementSibling;
                }
            }
            entry.children.forEach(visit);
        }
        roots.forEach(visit);
        return result;
    }

    function checkComponentAttribute(obj, attr) {
        for (const token of attr.jsonPath) {
            if (obj !== undefined && obj !== null)
                obj = obj[token];
        }
        const objValue = typeof obj === 'string' && !attr.caseSensetive ? obj.toUpperCase() : obj;
        const attrValue = typeof attr.value === 'string' && !attr.caseSensetive ? attr.value.toUpperCase() : attr.value;
        if (attr.op === '<truthy>')
            return !!objValue;
        if (attr.op === '=')
            return objValue === attrValue;
        if (typeof objValue !== 'string' || typeof attrValue !== 'string')
            return false;
        if (attr.op === '*=')
            return objValue.includes(attrValue);
        if (attr.op === '^=')
            return objValue.startsWith(attrValue);
        if (attr.op === '$=')
            return objValue.endsWith(attrValue);
        if (attr.op === '|=')
            return objValue === attrValue || objValue.startsWith(attrValue + '-');
        if (attr.op === '~=')
            return objValue.split(' ').includes(attrValue);
        return false;
    }

    function parseComponentSelector(selector) {
        let wp = 0;
        let EOL = selector.length === 0;
        const next = () => selector[wp] || '';
        const eat1 = () => {
            const result = next();
            ++wp;
            EOL = wp >= selector.length;
            return result;
        };
        const syntaxError = (stage) => {
            if (EOL)
                throw new Error(`Unexpected end of selector while parsing selector \`${selector}\``);
            throw new Error(`Error while parsing selector \`${selector}\` - unexpected symbol "${next()}" at position ${wp}` + (stage ? ' during ' + stage : ''));
        };

        function skipSpaces() {
            while (!EOL && /\s/.test(next()))
                eat1();
        }

        function readIdentifier() {
            let result = '';
            skipSpaces();
            while (!EOL && /[-$0-9A-Z_]/i.test(next()))
                result += eat1();
            return result;
        }

        function readQuotedString(quote) {
            let result = eat1();
            if (result !== quote)
                syntaxError('parsing quoted string');
            while (!EOL && next() !== quote) {
                if (next() === '\\')
                    eat1();
                result += eat1();
            }
            if (next() !== quote)
                syntaxError('parsing quoted string');
            result += eat1();
            return result;
        }

        function readAttributeToken() {
            let token = '';
            skipSpaces();
            if (next() === `'` || next() === `"`)
                token = readQuotedString(next()).slice(1, -1);
            else
                token = readIdentifier();
            if (!token)
                syntaxError('parsing property path');
            return token;
        }

        function readOperator() {
            skipSpaces();
            let op = '';
            if (!EOL)
                op += eat1();
            if (!EOL && (op !== '='))
                op += eat1();
            if (!['=', '*=', '^=', '$=', '|=', '~='].includes(op))
                syntaxError('parsing operator');
            return op;
        }

        function readAttribute() {
            eat1();
            const jsonPath = [];
            jsonPath.push(readAttributeToken());
            skipSpaces();
            while (next() === '.') {
                eat1();
                jsonPath.push(readAttributeToken());
                skipSpaces();
            }
            if (next() === ']') {
                eat1();
                return {
                    jsonPath,
                    op: '<truthy>',
                    value: null,
                    caseSensetive: false
                };
            }
            const operator = readOperator();
            let value = undefined;
            let caseSensetive = true;
            skipSpaces();
            if (next() === `'` || next() === `"`) {
                value = readQuotedString(next()).slice(1, -1);
                skipSpaces();
                if (next() === 'i' || next() === 'I') {
                    caseSensetive = false;
                    eat1();
                } else if (next() === 's' || next() === 'S') {
                    caseSensetive = true;
                    eat1();
                }
            } else {
                value = '';
                while (!EOL && !/\s/.test(next()) && next() !== ']')
                    value += eat1();
                if (value === 'true') {
                    value = true;
                } else if (value === 'false') {
                    value = false;
                } else {
                    value = +value;
                    if (isNaN(value))
                        syntaxError('parsing attribute value');
                }
            }
            skipSpaces();
            if (next() !== ']')
                syntaxError('parsing attribute value');
            eat1();
            if (operator !== '=' && typeof value !== 'string')
                throw new Error(`Error while parsing selector \`${selector}\` - cannot use ${operator} in attribute with non-string matching value - ${value}`);
            return {
                jsonPath,
                op: operator,
                value,
                caseSensetive
            };
        }
        const result = {
            name: '',
            attributes: [],
        };
        result.name = readIdentifier();
        skipSpaces();
        while (next() === '[') {
            result.attributes.push(readAttribute());
            skipSpaces();
        }
        if (!EOL)
            syntaxError(undefined);
        if (!result.name && !result.attributes.length)
            throw new Error(`Error while parsing selector \`${selector}\` - selector cannot be empty`);
        return result;
    }

    function getComponentName(reactElement) {
        if (typeof reactElement.type === 'function')
            return reactElement.type.displayName || reactElement.type.name || 'Anonymous';
        if (typeof reactElement.type === 'string')
            return reactElement.type;
        if (reactElement._currentElement) {
            const elementType = reactElement._currentElement.type;
            if (typeof elementType === 'string')
                return elementType;
            if (typeof elementType === 'function')
                return elementType.displayName || elementType.name || 'Anonymous';
        }
        return '';
    }

    function getChildren(reactElement) {
        if (reactElement.child) {
            const children = [];
            for (let child = reactElement.child; child; child = child.sibling)
                children.push(child);
            return children;
        }
        if (!reactElement._currentElement)
            return [];
        const isKnownElement = (reactElement) => {
            const elementType = reactElement._currentElement?.type;
            return typeof elementType === 'function' || typeof elementType === 'string';
        };
        if (reactElement._renderedComponent) {
            const child = reactElement._renderedComponent;
            return isKnownElement(child) ? [child] : [];
        }
        if (reactElement._renderedChildren)
            return [...Object.values(reactElement._renderedChildren)].filter(isKnownElement);
        return [];
    }

    function getProps(reactElement) {
        const props = reactElement.memoizedProps ||
            reactElement._currentElement?.props;
        if (!props || typeof props === 'string')
            return props;
        const result = {
            ...props
        };
        delete result.children;
        return result;
    }

    function buildComponentsTree(reactElement) {
        const treeNode = {
            name: getComponentName(reactElement),
            children: getChildren(reactElement).map(buildComponentsTree),
            rootElements: [],
            props: getProps(reactElement),
        };
        const rootElement = reactElement.stateNode ||
            reactElement._hostNode || reactElement._renderedComponent?._hostNode;
        if (rootElement instanceof Element) {
            treeNode.rootElements.push(rootElement);
        } else {
            for (const child of treeNode.children)
                treeNode.rootElements.push(...child.rootElements);
        }
        return treeNode;
    }

    function filterComponentsTree$1(treeNode, searchFn, result = []) {
        if (searchFn(treeNode))
            result.push(treeNode);
        for (const child of treeNode.children)
            filterComponentsTree$1(child, searchFn, result);
        return result;
    }

    function findReactRoots(root, roots = []) {
        const walker = document.createTreeWalker(root, NodeFilter.SHOW_ELEMENT);
        do {
            const node = walker.currentNode;
            if (node.hasOwnProperty('_reactRootContainer'))
                roots.push(node._reactRootContainer._internalRoot.current);
            if ((node instanceof Element) && node.hasAttribute('data-reactroot')) {
                for (const key of Object.keys(node)) {
                    if (key.startsWith('__reactInternalInstance') || key.startsWith('__reactFiber'))
                        roots.push(node[key]);
                }
            }
            const shadowRoot = node instanceof Element ? node.shadowRoot : null;
            if (shadowRoot)
                findReactRoots(shadowRoot, roots);
        } while (walker.nextNode());
        return roots;
    }
    const ReactEngine = {
        queryAll(scope, selector) {
            const {
                name,
                attributes
            } = parseComponentSelector(selector);
            const reactRoots = findReactRoots(document);
            const trees = reactRoots.map(reactRoot => buildComponentsTree(reactRoot));
            const treeNodes = trees.map(tree => filterComponentsTree$1(tree, treeNode => {
                if (name && treeNode.name !== name)
                    return false;
                if (treeNode.rootElements.some(domNode => !isInsideScope(scope, domNode)))
                    return false;
                for (const attr of attributes) {
                    if (!checkComponentAttribute(treeNode.props, attr))
                        return false;
                }
                return true;
            })).flat();
            const allRootElements = new Set();
            for (const treeNode of treeNodes) {
                for (const domNode of treeNode.rootElements)
                    allRootElements.add(domNode);
            }
            return [...allRootElements];
        }
    };

    function basename(filename, ext) {
        const normalized = filename.replace(/^[a-zA-Z]:/, '').replace(/\\/g, '/');
        let result = normalized.substring(normalized.lastIndexOf('/') + 1);
        if (ext && result.endsWith(ext))
            result = result.substring(0, result.length - ext.length);
        return result;
    }

    function toUpper(_, c) {
        return c ? c.toUpperCase() : '';
    }
    const classifyRE = /(?:^|[-_/])(\w)/g;
    const classify = (str) => {
        return str && str.replace(classifyRE, toUpper);
    };

    function buildComponentsTreeVue3(instance) {
        function getComponentTypeName(options) {
            const name = options.name || options._componentTag || options.__playwright_guessedName;
            if (name)
                return name;
            const file = options.__file;
            if (file)
                return classify(basename(file, '.vue'));
        }

        function saveComponentName(instance, key) {
            instance.type.__playwright_guessedName = key;
            return key;
        }

        function getInstanceName(instance) {
            const name = getComponentTypeName(instance.type || {});
            if (name)
                return name;
            if (instance.root === instance)
                return 'Root';
            for (const key in instance.parent?.type?.components)
                if (instance.parent?.type.components[key] === instance.type)
                    return saveComponentName(instance, key);
            for (const key in instance.appContext?.components)
                if (instance.appContext.components[key] === instance.type)
                    return saveComponentName(instance, key);
            return 'Anonymous Component';
        }

        function isBeingDestroyed(instance) {
            return instance._isBeingDestroyed || instance.isUnmounted;
        }

        function isFragment(instance) {
            return instance.subTree.type.toString() === 'Symbol(Fragment)';
        }

        function getInternalInstanceChildren(subTree) {
            const list = [];
            if (subTree.component)
                list.push(subTree.component);
            if (subTree.suspense)
                list.push(...getInternalInstanceChildren(subTree.suspense.activeBranch));
            if (Array.isArray(subTree.children)) {
                subTree.children.forEach((childSubTree) => {
                    if (childSubTree.component)
                        list.push(childSubTree.component);
                    else
                        list.push(...getInternalInstanceChildren(childSubTree));
                });
            }
            return list.filter(child => !isBeingDestroyed(child) && !child.type.devtools?.hide);
        }

        function getRootElementsFromComponentInstance(instance) {
            if (isFragment(instance))
                return getFragmentRootElements(instance.subTree);
            return [instance.subTree.el];
        }

        function getFragmentRootElements(vnode) {
            if (!vnode.children)
                return [];
            const list = [];
            for (let i = 0, l = vnode.children.length; i < l; i++) {
                const childVnode = vnode.children[i];
                if (childVnode.component)
                    list.push(...getRootElementsFromComponentInstance(childVnode.component));
                else if (childVnode.el)
                    list.push(childVnode.el);
            }
            return list;
        }

        function buildComponentsTree(instance) {
            return {
                name: getInstanceName(instance),
                children: getInternalInstanceChildren(instance.subTree).map(buildComponentsTree),
                rootElements: getRootElementsFromComponentInstance(instance),
                props: instance.props,
            };
        }
        return buildComponentsTree(instance);
    }

    function buildComponentsTreeVue2(instance) {
        function getComponentName(options) {
            const name = options.displayName || options.name || options._componentTag;
            if (name)
                return name;
            const file = options.__file;
            if (file)
                return classify(basename(file, '.vue'));
        }

        function getInstanceName(instance) {
            const name = getComponentName(instance.$options || instance.fnOptions || {});
            if (name)
                return name;
            return instance.$root === instance ? 'Root' : 'Anonymous Component';
        }

        function getInternalInstanceChildren(instance) {
            if (instance.$children)
                return instance.$children;
            if (Array.isArray(instance.subTree.children))
                return instance.subTree.children.filter((vnode) => !!vnode.component).map((vnode) => vnode.component);
            return [];
        }

        function buildComponentsTree(instance) {
            return {
                name: getInstanceName(instance),
                children: getInternalInstanceChildren(instance).map(buildComponentsTree),
                rootElements: [instance.$el],
                props: instance._props,
            };
        }
        return buildComponentsTree(instance);
    }

    function filterComponentsTree(treeNode, searchFn, result = []) {
        if (searchFn(treeNode))
            result.push(treeNode);
        for (const child of treeNode.children)
            filterComponentsTree(child, searchFn, result);
        return result;
    }

    function findVueRoots(root, roots = []) {
        const walker = document.createTreeWalker(root, NodeFilter.SHOW_ELEMENT);
        const vue2Roots = new Set();
        do {
            const node = walker.currentNode;
            if (node.__vue__)
                vue2Roots.add(node.__vue__.$root);
            if (node.__vue_app__ && node._vnode && node._vnode.component)
                roots.push({
                    root: node._vnode.component,
                    version: 3
                });
            const shadowRoot = node instanceof Element ? node.shadowRoot : null;
            if (shadowRoot)
                findVueRoots(shadowRoot, roots);
        } while (walker.nextNode());
        for (const vue2root of vue2Roots) {
            roots.push({
                version: 2,
                root: vue2root,
            });
        }
        return roots;
    }
    const VueEngine = {
        queryAll(scope, selector) {
            const {
                name,
                attributes
            } = parseComponentSelector(selector);
            const vueRoots = findVueRoots(document);
            const trees = vueRoots.map(vueRoot => vueRoot.version === 3 ? buildComponentsTreeVue3(vueRoot.root) : buildComponentsTreeVue2(vueRoot.root));
            const treeNodes = trees.map(tree => filterComponentsTree(tree, treeNode => {
                if (name && treeNode.name !== name)
                    return false;
                if (treeNode.rootElements.some(rootElement => !isInsideScope(scope, rootElement)))
                    return false;
                for (const attr of attributes) {
                    if (!checkComponentAttribute(treeNode.props, attr))
                        return false;
                }
                return true;
            })).flat();
            const allRootElements = new Set();
            for (const treeNode of treeNodes) {
                for (const rootElement of treeNode.rootElements)
                    allRootElements.add(rootElement);
            }
            return [...allRootElements];
        }
    };

    const cacheAllowText = new Map();
    const cacheDisallowText = new Map();

    function generateSelector(injectedScript, targetElement) {
        injectedScript._evaluator.begin();
        try {
            targetElement = targetElement.closest('button,select,input,[role=button],[role=checkbox],[role=radio]') || targetElement;
            const targetTokens = generateSelectorFor(injectedScript, targetElement);
            const bestTokens = targetTokens || [cssFallback(injectedScript, targetElement)];
            const selector = joinTokens(bestTokens);
            const parsedSelector = injectedScript.parseSelector(selector);
            return {
                selector,
                elements: injectedScript.querySelectorAll(parsedSelector, targetElement.ownerDocument)
            };
        } finally {
            cacheAllowText.clear();
            cacheDisallowText.clear();
            injectedScript._evaluator.end();
        }
    }

    function filterRegexTokens(textCandidates) {
        return textCandidates.filter(c => c[0].selector[0] !== '/');
    }

    function generateSelectorFor(injectedScript, targetElement) {
        if (targetElement.ownerDocument.documentElement === targetElement)
            return [{
                engine: 'css',
                selector: 'html',
                score: 1
            }];
        const calculate = (element, allowText) => {
            const allowNthMatch = element === targetElement;
            let textCandidates = allowText ? buildTextCandidates(injectedScript, element, element === targetElement).map(token => [token]) : [];
            if (element !== targetElement) {
                textCandidates = filterRegexTokens(textCandidates);
            }
            const noTextCandidates = buildCandidates(injectedScript, element).map(token => [token]);
            let result = chooseFirstSelector(injectedScript, targetElement.ownerDocument, element, [...textCandidates, ...noTextCandidates], allowNthMatch);
            textCandidates = filterRegexTokens(textCandidates);
            const checkWithText = (textCandidatesToUse) => {
                const allowParentText = allowText && !textCandidatesToUse.length;
                const candidates = [...textCandidatesToUse, ...noTextCandidates].filter(c => {
                    if (!result)
                        return true;
                    return combineScores(c) < combineScores(result);
                });
                let bestPossibleInParent = candidates[0];
                if (!bestPossibleInParent)
                    return;
                for (let parent = parentElementOrShadowHost(element); parent; parent = parentElementOrShadowHost(parent)) {
                    const parentTokens = calculateCached(parent, allowParentText);
                    if (!parentTokens)
                        continue;
                    if (result && combineScores([...parentTokens, ...bestPossibleInParent]) >= combineScores(result))
                        continue;
                    bestPossibleInParent = chooseFirstSelector(injectedScript, parent, element, candidates, allowNthMatch);
                    if (!bestPossibleInParent)
                        return;
                    const combined = [...parentTokens, ...bestPossibleInParent];
                    if (!result || combineScores(combined) < combineScores(result))
                        result = combined;
                }
            };
            checkWithText(textCandidates);
            if (element === targetElement && textCandidates.length)
                checkWithText([]);
            return result;
        };
        const calculateCached = (element, allowText) => {
            const cache = allowText ? cacheAllowText : cacheDisallowText;
            let value = cache.get(element);
            if (value === undefined) {
                value = calculate(element, allowText);
                cache.set(element, value);
            }
            return value;
        };
        return calculateCached(targetElement, true);
    }

    function buildCandidates(injectedScript, element) {
        const candidates = [];
        for (const attribute of ['data-testid', 'data-test-id', 'data-test']) {
            if (element.hasAttribute(attribute))
                candidates.push({
                    engine: 'css',
                    selector: `[${attribute}=${quoteString(element.getAttribute(attribute))}]`,
                    score: 1
                });
        }
        if (element.nodeName === 'INPUT') {
            const input = element;
            if (input.placeholder)
                candidates.push({
                    engine: 'css',
                    selector: `[placeholder=${quoteString(input.placeholder)}]`,
                    score: 10
                });
        }
        if (element.hasAttribute('aria-label'))
            candidates.push({
                engine: 'css',
                selector: `[aria-label=${quoteString(element.getAttribute('aria-label'))}]`,
                score: 10
            });
        if (element.getAttribute('alt') && ['APPLET', 'AREA', 'IMG', 'INPUT'].includes(element.nodeName))
            candidates.push({
                engine: 'css',
                selector: `${cssEscape(element.nodeName.toLowerCase())}[alt=${quoteString(element.getAttribute('alt'))}]`,
                score: 10
            });
        if (element.hasAttribute('role'))
            candidates.push({
                engine: 'css',
                selector: `${cssEscape(element.nodeName.toLowerCase())}[role=${quoteString(element.getAttribute('role'))}]`,
                score: 50
            });
        if (element.getAttribute('name') && ['BUTTON', 'FORM', 'FIELDSET', 'IFRAME', 'INPUT', 'KEYGEN', 'OBJECT', 'OUTPUT', 'SELECT', 'TEXTAREA', 'MAP', 'META', 'PARAM'].includes(element.nodeName))
            candidates.push({
                engine: 'css',
                selector: `${cssEscape(element.nodeName.toLowerCase())}[name=${quoteString(element.getAttribute('name'))}]`,
                score: 50
            });
        if (['INPUT', 'TEXTAREA'].includes(element.nodeName) && element.getAttribute('type') !== 'hidden') {
            if (element.getAttribute('type'))
                candidates.push({
                    engine: 'css',
                    selector: `${cssEscape(element.nodeName.toLowerCase())}[type=${quoteString(element.getAttribute('type'))}]`,
                    score: 50
                });
        }
        if (['INPUT', 'TEXTAREA', 'SELECT'].includes(element.nodeName))
            candidates.push({
                engine: 'css',
                selector: cssEscape(element.nodeName.toLowerCase()),
                score: 50
            });
        const idAttr = element.getAttribute('id');
        if (idAttr && !isGuidLike(idAttr))
            candidates.push({
                engine: 'css',
                selector: makeSelectorForId(idAttr),
                score: 100
            });
        candidates.push({
            engine: 'css',
            selector: cssEscape(element.nodeName.toLowerCase()),
            score: 200
        });
        return candidates;
    }

    function buildTextCandidates(injectedScript, element, allowHasText) {
        if (element.nodeName === 'SELECT')
            return [];
        const text = elementText(injectedScript._evaluator, element).full.trim().replace(/\s+/g, ' ').substring(0, 80);
        if (!text)
            return [];
        const candidates = [];
        let escaped = text;
        if (text.includes('"') || text.includes('>>') || text[0] === '/')
            escaped = `/.*${escapeForRegex(text)}.*/`;
        candidates.push({
            engine: 'text',
            selector: escaped,
            score: 10
        });
        if (allowHasText && escaped === text) {
            let prefix = element.nodeName.toLowerCase();
            if (element.hasAttribute('role'))
                prefix += `[role=${quoteString(element.getAttribute('role'))}]`;
            candidates.push({
                engine: 'css',
                selector: `${prefix}:has-text("${text}")`,
                score: 30
            });
        }
        return candidates;
    }

    function parentElementOrShadowHost(element) {
        if (element.parentElement)
            return element.parentElement;
        if (!element.parentNode)
            return null;
        if (element.parentNode.nodeType === Node.DOCUMENT_FRAGMENT_NODE && element.parentNode.host)
            return element.parentNode.host;
        return null;
    }

    function makeSelectorForId(id) {
        return /^[a-zA-Z][a-zA-Z0-9\-\_]+$/.test(id) ? '#' + id : `[id="${cssEscape(id)}"]`;
    }

    function cssFallback(injectedScript, targetElement) {
        const kFallbackScore = 10000000;
        const root = targetElement.ownerDocument;
        const tokens = [];

        function uniqueCSSSelector(prefix) {
            const path = tokens.slice();
            if (prefix)
                path.unshift(prefix);
            const selector = path.join(' ');
            const parsedSelector = injectedScript.parseSelector(selector);
            const node = injectedScript.querySelector(parsedSelector, targetElement.ownerDocument, false);
            return node === targetElement ? selector : undefined;
        }
        for (let element = targetElement; element && element !== root; element = parentElementOrShadowHost(element)) {
            const nodeName = element.nodeName.toLowerCase();
            let bestTokenForLevel = '';
            if (element.id) {
                const token = makeSelectorForId(element.id);
                const selector = uniqueCSSSelector(token);
                if (selector)
                    return {
                        engine: 'css',
                        selector,
                        score: kFallbackScore
                    };
                bestTokenForLevel = token;
            }
            const parent = element.parentNode;
            const classes = [...element.classList];
            for (let i = 0; i < classes.length; ++i) {
                const token = '.' + classes.slice(0, i + 1).join('.');
                const selector = uniqueCSSSelector(token);
                if (selector)
                    return {
                        engine: 'css',
                        selector,
                        score: kFallbackScore
                    };
                if (!bestTokenForLevel && parent) {
                    const sameClassSiblings = parent.querySelectorAll(token);
                    if (sameClassSiblings.length === 1)
                        bestTokenForLevel = token;
                }
            }
            if (parent) {
                const siblings = [...parent.children];
                const sameTagSiblings = siblings.filter(sibling => (sibling).nodeName.toLowerCase() === nodeName);
                const token = sameTagSiblings.indexOf(element) === 0 ? cssEscape(nodeName) : `${cssEscape(nodeName)}:nth-child(${1 + siblings.indexOf(element)})`;
                const selector = uniqueCSSSelector(token);
                if (selector)
                    return {
                        engine: 'css',
                        selector,
                        score: kFallbackScore
                    };
                if (!bestTokenForLevel)
                    bestTokenForLevel = token;
            } else if (!bestTokenForLevel) {
                bestTokenForLevel = nodeName;
            }
            tokens.unshift(bestTokenForLevel);
        }
        return {
            engine: 'css',
            selector: uniqueCSSSelector(),
            score: kFallbackScore
        };
    }

    function escapeForRegex(text) {
        return text.replace(/[.*+?^>${}()|[\]\\]/g, '\\$&');
    }

    function quoteString(text) {
        return `"${cssEscape(text)}"`;
    }

    function joinTokens(tokens) {
        const parts = [];
        let lastEngine = '';
        for (const {
                engine,
                selector
            } of tokens) {
            if (parts.length && (lastEngine !== 'css' || engine !== 'css' || selector.startsWith(':nth-match(')))
                parts.push('>>');
            lastEngine = engine;
            if (engine === 'css')
                parts.push(selector);
            else
                parts.push(`${engine}=${selector}`);
        }
        return parts.join(' ');
    }

    function combineScores(tokens) {
        let score = 0;
        for (let i = 0; i < tokens.length; i++)
            score += tokens[i].score * (tokens.length - i);
        return score;
    }

    function chooseFirstSelector(injectedScript, scope, targetElement, selectors, allowNthMatch) {
        const joined = selectors.map(tokens => ({
            tokens,
            score: combineScores(tokens)
        }));
        joined.sort((a, b) => a.score - b.score);
        let bestWithIndex = null;
        for (const {
                tokens
            } of joined) {
            const parsedSelector = injectedScript.parseSelector(joinTokens(tokens));
            const result = injectedScript.querySelectorAll(parsedSelector, scope);
            const index = result.indexOf(targetElement);
            if (index === 0) {
                return tokens;
            }
            if (!allowNthMatch || bestWithIndex || index === -1 || result.length > 5)
                continue;
            const allCss = tokens.map(token => {
                if (token.engine !== 'text')
                    return token;
                if (token.selector.startsWith('/') && token.selector.endsWith('/'))
                    return {
                        engine: 'css',
                        selector: `:text-matches("${token.selector.substring(1, token.selector.length - 1)}")`,
                        score: token.score
                    };
                return {
                    engine: 'css',
                    selector: `:text("${token.selector}")`,
                    score: token.score
                };
            });
            const combined = joinTokens(allCss);
            bestWithIndex = [{
                engine: 'css',
                selector: `:nth-match(${combined}, ${index + 1})`,
                score: combineScores(allCss) + 1000
            }];
        }
        return bestWithIndex;
    }

    function isGuidLike(id) {
        let lastCharacterType;
        let transitionCount = 0;
        for (let i = 0; i < id.length; ++i) {
            const c = id[i];
            let characterType;
            if (c === '-' || c === '_')
                continue;
            if (c >= 'a' && c <= 'z')
                characterType = 'lower';
            else if (c >= 'A' && c <= 'Z')
                characterType = 'upper';
            else if (c >= '0' && c <= '9')
                characterType = 'digit';
            else
                characterType = 'other';
            if (characterType === 'lower' && lastCharacterType === 'upper') {
                lastCharacterType = characterType;
                continue;
            }
            if (lastCharacterType && lastCharacterType !== characterType)
                ++transitionCount;
            lastCharacterType = characterType;
        }
        return transitionCount >= id.length / 4;
    }

    function cssEscape(s) {
        let result = '';
        for (let i = 0; i < s.length; i++)
            result += cssEscapeOne(s, i);
        return result;
    }

    function cssEscapeOne(s, i) {
        const c = s.charCodeAt(i);
        if (c === 0x0000)
            return '\uFFFD';
        if ((c >= 0x0001 && c <= 0x001f) ||
            (c >= 0x0030 && c <= 0x0039 && (i === 0 || (i === 1 && s.charCodeAt(0) === 0x002d))))
            return '\\' + c.toString(16) + ' ';
        if (i === 0 && c === 0x002d && s.length === 1)
            return '\\' + s.charAt(i);
        if (c >= 0x0080 || c === 0x002d || c === 0x005f || (c >= 0x0030 && c <= 0x0039) ||
            (c >= 0x0041 && c <= 0x005a) || (c >= 0x0061 && c <= 0x007a))
            return s.charAt(i);
        return '\\' + s.charAt(i);
    }

    class InjectedScript {
        _engines;
        _evaluator;
        _stableRafCount;
        _browserName;
        onGlobalListenersRemoved = new Set();
        _hitTargetInterceptor;
        constructor(stableRafCount, browserName, customEngines) {
            this._evaluator = new SelectorEvaluatorImpl(new Map());
            this._engines = new Map();
            this._engines.set('xpath', XPathEngine);
            this._engines.set('xpath:light', XPathEngine);
            this._engines.set('_react', ReactEngine);
            this._engines.set('_vue', VueEngine);
            this._engines.set('text', this._createTextEngine(true));
            this._engines.set('text:light', this._createTextEngine(false));
            this._engines.set('id', this._createAttributeEngine('id', true));
            this._engines.set('id:light', this._createAttributeEngine('id', false));
            this._engines.set('data-testid', this._createAttributeEngine('data-testid', true));
            this._engines.set('data-testid:light', this._createAttributeEngine('data-testid', false));
            this._engines.set('data-test-id', this._createAttributeEngine('data-test-id', true));
            this._engines.set('data-test-id:light', this._createAttributeEngine('data-test-id', false));
            this._engines.set('data-test', this._createAttributeEngine('data-test', true));
            this._engines.set('data-test:light', this._createAttributeEngine('data-test', false));
            this._engines.set('css', this._createCSSEngine());
            this._engines.set('nth', {
                queryAll: () => []
            });
            this._engines.set('visible', {
                queryAll: () => []
            });
            this._engines.set('control', this._createControlEngine());
            for (const {
                    name,
                    engine
                } of customEngines)
                this._engines.set(name, engine);
            this._stableRafCount = stableRafCount;
            this._browserName = browserName;
            this._setupGlobalListenersRemovalDetection();
            this._setupHitTargetInterceptors();
        }
        eval(expression) {
            return globalThis.eval(expression);
        }
        parseSelector(selector) {
            const result = parseSelector(selector);
            for (const part of result.parts) {
                if (!this._engines.has(part.name))
                    throw this.createStacklessError(`Unknown engine "${part.name}" while parsing selector ${selector}`);
            }
            return result;
        }
        querySelector(selector, root, strict) {
            if (!root['querySelector'])
                throw this.createStacklessError('Node is not queryable.');
            this._evaluator.begin();
            try {
                const result = this._querySelectorRecursively([{
                    element: root,
                    capture: undefined
                }], selector, 0, new Map());
                if (strict && result.length > 1)
                    throw this.strictModeViolationError(selector, result.map(r => r.element));
                return result[0]?.capture || result[0]?.element;
            } finally {
                this._evaluator.end();
            }
        }
        _querySelectorRecursively(roots, selector, index, queryCache) {
            if (index === selector.parts.length)
                return roots;
            const part = selector.parts[index];
            if (part.name === 'nth') {
                let filtered = [];
                if (part.body === '0') {
                    filtered = roots.slice(0, 1);
                } else if (part.body === '-1') {
                    if (roots.length)
                        filtered = roots.slice(roots.length - 1);
                } else {
                    if (typeof selector.capture === 'number')
                        throw this.createStacklessError(`Can't query n-th element in a request with the capture.`);
                    const nth = +part.body;
                    const set = new Set();
                    for (const root of roots) {
                        set.add(root.element);
                        if (nth + 1 === set.size)
                            filtered = [root];
                    }
                }
                return this._querySelectorRecursively(filtered, selector, index + 1, queryCache);
            }
            if (part.name === 'visible') {
                const visible = Boolean(part.body);
                const filtered = roots.filter(match => visible === isVisible(match.element));
                return this._querySelectorRecursively(filtered, selector, index + 1, queryCache);
            }
            const result = [];
            for (const root of roots) {
                const capture = index - 1 === selector.capture ? root.element : root.capture;
                let queryResults = queryCache.get(root.element);
                if (!queryResults) {
                    queryResults = [];
                    queryCache.set(root.element, queryResults);
                }
                let all = queryResults[index];
                if (!all) {
                    all = this._queryEngineAll(selector.parts[index], root.element);
                    queryResults[index] = all;
                }
                for (const element of all) {
                    if (!('nodeName' in element))
                        throw this.createStacklessError(`Expected a Node but got ${Object.prototype.toString.call(element)}`);
                    result.push({
                        element,
                        capture
                    });
                }
            }
            return this._querySelectorRecursively(result, selector, index + 1, queryCache);
        }
        querySelectorAll(selector, root) {
            if (!root['querySelectorAll'])
                throw this.createStacklessError('Node is not queryable.');
            this._evaluator.begin();
            try {
                const result = this._querySelectorRecursively([{
                    element: root,
                    capture: undefined
                }], selector, 0, new Map());
                const set = new Set();
                for (const r of result)
                    set.add(r.capture || r.element);
                return [...set];
            } finally {
                this._evaluator.end();
            }
        }
        _queryEngineAll(part, root) {
            return this._engines.get(part.name).queryAll(root, part.body);
        }
        _createAttributeEngine(attribute, shadow) {
            const toCSS = (selector) => {
                const css = `[${attribute}=${JSON.stringify(selector)}]`;
                return [{
                    simples: [{
                        selector: {
                            css,
                            functions: []
                        },
                        combinator: ''
                    }]
                }];
            };
            return {
                queryAll: (root, selector) => {
                    return this._evaluator.query({
                        scope: root,
                        pierceShadow: shadow
                    }, toCSS(selector));
                }
            };
        }
        _createCSSEngine() {
            const evaluator = this._evaluator;
            return {
                queryAll(root, body) {
                    return evaluator.query({
                        scope: root,
                        pierceShadow: true
                    }, body);
                }
            };
        }
        _createTextEngine(shadow) {
            const queryList = (root, selector) => {
                const {
                    matcher,
                    kind
                } = createTextMatcher(selector);
                const result = [];
                let lastDidNotMatchSelf = null;
                const appendElement = (element) => {
                    if (kind === 'lax' && lastDidNotMatchSelf && lastDidNotMatchSelf.contains(element))
                        return false;
                    const matches = elementMatchesText(this._evaluator, element, matcher);
                    if (matches === 'none')
                        lastDidNotMatchSelf = element;
                    if (matches === 'self' || (matches === 'selfAndChildren' && kind === 'strict'))
                        result.push(element);
                };
                if (root.nodeType === Node.ELEMENT_NODE)
                    appendElement(root);
                const elements = this._evaluator._queryCSS({
                    scope: root,
                    pierceShadow: shadow
                }, '*');
                for (const element of elements)
                    appendElement(element);
                return result;
            };
            return {
                queryAll: (root, selector) => {
                    return queryList(root, selector);
                }
            };
        }
        _createControlEngine() {
            return {
                queryAll(root, body) {
                    if (body === 'enter-frame')
                        return [];
                    if (body === 'return-empty')
                        return [];
                    throw new Error(`Internal error, unknown control selector ${body}`);
                }
            };
        }
        extend(source, params) {
            const constrFunction = globalThis.eval(`
    (() => {
      ${source}
      return pwExport;
    })()`);
            return new constrFunction(this, params);
        }
        isVisible(element) {
            return isVisible(element);
        }
        pollRaf(predicate) {
            return this.poll(predicate, next => requestAnimationFrame(next));
        }
        pollInterval(pollInterval, predicate) {
            return this.poll(predicate, next => setTimeout(next, pollInterval));
        }
        pollLogScale(predicate) {
            const pollIntervals = [100, 250, 500];
            let attempts = 0;
            return this.poll(predicate, next => setTimeout(next, pollIntervals[attempts++] || 1000));
        }
        poll(predicate, scheduleNext) {
            return this._runAbortableTask(progress => {
                let fulfill;
                let reject;
                const result = new Promise((f, r) => {
                    fulfill = f;
                    reject = r;
                });
                const next = () => {
                    if (progress.aborted)
                        return;
                    try {
                        const success = predicate(progress);
                        if (success !== progress.continuePolling)
                            fulfill(success);
                        else
                            scheduleNext(next);
                    } catch (e) {
                        progress.log('  ' + e.message);
                        reject(e);
                    }
                };
                next();
                return result;
            });
        }
        _runAbortableTask(task) {
            let unsentLog = [];
            let takeNextLogsCallback;
            let taskFinished = false;
            const logReady = () => {
                if (!takeNextLogsCallback)
                    return;
                takeNextLogsCallback(unsentLog);
                unsentLog = [];
                takeNextLogsCallback = undefined;
            };
            const takeNextLogs = () => new Promise(fulfill => {
                takeNextLogsCallback = fulfill;
                if (unsentLog.length || taskFinished)
                    logReady();
            });
            let lastMessage = '';
            let lastIntermediateResult = undefined;
            const progress = {
                injectedScript: this,
                aborted: false,
                continuePolling: Symbol('continuePolling'),
                log: (message) => {
                    lastMessage = message;
                    unsentLog.push({
                        message
                    });
                    logReady();
                },
                logRepeating: (message) => {
                    if (message !== lastMessage)
                        progress.log(message);
                },
                setIntermediateResult: (intermediateResult) => {
                    if (lastIntermediateResult === intermediateResult)
                        return;
                    lastIntermediateResult = intermediateResult;
                    unsentLog.push({
                        intermediateResult
                    });
                    logReady();
                },
            };
            const run = () => {
                const result = task(progress);
                result.finally(() => {
                    taskFinished = true;
                    logReady();
                });
                return result;
            };
            return {
                takeNextLogs,
                run,
                cancel: () => {
                    progress.aborted = true;
                },
                takeLastLogs: () => unsentLog,
            };
        }
        getElementBorderWidth(node) {
            if (node.nodeType !== Node.ELEMENT_NODE || !node.ownerDocument || !node.ownerDocument.defaultView)
                return {
                    left: 0,
                    top: 0
                };
            const style = node.ownerDocument.defaultView.getComputedStyle(node);
            return {
                left: parseInt(style.borderLeftWidth || '', 10),
                top: parseInt(style.borderTopWidth || '', 10)
            };
        }
        retarget(node, behavior) {
            let element = node.nodeType === Node.ELEMENT_NODE ? node : node.parentElement;
            if (!element)
                return null;
            if (!element.matches('input, textarea, select'))
                element = element.closest('button, [role=button], [role=checkbox], [role=radio]') || element;
            if (behavior === 'follow-label') {
                if (!element.matches('input, textarea, button, select, [role=button], [role=checkbox], [role=radio]') &&
                    !element.isContentEditable) {
                    element = element.closest('label') || element;
                }
                if (element.nodeName === 'LABEL')
                    element = element.control || element;
            }
            return element;
        }
        waitForElementStatesAndPerformAction(node, states, force, callback) {
            let lastRect;
            let counter = 0;
            let samePositionCounter = 0;
            let lastTime = 0;
            return this.pollRaf(progress => {
                if (force) {
                    progress.log(`    forcing action`);
                    return callback(node, progress);
                }
                for (const state of states) {
                    if (state !== 'stable') {
                        const result = this.elementState(node, state);
                        if (typeof result !== 'boolean')
                            return result;
                        if (!result) {
                            progress.logRepeating(`    element is not ${state} - waiting...`);
                            return progress.continuePolling;
                        }
                        continue;
                    }
                    const element = this.retarget(node, 'no-follow-label');
                    if (!element)
                        return 'error:notconnected';
                    if (++counter === 1)
                        return progress.continuePolling;
                    const time = performance.now();
                    if (this._stableRafCount > 1 && time - lastTime < 15)
                        return progress.continuePolling;
                    lastTime = time;
                    const clientRect = element.getBoundingClientRect();
                    const rect = {
                        x: clientRect.top,
                        y: clientRect.left,
                        width: clientRect.width,
                        height: clientRect.height
                    };
                    const samePosition = lastRect && rect.x === lastRect.x && rect.y === lastRect.y && rect.width === lastRect.width && rect.height === lastRect.height;
                    if (samePosition)
                        ++samePositionCounter;
                    else
                        samePositionCounter = 0;
                    const isStable = samePositionCounter >= this._stableRafCount;
                    const isStableForLogs = isStable || !lastRect;
                    lastRect = rect;
                    if (!isStableForLogs)
                        progress.logRepeating(`    element is not stable - waiting...`);
                    if (!isStable)
                        return progress.continuePolling;
                }
                return callback(node, progress);
            });
        }
        elementState(node, state) {
            const element = this.retarget(node, ['stable', 'visible', 'hidden'].includes(state) ? 'no-follow-label' : 'follow-label');
            if (!element || !element.isConnected) {
                if (state === 'hidden')
                    return true;
                return 'error:notconnected';
            }
            if (state === 'visible')
                return this.isVisible(element);
            if (state === 'hidden')
                return !this.isVisible(element);
            const disabled = isElementDisabled(element);
            if (state === 'disabled')
                return disabled;
            if (state === 'enabled')
                return !disabled;
            const editable = !(['INPUT', 'TEXTAREA', 'SELECT'].includes(element.nodeName) && element.hasAttribute('readonly'));
            if (state === 'editable')
                return !disabled && editable;
            if (state === 'checked' || state === 'unchecked') {
                if (['checkbox', 'radio'].includes(element.getAttribute('role') || '')) {
                    const result = element.getAttribute('aria-checked') === 'true';
                    return state === 'checked' ? result : !result;
                }
                if (element.nodeName !== 'INPUT')
                    throw this.createStacklessError('Not a checkbox or radio button');
                if (!['radio', 'checkbox'].includes(element.type.toLowerCase()))
                    throw this.createStacklessError('Not a checkbox or radio button');
                const result = element.checked;
                return state === 'checked' ? result : !result;
            }
            throw this.createStacklessError(`Unexpected element state "${state}"`);
        }
        selectOptions(optionsToSelect, node, progress) {
            const element = this.retarget(node, 'follow-label');
            if (!element)
                return 'error:notconnected';
            if (element.nodeName.toLowerCase() !== 'select')
                throw this.createStacklessError('Element is not a <select> element');
            const select = element;
            const options = [...select.options];
            const selectedOptions = [];
            let remainingOptionsToSelect = optionsToSelect.slice();
            for (let index = 0; index < options.length; index++) {
                const option = options[index];
                const filter = (optionToSelect) => {
                    if (optionToSelect instanceof Node)
                        return option === optionToSelect;
                    let matches = true;
                    if (optionToSelect.value !== undefined)
                        matches = matches && optionToSelect.value === option.value;
                    if (optionToSelect.label !== undefined)
                        matches = matches && optionToSelect.label === option.label;
                    if (optionToSelect.index !== undefined)
                        matches = matches && optionToSelect.index === index;
                    return matches;
                };
                if (!remainingOptionsToSelect.some(filter))
                    continue;
                selectedOptions.push(option);
                if (select.multiple) {
                    remainingOptionsToSelect = remainingOptionsToSelect.filter(o => !filter(o));
                } else {
                    remainingOptionsToSelect = [];
                    break;
                }
            }
            if (remainingOptionsToSelect.length) {
                progress.logRepeating('    did not find some options - waiting... ');
                return progress.continuePolling;
            }
            select.value = undefined;
            selectedOptions.forEach(option => option.selected = true);
            progress.log('    selected specified option(s)');
            select.dispatchEvent(new Event('input', {
                'bubbles': true
            }));
            select.dispatchEvent(new Event('change', {
                'bubbles': true
            }));
            return selectedOptions.map(option => option.value);
        }
        fill(value, node, progress) {
            const element = this.retarget(node, 'follow-label');
            if (!element)
                return 'error:notconnected';
            if (element.nodeName.toLowerCase() === 'input') {
                const input = element;
                const type = input.type.toLowerCase();
                const kInputTypesToSetValue = new Set(['color', 'date', 'time', 'datetime', 'datetime-local', 'month', 'range', 'week']);
                const kInputTypesToTypeInto = new Set(['', 'email', 'number', 'password', 'search', 'tel', 'text', 'url']);
                if (!kInputTypesToTypeInto.has(type) && !kInputTypesToSetValue.has(type)) {
                    progress.log(`    input of type "${type}" cannot be filled`);
                    throw this.createStacklessError(`Input of type "${type}" cannot be filled`);
                }
                if (type === 'number') {
                    value = value.trim();
                    if (isNaN(Number(value)))
                        throw this.createStacklessError('Cannot type text into input[type=number]');
                }
                if (kInputTypesToSetValue.has(type)) {
                    value = value.trim();
                    input.focus();
                    input.value = value;
                    if (input.value !== value)
                        throw this.createStacklessError('Malformed value');
                    element.dispatchEvent(new Event('input', {
                        'bubbles': true
                    }));
                    element.dispatchEvent(new Event('change', {
                        'bubbles': true
                    }));
                    return 'done';
                }
            } else if (element.nodeName.toLowerCase() === 'textarea');
            else if (!element.isContentEditable) {
                throw this.createStacklessError('Element is not an <input>, <textarea> or [contenteditable] element');
            }
            this.selectText(element);
            return 'needsinput';
        }
        selectText(node) {
            const element = this.retarget(node, 'follow-label');
            if (!element)
                return 'error:notconnected';
            if (element.nodeName.toLowerCase() === 'input') {
                const input = element;
                input.select();
                input.focus();
                return 'done';
            }
            if (element.nodeName.toLowerCase() === 'textarea') {
                const textarea = element;
                textarea.selectionStart = 0;
                textarea.selectionEnd = textarea.value.length;
                textarea.focus();
                return 'done';
            }
            const range = element.ownerDocument.createRange();
            range.selectNodeContents(element);
            const selection = element.ownerDocument.defaultView.getSelection();
            if (selection) {
                selection.removeAllRanges();
                selection.addRange(range);
            }
            element.focus();
            return 'done';
        }
        focusNode(node, resetSelectionIfNotFocused) {
            if (!node.isConnected)
                return 'error:notconnected';
            if (node.nodeType !== Node.ELEMENT_NODE)
                throw this.createStacklessError('Node is not an element');
            const wasFocused = node.getRootNode().activeElement === node && node.ownerDocument && node.ownerDocument.hasFocus();
            node.focus();
            if (resetSelectionIfNotFocused && !wasFocused && node.nodeName.toLowerCase() === 'input') {
                try {
                    const input = node;
                    input.setSelectionRange(0, 0);
                } catch (e) {}
            }
            return 'done';
        }
        setInputFiles(node, payloads) {
            if (node.nodeType !== Node.ELEMENT_NODE)
                return 'Node is not of type HTMLElement';
            const element = node;
            if (element.nodeName !== 'INPUT')
                return 'Not an <input> element';
            const input = element;
            const type = (input.getAttribute('type') || '').toLowerCase();
            if (type !== 'file')
                return 'Not an input[type=file] element';
            const files = payloads.map(file => {
                const bytes = Uint8Array.from(atob(file.buffer), c => c.charCodeAt(0));
                return new File([bytes], file.name, {
                    type: file.mimeType
                });
            });
            const dt = new DataTransfer();
            for (const file of files)
                dt.items.add(file);
            input.files = dt.files;
            input.dispatchEvent(new Event('input', {
                'bubbles': true
            }));
            input.dispatchEvent(new Event('change', {
                'bubbles': true
            }));
        }
        checkHitTargetAt(node, point) {
            let element = node.nodeType === Node.ELEMENT_NODE ? node : node.parentElement;
            if (!element || !element.isConnected)
                return 'error:notconnected';
            element = element.closest('button, [role=button]') || element;
            const hitElement = this.deepElementFromPoint(document, point.x, point.y);
            return this._expectHitTargetParent(hitElement, element);
        }
        _expectHitTargetParent(hitElement, targetElement) {
            const hitParents = [];
            while (hitElement && hitElement !== targetElement) {
                hitParents.push(hitElement);
                hitElement = parentElementOrShadowHost$1(hitElement);
            }
            if (hitElement === targetElement)
                return 'done';
            const hitTargetDescription = this.previewNode(hitParents[0] || document.documentElement);
            let rootHitTargetDescription;
            let element = targetElement;
            while (element) {
                const index = hitParents.indexOf(element);
                if (index !== -1) {
                    if (index > 1)
                        rootHitTargetDescription = this.previewNode(hitParents[index - 1]);
                    break;
                }
                element = parentElementOrShadowHost$1(element);
            }
            if (rootHitTargetDescription)
                return {
                    hitTargetDescription: `${hitTargetDescription} from ${rootHitTargetDescription} subtree`
                };
            return {
                hitTargetDescription
            };
        }
        setupHitTargetInterceptor(node, action, blockAllEvents) {
            const maybeElement = node.nodeType === Node.ELEMENT_NODE ? node : node.parentElement;
            if (!maybeElement || !maybeElement.isConnected)
                return 'error:notconnected';
            const element = maybeElement.closest('button, [role=button]') || maybeElement;
            const events = {
                'hover': kHoverHitTargetInterceptorEvents,
                'tap': kTapHitTargetInterceptorEvents,
                'mouse': kMouseHitTargetInterceptorEvents,
            } [action];
            let result;
            const listener = (event) => {
                if (!events.has(event.type))
                    return;
                if (!event.isTrusted)
                    return;
                const point = (!!window.TouchEvent && (event instanceof window.TouchEvent)) ? event.touches[0] : event;
                if (result === undefined && point) {
                    const hitElement = this.deepElementFromPoint(document, point.clientX, point.clientY);
                    result = this._expectHitTargetParent(hitElement, element);
                }
                if (blockAllEvents || (result !== 'done' && result !== undefined)) {
                    event.preventDefault();
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                }
            };
            const stop = () => {
                if (this._hitTargetInterceptor === listener)
                    this._hitTargetInterceptor = undefined;
                return result || 'done';
            };
            this._hitTargetInterceptor = listener;
            return {
                stop
            };
        }
        dispatchEvent(node, type, eventInit) {
            let event;
            eventInit = {
                bubbles: true,
                cancelable: true,
                composed: true,
                ...eventInit
            };
            switch (eventType.get(type)) {
                case 'mouse':
                    event = new MouseEvent(type, eventInit);
                    break;
                case 'keyboard':
                    event = new KeyboardEvent(type, eventInit);
                    break;
                case 'touch':
                    event = new TouchEvent(type, eventInit);
                    break;
                case 'pointer':
                    event = new PointerEvent(type, eventInit);
                    break;
                case 'focus':
                    event = new FocusEvent(type, eventInit);
                    break;
                case 'drag':
                    event = new DragEvent(type, eventInit);
                    break;
                default:
                    event = new Event(type, eventInit);
                    break;
            }
            node.dispatchEvent(event);
        }
        deepElementFromPoint(document, x, y) {
            let container = document;
            let element;
            while (container) {
                const elements = container.elementsFromPoint(x, y);
                const innerElement = elements[0];
                if (!innerElement || element === innerElement)
                    break;
                element = innerElement;
                container = element.shadowRoot;
            }
            return element;
        }
        previewNode(node) {
            if (node.nodeType === Node.TEXT_NODE)
                return oneLine(`#text=${node.nodeValue || ''}`);
            if (node.nodeType !== Node.ELEMENT_NODE)
                return oneLine(`<${node.nodeName.toLowerCase()} />`);
            const element = node;
            const attrs = [];
            for (let i = 0; i < element.attributes.length; i++) {
                const {
                    name,
                    value
                } = element.attributes[i];
                if (name === 'style' || name.startsWith('__playwright'))
                    continue;
                if (!value && booleanAttributes.has(name))
                    attrs.push(` ${name}`);
                else
                    attrs.push(` ${name}="${value}"`);
            }
            attrs.sort((a, b) => a.length - b.length);
            let attrText = attrs.join('');
            if (attrText.length > 50)
                attrText = attrText.substring(0, 49) + '\u2026';
            if (autoClosingTags.has(element.nodeName))
                return oneLine(`<${element.nodeName.toLowerCase()}${attrText}/>`);
            const children = element.childNodes;
            let onlyText = false;
            if (children.length <= 5) {
                onlyText = true;
                for (let i = 0; i < children.length; i++)
                    onlyText = onlyText && children[i].nodeType === Node.TEXT_NODE;
            }
            let text = onlyText ? (element.textContent || '') : (children.length ? '\u2026' : '');
            if (text.length > 50)
                text = text.substring(0, 49) + '\u2026';
            return oneLine(`<${element.nodeName.toLowerCase()}${attrText}>${text}</${element.nodeName.toLowerCase()}>`);
        }
        strictModeViolationError(selector, matches) {
            const infos = matches.slice(0, 10).map(m => ({
                preview: this.previewNode(m),
                selector: generateSelector(this, m).selector
            }));
            const lines = infos.map((info, i) => `\n    ${i + 1}) ${info.preview} aka playwright.$("${info.selector}")`);
            if (infos.length < matches.length)
                lines.push('\n    ...');
            return this.createStacklessError(`strict mode violation: "${stringifySelector(selector)}" resolved to ${matches.length} elements:${lines.join('')}\n`);
        }
        createStacklessError(message) {
            if (this._browserName === 'firefox') {
                const error = new Error('Error: ' + message);
                error.stack = '';
                return error;
            }
            const error = new Error(message);
            delete error.stack;
            return error;
        }
        _setupGlobalListenersRemovalDetection() {
            const customEventName = '__playwright_global_listeners_check__';
            let seenEvent = false;
            const handleCustomEvent = () => seenEvent = true;
            window.addEventListener(customEventName, handleCustomEvent);
            new MutationObserver(entries => {
                const newDocumentElement = entries.some(entry => Array.from(entry.addedNodes).includes(document.documentElement));
                if (!newDocumentElement)
                    return;
                seenEvent = false;
                window.dispatchEvent(new CustomEvent(customEventName));
                if (seenEvent)
                    return;
                window.addEventListener(customEventName, handleCustomEvent);
                for (const callback of this.onGlobalListenersRemoved)
                    callback();
            }).observe(document, {
                childList: true
            });
        }
        _setupHitTargetInterceptors() {
            const listener = (event) => this._hitTargetInterceptor?.(event);
            const addHitTargetInterceptorListeners = () => {
                for (const event of kAllHitTargetInterceptorEvents)
                    window.addEventListener(event, listener, {
                        capture: true,
                        passive: false
                    });
            };
            addHitTargetInterceptorListeners();
            this.onGlobalListenersRemoved.add(addHitTargetInterceptorListeners);
        }
        expectSingleElement(progress, element, options) {
            const injected = progress.injectedScript;
            const expression = options.expression; {
                let elementState;
                if (expression === 'to.be.checked') {
                    elementState = progress.injectedScript.elementState(element, 'checked');
                } else if (expression === 'to.be.unchecked') {
                    elementState = progress.injectedScript.elementState(element, 'unchecked');
                } else if (expression === 'to.be.disabled') {
                    elementState = progress.injectedScript.elementState(element, 'disabled');
                } else if (expression === 'to.be.editable') {
                    elementState = progress.injectedScript.elementState(element, 'editable');
                } else if (expression === 'to.be.empty') {
                    if (element.nodeName === 'INPUT' || element.nodeName === 'TEXTAREA')
                        elementState = !element.value;
                    else
                        elementState = !element.textContent?.trim();
                } else if (expression === 'to.be.enabled') {
                    elementState = progress.injectedScript.elementState(element, 'enabled');
                } else if (expression === 'to.be.focused') {
                    elementState = document.activeElement === element;
                } else if (expression === 'to.be.hidden') {
                    elementState = progress.injectedScript.elementState(element, 'hidden');
                } else if (expression === 'to.be.visible') {
                    elementState = progress.injectedScript.elementState(element, 'visible');
                }
                if (elementState !== undefined) {
                    if (elementState === 'error:notcheckbox')
                        throw injected.createStacklessError('Element is not a checkbox');
                    if (elementState === 'error:notconnected')
                        throw injected.createStacklessError('Element is not connected');
                    return {
                        received: elementState,
                        matches: elementState
                    };
                }
            } {
                if (expression === 'to.have.property') {
                    const received = element[options.expressionArg];
                    const matches = deepEquals(received, options.expectedValue);
                    return {
                        received,
                        matches
                    };
                }
            } {
                let received;
                if (expression === 'to.have.attribute') {
                    received = element.getAttribute(options.expressionArg) || '';
                } else if (expression === 'to.have.class') {
                    received = element.className;
                } else if (expression === 'to.have.css') {
                    received = window.getComputedStyle(element)[options.expressionArg];
                } else if (expression === 'to.have.id') {
                    received = element.id;
                } else if (expression === 'to.have.text') {
                    received = options.useInnerText ? element.innerText : element.textContent || '';
                } else if (expression === 'to.have.title') {
                    received = document.title;
                } else if (expression === 'to.have.url') {
                    received = document.location.href;
                } else if (expression === 'to.have.value') {
                    element = this.retarget(element, 'follow-label');
                    if (element.nodeName !== 'INPUT' && element.nodeName !== 'TEXTAREA' && element.nodeName !== 'SELECT')
                        throw this.createStacklessError('Not an input element');
                    received = element.value;
                }
                if (received !== undefined && options.expectedText) {
                    const matcher = new ExpectedTextMatcher(options.expectedText[0]);
                    return {
                        received,
                        matches: matcher.matches(received)
                    };
                }
            }
            throw this.createStacklessError('Unknown expect matcher: ' + expression);
        }
        expectArray(elements, options) {
            const expression = options.expression;
            if (expression === 'to.have.count') {
                const received = elements.length;
                const matches = received === options.expectedNumber;
                return {
                    received,
                    matches
                };
            }
            let received;
            if (expression === 'to.have.text.array' || expression === 'to.contain.text.array')
                received = elements.map(e => options.useInnerText ? e.innerText : e.textContent || '');
            else if (expression === 'to.have.class.array')
                received = elements.map(e => e.className);
            if (received && options.expectedText) {
                const lengthShouldMatch = expression !== 'to.contain.text.array';
                const matchesLength = received.length === options.expectedText.length || !lengthShouldMatch;
                if (!matchesLength)
                    return {
                        received,
                        matches: false
                    };
                let i = 0;
                const matchers = options.expectedText.map(e => new ExpectedTextMatcher(e));
                let allMatchesFound = true;
                for (const matcher of matchers) {
                    while (i < received.length && !matcher.matches(received[i]))
                        i++;
                    if (i >= received.length) {
                        allMatchesFound = false;
                        break;
                    }
                }
                return {
                    received,
                    matches: allMatchesFound
                };
            }
            throw this.createStacklessError('Unknown expect matcher: ' + expression);
        }
    }
    const autoClosingTags = new Set(['AREA', 'BASE', 'BR', 'COL', 'COMMAND', 'EMBED', 'HR', 'IMG', 'INPUT', 'KEYGEN', 'LINK', 'MENUITEM', 'META', 'PARAM', 'SOURCE', 'TRACK', 'WBR']);
    const booleanAttributes = new Set(['checked', 'selected', 'disabled', 'readonly', 'multiple']);

    function oneLine(s) {
        return s.replace(/\n/g, '↵').replace(/\t/g, '⇆');
    }
    const eventType = new Map([
        ['auxclick', 'mouse'],
        ['click', 'mouse'],
        ['dblclick', 'mouse'],
        ['mousedown', 'mouse'],
        ['mouseeenter', 'mouse'],
        ['mouseleave', 'mouse'],
        ['mousemove', 'mouse'],
        ['mouseout', 'mouse'],
        ['mouseover', 'mouse'],
        ['mouseup', 'mouse'],
        ['mouseleave', 'mouse'],
        ['mousewheel', 'mouse'],
        ['keydown', 'keyboard'],
        ['keyup', 'keyboard'],
        ['keypress', 'keyboard'],
        ['textInput', 'keyboard'],
        ['touchstart', 'touch'],
        ['touchmove', 'touch'],
        ['touchend', 'touch'],
        ['touchcancel', 'touch'],
        ['pointerover', 'pointer'],
        ['pointerout', 'pointer'],
        ['pointerenter', 'pointer'],
        ['pointerleave', 'pointer'],
        ['pointerdown', 'pointer'],
        ['pointerup', 'pointer'],
        ['pointermove', 'pointer'],
        ['pointercancel', 'pointer'],
        ['gotpointercapture', 'pointer'],
        ['lostpointercapture', 'pointer'],
        ['focus', 'focus'],
        ['blur', 'focus'],
        ['drag', 'drag'],
        ['dragstart', 'drag'],
        ['dragend', 'drag'],
        ['dragover', 'drag'],
        ['dragenter', 'drag'],
        ['dragleave', 'drag'],
        ['dragexit', 'drag'],
        ['drop', 'drag'],
    ]);
    const kHoverHitTargetInterceptorEvents = new Set(['mousemove']);
    const kTapHitTargetInterceptorEvents = new Set(['pointerdown', 'pointerup', 'touchstart', 'touchend', 'touchcancel']);
    const kMouseHitTargetInterceptorEvents = new Set(['mousedown', 'mouseup', 'pointerdown', 'pointerup', 'click', 'auxclick', 'dblclick', 'contextmenu']);
    const kAllHitTargetInterceptorEvents = new Set([...kHoverHitTargetInterceptorEvents, ...kTapHitTargetInterceptorEvents, ...kMouseHitTargetInterceptorEvents]);

    function unescape(s) {
        if (!s.includes('\\'))
            return s;
        const r = [];
        let i = 0;
        while (i < s.length) {
            if (s[i] === '\\' && i + 1 < s.length)
                i++;
            r.push(s[i++]);
        }
        return r.join('');
    }

    function createTextMatcher(selector) {
        if (selector[0] === '/' && selector.lastIndexOf('/') > 0) {
            const lastSlash = selector.lastIndexOf('/');
            const matcher = createRegexTextMatcher(selector.substring(1, lastSlash), selector.substring(lastSlash + 1));
            return {
                matcher,
                kind: 'regex'
            };
        }
        let strict = false;
        if (selector.length > 1 && selector[0] === '"' && selector[selector.length - 1] === '"') {
            selector = unescape(selector.substring(1, selector.length - 1));
            strict = true;
        }
        if (selector.length > 1 && selector[0] === "'" && selector[selector.length - 1] === "'") {
            selector = unescape(selector.substring(1, selector.length - 1));
            strict = true;
        }
        const matcher = strict ? createStrictTextMatcher(selector) : createLaxTextMatcher(selector);
        return {
            matcher,
            kind: strict ? 'strict' : 'lax'
        };
    }
    class ExpectedTextMatcher {
        _string;
        _substring;
        _regex;
        _normalizeWhiteSpace;
        constructor(expected) {
            this._normalizeWhiteSpace = expected.normalizeWhiteSpace;
            this._string = expected.matchSubstring ? undefined : this.normalizeWhiteSpace(expected.string);
            this._substring = expected.matchSubstring ? this.normalizeWhiteSpace(expected.string) : undefined;
            this._regex = expected.regexSource ? new RegExp(expected.regexSource, expected.regexFlags) : undefined;
        }
        matches(text) {
            if (this._normalizeWhiteSpace && !this._regex)
                text = this.normalizeWhiteSpace(text);
            if (this._string !== undefined)
                return text === this._string;
            if (this._substring !== undefined)
                return text.includes(this._substring);
            if (this._regex)
                return !!this._regex.test(text);
            return false;
        }
        normalizeWhiteSpace(s) {
            if (!s)
                return s;
            return this._normalizeWhiteSpace ? s.trim().replace(/\u200b/g, '').replace(/\s+/g, ' ') : s;
        }
    }

    function deepEquals(a, b) {
        if (a === b)
            return true;
        if (a && b && typeof a === 'object' && typeof b === 'object') {
            if (a.constructor !== b.constructor)
                return false;
            if (Array.isArray(a)) {
                if (a.length !== b.length)
                    return false;
                for (let i = 0; i < a.length; ++i) {
                    if (!deepEquals(a[i], b[i]))
                        return false;
                }
                return true;
            }
            if (a instanceof RegExp)
                return a.source === b.source && a.flags === b.flags;
            if (a.valueOf !== Object.prototype.valueOf)
                return a.valueOf() === b.valueOf();
            if (a.toString !== Object.prototype.toString)
                return a.toString() === b.toString();
            const keys = Object.keys(a);
            if (keys.length !== Object.keys(b).length)
                return false;
            for (let i = 0; i < keys.length; ++i) {
                if (!b.hasOwnProperty(keys[i]))
                    return false;
            }
            for (const key of keys) {
                if (!deepEquals(a[key], b[key]))
                    return false;
            }
            return true;
        }
        if (typeof a === 'number' && typeof b === 'number')
            return isNaN(a) && isNaN(b);
        return false;
    }

    function isElementDisabled(element) {
        const isRealFormControl = ['BUTTON', 'INPUT', 'SELECT', 'TEXTAREA'].includes(element.nodeName);
        if (isRealFormControl && element.hasAttribute('disabled'))
            return true;
        if (isRealFormControl && hasDisabledFieldSet(element))
            return true;
        if (hasAriaDisabled(element))
            return true;
        return false;
    }

    function hasDisabledFieldSet(element) {
        if (!element)
            return false;
        if (element.tagName === 'FIELDSET' && element.hasAttribute('disabled'))
            return true;
        return hasDisabledFieldSet(element.parentElement);
    }

    function hasAriaDisabled(element) {
        if (!element)
            return false;
        const attribute = (element.getAttribute('aria-disabled') || '').toLowerCase();
        if (attribute === 'true')
            return true;
        if (attribute === 'false')
            return false;
        return hasAriaDisabled(parentElementOrShadowHost$1(element));
    }

    exports.InjectedScript = InjectedScript;
    exports["default"] = InjectedScript;

    Object.defineProperty(exports, '__esModule', {
        value: true
    });

    return exports;

})({});

cdp4j.engine = new cdp4j.InjectedScript(0, 'chromium', []);

cdp4j.querySelector = function(doc, selector) {
    const parsedSelector = cdp4j.engine.parseSelector(selector);
    return cdp4j.engine.querySelector(parsedSelector, doc);
};

Object.freeze(cdp4j);

//# sourceURL=cdp4j-playwright-engine.js;